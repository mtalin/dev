﻿/*  This file is part of SevenZipSharp.

    SevenZipSharp is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SevenZipSharp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SevenZipSharp.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Forms;
using System.Reflection;
using SevenZip;
using System.Diagnostics;
using System.Linq;

namespace SevenZipTest
{
    class Program
    {
		public static bool Silent = false;

		static void Main(string[] args)
		{
			if (args.Length > 0 && args[0] != ".")
				SevenZipBase.SetLibraryPath(Path.GetFullPath(args[0]));
			if (!Silent)
			{
				var libraryVersion = SevenZipBase.GetLibraryVersion();
				Console.WriteLine(libraryVersion);
			}
			if (args.Length > 1)
				Go0(args[1]);
			if (args.Length > 1)
				Go1(args[1]);
		}

		public static void Go0(string path)
		{
			int total = GetArchiveEntries0(path).Count;
			DateTime t0 = new DateTime();
			if (!Silent)
				t0 = DateTime.UtcNow;
			for (int i = 0; i < 5000; ++i)
			{
				var aa = GetArchiveEntries0(path);
				total += aa.Count;
			}
			if (!Silent)
			{
				var t = DateTime.UtcNow - t0;
				Console.WriteLine($"Count: {total}, {t.TotalSeconds} seconds.");
			}
		}

		public static ICollection<DocumentArchiveEntry> GetArchiveEntries0(string path)
		{
			using (var sz = new SevenZip.SevenZipExtractor(path))
			{
				var result = sz.ArchiveFileData.Select(o =>
					new DocumentArchiveEntry
					{
						FileName = o.FileName,
						LastWriteTime = o.LastWriteTime,
						Length = (int)o.Size,
					}).ToList();
				return result;
			}
		}


		public static void Go1(string path)
		{
			int total = GetArchiveEntries0(path).Count;
			DateTime t0 = new DateTime();
			if (!Silent)
				t0 = DateTime.UtcNow;
			var bytes = File.ReadAllBytes(path);
			for (int i = 0; i < 5000; ++i)
			{
				using (var s = new MemoryStream(bytes))
				{
					var aa = GetArchiveEntries1(s);
					total += aa.Count;
				}
			}
			if (!Silent)
			{
				var t = DateTime.UtcNow - t0;
				Console.WriteLine($"Count: {total}, {t.TotalSeconds} seconds.");
			}
		}

		public static ICollection<DocumentArchiveEntry> GetArchiveEntries1(Stream stream)
		{
			using (var sz = new SevenZip.SevenZipExtractor(stream))
			{
				var result = sz.ArchiveFileData.Select(o =>
					new DocumentArchiveEntry
					{
						FileName = o.FileName,
						LastWriteTime = o.LastWriteTime,
						Length = (int)o.Size,
					}).ToList();
				return result;
			}
		}
	}

	internal class DocumentArchiveEntry
	{
		public string FileName { get; set; }
		public DateTime LastWriteTime { get; set; }
		public int Length { get; set; }
	}
}
