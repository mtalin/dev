<#
.SYNOPSIS
    Initialize C:\Application folder configuring Configuration, Data and Objects directories as symlinks.
#>
#Requires -RunAsAdministrator

[CmdletBinding()]
param (
    # Git source repository location
    # Default: C:\Projects
    [string]$root = "C:\Projects",
    # Subdirectory for the FsAdmin project
    # Default: FsAdmin
    [string]$fsa = "FsAdmin",
    # Subdirectory for the FsAdmin.dev project
    # Default: FsAdmin.dev
    [string]$dev = "FsAdmin.dev",
    # Use Release configuration for Objects
    [switch]$release = $false
)

$fsaSrc = [System.IO.Path]::Combine($root, $fsa, "src");
$devSrc = [System.IO.Path]::Combine($root, $dev);

if (!(Test-Path $fsaSrc -PathType:Container)) {
    throw "Cannot find FsAdmin source directory ($fsaSrc)"
    return -1;
}
if (!(Test-Path $devSrc -PathType:Container)) {
    throw "Cannot find FsAdmin.ev source directory ($devSrc)"
    return -1;
}

$app = "C:\Application";
if (!(Test-Path $app -PathType:Container)) { New-Item $app -ItemType:Directory }
if (Test-Path $app\Config) { Remove-Item $app\Config -Force -Recurse }
if (Test-Path $app\Configuration) { Remove-Item $app\Configuration -Force -Recurse }
if (Test-Path $app\Data) { Remove-Item $app\Data -Force -Recurse }
if (Test-Path $app\Objects) { Remove-Item $app\Objects -Force -Recurse }

echo "$app\Config                      => $fsaSrc\Config"
New-Item $app\Config -Value:$fsaSrc\Config -ItemType:Junction >$null
if (Test-Path $app\Config\env) { Remove-Item $app\Config\env -Force -Recurse }
if (Test-Path $app\Config\Configuration) { Remove-Item $app\Config\Configuration -Force -Recurse }

echo "$app\Config\env                  => $devSrc\Application\Config\env"
New-Item $app\Config\env -Value:$devSrc\Application\Config\env -ItemType:Junction >$null

echo "$app\Config\Configuration        => $devSrc\Application\Config\Configuration"
New-Item $app\Config\Configuration -Value:$devSrc\Application\Config\Configuration -ItemType:Junction >$null

echo "$app\Configuration               => $devSrc\Application\Config\Configuration"
New-Item $app\Configuration -Value:$devSrc\Application\Config\Configuration -ItemType:Junction >$null

if (Test-Path $app\Configuration\fs-access.txt) { Remove-Item $app\Configuration\fs-access.txt -Force }
echo "$app\Configuration\fs-access.txt => $app\Config\fs-access.txt"
New-Item $app\Configuration\fs-access.txt -Value:$app\Config\fs-access.txt -ItemType:SymbolicLink >$null

echo "$app\Data                        => $fsaSrc\Data"
New-Item $app\Data -Value:$fsaSrc\Data -ItemType:Junction >$null
if (Test-Path $fsaSrc\Externs.root\Collector\bin\$(if ($release) { 'Release' } else { 'Debug' })\Objects) {
    echo "$app\Objects                     => $fsaSrc\Externs.root\Collector\bin\$(if ($release) { 'Release' } else { 'Debug' })\Objects"
    New-Item $app\Objects -Value:$fsaSrc\Externs.root\Collector\bin\$(if ($release) { 'Release' } else { 'Debug' })\Objects -ItemType:Junction >$null
} else {
    Write-Warning "Objects directory not found ($fsaSrc\Externs.root\Collector\bin\$(if ($release) { 'Release' } else { 'Debug' })\Objects)"
}
