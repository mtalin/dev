:**/writers/writer	name	class	file
:**/rules/rule		target	type	include


configuration
	include	Fs.security.config.txt
	include	Fs.config.txt

	logging
		writers
			.	TextFile	Lexxys.Logging.TextFileLogWriter	C:\Logs\{YMD}-BC.log
		rules
			.	TextFile	All		*
	/logging

	database
		connection
			=server		blue\d
			=database	CharityPlanner
			=user		sa
			=password	post+Office2
		driver	SqlDataAdapter

	crypto
		providers
		
			:encryptor	name, class									assembly
			. DES 		Lexxys.Crypting.Cryptors.DesCryptor 		C:\Application\Common\StdCryptors.dll
			. RSA 		Lexxys.Crypting.Cryptors.RsaEncryptor 		C:\Application\Common\StdCryptors.dll
			
			:decryptor	name, class									assembly
			. DES 		Lexxys.Crypting.Cryptors.DesCryptor 		C:\Application\Common\StdCryptors.dll
			. RSA 		Lexxys.Crypting.Cryptors.RsaDecryptor 		C:\Application\Common\StdCryptors.dll
			. TripleDES	Lexxys.Crypting.Cryptors.TripleDesCryptor	C:\Application\Common\StdCryptors.dll
			
			:hasher		name, class									assembly
			. MD5 		Lexxys.Crypting.Cryptors.MD5Hasher			C:\Application\Common\StdCryptors.dll
			. MD5p		Lexxys.Crypting.Cryptors.MD5pHasher 		C:\Application\Common\MD5pHash.dll
			. SHA		Lexxys.Crypting.Cryptors.Sha1Hasher 		C:\Application\Common\StdCryptors.dll
			. SHA2		Lexxys.Crypting.Cryptors.Sha2Hasher 		C:\Application\Common\StdCryptors.dll
			. SHA3		Lexxys.Crypting.Cryptors.Sha3Hasher 		C:\Application\Common\StdCryptors.dll
			. SHA5		Lexxys.Crypting.Cryptors.Sha5Hasher 		C:\Application\Common\StdCryptors.dll
	/crypto

	stateMachines
		:*				name
		:*/state		id name
		:*/*/transition	action	=> target

		stateMachine Matrix

			. 0	 Default
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 10 InPrep
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 20 ReadyForReview
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 30 FirstReview
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 40 SecondReview
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 50 FinalCorrections
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 60 ReadyToPrintForClient
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 65 ReadyToMailToClient
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 70 SentToClient
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 80 SignedAndReceived
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

			. 90 ReadyForFiling
				. InPrep				=> 10
				. ReadyForReview		=> 20
				. FirstReview			=> 30
				. SecondReview			=> 40
				. FinalCorrections		=> 50
				. ReadyToPrintForClient	=> 60
				. ReadyToMailToClient	=> 65
				. SentToClient			=> 70
				. SignedAndReceived		=> 80
				. ReadyForFiling		=> 90

		/stateMachine Matrix
