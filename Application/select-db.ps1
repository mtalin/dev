param (
    [string]$db = $null,
    [string]$data = $null
)

$localData = 'C:\Temp\FsData'

$app = "C:\Application"

if ([String]::IsNullOrWhiteSpace($db) -or $db -ieq '(local)' -or $db -ieq 'local' -or $db -ieq '.') {
	$db = '(local)';
	if ([String]::IsNullOrWhiteSpace($data)) { $data = $localData; }
}
else {
    if ($db.StartsWith('\\')) { $db = $db.Substring(2); }
    if ([String]::IsNullOrWhiteSpace($data)) {
		$data = if ($db -match '(.*?)([\\|/](.+))') { "\\$($matches[1])\data\$($matches[3])" } else { "\\$db\data" }
	}
}

<#
	db
		server		blue\b
		user		sa
		password	post+Office2

	data
		local		C:\Application\Data
		check		\\blue\data\b\Checks
		blob		\\blue\data\b\Documents
		taxreports	C:\Application\Data\Reports
		checksIncoming	\\blue\data\b\Checks\TransferChecks
		checksProcessed	\\blue\data\b\Checks\ProcessedCheckFiles
#>

$file = "$app\Config\env\environment.config.txt"

$Error.Clear();
$s = [System.IO.File]::ReadAllText($file);
if ($Error.Count -gt 0) { throw "Cannot read $file"; return -1; }
echo $file;

if ($s -match '(\n\s+db\s*\n\s+server\s+)([^\s]+)') {
	$s = $s -replace '(\n\s+db\s*\n\s+server\s+)([^\s]+)', "`$1$db"
} else {
	Write-Warning "db.server item not found"
}

$parts = @(
	@{ A = "check"; B = "Checks" },
	@{ A = "blob"; B = "Documents" },
	@{ A = "checksIncoming"; B = "Checks\TransferChecks" },
	@{ A = "checksProcessed"; B = "Checks\ProcessedCheckFiles" }
);

foreach ($item in $parts) {
	$rex = "(\n\s+$($item.A)\s+)[^\s]+";
	if ($s -match $rex) {
		$s = $s -replace $rex, "`$1$data\$($item.B)"
	} else {
		Write-Warning "data.$($item.A) item not found"
	}
}

[System.IO.File]::WriteAllText($file, $s);


$file = "$app\Configuration\global.config.txt"

$Error.Clear();
$s = [System.IO.File]::ReadAllText($file);
if ($Error.Count -gt 0) { throw "Cannot read $file"; return -1; }
echo $file;

$s = $s -replace '(\n\s*=server\s+)[^\s]*', "`$1$db"
[System.IO.File]::WriteAllText($file, $s);


$file = "$app\\Configuration\Fs.base.config.xml"

$Error.Clear();
$s = [System.IO.File]::ReadAllText($file);
if ($Error.Count -gt 0) { throw "Cannot read $file"; return -1; }
echo $file;

$s = $s -replace '<server>[^<]*</server>', "<server>$db</server>"
[System.IO.File]::WriteAllText($file, $s);
