@echo off
set proj=C:\Projects\FoundationSource

set b=%1
if exist ..\..\Main\* (
	set proj=..\..
	if [%1]==[] set b=Main
)
if not exist %proj%\Main\* (
	echo ERROR: update reference to FoundationSource project in %~nx0
	goto :EOF
)
if not exist %proj%\%b%\Config (
	echo usage: %~nx0 BranchName [R[elese]]
	goto :EOF
)

set app=C:\Application
if not exist %app% md %app%
if not exist %app%\select-branch.bat copy %proj%\Dev\Application\s* %app%

set d=Debug
set r=%2
if /i "%r:~0,1%" equ "r" set d=Release

if exist %app%\Config\* rmdir Config
if exist %app%\Data\* rmdir Data
if exist %app%\Objects\* rmdir Objects

mklink /j %app%\Config %proj%\%b%\Config
if not exist %proj%\%b%\Data md %proj%\%b%\Data
mklink /j %app%\Data %proj%\%b%\Data
if not exist %proj%\%b%\Externs.root\Collector\bin\%d%\Objects md %proj%\%b%\Externs.root\Collector\bin\%d%\Objects
mklink /j %app%\Objects %proj%\%b%\Externs.root\Collector\bin\%d%\Objects

if not exist %app%\Tools mklink /j %app%\Tools %proj%\Dev\Application\Tools
if not exist %app%\Configuration\* mklink /j %app%\Configuration %app%\Config\Configuration
