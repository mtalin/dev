﻿$script:ApplicationFolder = 'C:\Application';
$script:DC = @{
	Server = 'DB1';
	Database = 'CharityPlanner';
	};

function Config([string]$Server, [string]$Database, [string]$Password)
{
	$config = @{ Database = $script:DC; }
	$configFile = ".;$PSScriptRoot;$script:ApplicationFolder\Config" -split ';' | % { join-path $_ 'deploy.config.ps1' } | ? { test-path $_ -pathType:leaf } | select -first 1;
	if ($WhatIf) { Write-Host "What if: Use config file: `"$configFile`"" -ForegroundColor Green; }
	if ($configFile -ne $null)
	{
		$eval = [IO.File]::ReadAllText($configFile);
		$block = iex "{`r`n$eval`r`n}"
		& $block;
	}
	if ($Server -ne '') { $config.Database.Server = $Server; }
	if ($Database -ne '') { $config.Database.Database = $Database; }
	if ($Password -ne '') { $config.Database.Password = $Password; }
	if ($config.Database.Server -ieq 'local') { $config.Database.Server = '(local)'; }
	if ($WhatIf) { Write-Host "What if: Configuration: Server=`"$($config.Database.Server)`"; Database=`"$($config.Database.Database)`"; Logon=$(if ([string]$config.Database.Password -eq '') { "windows" } else { "sa" })" -ForegroundColor Green; }
	$script:DC = $config.Database;
}

function RunSql([string]$expression, [switch]$scalar)
{
	$statement = $expression.Trim();
	if ($statement -eq '') { return }
	$connection = "Server=$($script:DC.Server);Database=$($script:DC.Database);"
	if ([string]$script:DC.Password -eq '')
		{ $connection += "Trusted_Connection=yes;"; }
	else
		{ $connection += "User ID=sa;Password=$($script:DC.Password);"; }
	try
	{
		$cn = $null;
		$cn = New-Object Data.SqlClient.SqlConnection($connection);
		$cn.Open()

		try
		{
			$cm = New-Object Data.SqlClient.SqlCommand($statement, $cn)
			$cm.CommandTimeout = 3600;
			if ($scalar) { return $cm.ExecuteScalar(); }

			$rd = $cm.ExecuteReader();
			$result = New-Object System.Collections.ArrayList
			while ($rd.Read())
			{
				$row = @{}
				for ($i = 0; $i -lt $rd.FieldCount; ++$i)
				{
					$name = ([string]$rd.GetName($i), [string]$i -ne '')[0];
					if ($rd.IsDBNull($i))
						{ $row[$name] = $null; }
					else
						{ $row[$name] = $rd.GetValue($i); }
				}
				[void]$result.Add((New-Object PsObject -Property $row));
			}
			return $result;
		}
		catch
		{
			throw
		}
		finally
		{
			if (-not [Object]::ReferenceEquals($rd, $null))
				{ $rd.Dispose(); $rd = $null; }
			if (-not [Object]::ReferenceEquals($cm, $null))
				{ $cm.Dispose(); $cm = $null; }
		}
	}
	catch
	{
		throw;
	}
	finally
	{
		if (-not [Object]::ReferenceEquals($cn, $null))
			{ $cn.Dispose(); }
	}
}

function FormatXml([xml]$xml)
{
	$sw = New-Object System.IO.StringWriter;
	$xw = New-Object System.Xml.XmlTextWriter $sw;
	$xw.Indentation = 1;
	$xw.IndentChar = "`t"
	$xw.Formatting = "indented";
	$xml.WriteTo($xw);
	$xw.Flush();
	$sw.Flush();
	return $sw.ToString()
}

