﻿param
(
	[string]$Server = 'DB1',
	[string]$Database = 'CharityPlanner',
	[securestring]$Password = $null,
	[switch]$WhaiIf
)

$script:Config = @{
	Server = $Server;
	Database = $Database;
	User = 'sa';
	Passwort = $Password;
	WhatIf = $WhaiIf;
	Timeout = 0;
};

<#
.SYNOPSIS
	Configure the sql-lib.  Set Server, Database, User, and Password values;
.PARAMETER configJson
	The configuratio values in JSON format.
#>
function Initialize-Sql
{
	[CmdletBinding(SupportsShouldProcess = $true)]
	param
	(
		[string]$serverOrJson = $null,
		[string]$database = $null,
		[string]$user = $null,
		[securestring]$password = $null,
		$timeout = $null
	)

	process
	{
		# Modify [CmdletBinding()] to [CmdletBinding(SupportsShouldProcess=$true)]
		$paths = @()
		foreach ($aPath in $Path) {
			if (!(Test-Path -LiteralPath $aPath)) {
				$ex = New-Object System.Management.Automation.ItemNotFoundException "Cannot find path '$aPath' because it does not exist."
				$category = [System.Management.Automation.ErrorCategory]::ObjectNotFound
				$errRecord = New-Object System.Management.Automation.ErrorRecord $ex,'PathNotFound',$category,$aPath
				$psCmdlet.WriteError($errRecord)
				continue
			}

			# Resolve any relative paths
			$paths += $psCmdlet.SessionState.Path.GetUnresolvedProviderPathFromPSPath($aPath)
		}

		foreach ($aPath in $paths) {
			if ($pscmdlet.ShouldProcess($aPath, 'Operation')) {
				# Process each path

			}
		}

		if ($serverOrJson.StartsWith('{'))
		{
			$c = ConvertFrom-Json $serverOrJson;
			if ([string]$c.Server -ne '') { $config.Server = [string]$c.Server; }
			if ([string]$c.Database -ne '') { $config.Database = [string]$c.Database; }
			if ([string]$c.User -ne '') { $config.User = [string]$c.User; }
			if ([string]$c.Passwort -ne '') { $config.Passwort = [string]$c.Passwort; }
		}
		else
		{
			if ([string]$serverOrJson -ne '') { $config.Server = [string]$serverOrJson; }
			if ([string]$database -ne '') { $config.Database = [string]$database; }
			if ([string]$user -ne '') { $config.User = [string]$user; }
			if ([string]$passwort -ne '') { $config.Passwort = [string]$passwort; }
			if ($whatIf -is [bool]) { $config.WhatIf = $whatIf; }
			if ($timeout -is [int]) { $config.Timeout = $timeout; }
		}

		if ($config.WhatIf) { Write-Host "What if: Set configuration: server=$($config.Server), database=$($config.Database), user=$($sonfig.User), timeout=$($config.Timeout);" -ForegroundColor Green; }
		$script:Config = $c;
	}
}

<#
.SYNOPSIS
	Executes the SQL statement and returns a single value or a collection of PsObjects or nothing.
.PARAMETER expression
	The SQL statement to run.
.PARAMETER scalar
	Switch indicates than function shuld return a single value (default $false).
.PARAMETER silent
	Switch indicates than function shuld return nothing (default $false).
.PARAMETER noThrow
	Switch indicates than function shuld not throw exceptiond (default $false).
.PARAMETER timeout
	Command timeout value in seconds (default 0 - use system default).
#>
function Invoke-Sql([string]$expression, [string]$database = $null, [switch]$scalar = $false, [swithch]$silent = $false, [switch]$noThrow = $false, [int]$timeout = 0, [switch]$whatIf = $false)
{
	if ($database -eq '') { $database = $script:Config.Database; }
	if ($whatIf -or $script:DC.WhatIf)
	{
		Write-Host "What if: execute sql script: `"$statement`" on $database" -ForegroundColor Green;
		if ($scalar) { return $null; }
		if ($silent) { return; }
		return @();
	}

	$connection = "Server=$($script:Config.Server);Database=$database;"
	if ([string]$script:Config.Password -eq '')
		{ $connection += "Trusted_Connection=yes;"; }
	else
		{ $connection += "User ID=$($script:Config.User);Password=$($script:Config.Password);"; }
	try
	{
		$cn = $null;
		$cn = New-Object Data.SqlClient.SqlConnection($connection);
		$cn.Open()

		$result = New-Object System.Collections.ArrayList
		foreach ($ex in $expression -isplit "(?:\A|`r*`n)\s*go\s*(?:\z|`r*`n)")
		{
			$statement = $ex.Trim();
			if ($statement -eq '' -or $statement -ieq 'go') { continue; }
			try
			{
				$cm = New-Object Data.SqlClient.SqlCommand($statement, $cn)
				$statementTimeout = $timeout;
				if ($statement -match "--\s*\`${\s*CommandTimeout\s*[:=]\s*([0-9]+)\s*}\s*") { $statementTimeout = [int]$Matches[1]; }
				if ($statementTimeout -ne 0)
				{
					if ($statementTimeout -lt 0)
					{
						$cm.CommandTimeout = 360000;
					}
					else
					{
						$cm.CommandTimeout = $statementTimeout;
					}
				}
				if ($scalar) { return $cm.ExecuteScalar(); }
				if ($silent)
				{
					[void]$cm.ExecuteNonQuery();
					return;
				}

				$rd = $cm.ExecuteReader();
				while ($rd.Read())
				{
					$row = @{}
					for ($i = 0; $i -lt $rd.FieldCount; ++$i)
					{
						$name = ([string]$rd.GetName($i), [string]$i -ne '')[0];
						if ($rd.IsDBNull($i))
							{ $row[$name] = $null; }
						else
							{ $row[$name] = $rd.GetValue($i); }
					}
					[void]$result.Add((New-Object PsObject -Property $row));
				}
				return $result;
			}
			catch
			{
				if ($noThrow) { return $null; } else { throw; }
			}
			finally
			{
				if (-not [Object]::ReferenceEquals($rd, $null))
					{ $rd.Dispose(); $rd = $null; }
				if (-not [Object]::ReferenceEquals($cm, $null))
					{ $cm.Dispose(); $cm = $null; }
			}
		}
	}
	catch
	{
		if ($noThrow) { return $null; } else { throw; }
	}
	finally
	{
		if (-not [Object]::ReferenceEquals($cn, $null))
			{ $cn.Dispose(); }
	}
}

function Format-Xml([xml]$xml)
{
	$sw = New-Object System.IO.StringWriter;
	$xw = New-Object System.Xml.XmlTextWriter $sw;
	$xw.Indentation = 1;
	$xw.IndentChar = "`t"
	$xw.Formatting = "indented";
	$xml.WriteTo($xw);
	$xw.Flush();
	$sw.Flush();
	return $sw.ToString()
}
