param
(
	[parameter(Mandatory=$true, ValueFromPipeline=$true)]
	[object[]]$File,
	[string]$Server = $null,
	[string]$Database = $null,
	[string]$Password = $null,
	[switch]$WhaiIf
)
begin
{
	. "$PSScriptRoot\sql-lib.ps1"
	Configure-Sql $Server $Database $Password
}
process
{
	foreach ($item in $File)
	{
		if ($item -is [System.IO.FileInfo]) { $item = $item.FullName };

		$table = [IO.Path]::GetFileNameWithoutExtension($item);
		$tableName = Run-Sql "select object_schema_name(object_id('$table')) + '.' + object_name(object_id('$table'))" -scalar;
		if ($tableName -eq $null) { Write-Error "Cannot find table $table"; continue; }
		if (-not (test-path $item -pathType:leaf))
		{
			$tmp = (([IO.Path]::GetDirectoryName($item), '.') -ne '')[0] + '\' + $tableName + '.xml'
			if (-not (test-path $tmp -pathType:leaf)) { Write-Error "File not found `"$item`""; continue; }
			$item = $tmp
		}
		if ($WhaiIf) { Write-Host "What if: Upload file `"$item`" into table $tableName" -ForegroundColor Green; continue; }
		$item = resolve-path $item;
		$xml = [IO.File]::ReadAllText($item);
		$statement = '
		declare @x xml = ''' + $xml.Replace("'", "''") + ''';
		exec dbo.upload_TableAsXml ''' + $table.Replace("'", "''") + ''', @x;
		';
		$result = script:RunSql $statement -scalar;
	}
}
