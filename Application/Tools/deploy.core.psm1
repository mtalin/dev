﻿$script:Version = "1.4.1";
$script:ApplicationFolder = 'C:\Application';
$script:DC = $null;
<#
DC.Current {
	Package { Name; StartTime; }
	Target { Is; Name; StartTime; }
	ApplicationsQueue @()
	StoppedApplications @()
	InstallApplications @{}
	Log { IndentString; }
}
#>
$global:Error.Clear();
[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.Web.Administration');
if ($global:Error.Count -gt 0) { exit; }

function Publish-Package([string]$source, [scriptblock]$do, [switch]$offline, [switch]$retry, [switch]$simulate = $false, [switch]$trace = $false)
{
	if ([string]$source -ne '') { $source = (resolve-path $source).Path; }
	#if (!$simulate -and !(script:Test-IsAdmin)) { return 'restart-admin'; }
	if (!$simulate -and !(script:Test-IsAdmin)) { throw "Insufficient user privileges for deployment: Try to run the deployment script as an Administrator."; }

	script:InitializeConfiguration $source $source -offline:$offline -retry:$retry -simulate:$simulate -trace:$trace
	if ($global:Error.Count -gt 0) { return 'error'; }

	if ($DC.IsArchive)
	{
		$target = script:Expand-Package $source $script:DC.ExtractDir;
		if ($simulate) { return 'done'; }

		script:InitializeConfiguration $target $source -offline:$offline -retry:$retry -simulate:$simulate -trace:$trace
		if ($global:Error.Count -gt 0) { return 'error'; }
	}

	$package = (ls "$($script:DC.Source)\*.pid" | % { $_.BaseName } | Sort-Object) -join ':'
	if ($package.Length -eq 0) { $package = split-path $script:DC.Source -leaf }
	if ($package.Length -gt 120)
	{
		script:Write-Log 'Package ID has been truncated to 120 characters length.' -error;
		$package = $package.Substring(0, 120);
	}

	$script:DC.Current.Package = @{ Name = $package; StartTime = get-date; }

	try
	{
		script:Write-BeginLog;
		script:Write-Log "Begin of deployment `"$package`" (deploy.core.psm1 version $script:Version; deploy.layout.json version $(if ($script:DC.Version) { $script:DC.Version } else { "-.-" }))"
		script:Trace-Begin
		if ($null -ne $script:DC.ConfigFile)
		{
			script:Write-Log "Getting configuration from `"$($script:DC.ConfigFile)`"";
		}
		$global:Error.Clear();

		if (script:Deploy-Tools -output) { return 'restart'; }

		& $do

		Publish-InstallApplication
		Publish-StartApplication

		script:Trace-End
		script:Write-Log "End of deployment `"$package`"";
		script:Write-Log "Deployment time $(script:Duration $script:DC.Current.Package.StartTime).";

		return 'done';
	}
	catch
	{
		script:Write-ErrorLog

		Publish-StartApplication

		script:Trace-End
		script:Write-Log "Deployment fault `"$package`"";
		script:Write-Log "Deployment time is $(script:Duration $script:DC.Current.Package.StartTime).";

		return 'error';
	}
}


function Publish-Target([string]$target, [scriptblock]$do, [string[]]$test = $null)
{
	$script:DC.Current.Target = @{}
	if ($null -ne $test -and $test.Count -gt 0)
	{
		$t = $test | ? { Publish-Test $_ } | select -first 1;
		if ($null -eq $t) { return; }
	}
	script:Write-Log "Begin of deployment to $target"
	$stamp = script:GetTargetStamp($target);
	if ($stamp.Id -eq 0)
	{
		if ($null -ne $stamp.EndTime)
			{ script:Write-Log "Package $($script:DC.Current.Package.Name) has been already deployed to $target at $($stamp.EndTime) (use -Retry flag to re-deploy)." }
		else
			{ script:Write-Log "Package $($script:DC.Current.Package.Name) is in deployment state to $target (starting at $($stamp.StartTime)) (use -Retry flag to re-deploy)." }
		return;
	}

	try
	{
		$script:DC.Current.Target = @{ Id = $stamp.Id; Name = $target; StartTime = get-date; }

		$global:Error.Clear();
		script:Trace-Begin

		& $do

		script:Trace-End
		script:SetTargetResult $true;
		script:Write-Log "End of deployment to $target ($(script:Duration $script:DC.Current.Target.StartTime))."

		$script:DC.Current.Target = @{}
	}
	catch
	{
		script:Write-ErrorLog
		script:Trace-End
		script:SetTargetResult $false;
		script:Write-Log "Deployment to $target failed ($(script:Duration $script:DC.Current.Package.StartTime)).";
		throw;
	}
}


function Publish-Part([string]$part, [scriptblock]$do)
{
	$app = Publish-Map $part;
	if (Publish-Test $app)
	{
		try
		{
			script:Write-Log "Deploy $($app.ApplicationItem)"
			script:Trace-Begin
			$global:Error.Clear();

			& $do

			Publish-Mark $app
			script:Trace-End
			script:Write-Log "Done $($app.ApplicationItem)"
		}
		catch
		{
			script:Write-ErrorLog
			script:Trace-End
			script:Write-Log "Broken $($app.ApplicationItem)"
			throw;
		}
	}
}


function Publish-Test($item, [switch]$skipDeploymentTest = $false)
{
	$app = Publish-Map $item;
	if (test-path $app.Source -pathType container)
	{
		script:Write-WhatIf "Test that folder `"$($app.Source)`" exists.";
		if (-not (test-path "$($app.Source)\*")) { return $false; }
	}
	else
	{
		script:Write-WhatIf "Test that file `"$($app.Source)`" exists.";
		if (-not (test-path $app.Source -pathType leaf)) { return $false; }
	}
	if ($skipDeploymentTest) { return $true; }
	script:Write-WhatIf "Test that application `"$($app.ApplicationItem)`" was not deployed.";
	return -not (script:TestPartDeployed $app.ApplicationItem);
}


function Publish-Mark($item)
{
	$app = Publish-Map $item;
	script:MarkPartDeployed $app.ApplicationItem
}


function Publish-Map($application)
{
	if ($application.Target) { return $application; }
	$rest = '';
	$app = $null;
	if ($application -is [hashtable])
	{
		$app = $application;
	}
	else
	{
		$app = $script:DC.Application[$application];
		if ($null -eq $app)
		{
			($name, $rest) = ($application -split '\\', 2);
			$app = $script:DC.Application[$name];
			if ($null -eq $app)
			{
				$app = @{ Name = $application; Map = 'App'; AppId = $application; Type = 'Data'; HotUpdate = $true; }
			}
			else
			{
				$app = $app.Clone();
				$app.Name = $application;
				$app.AppId = [System.IO.Path]::Combine($name, $rest);
			}
		}
		elseif ($app.Target)
		{
			return $app;
		}
	}
	$map = $script:DC.Map[$app.Map];

	$s = ([string]$map.Source, [string]$map, '{Name}' -ne '')[0];
	$t = ([string]$map.Target, $s -ne '')[0];
	$subst = {
		param($m)
		return [string]$app[$m.Groups[1].Value];
	};
	$s = [Text.RegularExpressions.Regex]::Replace($s, '\{(.+?)\}', $subst);
	$t = [Text.RegularExpressions.Regex]::Replace($t, '\{(.+?)\}', $subst);
	if ([string]$s -ne '') { $s = [System.IO.Path]::Combine($script:DC.Source, $s); }
	if ([string]$t -ne '') { $t = [System.IO.Path]::Combine($script:DC.Source, $t); }
	$app.ApplicationItem = $app.AppId;
	$app.Source = $s;
	$app.Target = $t;
	$script:DC.Application[$app.Name] = $app;
	return $app;
}


function Publish-Config()
{
	return $script:DC;
}


function Publish-ScriptPart([string]$part)
{
	$app = Publish-Map $part;
	Publish-Part $part `
	{
		if ($script:DC.WhatIf)
		{
			script:Write-WhatIf "Execute script `"$($app.Source)`"";
		}
		else
		{
			$wd = Get-Location;
			cd ([System.IO.Path]::GetDirectoryName($app.Source));
			. $app.Source;
			cd $wd;
		}
	}
}


function Publish-SqlPart([string]$direcoryOrFile, [string]$filter = $null, [string]$database = $null, [int]$timeout = 0, [switch]$output = $false)
{
	if ($database -eq '') { $database = $script:DC.Database.Database; }

	$app = Publish-Map $direcoryOrFile;
	if (-not (test-path $app.Source -filter:$filter)) { if ($output) { return $false; } else { return; } }
	try
	{
		$global:Error.Clear();
		$result = $false;
		foreach($file in ls $app.Source -filter:$filter -file | sort)
		{
			$fileName = $file.FullName;
			$item = $fileName.Substring($script:DC.Source.Length + 1);
			if (Publish-Test $item)
			{
				$result = $true;
				Publish-SqlFile -file:$fileName -database:$database -timeout:$timeout
				Publish-Mark $item
			}
		}
		if ($output) { return $result; }
	}
	catch
	{
		script:Write-ErrorLog
		throw;
	}
}


function Publish-XmlTablePart([string]$file, [string]$database = $null, [switch]$output = $false)
{
	if (-not (Publish-Test $file)) { if ($output) { return $false; } else { return; } }
	try
	{
		$global:Error.Clear();
		$result = (Publish-XmlTable $file $database -output);
		Publish-Mark $file;
		if ($output) { return $result; }
	}
	catch
	{
		script:Write-ErrorLog
		throw;
	}
}


function Publish-Sql([string]$expression, [string]$database = $null, [int]$timeout = 0, [string]$mark = $null, [switch]$output = $false)
{
	$result = @();
	if ([string]$mark -ne '') { if (-not (Publish-Test $mark)) { if ($output) { return $result; } else { return; } } }
	if ([string]$database -eq '') { $database = $script:DC.Database.Database; }
	$text = ($expression -replace '\s+', ' ').Trim();
	if ($text.Length -gt 255) { $text = $text.Substring(0, 252) + "..."; }
	script:Write-Log "Run SQL ($database): $text"
	$result = (Sql $expression $database -output:$output -timeout:$timeout)
	if ([string]$mark -ne '') { Publish-Mark $mark }
	if ($output) { return $result; }
}


function Publish-SqlFile([string]$file, [string]$database = $null, [int]$timeout = 0, [switch]$output = $false)
{
	if ($database -eq '') { $database = $script:DC.Database.Database; }
	script:Write-Log "Run SQL script: $([System.IO.Path]::GetFileName($file)) on $database";
	script:Trace-Begin
	$global:Error.Clear();
	if ($script:DC.WhatIf)
		{ script:Write-WhatIf "Execute SQL script from `"$file`" on `"$database`"" }
	else
		{ script:RunSql ([System.IO.File]::ReadAllText($file)) $database -timeout:$timeout }
		script:Trace-End
	if ($output) { return $true; }
}


function Publish-XmlTable([string]$file, [string]$table = $null, [string]$database = $null, [switch]$output = $false)
{
	$app = Publish-Map $file;
	if (-not (test-path $app.Source -pathType:leaf)) { if ($output) { return $false; } else { return; } }
	if ([string]$database -eq '') { $database = $script:DC.Database.Database; }
	if ([string]$table -eq '') { $table = [System.IO.Path]::GetFileNameWithoutExtension($app.Source); }
	if ($table.IndexOf(".") -lt 0) { $Table = "dbo.$table" }
	script:Write-Log "Upload table $table from $([System.IO.Path]::GetFileName($app.Source))";
	script:Trace-Begin
	script:Import-XmlTable -file:$app.Source -table:$table -database:$database
	script:Trace-End
	if ($output) { return $true; }
}


function Publish-Application
{
	foreach ($item in @($script:DC.Application.Values))
	{
		$app = Publish-Map $item;
		if (Publish-Test $app) { Publish-StopApplication $app; }
	}
	foreach ($item in @($script:DC.Application.Values))
	{
		if ($item.Type -ne 'database') { script:Deploy $item; }
	}
}


function Publish-StopApplication($applications, [switch]$force = $false, [switch]$output = $false)
{
	if ($null -eq $applications)
	{
		$applications = [string[]]$script:DC.Application.Keys;
	}
	$count = 0;
	if ($script:DC.Trace) {
		script:Write-Trace "Enter StopApplication $(($applications |% { (Publish-Map $_).Name }) -join ', ') -force:$force -output:$output";
	}
	foreach ($item in $applications)
	{
		try
		{
			$app = Publish-Map $item;
			if ($script:DC.Current.StoppedApplications.ContainsKey($app.Name)) { continue; }

			script:Write-Trace "Test that $($app.Name) is running"
			$test = script:Test-Application($app);
			if (-not $test.IsRunning) { continue; }
			if ($app.HotUpdate -and (-not $force)) { continue; }

			$dependents = $script:DC.Application.Values | ? { $_.Uses -contains $app.Name } | % { $_.Name };
			if ($dependents.Count -gt 0) {
				script:Write-Trace "Test that depended applications are running"
				$count += (Publish-StopApplication $dependents -force:$force -output);
			}
			if ($app.IsResource) { continue; }

			if ($null -eq $script:DC.Current.ApplicationsQueue) { $script:DC.Current.ApplicationsQueue = new-object Collections.ArrayList; }
			[void]$script:DC.Current.ApplicationsQueue.Add($app.Name)
			script:Write-Log "Stop application $($app.Name)"

			$script:DC.Current.StoppedApplications[$app.Name] = $test.Data;
			script:Stop-Application $app
			++$count;
		}
		catch
		{
			script:Write-ErrorLog
		}
	}
	if ($script:DC.Trace) {
		script:Write-Trace "Exit StopApplication $(($applications |% { (Publish-Map $_).Name }) -join ', ') -force:$force -output:$output";
	}
	if ($output) { return $count; }
}

function Publish-StartApplication()
{
	if ($null -eq $script:DC.Current.ApplicationsQueue) { return }
	$script:DC.Current.ApplicationsQueue.Reverse();
	foreach($appName in $script:DC.Current.ApplicationsQueue)
	{
		script:Write-Log "Start application $appName"
		script:Start-Application (Publish-Map $appName);
	}
	$script:DC.Current.ApplicationsQueue = $null;
}


function Publish-InstallApplication()
{
	$hasError = $false;
	foreach ($a in $script:DC.Current.InstallApplications.Values)
	{
		if ($script:DC.WhatIf) { script:Write-WhatIf "Install application $($a.Name)" }
		$ss = ls $a.Target -ea:Ignore | ? { 'install.bat', 'install.cmd', 'install.ps1' -contains $_.Name }
		if ($ss.Count -eq 0) { continue; }
		$s = $ss | ? { $_.Extension -eq '.bat'}
		if ($null -eq $s) { $s = $ss | ? { $_.Extension -eq '.cmd'} }
		if ($null -eq $s) { $s = $ss[0] }

		script:Write-Log "Installing $($a.Name)."
		if ($script:DC.WhatIf)
		{
			script:Write-WhatIf "Execute $($s.FullPath)."
		}
		else
		{
			try
			{
				& $s.FullName;
			}
			catch
			{
				script:Write-Log "Installation failed."
				$hasError = $true;
			}
		}
	}
	if ($hasError) { throw "Installation failed." }
}


function Invoke-RequireAdmin([System.Management.Automation.InvocationInfo]$invocation)
{
    if (-not (script:Test-IsAdmin))
    {
        # Get the script path
        $scriptPath = $invocation.MyCommand.Path
        $scriptPath = script:Get-UNCFromPath -Path $scriptPath

        # Need to quote the paths in case of spaces
        $scriptPath = '"' + $scriptPath + '"'

        # Build base arguments for powershell.exe
        [string[]]$argsList = @('-NoLogo -NoProfile', '-ExecutionPolicy Bypass', '-File', $scriptPath)

        # Add
        $argsList += $invocation.BoundParameters.GetEnumerator() | ForEach-Object {"-$($_.Key)", "$($_.Value)"}
        $argsList += $invocation.UnboundArguments

        try
		{
            $process = Start-Process PowerShell.exe -PassThru -Verb Runas -Wait -WorkingDirectory $pwd -ArgumentList $argsList
            exit $process.ExitCode;
        }
        catch { exit 4; }

        # Generic failure code
        exit 1;
    }
}


function script:Get-UNCFromPath([string]$path)
{
    if ($Path.Contains([System.IO.Path]::VolumeSeparatorChar)) {
        $psdrive = Get-PSDrive -Name $Path.Substring(0, 1) -PSProvider 'FileSystem'

        # Is it a mapped drive?
        if ($psdrive.DisplayRoot) { $Path = $Path.Replace($psdrive.Name + [System.IO.Path]::VolumeSeparatorChar, $psdrive.DisplayRoot) }
    }
    return $path;
}


function script:Deploy($application, [switch]$output = $false)
{
	$app = Publish-Map $application;
	if (-not (Publish-Test $app)) { if ($output) { return $false; } else { return; } }
	$global:Error.Clear();

	$stopped = 0;
	try
	{
		script:Write-Log "Deploy $($app.Name) of type '$($app.Type)' from $($app.Source) to $($app.Target)"
		$stopped = (Publish-StopApplication $app -output);
		if (-not (test-path $app.Target))
		{
			$script:DC.Current.InstallApplications[$app.AppId] = $app
			if ($script:DC.WhatIf) { script:Write-WhatIf "Include `"$($app.AppId)`" into pending install." }
		}
		script:Cpy $app.Source $app.Target -exclude:$app.Exclude -append:($app.Type -eq 'data')
		Publish-Mark $app.Name
		if ($output) { return $true; }
		return;
	}
	catch
	{
		script:Write-ErrorLog
		if ($stopped -gt 0) { throw; }
		if ((Publish-StopApplication $app -output -force) -eq 0) { throw; }
	}

	try
	{
		script:Write-Log "Second try ..."
		script:Cpy $app.Source $app.Target -exclude:$app.Exclude -append:($app.Type -eq 'data')
		Publish-Mark $app.Name
		if ($output) { return $true; }
		return;
	}
	catch
	{
		script:Write-ErrorLog
		throw "Second try failed";
	}
}


function script:Deploy-Tools([switch]$output = $false)
{
	$app = Publish-Map 'tools';
	if (!(Publish-Test $app -skipDeploymentTest)) { if ($output) { return $false; } return; }
	$global:Error.Clear();

	try
	{
		script:Write-Log "Deploy $($app.Name) of type '$($app.Type)' from $($app.Source) to $($app.Target)"
		$act = script:Cpy $app.Source $app.Target -exclude:$app.Exclude -append:($app.Type -eq 'data') -output
		Publish-Mark $app.Name
		if ($output) { return $act -gt 0; }
		return;
	}
	catch
	{
		script:Write-ErrorLog
		throw "Cannot deploy Tools";
	}
}


function script:Test-Application([hashtable]$app)
{
	script:Write-WhatIf "Check whether application `"$($app.Name)`" ($($app.Type)) is running";
	if ($app.Test -is [scriptblock])
	{
		script:Write-Trace "Test that app `"$($app.Name)`" is running using custom script";
		$running = &($app.Test);
		script:Write-Trace "Result: '$running'";
		if ($running -is [bool]) { return script:AppStatus -running:$running; }
	}
	if ($app.Type -eq 'IisApplication')
	{
		script:Write-Trace "Test that IIS application `"$($app.Name)`" is running";
		$data = script:GetIisApplication $app 'Started';
		script:Write-Trace "Result: '$($null -ne $data)'";
		return script:AppStatus $data -running:($null -ne $data);
	}
	if ($app.Type -eq 'ScheduledTask')
	{
		$p = script:SplitTaskPath $app.AppId;
		script:Write-Trace "Test that scheduled task '$($p.Name)' at '$($p.Path)' is active";
		$tt = Get-ScheduledTask $p.Name $p.Path -ea:ignore;
		if ($null -eq $tt -or $tt.Count -eq 0) {
			script:Write-Log "Scheduled task '$($p.Name)' under '$($p.Path)' is not configured"
			return script:AppStatus -running:$false;
		}
		script:Write-Trace "found task{s}: $($tt -join '; ')";
		if ($tt.Count -gt 1) {
			script:Write-Error "More than one scheduled tasks '$($p.Name)' found under '$($p.Path)'"
		}
		$task = $tt[0];
		script:Write-Trace "Result: '$($null -ne $task)', task.State='$($task.State)'";
		if ($null -eq $task -or $task.State -eq 'Disabled') {
			return script:AppStatus -running:$false;
		}
		return script:AppStatus $(if ($app.Terminate -eq $false) { '' } else { $task.State; }) -running;
	}
	if ($app.Type -eq 'Application')
	{
		script:Write-Trace "Test that application at '$($app.Target)' is running";
		$pp = Get-Process | ? { $null -ne $_.Path -and $_.Path.StartsWith($app.Target, 1) };
		script:Write-Trace "Resut: '$($pp.Count -gt 0)', Data='$((($pp |% { $_.Path }) -join '; '))'";
		if ($null -eq $pp -or $app.Count -eq 0) {
			return script:AppStatus -running:$false;
		}
		return script:AppStatus ($pp |% { $_.Path }) -running:$false;
	}
	if ($app.Type -eq 'Service')
	{
		script:Write-Trace "Test that service '$($app.AppId)' is running";
		$p = Get-Service $app.AppId -ea:ignore;
		if ($null -eq $p) {
			script:Write-Log "Service '$($app.AppId)' is not installed"
			return script:AppStatus -notInstalled;
		}
		$i = 0;
		while ($p.Status -like '*Pending')
		{
			if ($i -ge $script:DC.PingTimeout * 10) { script:Write-Log "Waiting while $($app.Name) is in $($p.Status) status"; $i = 0; }
			++$i;
			[Threading.Thread]::Sleep(100);
			$p = Get-Service $app.AppId -ea:ignore;
		}
		script:Write-Trace "Result: '$($p.Status -eq 'Running')'";
		return script:AppStatus -running:($p.Status -eq 'Running');
	}
	if ($app.IsResource)
	{
		return script:AppStatus -running;
	}
	throw "Cannot determine if application $($app.Name) is running.";
}

function script:AppStatus($data = $null, [switch]$running = $false, [switch]$notInstalled = $false) {
	return @{IsRunning = $running; IsInstalled = ($running -or !$notInstalled); Data = $data};
}

function script:Stop-Application([hashtable]$app)
{
	if ($script:DC.WhatIf) { script:Write-WhatIf "Stop application `"$($app.Name)`" ($($app.Type))"; return; }

	script:Stop-Instance $app;

	$i = $script:DC.PingTimeout * 10;
	while ((script:Test-Application $app).Running)
	{
		if ($i -ge $script:DC.PingTimeout * 10) { script:Write-Log 'Waiting for stopping...'; $i = 0; }
		++$i;
		[Threading.Thread]::Sleep(100);
	}
}

function script:Stop-Instance([hashtable]$app)
{
	if ($app.IsResource) { return; }
	if ($app.Type -eq 'IisApplication')
	{
		script:Write-Trace "Stop IIS application pool for '$($app.Name)'";
		$data = $script:DC.Current.StoppedApplications[$app.Name];
		$data.Site | % { [void]$_.Stop() };
		if ($app.Stop -is [scriptblock])
		{
			script:Write-Trace "Exevute script block to stop application '$($app.Name)'";
			if (& ($app.Stop)) { return; }
		}
		$data.Pool | % { [void]$_.Stop() };
		return;
	}
	if ($app.Type -eq 'ScheduledTask')
	{
		$p = script:SplitTaskPath $app.AppId;
		script:Write-Trace "Disable scheduled task '$($p.Name)' at '$($p.Path)'";
		Disable-ScheduledTask $p.Name $p.Path -errorAction:ignore >$null
		try
		{
			if ($app.Stop -is [scriptblock])
			{
				script:Write-Trace "Execute script block to stop application '$($app.Name)'";
				if (& ($app.Stop)) { return; }
			}

			$t = Get-ScheduledTask $p.Name $p.Path -errorAction:ignore
			$ss = ($t.Actions | % {$_.Execute});
			if ($ss.Count -gt 0)
			{
				$processes = Get-Process | ? { $ss -contains $_.Path };
				if ($processes.Count -gt 0)
				{
					script:Write-Trace "Stop processes at '$($ss -join '; ')'";
					if ($app.Terminate -eq $false) { script:Write-Log "Manually stop the application `"$($app.Name)`" or wait for it to close."; return; }
					$app.Processes = ($processes | % { $_.Path });
					script:Stop-Process $app.Name, $processes
				}
			}
			script:Write-Trace "Stop scheduled task '$($p.Name)' at '$($p.Path)'";
			Stop-ScheduledTask $p.Name $p.Path -errorAction:ignore >$null
		}
		catch
		{
			script:Write-Trace "In catch block: enable scheduled task '$($p.Name)' at '$($p.Path)'";
			Enable-ScheduledTask $p.Name $p.Path -errorAction:ignore >$null
			throw;
		}
		return;
	}
	if ($app.Stop -is [scriptblock])
	{
		script:Write-Trace "Execute script block to stop application '$($app.Name)'";
		& ($app.Stop);
		return;
	}
	if ($app.Type -eq 'Application')
	{
		script:Write-Trace "Stop all processes at '$($app.Target)'";
		$processes = (Get-Process | ? { $null -ne $_.Path -and $_.Path.StartsWith($app.Target, 1) });
		if ($processes.Count -gt 0)
		{
			if ($app.Terminate -eq $false) { script:Write-Log "Manually stop the application `"$($app.Name)`" or wait for it to close."; return; }
		}
		$app.Processes = ($processes | % { $_.Path });
		script:Stop-Process $app.Name, $processes;
		return;
	}
	if ($app.Type -eq 'Service')
	{
		script:Write-Trace "Stop service '$($app.AppId)'";
		$p = Get-Service $app.AppId -ea:ignore;
		$i = 0;
		while ($p.Status -like '*Pending')
		{
			if ($i -ge $script:DC.PingTimeout * 10) { script:Write-Log "Waiting while $($app.Name) is in $($p.Status) status"; $i = 0; }
			++$i;
			[Threading.Thread]::Sleep(100);
			$p = Get-Service $app.AppId -ea:ignore;
		}
		if ($p.Status -eq 'Running') { $p.Stop(); }
		return;
	}
	throw "Cannot determine how to stop application $($app.Name) of type '$($app.Type)'.";
}

function script:Stop-Process([string]$appName, $processes)
{
	for (;;)
	{
		if ($processes.Count -eq 0) { return }
		if ($processes.Count -eq 1)
		{
			script:Write-Log "The application $($p.Path) is running."
		}
		else
		{
			script:Write-Log "The following applications are running:"
			foreach ($p in $processes)
			{
				script:Write-Log "$($p.Path)"
			}
		}
		$ynr = Read-Host "Kill the application$(@('', 's')[$processes.Count -gt 1])? (Yes/No/Refresh)"
		if ($ynr[0] -ieq 'Y')
		{
			foreach ($p in $processes)
			{
				if (-not $p.HasExited)
				{
					script:Write-Log "Closing $($p.Name)"
					if (-not $p.CloseMainWindow()) { $p.Kill(); }
				}
			}
			return;
		}
		if ($ynr[0] -ieq 'N')
		{
			throw "Cannot stop proces(s) of application $appName.";
		}
	}
}

function script:Start-Application([hashtable]$app)
{
	try
	{
		if ($script:DC.WhatIf) { script:Write-WhatIf "Start application `"$($app.Name)`" ($($app.Type))"; return; }
		if ($app.IsResource) { script:Write-Trace "Start resource '$($app.Name)'"; return; }
		$data = $script:DC.Current.StoppedApplications[$app.Name];
		if ($app.Type -eq 'IisApplication')
		{
			script:Write-Trace "Start IIS applications '$($app.AppId)'";
			$data.Site | % { [void]$_.Start() };
			$data.Pool | % { [void]$_.Start() };
			if ($app.Start -is [scriptblock]) { & ($app.Start); }
			return;
		}
		if ($app.Type -eq 'ScheduledTask')
		{
			$p = script:SplitTaskPath $app.AppId;
			script:Write-Trace "Start scheduled task '$($p.Name)' at '$($p.Path)'";
			$t = Get-ScheduledTask $p.Name $p.Path -errorAction:ignore
			if ($t.State -ne "Disabled")
			{
				script:Write-Log "$($app.Name) was not stopped."
				return;
			}
			script:Write-Trace "Enable scheduled task '$($p.Name)' at '$($p.Path)'";
			Enable-ScheduledTask $p.Name $p.Path -errorAction:ignore >$null
			if ($app.Start -is [scriptblock])
			{
				if (& ($app.Start)) { return; }
			}
			if ($data -eq 'Running') {
				script:Write-Trace "Run scheduled task '$($p.Name)' at '$($p.Path)'";
				Start-ScheduledTask $p.Name $p.Path -errorAction:ignore >$null
			}
			return;
		}
		if ($app.Start -is [scriptblock])
		{
			script:Write-Trace "Start '$($app.Name)' using custom script";
			& ($app.Start);
			return;
		}
		if ($app.Type -eq 'Application')
		{
			script:Write-Log "$($app.Name) cannot be started automatically."
			return;
		}
		if ($app.Type -eq 'Service')
		{
			script:Write-Trace "Find service '$($app.AppId)'";
			$p = Get-Service $app.AppId -ea:ignore;
			$i = 0;
			while ($p.Status -like '*Pending')
			{
				if ($i -ge $script:DC.PingTimeout * 10) { script:Write-Log "Waiting while $($app.Name) is in $($p.Status) status"; $i = 0; }
				++$i;
				[Threading.Thread]::Sleep(100);
			}
			if ($p.Status -eq 'Stopped') {
				script:Write-Trace "Start service '$($app.AppId)'";
				$p.Start();
			}
			return;
		}
		throw "Cannot determine how to start application $($app.Name) of type '$($app.Type)'.";
	}
	catch
	{
		script:Write-ErrorLog
	}
}

function script:SplitTaskPath([string]$taskName)
{
	$taskPath = '\';
	$taskName = $taskName.Replace('/', '\');
	$i = $taskName.LastIndexOf('\');
	if ($i -ge 0)
	{
		$taskPath = '\' + $taskName.Substring(0, $i+ 1).Trim('\') + '\';
		$taskName = $taskName.Substring($i + 1);
	}
	return @{ Name = $taskName; Path = $taskPath; };
}

function script:GetIisApplication([hashtable]$app, [string]$state)
{
	$iis = [Microsoft.Web.Administration.ServerManager]::OpenRemote((Get-Content env:computername).ToLower())
	$ss = @();
	$aa = @{};
	foreach ($s in $iis.Sites)
	{
		$a = $s.Applications | ? { ($_.VirtualDirectories | % { $_.PhysicalPath }) -icontains  $app.Target };
		if ($a.Count -gt 0)
		{
			$ss += $s;
			$a | % { $aa[$_.ApplicationPoolName] = $true; };
		}
	}

	script:Write-Trace "$($ss.Count) site(s) were found for $($app.Target).";

	if ($ss.Count -eq 0) { return $null; }
	$pp = ($iis.ApplicationPools | ? { $aa[$_.Name] })

	$i = 0;
	while ($null -ne ($a = ($ss | ? { 'Starting', 'Stopping' -contains $_.State } | select -First 1)))
	{
		if ($i -ge $script:DC.PingTimeout * 10) { script:Write-Log "Waiting while site $($a.Name) is $($a.State)"; $i = 0; }
		[Threading.Thread]::Sleep(100);
		++$i;
	}
	while ($null -ne ($a = ($pp | ? { 'Starting', 'Stopping' -contains $_.State } | select -First 1)))
	{
		if ($i -ge $script:DC.PingTimeout * 10) { script:Write-Log "Waiting while application pool $($a.Name) is $($a.State)"; $i = 0; }
		[Threading.Thread]::Sleep(100);
		++$i;
	}
	script:Write-Trace "$($pp.Count) application pool(s) were found for sites.";

#	$site = $ss | ? { $_.State -eq $state };
	$pool = $pp | ? { $_.State -eq $state };
#	if ($site.Count -eq 0 -and $pool.Count -eq 0) { return $null; }
#	return @{ Site = $site; Pool = $pool; }
	script:Write-Trace "$($pool.Count) application pool(s) in $state state.";
	if ($pool.Count -eq 0) { return $null; }
	return @{ Site = @(); Pool = $pool; }
}

#region Utilities

$script:LastErrorMessage;
$script:Utf8Encoding;
$script:NewLine = if ($null -ne [System.Environment]::NewLine) { [System.Environment]::NewLine } else { "`n" }

function script:Write-Log([string]$text, [switch]$isError = $false, [switch]$trace = $false)
{
	if ($trace -and !$isError -and !$script:DC.Trace) { return; }
	if ($isError)
	{
		if ($script:LastErrorMessage -ceq $text) { return; }
		$script:LastErrorMessage = $text;
	}
	if ($false -eq $script:DC.WhatIf)
	{
		if ($null -eq $script:Utf8Encoding) { $script:Utf8Encoding = New-Object System.Text.UTF8Encoding $false }
		$time = (get-date).ToString('yyyy-MM-dd hh:mm:ss.fffff')
		$lines = $text -split "`r?`n|`r";
		$p = if ($isError) { 'ERROR: '; } elseif ($trace) { 'TRACE: '} else { '' };
		foreach ($line in $lines)
		{
			$script:DC.Log.File |? { [string]$_ -ne '' } |% { [System.IO.File]::AppendAllText($_, "$time $($script:DC.Log.IndentString)$p$line$script:NewLine", $script:Utf8Encoding); }
		}
	}
	script:Write-Console $text -red:$isError -trace:$trace
}

function script:Write-Trace([string]$text)
{
	script:Write-Log $text -trace
}

function script:Write-BeginLog()
{
	if ($script:DC.WhatIf) { return; }
	if ($null -eq $script:Utf8Encoding) { $script:Utf8Encoding = New-Object System.Text.UTF8Encoding $false }
	$script:DC.Log.File |? { [string]$_ -ne '' -and (test-path $_ -pathType leaf) } |% { [System.IO.File]::AppendAllText($_, "************************* **************************************************$script:NewLine", $script:Utf8Encoding); }
}

function script:Write-Console([string]$text, [switch]$red = $false, [switch]$trace = $false)
{
	$lines = $text -split "`r?`n|`r";
	$w = ([int]$Host.UI.RawUI.BufferSize.Width, 120 -ne 0)[0];
	$lines | % `
	{
		$t = $script:DC.Log.IndentString + $_;
		if ($red)
		{
			script:Write-Host_ $t Red -NoNewline:($t.Length -eq $w)
		}
		elseif ($trace)
		{
			script:Write-Host_ $t Green -NoNewline:($t.Length -eq $w)
		}
		else
		{
			if ($t.Length -gt $w) { $t = $t.Substring(0, $w - 3) + "..."; }
			script:Write-Host_ $t -NoNewline:($t.Length -eq $w)
		}
	}
}


function script:Write-ErrorLog()
{
	foreach ($item in $global:Error)
	{
		script:Write-Log $item.ToString() -error
	}
	$global:Error.Clear();
}

function script:Write-WhatIf([string]$text, [switch]$test = $false)
{
	if ($script:DC.WhatIf) { script:Write-WhatIf_ $text; }
	if ($test) { return $script:DC.WhatIf; }
}

function script:Write-WhatIf_([string]$text)
{
	script:Write-Host_ "What if: $text" Green;
}

function script:Write-Host_([string]$text, [string]$color = $null, [switch]$noNewLine)
{
	if ([string]$color -eq '') { $color = $Host.UI.RawUI.ForegroundColor; }
	Write-Host $text -ForegroundColor $color -NoNewline:$noNewLine;
}

function script:Trace-Begin()
{
	++$script:DC.Log.IndentLevel;
	$script:DC.Log.IndentString = "  " * $script:DC.Log.IndentLevel;
}

function script:Trace-End()
{
	if ($script:DC.Log.IndentLevel -gt 0)
	{
		--$script:DC.Log.IndentLevel;
		$script:DC.Log.IndentString = "  " * $script:DC.Log.IndentLevel;
	}
}


function script:ConvertToHashTable($obj, [switch]$required)
{
    if ($null -eq $obj -and $required) { return @{}; }
    if (-not ($obj -is [PSCustomObject])) { return $obj; }
    $hash = @{};
    $obj.PSObject.Properties | % { $hash[(script:NameKey $_.Name)] = script:ConvertToHashTable $_.Value };
    return $hash;
}

function script:NameKey([string]$value) {
	return ($value -split '-|_' |% { if ($_.Length -eq 0) {''} else {[Char]::ToUpperInvariant($_[0]) + $_.Substring(1)} }) -join '';
	# -replace '[-_](.)', { $_.Groups[1].Value.ToUpperInvariant() };
	# return $name.Substring(0, 1).ToUpperInvariant() + $name.Substring(1);
}

function script:JoinHashTable($obj, $add)
{
	foreach ($i in $add.Keys)
	{
        if (!$obj.ContainsKey($i))
        {
            $obj[$i] = $add[$i];
        }
        elseif ($obj[$i] -is [hashtable])
        {
			if (!($add[$i] -is [hashtable])) { throw "Cannot convert Value to Map" }
            script:JoinHashTable $obj[$i] $add[$i]
        }
		else
		{
			if ($add[$i] -is [hashtable]) { throw "Cannot convert Map to Value" }
            $obj[$i] = $add[$i];
		}
	}
}


function script:Test-IsAdmin()
{
	if (([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) { return $true; }
	return $false;
}


function script:ReadConfig([string]$config)
{
	$content = [string]::Join("`n", (Get-Content $config));
	return script:ConvertToHashTable (ConvertFrom-Json $content) -required
}


function script:InitializeConfiguration([string]$source, [string]$original, [switch]$offline = $false, [switch]$retry = $false, [switch]$simulate = $false, [switch]$trace = $false)
{
	<#
	Log file:
		Always in the deployment dir
		0. deploy\Main.20190101\deploy.log
		Extra log in case of Archive:
		1. deploy\Main.20190101.deploy.log *
		2. deploy\Main.20190101.7z.log
		3. deploy\Main.20190101.log
		4. deploy\Main.20190101.7z.deploy.log
		5. deploy\deploy.log
	#>
	$global:Error.Clear();

	$source = $source.Trim();
	if ($source -eq '') { $source = (Get-Location).Path; }
	$original = $original.Trim();
	if ($original -eq '') { $original = (Get-Location).Path; }
	$p = (convert-path $source -ea silent).TrimEnd('\');
	if ($null -eq $p) { throw "Cannot find deployment source: `"$source`""; }
	$source = $p;
	if (script:IsSpecialDirectory($source)) { throw "Cannot deploy from system folder ($source)." }
	if ($simulate) { script:Write-WhatIf_ "Use deployment source: `"$source`""; }
	if (-not (test-path $source)) { throw "Cannot find deployment source: `"$source`""; }

	$layoutFile = ($source, "$source\Tools", $PSScriptRoot, "$script:ApplicationFolder\Tools") |% { join-path $_ 'deploy.layout.json' } | ? { test-path $_ -pathType:leaf } | select -first 1;
	if ($simulate) { script:Write-WhatIf_ "Use layout file: `"$layoutFile`""; }
	if ($null -eq $layoutFile) { throw 'Cannot find deployment layout file'; }
	$config = script:ReadConfig $layoutFile

	if (-not ($config.Map -is [hashtable])) { $config.Map = @{}; }
	if (-not ($config.Map.App -is [hashtable])) { $config.Map.App = @{}; }
	if (-not ($config.Log -is [hashtable])) { $config.Log = @{} }
	if (-not ($config.Server -is [hashtable])) { $config.Server = @{} }
	if ($null -eq $config.Server.Name) { $config.Server.Name = $env:COMPUTERNAME }

	$configFile = "$source;$source\Config;$script:ApplicationFolder\Config;$PSScriptRoot;$script:ApplicationFolder\Tools" -split ';' | % { join-path $_ 'deploy.config.json' } | ? { test-path $_ -pathType:leaf } | select -first 1;
	if ($simulate) { script:Write-WhatIf_ "Use config file: `"$configFile`""; }
	if ($null -eq $configFile)
	{
		Write-Warning 'Cannot find deployment configuration.  Default configuration is used.';
	}
	else
	{
		$local = script:ReadConfig $configFile;
		script:JoinHashTable $config $local
	}

	$isArchive = test-path $source -PathType Leaf;
	if ($simulate -and $isArchive) { script:Write-WhatIf_ "Assumes that the source is an archive"; }

	$config.ConfigFile = $configFile;
	$config.Source = $source;
	$config.Online = (-not [bool]$offline);
	$config.Retry = [bool]$retry;
	$config.PingTimeout = 3;
	$config.WhatIf = [bool]$simulate;
	$config.Log.IndentLevel = 0;
	$config.Log.IndentString = "";
	$config.IsArchive = $isArchive;
	$config.Trace = $trace;

	if ($null -eq $config.Map) { throw "Required configuration section `"Map`" is missing" }
	if ($null -eq $config.Map.App) { throw "Required configuration section `"Map.App`" is missing" }
	if ($null -eq $config.Map.App.Source) { throw "Required configuration value `"Map.App.Source`" is missing"; }
	if ($null -eq $config.Map.App.Target) { throw "Required configuration value `"Map.App.Target`" is missing"; }
	if ($null -eq $config.Database) { throw "Required configuration section `"Database`" is missing" }
	if ($null -eq $config.Database.Server) { throw "Required configuration section `"Database.Server`" is missing" }

	if (test-path $source -pathType container -ea silent) {
		# 1. Main.20190101\deploy.log
		$config.Log.File = join-path (resolve-path $source) "deploy.log"
	}
	if (test-path $original -pathType leaf -ea silent) {
		# 1. deploy\Main.20190101.deploy.log
		$config.Log.File = $config.Log.File, [System.IO.Path]::ChangeExtension((resolve-path $original), ".deploy.log");
	}

	if ($null -eq $config.ExtractDir) { $config.ExtractDir = '@Extracted' }
	if ($null -eq $config.Database.Database) { $config.Database.Database = 'master' }
	if ($null -eq $config.Database.Sspi) { $config.Database.Sspi = ([string]$config.Database.Login -eq '' -or [string]$config.Database.Password -eq '') }
	if ([int]$config.Database.Timeout -le 0) { $config.Database.Timeout = 360000 }

	$config.Current = @{}
	$config.Current.StoppedApplications = @{};
	$config.Current.InstallApplications = @{};
	foreach ($name in @($config.Application.Keys))
	{
		$app = $config.Application[$name];
		$app.Remove('Target');
		$app.Name = $name;
		if ([string]$app.AppId -eq '') { $app.AppId = $name; }
		$app.Type = if ([string]$app.Type -eq '') { 'Data' } else { script:NameKey $app.Type }
		if ($null -eq $app.IsResource) { $app.IsResource = ('Resources', 'Data', 'Database' -contains $app.Type) }
		if ([string]$app.Map -eq '') { $app.Map = 'App'; }
		if ($null -eq $config.Map[$app.Map]) { throw "Cannot find layout map `"$($app.Map)`" for application `"$name`"" }
		if ($null -eq $app.HotUpdate) { $app.HotUpdate = $true; }
	}

	if ($null -eq $config.Tools) { $config.Tools = (([string]$config.Map.Tools.Target -replace '{Name}', 'Tools'), "$script:ApplicationFolder\Tools" -ne '' | select -first 1); }

	$script:DC = $config;
	if ($config.Online -and -not $isArchive) { script:CreateDeploymentProcedures }
}

function script:FixPath([string]$current, [string]$path, [string]$default)
{
	if ([string]$path -eq '') { $path = $default; }
	return [System.IO.Path]::Combine($current, $path);
}

function script:IsSpecialDirectory([string]$path)
{
	$specials = @(
		'CommonApplicationData',
		'Windows',
		'System',
		'ProgramFiles',
		'ProgramFilesX86'
		);
	$path = $path.Replace('/', '\');
	if (-not $path.EndsWith('\')) { $path += '\' }
	foreach ($s in $specials)
	{
		$t = [Environment]::GetFolderPath($s).Replace('/', '\');
		if (-not $t.EndsWith('\')) { $t += '\' }
		if ($path.StartsWith($t, [StringComparison]::OrdinalIgnoreCase)) { return $true; }
	}
	return $false;
}


function script:Sql([string]$expression, [string]$database = $null, [switch]$output = $false, [int]$timeout = 0)
{
	$global:Error.Clear();
	if ([string]$database -eq '') { $database = $script:DC.Database.Database; }
	if ($script:DC.WhatIf)
	{
		script:Write-WhatIf "Execute sql script: `"$expression`" on $database";
		if ($output) { return @(); } return;
	}
	script:RunSql $expression $database -output:$output -timeout:$timeout
}


function script:RunSql([string]$expression, [string]$database, [switch]$output = $false, [int]$timeout = 0)
{
	if ([string]$database -eq '') { $database = $script:DC.Database.Database; }
	$connection = "Server=$($script:DC.Database.Server);Database=$database;"
	if ($script:DC.Database.Sspi)
		{ $connection += "Trusted_Connection=yes;"; }
	else
		{ $connection += "User ID=$($script:DC.Database.Login);Password=$($script:DC.Database.Password);"; }

	if ($timeout -eq 0)
	{
		$timeout = $script:DC.Database.Timeout;
	}
	elseif ($timeout -eq -1)
	{
		$timeout = 360000;
	}

	try
	{
		$cn = $null;
		$cn = New-Object Data.SqlClient.SqlConnection($connection);
		$cn.Open()

		$result = @()
		foreach ($ex in $expression -isplit "(?:\A|`r*`n)\s*go\s*(?:\z|`r*`n)")
		{
			$statement = $ex.Trim();
			if ($statement -eq '' -or $statement -ieq 'go')
				{ continue; }
			try
			{
				$cm = New-Object Data.SqlClient.SqlCommand($statement, $cn)
				if ($statement -match "--\s*\`${\s*CommandTimeout\s*[:=]\s*([0-9]+)\s*}\s*")
				{
					$timeout = [int]$Matches[1];
					if ($timeout -eq 0)
					{
						$timeout = $script:DC.Database.Timeout;
					}
					elseif ($timeout -eq -1)
					{
						$timeout = 360000;
					}
				}
				$cm.CommandTimeout = $timeout;
				$rd = $null;
				if (-not $output)
				{
					[void]$cm.ExecuteNonQuery()
				}
				else
				{
					$rd = $cm.ExecuteReader();
					$res = @()
					while ($rd.Read())
					{
						$row = @{}
						for ($i = 0; $i -lt $rd.FieldCount; ++$i)
						{
							$name = ([string]$rd.GetName($i), [string]$i -ne '')[0];
							if ($rd.IsDBNull($i))
								{ $row[$name] = $null; }
							else
								{ $row[$name] = $rd.GetValue($i); }
						}
						$res += New-Object PsObject -Property $row
					}
					if ($result.Length -eq 0) { $result = $res; } else { $result += $res; }
				}
			}
			catch
			{
				throw
			}
			finally
			{
				if (-not [Object]::ReferenceEquals($rd, $null))
					{ $rd.Dispose(); $rd = $null; }
				if (-not [Object]::ReferenceEquals($cm, $null))
					{ $cm.Dispose(); $cm = $null; }
			}
		}
		if ($output) { return $result }
	}
	catch
	{
		throw;
	}
	finally
	{
		if (-not [Object]::ReferenceEquals($cn, $null))
			{ $cn.Dispose(); }
	}
}


function script:Copy-SqlTable([string]$file, [string]$table, [string]$database)
{
	$global:Error.Clear();
	if ([string]$database -eq '') { $database = $script:DC.Database.Database; }
	if ($script:DC.WhatIf) { script:Write-WhatIf "Copy file `"$file`" to sql table `"$table`" on $database"; return; }
	$login = '-T'; if (-not $script:DC.Database.Sspi) { $login = "-U $($script:DC.Database.Login) -P $($script:DC.Database.Password)"; }
	#$cmd = "bcp `"$table`" in `"$file`" -C RAW -N -S `"$($script:DC.Database.Server)`" -d $database $login -E -h`"TABLOCK`""
	script:Sql "set nocount on; delete from $table;" -database:$database
	$result = bcp $table in $file -C RAW -N -S $script:DC.Database.Server -d $database $login -E -hTABLOCK
	if ($lastexitcode -gt 0) { throw $result; }
}


function script:Import-XmlTable([string]$file, [string]$table, [string]$database)
{
	$global:Error.Clear();
	if ([string]$database -eq '') { $database = $script:DC.Database.Database; }
	if ($script:DC.WhatIf) { script:Write-WhatIf "Upload data from `"$file`" into $table on $database"; return; }
	if ($null -eq (script:RunSql "select object_id('$table')" $database -output)) { throw "Cannot find table `"$table`""; }
	$statement = '
		declare @x xml = ''' + [System.IO.File]::ReadAllText($file).Replace("'", "''") + ''';
		exec dbo.upload_TableAsXml ''' + $table.Replace("'", "''") + ''', @x;';
	script:RunSql $statement $database;
}


function script:Cpy([string]$source, [string]$target, [string[]]$exclude = $null, [switch]$append = $false, [switch]$output = $false) {
	if ($script:DC.WhatIf)
		{ script:Write-WhatIf "Copy from `"$source`" to `"$target`""; }
	$sourceDir = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($script:DC.Source, $source))
	$targetDir = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($script:DC.Source, $target))

	if (-not (test-path $sourceDir)) { if ($output) { return 0; } return; }

	if (-not (test-path $targetDir))
		{
		if ($script:DC.WhatIf)
			{ script:Write-WhatIf "Create durectory `"$targetDir`""; }
		else
			{ new-item $targetDir -itemType:directory -errorAction:stop >$null }
		}
	if (test-path $sourceDir -PathType Leaf)
		{
		if ($script:DC.WhatIf)
			{ script:Write-WhatIf "Copy file `"$sourceDir`" to `"$targetDir`""; }
		else
			{ copy-item $sourceDir $targetDir -force -errorAction:stop }
		if ($output) { return 0; }
		return;
		}
	if ($script:DC.WhatIf)
		{
		if ($append)
			{ script:Write-WhatIf "Copy files from `"$sourceDir`" to `"$targetDir`""; }
		else
			{ script:Write-WhatIf "Sync files between `"$sourceDir`" and `"$targetDir`""; }
		if ($output) { return 0; }
		return;
		}

	$excDirs = ($exclude |% { $_.Trim() } |? { $_.Length -gt 0 -and (Test-Path [System.IO.Path]::Combine($sourceDir, $a) -PathType:Container) });
	$excFiles = ($exclude |% { $_.Trim() } |? { $_.Length -gt 0 -and (Test-Path [System.IO.Path]::Combine($sourceDir, $a) -PathType:Leaf) });

	if ($append)
		{ $cpy = robocopy.exe $sourceDir $targetDir /r:5 /w:1 /njh /njs /fp /ns /nc /ndl /np /nfl /xf $excFiles /xd $excDirs /e }
	else
 		{ $cpy = robocopy.exe $sourceDir $targetDir /r:5 /w:1 /njh /njs /fp /ns /nc /ndl /np /nfl /xf $excFiles /xd $excDirs /mir }
	if (($lastexitcode -ge 8) -and ($lastexitcode -lt 16))
		{ throw "Retry limit was exceeded"; }
	if ($lastexitcode -ge 4)
		{ throw $cpy; }
	if ($output)
		{ return $lastexitcode; }
	return;
}


function script:Expand-Package([string]$source, [string]$target = $null, [switch]$subfolder)
{
	$baseName = [System.IO.Path]::GetFileNameWithoutExtension($source);

	$current = Split-Path $source;
	if ([string]$target -eq '') {
		$target = $current;
	}
	else {
		$target = [System.IO.Path]::Combine($current, $target);
	}
	$target2 = if ($target.EndsWith("\$baseName")) { $target } else { "$target\$baseName" }
	$7z = script:Get-7ZipCommand
	if ($null -ne $7z) {
		# check for single subfolder
		set-alias 7z $7z
		$content = 7z l $source |?{ $_ -match '^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d \..{33}' } |%{ $_.Substring(53) }
		if ($null -ne ($content |?{ -not $_.StartsWith("$baseName\", 1) } | select -first 1)) {
			$target = $target2;
		}
		if (script:Write-WhatIf "Extract archive `"$source`" to `"$target`"") { returm; }

		script:Write-Console "Extracting archive $source to $target";
		$output = 7z x $source -aoa -y -o"$target"
		if ($LASTEXITCODE -ne 0) {
			echo $output;
			throw "Cannot extract archive `"$source`"";
		}
	}
	elseif ($source -ilike '*.zip') {
		if (script:Write-WhatIf "Extract archive `"$source`" to `"$target2`"") { returm; }

		script:Write-Console "Extracting archive $source to $target2";
		Expand-Archive $source $target2 -Force
	}
	else {
		throw "Cannot find 7z.exe archiver"
	}
	return $target2;
}


function script:Get-7ZipCommand()
{
	$7z = Get-Command 7z -ErrorAction SilentlyContinue;
	if ($null -ne $7z) {
		return $7z.Source;
	}
	$path = "$env:PATH;$env:ProgramFiles\7-Zip;${env:ProgramFiles(x86)}\7-Zip;$script:ApplicationFolder\Tools" -split ';' |? { Test-Path "$_\7z.exe" -PathType Leaf } | select -first 1;
	if ($null -ne $path) {
		return "$path\7z.exe";
	}
	return $null;
}


function script:GetTargetStamp([string]$target)
{
	if (-not $script:DC.Online) { return @{ Id = 1; Retry = 0; StartTime = (get-date); EndTime = $null; Success = $false; }; }
	script:Write-WhatIf "Get ID for package `"$($script:DC.Current.Package.Name)`" deploying to `"$target`"";
	$packageSql = $script:DC.Current.Package.Name.Replace("'", "''");
	$targetSql = $target.Replace("'", "''");
	$retryBit = [int]$script:DC.Retry
	$whatIfBit = [int]$script:DC.WhatIf
	return (script:RunSql "exec deploy.DeploymentStart '$packageSql','$targetSql',$retryBit,$whatIfBit;" $script:DC.Database.Database -output);
}


function script:SetTargetResult([bool]$success)
{
	if ($script:DC.WhatIf)
	{
		$deployed = ('NOT deployed', 'deployed')[$success]
		script:Write-WhatIf "Mark package `"$($script:DC.Current.Package.Name)`" as $deployed to $($script:DC.Current.Target.Name)."
	}
	elseif ($script:DC.Online)
	{
		$successBit = [int]$success;
		script:Sql "exec deploy.DeploymentEnd $($script:DC.Current.Target.Id),$successBit;"
	}
}


function script:TestPartDeployed([string]$part)
{
	if ($script:DC.Online -and $script:DC.Current.Target.Id -gt 0)
	{
		script:Write-WhatIf "Check whether item `"$part`" was deployed.";
		$t = script:RunSql "select ID from deploy.DeploymentsLogItems where DeploymentLog=$([int]$script:DC.Current.Target.Id) and Item=$(script:sqlStr $part)" $script:DC.Database.Database -output;
		return $t.ID -gt 0;
	}
	return $false;
}


function script:MarkPartDeployed([string]$part)
{
	if ($DC.Online -and $script:DC.Current.Target.Id -gt 0)
	{
		if ($script:DC.WhatIf)
		{
			script:Write-WhatIf "Mark item `"$($part)`" as deployed"
		}
		else
		{
			script:Sql "exec deploy.DeploymentMark $([int]$script:DC.Current.Target.Id), $(script:sqlStr $part)"
		}
	}
}

function script:sqlStr([string]$value)
{
	return "'" + ($part -replace "'", "''") + "'";
}


function script:CreateDeploymentProcedures()
{
	$x = script:RunSql "select object_id('deploy.DeploymentsLog') A" $script:DC.Database.Database -output;
	if ($null -eq $x.A)
	{
		script:Sql "
if (schema_id('deploy') is null)
	exec ('create schema deploy');
go
if (object_id('deploy.DeploymentsLogItems') is not null)
	drop table deploy.DeploymentsLogItems;
if (object_id('deploy.DeploymentsLog') is not null)
	drop table deploy.DeploymentsLog;
go
create table deploy.DeploymentsLog
(
	ID int identity(1, 1) not null
		constraint PK_Deploy_DeploymentsLog primary key,
	DeploymentId nvarchar(200) not null,
	Target nvarchar(200) not null,
	Retry int not null,
	StartTime datetime not null
		constraint DF_Deploy_DeploymentsLog_StartTime default getdate(),
	EndTime datetime null,
	Success bit not null
		constraint DF_DeploymentsLog_Success default 0
);
go
create unique index [IX_Deploy_DeploymentsLog_DeploymentId_Target] on deploy.DeploymentsLog ([DeploymentId] asc, [Target] asc, [Retry] asc);
go
create table deploy.DeploymentsLogItems
(
	ID int identity(1, 1) not null
		constraint PK_Deploy_DeploumentsLogItems primary key,
	DeploymentLog int not null
		constraint FK_Deploy_DeploymentsLogItems_DeploymentLog foreign key references deploy.DeploymentsLog (ID),
	Item nvarchar(200) not null,
	DtCreated datetime not null
		constraint DF_Deploy_DeploymentsLogItems_DtCreated default getdate(),
)
" $script:DC.Database.Database;
	}
	$version = 'deploy.DeploymentStart v.1.0'
	$x = script:RunSql "select ID from deploy.DeploymentsLog where DeploymentId = '$version'" $script:DC.Database.Database -output;
	if ($null -eq $x.ID)
	{
		script:RunSql "
if (object_id('deploy.DeploymentStart') is null)
	exec ('create procedure deploy.DeploymentStart as return;')
go
alter procedure [deploy].[DeploymentStart](@deploymentId varchar(200), @targetName varchar(200), @retry bit = 0, @whatIf bit = 0)
as
	begin
	declare @logId int;
	declare @startTime datetime;
	declare @endTime datetime;
	declare @retryNo int;
	declare @success bit;
	begin tran;

	select top 1
		@logId		= ID,
		@startTime	= StartTime,
		@endTime	= EndTime,
		@retryNo	= Retry,
		@success	= Success
	from deploy.DeploymentsLog
	where DeploymentId = @deploymentId
		and Target = @targetName
	order by Retry desc;

	if (@startTime is null)
		insert into deploy.DeploymentsLog (DeploymentId, Target, Retry)
		output inserted.Id, inserted.Retry, inserted.StartTime, inserted.EndTime, inserted.Success
		values(@deploymentId,@targetName,0);

	else if (@retry = 1)
		insert into deploy.DeploymentsLog (DeploymentId, Target, Retry)
		output inserted.Id, inserted.Retry, inserted.StartTime, inserted.EndTime, @success Success
		values(@deploymentId,@targetName,@retryNo + 1);

	else if (@endTime is not null and @success = 0)
	begin
		update deploy.DeploymentsLog
		set EndTime = null,
			Retry = @retryNo + 1
		where ID = @logId;
		select @logId Id, @retryNo + 1 Retry, @startTime StartTime, cast(null as datetime) EndTime, cast(0 as bit) Success
	end

	else
		select 0 Id, @retryNo Retry, @startTime StartTime, @endTime EndTime, @success Success;

	if (@whatIf = 1)
		rollback;
	else
		commit;
	end;
go
if (object_id('deploy.DeploymentEnd') is null)
	exec ('create procedure deploy.DeploymentEnd as return;')
go
alter procedure deploy.DeploymentEnd(@targetId int, @success bit)
as
	begin
	update deploy.DeploymentsLog
	set EndTime = getdate(),
		Success = @success
	where Id = @targetId;
	end;
go
if (object_id('deploy.DeploymentMark') is null)
	exec ('create procedure deploy.DeploymentMark as return;')
go
alter procedure deploy.DeploymentMark(@targetId as int, @item varchar(200))
as
	begin
	insert into deploy.DeploymentsLogItems(DeploymentLog, Item) values (@targetId, @item);
	end
go
insert into deploy.DeploymentsLog (DeploymentId, Target, Retry, StartTime, EndTime, Success)
values ('$version', 'DB', 0, getdate(), getdate(), 1);

" $script:DC.Database.Database;
	}
}


function script:Duration([DateTime]$start)
{
	$result = (get-date).Subtract($start).ToString("\ h' h 'm' min 's\.fff' sec'").Replace(' 0 h ', ' ').Replace(' 0 min ', '').Trim();
	return $result;
}

#endregion
