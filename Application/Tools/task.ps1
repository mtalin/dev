﻿param
(
	[string]$action,
	[double]$timeout
)
$cfg = 'C:\Application\Config\task.config.ps1'
$url = 'http://localhost/admin/svc/action'

if (test-path $cfg -pathType:leaf)
{
    (.$cfg)
}
if ($timeout -gt 0)
{
	$url += "?a=$action&t=$timeout"
}
else
{
	$url += "?$action"
}
$r = invoke-webRequest $url -TimeoutSec 72000
echo $r.Content
