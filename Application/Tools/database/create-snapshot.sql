use master
declare @resotre bit = 1;
declare @tag varchar(50) = '_' + convert(varchar, getdate(), 112) + '_' + replace(convert(varchar, getdate(), 108), ':', '');
declare @path varchar(250) = 'D:\SQL Server\' + @@servicename + '\MSSQL11.' + @@servicename + '\MSSQL\Data\';

declare @database varchar(100) = 'CharityPlanner';
declare @snapshot varchar(999);

set @path = 'D:\MSSQL11.MSSQLSERVER\MSSQL\Backup\'; -- 'D:\SqlServer\Snapshots\';

if (@resotre=1)
	begin
	set @tag = '_20180810_123836';
	set @snapshot = @database + '_snapshot' + @tag;
	exec ('
	alter database ' + @database + ' set single_user with rollback after 0;
	restore database ' + @database + ' from database_snapshot = ''' + @snapshot + ''';
	alter database ' + @database + ' set multi_user;
	')
	end
else
	begin
	declare @file sysname, @stmt nvarchar(max) = N'use ' + @database + N';set @file=(select top 1 name from sys.database_files where type=0)';
	exec sp_executesql @stmt, N'@file sysname out', @file out
	exec ('
	use ' + @database + ';
	create database ' + @database + '_snapshot' + @tag + ' on
		(
		name = ' + @file + ',
		filename = ''' + @path + @database + '_snapshot' + @tag + '.ss''
		)
	as snapshot of ' + @database + ';
	print char(9) + ''set @tag = ''''' + @tag + ''''';'';
	print char(9) + ''' + @database + '_snapshot' + @tag + ''';
	')
	end
