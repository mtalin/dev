use CharityPlanner
GO

--> Create RO user

while (@@trancount > 0) rollback
go
-- use CharityPlanner
exec begin_log 'v90-xxx-000-user.sql'
go
set nocount on
go
declare @seed int set @seed = 1000
---


if (exists (select * from master..sysdatabases where name='Charities4'))
	exec ('use Charities4 if (exists(select * from sysusers where name=''ro'')) exec sp_dropuser ''ro''')
if (exists (select * from master..sysdatabases where name='Charities'))
	exec ('use Charities if (exists(select * from sysusers where name=''ro'')) exec sp_dropuser ''ro''')
if (exists (select * from master..sysdatabases where name='CharityPlanner'))
	exec ('use CharityPlanner if (exists(select * from sysusers where name=''ro'')) exec sp_dropuser ''ro''')
go

use master
if exists (select * from syslogins where loginname = 'ro')
	exec sp_droplogin 'ro'
go
exec sp_addlogin 'ro', 'r+only.4.test', 'charityplanner', 'us_english'
go

if (exists (select * from master..sysdatabases where name='Charities4'))
	exec ('use Charities4 exec sp_adduser ''ro'', ''ro'', ''db_datareader''')
if (exists (select * from master..sysdatabases where name='Charities'))
	exec ('use Charities exec sp_adduser ''ro'', ''ro'', ''db_datareader''')
if (exists (select * from master..sysdatabases where name='CharityPlanner'))
	exec ('use CharityPlanner exec sp_adduser ''ro'', ''ro'', ''db_datareader''')
go

use CharityPlanner
exec grant_ro_access

---
exec end_log
go
