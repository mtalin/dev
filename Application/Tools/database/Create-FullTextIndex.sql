USE [Charities]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

CREATE FULLTEXT CATALOG [Charities] WITH ACCENT_SENSITIVITY = ON
GO

CREATE FULLTEXT INDEX ON [dbo].[Charities](
[Address] LANGUAGE [English], 
[Address2] LANGUAGE [English], 
[AltAddress] LANGUAGE [English], 
[AltAddress2] LANGUAGE [English], 
[AltCity] LANGUAGE [English], 
[AltName] LANGUAGE [English], 
[AltState] LANGUAGE [English], 
[AltZipPlus4] LANGUAGE [English], 
[City] LANGUAGE [English], 
[OrgName] LANGUAGE [English], 
[Phone] LANGUAGE [English], 
[State] LANGUAGE [English])
KEY INDEX [PK_Charities]ON ([Charities], FILEGROUP [PRIMARY])
WITH (CHANGE_TRACKING = AUTO, STOPLIST = SYSTEM)
GO

CREATE FULLTEXT INDEX ON [dbo].[CharityNotesFTS](
[NoteText] LANGUAGE [English])
KEY INDEX [PK_CharityNotesFTS]ON ([Charities], FILEGROUP [PRIMARY])
WITH (CHANGE_TRACKING = AUTO, STOPLIST = SYSTEM)
GO

CREATE FULLTEXT INDEX ON [dbo].[CharitiesFTS](
[SearchAddress] LANGUAGE [English], 
[SearchText] LANGUAGE [English])
KEY INDEX [IX_CharitiesFTS_ID]ON ([Charities], FILEGROUP [PRIMARY])
WITH (CHANGE_TRACKING = AUTO, STOPLIST = SYSTEM)
GO
