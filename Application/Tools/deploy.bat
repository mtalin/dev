@echo off
set args=%*
if (%1)==() set args=%~dp0
net file 1>nul 2>nul && goto :run || powershell -ex unrestricted -Command "Start-Process -Verb RunAs -FilePath '%comspec%' -ArgumentList '/c %~fnx0 %args%'"
goto :eof
:run
powershell -command C:\Application\Tools\deploy.ps1 %* -pause
exit /b %errorlevel%