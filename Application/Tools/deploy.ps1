﻿param
(
	[parameter(Mandatory=$false, ValueFromPipeline=$true)]
	$Source,
	[switch]$Offline,
	[switch]$Retry,
	[switch]$WhatIf,
	[switch]$Trace,
	[switch]$Pause
)

Import-Module -Force $PSScriptRoot\deploy.core.psm1 -DisableNameChecking

$result = Publish-Package $Source `
{
	Publish-Target "$((Publish-Config).Database.Server).db" -test:'Database' `
	{
		Publish-Part 'Database' `
		{
			if (Publish-Test 'Database\Scripts') { Publish-StopApplication -force; }
			Publish-ScriptPart 'Database\begin.ps1';
			Publish-Part 'Database\Data' `
			{
				$m = (Publish-Map 'Database\Data');
				foreach ($item in (ls "$($m.Source)\*.xml"))
				{
					if (Publish-XmlTablePart "Database\Data\$($item.Name)" -output)
					{
						if ($item.BaseName.EndsWith('RegistryRules990', [StringComparison]::OrdinalIgnoreCase)) { Publish-Sql 'exec update_rr990_order;' }
						if ($item.BaseName.EndsWith('RegistryRulesStatements', [StringComparison]::OrdinalIgnoreCase)) { Publish-Sql 'exec PrepareRegistryRulesStatements;' }
					}
				}
			}

			$testA = (Publish-SqlPart 'Database\Scripts' -filter:'????????+?*.sql' -database:'CharityPlanner' -output);

			$testB = (Publish-SqlPart 'Database\CharityPlanner\Functions'          -database:'CharityPlanner' -output) -or $testA;
					  Publish-SqlPart 'Database\CharityPlanner\Views'              -database:'CharityPlanner'
			$testB = (Publish-SqlPart 'Database\CharityPlanner\Procedures'         -database:'CharityPlanner' -output) -or $testB;
			$testB = (Publish-SqlPart 'Database\Charities\Functions'               -database:'Charities'      -output) -or $testB;
					  Publish-SqlPart 'Database\Charities\Views'                   -database:'Charities'
			$testB = (Publish-SqlPart 'Database\Charities\Procedures'              -database:'Charities'      -output) -or $testB;

			if ($testA) { Publish-Sql 'exec recompile_procs;' }

			Publish-SqlPart 'Database\Scripts' -filter:'????????-?*.sql' -database:'CharityPlanner';

			if ($testA -or $testB) { Publish-Sql 'exec grant_ro_access;' }

			Publish-ScriptPart 'Database\end.ps1';
		}
	}

	Publish-Target "$((Publish-Config).Server.Name).app" `
	{
		Publish-ScriptPart 'begin.ps1';
		Publish-Application
		Publish-ScriptPart 'end.ps1';
	}

} -offline:$Offline -retry:$Retry -simulate:$WhatIf -trace:$Trace -skipTools:$SkipTools

if ($result -eq 'restart-admin') {
	$cmd = "-c $PSCommandPath " + (resolve-path $Source);
	if ($Offline) { $cmd += " -Offline"}
	if ($Retry) { $cmd += " -Retry"}
	if ($WhatIf) { $cmd += " -WhatIf"}
	if ($Trace) { $cmd += " -Trace"}
	if ($Pause) { $cmd += " -Pause"}
	# will start in system32 directory
	Start-Process -Verb RunAs powershell -ArgumentList $cmd
	exit;
}
if ($result -eq 'restart') {
	& $PSCommandPath $Source -offline:$Offline -retry:$Retry -whatIf:$WhatIf -trace:$Trace -pause:$Pause
	exit;
}
if ($Pause) {
	[System.Console]::Write("Press any key to continue . . .")
	[void][System.Console]::ReadKey($true)
}

exit if ($result -eq 'error') { -1 } else { 0 };
