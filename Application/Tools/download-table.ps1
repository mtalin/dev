param
(
	[parameter(Mandatory=$true, ValueFromPipeline=$true)]
	[string[]]$Table,
	[string]$Server = $null,
	[string]$Database = $null,
	[string]$Password = $null,
	[switch]$Schema,
	[switch]$SchemaOnly,
	[switch]$WhatIf
)
begin
{
	if ($SchemaOnly) { $Schema = $true; }
	. "$PSScriptRoot\upload-lib.ps1"
	Config $Server $Database $Password
}
process
{
	foreach ($item in $Table)
	{
		$tableName = RunSql "select object_schema_name(object_id('$item')) + '.' + object_name(object_id('$item'))" -scalar;
		if ($tableName -eq $null)
		{
			Write-Error "Cannot find table $item";
			continue;
		}
		if ($WhatIf)
		{
			if (-not $SchemaOnly) { Write-Host "What if: Download data from `"$tableName`" into `"$Pwd\$tableName.xml`"" -ForegroundColor Green; }
			if ($Schema) { Write-Host "What if: Download schema definition for `"$tableName.xml`" into `"$Pwd\$tableName.xsd`"" -ForegroundColor Green; }
			continue;
		}
		if (-not $SchemaOnly)
		{
			$xml = [xml](script:RunSql "
				declare @r xml;
				exec dbo.download_TableAsXml '$item', @r out
				select @r;" -scalar);
			[IO.File]::WriteAllText("$Pwd\$tableName.xml", (FormatXml($xml)));
		}
		if ($Schema)
		{
			$xml = [xml](script:RunSql "select dbo.get_TableXsd('$item');" -scalar);
			[IO.File]::WriteAllText("$Pwd\$tableName.xsd", (FormatXml($xml)));
		}
	}
}
