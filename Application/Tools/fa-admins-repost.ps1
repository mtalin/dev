﻿param
(
	[string]$Server = 'DB1',
	[string]$Database = 'CharityPlanner',
	[string]$Password = $null,
	[switch]$WhaiIf
)

$script:Config = @{
	Server = $Server;
	Database = $Database;
	Passwort = $Password;
	WhatIf = $WhaiIf;
	Timeout = 0;
};


function script:Configure([string]$configJson)
{
	$c = ConvertFrom-Json $configJson;
	if ([string]$c.Server -ne '') { $script:Config.Server = [string]$c.Server; }
	if ([string]$c.Database -ne '') { $script:Config.Database = [string]$c.Database; }
	if ([string]$c.Passwort -ne '') { $script:Config.Passwort = [string]$c.Passwort; }
	if ([string]$c.Timeout -ne '') { $script:Config.Passwort = [string]$c.Passwort; }
	if ($c.WhatIf -is [bool]) { $script:Config.WhatIf = $c.WhatIf; }
	if ($c.Timeout -is [int]) { $script:Config.Timeout = $c.Timeout; }

	if ($c.WhatIf) { Write-Host "What if: Use configuration: `"$configJson`"" -ForegroundColor Green; }
}

function Run-Sql([string]$expression, [switch]$scalar = $false, [switch]$silent = $false, [switch]$noThrow = $false, [int]$timeout = 0)
{
	$statement = $expression.Trim();
	if ($statement -eq '') { return }
	if ($script:Config.WhatIf) { return }
	$connection = "Server=$($script:Config.Server);Database=$($script:Config.Database);"
	if ([string]$script:Config.Password -eq '')
		{ $connection += "Trusted_Connection=yes;"; }
	else
		{ $connection += "User ID=sa;Password=$($script:Config.Password);"; }
	try
	{
		$cn = $null;
		$cn = New-Object Data.SqlClient.SqlConnection($connection);
		$cn.Open()

		try
		{
			$cm = New-Object Data.SqlClient.SqlCommand($statement, $cn)
			if ($statement -match "--\s*\`${\s*CommandTimeout\s*[:=]\s*([0-9]+)\s*}\s*") { $timeout = [int]$Matches[1]; }
			if ($timeout -ne 0)
			{
				if ($timeout -lt 0)
				{
					$cm.CommandTimeout = 360000;
				}
				else
				{
					$cm.CommandTimeout = $timeout;
				}
			}
			if ($scalar) { return $cm.ExecuteScalar(); }
			if ($silent)
			{
				[void]$cm.ExecuteNonQuery();
				return;
			}

			$rd = $cm.ExecuteReader();
			$result = New-Object System.Collections.ArrayList
			while ($rd.Read())
			{
				$row = @{}
				for ($i = 0; $i -lt $rd.FieldCount; ++$i)
				{
					$name = ([string]$rd.GetName($i), [string]$i -ne '')[0];
					if ($rd.IsDBNull($i))
						{ $row[$name] = $null; }
					else
						{ $row[$name] = $rd.GetValue($i); }
				}
				[void]$result.Add((New-Object PsObject -Property $row));
			}
			return $result;
		}
		catch
		{
			if ($noThrow) { return $null; } else { throw; }
		}
		finally
		{
			if (-not [Object]::ReferenceEquals($rd, $null))
				{ $rd.Dispose(); $rd = $null; }
			if (-not [Object]::ReferenceEquals($cm, $null))
				{ $cm.Dispose(); $cm = $null; }
		}
	}
	catch
	{
		if ($noThrow) { return $null; } else { throw; }
	}
	finally
	{
		if (-not [Object]::ReferenceEquals($cn, $null))
			{ $cn.Dispose(); }
	}
}

function Format-Xml([xml]$xml)
{
	$sw = New-Object System.IO.StringWriter;
	$xw = New-Object System.Xml.XmlTextWriter $sw;
	$xw.Indentation = 1;
	$xw.IndentChar = "`t"
	$xw.Formatting = "indented";
	$xml.WriteTo($xw);
	$xw.Flush();
	$sw.Flush();
	return $sw.ToString()
}

Configure '
{
  "Server": "STATE10"
}
'


Add-Type 'C:\Application\Objects\Fsbcl.1.dll'

$ap = New-Object 'FsBcl.Ap'

$x = Run-Sql "select Person from vActiveCompanyPersons where StructuredCompany=1"
