@echo off
net file 1>nul 2>nul && goto :run || powershell -ex unrestricted -Command "Start-Process -Verb RunAs -FilePath '%comspec%' -ArgumentList '/c %~dpfnx0 %*'"
goto :eof
:run
powershell -command %~dp0init-application.ps1 %*
pause
