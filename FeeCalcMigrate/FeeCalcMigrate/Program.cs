﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using FoundationSource.Admin.Common;
using FoundationSource.Admin.CPBase;
using FoundationSource.Admin.CPMain;
using Lexxys;
using Lexxys.Data;

namespace FoundationSource.Admin
{
	class Program
	{
		public const PendingTransactSteps ExcludedStep = PendingTransactSteps.AwaitingConfirmation; // 27;
		public static readonly DateTime StartDate = new DateTime(2014, 1, 1);

		public const bool ToDb = true;
		public const bool Rollback = false;

		static void Main(string[] args)
		{
			var id = Dc.GetValue<int>(@"
				insert TrustedActions 
					(Admin, Person, Foundation, DtInstructed, InstructedBy, DtCreated)
				output inserted.ID
				values
					(1, 1, 108, getdate(), 'A.Nesterov', getDate())");
			Application.LogOn("trustedAction", id, null, null);

			var authorized = Application.CurrentUserId > 0;
			Console.WriteLine("Start as {0} {1}\r\n", Application.CurrentUserId, authorized ? "authorized" : "NOT AUTHORIZED");
			if (!authorized) return;

			if (ToDb && !Rollback)
				Dc.Execute(@"
					declare @T table (Id int)
					insert @T select Bill from BillSteps where Author = 1
					delete from BillItems where Bill in (select Id from @T)
					delete from BillSteps where Bill in (select Id from @T)
					delete from Bills where Id in (select Id from @T)
					delete from InvoiceSteps where Invoice in (select Id from Invoices where Author = 1)
					delete from Invoices where Author = 1");

			DoSf();
			DoQf();

			Dc.Execute("delete from TrustedActions where id = " + Dc.Id(id));
		}

		public static void DoSf()
		{
			var billsSql = @"
				select 
					y.ID, fc.FeeType, fc.Amount,  y.DtBill, fc.DtPeriod, fc.Contract, fc.BillTo, fc.Step, fc.Note, fc.DtCreated
				from (
					select 
						x.ID, x.DtBill, ps.BillingType
					from (
						select
							fc.ID,
							case 
								when fc.DtPeriod is null then cast(fc.DtCreated as date) 
								else cast(dateadd(d, -1, dateadd(q, 1, fc.DtPeriod)) as date) 
							end DtBill,
							cast(cast(S.Loc.query('ID/text()') as varchar(255)) as int) sLoc
						from FeeCalculations fc
							cross apply Snapshot.nodes('/SFSnapshot/ScheduleNet') as S(Loc)
							where fc.DtCreated >= " + Dc.Value(StartDate) + @"
								and fc.Step > " + Dc.Value(ExcludedStep) + @"
								and fc.Amount <> 0
						) x
						left join PricingScheduleInstances psi on psi.ID = sLoc
						left join PricingScheduleInstances_tmp psi2 on psi2.ID = sLoc
						left join ContractPricings cp on cp.ID = ISNULL(psi.ContractPricing, psi2.ContractPricing)
						left join PricingSchedules ps on ps.ID = cp.PricingSchedule
					group by x.ID, x.DtBill, ps.BillingType
					) y
					join FeeCalculations fc on y.ID = fc.ID
					join Contracts c on fc.Contract = c.ID
				where 
					c.ParentContract is null and c.IsActive = 1 AND c.IsTemplate = 0 and isnull(c.DtTerminate, '3000-01-01') > dbo.FixDate(getdate())
				order by fc.id";
			var bills = Dc.GetList<FcBill>(billsSql);

			Console.WriteLine("SF BILLS");
			var n = 0;

			foreach (var bill in bills)
			{
				Console.WriteLine("{0}: FcId={1}", ++n, bill.FcId);

				var psi = GetPricingScheduleInstancesSf(bill.FcId);
				var taxes = GetTaxes(bill.FcId);
				if (taxes.Count == 0)
					continue;

				if (ToDb)
				{
					using (Dc.Transaction())
					{
						CreateBill(bill, taxes, psiSf: psi);
						if (Rollback)
							Dc.Rollback();
						else
							Dc.Commit();
					}
				}
			}
		}

		public static void DoQf()
		{
			var billsSql = @"
				select
					fc.ID, fc.FeeType, fc.Amount, 
					case 
						when fc.DtPeriod is null then cast(fc.DtCreated as date) 
						else dateadd(d, -1, cast(dateadd(q, 1, fc.DtPeriod) as date)) 
					end DtBill, 
					fc.DtPeriod, fc.Contract, fc.BillTo, fc.Step, Note, fc.DtCreated
				from FeeCalculations fc
					join Contracts c on fc.Contract = c.ID
				where fc.Snapshot is not null and cast(Snapshot as varchar(max)) like '<QFSnapshot>%'
					and fc.Amount <> 0
					and fc.DtCreated >= " + Dc.Value(StartDate) + @"
					and fc.Step > " + Dc.Value(ExcludedStep) + @"
					and c.ParentContract is null and c.IsActive = 1 AND c.IsTemplate = 0 and isnull(c.DtTerminate, '3000-01-01') > dbo.FixDate(getdate())
				order by fc.id";
			var bills = Dc.GetList<FcBill>(billsSql);

			Console.WriteLine("QF BILLS");
			var n = 0;

			foreach (var bill in bills)
			{
				Console.WriteLine("{0}: FcId={1}", ++n, bill.FcId);

				var taxes = GetTaxes(bill.FcId);
				var psi = GetPricingScheduleInstancesQf(bill.FcId);
				if (psi.Count == 0 || taxes.Count == 0)
					continue;

				var x = new List<PsiQf>();
				var y = Dc.GetList(new { PsId = 0, Amount = 0m }, @"
					select 
						PsID, cast((PartAmount * RealDays / Days) / 4 as money) PricingScheduleAmount 
					from (
						select 
							FcId, PsID,
							AverageBalance * Multiplier + Addend PartAmount,
							datediff(d, DtStart, DtEnd) RealDays,
							datediff(d, DtPeriod, dateadd(q, 1, DtPeriod)) [Days]
						from (
							select 
								fc.Id FcId, sn.PsId PsId, 
								sn.[From], isnull(sn.[To], 9999999999) [To],
								sn.Addend, sn.Limit, sn.Multiplier,
								cast(cast(T1.Loc.query('AverageBalanceTotal/text()') as varchar) as money) AverageBalance,
								case when sn.DtStart < t.DtPeriod  then t.DtPeriod else sn.DtStart end DtStart,
								case when isnull(dateadd(d, 1, sn.DtEnd), getdate() + 1) > dateadd(q, 1, t.DtPeriod) then dateadd(q, 1, t.DtPeriod) else isnull(dateadd(d, 1, sn.DtEnd), getdate() + 1) end DtEnd,
								t.DtPeriod
							from ActivePricingsOfQuaterlyFeeFromSnapshot sn
								join FeeCalculations fc on fc.Id = sn.ID
								join Taxes t on t.FeeCalculation = fc.Id
								cross apply Snapshot.nodes('/QFSnapshot') as T1(Loc)
							) l1
					where 
						AverageBalance >= [From] and AverageBalance <= [To]
					) l2
				where FcId = " + Dc.Id(bill.FcId));

				foreach (var p in psi)
				{
					var z = y.FirstOrDefault(y1 => y1.PsId == p.PricingScheduleInstance && y1.Amount != 0);
					if (z != null)
					{
						p.PsiAmount = Math.Round(z.Amount, 2);
						x.Add(p);
					}
				}

				var sumX = x.Sum(p => p.PsiAmount);
				if (x.Count == 0 || Math.Abs(sumX - bill.Amount) > 0.01m)
				{
					if (x.Count == 0)
					{
						x = psi.Where(p =>
							p.SnapshotStartDate <= bill.DtPeriod &&
							(p.SnapshotEndDate >= bill.DtPeriod || p.SnapshotEndDate == new DateTime(1900, 1, 1))).ToList();
					}
					if (x.Count == 0)
						x = psi;
					foreach (var item in x)
					{
						item.PsiAmount = bill.Amount / x.Count;
					}
				}

				if (ToDb)
				{
					using (Dc.Transaction())
					{
						CreateBill(bill, taxes, psiQf: x);
						if (Rollback)
							Dc.Rollback();
						else
							Dc.Commit();
					}
				}
			}
		}

		public static List<PsiSf> GetPricingScheduleInstancesSf(int id)
		{
			var psiSql = @"
				select
					fc.ID,
					cast(cast(S.Loc.query('ID/text()') as varchar(255)) as int) PricingScheduleInstance,
					cast(cast(S.Loc.query('DtStart/text()') as varchar(255)) as date) DtStart,
					psi2.TransactionType,
					psi2.Amount,
					psi.Id
				from FeeCalculations fc
					cross apply Snapshot.nodes('/SFSnapshot/ScheduleNet') as S(Loc)
					join PricingScheduleInstances_tmp psi2 on psi2.ID = cast(cast(S.Loc.query('ID/text()') as varchar(255)) as int)
					join PricingScheduleInstances psi on psi2.IdNew = psi.ID
				where fc.Id = " + Dc.Id(id) + @"
					and psi2.Amount > 0
				order by fc.Id";
			return Dc.GetList<PsiSf>(psiSql);
		}

		public static List<PsiQf> GetPricingScheduleInstancesQf(int id, StreamWriter f = null)
		{
			var psiSql = @"
				select 
					fc.ID, 
					fc2.PricingScheduleInstance, 
					psi.TransactionType, 
					psi.Amount, 
					fc2.DtStart, fc2.DtEnd, psi.StartDate, psi.EndDate, fc.DtCreated, psi.DtCreated
				from FeeCalculations fc
					join (
						select 
							fc.ID,
							cast(cast(S.Loc.query('ID/text()') as varchar(255)) as int) PricingScheduleInstance,
							cast(cast(S.Loc.query('DtStart/text()') as varchar(255)) as date) DtStart,
							cast(cast(S.Loc.query('DtEnd/text()') as varchar(255)) as date) DtEnd
						from FeeCalculations fc
							cross apply Snapshot.nodes('/QFSnapshot/ScheduleNet') as S(Loc)
						) fc2 on fc.ID = fc2.ID
					join PricingScheduleInstances psi on psi.ID = fc2.PricingScheduleInstance 
					join ContractPricings cp on cp.ID = psi.ContractPricing
					join PricingSchedules ps on cp.PricingSchedule = ps.ID and ps.BillingType = 12
				where fc.Id = " + Dc.Id(id) + @"
				order by fc.Id";
			return Dc.GetList<PsiQf>(psiSql);
		}

		public static List<Tax> GetTaxes(int id)
		{
			var taxesSql = @"
				select 
					t.ID, t.Contract, t.BillTo, t.Type, t.Amount, t.Amount2, t.DtPeriod, t.Step, t.DtCreated, ii.Id, ii.Invoice, i.Step
				from Taxes t
					left join InvoicesItems_Old ii on t.Id = ii.Tax
					left join Invoices_Old i on ii.Invoice = i.ID
				where t.FeeCalculation = " + Dc.Id(id) + @"
				order by t.Type";
			return Dc.GetList<Tax>(taxesSql);
		}

		public static Bill CreateBill(FcBill bill, List<Tax> taxes, List<PsiSf> psiSf = null, List<PsiQf> psiQf = null)
		{
			BillingType bt;
			if (psiQf != null)
				bt = BillingType.Quarter;
			else
				bt = 
					bill.FeeType == 58 ? BillingType.Quarter : 
					bill.FeeType == 60 ? BillingType.Termination : 
					bill.FeeType == 102 ? BillingType.Setup : 
					bill.FeeType == 326 ? BillingType.Termination : BillingType.Other;
			using (Dc.Transaction())
			{
				var b = new Bill
				{
					Amount = bill.Amount,
					BillDate = bill.DtBill,
					BillToId = bill.BillTo,
					ContractId = bill.Contract,
					Step = BillStep.Sent,
					Note = bill.Note,
					BillType = bt,
				};
				b.Update();
				if (psiSf != null)
				{
					foreach (var item in psiSf)
					{
						var psInst = PricingScheduleInstance.TryLoad(item.NewPsiId);
						var bi = new BillItem(psInst)
						{
							BillId = b.Id,
							Amount = item.PsiAmount,
							BillDate = bill.DtBill,
						};
						bi.PricingScheduleInstance.StartDate = bill.DtBill;
						bi.PricingScheduleInstance.EndDate = null;
						bi.PricingScheduleInstance.BillingPricing.RecalculateSnapshotInternal(bi);
						bi.Amount = item.PsiAmount;

						var taxId = taxes.FirstOrDefault(t => t.Type == item.TransactionType && t.Amount == item.PsiAmount)?.Id ??
									taxes.FirstOrDefault(t => t.Type == item.TransactionType && t.Amount > item.PsiAmount)?.Id;
						if (Common.Check.IsId(taxId))
							bi.Tax = taxId;
						bi.Update();
					}
				}
				if (psiQf != null)
				{
					foreach (var item in psiQf)
					{
						var psInst = PricingScheduleInstance.TryLoad(item.PricingScheduleInstance);
						var bi = new BillItem(psInst)
						{
							BillId = b.Id,
							Amount = item.PsiAmount,
							BillDate = bill.DtBill,
							PeriodDate = bill.DtPeriod.GetValueOrDefault(),
						};
						bi.PricingScheduleInstance.StartDate = item.SnapshotStartDate;
						bi.PricingScheduleInstance.EndDate = null;
						bi.PricingScheduleInstance.BillingPricing.RecalculateSnapshotInternal(bi);
						bi.Amount = item.PsiAmount;
						var taxId = taxes.Any() ? taxes[0].Id : 0;
						if (Common.Check.IsId(taxId))
							bi.Tax = taxId;
						bi.Update();
					}
				}

				var tx = taxes.FirstOrDefault(t => t.InvoiceItemId != null);
				if (tx != null)
				{
					var id =  CreateInvoice(bill.BillTo, bill.Amount, tx.InvoiceStep.Value, tx.Invoice.Value);
					b.InvoiceId = id;
					try
					{
						b.Update();
					}
					catch (Exception e)
					{
						e.LogError();
						Console.WriteLine(e);
					}
				}

				if (Rollback)
					Dc.Rollback();
				else
					Dc.Commit();
				return b;
			}
		}

		public static int CreateInvoice(int billTo, decimal amount, int step, int oldId)
		{
			using (Dc.Transaction())
			{
				var invoice = new Invoice
				{
					BillTo = billTo,
					Amount = amount,
					Step = (InvoiceStep) step,
					Number = oldId
				};
				invoice.Update();
				if (Rollback)
					Dc.Rollback();
				else
					Dc.Commit();
				return invoice.Id;
			}
		}

		public static void GenerateInvoiceDocumets()
		{
			var oldInvoiceNumbers = Dc.GetList<int>("select distinct Number from Invoices");
			foreach (var number in oldInvoiceNumbers)
			{
				var doc = InvoiceProcess.GenerateInvoice(new[] {number});
				var docId = doc.Id;
				Dc.Execute("update Invoices set Document = " + Dc.Value(docId) + " where Number = " + Dc.Value(number));
			}
		}

		#region Write
		public static void WritePsiSf(List<PsiSf> psi, StreamWriter f)
		{
			f.WriteLine(",PricingScheduleInstances");
			f.WriteLine(",ID, Type, Amount");
			foreach (var i in psi)
			{
				f.WriteLine(", {0}, {1}, {2}",
					i.NewPsiId, i.TransactionType, i.PsiAmount);
			}
		}

		public static void WritePsiQf(List<PsiQf> psi, StreamWriter f)
		{
			f?.WriteLine(",PricingScheduleInstances");
			f?.WriteLine(",ID, Type, SnapshotStartDate, SnapshotEndDate, PsiStartDate, PsiEndDate");
			foreach (var i in psi)
			{
				f?.WriteLine(", {0}, {1}, {2:MM/dd/yy}, {3:MM/dd/yy}, {4:MM/dd/yy}, {5:MM/dd/yy}",
					i.PricingScheduleInstance, i.TransactionType, i.SnapshotStartDate, i.SnapshotEndDate, i.PsiStartDate, i.PsiEndDate);
			}
		}

		public static void WriteTaxes(List<Tax> taxes, StreamWriter f)
		{
			f.WriteLine(",Taxes");
			f.WriteLine(", Id, Type, Amount");
			foreach (var t in taxes)
			{
				f.WriteLine(", {0}, {1}, {2}", t.Id, t.Type, t.Amount);
			}
		}
		#endregion

		public class FcBill
		{
			public int FcId;
			public int FeeType;
			public decimal Amount;
			public DateTime DtBill;
			public DateTime? DtPeriod;
			public int Contract;
			public int BillTo;
			public int Step;
			public string Note;
			public DateTime DtCreated;

			public FcBill(int fcId, int type, decimal amount, DateTime dtBill, DateTime? dtPeriod, int contract, int billTo, int step, string note, DateTime dtCreated)
			{
				FcId = fcId;
				FeeType = type;
				Amount = amount;
				DtBill = dtBill;
				DtPeriod = dtPeriod;
				Contract = contract;
				BillTo = billTo;
				Step = step;
				Note = note;
				DtCreated = dtCreated;
			}
		}

		public class PsiSf
		{
			public int FcId;
			public int PricingScheduleInstance;
			public DateTime? SnapshotStartDate;
			public int TransactionType;
			public decimal PsiAmount;
			public int NewPsiId;

			public PsiSf(int fcId, int pricingScheduleInstance, DateTime? snapshotStartDate, int transactionType, decimal psiAmount, int newPsiId)
			{
				FcId = fcId;
				PricingScheduleInstance = pricingScheduleInstance;
				SnapshotStartDate = snapshotStartDate;
				TransactionType = transactionType;
				PsiAmount = psiAmount;
				NewPsiId = newPsiId;
			}
		}

		public class PsiQf
		{
			public int FcId;
			public int PricingScheduleInstance;
			public int TransactionType;
			public decimal PsiAmount;
			public DateTime? SnapshotStartDate;
			public DateTime? SnapshotEndDate;
			public DateTime? PsiStartDate;
			public DateTime? PsiEndDate;
			public DateTime FcDtCreated;
			public DateTime PsiDtCreated;

			public PsiQf(int fcId, int pricingScheduleInstance, int transactionType, decimal psiAmount, DateTime? snapshotStartDate, DateTime? snapshotEndDate, DateTime? psiStartDate, DateTime? psiEndDate, DateTime fcDtCreated, DateTime psiDtCreated)
			{
				FcId = fcId;
				PricingScheduleInstance = pricingScheduleInstance;
				TransactionType = transactionType;
				PsiAmount = psiAmount;
				SnapshotStartDate = snapshotStartDate;
				SnapshotEndDate = snapshotEndDate;
				PsiStartDate = psiStartDate;
				PsiEndDate = psiEndDate;
				FcDtCreated = fcDtCreated;
				PsiDtCreated = psiDtCreated;
			}
		}

		public class Tax
		{
			public int Id;
			public int? Contract;
			public int? BillTo;
			public int Type;
			public decimal Amount;
			public decimal? Amount2;
			public DateTime? DtPeriod;
			public int Step;
			public DateTime DtCreated;
			public int? InvoiceItemId;
			public int? Invoice;
			public int? InvoiceStep;

			public Tax(int id, int? contract, int? billTo, int type, decimal amount, decimal? amount2, DateTime? dtPeriod, int step, DateTime dtCreated, int? invoiceItemId, int? invoice, int? invoiceStep)
			{
				Id = id;
				Contract = contract;
				BillTo = billTo;
				Type = type;
				Amount = amount;
				Amount2 = amount2;
				DtPeriod = dtPeriod;
				Step = step;
				DtCreated = dtCreated;
				InvoiceItemId = invoiceItemId;
				Invoice = invoice;
				InvoiceStep = invoiceStep;
			}
		}

	}
}
