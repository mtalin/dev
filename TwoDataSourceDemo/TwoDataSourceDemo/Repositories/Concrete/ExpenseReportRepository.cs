﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoDataSourceDemo.Models;

namespace TwoDataSourceDemo.Repositories.Concrete
{
	public class ExpenseReportRepository : IExpenseReportRepository
	{
		#region IExpenseReportRepository Members

		public ExpenseReport Get(int id)
		{
			ExpenseReport er;
			using (EFDataContext cntx = new EFDataContext())
			{ 
				er = cntx.ExpenseReports.FirstOrDefault(e => e.Id == id);
			}
			if (er != null)
			{
				OuterExpenseReport outerER;
				if (_outerDataSource.TryGetValue(id, out outerER))
				{
					er.SpecInstruct = outerER.SpecInstruct;	
				}
			}

			return er;
		}

		public bool Save(ExpenseReport er)
		{
			if (er == null)
				throw new ArgumentNullException("er");

			EntityState state = er.Id > 0 ? EntityState.Modified : EntityState.Added;
			//TODO: It seems it is impossible to perform outer and local updates in transaction

			OuterExpenseReport outerER;
			_outerDataSource.TryGetValue(er.Id, out outerER);
			if (state == EntityState.Added)
			{ 
				if (outerER != null)
					throw new InvalidOperationException();

				_outerDataSource.Add(er.Id, new OuterExpenseReport() {Id = er.Id, SpecInstruct = er.SpecInstruct });
			}
			else 
			{
				if (outerER == null)
					throw new InvalidOperationException();

				outerER.SpecInstruct = er.SpecInstruct;
			}

			using (EFDataContext cntx = new EFDataContext())
			{
				cntx.Entry(er).State = state;
				cntx.SaveChanges();
			}

			return true;
		}

		public ICollection<ExpenseReport> GetExpenseReport(int foundationId)
		{
			throw new NotImplementedException();
		}

		#endregion


		private static Dictionary<int, OuterExpenseReport> _outerDataSource = new Dictionary<int, OuterExpenseReport>() { 
			{ 71, new OuterExpenseReport() { Id = 71, SpecInstruct = "Instruction From Outer DataSource" } },
			{ 68, new OuterExpenseReport() { Id = 68, SpecInstruct = "Another Instruction From Outer DataSource" } }
		};

	
		private class OuterExpenseReport
		{
			public int Id;
			public string SpecInstruct;
		}
		
	}
}
