﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoDataSourceDemo.Models;
using TwoDataSourceDemo.Repositories.Concrete.Mapping;

namespace TwoDataSourceDemo.Repositories.Concrete
{
	public class EFDataContext: DbContext
	{
		static EFDataContext()
		{
			Database.SetInitializer<EFDataContext>(null);
		}

		public DbSet<ExpenseReport> ExpenseReports { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new ExpenseReportMap());
		}
	}
}
