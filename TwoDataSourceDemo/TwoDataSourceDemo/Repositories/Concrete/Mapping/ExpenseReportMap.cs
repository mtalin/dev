﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoDataSourceDemo.Models;

namespace TwoDataSourceDemo.Repositories.Concrete.Mapping
{
	public class ExpenseReportMap : EntityTypeConfiguration<ExpenseReport>
	{
		public ExpenseReportMap()
		{
			// Primary Key
			this.HasKey(t => t.Id);

			// Properties
			//this.Property(t => t.SpecInstruct)
			//	.HasMaxLength(200);

			// Table & Column Mappings
			this.ToTable("ExpenseReports");
			this.Property(t => t.Id).HasColumnName("Id");
			this.Property(t => t.Foundation).HasColumnName("Foundation");
			this.Property(t => t.ReimbursableTo).HasColumnName("ReimbursableTo");
			this.Property(t => t.ReimbursableContactInfo).HasColumnName("ReimbursableContactInfo");
			this.Property(t => t.Report).HasColumnName("Report");
			this.Property(t => t.Coversheet).HasColumnName("Coversheet");
			//this.Property(t => t.SpecInstruct).HasColumnName("SpecInstruct");
			this.Property(t => t.IsSentEmail).HasColumnName("IsSentEmail");
			this.Property(t => t.DtCreated).HasColumnName("DtCreated");
			this.Property(t => t.Author).HasColumnName("Author");
			this.Property(t => t.Step).HasColumnName("Step");
			//this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
		}
	}
}
