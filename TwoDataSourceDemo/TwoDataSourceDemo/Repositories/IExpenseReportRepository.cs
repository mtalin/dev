﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoDataSourceDemo.Models;

namespace TwoDataSourceDemo.Repositories
{
	public interface IExpenseReportRepository
	{
		ExpenseReport Get(int id);
		
		bool Save(ExpenseReport er);

		ICollection<ExpenseReport> GetExpenseReport(int foundationId);
	}
}
