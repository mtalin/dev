﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using TwoDataSourceDemo.Models;
using TwoDataSourceDemo.Repositories;
using TwoDataSourceDemo.Repositories.Concrete;

namespace TwoDataSourceDemo
{
	class Program
	{
		static void Main(string[] args)
		{
			//ExpenseReport.Amount is managed by EF, ExpenseReport.SpecInstruct is managed by outer dataSource
			//Project just show how we can implement several datasource that is transparent in our ExpenseReport domain class.
			//Notice that ExpenseReport.SpecInstruct hasn't any EF mapping.


			var rep = new ExpenseReportRepository();
			ExpenseReport er = rep.Get(71);

			Console.WriteLine("ID:{0}; Amount:{1} SpecInstr:[{2}]", er.Id, er.TotalAmount, er.SpecInstruct);

			er.SpecInstruct = "Yo! yo!";

			Console.Write("Please input new Amount");
			er.TotalAmount = decimal.Parse(Console.ReadLine());
			rep.Save(er);

			er = rep.Get(71);
			Console.WriteLine("ID:{0}; Amount:{1} SpecInstr:[{2}]", er.Id, er.TotalAmount, er.SpecInstruct);

		}
	}
}
