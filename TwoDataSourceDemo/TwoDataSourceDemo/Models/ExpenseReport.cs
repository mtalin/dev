﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDataSourceDemo.Models
{
	public class ExpenseReport //: IEntity
	{
		public ExpenseReport()
		{
			//this.Expenses = new List<Expens>();
		}

		public int Id { get; private set; }
		public int Foundation { get; set; }
		public int ReimbursableTo { get; set; }
		public Nullable<int> ReimbursableContactInfo { get; set; }
		public Nullable<int> Report { get; set; }
		public Nullable<int> Coversheet { get; set; }
		public string SpecInstruct { get; set; }
		public bool IsSentEmail { get; set; }
		public System.DateTime DtCreated { get; set; }
		public int Author { get; set; }
		public int Step { get; set; }
		public Nullable<decimal> TotalAmount { get; set; }
		/*
		public virtual Document1 Document { get; set; }
		public virtual Document1 Document1 { get; set; }
		public virtual Person Person { get; set; }
		public virtual Foundation Foundation1 { get; set; }
		public virtual PostalAddress PostalAddress { get; set; }
		public virtual PayeeProfile PayeeProfile { get; set; }
		public virtual ICollection<Expens> Expenses { get; set; }
		 */
	}
}
