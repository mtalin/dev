﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Lexxys;
using Lexxys.Data;

namespace EmailTemplates
{
	class Program
	{
		static void Main(string[] args)
		{
			var list = Dc.GetList(new {Id = 0, Body = ""}, @"
				select ID, Body 
				from EmailTemplates 
				where Body is not null --and ID = 634 --63 -- 634 --324
				order by ID");

			foreach (var item in list)
			{
				Console.WriteLine($"{item.Id}; ");
				const string name = @"/body";
				const string characters = @"*\`#{}+!_";

				var itemBody = item.Body;
				var body = "";

				bool expr = false;
				for (int i = 0; i<itemBody.Length; i++)
				{
					var c = itemBody[i];
					if (i < itemBody.Length - 1 && itemBody.Substring(i, 2).Equals("{.", StringComparison.Ordinal))
					{
						expr = true;
						body += "{.";
						i++;
						continue;
					}
					if (i < itemBody.Length - 1 && itemBody.Substring(i, 2).Equals(".}", StringComparison.Ordinal))
					{
						expr = false;
						body += ".}";
						i++;
						continue;
					}

					if (i < itemBody.Length - 4 && itemBody.Substring(i, 4).Equals("http", StringComparison.Ordinal))
					{
						var m = Regex.Match(itemBody.Substring(i), @"^http([s]??)://(.*)[\s\r\n]");
						if (m.Success)
						{
							i += m.Value.Length - 1;
							body += m.Value;
							continue;
						}
					}

					if (!expr && characters.Contains(itemBody[i]))
						body += @"\" + itemBody[i];
					else 
						body += itemBody[i];
				}
				body = Regex.Replace(body, @"\{\. *= *", @"{.");
				body = Regex.Replace(body, "\r\n[ \t]*--[ \t]*", "\r\n* ", RegexOptions.Singleline);
				body = Regex.Replace(body, "\r\n[ \t]*-[ \t]*", "\r\n* ", RegexOptions.Singleline);

				var id = Dc.GetValue<int>($"select Id from EmailTemplatePlaceholders where EmailTemplate = {item.Id} and Name = '/body'");
				
				var sql = id > 0 
					? $"update EmailTemplatePlaceholders set Value = '{body.Replace("'", "''")}' where ID = {id}"
					: $"insert into EmailTemplatePlaceholders (EmailTemplate, Name, Value) values ({item.Id}, '{name}', '{body.Replace("'", "''")}')";
				Dc.Execute(sql);
				Dc.Execute($"update EmailTemplates set Template = 'Basic' where ID = {item.Id}");
			}
		}
	}
}
