﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FoundationSource.Admin;
using FoundationSource.Admin.CPBase;
using Lexxys;
using Lexxys.Data;

namespace RestoreDocLocation
{

	class Program
	{

		static async Task Main(string[] args)
		{

			var location = Application.Config.Data.LocalStore.FirstOrDefault(o => o.Domain == "Documents").Path;
			if (location == null || !Directory.Exists(location))
				throw new InvalidOperationException("Cannot find Documents location");
			var path = Path.GetFullPath(location);
			await Collect(path, path[path.Length - 1] == Path.DirectorySeparatorChar ? location: location + Path.DirectorySeparatorChar);
		}

		private static async Task Collect(string location, string root)
		{
			foreach (var file in Directory.EnumerateFiles(location))
			{
				await UpdateDocument(file, root);
			}
			foreach (var directory in Directory.EnumerateDirectories(location))
			{
				await Collect(directory, root);
			}
		}

		private static async Task UpdateDocument(string file, string root)
		{
			var fi = new FileInfo(file);
			int length = (int)fi.Length;
			string name = fi.Name;
			int i = name.IndexOf('-');
			if (i < 0)
				i = name.IndexOf('.');
			if (i > 0)
				name = name.Substring(0, i);
			int id = name.AsInt32(0);
			if (id <= 0)
			{
				Console.WriteLine($"{file} -- Unknown file");
				return;
			}

			await Dc.ExecuteAsync(@"update Documents set StorageLocation=@P, FileLength=@L where ID=@I",
				Dc.Parameter("@P", file.Substring(root.Length)),
				Dc.Parameter("@L", length),
				Dc.Parameter("@I", id));
			Log();
		}

		private static void Log()
		{
			if (Interlocked.Increment(ref _count) % 10000 == 0)
				Console.WriteLine($"{_count} items have been items processed.");
		}

		private static int _count;
	}
}
