while (@@trancount > 0) rollback
go
use CharityPlanner

--- do not modify
begin tran; set nocount on; set ansi_nulls on; set quoted_identifier on;
go
declare @seed varchar(9);
---
set @seed = 1000;


/* part 1. Alter table structure */

if (type_id('int') = (select system_type_id from sys.columns where object_id = object_id('Xxx') and name = 'x'))
	goto err;

alter table Xxx alter
	column x int;
if (@@error <> 0) begin set @seed = 1010; goto err; end;


--- do not modify
return;err:if(@@trancount>0)rollback;raiserror(14661,11,127,@seed);return;
go
declare @seed varchar(9);if(@@error not in(0,14661))begin set @seed=-1;goto err;end;if(@@trancount=0)return;
---
set @seed = 2000;


/* part 2. Update table data */

update Xxx
set x = 99
if (@@error <> 0) begin set @seed = 2010; goto err; end;

insert into Xxx(x)
values (91)
if (@@error <> 0) begin set @seed = 2020; goto err; end;


--- do not modify
return;err:if(@@trancount>0)rollback;raiserror(14661,11,127,@seed);return;
go
if(@@error not in(0,14661))begin if(@@trancount>0)rollback;raiserror(14661,11,127,'-1');end;
if(@@trancount>0)commit;
go
