while (@@trancount > 0) rollback
go
use CharityPlanner

--- do not modify
begin tran set nocount on set ansi_nulls on set quoted_identifier on
---

go
/*
	<schema_name, sysname, dbo>.<proc_name, sysname, ProcName>
	Query persons of specified registration step

	parameters:
		@step	- RegistrationStep value to be used if the query condition (default 0)

	returns:
		ID and Name of persons with registration step = @step
*/
create or alter procedure <schema_name, sysname, dbo>.<proc_name, sysname, ProcName>
	(
	@step as int = 0
	)
as
	begin
	select ID, FirstName + ' ' + LastName
	from Persons
	where RegistrationStep = @step
	end
go

--- do not modify
if(@@error<>0)begin if(@@trancount>0)rollback;raiserror(14661,11,127,'create procedure');end;
if(@@trancount>0)commit;
go
