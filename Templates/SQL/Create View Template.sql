while (@@trancount > 0) rollback
go
use CharityPlanner

--- do not modify
begin tran set nocount on set ansi_nulls on set quoted_identifier on
---

go
create or alter view <schema_name, sysname, dbo>.<view_name, sysname, ViewName>
as
	select ID, FirstName + ' ' + LastName
	from Persons
	where RegistrationStep = 0
go

--- do not modify
if(@@error<>0)begin if(@@trancount>0)rollback;raiserror(14661,11,127,'create view');end;
if(@@trancount>0)commit;
go
