while (@@trancount > 0) rollback
go
use CharityPlanner

--- do not modify
begin tran set nocount on set ansi_nulls on set quoted_identifier on
---

go
/*
	<schema_name, sysname, dbo>.<func_name, sysname, ProcName>
	Query number of persons of specified registration step

	parameters:
		@step	- RegistrationStep value to be used if the query condition (default 0)

	returns:
		number of persons of specified registration step
*/
create or alter function <schema_name, sysname, dbo>.<func_name, sysname, FuncName>
	(
	@step as int = 0
	)
returns int
	begin
	return (select count(ID) from Persons where RegistrationStep = @step)
	end
go

--- do not modify
if(@@error<>0)begin if(@@trancount>0)rollback;raiserror(14661,11,127,'create function');end;
if(@@trancount>0)commit;
go
