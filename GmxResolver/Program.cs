﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace GmxResolver
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var client = new Auth.AuthClient())
			{
				string host = GetServiceRoot(client.Endpoint.Address.Uri);

				string user = ConfigurationManager.AppSettings["login_name"];
				string password = ConfigurationManager.AppSettings["login_password"];
				string sessionId = client.LogOn(user, password);
				if (String.IsNullOrEmpty(sessionId))
				{
					WriteErrorFatal($"Cannot get sessionId for user <{user}>.");
				}
				else
				{
					Console.WriteLine("Host      : " + host);
					Console.WriteLine("Session id: " + sessionId);
					Console.WriteLine();

					while (true)
					{
						int fndId = 0;
						while (true)
						{
							Console.Write("Enter foundation id: ");
							string x = Console.ReadLine();
							if (Int32.TryParse(x, out fndId) && fndId > 0)
								break;

							WriteBad();
							Console.WriteLine();
						}
						WriteGood();
						Console.WriteLine();
						Console.WriteLine();

						client.ChangeFoundation(sessionId, fndId);

						string token = client.GetAspKeyToken(sessionId);
						if (String.IsNullOrEmpty(sessionId))
						{
							WriteErrorFatal("Cannot get token.");
						}
						else
						{
							Console.WriteLine("What we gonna do?:");
							while (true)
							{
								Console.WriteLine("- [1] Generate links");
								Console.WriteLine("- [2] Create HH grant");

								ConsoleKeyInfo x = Console.ReadKey(true);
								if (Char.IsDigit(x.KeyChar))
								{
									if (Int32.TryParse(x.KeyChar.ToString(), out int z))
									{
										if (z == 2)
										{
											Console.WriteLine();
											ProcessHouseholdGrant(sessionId, fndId);
											break;
										}
										else if (z == 1)
										{
											Console.WriteLine();
											ProcessFsolLinks(host, token);
											break;
										}
									}
								}

								WriteBad();
								Console.WriteLine();
							}

							Console.WriteLine();
							Console.WriteLine(new String('.', Console.WindowWidth));
						}
					}
				}
			}
		}

		private static void ProcessFsolLinks(string host, string token)
		{
			Console.WriteLine("What FSOL link do you need?:");
			while (true)
			{
				Console.WriteLine("- [1] Tax Center");
				Console.WriteLine("- [2] 1% Calculator");
				Console.WriteLine("- [3] Questionnaire");

				ConsoleKeyInfo x = Console.ReadKey(true);
				if (Char.IsDigit(x.KeyChar))
				{
					if (Int32.TryParse(x.KeyChar.ToString(), out int z) && z > 0 && z <= _links.Length)
					{
						Console.WriteLine();
						Console.WriteLine(String.Format(_links[z - 1], host, token));
						break;
					}
				}

				WriteBad();
				Console.WriteLine();
			}
		}

		private static void ProcessHouseholdGrant(string sessionId, int fndId)
		{
			ReadInt("Enter HH id:", out int hhId);
			using (var grantClient = new Grant.GrantClient())
			{
				try
				{
					var grantDetailsInfo = new Grant.GrantDetailsInfo()
					{
						FoundationId = fndId,
						HouseHoldAccountId = hhId
					};
					using (var client = new HttpClient())
					{
						client.DefaultRequestHeaders.Add("sessionId", sessionId);
						var response = client.GetAsync(ConfigurationManager.AppSettings["gmxapi_root"] + "/household/application/" + Uri.EscapeDataString(hhId.ToString())).Result;
						if (response.StatusCode != HttpStatusCode.OK)
							WriteErrorFatal("Cannot get household application info.");

						dynamic json = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);
						var attributes = json.data.attributes;
						grantDetailsInfo.Amount = attributes["requested-amount"];
						grantDetailsInfo.AuthorId = attributes["authorized-person-id"];
						grantDetailsInfo.OrgName = GetApplicationName((DateTime?)attributes["authorized-date"], (string)attributes.address.city, (string)attributes.address.state, (string)attributes.address.country);
						grantDetailsInfo.RecipientType = 1/*GrantRecipientType.Household*/;
						grantDetailsInfo.Anonymous = false;
					}

					var grantDetails = grantClient.AddGrantDetails(sessionId, grantDetailsInfo);
					grantClient.CreatePayment(sessionId, grantDetails.Id, 0);

					WriteGood();
				}
				catch (Exception ex)
				{
					WriteErrorFatal(ex.Message);
				}

			}
		}

		private static string GetApplicationName(DateTime? dateAuthorized, string city, string state, string country)
		{
			return $"Created from API ({dateAuthorized:MM/dd/yyyy}) - {city}, {(state == null ? "" : state + ", ")}{country}";
		}

		private static void ReadDecimal(string text, out decimal value)
		{
			while (true)
			{
				Console.WriteLine(text);
				string x = Console.ReadLine();
				if (Decimal.TryParse(x, out value))
					return;

				WriteBad();
				Console.WriteLine();
			}
		}
		private static void ReadString(string text, out string value)
		{
			while (true)
			{
				Console.WriteLine(text);
				string x = Console.ReadLine();
				if (!String.IsNullOrEmpty(x))
				{
					value = x;
					return;
				}

				WriteBad();
				Console.WriteLine();
			}
		}
		private static void ReadInt(string text, out int value)
		{
			while (true)
			{
				Console.WriteLine(text);
				string x = Console.ReadLine();
				if (Int32.TryParse(x, out value))
					return;

				WriteBad();
				Console.WriteLine();
			}
		}

		private static string[] _links = new[]
		{
			"{0}/fsol/taxCenter?_tk={1}",
			"{0}/fsol/taxCenter/onePercentCalc?_tk={1}",
			"{0}/fsol/questionnaire/edit?_tk={1}",
		};

		private static void WriteErrorFatal(string message)
		{
			WriteBad();
			Console.Write(' ');
			Console.WriteLine(message);
			WritePressAnyKey();
			Console.ReadKey(true);
		}

		private static void WritePressAnyKey()
		{
			Console.WriteLine("Press any key to continue...");
		}

		private static void WriteBad()
		{
			ConsoleColor oldColor = Console.ForegroundColor;
			try
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.Write(":(");
			}
			finally
			{
				Console.ForegroundColor = oldColor;
			}
		}
		private static void WriteGood()
		{
			ConsoleColor oldColor = Console.ForegroundColor;
			try
			{
				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.Write(":)");
			}
			finally
			{
				Console.ForegroundColor = oldColor;
			}
		}

		private static string GetServiceRoot(Uri uri)
		{
			return uri.Scheme + Uri.SchemeDelimiter + uri.Host + (!uri.IsDefaultPort ? ":" + uri.Port : null);
		}
	}
}
