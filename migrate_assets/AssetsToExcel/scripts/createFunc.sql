create function x(@value varchar(100), @pattern varchar(50)) returns bit
as 
begin
	declare @divs varchar(50) = '[ ,:."''!/()?\_-]'
	declare @escape char(1) = '\'
	declare @result bit = 0

	if (
		@value like @pattern or
		@value like @pattern + @divs + '%' ESCAPE @escape or
		@value like '%' + @divs + @pattern + @divs + '%' ESCAPE @escape or
		@value like '%' + @divs + @pattern ESCAPE @escape
	)
		set @result = 1;
	return @result;
end
