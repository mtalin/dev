#!/usr/bin/env python3.7

from cx_Freeze import setup, Executable

setup(
    name = "AssetsToExcel",
    version = "0.1",
    description = "AssetsToExcel",
    executables = [Executable("AssetsToExcel.py")]
)