#!/usr/bin/env python3.7

import sys

import pyodbc 
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl.worksheet.datavalidation import DataValidation
from openpyxl.styles import PatternFill

if len(sys.argv) == 4:
	server, user, pwd = sys.argv[1:]
else:
    #server, user, pwd = (r'BLUE\C', 'sa', 'post+Office2')
    print('\nUsage: AssetsToExcel <server> <userid> <password>\n')
    input("Press Enter\n")
    sys.exit()

conn = pyodbc.connect(r'Driver={SQL Server};Server=' + server + ';Database=CharityPlanner;UID=' + user + ';PWD=' + pwd + ';')
cursor = conn.cursor()

wb = Workbook()
ws = wb.active
ws.title = "Assets"

ws = wb.create_sheet("Types")

refCol = 1 #Types!A
refRow = 1
refLetter1 = get_column_letter(refCol)
refLetter2 = get_column_letter(refCol + 1)
refLetter3 = get_column_letter(refCol + 2)

ws[f'{refLetter1}{refRow}'] = 'Type'
ws[f'{refLetter2}{refRow}'] = 'Type Name'
ws[f'{refLetter3}{refRow}'] = 'Type Abbrev'

refStart = refRow + 1
fill = PatternFill("solid", fgColor="DDDDDD")
assTypes = {
    6: ['Partnership', 'Partnership'], #11
    7: ['Life Insurance', 'Life ins.'], #9
    8: ['Real Property buildings', 'R-prop. bld.'], #12
    9: ['Other Alternative Asset', 'Other'], #5
    14: ['Program Related Investments - to public charity', 'PRI public'], #10
    15: ['Accounts Receivable', 'Acc. rcv.'], #9
    16: ['Loans Receivable', 'Loans rcv.'], #10
    18: ['Note Receivable', 'Note rcv.'], #9
    19: ['Other notes payable', 'Other notes'], #11
    22: ['Other Alternative Assets (Multiple Units)', 'Other mult.'], #11
    26: ['Mortgage Loan Receivable', 'Mortgage loan rcv.'], #18
    28: ['Program Related Investments - to non-public charity', 'PRI non-public'], #14
    31: ['Annuity', 'Annuity'], #7
    33: ['Inventory', 'Inventory'], #9
    35: ['Real Property Land', 'R-prop. land'], #12
    36: ['Real Property improvements', 'R-prop. imp.'], #12
    37: ['Collectible', 'Collectible'], #11
    38: ['Intangible - Amortizied', 'I.A. amrt.'], #10
    39: ['Intangible - Not Amortizied', 'I.A. not amrt.'], #14
    40: ['Personal Property', 'Personal prop.'], #14
    41: ['Loans to officers & Directors, Trustee', 'Loans'], #5
    42: ['Security deposit', 'Sec. dep.'], #9
} 
atSorted = sorted(assTypes.items(), key = lambda x:x[1][1].lower())

for item in atSorted: 
    refRow += 1
    c = refCol
    ws[get_column_letter(c) + str(refRow)] = item[0]
    ws[get_column_letter(c) + str(refRow)].fill = fill
    for field in item[1]:
        c += 1
        ws[get_column_letter(c) + str(refRow)] = field
        ws[get_column_letter(c) + str(refRow)].fill = fill

ws.column_dimensions[refLetter2].width = max((len(cell.value) if cell.value else 0) for cell in ws[refLetter2])
ws.column_dimensions[refLetter3].width = max((len(cell.value) if cell.value else 0) for cell in ws[refLetter3])

dv = DataValidation(type="list", formula1='Types!$' + refLetter3 + '$' + str(refStart) + ':$' + refLetter3 + '$' + str(refRow), allow_blank=False)
dv.error ='Your entry is not in the list'
dv.errorTitle = 'Invalid Entry'
dv.prompt = 'Please select from the list'
dv.promptTitle = 'List Selection'

ws = wb["Assets"]

FoundationId, Foundation, Financial_Analyst, Id, Name, Type, Type_Abbrev, New_Type, New_Type_Abbrev, Date_Sold, Fiscal_Year = 'ABCDEFGHIJK'
#a            b           c                  d   e     f     g            h         i                j          k

ws[f'{FoundationId}1'] = 'FS Account'
ws[f'{Foundation}1'] = 'Foundation'
ws[f'{Financial_Analyst}1'] = 'Financial Analyst'
ws[f'{Id}1'] = 'Id'
ws[f'{Name}1'] = 'Name'
ws[f'{Type}1'] = 'Type'
ws[f'{Type_Abbrev}1'] = 'Type Abbrev'
ws[f'{New_Type}1'] = 'New Type'
ws[f'{New_Type_Abbrev}1'] = 'New Type Abbrev'
ws[f'{Date_Sold}1'] = 'Date Sold'
ws[f'{Fiscal_Year}1'] = 'Fiscal Year End'

try:
    with open('scripts/dropFunc.sql', 'r') as dropFunc:
        sql = dropFunc.read() 
    cursor.execute(sql)
    with open('scripts/createFunc.sql', 'r') as createFunc:
        sql = createFunc.read() 
    cursor.execute(sql)

    with open('scripts/mainScript.sql', 'r') as mainScript:
        sql = mainScript.read() 
    cursor.execute(sql)

    r = 1
    for row in cursor: #.fetchall():
        r += 1
        ws[f'{FoundationId}{r}'].value = row.FoundationId
        ws[f'{Foundation}{r}'].value = row.Foundation
        ws[f'{Financial_Analyst}{r}'].value = row.FinAnalyst
        ws[f'{Id}{r}'].value = row.AssetId
        ws[f'{Name}{r}'].value = row.AssetName

        ws[f'{Type}{r}'].value = row.Type
        ws[f'{Type_Abbrev}{r}'].value = assTypes[row.Type][1] #abbrev

        ws[f'{New_Type}{r}'].value = f'=LOOKUP({New_Type_Abbrev}{r}, Types!{refLetter3}{refStart}:{refLetter3}{refRow}, Types!{refLetter1}{refStart}:{refLetter1}{refRow})'
        ws[f'{New_Type_Abbrev}{r}'].value = assTypes[row.TypeNew][1] #abbrev
        ws[f'{Date_Sold}{r}'].value = row.DtSoldOut
        ws[f'{Fiscal_Year}{r}'].value = row.FiscalYear
        dv.add(ws[f'{New_Type_Abbrev}{r}'])

        print(row.AssetId)

    ws.column_dimensions[f'{Id}'].hidden = True
    ws.column_dimensions[f'{Type}'].hidden = True
    ws.column_dimensions[f'{New_Type}'].hidden = True

    ws.column_dimensions[f'{Foundation}'].width = max((len(cell.value) if cell.value else 0) for cell in ws[f'{Foundation}'])
    ws.column_dimensions[f'{Financial_Analyst}'].width = max((len(cell.value) if cell.value else 0) for cell in ws[f'{Financial_Analyst}'])
    ws.column_dimensions[f'{Name}'].width = max((len(cell.value) if cell.value else 0) for cell in ws[f'{Name}'])
    ws.column_dimensions[f'{Type_Abbrev}'].width = max((len(cell.value) if cell.value else 0) for cell in ws[f'{Type_Abbrev}'])
    ws.column_dimensions[f'{New_Type_Abbrev}'].width = max((len(cell.value) if cell.value else 0) for cell in ws[f'{New_Type_Abbrev}'])
    ws.column_dimensions[f'{Date_Sold}'].width = max((len(cell.value) if cell.value else 0) for cell in ws[f'{Date_Sold}'])

    ws.add_data_validation(dv)
    ws.freeze_panes = 'A2'

    wb.save('out.xlsx')
finally:
    with open('scripts/dropFunc.sql', 'r') as dropFunc:
        sql = dropFunc.read() 
    cursor.execute(sql)

print('Export Finished')
input('Press Enter')

