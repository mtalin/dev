#!/usr/bin/env python3.7

from cx_Freeze import setup, Executable

build_exe_options = {"include_msvcr": True}

setup(
    name = "AssetsFromExcel",
    version = "0.1",
    description = "AssetsFromExcel",
    options = {"build_exe": build_exe_options},
    executables = [Executable("AssetsFromExcel.py")]
)