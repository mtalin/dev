#!/usr/bin/env python3.7

import sys
import datetime

import pyodbc 
from openpyxl import Workbook
from openpyxl import load_workbook

logFileName = 'AssetsFromExcel_Log.txt'

def write_log(message):
	with open(logFileName, 'a') as log:
		log.writelines(f'{message}\n')
		print(message)

if len(sys.argv) == 4:
	server, user, pwd = sys.argv[1:]
else:
    #server, user, pwd = (r'BLUE\C', 'sa', 'post+Office2')
    print('\nUsage: AssetsToExcel <server> <userid> <password>\n')
    input("Press Enter\n")
    sys.exit()

write_log(f'\nUpdate started: {datetime.datetime.now()}')

listErr = []
try:
	file = 'out.xlsx'
	wb = load_workbook(filename = file, data_only=True)

	ws = wb['Types']
	TYPE_ID, TYPE_NAME, TYPE_ABBREV = range(3)

	assetTypes = {}
	for row in ws.rows:
		if not row[TYPE_ABBREV].value or type(row[TYPE_ABBREV].value) is not str or row[TYPE_ABBREV].value == 'Type Abbrev':
			continue
		assetTypes[row[TYPE_ABBREV].value.lower()] = (row[TYPE_ID].value, row[TYPE_NAME].value)

	ws = wb['Assets']
	FND_ID, FND, F_ANALYST, ASSET_ID, NAME, TYPE, TYPE_ABBREV, NEW_TYPE, NEW_TYPE_ABBREV, DATE_SOLD, FISCAL_YEAR_END = range(11)

	listUpdate = []
	for row in ws.rows:
		asset_id = row[ASSET_ID].value
		if not asset_id or type(asset_id) is not int:
			continue
		fnd_id = row[FND_ID].value
		fnd = row[FND].value
		f_analyst = row[F_ANALYST].value
		name = row[NAME].value	
		oldType = row[TYPE].value
		oldTypeAbbrev = row[TYPE_ABBREV].value
		newTypeAbbrev = row[NEW_TYPE_ABBREV].value
		if not newTypeAbbrev or type(newTypeAbbrev) is not str or newTypeAbbrev.lower() not in assetTypes:
			listErr.append(f'KeyError: "{newTypeAbbrev}" is wrong New Type Abbrev; Asset ID: {asset_id}, Asset Name: "{name}"')
			continue
		newType = assetTypes[newTypeAbbrev.lower()][TYPE_ID] 
		date_sold = row[DATE_SOLD].value
		fye = row[FISCAL_YEAR_END].value

		if oldType != newType:
			listUpdate.append(f"update Assets set Type = {newType} where ID = {asset_id}; -- OldType = {oldType}")

	conn = pyodbc.connect(r'Driver={SQL Server};Server=' + server + ';Database=CharityPlanner;UID=' + user + ';PWD=' + pwd + ';')
	cursor = conn.cursor()

	cnt = 0
	for upd in listUpdate:
		try:
			cursor.execute(upd)
			conn.commit()
			print(upd)
			cnt += 1
		except DatabaseError as e:
			listErr.append(f'Database Error: {e}\n\t{upd}')
	write_log(f'\n{cnt} assets were updated')

except Exception as e:
	listErr.append(f'Error: {e}\n')

print()

write_log('\nERRORS:')
for err in listErr:
	write_log(f'{err}')

print()
write_log(f'\nUpdate finished: {datetime.datetime.now()}')
write_log('-'*80)
print(f'See file {logFileName}')
input('Press Enter')
