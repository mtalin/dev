set nocount on;

exec drop_func 'x'
go
create function x(@value varchar(100), @pattern varchar(50)) returns bit
as 
begin
	declare @divs varchar(50) = '[ ,:."''!/()?\_-]'
	declare @escape char(1) = '\'
	declare @result bit = 0

	if (
		@value like @pattern or
		@value like @pattern + @divs + '%' ESCAPE @escape or
		@value like '%' + @divs + @pattern + @divs + '%' ESCAPE @escape or
		@value like '%' + @divs + @pattern ESCAPE @escape
	)
		set @result = 1;
	return @result;
end
go

declare @minSoldOutYear int = 2019
declare @x as table(id int, [Name] varchar(100), [Type] int, TypeNew int, FoundationId int, Foundation varchar(100), FinAnalyst varchar(200), DtSoldOut date, FiscalYear date)
insert into @x
select
	x.AssetId,
	x.AssetName,
	x.AssetType,
	x.AssetType,
	x.FoundationId,
	x.FoundationName,
	x.FinancialAnalyst,
	x.DtSoldOut,
	dbo.get_FiscalYearEndFromDate(x.FoundationId, getdate()) FiscalYear
from
(
	select 
		a.ID AssetId, a.Name AssetName, a.Type AssetType, f.ID FoundationId, f.Name FoundationName, fa.FullName FinancialAnalyst,
		case when not exists (select * from dbo.get_AssetsBalanceDetails(la.Foundation, getdate()) where Asset = la.ID)
			then (select max(DtClosed) from Holdings where Foundation = f.ID and Asset = la.ID)
			else null end DtSoldOut
	from Assets a
		join enums_AssetTypes atp on atp.ItemID = a.Type
		join AssetBooks ab on ab.Id = a.id
		join (select AssetBook, min(DtCreated) DtCreated from AssetValuations group by AssetBook) av on av.AssetBook = ab.ID
		join LocalAssets la on la.Id = a.id
		join vActiveFoundations f on f.ID = la.Foundation
		join vFinancialAnalystsFoundations fa on fa.Foundation = la.Foundation
	where a.Type in (9, 22)	/* Other Alternative Assets */
) x
where x.DtSoldOut is null or x.DtSoldOut >= dbo.get_FiscalYearStartFromYear(x.FoundationId, @minSoldOutYear)


declare @y as table(id int, [Name] varchar(100), [Type] int, TypeNew int, FoundationId int, Foundation varchar(100), FinAnalyst varchar(200), DtSoldOut date, FiscalYear date)
insert into @y
select
	x.AssetId,
	x.AssetName,
	x.AssetType,
	x.AssetType,
	x.FoundationId,
	x.FoundationName,
	x.FinancialAnalyst,
	x.DtSoldOut,
	dbo.get_FiscalYearEndFromDate(x.FoundationId, getdate()) FiscalYear
from (
	select 
		a.ID AssetId, a.Name AssetName, a.Type AssetType, f.ID FoundationId, f.Name FoundationName, fa.FullName FinancialAnalyst,
		case when not exists (select * from dbo.get_AssetsBalanceDetails(la.Foundation, getdate()) where Asset = la.ID)
			then (select max(DtClosed) from Holdings where Foundation = f.ID and Asset = la.ID)
			else null 
		end DtSoldOut
	from Assets a
		join enums_AssetTypes atp on atp.ItemID = a.Type
		join AssetBooks ab on ab.Id = a.id
		join (select AssetBook, min(DtCreated) DtCreated from AssetValuations group by AssetBook) av on av.AssetBook = ab.ID
		join LocalAssets la on la.Id = a.id
		join vActiveFoundations f on f.ID = la.Foundation
		join vFinancialAnalystsFoundations fa on fa.Foundation = la.Foundation
	where a.Type not in (9, 22)	/* Other Alternative Assets */ and a.Type <> 6 /* Partnership */
) x
where x.DtSoldOut is null or x.DtSoldOut >= dbo.get_FiscalYearStartFromYear(x.FoundationId, @minSoldOutYear)


update @x
	set TypeNew = 40 /* Personal property */
from @x a
where dbo.x(a.[Name], 'laptop')=1 or dbo.x(a.[Name], 'computer')=1 or dbo.x(a.[Name], 'macbook')=1 or dbo.x(a.[Name], 'furniture')=1 or dbo.x(a.[Name], 'equipment')=1

update @x
	set TypeNew = 37 /* Collectible */
from @x a
where dbo.x(a.[Name], 'coin')=1 or dbo.x(a.[Name], 'map')=1 or dbo.x(a.[Name], 'artwork')=1 or dbo.x(a.[Name], 'painting')=1 or dbo.x(a.[Name], 'paintings')=1 or dbo.x(a.[Name], 'figure')=1 or dbo.x(a.[Name], 'art')=1 or dbo.x(a.[Name], 'antique')=1 or dbo.x(a.[Name], 'motorcycle')=1

update @x
	set TypeNew = 38 /* Intangible - Amorizied */
from @x a
where dbo.x(a.[Name], 'organizational costs')=1 or dbo.x(a.[Name], 'start-up costs')=1 or dbo.x(a.[Name], 'startup costs')=1

update @x
	set TypeNew = 39 /* Intangible - Not Amorizied */
from @x a
where dbo.x(a.[Name], 'trademark')=1


update @y
	set TypeNew = 35 /* Real Property Land */
from @y a
where dbo.x(a.[Name], 'land')=1 and dbo.x(a.[Name], 'imp')=0 and dbo.x(a.[Name], 'improvements')=0 and dbo.x(a.[Name], 'trust')=0

update @y
	set TypeNew = 35 /* Real Property Land */
from @y a
where dbo.x(a.[Name], 'land')=1 and dbo.x(a.[Name], 'imp')=0 and dbo.x(a.[Name], 'improvements')=0 and dbo.x(a.[Name], 'trust')=0

update @y
	set TypeNew = 36 /* Real Property improvements */
from @y a
where dbo.x(a.[Name], 'land')=1 and (dbo.x(a.[Name], 'imp')=1 or dbo.x(a.[Name], 'improvements')=1)

select 
	t.id as AssetId, t.Name as AssetName, t.Type, t.TypeNew, t.Status, t.FoundationId, t.Foundation, t.FinAnalyst, t.DtSoldOut, t.FiscalYear
from (
	select
		t.id, t.[Name], t.[Type], t.TypeNew, case when t.[type] <> t.TypeNew then 'Migrated' else null end [Status], t.FoundationId, t.Foundation, t.FinAnalyst, t.DtSoldOut, t.FiscalYear
	from @x t
	union
	select
		t.id, t.[Name], t.[Type], t.TypeNew, case when t.[type] <> t.TypeNew then 'Migrated' else null end [Status], t.FoundationId, t.Foundation, t.FinAnalyst, t.DtSoldOut, t.FiscalYear
	from @y t
) t
order by t.Foundation, t.[Name]

exec drop_func 'x'