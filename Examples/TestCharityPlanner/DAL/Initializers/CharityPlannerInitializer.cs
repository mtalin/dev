﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using DAL.Entities;

namespace DAL
{
	public class CharityPlannerInitializer : DropCreateDatabaseIfModelChanges<CharityPlannerTestContext>
	{
		protected override void Seed(CharityPlannerTestContext context)
		{

			CreateObjectsForLogin(context);
			CreateUserRo(context);
			AddGrantsToRo(context);
		}

		private void CreateObjectsForLogin(CharityPlannerTestContext context)
		{
			context.Database.ExecuteSqlCommand("create view dbo.Users as select * from CharityPlanner.dbo.Users");
			context.Database.ExecuteSqlCommand("create view dbo.Persons as select * from CharityPlanner.dbo.Persons");
			context.Database.ExecuteSqlCommand("create view dbo.Foundations as select * from CharityPlanner.dbo.Foundations");
			context.Database.ExecuteSqlCommand("create view dbo.vActiveCompanyPersons as select * from CharityPlanner.dbo.vActiveCompanyPersons");
			context.Database.ExecuteSqlCommand("create view dbo.FinancialPartners as select * from CharityPlanner.dbo.FinancialPartners");
			context.Database.ExecuteSqlCommand("create view dbo.StaticPermissions as select * from CharityPlanner.dbo.StaticPermissions");
			context.Database.ExecuteSqlCommand("create view dbo.ServiceUseCases as select * from CharityPlanner.dbo.ServiceUseCases");
			context.Database.ExecuteSqlCommand("create view dbo.ServiceUseCaseToStaticPermissions as select * from CharityPlanner.dbo.ServiceUseCaseToStaticPermissions");
			context.Database.ExecuteSqlCommand("create view dbo.EnumsCategories as select * from CharityPlanner.dbo.EnumsCategories");
			context.Database.ExecuteSqlCommand("create view dbo.Enums as select * from CharityPlanner.dbo.Enums");
			context.Database.ExecuteSqlCommand("create view dbo.LocalAssetsDetails as select * from CharityPlanner.dbo.LocalAssetsDetails");
			context.Database.ExecuteSqlCommand("create view dbo.DonationCertificates as select * from CharityPlanner.dbo.DonationCertificates");
			context.Database.ExecuteSqlCommand("create view dbo.AspSessions as select * from CharityPlanner.dbo.AspSessions");
			context.Database.ExecuteSqlCommand("create schema scm");
			context.Database.ExecuteSqlCommand("create view scm.ObjectsActionsLog as select * from CharityPlanner.scm.ObjectsActionsLog");

			context.Database.ExecuteSqlCommand(@"alter table AssetBooks add constraint DF_AssetBooks_UBTI default 0 for UBTI");
			context.Database.ExecuteSqlCommand(@"alter table AssetBooks add constraint DF_AssetBooks_DtCreated default getdate() for DtCreated");
			context.Database.ExecuteSqlCommand(@"alter table LocalAssets add constraint DF_LocalAssets_InvPercent default 1 for InvPercent");
			context.Database.ExecuteSqlCommand(@"alter table LocalAssets add constraint DF_LocalAssets_ChrPercent default 0 for ChrPercent");
			

			context.Database.ExecuteSqlCommand(
				@"create function [scm].[GetPermissionsGroup](@groupName varchar(50))
				returns xml
				as
				begin
					return CharityPlanner.scm.GetPermissionsGroup(@groupName)
				end");

			context.Database.ExecuteSqlCommand(
				@"create proc scm.GetActionsLog(@CheckPoint int = 0)
				as
				begin
					exec  CharityPlanner.scm.GetActionsLog @CheckPoint
				end");
		}

		private void CreateUserRo(CharityPlannerTestContext context)
		{
			context.Database.ExecuteSqlCommand("create user ro for login ro  WITH DEFAULT_SCHEMA = dbo;");
		}

		private void AddGrantsToRo(CharityPlannerTestContext context)
		{
			context.Database.ExecuteSqlCommand("grant all on users to ro");
			context.Database.ExecuteSqlCommand("grant all on persons to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.Foundations to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.Assets to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.AssetBooks to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.LocalAssets to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.EnumsCategories to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.Enums to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.LocalAssetsDetails to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.vActiveCompanyPersons to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.FinancialPartners to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.DonationCertificates to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.StaticPermissions to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.ServiceUseCases to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.ServiceUseCaseToStaticPermissions to ro");
			context.Database.ExecuteSqlCommand("grant all on dbo.AspSessions to ro");
		}
	}
}
