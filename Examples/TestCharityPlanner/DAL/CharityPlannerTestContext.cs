﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DAL.Entities;

namespace DAL
{
	public class CharityPlannerTestContext:DbContext
	{
		public CharityPlannerTestContext()
		{
		}

		public DbSet<Asset> Assets { get; set; }
		public DbSet<AssetBook> AssetBooks { get; set; }
		public DbSet<LocalAsset> LocalAssets { get; set; }
		//public DbSet<User> Users { get; set; }
		//public DbSet<Person> Persons { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			//base.OnModelCreating(modelBuilder);

			//modelBuilder
			//	.Entity<Person>()
			//	.ToTable("Persons");

			//modelBuilder
			//	.Entity<Person>()
			//	.HasRequired(p => p.User)
			//	.WithMany()
			//	.HasForeignKey(p => p.UserID)
			//	.WillCascadeOnDelete(false);
		}
	}
}
