﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DAL.Entities
{
	public class LocalAsset
	{
		[Key][DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int ID { get; set; }
		public int Foundation { get; set; }
		public Single InvPercent { get; set; }
		public Single ChrPercent { get; set; }
	}
}
