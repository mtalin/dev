﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
	public class Asset
	{
		[Key]
		public int ID { get; set; }
		[Column(TypeName = "varchar")][MaxLength(100)]
		public string Name { get; set; }
		[Column(TypeName = "varchar")][MaxLength(100)]
		public string Name2 { get; set; }
		[Column(TypeName = "varchar")][MaxLength(100)]
		public string Name3 { get; set; }
		[Column(TypeName = "varchar")][MaxLength(100)]
		public string Name4 { get; set; }
		public int Type { get; set; }
	}
}
