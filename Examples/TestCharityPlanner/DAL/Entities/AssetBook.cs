﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DAL.Entities
{
	public class AssetBook
	{

		[Key][DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int ID { get; set; }
		public int Foundation { get; set; }
		[Column(TypeName = "varchar")][MaxLength(250)]
		public string Description { get; set; }
		public bool UBTI { get; set; }
		public int? UBTIDocument { get; set; }
		public DateTime DtCreated { get; set; }
		public DateTime? DtExpired { get; set; }
		public int Author {get; set;}
		public int? UBTINote{get; set;}
	}
}
