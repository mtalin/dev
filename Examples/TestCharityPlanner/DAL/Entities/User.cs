﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
	public class User
	{
		[Key][DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int ID { get; set; }

		[Column(TypeName = "varchar")][MaxLength(20)]
		public string LoginName { get; set; }

		[Column(TypeName = "char")][StringLength(129)]
		public string Password { get; set; }

		public Guid SID { get; set; }

		public DateTime DtCreated { get { return DateTime.Now; } private set { value = DtCreated; } }

		public int CanChangeName { get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string SSOID { get; set; }

		public int? RealID { get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string PartnerUID { get; set; }
	}
}
