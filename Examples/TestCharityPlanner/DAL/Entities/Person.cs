﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
	public class Person
	{
		[Key]
		public int PersonID { get; set; }

		[Column("ID")]
		public int UserID { get; set; }

		public User User { get; set; }

		public int? NamePrefix { get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string FirstName { get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string MiddleInitial{ get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string LastName{ get; set; }

		public int? NameSuffix{ get; set; }
		public int? KeyQuestion{ get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string KeyAnswer{ get; set; }

		[Column(TypeName = "char")][StringLength(9)]
		public string SSN{ get; set; }
		public int PostalAddress{ get; set; }
		public DateTime? DOB{ get; set; }
		public bool? Gender{ get; set; }
		public int? MaritalStatus{ get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string Email { get; set; }

		[Column(TypeName = "char")][StringLength(10)]
		public string HomePhone { get; set; }

		[Column(TypeName = "char")][StringLength(10)]
		public string BusinessPhone { get; set; }

		[Column(TypeName = "char")][StringLength(10)]
		public string BusinessFax { get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string Employer { get; set; }

		public int? SalaryRange { get; set; }
		public int RegistrationStep { get; set; }
		public bool? MatchingGift { get; set; }

		[Column(TypeName = "varchar")][MaxLength(256)]
		public string AnotherPosition { get; set; }

		[Column(TypeName = "varchar")][MaxLength(250)]
		public string ShareHolder { get; set; }
		public bool? StockExchange { get; set; }
		public int? SHAddress { get; set; }
		public bool AlertMessage { get; set; }
		public int Rank { get; set; }
		public int? ProgramCode { get; set; }
		public DateTime? DOD { get; set; }

		[Column(TypeName = "varchar")][MaxLength(30)]
		public string EmployeeID { get; set; }

		public int? DesignMode { get; set; }

		[Column(TypeName = "varchar")][MaxLength(256)]
		public string Rights { get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string Email2 { get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string Email3 { get; set; }

		public int? PostalAddress2 { get; set; }
		public int? PostalAddress3 { get; set; }

		[Column(TypeName = "varchar")]
		public string CellPhone { get; set; }

		[Column(TypeName = "varchar")]
		public string OtherPhone { get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string Assistant { get; set; }

		[Column(TypeName = "char")][MaxLength(10)]
		public string AsstPhone { get; set; }

		[Column(TypeName = "varchar")][MaxLength(50)]
		public string Dear { get; set; }

		public bool OnlineAccess { get; set; }
		public bool AllowEmail { get; set; }
		public bool AllowMailing { get; set; }
		public int? MailingTax { get; set; }
		public int? MailingTaxAddress { get; set; }
		public int? MailingMarketing { get; set; }
		public int? MailingMarketingAddress { get; set; }
		public int? MailingInformal { get; set; }
		public int? MailingInformalAddress { get; set; }
		public int? MailingFormal { get; set; }
		public int? MailingFormalAddress { get; set; }
		public int? MailingNewServices { get; set; }
		public int? MailingNewServicesAddress { get; set; }

		[Column(TypeName = "varchar")][MaxLength(500)]
		public string MailingDescription { get; set; }
		public bool AllowCall { get; set; }
		public bool CallGrantsQuestions { get; set; }
		public bool CallNewFeaturesReview { get; set; }
		public bool CallMarketing { get; set; }
		public bool CallMisc { get; set; }
		public int? Old_FinancialPartner { get; set; }
		public int? MoreThanMoney { get; set; }
		public int? MoreThanMoneyAddress { get; set; }
		public bool ExpenseReinbursement { get; set; }
		public int? ExpenseReinbursementAddress { get; set; }

		[Column(TypeName = "varchar")][MaxLength(4000)]
		public string GmxRefs { get; set; }
	}
}
