﻿using System;
using System.Data.Entity;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using DAL.Entities;
using System.Dynamic;

namespace Test
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class TestAsset
	{
		private CharityPlannerTestContext db;
		//private readonly string cnnString = "Data Source=11V2; Initial Catalog=CharityPlanneTest; User ID=sa; Password=post+Office2";
		
		private TestContext testContextInstance;
		public TestAsset()
		{
			SetInitializer();
		}

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		[TestInitialize()]
		public void MyTestInitialize() 
		{
			db = new CharityPlannerTestContext();
		}
		
		// Use TestCleanup to run code after each test has run
		[TestCleanup()]
		public void MyTestCleanup() 
		{
			db.Database.ExecuteSqlCommand("delete from Assets");
			db.Database.ExecuteSqlCommand("delete from LocalAssets");
			db.Database.ExecuteSqlCommand("delete from AssetBooks");
			db.Dispose();
		}
		
		#endregion

		[TestMethod]
		public void IsCreatedAsset()
		{
			dynamic appCpMain = HelperInteropCom.Login();
			dynamic appCpBase = appCpMain.cp;
			dynamic author = appCpBase.Author;
			dynamic assetBook = appCpBase.AssetBook;

			assetBook.CreateNew("Test", 7, 28);
			assetBook.Description = "Land with Gravel Pit in Town of Cambridge, County of Somerset, Maine";
			assetBook.UBTI = true;
			assetBook.Author = author.ID;
			assetBook.Update();
			int assetBookId = Convert.ToInt32(assetBook.ID);

			Assert.AreEqual(db.AssetBooks.Where(ab => ab.ID == assetBookId).Count(), 1);
			Assert.AreEqual(db.LocalAssets.Where(la => la.ID == assetBookId).Count(), 1);
			Assert.AreEqual(db.Assets.Count(), 1);
		}

		[TestMethod]
		public void CreateDb()
		{
			int cnt = db.LocalAssets.Count();
		}

		private void SetInitializer()
		{
			Database.SetInitializer<CharityPlannerTestContext>(new CharityPlannerInitializer());
		}

		public void ExampleArbitraryQuery()
		{
			db = new CharityPlannerTestContext();
			var assets = db.Database.SqlQuery<MyType>("select Id, Name from Assets");

			foreach (var asset in assets)
			{
				int Id = asset.ID;
				string Name = asset.Name;
			}
		}
	}

	public class MyType
	{
		public int ID { get; set; }
		public string Name { get; set; }
	}

	public static class HelperInteropCom
	{
		public static object Login()
		{
			dynamic appCPMain = GetInstance("CpMain.Ap");
			var code = appCPMain.login("user", "user1146", "staging123");

			return appCPMain;
		}

		private static object GetInstance(string name)
		{
			Type type = Type.GetTypeFromProgID(name);
			if (type == null)
				throw new Exception("Instance is null.");

			object obj =  Activator.CreateInstance(type);
			if(obj == null)
				throw new Exception("Can't create object.");

			return obj;
		}
	}
}
