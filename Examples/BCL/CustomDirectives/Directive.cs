﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TextTemplating;

namespace CustomDirectiveProcessor
{
	public class Processor : DirectiveProcessor
    {
		private StringBuilder codeBuffer;
		private CodeDomProvider codeDomProvider;
		private String templateContents;
		private CompilerErrorCollection errorsValue;
		
		public override void Initialize(ITextTemplatingEngineHost host)
		{
			//we do not need to do any initialization work
		}
		
		public override void StartProcessingRun(CodeDomProvider languageProvider, String templateContents, CompilerErrorCollection errors)
		{
			this.codeDomProvider = languageProvider;
			this.templateContents = templateContents;
			this.errorsValue = errors;

			this.codeBuffer = new StringBuilder();
		}
		
		public new CompilerErrorCollection Errors
		{
			get { return errorsValue; }
		}	

		public override bool IsDirectiveSupported(string directiveName)
        {
			if (string.Compare(directiveName, "Namespace", StringComparison.OrdinalIgnoreCase) == 0)
				return true;

			if (string.Compare(directiveName, "Class", StringComparison.OrdinalIgnoreCase) == 0)
                return true;

			if (string.Compare(directiveName, "Table", StringComparison.OrdinalIgnoreCase) == 0)
				return true;

			if (string.Compare(directiveName, "Key", StringComparison.OrdinalIgnoreCase) == 0)
				return true;

			if (string.Compare(directiveName, "Connection", StringComparison.OrdinalIgnoreCase) == 0)
				return true;

			if (string.Compare(directiveName, "Rename", StringComparison.OrdinalIgnoreCase) == 0)
				return true;

            return false;
        }

        public override void ProcessDirective(string directiveName, IDictionary<string, string> arguments)
        {
			if (string.Compare(directiveName, "Namespace", StringComparison.OrdinalIgnoreCase) == 0)
			{
				string value;

				if (!arguments.TryGetValue("Value", out value))
					throw new DirectiveProcessorException("Required argument 'Value' not specified.");
				
				CreateProperty("Namespace", value);
			}

			if (string.Compare(directiveName, "Class", StringComparison.OrdinalIgnoreCase) == 0)
            {
                string value;

				if (!arguments.TryGetValue("Value", out value))
					throw new DirectiveProcessorException("Required argument 'Value' not specified.");

				CreateProperty("ClassName", value);
            }

			if (string.Compare(directiveName, "Table", StringComparison.OrdinalIgnoreCase) == 0)
			{
				string value;

				if (!arguments.TryGetValue("Value", out value))
					throw new DirectiveProcessorException("Required argument 'Value' not specified.");

				CreateProperty("TableName", value);
			}

			if (string.Compare(directiveName, "Key", StringComparison.OrdinalIgnoreCase) == 0)
			{
				string value;

				if (!arguments.TryGetValue("Value", out value))
					throw new DirectiveProcessorException("Required argument 'Value' not specified.");

				CreateProperty("Key", value);
			}

			if (string.Compare(directiveName, "Connection", StringComparison.OrdinalIgnoreCase) == 0)
			{
				string value;

				if (!arguments.TryGetValue("Value", out value))
					throw new DirectiveProcessorException("Required argument 'Value' not specified.");

				CreateProperty("Connection", value);
			}

			if (string.Compare(directiveName, "Rename", StringComparison.OrdinalIgnoreCase) == 0)
			{
				string value;

				if (!arguments.TryGetValue("Value", out value))
					throw new DirectiveProcessorException("Required argument 'Value' not specified.");

				CreateProperty("Rename", value);
			}
		}

		public override void FinishProcessingRun()
		{
			this.codeDomProvider = null;
		}

		public override string GetPreInitializationCodeForProcessingRun()
		{
			return String.Empty;
		}

		public override string GetPostInitializationCodeForProcessingRun()
		{
			return String.Empty;
		}

		public override string GetClassCodeForProcessingRun()
		{
			return codeBuffer.ToString();
		}

		public override string[] GetReferencesForProcessingRun()
		{
			return new string[]
            {
                this.GetType().Assembly.Location
            };
		}

		public override string[] GetImportsForProcessingRun()
		{
			return new string[]
            {
            };
		}

		private void CreateProperty (string name, string value)
		{
			var classProperty = new CodeMemberProperty();
			classProperty.Name = name;
			classProperty.Type = new CodeTypeReference("System.String");
			classProperty.Attributes = MemberAttributes.Public;
			classProperty.HasGet = true;
			classProperty.HasSet = false;
			classProperty.GetStatements.Add(new CodeMethodReturnStatement(new CodePrimitiveExpression(value)));

			CodeGeneratorOptions options = new CodeGeneratorOptions();
			options.BlankLinesBetweenMembers = true;
			options.IndentString = "    ";
			options.VerbatimOrder = true;
			options.BracingStyle = "C";

			using (StringWriter writer = new StringWriter(codeBuffer, CultureInfo.InvariantCulture))
			{
				codeDomProvider.GenerateCodeFromMember(classProperty, writer, options);
			}
		}
	}
}
