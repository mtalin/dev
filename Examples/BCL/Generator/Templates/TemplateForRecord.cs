﻿

using System;

namespace BaseClasses.Records
{
	internal class Asset
	{
		private bool _modified;
		private bool _isNew;
		private bool _isLoaded;
		private string _id;
		private string _name;
		private bool _name_Modified;
		private string _name2;
		private bool _name2_Modified;
		private string _name3;
		private bool _name3_Modified;
		private string _name4;
		private bool _name4_Modified;
		private int? _type;
		private bool _type_Modified;
				
		public Asset()
		{
			InitializeVariables();
		}

		public Asset(string id, string name, string name2, string name3, string name4, int? type):this()
		{
			_id = id;
			_name = name;
			_name2 = name2;
			_name3 = name3;
			_name4 = name4;
			_type = type;
		}

		public string ID
		{
			get { return _ID; }
			set { _ID = value; }
		}

		public string Name
		{
			get{ return _name;}
			set
			{
				if (_name != value)
				{
					_name_Modified = true;
					_modified = true;
					_name = value;
				}
			}
		}

		public bool Name_Modified
		{
			get { return _name_Modified; }
			set { _name_Modified = value; }
		}

		public string Name2
		{
			get{ return _name2;}
			set
			{
				if (_name2 != value)
				{
					_name2_Modified = true;
					_modified = true;
					_name2 = value;
				}
			}
		}

		public bool Name2_Modified
		{
			get { return _name2_Modified; }
			set { _name2_Modified = value; }
		}

		public string Name3
		{
			get{ return _name3;}
			set
			{
				if (_name3 != value)
				{
					_name3_Modified = true;
					_modified = true;
					_name3 = value;
				}
			}
		}

		public bool Name3_Modified
		{
			get { return _name3_Modified; }
			set { _name3_Modified = value; }
		}

		public string Name4
		{
			get{ return _name4;}
			set
			{
				if (_name4 != value)
				{
					_name4_Modified = true;
					_modified = true;
					_name4 = value;
				}
			}
		}

		public bool Name4_Modified
		{
			get { return _name4_Modified; }
			set { _name4_Modified = value; }
		}

		public int? Type
		{
			get{ return _type;}
			set
			{
				if (_type != value)
				{
					_type_Modified = true;
					_modified = true;
					_type = value;
				}
			}
		}

		public bool Type_Modified
		{
			get { return _type_Modified; }
			set { _type_Modified = value; }
		}


		public bool Modified
		{
			get { return _name_Modified || _name2_Modified || _name3_Modified || _name4_Modified || _type_Modified; }
		}

		public bool IsNew
		{
			get { return _isNew; }
			set { _isNew = value; }
		}

		public bool IsLoaded
		{
			get { return _isLoaded; }
			set { _isLoaded = value; }
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType()) return false;
			var o = (Asset)obj;

			return (ID == o.ID) && (Name == o.Name) && (Name2 == o.Name2) && (Name3 == o.Name3) && (Name4 == o.Name4) && (Type == o.Type);

		}

		public override int GetHashCode()
		{
			return	ID.GetHashCode() ^ Name.GetHashCode() ^ Name2.GetHashCode() ^ Name3.GetHashCode() ^ Name4.GetHashCode() ^ Type.GetHashCode();
		}
		
		private void InitializeVariables()
		{
			_isLoaded = false;
			_name = null; _name_Modified = false;
			_name2 = null; _name2_Modified = false;
			_name3 = null; _name3_Modified = false;
			_name4 = null; _name4_Modified = false;
			_type = null; _type_Modified = false;
		}
	}
}