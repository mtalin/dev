﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using BaseClasses.Modules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BaseClasses;
using BaseClasses.Entities;
using System.Xml.Linq;

namespace TestBaseClass
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class TestAsset
	{
		private Context context;
		private Asset asset;
		public TestAsset()
		{
			
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		[TestInitialize()]
		public void TestInitialize()
		{
			context = new Context();
			asset = Asset.NewInstance();
		}
		
		// Use TestCleanup to run code after each test has run
		[TestCleanup()]
		public void TestCleanup()
		{
			asset = null;
			context.Close();
			context = null;
		}
		
		#endregion

		[TestMethod]
		public void TestCreateAndUpdate()
		{	
			asset.CreateNew();
			asset.Name = "TestAsset";
			asset.AssetType = 7;
			bool result = context.Update();

			Assert.IsTrue(result);	
		}

		[TestMethod]
		public void TestChangeAndUpdate()
		{
			context.BeginTrans();
			asset.Load("1");
			asset.Name2 = "Insurance";
			asset.Update();

			Assert.AreEqual("Insurance", asset.Name2);

			context.RollbackTrans();
		}

		[TestMethod]
		public void TestToXml()
		{
			XElement expected = XElement.Parse(@"<Asset>
					<ID>1</ID>
					<Name Modified='true'>Insurance</Name>
					<Name2 Modified='false' />
					<Name3 Modified='false' />
					<Name4 Modified='false' />
					<AssetType Modified='true'>7</AssetType>
				</Asset>");

			asset.Load("1");
			XElement result = XElement.Parse(asset.ToXml());

			Assert.AreEqual(expected.Value, result.Value);			
		}

		[TestMethod]
		public void TestFromXml()
		{
			string sXml =
				@"<Asset>
				<ID>1</ID>
				<Name Modified='true'>Insurance</Name>
				<Name2 Modified='false' />
				<Name3 Modified='false' />
				<Name4 Modified='false' />
				<AssetType Modified='true'>7</AssetType>
			</Asset>";

			asset.CreateNew();
			asset.FromXml(sXml);

			Assert.AreEqual("1", asset.ID);
			Assert.AreEqual("Insurance", asset.Name);
			Assert.IsNull(asset.Name2);
			Assert.IsNull(asset.Name3);
			Assert.IsNull(asset.Name4);
			Assert.AreEqual(7, asset.AssetType);
			Assert.IsTrue(asset.Modified);		
		}

		[TestMethod]
		public void TestValidate()
		{
			Asset asset = Asset.NewInstance();
			asset.Load("1");
			string res = asset.Validate();

			Assert.AreEqual("", res);
		}

		[TestMethod]
		public void TestDump()
		{
			asset.Load("1");
			string expected = string.Format(
				"Asset:ID={0},Name={1},Name2={2},Name3={3},Name4={4},AssetType={5}",
				asset.ID,
				Tools.DbValue(asset.Name),
				Tools.DbValue(asset.Name2),
				Tools.DbValue(asset.Name3),
				Tools.DbValue(asset.Name4),
				Tools.DbValue(asset.AssetType)
				);

			string res = asset.Dump();

			Assert.AreEqual(expected, res);	
		}

		[TestMethod]
		public void TestSafeLoad()
		{
			bool res = asset.SafeLoad("1");

			Assert.IsTrue(res);	
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void TestIsValidExpectedExceptionIfIdisNull()
		{
			asset.Load(null);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		[TestCategory("Test")]
		public void TestUpdateIfNotCreatedOrNotLoaded()
		{
			asset.Update();
		}
	}
}
