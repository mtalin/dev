﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BaseClasses;
using BaseClasses.Entities;

namespace TestBaseClass
{
	/// <summary>
	/// Summary description for TestDC
	/// </summary>
	[TestClass]
	public class TestAsset2
	{
		public TestAsset2()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;
		private Asset2 asset;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		
		#endregion

		//Use TestInitialize to run code before running each test 
		[TestInitialize()]
		public void TestInitialize()
		{
			asset = Asset2.NewInstance();
		}

		//Use TestCleanup to run code after each test has run
		[TestCleanup()]
		public void TestCleanup()
		{
			asset = null;
		}

		[TestMethod]
		public void TestChangeAndUpdate()
		{
			DC.BeginTrans();
			asset.Load("1");
			asset.Name = "Insurance";
			asset.Name = "Test123";
			asset.Update();
			DC.RollbackTrans();
		}

		[TestMethod]
		public void TestCreateAndUpdate()
		{
			DC.BeginTrans();
			asset.CreateNew();
			asset.Name = "Asset1";
			asset.AssetType = 7;
			asset.Update();
			DC.CommitTrans();
		}

		
	}
}
