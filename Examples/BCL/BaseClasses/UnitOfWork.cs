﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseClasses.Repositories.Abstract;
using BaseClasses.Repositories.Concrete;
using System.Data.SqlClient;
using System.Threading;
using BaseClasses.Entities;
using BaseClasses.Records;

namespace BaseClasses
{
	public class UnitOfWork : IDisposable
	{
		private IRepository<AssetRecord> _assetRepository;
		private readonly SqlConnection _cnn;
		private SqlTransaction _transaction;
		private List<DomainObject> _cleanObjects;
		private  List<DomainObject> _newObjects;
		private  List<DomainObject> _changedObjects;

		public UnitOfWork()
		{
			_cleanObjects = new List<DomainObject>();
			_newObjects = new List<DomainObject>();
			_changedObjects = new List<DomainObject>();

			//Get connection from other place. Hardcode.
			_cnn = new SqlConnection("Data Source=11V2;Initial Catalog=CharityPlanner;User ID=sa;Password=post+Office2");
			_cnn.Open();
		}
		
		public void BeginTransaction()
		{
			//if(_transaction == null)
				_transaction = _cnn.BeginTransaction();
		}

		public void CommitTransaction()
		{
			if(_transaction != null)
				_transaction.Commit();
		}

		public void RollbackTransaction()
		{
			if(_transaction != null)
				_transaction.Rollback();
		}

		public void RegisterClean(DomainObject obj)
		{
			if(obj == null)
				throw new Exception("Object is null.");

			if (!_cleanObjects.Contains(obj))
				_cleanObjects.Add(obj);
		}

		public void RegisterNew(DomainObject obj)
		{
			if (obj == null)
				throw new Exception("Object is null.");

			if(!_cleanObjects.Contains(obj) && !_changedObjects.Contains(obj))
				_newObjects.Add(obj);
		}

		public void RegisterChanged(DomainObject obj)
		{
			if (obj == null)
				throw new Exception("Object is null.");

			if (!_changedObjects.Contains(obj) && !_newObjects.Contains(obj))
				_changedObjects.Add(obj);
		}

		public ICollection<DomainObject> NewObjects
		{
			get { return _newObjects.AsReadOnly(); }
		}

		public ICollection<DomainObject> ChangedObjects
		{
			get { return _changedObjects.AsReadOnly(); }
		}

		internal IRepository<AssetRecord> AssetRepository
		{
			get
			{
				if (_assetRepository == null)
				{
					_assetRepository = new AssetSqlRepository(_cnn, _transaction);
					//_assetRepository = new AssetFakeRepository();
				}

				return _assetRepository;
			}
		}

		private bool _disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					_cnn.Close();
					_cleanObjects = null;
					_newObjects = null;
					_changedObjects = null;
					_assetRepository = null;
				}
			}
			_disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
