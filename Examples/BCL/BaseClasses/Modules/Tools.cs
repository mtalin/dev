﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseClasses.Modules
{
	public static class Tools
	{
		public static string DbValue(object value, string nullValue = "NULL")
		{
			if (value == null)
				return nullValue;

			return value.ToString();
		}
	}
}
