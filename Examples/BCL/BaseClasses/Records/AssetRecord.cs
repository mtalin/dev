﻿using System;

namespace BaseClasses.Records
{
	internal class AssetRecord:Record
	{
		private bool _modified;
		private string _id;
		private string _name;
		private bool _name_Modified;
		private string _name2;
		private bool _name2_Modified;
		private string _name3;
		private bool _name3_Modified;
		private string _name4;
		private bool _name4_Modified;
		private int? _assetType;
		private bool _assetType_Modified;
		private bool _isNew;
		private bool _isLoaded;

		public AssetRecord()
		{
			InitializeVariables();
		}

		public AssetRecord(string id, string name, string name2, string name3, string name4, int? assetType):this()
		{
			_id = id;
			_name = name;
			_name2 = name2;
			_name3 = name3;
			_name4 = name4;
			_assetType = assetType;
		}

		public string ID
		{
			get { return _id; }
			set { _id = value; }
		}

		public string Name
		{
			get { return _name; } 
			set
			{
				if (_name != value)
				{
					_name_Modified = true;
					_modified = true;
					_name = value;
				}
			}
		}

		public bool Name_Modified
		{
			get { return _name_Modified; }
			set { _name_Modified = value; }
		}

		public string Name2
		{
			get { return _name2; }
			set
			{
				if (_name2 != value)
				{
					_name2_Modified = true;
					_modified = true;
					_name2 = value;
				}
			}
		}

		public bool Name2_Modified
		{
			get { return _name2_Modified; }
			set { _name2_Modified = value; }
		}

		public string Name3
		{
			get { return _name3; }
			set
			{
				if (_name3 != value)
				{
					_name3_Modified = true;
					_modified = true;
					_name3 = value;
				}
			}
		}

		public bool Name3_Modified
		{
			get { return _name3_Modified; }
			set { _name3_Modified = value; }
		}

		public string Name4
		{
			get { return _name4; }
			set
			{
				if (_name4 != value)
				{
					_name4_Modified = true;
					_modified = true;
					_name4 = value;
				}
			}
		}

		public bool Name4_Modified
		{
			get { return _name4_Modified; }
			set { _name4_Modified = value; }
		}

		public int? AssetType
		{
			get { return _assetType; }
			set
			{
				if (_assetType != value)
				{
					_assetType_Modified = true;
					_modified = true;
					_assetType = value;
				}
			}
		}

		public bool AssetType_Modified
		{
			get { return _assetType_Modified; }
			set { _assetType_Modified = value; }
		}

		public bool Modified
		{
			get { return _name_Modified || _name2_Modified || _name3_Modified || _name4_Modified || _assetType_Modified; }
		}

		public bool IsNew
		{
			get { return _isNew; }
			set { _isNew = value; }
		}

		public bool IsLoaded
		{
			get { return _isLoaded; }
			set { _isLoaded = value; }
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType()) return false;
			var a = (AssetRecord)obj;

			return (ID == a.ID) && (Name == a.Name) && (Name2 == a.Name2) 
					&& (Name3 == a.Name3) && (Name4 == a.Name4) 
					&& (AssetType == a.AssetType);

		}

		public override int GetHashCode()
		{
			return	ID.GetHashCode() ^ Name.GetHashCode() ^ Name.GetHashCode() 
					^ Name2.GetHashCode() ^ Name3.GetHashCode() ^ Name4.GetHashCode() 
					^ AssetType.GetHashCode();
		}
		
		private void InitializeVariables()
		{
			_name = null; _name_Modified = false;
			_name2 = null; _name2_Modified = false;
			_name3 = null; _name3_Modified = false;
			_name4 = null; _name4_Modified = false;
			_assetType = 0; _assetType_Modified = false;
			_isLoaded = false;
		}

		public AssetRecord Copy()
		{
			return new AssetRecord(_id, _name, _name2, _name3, _name4, _assetType);
		}
	}
}
