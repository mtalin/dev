﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseClasses.Entities
{
	public abstract class DomainObject
	{
		public abstract void Update();
	}
}
