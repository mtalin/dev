﻿#define DEBUG
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using BaseClasses.Modules;
using BaseClasses.Records;
using System.Xml.Linq;

namespace BaseClasses.Entities
{
	public class Asset : DomainObject
	{
		private AssetRecord _assetRecord;
		private UnitOfWork _unitOfWork;
		//private RecordWatcher _watcher;
		private bool _modified;
		private bool _loaded;

		private Asset()
		{
			InitializeVariables();
		}
		
		public string ID
		{
			get { return _assetRecord.ID; }
		}

		public string Name
		{
			get { return _assetRecord.Name; }
			set
			{
				if (_assetRecord.Name != value)
				{
					_assetRecord.Name = value;
					MarkAsChanged();
				}
			}
		}

		public string Name2
		{
			get { return _assetRecord.Name2; }
			set
			{
				if (_assetRecord.Name2 != value)
				{
					_assetRecord.Name2 = value;
					MarkAsChanged();
				}
			}
		}

		public string Name3
		{
			get { return _assetRecord.Name3; }
			set
			{
				if (_assetRecord.Name3 != value)
				{
					_assetRecord.Name3 = value;
					MarkAsChanged();
				}
			}
		}

		public string Name4
		{
			get { return _assetRecord.Name4; }
			set
			{
				if (_assetRecord.Name4 != value)
				{
					_assetRecord.Name4 = value;
					MarkAsChanged();
				}
			}
		}

		public int? AssetType
		{
			get { return _assetRecord.AssetType; }
			set
			{
				if (_assetRecord.AssetType != value)
				{
					_assetRecord.AssetType = value;
					MarkAsChanged();
				}
			}
		}

		public bool Modified
		{
			get { return _assetRecord.Modified; }
		}

		public static Asset NewInstance()
		{
			return new Asset();
		}

		public void CreateNew()
		{
			_assetRecord.IsNew = true;
			_loaded = true;
			MarkAsNew();
		}

		public void Load(string id)
		{
			if (id == null)
				throw new ArgumentNullException();
			
			try
			{
				_assetRecord = _unitOfWork.AssetRepository.LoadById(id);

				if (_assetRecord.ID == id)
				{
					_loaded = true;
					_assetRecord.IsLoaded = true;
					MarkAsClean();
				}
				else
				{
					throw new InvalidDataException();
				}
			}
			catch(Exception e)
			{
				throw new InvalidOperationException("Asset.Load");
			}
		}

		public override void Update()
		{
			if (!_loaded)
				throw new InvalidOperationException();

			if (_unitOfWork == null)
				throw new InvalidDataException();

			CheckInvariant("Asset.update");

			try
			{
				_unitOfWork.AssetRepository.Update(_assetRecord);
			}
			catch (Exception e)
			{
				Error.Throw("Asset.Update");
			}
		}

		public string Validate()
		{
			var msg = new StringBuilder();

			msg.Append(Checker.String(Name, 100, "Name", AllowNull.NotNull));
			msg.Append(Checker.String(Name2, 100, "Name2", AllowNull.Null));
			msg.Append(Checker.String(Name3, 100, "Name3" , AllowNull.Null));
			msg.Append(Checker.String(Name4, 100, "Name4", AllowNull.Null));
			msg.Append(Checker.Enum(AssetType, "AssetType", true));

			return msg.ToString();
		}

		public void FromXml(string sXml, string filter = "")
		{
			if (sXml == null)
				throw new ArgumentNullException();

			try
			{
				var xml = XElement.Parse(sXml);

				var xID = xml.Element("ID");
				if (xID != null) _assetRecord.ID = xID.Value;

				var xName = xml.Element("Name");
				if (xName == null || xName.IsEmpty)
				{
					_assetRecord.Name = null;
				}
				else
				{
					_assetRecord.Name = xName.Value;
					var xAttribute = xName.Attribute("Modified");
					if (xAttribute != null)
						_assetRecord.Name_Modified = Boolean.Parse(xAttribute.Value);
				}

				var xName2 = xml.Element("Name2");
				if (xName2 == null || xName2.IsEmpty)
				{
					_assetRecord.Name2 = null;
				}
				else
				{
					_assetRecord.Name2 = xName2.Value;
					var xAttribute = xName2.Attribute("Modified");
					if (xAttribute != null) _assetRecord.Name2_Modified = Boolean.Parse(xAttribute.Value);
				}

				var xName3 = xml.Element("Name3");
				if (xName3 == null || xName3.IsEmpty)
				{
					_assetRecord.Name3 = null;
				}
				else
				{
					_assetRecord.Name3 = xName3.Value;
					var xAttribute = xName3.Attribute("Modified");
					if (xAttribute != null) _assetRecord.Name3_Modified = Boolean.Parse(xAttribute.Value);
				}

				var xName4 = xml.Element("Name4");
				if (xName4 == null || xName4.IsEmpty)
				{
					_assetRecord.Name3 = null;
				}
				else
				{
					_assetRecord.Name4 = xName4.Value;
					var xAttribute = xName4.Attribute("Modified");
					if (xAttribute != null) _assetRecord.Name4_Modified = Boolean.Parse(xAttribute.Value);
				}

				var xAssetType = xml.Element("AssetType");
				if (xAssetType == null || xAssetType.IsEmpty)
				{
					_assetRecord.Name3 = null;
				}
				else
				{
					_assetRecord.AssetType = Int32.Parse(xAssetType.Value);
					var xAttribute = xAssetType.Attribute("Modified");
					if (xAttribute != null)
					{
						var attribute = xAssetType.Attribute("Modified");
						if (attribute != null) _assetRecord.AssetType_Modified = Boolean.Parse(attribute.Value);
					}
				}
			}
			catch (Exception e)
			{
				Error.Throw("Asset.FromXml");
			}

		}

		public string ToXml()
		{
			if (_assetRecord == null)
				throw new ArgumentNullException();

			var xml = new XElement("Asset",
			        new XElement("ID", _assetRecord.ID),
					new XElement("Name", _assetRecord.Name, new XAttribute("Modified", _assetRecord.Name_Modified)),
					new XElement("Name2", _assetRecord.Name2, new XAttribute("Modified", _assetRecord.Name2_Modified)),
					new XElement("Name3", _assetRecord.Name3, new XAttribute("Modified", _assetRecord.Name3_Modified)),
					new XElement("Name4", _assetRecord.Name4, new XAttribute("Modified", _assetRecord.Name4_Modified)),
					new XElement("AssetType", _assetRecord.AssetType, new XAttribute("Modified", _assetRecord.AssetType_Modified))
				);

			return xml.ToString();
		}
		
		//public void UnFold(object[] v)
		//{
		//    _assetRecord.ID = (string) v[0];
		//    _assetRecord.Name = (string) v[1];
		//    _assetRecord.Name_Modified = (bool) v[2];
		//    _assetRecord.Name2 = (string) v[3];
		//    _assetRecord.Name2_Modified = (bool) v[4];
		//    _assetRecord.Name3 = (string) v[5];
		//    _assetRecord.Name3_Modified = (bool) v[6];
		//    _assetRecord.Name4 = (string) v[7];
		//    _assetRecord.Name4_Modified = (bool) v[8];
		//    _assetRecord.AssetType = (int?) v[9];
		//    _assetRecord.AssetType_Modified = (bool) v[10];
		//}

		//public object[] Fold()
		//{
		//    if(_assetRecord == null)
		//        throw new ArgumentNullException();

		//    var v = new object[11];

		//    v[0] = _assetRecord.ID;
		//    v[1] = _assetRecord.Name;
		//    v[2] = _assetRecord.Name_Modified;
		//    v[3] = _assetRecord.Name2;
		//    v[4] = _assetRecord.Name2_Modified;
		//    v[5] = _assetRecord.Name3;
		//    v[6] = _assetRecord.Name3_Modified;
		//    v[7] = _assetRecord.Name4;
		//    v[8] = _assetRecord.Name4_Modified;
		//    v[9] = _assetRecord.AssetType;
		//    v[10] = _assetRecord.AssetType_Modified;

		//    return v;
		//}

		public bool SafeLoad(string id)
		{
			if (Checker.Key(id, "Assets", "ID"))
			{
				Load(id);
				return true;
			}

			return false;
		}

		public void CheckInvariant(string source = "Asset.checkInvariant")
		{
			#if (DEBUG)
			{
				string msg = Validate();

				if (string.IsNullOrEmpty(msg) && !string.IsNullOrEmpty(_assetRecord.ID))
					msg = Checker.Reference(_assetRecord.ID, "Assets", "ID", "ID", false);

				if (!string.IsNullOrEmpty(msg))
					Error.Raise("Asset.CheckInvariant", "checkInvariant failed with message: \"" + msg + "\"" + Dump());
			}
			#endif
		}

		public string Dump()
		{
			var dumpValue = new StringBuilder();
			dumpValue.Append("Asset:");
			dumpValue.AppendFormat("ID={0}", _assetRecord.ID);
			dumpValue.Append(_modified ? ",modified" : "");
			dumpValue.Append(_modified ? ",loaded" : "");

			dumpValue.AppendFormat(",Name={0}", Tools.DbValue(_assetRecord.Name)).Append(_assetRecord.Name_Modified ? "+": "");
			dumpValue.AppendFormat(",Name2={0}", Tools.DbValue(_assetRecord.Name2)).Append(_assetRecord.Name2_Modified ? "+": "");
			dumpValue.AppendFormat(",Name3={0}", Tools.DbValue(_assetRecord.Name3)).Append(_assetRecord.Name3_Modified ? "+": "");
			dumpValue.AppendFormat(",Name4={0}", Tools.DbValue(_assetRecord.Name4)).Append(_assetRecord.Name4_Modified ? "+": "");
			dumpValue.AppendFormat(",AssetType={0}", Tools.DbValue(_assetRecord.AssetType)).Append(_assetRecord.AssetType_Modified ? "+": "");

			return dumpValue.ToString();
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType()) return false;
			var a = (Asset)obj;

			return (ID == a.ID);
		}

		public override int GetHashCode()
		{
			return ID.GetHashCode();
		}

		protected void MarkAsClean()
		{
			_unitOfWork.RegisterClean(this);
		}

		protected void MarkAsNew()
		{
			_unitOfWork.RegisterNew(this);
		}

		protected void MarkAsChanged()
		{
			_unitOfWork.RegisterChanged(this);
		}

		private void InitializeVariables()
		{
			_loaded = false;
			_modified = false;
			_assetRecord = new AssetRecord();
			_unitOfWork = Context.GetUnitOfWork();
		}

	}
}
