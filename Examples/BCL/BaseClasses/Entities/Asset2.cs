﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using BaseClasses.Modules;
using BaseClasses.Records;
using BaseClasses.Repositories.Abstract;
using BaseClasses.Repositories.Concrete;

namespace BaseClasses.Entities
{
	public class Asset2 : DomainObject
	{
		private AssetRecord _assetRecord;
		private bool _loaded;
		private IRepository<AssetRecord> _assetRepository;
		private const bool ComplexUpdate = true;

		private Asset2()
		{
			InitializeVariables();
		}
		
		public string ID
		{
			get { return _assetRecord.ID; }
		}

		public string Name
		{
			get { return _assetRecord.Name; }
			set{ _assetRecord.Name = value;	}
		}

		public string Name2
		{
			get { return _assetRecord.Name2; }
			set { _assetRecord.Name2 = value;}
		}

		public string Name3
		{
			get { return _assetRecord.Name3; }
			set { _assetRecord.Name3 = value;}
		}

		public string Name4
		{
			get { return _assetRecord.Name4; }
			set { _assetRecord.Name4 = value;}
		}

		public int? AssetType
		{
			get { return _assetRecord.AssetType; }
			set {_assetRecord.AssetType = value; }
		}

		public bool Modified
		{
			get { return _assetRecord.Modified; }
		}

		public static Asset2 NewInstance()
		{
			return new Asset2();
		}

		public void CreateNew()
		{
			_assetRecord.IsNew = true;
			_loaded = true;
		}

		public void Load(string id)
		{
			if (id == null)
				throw new ArgumentNullException();

			if (_assetRecord.ID == id && !Modified)
				return;

			DC.Connect();
			try
			{
				_assetRecord = __AssetRepository.LoadById(id);
				_loaded = true;
				_assetRecord.IsLoaded = true;
			}
			finally
			{
				DC.Disconnect();
			}		
		}

		public override void Update()
		{
			if (!_loaded)
			    throw new InvalidOperationException();

			if (!Modified)
				return;

			CheckInvariant("Asset.update");

			if (ComplexUpdate)
				DC.BeginTrans();
			else
				DC.Connect();

			try
			{
				__AssetRepository.Update(_assetRecord);
			}
			catch (Exception e)
			{
				goto ErrHandler;
			}

			if (ComplexUpdate)
				DC.CommitTrans();
			else
				DC.Disconnect();

			return;

			ErrHandler:
				if (ComplexUpdate)
					DC.RollbackTrans();
				else
					DC.Disconnect();

				throw new Exception(string.Format("Asset.Update:ID={0}, ComplexUpdate={1}", _assetRecord.ID, ComplexUpdate));
		}

		public string Validate()
		{
			var msg = new StringBuilder();

			msg.Append(Checker.String(Name, 100, "Name", AllowNull.NotNull));
			msg.Append(Checker.String(Name2, 100, "Name2", AllowNull.Null));
			msg.Append(Checker.String(Name3, 100, "Name3" , AllowNull.Null));
			msg.Append(Checker.String(Name4, 100, "Name4", AllowNull.Null));
			msg.Append(Checker.Enum(AssetType, "AssetType", true));

			return msg.ToString();
		}

		public void FromXml(string sXml, string filter = "")
		{
			if (sXml == null)
				throw new ArgumentNullException();

			if (string.IsNullOrEmpty(filter))
			{
				if(Checker.IsId(filter))
				{
					filter = string.Format("//Asset[@ID={0}]", Checker.dbStr(filter));
				}
			}
			else
			{
				filter = "//Asset";
			}

			try
			{
				var xml = XElement.Parse(sXml);
				
				var xID = xml.Element("ID");
				if (xID != null) _assetRecord.ID = xID.Value;

				var xName = xml.Element("Name");
				if (xName == null || xName.IsEmpty)
				{
					_assetRecord.Name = null;
				}
				else
				{
					_assetRecord.Name = xName.Value;
					var xAttribute = xName.Attribute("Modified");
					if (xAttribute != null)
						_assetRecord.Name_Modified = Boolean.Parse(xAttribute.Value);
				}

				var xName2 = xml.Element("Name2");
				if (xName2 == null || xName2.IsEmpty)
				{
					_assetRecord.Name2 = null;
				}
				else
				{
					_assetRecord.Name2 = xName2.Value;
					var xAttribute = xName2.Attribute("Modified");
					if (xAttribute != null) _assetRecord.Name2_Modified = Boolean.Parse(xAttribute.Value);
				}

				var xName3 = xml.Element("Name3");
				if (xName3 == null || xName3.IsEmpty)
				{
					_assetRecord.Name3 = null;
				}
				else
				{
					_assetRecord.Name3 = xName3.Value;
					var xAttribute = xName3.Attribute("Modified");
					if (xAttribute != null) _assetRecord.Name3_Modified = Boolean.Parse(xAttribute.Value);
				}

				var xName4 = xml.Element("Name4");
				if (xName4 == null || xName4.IsEmpty)
				{
					_assetRecord.Name3 = null;
				}
				else
				{
					_assetRecord.Name4 = xName4.Value;
					var xAttribute = xName4.Attribute("Modified");
					if (xAttribute != null) _assetRecord.Name4_Modified = Boolean.Parse(xAttribute.Value);
				}

				var xAssetType = xml.Element("AssetType");
				if (xAssetType == null || xAssetType.IsEmpty)
				{
					_assetRecord.Name3 = null;
				}
				else
				{
					_assetRecord.AssetType = Int32.Parse(xAssetType.Value);
					var xAttribute = xAssetType.Attribute("Modified");
					if (xAttribute != null)
					{
						var attribute = xAssetType.Attribute("Modified");
						if (attribute != null) _assetRecord.AssetType_Modified = Boolean.Parse(attribute.Value);
					}
				}
			}
			catch (Exception e)
			{
				Error.Throw("Asset.FromXml");
			}

		}

		public string ToXml()
		{
			if (_assetRecord == null)
				throw new ArgumentNullException();

			var xml = new XElement("Asset",
			        new XElement("ID", _assetRecord.ID),
					new XElement("Name", _assetRecord.Name, new XAttribute("Modified", _assetRecord.Name_Modified)),
					new XElement("Name2", _assetRecord.Name2, new XAttribute("Modified", _assetRecord.Name2_Modified)),
					new XElement("Name3", _assetRecord.Name3, new XAttribute("Modified", _assetRecord.Name3_Modified)),
					new XElement("Name4", _assetRecord.Name4, new XAttribute("Modified", _assetRecord.Name4_Modified)),
					new XElement("AssetType", _assetRecord.AssetType, new XAttribute("Modified", _assetRecord.AssetType_Modified))
				);

			return xml.ToString();
		}

		public bool SafeLoad(string id)
		{
			if (Checker.Key(id, "Assets", "ID"))
			{
				Load(id);
				return true;
			}

			return false;
		}

		public void CheckInvariant(string source = "Asset.checkInvariant")
		{
			#if (DEBUG)
			{
				string msg = Validate();

				if (string.IsNullOrEmpty(msg) && !string.IsNullOrEmpty(_assetRecord.ID))
					msg = Checker.Reference(_assetRecord.ID, "Assets", "ID", "ID", false);

				if (!string.IsNullOrEmpty(msg))
					Error.Raise("Asset.CheckInvariant", "checkInvariant failed with message: \"" + msg + "\"" + Dump());
			}
			#endif
		}

		public string Dump()
		{
			var dumpValue = new StringBuilder();
			dumpValue.Append("Asset:");
			dumpValue.AppendFormat("ID={0}", _assetRecord.ID);
			dumpValue.Append(Modified ? ",modified" : "");
			dumpValue.Append(_loaded ? ",loaded" : "");

			dumpValue.AppendFormat(",Name={0}", Tools.DbValue(_assetRecord.Name)).Append(_assetRecord.Name_Modified ? "+": "");
			dumpValue.AppendFormat(",Name2={0}", Tools.DbValue(_assetRecord.Name2)).Append(_assetRecord.Name2_Modified ? "+": "");
			dumpValue.AppendFormat(",Name3={0}", Tools.DbValue(_assetRecord.Name3)).Append(_assetRecord.Name3_Modified ? "+": "");
			dumpValue.AppendFormat(",Name4={0}", Tools.DbValue(_assetRecord.Name4)).Append(_assetRecord.Name4_Modified ? "+": "");
			dumpValue.AppendFormat(",AssetType={0}", Tools.DbValue(_assetRecord.AssetType)).Append(_assetRecord.AssetType_Modified ? "+": "");

			return dumpValue.ToString();
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType()) return false;
			var a = (Asset)obj;

			return (ID == a.ID);
		}

		public override int GetHashCode()
		{
			return ID.GetHashCode();
		}

		private void InitializeVariables()
		{
			_loaded = false;
			_assetRecord = new AssetRecord();
		}

		private IRepository<AssetRecord> __AssetRepository
		{
			get
			{
				if (_assetRepository == null)
				{
					_assetRepository = new AssetSqlRepository2(DC.Connection, DC.Transaction);
					//_assetRepository = new AssetFakeRepository();
				}

				return _assetRepository;
			}
		}
	}
}
