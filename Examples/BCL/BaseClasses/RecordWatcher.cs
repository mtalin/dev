﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseClasses.Records;

namespace BaseClasses
{
	public class RecordWatcher
	{
		private static RecordWatcher _watcher;
		private Dictionary<string, Record> _records = new Dictionary<string, Record>();

		private RecordWatcher()
		{
			
		}

		public static RecordWatcher GetWatcher()
		{
			if(_watcher == null)
				_watcher = new RecordWatcher();

			return _watcher;
		}

		public static Record GetRecord( string key)
		{
			RecordWatcher watcher = GetWatcher();

			if (watcher == null)
				throw new InvalidOperationException("Watcher is null.");

			if (watcher._records.ContainsKey(key))
				return watcher._records[key];
			
			return null;
		}

		public static void AddRecord(string key, Record record)
		{
			RecordWatcher watcher = GetWatcher();

			if (watcher == null)
				throw new InvalidOperationException("Watcher is null.");
			
			if (watcher._records.ContainsKey(key))
				watcher._records[key] = record;

			watcher._records.Add(key, record);
		}
	}
}
