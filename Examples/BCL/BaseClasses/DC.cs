﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using BaseClasses.Repositories.Abstract;
using BaseClasses.Repositories.Concrete;

namespace BaseClasses
{
	public static class DC
	{
		private static SqlConnection _cnn;	
		private static SqlTransaction _transaction;

		[ThreadStatic]
		private static int _cntTransactions;

		[ThreadStatic]
		private static int _cntConnections;

		private static object Locker = new object();

		static DC()
		{
			_cnn = new SqlConnection("Data Source=11V2;Initial Catalog=CharityPlanner;User ID=sa;Password=post+Office2");
		}

		public static void Connect()
		{
			if(_cntConnections == 0)
				_cnn.Open();

			_cntConnections ++;
		}

		public static void Disconnect()
		{
			if (_cntConnections >= 0)
				_cntConnections --;

			if(_cntConnections == 0)
				_cnn.Close();
		}

		public static void BeginTrans()
		{
			Connect();
			if (_cntTransactions == 0)
				_transaction = _cnn.BeginTransaction();

			_cntTransactions++;
		}

		public static void CommitTrans()
		{
			if(_cntTransactions == 1)
				_transaction.Commit();

			Disconnect();
			_cntTransactions--;
		}

		public static void RollbackTrans()
		{
			if(_cntTransactions == 1)
				_transaction.Rollback();

			Disconnect();
			_cntTransactions--;
		}

		public static Array Query(string sql)
		{
			return null;
		}

		public static Array QueryLine(string sql)
		{
			return null;
		}

		public static Array QueryColumn(string sql)
		{
			return null;
		}

		public static object QueryValue(string sql)
		{
			return null;
		}

		public static SqlConnection Connection
		{
			get
			{
				lock(Locker)
				{
					return _cnn;
				}
			}		
		}

		public static SqlTransaction Transaction
		{
			get
			{
				lock (Locker)
				{
					return _transaction;
				}
			}
		}
	}
}
