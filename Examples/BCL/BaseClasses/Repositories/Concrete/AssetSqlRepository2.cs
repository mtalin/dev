﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using BaseClasses.Records;
using BaseClasses.Repositories.Abstract;

namespace BaseClasses.Repositories.Concrete
{
	internal class AssetSqlRepository2:IRepository<AssetRecord>
	{
		private SqlConnection _cnn;
		private SqlTransaction _transact;

		public AssetSqlRepository2(SqlConnection cnn, SqlTransaction transact)
		{
			_cnn = cnn;
			_transact = transact;
		}

		public AssetRecord LoadById(string id)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException();

			var param = new SqlParameter("@ID", SqlDbType.Int);
			param.Value = id;

			var command = new SqlCommand("select Top 1 ID, Name, Name2, Name3, Name4, Type from Assets where ID = @ID");
			command.Connection = _cnn;
			command.Transaction = _transact;
			command.Parameters.Add(param);

			SqlDataReader reader = command.ExecuteReader();
			if(reader.Read())
			{
					var asset = new AssetRecord(
					Convert.ToString(reader.GetInt32(0)),
					reader.GetString(1),
					reader.IsDBNull(2) ? null : reader.GetString(2),
					reader.IsDBNull(3) ? null : reader.GetString(3),
					reader.IsDBNull(4) ? null : reader.GetString(4),
					reader.GetInt32(5)
					);

				reader.Close();

				return asset;
			}
			
			throw new InvalidDataException();
		}

		public void Update(AssetRecord record)
		{
			if(record == null)
				throw new ArgumentNullException();

			string sql = BuildQuery(record);

			if (sql != null)
			{
				var command = new SqlCommand(sql, _cnn);
				command.Transaction = _transact;
				if (record.IsLoaded)
					command.ExecuteNonQuery();
				else
					record.ID = Convert.ToString(command.ExecuteScalar());
			}
		}


		private string BuildQuery(AssetRecord record)
		{
			if(record.IsNew && record.IsLoaded)
				throw new InvalidOperationException();

			if (record.IsLoaded)
				return SqlForUpdate(record);

			return SqlForInsert(record);
		}

		private string SqlForUpdate(AssetRecord record)
		{
			var sql = new StringBuilder();

			if (record.Modified)
			{
				sql.Append("update Assets set ");

				if (record.Name_Modified)
				{
					sql.AppendFormat("Name = '{0}'", record.Name);
					sql.Append(",");
				}

				if (record.Name2_Modified)
				{
					sql.AppendFormat("Name2 = '{0}'", record.Name2);
					sql.Append(",");
				}

				if (record.Name3_Modified)
				{
					sql.AppendFormat("Name3 = '{0}'", record.Name3);
					sql.Append(",");
				}

				if (record.Name4_Modified)
				{
					sql.AppendFormat("Name4 = '{0}'", record.Name4);
					sql.Append(",");
				}

				if (record.AssetType_Modified)
				{
					sql.AppendFormat("Type = {0},", record.AssetType);
					sql.Append(",");
				}

				sql.Remove(sql.Length - 1, 1);

				sql.AppendFormat(" where ID = {0}", record.ID);

				return sql.ToString();
			}

			return null;
		}

		private string SqlForInsert(AssetRecord record)
		{
			var sql = new StringBuilder();
			sql.Append("insert into Assets(Name, Name2, Name3, Name4, Type) ");
			sql.AppendFormat("values({0}, {1}, {2}, {3}, {4})", 
				"'" + record.Name + "'",
				record.Name2 == null ? "NULL" : "'" + record.Name2 + "'",
				record.Name3 == null ? "NULL" : "'" + record.Name3 + "'",
				record.Name4 == null ? "NULL" : "'" + record.Name4 + "'",
				record.AssetType);
			sql.Append(" ").Append("select cast(scope_identity() as int)");
			return sql.ToString();
		}

		private string GetKey(string id)
		{
			return typeof(AssetRecord).Name + "." + id;
		}
	}
}
