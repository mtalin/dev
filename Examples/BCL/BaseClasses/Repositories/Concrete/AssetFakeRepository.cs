﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseClasses.Records;
using BaseClasses.Repositories.Abstract;

namespace BaseClasses.Repositories.Concrete
{
	internal class AssetFakeRepository:IRepository<AssetRecord>
	{
		private List<AssetRecord> _records = new List<AssetRecord>()
		                                    	{
		                                    		new AssetRecord("1", "Insurance", null, null, null, 7),
		                                    		new AssetRecord("2", "Land: Cambridge, ME", null, null, null, 8)
		                                    	};

		public AssetRecord LoadById(string id)
		{
			return _records.Where(r=>r.ID==id).FirstOrDefault();
		}

		public void Update(AssetRecord asset)
		{
			if(asset.IsNew)
				_records.Add(asset);

			if (asset.IsLoaded)
			{
				_records.Remove(asset);
				_records.Add(asset);
			}
		}

	}
}
