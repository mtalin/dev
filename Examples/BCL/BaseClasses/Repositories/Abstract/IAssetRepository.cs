﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseClasses.Entities;
using BaseClasses.Records;
using System.Data.Common;

namespace BaseClasses.Repositories.Abstract
{
	internal interface IRepository<T>
	{
		T LoadById(string id);
		void Update(T record);
	}
	
}
