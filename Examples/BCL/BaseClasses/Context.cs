﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BaseClasses
{
	public class Context:IDisposable
	{
		private static readonly ThreadLocal<UnitOfWork> CurrentThread = new ThreadLocal<UnitOfWork>();
		private UnitOfWork _unit;
		//private RecordWatcher _watcher;
		private static readonly object Locker = new object();

		public Context()
		{
			NewUnitOfWork();
			_unit = GetUnitOfWork();
		}
		
		public bool Update()
		{
			try
			{
				_unit.BeginTransaction();
				SaveNew();
				SaveChanged();
				_unit.CommitTransaction();
			}
			catch (Exception e)
			{
				_unit.RollbackTransaction();
				return false;
			}

			return true;
		}

		public void BeginTrans()
		{
			_unit.BeginTransaction();			
		}

		public void CommitTrans()
		{
			_unit.CommitTransaction();
		}

		public void RollbackTrans()
		{
			_unit.RollbackTransaction();
		}

		private void SaveNew()
		{
			foreach (var newObj in _unit.NewObjects)
			{
				newObj.Update();
			}
		}

		private void SaveChanged()
		{
			foreach (var changedObj in _unit.ChangedObjects)
			{
				changedObj.Update();
			}
		}

		private static void NewUnitOfWork()
		{
			SetUnitOfWork(new UnitOfWork());
		}

		private static void SetUnitOfWork(UnitOfWork unit)
		{
			CurrentThread.Value = unit;
		}

		public static UnitOfWork GetUnitOfWork()
		{
			lock (Locker)
			{
				return CurrentThread.Value;		
			}
		}

		public void Close()
		{
			Dispose();
		}

		private bool _disposed;

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					_unit.Dispose();
					_unit = null;
				}
			}
			_disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

	}
}
