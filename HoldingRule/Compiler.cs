﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Globalization;

namespace Holding
{
	static class Compiler
	{
		public static int Main(string[] args)
		{
			//string xx = Test("UnitsNumber = sum(t.Asset2, UnitsNumber) * (t.Ratio - 1.0);");
			//args = new string[] { @"Rules.ini" };
			RulesOptions vars = ParseParameters(args.Length == 0 ? new [] { "Rules.ini" }: args);
			if (vars == null)
				return 1;

			string sourceFile = vars["$source"];
			if (!File.Exists(sourceFile))
			{
				Logo();
				Console.WriteLine("File not found \"{0}\".", sourceFile);
				return 1;
			}
			vars.Default("$output", string.Format(CultureInfo.InstalledUICulture, "{0:yyyyMMdd}-1-{1}.sql", DateTime.Today, sourceFile));
			string outputFile = vars["$output"];
			vars.Default("$tool", "hrc " + Version);

			RulesCollection rules = RulesCollection.Parse(sourceFile, vars);
			if (!rules.HasErrors)
				rules.Compile();
			if (rules.HasErrors)
			{
				Console.WriteLine(rules.ErrorMessage);
				return 1;
			}

			TextWriter output;
			if (outputFile == "-")
			{
				output = Console.Out;
			}
			else
			{
				int i = outputFile.IndexOf("-1-", StringComparison.Ordinal);
				if (i >= 0)
				{
					int k = 1;
					while (File.Exists($"{outputFile.Substring(0, i)}-{(char)(k < 10 ? k + '0': k + 'A')}-{outputFile.Substring(i + 3)}"))
					{
						++k;
					}
					outputFile = $"{outputFile.Substring(0, i)}-{(char)(k < 10 ? k + '0': k + 'A')}-{outputFile.Substring(i + 3)}";
				}

				try
				{
					output = File.CreateText(outputFile);
				}
				catch (Exception)
				{
					Console.WriteLine("Cannot open file \"{0}\" for output", outputFile);
					return 1;
				}
			}
			output.Write(rules.BatchHeader);
			output.Write(rules.CompiledRules);
			output.Write(rules.Startup);
			output.Write(rules.BatchFooter);
			output.Dispose();
			return 0;
		}

		private static RulesOptions ParseParameters(string[] args)
		{
			RulesOptions vars = new RulesOptions();

			foreach (string arg in args)
			{
				if (arg == null || arg.Length == 0)
					continue;
				if (arg[0] == '-' || arg[0] == '/')
				{
					int i = arg.IndexOf(':');
					if (i < 1 || i == arg.Length - 1)
						return Usage();

					string argName = arg.Substring(1, i - 1).Trim().ToLowerInvariant();
					string argValue = arg.Substring(i + 1).Trim();
					if (argValue.Length == 0)
						return Usage();

					switch (argName)
					{
						case "o":
						case "out":
						case "output":
							if (vars.Exists("$output"))
								return Usage();
							vars.Add("$output", argValue);
							break;

						case "d":
						case "def":
						case "define":
							i = argValue.IndexOf('=');
							if (i <= 0 || i == argValue.Length - 1)
								return Usage();
							argName = argValue.Substring(0, i).Trim();
							argValue = argValue.Substring(i + 1).Trim();
							if (argName.Length == 0 || argValue.Length == 0)
								return Usage();
							vars[argName] = argValue;
							break;

						case "?":
						case "h":
						case "help":
							return Usage();

						default:
							return Usage();
					}
				}
				else
				{
					if (vars.Exists("$source"))
						return Usage();
					vars.Add("$source", arg);
				}
			}
			if (!vars.Exists("$source"))
				return Usage();

			return vars;
		}

		private static RulesOptions Usage()
		{
			Logo();
			Console.WriteLine("Usage: hrc [ option... ] filename");
			Console.WriteLine();
			Console.WriteLine("Options:");
			Console.WriteLine("    -o[ut]:<file>           specify output file name ('-' for stdout)");
			Console.WriteLine("                                default: source file name with .sql extension");
			Console.WriteLine("    -d[ef]:<name>=<value>   define variable");
			Console.WriteLine();
			Console.WriteLine("Some of useful variables:");
			Console.WriteLine();
			Console.WriteLine("    usd         - ID of USD Asset in the Assets table (default 1059)");
			Console.WriteLine("    $holding    - name of Holding table (default dbo.Holdings)");
			Console.WriteLine("    $schema     - db schema name for generated procedures (default xhc)");
			Console.WriteLine("    $prefix     - name prefix for gerenerated procedures (default z_h_)");
			Console.WriteLine("    $go         - name of starting procedure (default xhc.z_h_Go)");
			return null;
		}

		private static void Logo()
		{
			Console.WriteLine("Holding Rules Compiler version {0} (2008)", Version);
			Console.WriteLine();
		}

		private static string _version;
		private static string Version
		{
			get
			{
				if (_version == null)
				{
					try
					{
						_version = typeof(Compiler).Assembly.GetName().Version.ToString();
					}
					catch
					{
						_version = "0.0";
					}
				}
				return _version;
			}
		}

		static string Test(string program)
		{
			HoldingRuleLexer lexer = new HoldingRuleLexer(new Antlr.Runtime.ANTLRStringStream(program));
			HoldingRuleParser parser = new HoldingRuleParser(new Antlr.Runtime.CommonTokenStream(lexer));

			Program prog;
			try
			{
				parser.Context.Declaration.Define("procedureName", "'TEST'");
				parser.Context.SetValue("procedureName", true);
				parser.program();
				prog = parser.Context;
			}
			catch (Exception e)
			{
				Console.WriteLine(program);
				Console.WriteLine(e.Message);
				Console.WriteLine(e.StackTrace);
				return null;
			}
			if (prog.HasError)
			{
				Console.WriteLine(prog.GetErrorMessage(Environment.NewLine, "error", 0));
			}
			string x = prog.GetText(0);
			return x;
		}

	}
}
