﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Holding
{
	[DebuggerDisplay("{Text}, prty={Priority}, ordered={Ordered}")]
	public struct ExpressionOp
	{
		public string Text { get; }
		public int Priority { get; }
		public bool Ordered { get; }
		private ExpressionOp(string text, int priority)
			: this()
		{
			Text = text.ToUpperInvariant();
			Priority = priority;
		}

		private ExpressionOp(string text, int priority, bool ordered)
			: this()
		{
			Text = text.ToUpperInvariant();
			Priority = priority;
			Ordered = ordered;
		}

		public const int PriorityTop		= 10;
		public const int PriorityBraces		= 9;
		public const int PriorityUnary		= 8;
		public const int PriorityMult		= 7;
		public const int PriorityAdd		= 6;
		public const int PriorityCompare	= 5;
		public const int PriorityAnd		= 4;
		public const int PriorityXor		= 3;
		public const int PriorityOr			= 2;
		public const int PriorityComa		= 1;

		public static readonly ExpressionOp Not		= new ExpressionOp("not", PriorityUnary);
		public static readonly ExpressionOp Neg		= new ExpressionOp("-", PriorityUnary);

		public static readonly ExpressionOp Mult	= new ExpressionOp("*", PriorityMult);
		public static readonly ExpressionOp Div		= new ExpressionOp("/", PriorityMult, true);
		public static readonly ExpressionOp Mod		= new ExpressionOp("%", PriorityMult, true);

		public static readonly ExpressionOp Add		= new ExpressionOp("+", PriorityAdd);
		public static readonly ExpressionOp Sub		= new ExpressionOp("-", PriorityAdd, true);

		public static readonly ExpressionOp Grt		= new ExpressionOp(">", PriorityCompare);
		public static readonly ExpressionOp Lst		= new ExpressionOp("<", PriorityCompare);
		public static readonly ExpressionOp Eq		= new ExpressionOp("=", PriorityCompare);
		public static readonly ExpressionOp GrE		= new ExpressionOp(">=", PriorityCompare);
		public static readonly ExpressionOp LsE		= new ExpressionOp("<=", PriorityCompare);
		public static readonly ExpressionOp NEq		= new ExpressionOp("<>", PriorityCompare);
		public static readonly ExpressionOp In		= new ExpressionOp("in", PriorityCompare);
		public static readonly ExpressionOp NotIn	= new ExpressionOp("not in", PriorityCompare);
		public static readonly ExpressionOp Contains	= new ExpressionOp("contains", PriorityCompare);
		public static readonly ExpressionOp NotContains	= new ExpressionOp("not contains", PriorityCompare);

		public static readonly ExpressionOp And		= new ExpressionOp("and", PriorityAnd);
		public static readonly ExpressionOp Xor		= new ExpressionOp("xor", PriorityXor);
		public static readonly ExpressionOp Or		= new ExpressionOp("or", PriorityOr);

		public static readonly ExpressionOp Comma	= new ExpressionOp(",", PriorityComa);

		public static ExpressionOp Create(string text)
		{
			switch (text.ToUpperInvariant())
			{
				case "NOT": return Not;
				case "~": return Not;
				case "*": return Mult;
				case "/": return Div;
				case "%": return Mod;
				case "+": return Add;
				case "-": return Sub;
				case ">": return Grt;
				case "<": return Lst;
				case "=": return Eq;
				case "==": return Eq;
				case ">=": return GrE;
				case "<=": return LsE;
				case "<>": return NEq;
				case "!=": return NEq;
				case "IN": return In;
				case "NOTIN": return NotIn;
				case "NOT IN": return NotIn;
				case "CONTAINS": return Contains;
				case "NOTCONTAINS": return NotContains;
				case "NOT CONTAINS": return NotContains;
				case "AND": return And;
				case "XOR": return Xor;
				case "OR": return Or;

				default:
					throw new ArgumentOutOfRangeException("text", text, "Undefined operation exception");
			}
		}

		public static bool operator==(ExpressionOp left, ExpressionOp right)
		{
			return left.Text == right.Text;
		}

		public static bool operator!=(ExpressionOp left, ExpressionOp right)
		{
			return left.Text != right.Text;
		}

		public override int GetHashCode()
		{
			return Text.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			if (!(obj is ExpressionOp))
				return false;
			ExpressionOp that = (ExpressionOp)obj;
			return this.Text == that.Text;
		}
	}
}
