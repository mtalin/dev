grammar HoldingRule;

options
{
	language = CSharp2;
//	output = template;
//	output = AST;
}

@namespace
{
	Holding
}

@members
{
	private Program _program = new Program();
	
	public Program Context
	{
		get { return _program; }
	}
	
	public override void ReportError(RecognitionException e)
	{
	    Context.AddError(new SyntaxError(e.Line, e.CharPositionInLine, GetErrorMessage(e, TokenNames)));
	}
}

@header
{
	using System.Collections.Generic;
	using System.Text;
}


program
	returns [bool Value]
	:	statements EOF
		{
			Context.Statements = $statements.Value;
			Value = true;
		}
	;

statements
	returns [StatementsList Value = new StatementsList()]
	:	( statement { Value.Add($statement.Value); }
		)*
	;

statement
	returns [Statement Value]
	:	empty_stmt
		{ return $empty_stmt.Value; }
	|	declaration_stmt
		{ return $declaration_stmt.Value; }
	|	assign_stmt
		{ return $assign_stmt.Value; }
	|	call_stmt
		{ return $call_stmt.Value; }
	|	goto_stmt
		{ return $goto_stmt.Value; }
	|	label_stmt
		{ return $label_stmt.Value; }
	|	if_stmt
		{ return $if_stmt.Value; }
	|	distribute_stmt
		{ return $distribute_stmt.Value; }
	|	block_stmt
		{ return $block_stmt.Value; }
	|	return_stmt
		{ return $return_stmt.Value; }
	;

empty_stmt
	returns [Statement Value]
	:	';'
	;

return_stmt
	returns [Statement Value]
	:	('RETURN'|'Return'|'return') ';'
		{ Value = Statement.Return(); }
	;

block_stmt
	returns [Statement Value]
	:	'{' statements '}'
		{ Value = Statement.Block($statements.Value); }
	;

goto_stmt
	returns [Statement Value]
	:	('GOTO'|'GoTo'|'Goto'|'goto') lbl=IDENTIFIER ';'
		{ Value = Statement.GoTo($lbl.text); }
	;

declaration_stmt
	returns [Statement Value]
	@init { StatementsList slist = new StatementsList(); }
	:	('VAR'|'Var'|'var') d1=declaration { slist.Add($d1.Value); }
		( ',' d2=declaration { slist.Add($d2.Value); } )* ';'
		{ Value = slist; }
	;

fragment
declaration
	returns [Statement Value]
	:	IDENTIFIER tp=type? ('=' ex=expression )?
		{
			Identifier ident = Context.Declare($tp.Value == null ? ExpressionType.Decimal: $tp.Value, $IDENTIFIER.text);
			if ($ex.Value != null)
				Value = Statement.Assign(ident, $ex.Value);
		}
	;

type
	returns [ExpressionType Value]
	:	('INT'|'Int'|'int')
		{ Value = ExpressionType.Integer; }
	|	('LONG'|'Long'|'long')
		{ Value = ExpressionType.Long; }
	|	('STRING'|'String'|'string') ( '(' length ')' )?
		{ Value = new ExpressionType(ExpressionTypeCode.String, $length.text == null || $length.text.ToLowerInvariant() == "max" ? "varchar(max)": "varchar(" + $length.text + ")"); }
	|	('DECIMAL'|'Decimal'|'decimal') ( '(' length (',' precision)? ')' )?
		{ Value = new ExpressionType(ExpressionTypeCode.Numeric,
			$length.text == null || $length.text.ToLowerInvariant() == "max" ? "decimal(19,8)":
			$precision.text == null ? "decimal(" + $length.text + ")":
			"decimal(" + $length.text + "," + $precision.text + ")" ); }
	|	('DECIMAL2'|'Decimal2'|'decimal2')
		{ Value = ExpressionType.Decimal2; }
	|	('MONEY'|'Money'|'money')
		{ Value = ExpressionType.Money; }
	|	('FLOAT'|'Float'|'float')
		{ Value = ExpressionType.Float; }
	|	('DATETIME'|'Datetime'|'datetime')
		{ Value = ExpressionType.DateTime; }
	;

length
	:	INT
	|	('MAX'|'Max'|'max')
	;

precision
	:	INT
	;

label_stmt
	returns [Statement Value]
	:	IDENTIFIER ':'
		{ Value = Statement.Label($IDENTIFIER.text); }
	;

if_stmt
	returns [Statement Value]
	:	('IF'|'If'|'if') '(' test=expression ')' thenStmt=statement
		(options {k=1;}: ('ELSE'|'Else'|'else') elseStmt=statement)?
		{ Value = Statement.If($test.Value, $thenStmt.Value, $elseStmt.Value); }
	;


call_stmt
	returns [Statement Value]
	:	('PRINT'|'Print'|'print') expression ';'
		{ Value = new Procedures.PrintProcedure($expression.Value); }
	|	('ERROR'|'Error'|'error') ('(' ')' | expression) ';'
		{ Value = new Procedures.ErrorProcedure($expression.Value); }
	|	('CREATELOT'|'CreateLot'|'createLot'|'createlot') expression ';'
		{ Value = new Procedures.CreateLotProcedure($expression.Value); }
	|	('CREATELOT'|'CreateLot'|'createLot'|'createlot') zero_expression ';'
		{ Value = new Procedures.CreateLotProcedure(); }
	|	('CLOSELOT'|'CloseLot'|'closeLot'|'closelot') zero_expression ';'
		{ Value = new Procedures.CloseLotProcedure(); }
	|	('SPLITCOSTBASIS'|'SplitCostBasis'|'SplitCostbasis'|'splitCostBasis'|'splitCostbasis'|'splitcostbasis') zero_expression ';'
		{ Value = new Procedures.SplitCostBasisProcedure(); }
	|	('FINDLAST'|'FindLast'|'Findlast'|'findLast'|'findlast') expression ';'
		{ Value = new Procedures.FindLastProcedure($expression.Value); }
	|	('UPDATECASH'|'UpdateCash'|'updateCash'|'updatecash') cash=expression? ';'
		{ Value = new Procedures.UpdateCashProcedure($cash.Value); }
	|	('INLINEQUERY'|'InlineQuery'|'inlineQuery'|'inlinequery') ('(' query=STRING ')' | query=STRING) ';'
		{ Value = new Procedures.InlineQueryProcedure(Expression.UnescapeString($query.text)); }
	|	('RULE'|'Rule'|'rule') ('(' rule=INT ',' count=expression ',' currency=expression ',' asset=expression ')' | rule=INT ',' count=expression ',' currency=expression ',' asset=expression) ';'
		{ Value = new Procedures.RegistryRuleProcedure(Int32.Parse($rule.text), $count.Value, $currency.Value, $asset.Value); }
	|	('MOVEASSETS'|'MoveAssets'|'moveAssets'|'moveassets') source=expression ';'
		{ Value = new Procedures.MoveAssetsProcedure($source.Value); }
	;

distribute_stmt
	returns [Statement Value]
	:	('DISTRIBUTE'|'Distribute'|'distribute')
		'(' asset=expression (',' ('*' | count=expression))? ')' statement
		{
			Value = Statement.Distribute($asset.Value, $count.Value, $statement.Value);
		}
	;

zero_expression
	:	('(' ')')?
	;

assign_stmt
	returns [Statements.AssignStatement Value]
	:	left_value op=(EQUAL | OPEQUAL) expression ';'
		{ Value = Statement.Assign($left_value.Value, $op.Text[0], $expression.Value); }
	;

left_value
	returns [List<Identifier> Value = new List<Identifier>()]
	:	i0=identifier				{ Value.Add($i0.Value); }
	|	'(' i1=identifier			{ Value.Add($i1.Value); }
			( ',' i2=identifier		{ Value.Add($i2.Value); } )*
		')'
	;

expression
	returns [Expression Value]
	:	left=and_expression			{ Value = $left.Value; }
		(OR right=and_expression	{ Value = new JoinedExpression(ExpressionOp.Or, Value, $right.Value); }
		)*
	;

and_expression
	returns [Expression Value]
	:	left=test_expression		{ Value = $left.Value; }
		(AND right=test_expression	{ Value = new JoinedExpression(ExpressionOp.And, Value, $right.Value); }
		)*
	;

test_expression
	returns [Expression Value]
	:	left=math_expression { Value = $left.Value; }
		(	(op=EQUAL | op=COMPARE | not=NOT? op=(IN|CONTAINS)) right=math_expression
			{
				string opText = $op.text.ToUpperInvariant();	// '=' | '==' | '!=' | '<>' | '<=' | '<' | '>=' | '>' | 'IN'
				if (opText == "==")
					opText = "=";
				else if (opText == "!=")
					opText = "<>";			// '=' | '<>' | '<=' | '<' | '>=' | '>'
				if ($not != null)
					opText = "NOT" + opText;
				Expression leftExp = $left.Value;
				Expression rightExp = $right.Value;
				if (leftExp.IsNull)
				{
					if (opText == "IN" || opText == "NOTIN" || opText == "CONTAINS" || opText == "NOCONTAINS")
						Value = Expression.Value(ExpressionType.Boolean, "0");
					else if (rightExp.IsNull)
						Value = Expression.Value(ExpressionType.Boolean, opText.IndexOf('=') >= 0 ? "1": "0");
					else
						Value = Expression.Value(ExpressionType.Boolean, opText.IndexOf('<') >= 0 ? "1": "0");
				}
				else if (rightExp.IsNull)
				{
					if (opText == "IN" || opText == "NOTIN" || opText == "CONTAINS" || opText == "NOCONTAINS")
						Value = Expression.Value(ExpressionType.Boolean, "0");
					else
						Value = Expression.Value(ExpressionType.Boolean, opText.IndexOf('>') >= 0 ? "1": "0");
				}
				else
				{
					Value = new JoinedExpression(ExpressionOp.Create(opText), leftExp, rightExp);
				}
			}
		)?
	;

math_expression
	returns [Expression Value]
	:	left=mult_expression			{ Value = $left.Value; }
		(	op=('+'|'-') right=mult_expression
										{ Value = new JoinedExpression(ExpressionOp.Create($op.text), Value, $right.Value); }
		)*
	;

mult_expression
	returns [Expression Value]
	:	left=unary_expression			{ Value = $left.Value; }
		(	op=('*'|'/'|'%') right=unary_expression
										{ Value = new JoinedExpression(ExpressionOp.Create($op.text), Value, $right.Value); }
		)*
	;


unary_expression
	returns [Expression Value]
	@init { ExpressionOp op = ExpressionOp.Neg; }
	:	primary_expression
		{ Value = $primary_expression.Value; }
	|	('-'	{ op = ExpressionOp.Neg; }
		|NOT	{ op = ExpressionOp.Not; }
		) value=primary_expression
		{ Value = new UnaryExpression(op, $value.Value); }
	;

	
primary_expression
	returns [Expression Value]
	@init { List<Expression> list = null; }
	:	identifier
		{ Value = $identifier.Value; }
	|	constant
		{ Value = $constant.Value; }
	|	internal_function
		{ Value = $internal_function.Value; }
	|	'(' left=expression { list = new List<Expression>(); list.Add($left.Value); }
			( ',' right=expression { list.Add($right.Value); } )*
		')'
		{
		Value = list.Count == 1 ? list[0]: Function.Coalesce(list.ToArray());
		}
	;


internal_function
	returns [Expression Value]
	:	('ABS'|'Abs'|'abs') '(' e1=math_expression ')'
		{ Value = Function.Abs($e1.Value); }
	|	('DATETIME'|'DateTime'|'Datetime'|'dateTime'|'datetime') '(' ')'
		{ Value = Function.DateTime(); }
	|	('DATE'|'Date'|'date') '(' ')'
		{ Value = Function.Date(); }
	|	('FMVPRICE'|'FmvPrice'|'Fmvprice'|'fmvPrice'|'fmvprice') '(' e1=math_expression ',' e2=math_expression ')'
		{ Value = Function.FmvPrice($e1.Value, $e2.Value); }
	|	('IF'|'If'|'if') '(' tst=expression ',' thn=expression ',' els=expression ')'
		{ Value = Function.If($tst.Value, $thn.Value, $els.Value); }
	|	('MAX'|'Max'|'max') '(' e1=math_expression ',' e2=math_expression ')'
		{ Value = Function.Max($e1.Value, $e2.Value); }
	|	('MIN'|'Min'|'min') '(' e1=math_expression ',' e2=math_expression ')'
		{ Value = Function.Min($e1.Value, $e2.Value); }
	|	('ROUND'|'Round'|'round') '(' e1=math_expression (',' e2=math_expression)? ')'
		{ Value = Function.Round($e1.Value, $e2.Value); }
	|	('SUM'|'Sum'|'sum') '(' asset=expression ',' field=IDENTIFIER ')'
		{ Value = Function.Sum($asset.Value, $field.text); }
	|	('FOUND'|'Found'|'found') '(' ')'
		{ Value = Function.Found(); }
	|	('DISTRIBUTED'|'Distributed'|'distributed') '(' ')'
		{ Value = Function.Distributed(); }
	|	('TIME'|'Time'|'time') '(' ')'
		{ Value = Function.Time(); }
	;


constant
	returns [Expression Value]
	:	DEC
		{ Value = Expression.Value(ExpressionType.Decimal, $DEC.text); }
	|	INT
		{ Value = Expression.Value($INT.text.Length > 9 ? ExpressionType.Long: ExpressionType.Integer, $INT.text); }
	|	STRING	{  }
		{ Value = Expression.Value(ExpressionType.String, $STRING.text); }
	|	('NULL'|'Null'|'null')
		{ Value = Expression.Value(ExpressionType.Undefined, "null"); }
	|	('TRUE'|'True'|'true')
		{ Value = Expression.Value(ExpressionType.Boolean, "1"); }
	|	('FALSE'|'False'|'false')
		{ Value = Expression.Value(ExpressionType.Boolean, "0"); }
	;

identifier
	returns [Identifier Value]
	:	i=TRANSACT_FIELD
		{ Value = Context.Declaration.FindIdentifier($i.text); }
	|	i=IDENTIFIER
		{ Value = Context.Declaration.FindIdentifier($i.text); }
	;

TRANSACT_FIELD
	:	('t'|'T'|'h'|'H'|'w'|'W'|'v'|'V')'.'('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*
	;

number
	:	DEC
	|	INT
	;

OR
	:	'OR'|'Or'|'or'
	|	'||'
	;

AND
	:	'AND'|'And'|'and'
	|	'&&'
	;

NOT
	:	'NOT' | 'Not' | 'not'
	|	'!'
	;

INT
	:	('0'..'9')+
	;

DEC
	:	'.'('0'..'9')+
	|	('0'..'9')+'.'('0'..'9')*
	;

STRING
	:	STRING1
	|	STRING2
	;

fragment
STRING1
	:	'\'' STRINGCHAR1* '\''
	;
fragment
STRING2
	:	'"' STRINGCHAR2* '"'
	;
fragment
STRINGCHAR1
	:	~'\''
	|	'\'\''
	;
fragment
STRINGCHAR2
	:	~'"'
	|	'""'
	;

COMPARE
	:	'==' | '!=' | '<>' | '<=' | '<' | '>=' | '>' 
	;

IN
	:	'IN'|'In'|'in'
	;

CONTAINS
	:	'CONTAINS'|'Contains'|'contains'
	;

EQUAL
	:	'='
	;

OPEQUAL
	:	'+=' | '-=' | '*=' | '/=' | '%='
//	:	'+' '=' | '-' '=' | '*' '=' | '/' '=' | '%' '='
	;

//PLUS
//	:	'+' | '-'
//	;

//MULT
//	:	'*' | '/' | '%'
//	;

IDENTIFIER
	:	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*
	;	

WS
//	:	(~('\u0000'..'\u0020'))+ {$channel=HIDDEN;}
	:	(' '|'\t'|'\n'|'\r')+ {$channel=HIDDEN;}
	;

COMMENTS
	:	'//' (~'\n')* '\n' {$channel=HIDDEN;}
	|	'/*' ( options {greedy=false;}: .)* '*/' {$channel=HIDDEN;}
	;
