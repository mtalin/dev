﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics;

namespace Holding
{

	[DebuggerDisplay("{_text} type={Type.SqlType} prty={Priority}")]
	public class Expression
	{
		private string _text;
		private ExpressionType _type;
		private int _priority;
		private ICollection<string> _variables;
		private static readonly string[] NoVariables = new string[0];

		protected Expression()
		{
			_variables = NoVariables;
		}

		protected Expression(ExpressionType type, string text, int priority, ICollection<string> variables)
		{
			_type = type;
			_text = text;
			_priority = priority;
			_variables = variables ?? NoVariables;
		 }

		public int Priority
		{
			get { return _priority; }
			protected set { _priority = value; }
		}

		public ICollection<string> Variables
		{
			get { return _variables; }
			protected set { _variables = value; }
		}

		public ExpressionType Type
		{
			get { return _type; }
			protected set { _type = value; }
		}

		public virtual string GetText(Program context)
		{
			return _text;
		}

		public bool IsNull
		{
			get { return false; } // GetText() == "null"; }
		}

		private static char[] _ops = new char[] { '+', '-', '*', '/', '%', '(', ')', '!', '|', '&' };
		public virtual bool IsSimple
		{
			get{ return _text.IndexOfAny(_ops) < 0; }
		}


		public virtual bool IsUnarySimple
		{
			get { return IsSimple; }
		}

		public static Expression Value(ExpressionType type, string value)
		{
			if (value == null)
				return new Expression(ExpressionType.Undefined, "null", ExpressionOp.PriorityTop, null);
			if (type.TypeCode != ExpressionTypeCode.String)
				return new Expression(type, value, ExpressionOp.PriorityTop, null);
			if (value.Length < 2)
				return new Expression(ExpressionType.String, String.Empty, ExpressionOp.PriorityTop, null);
			value = UnescapeString(value);
			if (value.IndexOf('\'') >= 0)
				value = value.Replace("'", "''");
			return new Expression(ExpressionType.String, "'" + value + "'", ExpressionOp.PriorityTop, null);
		}

		public static string UnescapeString(string value)
		{
			char c = value[value.Length - 1];
			value = value.Substring(1, value.Length - 2);
			if (value.IndexOf(c) >= 0)
				value = value.Replace(new string(c, 2), new string(c, 1));
			return value;
		}

		//public static Expression Join(ExpressionOp operation, Expression left, params Expression[] rightList)
		//{
		//    if (left == null)
		//        throw new ArgumentNullException("left");
		//    if (rightList == null)
		//        throw new ArgumentNullException("rightList");
		//    if (rightList.Length == 0)
		//        throw new ArgumentException("List is empty", "rightList");
		//    if (rightList.Length > 1 && operation != ExpressionOp.Comma)
		//        throw new Antlr.Runtime.RecognitionException("argument <right> contains too many elements"); // ArgumentException("List too big", "rightList");
		//    StringBuilder sb = new StringBuilder();
		//    if (operation == ExpressionOp.In || operation == ExpressionOp.NotIn)
		//    {
		//        return new Expression(ExpressionType.Boolean,
		//            "charindex(" + left.GetText() + ", " + rightList[0].GetText() + ")" + (operation == ExpressionOp.In ? " > 0": " = 0"),
		//            ExpressionOp.PriorityCompare, JoinList(left.Variables, rightList[0].Variables));
		//    }

		//    ExpressionType resultType = left.Type;
		//    if (operation.Priority == ExpressionOp.PriorityCompare)
		//        resultType = ExpressionType.Boolean;
		//    if (operation == ExpressionOp.Comma)
		//        sb.Append("coalesce(");
		//    if (left.Priority < operation.Priority)
		//        sb.Append("(").Append(left.GetText()).Append(")");
		//    else
		//        sb.Append(left.GetText());
		//    List<string> idents = new List<string>(left.Variables);
		//    bool letter = Char.IsLetter(operation.Text[0]);
		//    int rightCount = 0;
		//    foreach (var item in rightList)
		//    {
		//        if (item != null)
		//        {
		//            ++rightCount;
		//            if (letter && Char.IsLetterOrDigit(sb[sb.Length - 1]))
		//                sb.Append(' ');
		//            sb.Append(operation.Text);
		//            if (item.Priority < operation.Priority)
		//            {
		//                sb.Append("(").Append(item.GetText()).Append(")");
		//            }
		//            else if (item.Priority == operation.Priority && operation.Ordered)
		//            {
		//                sb.Append("(").Append(item.GetText()).Append(")");
		//            }
		//            else
		//            {
		//                string text = item.GetText();
		//                if (letter && (Char.IsLetterOrDigit(text[0]) || text[0] == '@'))
		//                    sb.Append(' ');
		//                sb.Append(item.GetText());
		//            }
		//            idents.AddRange(item.Variables);
		//        }
		//    }
		//    if (rightCount == 0)
		//        return left;
		//    if (operation == ExpressionOp.Comma)
		//        sb.Append(")");
		//    return new Expression(resultType, sb.ToString(), operation.Priority, idents.Count == 0 ? _noVariables: idents);
		//}


		public static ICollection<string> JoinList(ICollection<string> left, ICollection<string> right)
		{
			if (left == null || left.Count == 0)
				return right ?? NoVariables;
			if (right == null || right.Count == 0)
				return left;
			if (left.Equals(right))
				return left;
			var joined = new List<string>(left);
			foreach (var item in right)
			{
				if (item != null && !left.Contains(item))
					joined.Add(item);
			}
			return joined;
		}

		public static IEnumerable<string> JoinList(ICollection<string> left, params string[] right)
		{
			if (left == null || left.Count == 0)
				return right ?? NoVariables;
			if (right == null || right.Length == 0)
				return left;
			var joined = new List<string>(left);
			foreach (var item in right)
			{
				if (item != null && !left.Contains(item))
					joined.Add(item);
			}
			return joined;
		}
	}


	public class JoinedExpression: Expression
	{
		private Expression _left;
		private Expression _right;
		private ExpressionOp _operation;

		public JoinedExpression(ExpressionOp operation, Expression left, Expression right)
		{
			if (left == null)
				throw new ArgumentNullException(nameof(left));
			if (right == null)
				throw new ArgumentNullException(nameof(right));
			if (operation == ExpressionOp.Comma)
				throw new ArgumentException("Comma operator is not supported");
			_left = left;
			_right = right;
			_operation = operation;
			Priority = operation.Priority;
			Variables = JoinList(left.Variables, right.Variables);
			Type = operation.Priority == ExpressionOp.PriorityCompare ? ExpressionType.Boolean: _left.Type;
		}

		public override bool IsSimple
		{
			get { return false; }
		}

		public override string GetText(Program context)
		{
			StringBuilder sb = new StringBuilder();
			string leftText = _left.GetText(context);
			string rightText = _right.GetText(context);
			if (_operation == ExpressionOp.In || _operation == ExpressionOp.NotIn)
			{
				Functions.CoalesceFunction coalesce = _right as Functions.CoalesceFunction;
				if (coalesce != null)
					rightText = coalesce.GetText(context, true);
				else if (rightText.Length == 0)
					rightText = "(null)";
				else
					rightText = "(" + rightText + ")";
				return leftText + (_operation == ExpressionOp.In ? " in ": " not in ") + rightText;
			}
			if (_operation == ExpressionOp.Contains || _operation == ExpressionOp.NotContains)
			{
				return "charindex(" + rightText + ", " + leftText + ")" + (_operation == ExpressionOp.Contains ? " > 0": " = 0");
			}
			if (_operation == ExpressionOp.Eq)
				if (rightText.ToUpperInvariant() == "NULL")
					return leftText + " is null";
				else if (leftText.ToUpperInvariant() == "NULL")
					return rightText + " is null";
			if (_operation == ExpressionOp.NEq)
				if (rightText.ToUpperInvariant() == "NULL")
					return leftText + " is not null";
				else if (leftText.ToUpperInvariant() == "NULL")
					return rightText + " is not null";

			if (_left.Priority < _operation.Priority)
				sb.Append("(").Append(leftText).Append(")");
			else
				sb.Append(leftText);
			sb.Append(' ').Append(_operation.Text).Append(' ');
			if ((_right.Priority < _operation.Priority) || (_right.Priority == _operation.Priority && _operation.Ordered))
				sb.Append("(").Append(rightText).Append(")");
			else
				sb.Append(rightText);
			return sb.ToString();
		}

	}

	public class UnaryExpression: Expression
	{
		private Expression _value;
		private ExpressionOp _operation;

		public UnaryExpression(ExpressionOp operation, Expression value)
		{
			if (value == null)
				throw new ArgumentNullException("value");
			if (operation.Priority != ExpressionOp.PriorityUnary)
				throw new ArgumentOutOfRangeException("operation", operation, "Must be an unary operation");
			_value = value;
			_operation = operation;
			Priority = ExpressionOp.PriorityUnary;
			Variables = _value.Variables;
			Type = _value.Type;
		}

		public override bool IsSimple
		{
			get { return false; }
		}

		public override bool IsUnarySimple
		{
			get
			{
				return _value.IsSimple;
			}
		}

		public override string GetText(Program context)
		{
			StringBuilder sb = new StringBuilder(_operation.Text);
			if (_value.Priority < _operation.Priority)
			{
				sb.Append("(").Append(_value.GetText(context)).Append(")");
			}
			else
			{
				string txt = _value.GetText(context);
				if (_operation.Text == "not" && txt[0] != '(')
					sb.Append(' ');
				sb.Append(txt);
			}
			return sb.ToString();
		}
	}


	[DebuggerDisplay("{Name} type={Type.SqlType} readonly={IsReadonly}")]
	public class Identifier: Expression
	{
		public string Name { get; private set; }
		public bool IsReadonly { get; private set; }

		public Identifier(ExpressionType type, string name, string text, bool readOnly)
			: base(type, text, ExpressionOp.PriorityTop, new[] { name })
		{
			Name = name;
			IsReadonly = readOnly;
		}

		public Identifier(ExpressionType type, string name, string text)
			: base(type, text, ExpressionOp.PriorityTop, new[] { name })
		{
			Name = name;
		}

		public Identifier(ExpressionType type, string name)
			: base(type, "${{[" + name + "]}}", ExpressionOp.PriorityTop, new[] { name })
		{
			Name = name;
		}
	}
}
