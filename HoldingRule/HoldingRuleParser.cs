// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\output\\HoldingRule.g 2011-08-30 16:14:29

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


	using System.Collections.Generic;
	using System.Text;


using System;
using System.Collections.Generic;
using Antlr.Runtime;
using Stack = System.Collections.Generic.Stack<object>;
using List = System.Collections.IList;
using ArrayList = System.Collections.Generic.List<object>;

namespace 
	Holding

{
[System.CodeDom.Compiler.GeneratedCode("ANTLR", "3.3 Nov 30, 2010 12:45:30")]
[System.CLSCompliant(false)]
public partial class HoldingRuleParser : Antlr.Runtime.Parser
{
	internal static readonly string[] tokenNames = new string[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "IDENTIFIER", "INT", "STRING", "EQUAL", "OPEQUAL", "OR", "AND", "COMPARE", "NOT", "IN", "CONTAINS", "DEC", "TRANSACT_FIELD", "STRING1", "STRING2", "STRINGCHAR1", "STRINGCHAR2", "WS", "COMMENTS", "';'", "'RETURN'", "'Return'", "'return'", "'{'", "'}'", "'GOTO'", "'GoTo'", "'Goto'", "'goto'", "'VAR'", "'Var'", "'var'", "','", "'INT'", "'Int'", "'int'", "'LONG'", "'Long'", "'long'", "'STRING'", "'String'", "'string'", "'('", "')'", "'DECIMAL'", "'Decimal'", "'decimal'", "'DECIMAL2'", "'Decimal2'", "'decimal2'", "'MONEY'", "'Money'", "'money'", "'FLOAT'", "'Float'", "'float'", "'DATETIME'", "'Datetime'", "'datetime'", "'MAX'", "'Max'", "'max'", "':'", "'IF'", "'If'", "'if'", "'ELSE'", "'Else'", "'else'", "'PRINT'", "'Print'", "'print'", "'ERROR'", "'Error'", "'error'", "'CREATELOT'", "'CreateLot'", "'createLot'", "'createlot'", "'CLOSELOT'", "'CloseLot'", "'closeLot'", "'closelot'", "'SPLITCOSTBASIS'", "'SplitCostBasis'", "'SplitCostbasis'", "'splitCostBasis'", "'splitCostbasis'", "'splitcostbasis'", "'FINDLAST'", "'FindLast'", "'Findlast'", "'findLast'", "'findlast'", "'UPDATECASH'", "'UpdateCash'", "'updateCash'", "'updatecash'", "'INLINEQUERY'", "'InlineQuery'", "'inlineQuery'", "'inlinequery'", "'RULE'", "'Rule'", "'rule'", "'MOVEASSETS'", "'MoveAssets'", "'moveAssets'", "'moveassets'", "'DISTRIBUTE'", "'Distribute'", "'distribute'", "'*'", "'+'", "'-'", "'/'", "'%'", "'ABS'", "'Abs'", "'abs'", "'DateTime'", "'dateTime'", "'DATE'", "'Date'", "'date'", "'FMVPRICE'", "'FmvPrice'", "'Fmvprice'", "'fmvPrice'", "'fmvprice'", "'MIN'", "'Min'", "'min'", "'ROUND'", "'Round'", "'round'", "'SUM'", "'Sum'", "'sum'", "'FOUND'", "'Found'", "'found'", "'DISTRIBUTED'", "'Distributed'", "'distributed'", "'TIME'", "'Time'", "'time'", "'NULL'", "'Null'", "'null'", "'TRUE'", "'True'", "'true'", "'FALSE'", "'False'", "'false'"
	};
	public const int EOF=-1;
	public const int T__23=23;
	public const int T__24=24;
	public const int T__25=25;
	public const int T__26=26;
	public const int T__27=27;
	public const int T__28=28;
	public const int T__29=29;
	public const int T__30=30;
	public const int T__31=31;
	public const int T__32=32;
	public const int T__33=33;
	public const int T__34=34;
	public const int T__35=35;
	public const int T__36=36;
	public const int T__37=37;
	public const int T__38=38;
	public const int T__39=39;
	public const int T__40=40;
	public const int T__41=41;
	public const int T__42=42;
	public const int T__43=43;
	public const int T__44=44;
	public const int T__45=45;
	public const int T__46=46;
	public const int T__47=47;
	public const int T__48=48;
	public const int T__49=49;
	public const int T__50=50;
	public const int T__51=51;
	public const int T__52=52;
	public const int T__53=53;
	public const int T__54=54;
	public const int T__55=55;
	public const int T__56=56;
	public const int T__57=57;
	public const int T__58=58;
	public const int T__59=59;
	public const int T__60=60;
	public const int T__61=61;
	public const int T__62=62;
	public const int T__63=63;
	public const int T__64=64;
	public const int T__65=65;
	public const int T__66=66;
	public const int T__67=67;
	public const int T__68=68;
	public const int T__69=69;
	public const int T__70=70;
	public const int T__71=71;
	public const int T__72=72;
	public const int T__73=73;
	public const int T__74=74;
	public const int T__75=75;
	public const int T__76=76;
	public const int T__77=77;
	public const int T__78=78;
	public const int T__79=79;
	public const int T__80=80;
	public const int T__81=81;
	public const int T__82=82;
	public const int T__83=83;
	public const int T__84=84;
	public const int T__85=85;
	public const int T__86=86;
	public const int T__87=87;
	public const int T__88=88;
	public const int T__89=89;
	public const int T__90=90;
	public const int T__91=91;
	public const int T__92=92;
	public const int T__93=93;
	public const int T__94=94;
	public const int T__95=95;
	public const int T__96=96;
	public const int T__97=97;
	public const int T__98=98;
	public const int T__99=99;
	public const int T__100=100;
	public const int T__101=101;
	public const int T__102=102;
	public const int T__103=103;
	public const int T__104=104;
	public const int T__105=105;
	public const int T__106=106;
	public const int T__107=107;
	public const int T__108=108;
	public const int T__109=109;
	public const int T__110=110;
	public const int T__111=111;
	public const int T__112=112;
	public const int T__113=113;
	public const int T__114=114;
	public const int T__115=115;
	public const int T__116=116;
	public const int T__117=117;
	public const int T__118=118;
	public const int T__119=119;
	public const int T__120=120;
	public const int T__121=121;
	public const int T__122=122;
	public const int T__123=123;
	public const int T__124=124;
	public const int T__125=125;
	public const int T__126=126;
	public const int T__127=127;
	public const int T__128=128;
	public const int T__129=129;
	public const int T__130=130;
	public const int T__131=131;
	public const int T__132=132;
	public const int T__133=133;
	public const int T__134=134;
	public const int T__135=135;
	public const int T__136=136;
	public const int T__137=137;
	public const int T__138=138;
	public const int T__139=139;
	public const int T__140=140;
	public const int T__141=141;
	public const int T__142=142;
	public const int T__143=143;
	public const int T__144=144;
	public const int T__145=145;
	public const int T__146=146;
	public const int T__147=147;
	public const int T__148=148;
	public const int T__149=149;
	public const int T__150=150;
	public const int T__151=151;
	public const int T__152=152;
	public const int T__153=153;
	public const int T__154=154;
	public const int T__155=155;
	public const int T__156=156;
	public const int T__157=157;
	public const int T__158=158;
	public const int T__159=159;
	public const int T__160=160;
	public const int IDENTIFIER=4;
	public const int INT=5;
	public const int STRING=6;
	public const int EQUAL=7;
	public const int OPEQUAL=8;
	public const int OR=9;
	public const int AND=10;
	public const int COMPARE=11;
	public const int NOT=12;
	public const int IN=13;
	public const int CONTAINS=14;
	public const int DEC=15;
	public const int TRANSACT_FIELD=16;
	public const int STRING1=17;
	public const int STRING2=18;
	public const int STRINGCHAR1=19;
	public const int STRINGCHAR2=20;
	public const int WS=21;
	public const int COMMENTS=22;

	// delegates
	// delegators

	#if ANTLR_DEBUG
		private static readonly bool[] decisionCanBacktrack =
			new bool[]
			{
				false, // invalid decision
				false, false, false, false, false, false, false, false, false, false, 
				false, false, false, false, false, false, false, false, false, false, 
				false, false, false, false, false, false, false, false, false, false, 
				false, false, false, false, false
			};
	#else
		private static readonly bool[] decisionCanBacktrack = new bool[0];
	#endif
	public HoldingRuleParser( ITokenStream input )
		: this( input, new RecognizerSharedState() )
	{
	}
	public HoldingRuleParser(ITokenStream input, RecognizerSharedState state)
		: base(input, state)
	{

		OnCreated();
	}
		

	public override string[] TokenNames { get { return HoldingRuleParser.tokenNames; } }
	public override string GrammarFileName { get { return "C:\\output\\HoldingRule.g"; } }


		private Program _program = new Program();
		
		public Program Context
		{
			get { return _program; }
		}
		
		public override void ReportError(RecognitionException e)
		{
		    Context.AddError(new SyntaxError(e.Line, e.CharPositionInLine, GetErrorMessage(e, TokenNames)));
		}



 	protected virtual void OnCreated() {}
	protected virtual void EnterRule(string ruleName, int ruleIndex) {}
	protected virtual void LeaveRule(string ruleName, int ruleIndex) {}


    protected virtual void Enter_program() {}
    protected virtual void Leave_program() {}

    // $ANTLR start "program"
    // C:\\output\\HoldingRule.g:37:1: program returns [bool Value] : statements EOF ;
    [GrammarRule("program")]
    public bool program()
    {

        bool Value = default(bool);

        StatementsList statements1 = default(StatementsList);

    	try { DebugEnterRule(GrammarFileName, "program");
    	DebugLocation(37, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:39:2: ( statements EOF )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:39:4: statements EOF
    		{
    		DebugLocation(39, 4);
    		PushFollow(Follow._statements_in_program50);
    		statements1=statements();
    		PopFollow();

    		DebugLocation(39, 15);
    		Match(input,EOF,Follow._EOF_in_program52); 
    		DebugLocation(40, 3);

    					Context.Statements = statements1;
    					Value = true;
    				

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(44, 1);
    	} finally { DebugExitRule(GrammarFileName, "program"); }
    	return Value;

    }
    // $ANTLR end "program"


    protected virtual void Enter_statements() {}
    protected virtual void Leave_statements() {}

    // $ANTLR start "statements"
    // C:\\output\\HoldingRule.g:46:1: statements returns [StatementsList Value = new StatementsList()] : ( statement )* ;
    [GrammarRule("statements")]
    private StatementsList statements()
    {

        StatementsList Value =  new StatementsList();

        Statement statement2 = default(Statement);

    	try { DebugEnterRule(GrammarFileName, "statements");
    	DebugLocation(46, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:48:2: ( ( statement )* )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:48:4: ( statement )*
    		{
    		DebugLocation(48, 4);
    		// C:\\output\\HoldingRule.g:48:4: ( statement )*
    		try { DebugEnterSubRule(1);
    		while (true)
    		{
    			int alt1=2;
    			try { DebugEnterDecision(1, decisionCanBacktrack[1]);
    			int LA1_0 = input.LA(1);

    			if ((LA1_0==IDENTIFIER||LA1_0==TRANSACT_FIELD||(LA1_0>=23 && LA1_0<=27)||(LA1_0>=29 && LA1_0<=35)||LA1_0==46||(LA1_0>=67 && LA1_0<=69)||(LA1_0>=73 && LA1_0<=115)))
    			{
    				alt1=1;
    			}


    			} finally { DebugExitDecision(1); }
    			switch ( alt1 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:48:6: statement
    				{
    				DebugLocation(48, 6);
    				PushFollow(Follow._statement_in_statements74);
    				statement2=statement();
    				PopFollow();

    				DebugLocation(48, 16);
    				 Value.Add(statement2); 

    				}
    				break;

    			default:
    				goto loop1;
    			}
    		}

    		loop1:
    			;

    		} finally { DebugExitSubRule(1); }


    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(50, 1);
    	} finally { DebugExitRule(GrammarFileName, "statements"); }
    	return Value;

    }
    // $ANTLR end "statements"


    protected virtual void Enter_statement() {}
    protected virtual void Leave_statement() {}

    // $ANTLR start "statement"
    // C:\\output\\HoldingRule.g:52:1: statement returns [Statement Value] : ( empty_stmt | declaration_stmt | assign_stmt | call_stmt | goto_stmt | label_stmt | if_stmt | distribute_stmt | block_stmt | return_stmt );
    [GrammarRule("statement")]
    private Statement statement()
    {

        Statement Value = default(Statement);

        Statement empty_stmt3 = default(Statement);
        Statement declaration_stmt4 = default(Statement);
        Statements.AssignStatement assign_stmt5 = default(Statements.AssignStatement);
        Statement call_stmt6 = default(Statement);
        Statement goto_stmt7 = default(Statement);
        Statement label_stmt8 = default(Statement);
        Statement if_stmt9 = default(Statement);
        Statement distribute_stmt10 = default(Statement);
        Statement block_stmt11 = default(Statement);
        Statement return_stmt12 = default(Statement);

    	try { DebugEnterRule(GrammarFileName, "statement");
    	DebugLocation(52, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:54:2: ( empty_stmt | declaration_stmt | assign_stmt | call_stmt | goto_stmt | label_stmt | if_stmt | distribute_stmt | block_stmt | return_stmt )
    		int alt2=10;
    		try { DebugEnterDecision(2, decisionCanBacktrack[2]);
    		try
    		{
    			alt2 = dfa2.Predict(input);
    		}
    		catch (NoViableAltException nvae)
    		{
    			DebugRecognitionException(nvae);
    			throw;
    		}
    		} finally { DebugExitDecision(2); }
    		switch (alt2)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:54:4: empty_stmt
    			{
    			DebugLocation(54, 4);
    			PushFollow(Follow._empty_stmt_in_statement97);
    			empty_stmt3=empty_stmt();
    			PopFollow();

    			DebugLocation(55, 3);
    			 return empty_stmt3; 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:56:4: declaration_stmt
    			{
    			DebugLocation(56, 4);
    			PushFollow(Follow._declaration_stmt_in_statement106);
    			declaration_stmt4=declaration_stmt();
    			PopFollow();

    			DebugLocation(57, 3);
    			 return declaration_stmt4; 

    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:58:4: assign_stmt
    			{
    			DebugLocation(58, 4);
    			PushFollow(Follow._assign_stmt_in_statement115);
    			assign_stmt5=assign_stmt();
    			PopFollow();

    			DebugLocation(59, 3);
    			 return assign_stmt5; 

    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:60:4: call_stmt
    			{
    			DebugLocation(60, 4);
    			PushFollow(Follow._call_stmt_in_statement124);
    			call_stmt6=call_stmt();
    			PopFollow();

    			DebugLocation(61, 3);
    			 return call_stmt6; 

    			}
    			break;
    		case 5:
    			DebugEnterAlt(5);
    			// C:\\output\\HoldingRule.g:62:4: goto_stmt
    			{
    			DebugLocation(62, 4);
    			PushFollow(Follow._goto_stmt_in_statement133);
    			goto_stmt7=goto_stmt();
    			PopFollow();

    			DebugLocation(63, 3);
    			 return goto_stmt7; 

    			}
    			break;
    		case 6:
    			DebugEnterAlt(6);
    			// C:\\output\\HoldingRule.g:64:4: label_stmt
    			{
    			DebugLocation(64, 4);
    			PushFollow(Follow._label_stmt_in_statement142);
    			label_stmt8=label_stmt();
    			PopFollow();

    			DebugLocation(65, 3);
    			 return label_stmt8; 

    			}
    			break;
    		case 7:
    			DebugEnterAlt(7);
    			// C:\\output\\HoldingRule.g:66:4: if_stmt
    			{
    			DebugLocation(66, 4);
    			PushFollow(Follow._if_stmt_in_statement151);
    			if_stmt9=if_stmt();
    			PopFollow();

    			DebugLocation(67, 3);
    			 return if_stmt9; 

    			}
    			break;
    		case 8:
    			DebugEnterAlt(8);
    			// C:\\output\\HoldingRule.g:68:4: distribute_stmt
    			{
    			DebugLocation(68, 4);
    			PushFollow(Follow._distribute_stmt_in_statement160);
    			distribute_stmt10=distribute_stmt();
    			PopFollow();

    			DebugLocation(69, 3);
    			 return distribute_stmt10; 

    			}
    			break;
    		case 9:
    			DebugEnterAlt(9);
    			// C:\\output\\HoldingRule.g:70:4: block_stmt
    			{
    			DebugLocation(70, 4);
    			PushFollow(Follow._block_stmt_in_statement169);
    			block_stmt11=block_stmt();
    			PopFollow();

    			DebugLocation(71, 3);
    			 return block_stmt11; 

    			}
    			break;
    		case 10:
    			DebugEnterAlt(10);
    			// C:\\output\\HoldingRule.g:72:4: return_stmt
    			{
    			DebugLocation(72, 4);
    			PushFollow(Follow._return_stmt_in_statement178);
    			return_stmt12=return_stmt();
    			PopFollow();

    			DebugLocation(73, 3);
    			 return return_stmt12; 

    			}
    			break;

    		}
    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(74, 1);
    	} finally { DebugExitRule(GrammarFileName, "statement"); }
    	return Value;

    }
    // $ANTLR end "statement"


    protected virtual void Enter_empty_stmt() {}
    protected virtual void Leave_empty_stmt() {}

    // $ANTLR start "empty_stmt"
    // C:\\output\\HoldingRule.g:76:1: empty_stmt returns [Statement Value] : ';' ;
    [GrammarRule("empty_stmt")]
    private Statement empty_stmt()
    {

        Statement Value = default(Statement);

    	try { DebugEnterRule(GrammarFileName, "empty_stmt");
    	DebugLocation(76, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:78:2: ( ';' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:78:4: ';'
    		{
    		DebugLocation(78, 4);
    		Match(input,23,Follow._23_in_empty_stmt198); 

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(79, 1);
    	} finally { DebugExitRule(GrammarFileName, "empty_stmt"); }
    	return Value;

    }
    // $ANTLR end "empty_stmt"


    protected virtual void Enter_return_stmt() {}
    protected virtual void Leave_return_stmt() {}

    // $ANTLR start "return_stmt"
    // C:\\output\\HoldingRule.g:81:1: return_stmt returns [Statement Value] : ( 'RETURN' | 'Return' | 'return' ) ';' ;
    [GrammarRule("return_stmt")]
    private Statement return_stmt()
    {

        Statement Value = default(Statement);

    	try { DebugEnterRule(GrammarFileName, "return_stmt");
    	DebugLocation(81, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:83:2: ( ( 'RETURN' | 'Return' | 'return' ) ';' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:83:4: ( 'RETURN' | 'Return' | 'return' ) ';'
    		{
    		DebugLocation(83, 4);
    		if ((input.LA(1)>=24 && input.LA(1)<=26))
    		{
    			input.Consume();
    			state.errorRecovery=false;
    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			throw mse;
    		}

    		DebugLocation(83, 33);
    		Match(input,23,Follow._23_in_return_stmt222); 
    		DebugLocation(84, 3);
    		 Value = Statement.Return(); 

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(85, 1);
    	} finally { DebugExitRule(GrammarFileName, "return_stmt"); }
    	return Value;

    }
    // $ANTLR end "return_stmt"


    protected virtual void Enter_block_stmt() {}
    protected virtual void Leave_block_stmt() {}

    // $ANTLR start "block_stmt"
    // C:\\output\\HoldingRule.g:87:1: block_stmt returns [Statement Value] : '{' statements '}' ;
    [GrammarRule("block_stmt")]
    private Statement block_stmt()
    {

        Statement Value = default(Statement);

        StatementsList statements13 = default(StatementsList);

    	try { DebugEnterRule(GrammarFileName, "block_stmt");
    	DebugLocation(87, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:89:2: ( '{' statements '}' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:89:4: '{' statements '}'
    		{
    		DebugLocation(89, 4);
    		Match(input,27,Follow._27_in_block_stmt242); 
    		DebugLocation(89, 8);
    		PushFollow(Follow._statements_in_block_stmt244);
    		statements13=statements();
    		PopFollow();

    		DebugLocation(89, 19);
    		Match(input,28,Follow._28_in_block_stmt246); 
    		DebugLocation(90, 3);
    		 Value = Statement.Block(statements13); 

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(91, 1);
    	} finally { DebugExitRule(GrammarFileName, "block_stmt"); }
    	return Value;

    }
    // $ANTLR end "block_stmt"


    protected virtual void Enter_goto_stmt() {}
    protected virtual void Leave_goto_stmt() {}

    // $ANTLR start "goto_stmt"
    // C:\\output\\HoldingRule.g:93:1: goto_stmt returns [Statement Value] : ( 'GOTO' | 'GoTo' | 'Goto' | 'goto' ) lbl= IDENTIFIER ';' ;
    [GrammarRule("goto_stmt")]
    private Statement goto_stmt()
    {

        Statement Value = default(Statement);

        IToken lbl=null;

    	try { DebugEnterRule(GrammarFileName, "goto_stmt");
    	DebugLocation(93, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:95:2: ( ( 'GOTO' | 'GoTo' | 'Goto' | 'goto' ) lbl= IDENTIFIER ';' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:95:4: ( 'GOTO' | 'GoTo' | 'Goto' | 'goto' ) lbl= IDENTIFIER ';'
    		{
    		DebugLocation(95, 4);
    		if ((input.LA(1)>=29 && input.LA(1)<=32))
    		{
    			input.Consume();
    			state.errorRecovery=false;
    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			throw mse;
    		}

    		DebugLocation(95, 37);
    		lbl=(IToken)Match(input,IDENTIFIER,Follow._IDENTIFIER_in_goto_stmt278); 
    		DebugLocation(95, 49);
    		Match(input,23,Follow._23_in_goto_stmt280); 
    		DebugLocation(96, 3);
    		 Value = Statement.GoTo((lbl!=null?lbl.Text:null)); 

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(97, 1);
    	} finally { DebugExitRule(GrammarFileName, "goto_stmt"); }
    	return Value;

    }
    // $ANTLR end "goto_stmt"


    protected virtual void Enter_declaration_stmt() {}
    protected virtual void Leave_declaration_stmt() {}

    // $ANTLR start "declaration_stmt"
    // C:\\output\\HoldingRule.g:99:1: declaration_stmt returns [Statement Value] : ( 'VAR' | 'Var' | 'var' ) d1= declaration ( ',' d2= declaration )* ';' ;
    [GrammarRule("declaration_stmt")]
    private Statement declaration_stmt()
    {

        Statement Value = default(Statement);

        Statement d1 = default(Statement);
        Statement d2 = default(Statement);

         StatementsList slist = new StatementsList(); 
    	try { DebugEnterRule(GrammarFileName, "declaration_stmt");
    	DebugLocation(99, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:102:2: ( ( 'VAR' | 'Var' | 'var' ) d1= declaration ( ',' d2= declaration )* ';' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:102:4: ( 'VAR' | 'Var' | 'var' ) d1= declaration ( ',' d2= declaration )* ';'
    		{
    		DebugLocation(102, 4);
    		if ((input.LA(1)>=33 && input.LA(1)<=35))
    		{
    			input.Consume();
    			state.errorRecovery=false;
    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			throw mse;
    		}

    		DebugLocation(102, 26);
    		PushFollow(Follow._declaration_in_declaration_stmt316);
    		d1=declaration();
    		PopFollow();

    		DebugLocation(102, 39);
    		 slist.Add(d1); 
    		DebugLocation(103, 3);
    		// C:\\output\\HoldingRule.g:103:3: ( ',' d2= declaration )*
    		try { DebugEnterSubRule(3);
    		while (true)
    		{
    			int alt3=2;
    			try { DebugEnterDecision(3, decisionCanBacktrack[3]);
    			int LA3_0 = input.LA(1);

    			if ((LA3_0==36))
    			{
    				alt3=1;
    			}


    			} finally { DebugExitDecision(3); }
    			switch ( alt3 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:103:5: ',' d2= declaration
    				{
    				DebugLocation(103, 5);
    				Match(input,36,Follow._36_in_declaration_stmt324); 
    				DebugLocation(103, 11);
    				PushFollow(Follow._declaration_in_declaration_stmt328);
    				d2=declaration();
    				PopFollow();

    				DebugLocation(103, 24);
    				 slist.Add(d2); 

    				}
    				break;

    			default:
    				goto loop3;
    			}
    		}

    		loop3:
    			;

    		} finally { DebugExitSubRule(3); }

    		DebugLocation(103, 53);
    		Match(input,23,Follow._23_in_declaration_stmt335); 
    		DebugLocation(104, 3);
    		 Value = slist; 

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(105, 1);
    	} finally { DebugExitRule(GrammarFileName, "declaration_stmt"); }
    	return Value;

    }
    // $ANTLR end "declaration_stmt"


    protected virtual void Enter_declaration() {}
    protected virtual void Leave_declaration() {}

    // $ANTLR start "declaration"
    // C:\\output\\HoldingRule.g:107:1: fragment declaration returns [Statement Value] : IDENTIFIER (tp= type )? ( '=' ex= expression )? ;
    [GrammarRule("declaration")]
    private Statement declaration()
    {

        Statement Value = default(Statement);

        IToken IDENTIFIER14=null;
        ExpressionType tp = default(ExpressionType);
        Expression ex = default(Expression);

    	try { DebugEnterRule(GrammarFileName, "declaration");
    	DebugLocation(107, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:110:2: ( IDENTIFIER (tp= type )? ( '=' ex= expression )? )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:110:4: IDENTIFIER (tp= type )? ( '=' ex= expression )?
    		{
    		DebugLocation(110, 4);
    		IDENTIFIER14=(IToken)Match(input,IDENTIFIER,Follow._IDENTIFIER_in_declaration357); 
    		DebugLocation(110, 17);
    		// C:\\output\\HoldingRule.g:110:17: (tp= type )?
    		int alt4=2;
    		try { DebugEnterSubRule(4);
    		try { DebugEnterDecision(4, decisionCanBacktrack[4]);
    		int LA4_0 = input.LA(1);

    		if (((LA4_0>=37 && LA4_0<=45)||(LA4_0>=48 && LA4_0<=62)))
    		{
    			alt4=1;
    		}
    		} finally { DebugExitDecision(4); }
    		switch (alt4)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:110:17: tp= type
    			{
    			DebugLocation(110, 17);
    			PushFollow(Follow._type_in_declaration361);
    			tp=type();
    			PopFollow();


    			}
    			break;

    		}
    		} finally { DebugExitSubRule(4); }

    		DebugLocation(110, 24);
    		// C:\\output\\HoldingRule.g:110:24: ( '=' ex= expression )?
    		int alt5=2;
    		try { DebugEnterSubRule(5);
    		try { DebugEnterDecision(5, decisionCanBacktrack[5]);
    		int LA5_0 = input.LA(1);

    		if ((LA5_0==EQUAL))
    		{
    			alt5=1;
    		}
    		} finally { DebugExitDecision(5); }
    		switch (alt5)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:110:25: '=' ex= expression
    			{
    			DebugLocation(110, 25);
    			Match(input,EQUAL,Follow._EQUAL_in_declaration365); 
    			DebugLocation(110, 31);
    			PushFollow(Follow._expression_in_declaration369);
    			ex=expression();
    			PopFollow();


    			}
    			break;

    		}
    		} finally { DebugExitSubRule(5); }

    		DebugLocation(111, 3);

    					Identifier ident = Context.Declare(tp == null ? ExpressionType.Decimal: tp, (IDENTIFIER14!=null?IDENTIFIER14.Text:null));
    					if (ex != null)
    						Value = Statement.Assign(ident, ex);
    				

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(116, 1);
    	} finally { DebugExitRule(GrammarFileName, "declaration"); }
    	return Value;

    }
    // $ANTLR end "declaration"


    protected virtual void Enter_type() {}
    protected virtual void Leave_type() {}

    // $ANTLR start "type"
    // C:\\output\\HoldingRule.g:118:1: type returns [ExpressionType Value] : ( ( 'INT' | 'Int' | 'int' ) | ( 'LONG' | 'Long' | 'long' ) | ( 'STRING' | 'String' | 'string' ) ( '(' length ')' )? | ( 'DECIMAL' | 'Decimal' | 'decimal' ) ( '(' length ( ',' precision )? ')' )? | ( 'DECIMAL2' | 'Decimal2' | 'decimal2' ) | ( 'MONEY' | 'Money' | 'money' ) | ( 'FLOAT' | 'Float' | 'float' ) | ( 'DATETIME' | 'Datetime' | 'datetime' ) );
    [GrammarRule("type")]
    private ExpressionType type()
    {

        ExpressionType Value = default(ExpressionType);

        HoldingRuleParser.length_return length15 = default(HoldingRuleParser.length_return);
        HoldingRuleParser.length_return length16 = default(HoldingRuleParser.length_return);
        HoldingRuleParser.precision_return precision17 = default(HoldingRuleParser.precision_return);

    	try { DebugEnterRule(GrammarFileName, "type");
    	DebugLocation(118, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:120:2: ( ( 'INT' | 'Int' | 'int' ) | ( 'LONG' | 'Long' | 'long' ) | ( 'STRING' | 'String' | 'string' ) ( '(' length ')' )? | ( 'DECIMAL' | 'Decimal' | 'decimal' ) ( '(' length ( ',' precision )? ')' )? | ( 'DECIMAL2' | 'Decimal2' | 'decimal2' ) | ( 'MONEY' | 'Money' | 'money' ) | ( 'FLOAT' | 'Float' | 'float' ) | ( 'DATETIME' | 'Datetime' | 'datetime' ) )
    		int alt9=8;
    		try { DebugEnterDecision(9, decisionCanBacktrack[9]);
    		switch (input.LA(1))
    		{
    		case 37:
    		case 38:
    		case 39:
    			{
    			alt9=1;
    			}
    			break;
    		case 40:
    		case 41:
    		case 42:
    			{
    			alt9=2;
    			}
    			break;
    		case 43:
    		case 44:
    		case 45:
    			{
    			alt9=3;
    			}
    			break;
    		case 48:
    		case 49:
    		case 50:
    			{
    			alt9=4;
    			}
    			break;
    		case 51:
    		case 52:
    		case 53:
    			{
    			alt9=5;
    			}
    			break;
    		case 54:
    		case 55:
    		case 56:
    			{
    			alt9=6;
    			}
    			break;
    		case 57:
    		case 58:
    		case 59:
    			{
    			alt9=7;
    			}
    			break;
    		case 60:
    		case 61:
    		case 62:
    			{
    			alt9=8;
    			}
    			break;
    		default:
    			{
    				NoViableAltException nvae = new NoViableAltException("", 9, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}

    		} finally { DebugExitDecision(9); }
    		switch (alt9)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:120:4: ( 'INT' | 'Int' | 'int' )
    			{
    			DebugLocation(120, 4);
    			if ((input.LA(1)>=37 && input.LA(1)<=39))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(121, 3);
    			 Value = ExpressionType.Integer; 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:122:4: ( 'LONG' | 'Long' | 'long' )
    			{
    			DebugLocation(122, 4);
    			if ((input.LA(1)>=40 && input.LA(1)<=42))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(123, 3);
    			 Value = ExpressionType.Long; 

    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:124:4: ( 'STRING' | 'String' | 'string' ) ( '(' length ')' )?
    			{
    			DebugLocation(124, 4);
    			if ((input.LA(1)>=43 && input.LA(1)<=45))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(124, 33);
    			// C:\\output\\HoldingRule.g:124:33: ( '(' length ')' )?
    			int alt6=2;
    			try { DebugEnterSubRule(6);
    			try { DebugEnterDecision(6, decisionCanBacktrack[6]);
    			int LA6_0 = input.LA(1);

    			if ((LA6_0==46))
    			{
    				alt6=1;
    			}
    			} finally { DebugExitDecision(6); }
    			switch (alt6)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:124:35: '(' length ')'
    				{
    				DebugLocation(124, 35);
    				Match(input,46,Follow._46_in_type432); 
    				DebugLocation(124, 39);
    				PushFollow(Follow._length_in_type434);
    				length15=length();
    				PopFollow();

    				DebugLocation(124, 46);
    				Match(input,47,Follow._47_in_type436); 

    				}
    				break;

    			}
    			} finally { DebugExitSubRule(6); }

    			DebugLocation(125, 3);
    			 Value = new ExpressionType(ExpressionTypeCode.String, (length15!=null?input.ToString(length15.Start,length15.Stop):null) == null || (length15!=null?input.ToString(length15.Start,length15.Stop):null).ToLowerInvariant() == "max" ? "varchar(max)": "varchar(" + (length15!=null?input.ToString(length15.Start,length15.Stop):null) + ")"); 

    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:126:4: ( 'DECIMAL' | 'Decimal' | 'decimal' ) ( '(' length ( ',' precision )? ')' )?
    			{
    			DebugLocation(126, 4);
    			if ((input.LA(1)>=48 && input.LA(1)<=50))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(126, 36);
    			// C:\\output\\HoldingRule.g:126:36: ( '(' length ( ',' precision )? ')' )?
    			int alt8=2;
    			try { DebugEnterSubRule(8);
    			try { DebugEnterDecision(8, decisionCanBacktrack[8]);
    			int LA8_0 = input.LA(1);

    			if ((LA8_0==46))
    			{
    				alt8=1;
    			}
    			} finally { DebugExitDecision(8); }
    			switch (alt8)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:126:38: '(' length ( ',' precision )? ')'
    				{
    				DebugLocation(126, 38);
    				Match(input,46,Follow._46_in_type458); 
    				DebugLocation(126, 42);
    				PushFollow(Follow._length_in_type460);
    				length16=length();
    				PopFollow();

    				DebugLocation(126, 49);
    				// C:\\output\\HoldingRule.g:126:49: ( ',' precision )?
    				int alt7=2;
    				try { DebugEnterSubRule(7);
    				try { DebugEnterDecision(7, decisionCanBacktrack[7]);
    				int LA7_0 = input.LA(1);

    				if ((LA7_0==36))
    				{
    					alt7=1;
    				}
    				} finally { DebugExitDecision(7); }
    				switch (alt7)
    				{
    				case 1:
    					DebugEnterAlt(1);
    					// C:\\output\\HoldingRule.g:126:50: ',' precision
    					{
    					DebugLocation(126, 50);
    					Match(input,36,Follow._36_in_type463); 
    					DebugLocation(126, 54);
    					PushFollow(Follow._precision_in_type465);
    					precision17=precision();
    					PopFollow();


    					}
    					break;

    				}
    				} finally { DebugExitSubRule(7); }

    				DebugLocation(126, 66);
    				Match(input,47,Follow._47_in_type469); 

    				}
    				break;

    			}
    			} finally { DebugExitSubRule(8); }

    			DebugLocation(127, 3);
    			 Value = new ExpressionType(ExpressionTypeCode.Numeric,
    						(length16!=null?input.ToString(length16.Start,length16.Stop):null) == null || (length16!=null?input.ToString(length16.Start,length16.Stop):null).ToLowerInvariant() == "max" ? "decimal(19,8)":
    						(precision17!=null?input.ToString(precision17.Start,precision17.Stop):null) == null ? "decimal(" + (length16!=null?input.ToString(length16.Start,length16.Stop):null) + ")":
    						"decimal(" + (length16!=null?input.ToString(length16.Start,length16.Stop):null) + "," + (precision17!=null?input.ToString(precision17.Start,precision17.Stop):null) + ")" ); 

    			}
    			break;
    		case 5:
    			DebugEnterAlt(5);
    			// C:\\output\\HoldingRule.g:131:4: ( 'DECIMAL2' | 'Decimal2' | 'decimal2' )
    			{
    			DebugLocation(131, 4);
    			if ((input.LA(1)>=51 && input.LA(1)<=53))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(132, 3);
    			 Value = ExpressionType.Decimal2; 

    			}
    			break;
    		case 6:
    			DebugEnterAlt(6);
    			// C:\\output\\HoldingRule.g:133:4: ( 'MONEY' | 'Money' | 'money' )
    			{
    			DebugLocation(133, 4);
    			if ((input.LA(1)>=54 && input.LA(1)<=56))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(134, 3);
    			 Value = ExpressionType.Money; 

    			}
    			break;
    		case 7:
    			DebugEnterAlt(7);
    			// C:\\output\\HoldingRule.g:135:4: ( 'FLOAT' | 'Float' | 'float' )
    			{
    			DebugLocation(135, 4);
    			if ((input.LA(1)>=57 && input.LA(1)<=59))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(136, 3);
    			 Value = ExpressionType.Float; 

    			}
    			break;
    		case 8:
    			DebugEnterAlt(8);
    			// C:\\output\\HoldingRule.g:137:4: ( 'DATETIME' | 'Datetime' | 'datetime' )
    			{
    			DebugLocation(137, 4);
    			if ((input.LA(1)>=60 && input.LA(1)<=62))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(138, 3);
    			 Value = ExpressionType.DateTime; 

    			}
    			break;

    		}
    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(139, 1);
    	} finally { DebugExitRule(GrammarFileName, "type"); }
    	return Value;

    }
    // $ANTLR end "type"

    public class length_return : ParserRuleReturnScope<IToken>
    {
    }

    protected virtual void Enter_length() {}
    protected virtual void Leave_length() {}

    // $ANTLR start "length"
    // C:\\output\\HoldingRule.g:141:1: length : ( INT | ( 'MAX' | 'Max' | 'max' ) );
    [GrammarRule("length")]
    private HoldingRuleParser.length_return length()
    {

        HoldingRuleParser.length_return retval = new HoldingRuleParser.length_return();
        retval.Start = (IToken)input.LT(1);

    	try { DebugEnterRule(GrammarFileName, "length");
    	DebugLocation(141, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:142:2: ( INT | ( 'MAX' | 'Max' | 'max' ) )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:
    		{
    		DebugLocation(142, 2);
    		if (input.LA(1)==INT||(input.LA(1)>=63 && input.LA(1)<=65))
    		{
    			input.Consume();
    			state.errorRecovery=false;
    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			throw mse;
    		}


    		}

    		retval.Stop = (IToken)input.LT(-1);

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(144, 1);
    	} finally { DebugExitRule(GrammarFileName, "length"); }
    	return retval;

    }
    // $ANTLR end "length"

    public class precision_return : ParserRuleReturnScope<IToken>
    {
    }

    protected virtual void Enter_precision() {}
    protected virtual void Leave_precision() {}

    // $ANTLR start "precision"
    // C:\\output\\HoldingRule.g:146:1: precision : INT ;
    [GrammarRule("precision")]
    private HoldingRuleParser.precision_return precision()
    {

        HoldingRuleParser.precision_return retval = new HoldingRuleParser.precision_return();
        retval.Start = (IToken)input.LT(1);

    	try { DebugEnterRule(GrammarFileName, "precision");
    	DebugLocation(146, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:147:2: ( INT )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:147:4: INT
    		{
    		DebugLocation(147, 4);
    		Match(input,INT,Follow._INT_in_precision569); 

    		}

    		retval.Stop = (IToken)input.LT(-1);

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(148, 1);
    	} finally { DebugExitRule(GrammarFileName, "precision"); }
    	return retval;

    }
    // $ANTLR end "precision"


    protected virtual void Enter_label_stmt() {}
    protected virtual void Leave_label_stmt() {}

    // $ANTLR start "label_stmt"
    // C:\\output\\HoldingRule.g:150:1: label_stmt returns [Statement Value] : IDENTIFIER ':' ;
    [GrammarRule("label_stmt")]
    private Statement label_stmt()
    {

        Statement Value = default(Statement);

        IToken IDENTIFIER18=null;

    	try { DebugEnterRule(GrammarFileName, "label_stmt");
    	DebugLocation(150, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:152:2: ( IDENTIFIER ':' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:152:4: IDENTIFIER ':'
    		{
    		DebugLocation(152, 4);
    		IDENTIFIER18=(IToken)Match(input,IDENTIFIER,Follow._IDENTIFIER_in_label_stmt585); 
    		DebugLocation(152, 15);
    		Match(input,66,Follow._66_in_label_stmt587); 
    		DebugLocation(153, 3);
    		 Value = Statement.Label((IDENTIFIER18!=null?IDENTIFIER18.Text:null)); 

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(154, 1);
    	} finally { DebugExitRule(GrammarFileName, "label_stmt"); }
    	return Value;

    }
    // $ANTLR end "label_stmt"


    protected virtual void Enter_if_stmt() {}
    protected virtual void Leave_if_stmt() {}

    // $ANTLR start "if_stmt"
    // C:\\output\\HoldingRule.g:156:1: if_stmt returns [Statement Value] : ( 'IF' | 'If' | 'if' ) '(' test= expression ')' thenStmt= statement ( options {k=1; } : ( 'ELSE' | 'Else' | 'else' ) elseStmt= statement )? ;
    [GrammarRule("if_stmt")]
    private Statement if_stmt()
    {

        Statement Value = default(Statement);

        Expression test = default(Expression);
        Statement thenStmt = default(Statement);
        Statement elseStmt = default(Statement);

    	try { DebugEnterRule(GrammarFileName, "if_stmt");
    	DebugLocation(156, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:158:2: ( ( 'IF' | 'If' | 'if' ) '(' test= expression ')' thenStmt= statement ( options {k=1; } : ( 'ELSE' | 'Else' | 'else' ) elseStmt= statement )? )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:158:4: ( 'IF' | 'If' | 'if' ) '(' test= expression ')' thenStmt= statement ( options {k=1; } : ( 'ELSE' | 'Else' | 'else' ) elseStmt= statement )?
    		{
    		DebugLocation(158, 4);
    		if ((input.LA(1)>=67 && input.LA(1)<=69))
    		{
    			input.Consume();
    			state.errorRecovery=false;
    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			throw mse;
    		}

    		DebugLocation(158, 21);
    		Match(input,46,Follow._46_in_if_stmt615); 
    		DebugLocation(158, 29);
    		PushFollow(Follow._expression_in_if_stmt619);
    		test=expression();
    		PopFollow();

    		DebugLocation(158, 41);
    		Match(input,47,Follow._47_in_if_stmt621); 
    		DebugLocation(158, 53);
    		PushFollow(Follow._statement_in_if_stmt625);
    		thenStmt=statement();
    		PopFollow();

    		DebugLocation(159, 3);
    		// C:\\output\\HoldingRule.g:159:3: ( options {k=1; } : ( 'ELSE' | 'Else' | 'else' ) elseStmt= statement )?
    		int alt10=2;
    		try { DebugEnterSubRule(10);
    		try { DebugEnterDecision(10, decisionCanBacktrack[10]);
    		try
    		{
    			alt10 = dfa10.Predict(input);
    		}
    		catch (NoViableAltException nvae)
    		{
    			DebugRecognitionException(nvae);
    			throw;
    		}
    		} finally { DebugExitDecision(10); }
    		switch (alt10)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:159:20: ( 'ELSE' | 'Else' | 'else' ) elseStmt= statement
    			{
    			DebugLocation(159, 20);
    			if ((input.LA(1)>=70 && input.LA(1)<=72))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(159, 51);
    			PushFollow(Follow._statement_in_if_stmt648);
    			elseStmt=statement();
    			PopFollow();


    			}
    			break;

    		}
    		} finally { DebugExitSubRule(10); }

    		DebugLocation(160, 3);
    		 Value = Statement.If(test, thenStmt, elseStmt); 

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(161, 1);
    	} finally { DebugExitRule(GrammarFileName, "if_stmt"); }
    	return Value;

    }
    // $ANTLR end "if_stmt"


    protected virtual void Enter_call_stmt() {}
    protected virtual void Leave_call_stmt() {}

    // $ANTLR start "call_stmt"
    // C:\\output\\HoldingRule.g:164:1: call_stmt returns [Statement Value] : ( ( 'PRINT' | 'Print' | 'print' ) expression ';' | ( 'ERROR' | 'Error' | 'error' ) ( '(' ')' | expression ) ';' | ( 'CREATELOT' | 'CreateLot' | 'createLot' | 'createlot' ) expression ';' | ( 'CREATELOT' | 'CreateLot' | 'createLot' | 'createlot' ) zero_expression ';' | ( 'CLOSELOT' | 'CloseLot' | 'closeLot' | 'closelot' ) zero_expression ';' | ( 'SPLITCOSTBASIS' | 'SplitCostBasis' | 'SplitCostbasis' | 'splitCostBasis' | 'splitCostbasis' | 'splitcostbasis' ) zero_expression ';' | ( 'FINDLAST' | 'FindLast' | 'Findlast' | 'findLast' | 'findlast' ) expression ';' | ( 'UPDATECASH' | 'UpdateCash' | 'updateCash' | 'updatecash' ) (cash= expression )? ';' | ( 'INLINEQUERY' | 'InlineQuery' | 'inlineQuery' | 'inlinequery' ) ( '(' query= STRING ')' | query= STRING ) ';' | ( 'RULE' | 'Rule' | 'rule' ) ( '(' rule= INT ',' count= expression ',' currency= expression ',' asset= expression ')' | rule= INT ',' count= expression ',' currency= expression ',' asset= expression ) ';' | ( 'MOVEASSETS' | 'MoveAssets' | 'moveAssets' | 'moveassets' ) source= expression ';' );
    [GrammarRule("call_stmt")]
    private Statement call_stmt()
    {

        Statement Value = default(Statement);

        IToken query=null;
        IToken rule=null;
        Expression cash = default(Expression);
        Expression count = default(Expression);
        Expression currency = default(Expression);
        Expression asset = default(Expression);
        Expression source = default(Expression);
        Expression expression19 = default(Expression);
        Expression expression20 = default(Expression);
        Expression expression21 = default(Expression);
        Expression expression22 = default(Expression);

    	try { DebugEnterRule(GrammarFileName, "call_stmt");
    	DebugLocation(164, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:166:2: ( ( 'PRINT' | 'Print' | 'print' ) expression ';' | ( 'ERROR' | 'Error' | 'error' ) ( '(' ')' | expression ) ';' | ( 'CREATELOT' | 'CreateLot' | 'createLot' | 'createlot' ) expression ';' | ( 'CREATELOT' | 'CreateLot' | 'createLot' | 'createlot' ) zero_expression ';' | ( 'CLOSELOT' | 'CloseLot' | 'closeLot' | 'closelot' ) zero_expression ';' | ( 'SPLITCOSTBASIS' | 'SplitCostBasis' | 'SplitCostbasis' | 'splitCostBasis' | 'splitCostbasis' | 'splitcostbasis' ) zero_expression ';' | ( 'FINDLAST' | 'FindLast' | 'Findlast' | 'findLast' | 'findlast' ) expression ';' | ( 'UPDATECASH' | 'UpdateCash' | 'updateCash' | 'updatecash' ) (cash= expression )? ';' | ( 'INLINEQUERY' | 'InlineQuery' | 'inlineQuery' | 'inlinequery' ) ( '(' query= STRING ')' | query= STRING ) ';' | ( 'RULE' | 'Rule' | 'rule' ) ( '(' rule= INT ',' count= expression ',' currency= expression ',' asset= expression ')' | rule= INT ',' count= expression ',' currency= expression ',' asset= expression ) ';' | ( 'MOVEASSETS' | 'MoveAssets' | 'moveAssets' | 'moveassets' ) source= expression ';' )
    		int alt15=11;
    		try { DebugEnterDecision(15, decisionCanBacktrack[15]);
    		try
    		{
    			alt15 = dfa15.Predict(input);
    		}
    		catch (NoViableAltException nvae)
    		{
    			DebugRecognitionException(nvae);
    			throw;
    		}
    		} finally { DebugExitDecision(15); }
    		switch (alt15)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:166:4: ( 'PRINT' | 'Print' | 'print' ) expression ';'
    			{
    			DebugLocation(166, 4);
    			if ((input.LA(1)>=73 && input.LA(1)<=75))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(166, 30);
    			PushFollow(Follow._expression_in_call_stmt679);
    			expression19=expression();
    			PopFollow();

    			DebugLocation(166, 41);
    			Match(input,23,Follow._23_in_call_stmt681); 
    			DebugLocation(167, 3);
    			 Value = new Procedures.PrintProcedure(expression19); 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:168:4: ( 'ERROR' | 'Error' | 'error' ) ( '(' ')' | expression ) ';'
    			{
    			DebugLocation(168, 4);
    			if ((input.LA(1)>=76 && input.LA(1)<=78))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(168, 30);
    			// C:\\output\\HoldingRule.g:168:30: ( '(' ')' | expression )
    			int alt11=2;
    			try { DebugEnterSubRule(11);
    			try { DebugEnterDecision(11, decisionCanBacktrack[11]);
    			int LA11_0 = input.LA(1);

    			if ((LA11_0==46))
    			{
    				int LA11_1 = input.LA(2);

    				if ((LA11_1==47))
    				{
    					alt11=1;
    				}
    				else if (((LA11_1>=IDENTIFIER && LA11_1<=STRING)||LA11_1==NOT||(LA11_1>=DEC && LA11_1<=TRANSACT_FIELD)||LA11_1==46||(LA11_1>=60 && LA11_1<=65)||(LA11_1>=67 && LA11_1<=69)||LA11_1==118||(LA11_1>=121 && LA11_1<=160)))
    				{
    					alt11=2;
    				}
    				else
    				{
    					NoViableAltException nvae = new NoViableAltException("", 11, 1, input);

    					DebugRecognitionException(nvae);
    					throw nvae;
    				}
    			}
    			else if (((LA11_0>=IDENTIFIER && LA11_0<=STRING)||LA11_0==NOT||(LA11_0>=DEC && LA11_0<=TRANSACT_FIELD)||(LA11_0>=60 && LA11_0<=65)||(LA11_0>=67 && LA11_0<=69)||LA11_0==118||(LA11_0>=121 && LA11_0<=160)))
    			{
    				alt11=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 11, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    			} finally { DebugExitDecision(11); }
    			switch (alt11)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:168:31: '(' ')'
    				{
    				DebugLocation(168, 31);
    				Match(input,46,Follow._46_in_call_stmt699); 
    				DebugLocation(168, 35);
    				Match(input,47,Follow._47_in_call_stmt701); 

    				}
    				break;
    			case 2:
    				DebugEnterAlt(2);
    				// C:\\output\\HoldingRule.g:168:41: expression
    				{
    				DebugLocation(168, 41);
    				PushFollow(Follow._expression_in_call_stmt705);
    				expression20=expression();
    				PopFollow();


    				}
    				break;

    			}
    			} finally { DebugExitSubRule(11); }

    			DebugLocation(168, 53);
    			Match(input,23,Follow._23_in_call_stmt708); 
    			DebugLocation(169, 3);
    			 Value = new Procedures.ErrorProcedure(expression20); 

    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:170:4: ( 'CREATELOT' | 'CreateLot' | 'createLot' | 'createlot' ) expression ';'
    			{
    			DebugLocation(170, 4);
    			if ((input.LA(1)>=79 && input.LA(1)<=82))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(170, 54);
    			PushFollow(Follow._expression_in_call_stmt727);
    			expression21=expression();
    			PopFollow();

    			DebugLocation(170, 65);
    			Match(input,23,Follow._23_in_call_stmt729); 
    			DebugLocation(171, 3);
    			 Value = new Procedures.CreateLotProcedure(expression21); 

    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:172:4: ( 'CREATELOT' | 'CreateLot' | 'createLot' | 'createlot' ) zero_expression ';'
    			{
    			DebugLocation(172, 4);
    			if ((input.LA(1)>=79 && input.LA(1)<=82))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(172, 54);
    			PushFollow(Follow._zero_expression_in_call_stmt748);
    			zero_expression();
    			PopFollow();

    			DebugLocation(172, 70);
    			Match(input,23,Follow._23_in_call_stmt750); 
    			DebugLocation(173, 3);
    			 Value = new Procedures.CreateLotProcedure(); 

    			}
    			break;
    		case 5:
    			DebugEnterAlt(5);
    			// C:\\output\\HoldingRule.g:174:4: ( 'CLOSELOT' | 'CloseLot' | 'closeLot' | 'closelot' ) zero_expression ';'
    			{
    			DebugLocation(174, 4);
    			if ((input.LA(1)>=83 && input.LA(1)<=86))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(174, 50);
    			PushFollow(Follow._zero_expression_in_call_stmt769);
    			zero_expression();
    			PopFollow();

    			DebugLocation(174, 66);
    			Match(input,23,Follow._23_in_call_stmt771); 
    			DebugLocation(175, 3);
    			 Value = new Procedures.CloseLotProcedure(); 

    			}
    			break;
    		case 6:
    			DebugEnterAlt(6);
    			// C:\\output\\HoldingRule.g:176:4: ( 'SPLITCOSTBASIS' | 'SplitCostBasis' | 'SplitCostbasis' | 'splitCostBasis' | 'splitCostbasis' | 'splitcostbasis' ) zero_expression ';'
    			{
    			DebugLocation(176, 4);
    			if ((input.LA(1)>=87 && input.LA(1)<=92))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(176, 108);
    			PushFollow(Follow._zero_expression_in_call_stmt794);
    			zero_expression();
    			PopFollow();

    			DebugLocation(176, 124);
    			Match(input,23,Follow._23_in_call_stmt796); 
    			DebugLocation(177, 3);
    			 Value = new Procedures.SplitCostBasisProcedure(); 

    			}
    			break;
    		case 7:
    			DebugEnterAlt(7);
    			// C:\\output\\HoldingRule.g:178:4: ( 'FINDLAST' | 'FindLast' | 'Findlast' | 'findLast' | 'findlast' ) expression ';'
    			{
    			DebugLocation(178, 4);
    			if ((input.LA(1)>=93 && input.LA(1)<=97))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(178, 61);
    			PushFollow(Follow._expression_in_call_stmt817);
    			expression22=expression();
    			PopFollow();

    			DebugLocation(178, 72);
    			Match(input,23,Follow._23_in_call_stmt819); 
    			DebugLocation(179, 3);
    			 Value = new Procedures.FindLastProcedure(expression22); 

    			}
    			break;
    		case 8:
    			DebugEnterAlt(8);
    			// C:\\output\\HoldingRule.g:180:4: ( 'UPDATECASH' | 'UpdateCash' | 'updateCash' | 'updatecash' ) (cash= expression )? ';'
    			{
    			DebugLocation(180, 4);
    			if ((input.LA(1)>=98 && input.LA(1)<=101))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(180, 62);
    			// C:\\output\\HoldingRule.g:180:62: (cash= expression )?
    			int alt12=2;
    			try { DebugEnterSubRule(12);
    			try { DebugEnterDecision(12, decisionCanBacktrack[12]);
    			int LA12_0 = input.LA(1);

    			if (((LA12_0>=IDENTIFIER && LA12_0<=STRING)||LA12_0==NOT||(LA12_0>=DEC && LA12_0<=TRANSACT_FIELD)||LA12_0==46||(LA12_0>=60 && LA12_0<=65)||(LA12_0>=67 && LA12_0<=69)||LA12_0==118||(LA12_0>=121 && LA12_0<=160)))
    			{
    				alt12=1;
    			}
    			} finally { DebugExitDecision(12); }
    			switch (alt12)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:180:62: cash= expression
    				{
    				DebugLocation(180, 62);
    				PushFollow(Follow._expression_in_call_stmt840);
    				cash=expression();
    				PopFollow();


    				}
    				break;

    			}
    			} finally { DebugExitSubRule(12); }

    			DebugLocation(180, 75);
    			Match(input,23,Follow._23_in_call_stmt843); 
    			DebugLocation(181, 3);
    			 Value = new Procedures.UpdateCashProcedure(cash); 

    			}
    			break;
    		case 9:
    			DebugEnterAlt(9);
    			// C:\\output\\HoldingRule.g:182:4: ( 'INLINEQUERY' | 'InlineQuery' | 'inlineQuery' | 'inlinequery' ) ( '(' query= STRING ')' | query= STRING ) ';'
    			{
    			DebugLocation(182, 4);
    			if ((input.LA(1)>=102 && input.LA(1)<=105))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(182, 62);
    			// C:\\output\\HoldingRule.g:182:62: ( '(' query= STRING ')' | query= STRING )
    			int alt13=2;
    			try { DebugEnterSubRule(13);
    			try { DebugEnterDecision(13, decisionCanBacktrack[13]);
    			int LA13_0 = input.LA(1);

    			if ((LA13_0==46))
    			{
    				alt13=1;
    			}
    			else if ((LA13_0==STRING))
    			{
    				alt13=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 13, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    			} finally { DebugExitDecision(13); }
    			switch (alt13)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:182:63: '(' query= STRING ')'
    				{
    				DebugLocation(182, 63);
    				Match(input,46,Follow._46_in_call_stmt863); 
    				DebugLocation(182, 72);
    				query=(IToken)Match(input,STRING,Follow._STRING_in_call_stmt867); 
    				DebugLocation(182, 80);
    				Match(input,47,Follow._47_in_call_stmt869); 

    				}
    				break;
    			case 2:
    				DebugEnterAlt(2);
    				// C:\\output\\HoldingRule.g:182:86: query= STRING
    				{
    				DebugLocation(182, 91);
    				query=(IToken)Match(input,STRING,Follow._STRING_in_call_stmt875); 

    				}
    				break;

    			}
    			} finally { DebugExitSubRule(13); }

    			DebugLocation(182, 100);
    			Match(input,23,Follow._23_in_call_stmt878); 
    			DebugLocation(183, 3);
    			 Value = new Procedures.InlineQueryProcedure(Expression.UnescapeString((query!=null?query.Text:null))); 

    			}
    			break;
    		case 10:
    			DebugEnterAlt(10);
    			// C:\\output\\HoldingRule.g:184:4: ( 'RULE' | 'Rule' | 'rule' ) ( '(' rule= INT ',' count= expression ',' currency= expression ',' asset= expression ')' | rule= INT ',' count= expression ',' currency= expression ',' asset= expression ) ';'
    			{
    			DebugLocation(184, 4);
    			if ((input.LA(1)>=106 && input.LA(1)<=108))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(184, 27);
    			// C:\\output\\HoldingRule.g:184:27: ( '(' rule= INT ',' count= expression ',' currency= expression ',' asset= expression ')' | rule= INT ',' count= expression ',' currency= expression ',' asset= expression )
    			int alt14=2;
    			try { DebugEnterSubRule(14);
    			try { DebugEnterDecision(14, decisionCanBacktrack[14]);
    			int LA14_0 = input.LA(1);

    			if ((LA14_0==46))
    			{
    				alt14=1;
    			}
    			else if ((LA14_0==INT))
    			{
    				alt14=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 14, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    			} finally { DebugExitDecision(14); }
    			switch (alt14)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:184:28: '(' rule= INT ',' count= expression ',' currency= expression ',' asset= expression ')'
    				{
    				DebugLocation(184, 28);
    				Match(input,46,Follow._46_in_call_stmt896); 
    				DebugLocation(184, 36);
    				rule=(IToken)Match(input,INT,Follow._INT_in_call_stmt900); 
    				DebugLocation(184, 41);
    				Match(input,36,Follow._36_in_call_stmt902); 
    				DebugLocation(184, 50);
    				PushFollow(Follow._expression_in_call_stmt906);
    				count=expression();
    				PopFollow();

    				DebugLocation(184, 62);
    				Match(input,36,Follow._36_in_call_stmt908); 
    				DebugLocation(184, 74);
    				PushFollow(Follow._expression_in_call_stmt912);
    				currency=expression();
    				PopFollow();

    				DebugLocation(184, 86);
    				Match(input,36,Follow._36_in_call_stmt914); 
    				DebugLocation(184, 95);
    				PushFollow(Follow._expression_in_call_stmt918);
    				asset=expression();
    				PopFollow();

    				DebugLocation(184, 107);
    				Match(input,47,Follow._47_in_call_stmt920); 

    				}
    				break;
    			case 2:
    				DebugEnterAlt(2);
    				// C:\\output\\HoldingRule.g:184:113: rule= INT ',' count= expression ',' currency= expression ',' asset= expression
    				{
    				DebugLocation(184, 117);
    				rule=(IToken)Match(input,INT,Follow._INT_in_call_stmt926); 
    				DebugLocation(184, 122);
    				Match(input,36,Follow._36_in_call_stmt928); 
    				DebugLocation(184, 131);
    				PushFollow(Follow._expression_in_call_stmt932);
    				count=expression();
    				PopFollow();

    				DebugLocation(184, 143);
    				Match(input,36,Follow._36_in_call_stmt934); 
    				DebugLocation(184, 155);
    				PushFollow(Follow._expression_in_call_stmt938);
    				currency=expression();
    				PopFollow();

    				DebugLocation(184, 167);
    				Match(input,36,Follow._36_in_call_stmt940); 
    				DebugLocation(184, 176);
    				PushFollow(Follow._expression_in_call_stmt944);
    				asset=expression();
    				PopFollow();


    				}
    				break;

    			}
    			} finally { DebugExitSubRule(14); }

    			DebugLocation(184, 189);
    			Match(input,23,Follow._23_in_call_stmt947); 
    			DebugLocation(185, 3);
    			 Value = new Procedures.RegistryRuleProcedure(Int32.Parse((rule!=null?rule.Text:null)), count, currency, asset); 

    			}
    			break;
    		case 11:
    			DebugEnterAlt(11);
    			// C:\\output\\HoldingRule.g:186:4: ( 'MOVEASSETS' | 'MoveAssets' | 'moveAssets' | 'moveassets' ) source= expression ';'
    			{
    			DebugLocation(186, 4);
    			if ((input.LA(1)>=109 && input.LA(1)<=112))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(186, 64);
    			PushFollow(Follow._expression_in_call_stmt968);
    			source=expression();
    			PopFollow();

    			DebugLocation(186, 76);
    			Match(input,23,Follow._23_in_call_stmt970); 
    			DebugLocation(187, 3);
    			 Value = new Procedures.MoveAssetsProcedure(source); 

    			}
    			break;

    		}
    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(188, 1);
    	} finally { DebugExitRule(GrammarFileName, "call_stmt"); }
    	return Value;

    }
    // $ANTLR end "call_stmt"


    protected virtual void Enter_distribute_stmt() {}
    protected virtual void Leave_distribute_stmt() {}

    // $ANTLR start "distribute_stmt"
    // C:\\output\\HoldingRule.g:190:1: distribute_stmt returns [Statement Value] : ( 'DISTRIBUTE' | 'Distribute' | 'distribute' ) '(' asset= expression ( ',' ( '*' | count= expression ) )? ')' statement ;
    [GrammarRule("distribute_stmt")]
    private Statement distribute_stmt()
    {

        Statement Value = default(Statement);

        Expression asset = default(Expression);
        Expression count = default(Expression);
        Statement statement23 = default(Statement);

    	try { DebugEnterRule(GrammarFileName, "distribute_stmt");
    	DebugLocation(190, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:192:2: ( ( 'DISTRIBUTE' | 'Distribute' | 'distribute' ) '(' asset= expression ( ',' ( '*' | count= expression ) )? ')' statement )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:192:4: ( 'DISTRIBUTE' | 'Distribute' | 'distribute' ) '(' asset= expression ( ',' ( '*' | count= expression ) )? ')' statement
    		{
    		DebugLocation(192, 4);
    		if ((input.LA(1)>=113 && input.LA(1)<=115))
    		{
    			input.Consume();
    			state.errorRecovery=false;
    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			throw mse;
    		}

    		DebugLocation(193, 3);
    		Match(input,46,Follow._46_in_distribute_stmt1000); 
    		DebugLocation(193, 12);
    		PushFollow(Follow._expression_in_distribute_stmt1004);
    		asset=expression();
    		PopFollow();

    		DebugLocation(193, 24);
    		// C:\\output\\HoldingRule.g:193:24: ( ',' ( '*' | count= expression ) )?
    		int alt17=2;
    		try { DebugEnterSubRule(17);
    		try { DebugEnterDecision(17, decisionCanBacktrack[17]);
    		int LA17_0 = input.LA(1);

    		if ((LA17_0==36))
    		{
    			alt17=1;
    		}
    		} finally { DebugExitDecision(17); }
    		switch (alt17)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:193:25: ',' ( '*' | count= expression )
    			{
    			DebugLocation(193, 25);
    			Match(input,36,Follow._36_in_distribute_stmt1007); 
    			DebugLocation(193, 29);
    			// C:\\output\\HoldingRule.g:193:29: ( '*' | count= expression )
    			int alt16=2;
    			try { DebugEnterSubRule(16);
    			try { DebugEnterDecision(16, decisionCanBacktrack[16]);
    			int LA16_0 = input.LA(1);

    			if ((LA16_0==116))
    			{
    				alt16=1;
    			}
    			else if (((LA16_0>=IDENTIFIER && LA16_0<=STRING)||LA16_0==NOT||(LA16_0>=DEC && LA16_0<=TRANSACT_FIELD)||LA16_0==46||(LA16_0>=60 && LA16_0<=65)||(LA16_0>=67 && LA16_0<=69)||LA16_0==118||(LA16_0>=121 && LA16_0<=160)))
    			{
    				alt16=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 16, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    			} finally { DebugExitDecision(16); }
    			switch (alt16)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:193:30: '*'
    				{
    				DebugLocation(193, 30);
    				Match(input,116,Follow._116_in_distribute_stmt1010); 

    				}
    				break;
    			case 2:
    				DebugEnterAlt(2);
    				// C:\\output\\HoldingRule.g:193:36: count= expression
    				{
    				DebugLocation(193, 41);
    				PushFollow(Follow._expression_in_distribute_stmt1016);
    				count=expression();
    				PopFollow();


    				}
    				break;

    			}
    			} finally { DebugExitSubRule(16); }


    			}
    			break;

    		}
    		} finally { DebugExitSubRule(17); }

    		DebugLocation(193, 56);
    		Match(input,47,Follow._47_in_distribute_stmt1021); 
    		DebugLocation(193, 60);
    		PushFollow(Follow._statement_in_distribute_stmt1023);
    		statement23=statement();
    		PopFollow();

    		DebugLocation(194, 3);

    					Value = Statement.Distribute(asset, count, statement23);
    				

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(197, 1);
    	} finally { DebugExitRule(GrammarFileName, "distribute_stmt"); }
    	return Value;

    }
    // $ANTLR end "distribute_stmt"


    protected virtual void Enter_zero_expression() {}
    protected virtual void Leave_zero_expression() {}

    // $ANTLR start "zero_expression"
    // C:\\output\\HoldingRule.g:199:1: zero_expression : ( '(' ')' )? ;
    [GrammarRule("zero_expression")]
    private void zero_expression()
    {

    	try { DebugEnterRule(GrammarFileName, "zero_expression");
    	DebugLocation(199, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:200:2: ( ( '(' ')' )? )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:200:4: ( '(' ')' )?
    		{
    		DebugLocation(200, 4);
    		// C:\\output\\HoldingRule.g:200:4: ( '(' ')' )?
    		int alt18=2;
    		try { DebugEnterSubRule(18);
    		try { DebugEnterDecision(18, decisionCanBacktrack[18]);
    		int LA18_0 = input.LA(1);

    		if ((LA18_0==46))
    		{
    			alt18=1;
    		}
    		} finally { DebugExitDecision(18); }
    		switch (alt18)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:200:5: '(' ')'
    			{
    			DebugLocation(200, 5);
    			Match(input,46,Follow._46_in_zero_expression1039); 
    			DebugLocation(200, 9);
    			Match(input,47,Follow._47_in_zero_expression1041); 

    			}
    			break;

    		}
    		} finally { DebugExitSubRule(18); }


    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(201, 1);
    	} finally { DebugExitRule(GrammarFileName, "zero_expression"); }
    	return;

    }
    // $ANTLR end "zero_expression"


    protected virtual void Enter_assign_stmt() {}
    protected virtual void Leave_assign_stmt() {}

    // $ANTLR start "assign_stmt"
    // C:\\output\\HoldingRule.g:203:1: assign_stmt returns [Statements.AssignStatement Value] : left_value op= ( EQUAL | OPEQUAL ) expression ';' ;
    [GrammarRule("assign_stmt")]
    private Statements.AssignStatement assign_stmt()
    {

        Statements.AssignStatement Value = default(Statements.AssignStatement);

        IToken op=null;
        List<Identifier> left_value24 = default(List<Identifier>);
        Expression expression25 = default(Expression);

    	try { DebugEnterRule(GrammarFileName, "assign_stmt");
    	DebugLocation(203, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:205:2: ( left_value op= ( EQUAL | OPEQUAL ) expression ';' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:205:4: left_value op= ( EQUAL | OPEQUAL ) expression ';'
    		{
    		DebugLocation(205, 4);
    		PushFollow(Follow._left_value_in_assign_stmt1059);
    		left_value24=left_value();
    		PopFollow();

    		DebugLocation(205, 17);
    		op=(IToken)input.LT(1);
    		if ((input.LA(1)>=EQUAL && input.LA(1)<=OPEQUAL))
    		{
    			input.Consume();
    			state.errorRecovery=false;
    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			throw mse;
    		}

    		DebugLocation(205, 36);
    		PushFollow(Follow._expression_in_assign_stmt1071);
    		expression25=expression();
    		PopFollow();

    		DebugLocation(205, 47);
    		Match(input,23,Follow._23_in_assign_stmt1073); 
    		DebugLocation(206, 3);
    		 Value = Statement.Assign(left_value24, op.Text[0], expression25); 

    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(207, 1);
    	} finally { DebugExitRule(GrammarFileName, "assign_stmt"); }
    	return Value;

    }
    // $ANTLR end "assign_stmt"


    protected virtual void Enter_left_value() {}
    protected virtual void Leave_left_value() {}

    // $ANTLR start "left_value"
    // C:\\output\\HoldingRule.g:209:1: left_value returns [List<Identifier> Value = new List<Identifier>()] : (i0= identifier | '(' i1= identifier ( ',' i2= identifier )* ')' );
    [GrammarRule("left_value")]
    private List<Identifier> left_value()
    {

        List<Identifier> Value =  new List<Identifier>();

        Identifier i0 = default(Identifier);
        Identifier i1 = default(Identifier);
        Identifier i2 = default(Identifier);

    	try { DebugEnterRule(GrammarFileName, "left_value");
    	DebugLocation(209, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:211:2: (i0= identifier | '(' i1= identifier ( ',' i2= identifier )* ')' )
    		int alt20=2;
    		try { DebugEnterDecision(20, decisionCanBacktrack[20]);
    		int LA20_0 = input.LA(1);

    		if ((LA20_0==IDENTIFIER||LA20_0==TRANSACT_FIELD))
    		{
    			alt20=1;
    		}
    		else if ((LA20_0==46))
    		{
    			alt20=2;
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 20, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(20); }
    		switch (alt20)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:211:4: i0= identifier
    			{
    			DebugLocation(211, 6);
    			PushFollow(Follow._identifier_in_left_value1095);
    			i0=identifier();
    			PopFollow();

    			DebugLocation(211, 21);
    			 Value.Add(i0); 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:212:4: '(' i1= identifier ( ',' i2= identifier )* ')'
    			{
    			DebugLocation(212, 4);
    			Match(input,46,Follow._46_in_left_value1105); 
    			DebugLocation(212, 10);
    			PushFollow(Follow._identifier_in_left_value1109);
    			i1=identifier();
    			PopFollow();

    			DebugLocation(212, 24);
    			 Value.Add(i1); 
    			DebugLocation(213, 4);
    			// C:\\output\\HoldingRule.g:213:4: ( ',' i2= identifier )*
    			try { DebugEnterSubRule(19);
    			while (true)
    			{
    				int alt19=2;
    				try { DebugEnterDecision(19, decisionCanBacktrack[19]);
    				int LA19_0 = input.LA(1);

    				if ((LA19_0==36))
    				{
    					alt19=1;
    				}


    				} finally { DebugExitDecision(19); }
    				switch ( alt19 )
    				{
    				case 1:
    					DebugEnterAlt(1);
    					// C:\\output\\HoldingRule.g:213:6: ',' i2= identifier
    					{
    					DebugLocation(213, 6);
    					Match(input,36,Follow._36_in_left_value1120); 
    					DebugLocation(213, 12);
    					PushFollow(Follow._identifier_in_left_value1124);
    					i2=identifier();
    					PopFollow();

    					DebugLocation(213, 25);
    					 Value.Add(i2); 

    					}
    					break;

    				default:
    					goto loop19;
    				}
    			}

    			loop19:
    				;

    			} finally { DebugExitSubRule(19); }

    			DebugLocation(214, 3);
    			Match(input,47,Follow._47_in_left_value1134); 

    			}
    			break;

    		}
    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(215, 1);
    	} finally { DebugExitRule(GrammarFileName, "left_value"); }
    	return Value;

    }
    // $ANTLR end "left_value"


    protected virtual void Enter_expression() {}
    protected virtual void Leave_expression() {}

    // $ANTLR start "expression"
    // C:\\output\\HoldingRule.g:217:1: expression returns [Expression Value] : left= and_expression ( OR right= and_expression )* ;
    [GrammarRule("expression")]
    private Expression expression()
    {

        Expression Value = default(Expression);

        Expression left = default(Expression);
        Expression right = default(Expression);

    	try { DebugEnterRule(GrammarFileName, "expression");
    	DebugLocation(217, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:219:2: (left= and_expression ( OR right= and_expression )* )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:219:4: left= and_expression ( OR right= and_expression )*
    		{
    		DebugLocation(219, 8);
    		PushFollow(Follow._and_expression_in_expression1152);
    		left=and_expression();
    		PopFollow();

    		DebugLocation(219, 26);
    		 Value = left; 
    		DebugLocation(220, 3);
    		// C:\\output\\HoldingRule.g:220:3: ( OR right= and_expression )*
    		try { DebugEnterSubRule(21);
    		while (true)
    		{
    			int alt21=2;
    			try { DebugEnterDecision(21, decisionCanBacktrack[21]);
    			int LA21_0 = input.LA(1);

    			if ((LA21_0==OR))
    			{
    				alt21=1;
    			}


    			} finally { DebugExitDecision(21); }
    			switch ( alt21 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:220:4: OR right= and_expression
    				{
    				DebugLocation(220, 4);
    				Match(input,OR,Follow._OR_in_expression1161); 
    				DebugLocation(220, 12);
    				PushFollow(Follow._and_expression_in_expression1165);
    				right=and_expression();
    				PopFollow();

    				DebugLocation(220, 28);
    				 Value = new JoinedExpression(ExpressionOp.Or, Value, right); 

    				}
    				break;

    			default:
    				goto loop21;
    			}
    		}

    		loop21:
    			;

    		} finally { DebugExitSubRule(21); }


    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(222, 1);
    	} finally { DebugExitRule(GrammarFileName, "expression"); }
    	return Value;

    }
    // $ANTLR end "expression"


    protected virtual void Enter_and_expression() {}
    protected virtual void Leave_and_expression() {}

    // $ANTLR start "and_expression"
    // C:\\output\\HoldingRule.g:224:1: and_expression returns [Expression Value] : left= test_expression ( AND right= test_expression )* ;
    [GrammarRule("and_expression")]
    private Expression and_expression()
    {

        Expression Value = default(Expression);

        Expression left = default(Expression);
        Expression right = default(Expression);

    	try { DebugEnterRule(GrammarFileName, "and_expression");
    	DebugLocation(224, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:226:2: (left= test_expression ( AND right= test_expression )* )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:226:4: left= test_expression ( AND right= test_expression )*
    		{
    		DebugLocation(226, 8);
    		PushFollow(Follow._test_expression_in_and_expression1190);
    		left=test_expression();
    		PopFollow();

    		DebugLocation(226, 26);
    		 Value = left; 
    		DebugLocation(227, 3);
    		// C:\\output\\HoldingRule.g:227:3: ( AND right= test_expression )*
    		try { DebugEnterSubRule(22);
    		while (true)
    		{
    			int alt22=2;
    			try { DebugEnterDecision(22, decisionCanBacktrack[22]);
    			int LA22_0 = input.LA(1);

    			if ((LA22_0==AND))
    			{
    				alt22=1;
    			}


    			} finally { DebugExitDecision(22); }
    			switch ( alt22 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:227:4: AND right= test_expression
    				{
    				DebugLocation(227, 4);
    				Match(input,AND,Follow._AND_in_and_expression1198); 
    				DebugLocation(227, 13);
    				PushFollow(Follow._test_expression_in_and_expression1202);
    				right=test_expression();
    				PopFollow();

    				DebugLocation(227, 30);
    				 Value = new JoinedExpression(ExpressionOp.And, Value, right); 

    				}
    				break;

    			default:
    				goto loop22;
    			}
    		}

    		loop22:
    			;

    		} finally { DebugExitSubRule(22); }


    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(229, 1);
    	} finally { DebugExitRule(GrammarFileName, "and_expression"); }
    	return Value;

    }
    // $ANTLR end "and_expression"


    protected virtual void Enter_test_expression() {}
    protected virtual void Leave_test_expression() {}

    // $ANTLR start "test_expression"
    // C:\\output\\HoldingRule.g:231:1: test_expression returns [Expression Value] : left= math_expression ( (op= EQUAL | op= COMPARE | (not= NOT )? op= ( IN | CONTAINS ) ) right= math_expression )? ;
    [GrammarRule("test_expression")]
    private Expression test_expression()
    {

        Expression Value = default(Expression);

        IToken op=null;
        IToken not=null;
        Expression left = default(Expression);
        Expression right = default(Expression);

    	try { DebugEnterRule(GrammarFileName, "test_expression");
    	DebugLocation(231, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:233:2: (left= math_expression ( (op= EQUAL | op= COMPARE | (not= NOT )? op= ( IN | CONTAINS ) ) right= math_expression )? )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:233:4: left= math_expression ( (op= EQUAL | op= COMPARE | (not= NOT )? op= ( IN | CONTAINS ) ) right= math_expression )?
    		{
    		DebugLocation(233, 8);
    		PushFollow(Follow._math_expression_in_test_expression1227);
    		left=math_expression();
    		PopFollow();

    		DebugLocation(233, 25);
    		 Value = left; 
    		DebugLocation(234, 3);
    		// C:\\output\\HoldingRule.g:234:3: ( (op= EQUAL | op= COMPARE | (not= NOT )? op= ( IN | CONTAINS ) ) right= math_expression )?
    		int alt25=2;
    		try { DebugEnterSubRule(25);
    		try { DebugEnterDecision(25, decisionCanBacktrack[25]);
    		int LA25_0 = input.LA(1);

    		if ((LA25_0==EQUAL||(LA25_0>=COMPARE && LA25_0<=CONTAINS)))
    		{
    			alt25=1;
    		}
    		} finally { DebugExitDecision(25); }
    		switch (alt25)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:234:5: (op= EQUAL | op= COMPARE | (not= NOT )? op= ( IN | CONTAINS ) ) right= math_expression
    			{
    			DebugLocation(234, 5);
    			// C:\\output\\HoldingRule.g:234:5: (op= EQUAL | op= COMPARE | (not= NOT )? op= ( IN | CONTAINS ) )
    			int alt24=3;
    			try { DebugEnterSubRule(24);
    			try { DebugEnterDecision(24, decisionCanBacktrack[24]);
    			switch (input.LA(1))
    			{
    			case EQUAL:
    				{
    				alt24=1;
    				}
    				break;
    			case COMPARE:
    				{
    				alt24=2;
    				}
    				break;
    			case NOT:
    			case IN:
    			case CONTAINS:
    				{
    				alt24=3;
    				}
    				break;
    			default:
    				{
    					NoViableAltException nvae = new NoViableAltException("", 24, 0, input);

    					DebugRecognitionException(nvae);
    					throw nvae;
    				}
    			}

    			} finally { DebugExitDecision(24); }
    			switch (alt24)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:234:6: op= EQUAL
    				{
    				DebugLocation(234, 8);
    				op=(IToken)Match(input,EQUAL,Follow._EQUAL_in_test_expression1238); 

    				}
    				break;
    			case 2:
    				DebugEnterAlt(2);
    				// C:\\output\\HoldingRule.g:234:17: op= COMPARE
    				{
    				DebugLocation(234, 19);
    				op=(IToken)Match(input,COMPARE,Follow._COMPARE_in_test_expression1244); 

    				}
    				break;
    			case 3:
    				DebugEnterAlt(3);
    				// C:\\output\\HoldingRule.g:234:30: (not= NOT )? op= ( IN | CONTAINS )
    				{
    				DebugLocation(234, 33);
    				// C:\\output\\HoldingRule.g:234:33: (not= NOT )?
    				int alt23=2;
    				try { DebugEnterSubRule(23);
    				try { DebugEnterDecision(23, decisionCanBacktrack[23]);
    				int LA23_0 = input.LA(1);

    				if ((LA23_0==NOT))
    				{
    					alt23=1;
    				}
    				} finally { DebugExitDecision(23); }
    				switch (alt23)
    				{
    				case 1:
    					DebugEnterAlt(1);
    					// C:\\output\\HoldingRule.g:234:33: not= NOT
    					{
    					DebugLocation(234, 33);
    					not=(IToken)Match(input,NOT,Follow._NOT_in_test_expression1250); 

    					}
    					break;

    				}
    				} finally { DebugExitSubRule(23); }

    				DebugLocation(234, 41);
    				op=(IToken)input.LT(1);
    				if ((input.LA(1)>=IN && input.LA(1)<=CONTAINS))
    				{
    					input.Consume();
    					state.errorRecovery=false;
    				}
    				else
    				{
    					MismatchedSetException mse = new MismatchedSetException(null,input);
    					DebugRecognitionException(mse);
    					throw mse;
    				}


    				}
    				break;

    			}
    			} finally { DebugExitSubRule(24); }

    			DebugLocation(234, 62);
    			PushFollow(Follow._math_expression_in_test_expression1264);
    			right=math_expression();
    			PopFollow();

    			DebugLocation(235, 4);

    							string opText = (op!=null?op.Text:null).ToUpperInvariant();	// '=' | '==' | '!=' | '<>' | '<=' | '<' | '>=' | '>' | 'IN'
    							if (opText == "==")
    								opText = "=";
    							else if (opText == "!=")
    								opText = "<>";			// '=' | '<>' | '<=' | '<' | '>=' | '>'
    							if (not != null)
    								opText = "NOT" + opText;
    							Expression leftExp = left;
    							Expression rightExp = right;
    							if (leftExp.IsNull)
    							{
    								if (opText == "IN" || opText == "NOTIN" || opText == "CONTAINS" || opText == "NOCONTAINS")
    									Value = Expression.Value(ExpressionType.Boolean, "0");
    								else if (rightExp.IsNull)
    									Value = Expression.Value(ExpressionType.Boolean, opText.IndexOf('=') >= 0 ? "1": "0");
    								else
    									Value = Expression.Value(ExpressionType.Boolean, opText.IndexOf('<') >= 0 ? "1": "0");
    							}
    							else if (rightExp.IsNull)
    							{
    								if (opText == "IN" || opText == "NOTIN" || opText == "CONTAINS" || opText == "NOCONTAINS")
    									Value = Expression.Value(ExpressionType.Boolean, "0");
    								else
    									Value = Expression.Value(ExpressionType.Boolean, opText.IndexOf('>') >= 0 ? "1": "0");
    							}
    							else
    							{
    								Value = new JoinedExpression(ExpressionOp.Create(opText), leftExp, rightExp);
    							}
    						

    			}
    			break;

    		}
    		} finally { DebugExitSubRule(25); }


    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(267, 1);
    	} finally { DebugExitRule(GrammarFileName, "test_expression"); }
    	return Value;

    }
    // $ANTLR end "test_expression"


    protected virtual void Enter_math_expression() {}
    protected virtual void Leave_math_expression() {}

    // $ANTLR start "math_expression"
    // C:\\output\\HoldingRule.g:269:1: math_expression returns [Expression Value] : left= mult_expression (op= ( '+' | '-' ) right= mult_expression )* ;
    [GrammarRule("math_expression")]
    private Expression math_expression()
    {

        Expression Value = default(Expression);

        IToken op=null;
        Expression left = default(Expression);
        Expression right = default(Expression);

    	try { DebugEnterRule(GrammarFileName, "math_expression");
    	DebugLocation(269, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:271:2: (left= mult_expression (op= ( '+' | '-' ) right= mult_expression )* )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:271:4: left= mult_expression (op= ( '+' | '-' ) right= mult_expression )*
    		{
    		DebugLocation(271, 8);
    		PushFollow(Follow._mult_expression_in_math_expression1292);
    		left=mult_expression();
    		PopFollow();

    		DebugLocation(271, 27);
    		 Value = left; 
    		DebugLocation(272, 3);
    		// C:\\output\\HoldingRule.g:272:3: (op= ( '+' | '-' ) right= mult_expression )*
    		try { DebugEnterSubRule(26);
    		while (true)
    		{
    			int alt26=2;
    			try { DebugEnterDecision(26, decisionCanBacktrack[26]);
    			int LA26_0 = input.LA(1);

    			if (((LA26_0>=117 && LA26_0<=118)))
    			{
    				alt26=1;
    			}


    			} finally { DebugExitDecision(26); }
    			switch ( alt26 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:272:5: op= ( '+' | '-' ) right= mult_expression
    				{
    				DebugLocation(272, 7);
    				op=(IToken)input.LT(1);
    				if ((input.LA(1)>=117 && input.LA(1)<=118))
    				{
    					input.Consume();
    					state.errorRecovery=false;
    				}
    				else
    				{
    					MismatchedSetException mse = new MismatchedSetException(null,input);
    					DebugRecognitionException(mse);
    					throw mse;
    				}

    				DebugLocation(272, 23);
    				PushFollow(Follow._mult_expression_in_math_expression1312);
    				right=mult_expression();
    				PopFollow();

    				DebugLocation(273, 11);
    				 Value = new JoinedExpression(ExpressionOp.Create((op!=null?op.Text:null)), Value, right); 

    				}
    				break;

    			default:
    				goto loop26;
    			}
    		}

    		loop26:
    			;

    		} finally { DebugExitSubRule(26); }


    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(275, 1);
    	} finally { DebugExitRule(GrammarFileName, "math_expression"); }
    	return Value;

    }
    // $ANTLR end "math_expression"


    protected virtual void Enter_mult_expression() {}
    protected virtual void Leave_mult_expression() {}

    // $ANTLR start "mult_expression"
    // C:\\output\\HoldingRule.g:277:1: mult_expression returns [Expression Value] : left= unary_expression (op= ( '*' | '/' | '%' ) right= unary_expression )* ;
    [GrammarRule("mult_expression")]
    private Expression mult_expression()
    {

        Expression Value = default(Expression);

        IToken op=null;
        Expression left = default(Expression);
        Expression right = default(Expression);

    	try { DebugEnterRule(GrammarFileName, "mult_expression");
    	DebugLocation(277, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:279:2: (left= unary_expression (op= ( '*' | '/' | '%' ) right= unary_expression )* )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:279:4: left= unary_expression (op= ( '*' | '/' | '%' ) right= unary_expression )*
    		{
    		DebugLocation(279, 8);
    		PushFollow(Follow._unary_expression_in_mult_expression1347);
    		left=unary_expression();
    		PopFollow();

    		DebugLocation(279, 28);
    		 Value = left; 
    		DebugLocation(280, 3);
    		// C:\\output\\HoldingRule.g:280:3: (op= ( '*' | '/' | '%' ) right= unary_expression )*
    		try { DebugEnterSubRule(27);
    		while (true)
    		{
    			int alt27=2;
    			try { DebugEnterDecision(27, decisionCanBacktrack[27]);
    			int LA27_0 = input.LA(1);

    			if ((LA27_0==116||(LA27_0>=119 && LA27_0<=120)))
    			{
    				alt27=1;
    			}


    			} finally { DebugExitDecision(27); }
    			switch ( alt27 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:280:5: op= ( '*' | '/' | '%' ) right= unary_expression
    				{
    				DebugLocation(280, 7);
    				op=(IToken)input.LT(1);
    				if (input.LA(1)==116||(input.LA(1)>=119 && input.LA(1)<=120))
    				{
    					input.Consume();
    					state.errorRecovery=false;
    				}
    				else
    				{
    					MismatchedSetException mse = new MismatchedSetException(null,input);
    					DebugRecognitionException(mse);
    					throw mse;
    				}

    				DebugLocation(280, 27);
    				PushFollow(Follow._unary_expression_in_mult_expression1369);
    				right=unary_expression();
    				PopFollow();

    				DebugLocation(281, 11);
    				 Value = new JoinedExpression(ExpressionOp.Create((op!=null?op.Text:null)), Value, right); 

    				}
    				break;

    			default:
    				goto loop27;
    			}
    		}

    		loop27:
    			;

    		} finally { DebugExitSubRule(27); }


    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(283, 1);
    	} finally { DebugExitRule(GrammarFileName, "mult_expression"); }
    	return Value;

    }
    // $ANTLR end "mult_expression"


    protected virtual void Enter_unary_expression() {}
    protected virtual void Leave_unary_expression() {}

    // $ANTLR start "unary_expression"
    // C:\\output\\HoldingRule.g:286:1: unary_expression returns [Expression Value] : ( primary_expression | ( '-' | NOT ) value= primary_expression );
    [GrammarRule("unary_expression")]
    private Expression unary_expression()
    {

        Expression Value = default(Expression);

        Expression value = default(Expression);
        Expression primary_expression26 = default(Expression);

         ExpressionOp op = ExpressionOp.Neg; 
    	try { DebugEnterRule(GrammarFileName, "unary_expression");
    	DebugLocation(286, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:289:2: ( primary_expression | ( '-' | NOT ) value= primary_expression )
    		int alt29=2;
    		try { DebugEnterDecision(29, decisionCanBacktrack[29]);
    		int LA29_0 = input.LA(1);

    		if (((LA29_0>=IDENTIFIER && LA29_0<=STRING)||(LA29_0>=DEC && LA29_0<=TRANSACT_FIELD)||LA29_0==46||(LA29_0>=60 && LA29_0<=65)||(LA29_0>=67 && LA29_0<=69)||(LA29_0>=121 && LA29_0<=160)))
    		{
    			alt29=1;
    		}
    		else if ((LA29_0==NOT||LA29_0==118))
    		{
    			alt29=2;
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 29, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(29); }
    		switch (alt29)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:289:4: primary_expression
    			{
    			DebugLocation(289, 4);
    			PushFollow(Follow._primary_expression_in_unary_expression1409);
    			primary_expression26=primary_expression();
    			PopFollow();

    			DebugLocation(290, 3);
    			 Value = primary_expression26; 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:291:4: ( '-' | NOT ) value= primary_expression
    			{
    			DebugLocation(291, 4);
    			// C:\\output\\HoldingRule.g:291:4: ( '-' | NOT )
    			int alt28=2;
    			try { DebugEnterSubRule(28);
    			try { DebugEnterDecision(28, decisionCanBacktrack[28]);
    			int LA28_0 = input.LA(1);

    			if ((LA28_0==118))
    			{
    				alt28=1;
    			}
    			else if ((LA28_0==NOT))
    			{
    				alt28=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 28, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    			} finally { DebugExitDecision(28); }
    			switch (alt28)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:291:5: '-'
    				{
    				DebugLocation(291, 5);
    				Match(input,118,Follow._118_in_unary_expression1419); 
    				DebugLocation(291, 9);
    				 op = ExpressionOp.Neg; 

    				}
    				break;
    			case 2:
    				DebugEnterAlt(2);
    				// C:\\output\\HoldingRule.g:292:4: NOT
    				{
    				DebugLocation(292, 4);
    				Match(input,NOT,Follow._NOT_in_unary_expression1426); 
    				DebugLocation(292, 8);
    				 op = ExpressionOp.Not; 

    				}
    				break;

    			}
    			} finally { DebugExitSubRule(28); }

    			DebugLocation(293, 10);
    			PushFollow(Follow._primary_expression_in_unary_expression1436);
    			value=primary_expression();
    			PopFollow();

    			DebugLocation(294, 3);
    			 Value = new UnaryExpression(op, value); 

    			}
    			break;

    		}
    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(295, 1);
    	} finally { DebugExitRule(GrammarFileName, "unary_expression"); }
    	return Value;

    }
    // $ANTLR end "unary_expression"


    protected virtual void Enter_primary_expression() {}
    protected virtual void Leave_primary_expression() {}

    // $ANTLR start "primary_expression"
    // C:\\output\\HoldingRule.g:298:1: primary_expression returns [Expression Value] : ( identifier | constant | internal_function | '(' left= expression ( ',' right= expression )* ')' );
    [GrammarRule("primary_expression")]
    private Expression primary_expression()
    {

        Expression Value = default(Expression);

        Expression left = default(Expression);
        Expression right = default(Expression);
        Identifier identifier27 = default(Identifier);
        Expression constant28 = default(Expression);
        Expression internal_function29 = default(Expression);

         List<Expression> list = null; 
    	try { DebugEnterRule(GrammarFileName, "primary_expression");
    	DebugLocation(298, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:301:2: ( identifier | constant | internal_function | '(' left= expression ( ',' right= expression )* ')' )
    		int alt31=4;
    		try { DebugEnterDecision(31, decisionCanBacktrack[31]);
    		switch (input.LA(1))
    		{
    		case IDENTIFIER:
    		case TRANSACT_FIELD:
    			{
    			alt31=1;
    			}
    			break;
    		case INT:
    		case STRING:
    		case DEC:
    		case 152:
    		case 153:
    		case 154:
    		case 155:
    		case 156:
    		case 157:
    		case 158:
    		case 159:
    		case 160:
    			{
    			alt31=2;
    			}
    			break;
    		case 60:
    		case 61:
    		case 62:
    		case 63:
    		case 64:
    		case 65:
    		case 67:
    		case 68:
    		case 69:
    		case 121:
    		case 122:
    		case 123:
    		case 124:
    		case 125:
    		case 126:
    		case 127:
    		case 128:
    		case 129:
    		case 130:
    		case 131:
    		case 132:
    		case 133:
    		case 134:
    		case 135:
    		case 136:
    		case 137:
    		case 138:
    		case 139:
    		case 140:
    		case 141:
    		case 142:
    		case 143:
    		case 144:
    		case 145:
    		case 146:
    		case 147:
    		case 148:
    		case 149:
    		case 150:
    		case 151:
    			{
    			alt31=3;
    			}
    			break;
    		case 46:
    			{
    			alt31=4;
    			}
    			break;
    		default:
    			{
    				NoViableAltException nvae = new NoViableAltException("", 31, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}

    		} finally { DebugExitDecision(31); }
    		switch (alt31)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:301:4: identifier
    			{
    			DebugLocation(301, 4);
    			PushFollow(Follow._identifier_in_primary_expression1464);
    			identifier27=identifier();
    			PopFollow();

    			DebugLocation(302, 3);
    			 Value = identifier27; 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:303:4: constant
    			{
    			DebugLocation(303, 4);
    			PushFollow(Follow._constant_in_primary_expression1473);
    			constant28=constant();
    			PopFollow();

    			DebugLocation(304, 3);
    			 Value = constant28; 

    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:305:4: internal_function
    			{
    			DebugLocation(305, 4);
    			PushFollow(Follow._internal_function_in_primary_expression1482);
    			internal_function29=internal_function();
    			PopFollow();

    			DebugLocation(306, 3);
    			 Value = internal_function29; 

    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:307:4: '(' left= expression ( ',' right= expression )* ')'
    			{
    			DebugLocation(307, 4);
    			Match(input,46,Follow._46_in_primary_expression1491); 
    			DebugLocation(307, 12);
    			PushFollow(Follow._expression_in_primary_expression1495);
    			left=expression();
    			PopFollow();

    			DebugLocation(307, 24);
    			 list = new List<Expression>(); list.Add(left); 
    			DebugLocation(308, 4);
    			// C:\\output\\HoldingRule.g:308:4: ( ',' right= expression )*
    			try { DebugEnterSubRule(30);
    			while (true)
    			{
    				int alt30=2;
    				try { DebugEnterDecision(30, decisionCanBacktrack[30]);
    				int LA30_0 = input.LA(1);

    				if ((LA30_0==36))
    				{
    					alt30=1;
    				}


    				} finally { DebugExitDecision(30); }
    				switch ( alt30 )
    				{
    				case 1:
    					DebugEnterAlt(1);
    					// C:\\output\\HoldingRule.g:308:6: ',' right= expression
    					{
    					DebugLocation(308, 6);
    					Match(input,36,Follow._36_in_primary_expression1504); 
    					DebugLocation(308, 15);
    					PushFollow(Follow._expression_in_primary_expression1508);
    					right=expression();
    					PopFollow();

    					DebugLocation(308, 27);
    					 list.Add(right); 

    					}
    					break;

    				default:
    					goto loop30;
    				}
    			}

    			loop30:
    				;

    			} finally { DebugExitSubRule(30); }

    			DebugLocation(309, 3);
    			Match(input,47,Follow._47_in_primary_expression1517); 
    			DebugLocation(310, 3);

    					Value = list.Count == 1 ? list[0]: Function.Coalesce(list.ToArray());
    					

    			}
    			break;

    		}
    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(313, 1);
    	} finally { DebugExitRule(GrammarFileName, "primary_expression"); }
    	return Value;

    }
    // $ANTLR end "primary_expression"


    protected virtual void Enter_internal_function() {}
    protected virtual void Leave_internal_function() {}

    // $ANTLR start "internal_function"
    // C:\\output\\HoldingRule.g:316:1: internal_function returns [Expression Value] : ( ( 'ABS' | 'Abs' | 'abs' ) '(' e1= math_expression ')' | ( 'DATETIME' | 'DateTime' | 'Datetime' | 'dateTime' | 'datetime' ) '(' ')' | ( 'DATE' | 'Date' | 'date' ) '(' ')' | ( 'FMVPRICE' | 'FmvPrice' | 'Fmvprice' | 'fmvPrice' | 'fmvprice' ) '(' e1= math_expression ',' e2= math_expression ')' | ( 'IF' | 'If' | 'if' ) '(' tst= expression ',' thn= expression ',' els= expression ')' | ( 'MAX' | 'Max' | 'max' ) '(' e1= math_expression ',' e2= math_expression ')' | ( 'MIN' | 'Min' | 'min' ) '(' e1= math_expression ',' e2= math_expression ')' | ( 'ROUND' | 'Round' | 'round' ) '(' e1= math_expression ( ',' e2= math_expression )? ')' | ( 'SUM' | 'Sum' | 'sum' ) '(' asset= expression ',' field= IDENTIFIER ')' | ( 'FOUND' | 'Found' | 'found' ) '(' ')' | ( 'DISTRIBUTED' | 'Distributed' | 'distributed' ) '(' ')' | ( 'TIME' | 'Time' | 'time' ) '(' ')' );
    [GrammarRule("internal_function")]
    private Expression internal_function()
    {

        Expression Value = default(Expression);

        IToken field=null;
        Expression e1 = default(Expression);
        Expression e2 = default(Expression);
        Expression tst = default(Expression);
        Expression thn = default(Expression);
        Expression els = default(Expression);
        Expression asset = default(Expression);

    	try { DebugEnterRule(GrammarFileName, "internal_function");
    	DebugLocation(316, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:318:2: ( ( 'ABS' | 'Abs' | 'abs' ) '(' e1= math_expression ')' | ( 'DATETIME' | 'DateTime' | 'Datetime' | 'dateTime' | 'datetime' ) '(' ')' | ( 'DATE' | 'Date' | 'date' ) '(' ')' | ( 'FMVPRICE' | 'FmvPrice' | 'Fmvprice' | 'fmvPrice' | 'fmvprice' ) '(' e1= math_expression ',' e2= math_expression ')' | ( 'IF' | 'If' | 'if' ) '(' tst= expression ',' thn= expression ',' els= expression ')' | ( 'MAX' | 'Max' | 'max' ) '(' e1= math_expression ',' e2= math_expression ')' | ( 'MIN' | 'Min' | 'min' ) '(' e1= math_expression ',' e2= math_expression ')' | ( 'ROUND' | 'Round' | 'round' ) '(' e1= math_expression ( ',' e2= math_expression )? ')' | ( 'SUM' | 'Sum' | 'sum' ) '(' asset= expression ',' field= IDENTIFIER ')' | ( 'FOUND' | 'Found' | 'found' ) '(' ')' | ( 'DISTRIBUTED' | 'Distributed' | 'distributed' ) '(' ')' | ( 'TIME' | 'Time' | 'time' ) '(' ')' )
    		int alt33=12;
    		try { DebugEnterDecision(33, decisionCanBacktrack[33]);
    		switch (input.LA(1))
    		{
    		case 121:
    		case 122:
    		case 123:
    			{
    			alt33=1;
    			}
    			break;
    		case 60:
    		case 61:
    		case 62:
    		case 124:
    		case 125:
    			{
    			alt33=2;
    			}
    			break;
    		case 126:
    		case 127:
    		case 128:
    			{
    			alt33=3;
    			}
    			break;
    		case 129:
    		case 130:
    		case 131:
    		case 132:
    		case 133:
    			{
    			alt33=4;
    			}
    			break;
    		case 67:
    		case 68:
    		case 69:
    			{
    			alt33=5;
    			}
    			break;
    		case 63:
    		case 64:
    		case 65:
    			{
    			alt33=6;
    			}
    			break;
    		case 134:
    		case 135:
    		case 136:
    			{
    			alt33=7;
    			}
    			break;
    		case 137:
    		case 138:
    		case 139:
    			{
    			alt33=8;
    			}
    			break;
    		case 140:
    		case 141:
    		case 142:
    			{
    			alt33=9;
    			}
    			break;
    		case 143:
    		case 144:
    		case 145:
    			{
    			alt33=10;
    			}
    			break;
    		case 146:
    		case 147:
    		case 148:
    			{
    			alt33=11;
    			}
    			break;
    		case 149:
    		case 150:
    		case 151:
    			{
    			alt33=12;
    			}
    			break;
    		default:
    			{
    				NoViableAltException nvae = new NoViableAltException("", 33, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}

    		} finally { DebugExitDecision(33); }
    		switch (alt33)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:318:4: ( 'ABS' | 'Abs' | 'abs' ) '(' e1= math_expression ')'
    			{
    			DebugLocation(318, 4);
    			if ((input.LA(1)>=121 && input.LA(1)<=123))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(318, 24);
    			Match(input,46,Follow._46_in_internal_function1546); 
    			DebugLocation(318, 30);
    			PushFollow(Follow._math_expression_in_internal_function1550);
    			e1=math_expression();
    			PopFollow();

    			DebugLocation(318, 47);
    			Match(input,47,Follow._47_in_internal_function1552); 
    			DebugLocation(319, 3);
    			 Value = Function.Abs(e1); 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:320:4: ( 'DATETIME' | 'DateTime' | 'Datetime' | 'dateTime' | 'datetime' ) '(' ')'
    			{
    			DebugLocation(320, 4);
    			if ((input.LA(1)>=60 && input.LA(1)<=62)||(input.LA(1)>=124 && input.LA(1)<=125))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(320, 61);
    			Match(input,46,Follow._46_in_internal_function1573); 
    			DebugLocation(320, 65);
    			Match(input,47,Follow._47_in_internal_function1575); 
    			DebugLocation(321, 3);
    			 Value = Function.DateTime(); 

    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:322:4: ( 'DATE' | 'Date' | 'date' ) '(' ')'
    			{
    			DebugLocation(322, 4);
    			if ((input.LA(1)>=126 && input.LA(1)<=128))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(322, 27);
    			Match(input,46,Follow._46_in_internal_function1592); 
    			DebugLocation(322, 31);
    			Match(input,47,Follow._47_in_internal_function1594); 
    			DebugLocation(323, 3);
    			 Value = Function.Date(); 

    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:324:4: ( 'FMVPRICE' | 'FmvPrice' | 'Fmvprice' | 'fmvPrice' | 'fmvprice' ) '(' e1= math_expression ',' e2= math_expression ')'
    			{
    			DebugLocation(324, 4);
    			if ((input.LA(1)>=129 && input.LA(1)<=133))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(324, 61);
    			Match(input,46,Follow._46_in_internal_function1615); 
    			DebugLocation(324, 67);
    			PushFollow(Follow._math_expression_in_internal_function1619);
    			e1=math_expression();
    			PopFollow();

    			DebugLocation(324, 84);
    			Match(input,36,Follow._36_in_internal_function1621); 
    			DebugLocation(324, 90);
    			PushFollow(Follow._math_expression_in_internal_function1625);
    			e2=math_expression();
    			PopFollow();

    			DebugLocation(324, 107);
    			Match(input,47,Follow._47_in_internal_function1627); 
    			DebugLocation(325, 3);
    			 Value = Function.FmvPrice(e1, e2); 

    			}
    			break;
    		case 5:
    			DebugEnterAlt(5);
    			// C:\\output\\HoldingRule.g:326:4: ( 'IF' | 'If' | 'if' ) '(' tst= expression ',' thn= expression ',' els= expression ')'
    			{
    			DebugLocation(326, 4);
    			if ((input.LA(1)>=67 && input.LA(1)<=69))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(326, 21);
    			Match(input,46,Follow._46_in_internal_function1644); 
    			DebugLocation(326, 28);
    			PushFollow(Follow._expression_in_internal_function1648);
    			tst=expression();
    			PopFollow();

    			DebugLocation(326, 40);
    			Match(input,36,Follow._36_in_internal_function1650); 
    			DebugLocation(326, 47);
    			PushFollow(Follow._expression_in_internal_function1654);
    			thn=expression();
    			PopFollow();

    			DebugLocation(326, 59);
    			Match(input,36,Follow._36_in_internal_function1656); 
    			DebugLocation(326, 66);
    			PushFollow(Follow._expression_in_internal_function1660);
    			els=expression();
    			PopFollow();

    			DebugLocation(326, 78);
    			Match(input,47,Follow._47_in_internal_function1662); 
    			DebugLocation(327, 3);
    			 Value = Function.If(tst, thn, els); 

    			}
    			break;
    		case 6:
    			DebugEnterAlt(6);
    			// C:\\output\\HoldingRule.g:328:4: ( 'MAX' | 'Max' | 'max' ) '(' e1= math_expression ',' e2= math_expression ')'
    			{
    			DebugLocation(328, 4);
    			if ((input.LA(1)>=63 && input.LA(1)<=65))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(328, 24);
    			Match(input,46,Follow._46_in_internal_function1679); 
    			DebugLocation(328, 30);
    			PushFollow(Follow._math_expression_in_internal_function1683);
    			e1=math_expression();
    			PopFollow();

    			DebugLocation(328, 47);
    			Match(input,36,Follow._36_in_internal_function1685); 
    			DebugLocation(328, 53);
    			PushFollow(Follow._math_expression_in_internal_function1689);
    			e2=math_expression();
    			PopFollow();

    			DebugLocation(328, 70);
    			Match(input,47,Follow._47_in_internal_function1691); 
    			DebugLocation(329, 3);
    			 Value = Function.Max(e1, e2); 

    			}
    			break;
    		case 7:
    			DebugEnterAlt(7);
    			// C:\\output\\HoldingRule.g:330:4: ( 'MIN' | 'Min' | 'min' ) '(' e1= math_expression ',' e2= math_expression ')'
    			{
    			DebugLocation(330, 4);
    			if ((input.LA(1)>=134 && input.LA(1)<=136))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(330, 24);
    			Match(input,46,Follow._46_in_internal_function1708); 
    			DebugLocation(330, 30);
    			PushFollow(Follow._math_expression_in_internal_function1712);
    			e1=math_expression();
    			PopFollow();

    			DebugLocation(330, 47);
    			Match(input,36,Follow._36_in_internal_function1714); 
    			DebugLocation(330, 53);
    			PushFollow(Follow._math_expression_in_internal_function1718);
    			e2=math_expression();
    			PopFollow();

    			DebugLocation(330, 70);
    			Match(input,47,Follow._47_in_internal_function1720); 
    			DebugLocation(331, 3);
    			 Value = Function.Min(e1, e2); 

    			}
    			break;
    		case 8:
    			DebugEnterAlt(8);
    			// C:\\output\\HoldingRule.g:332:4: ( 'ROUND' | 'Round' | 'round' ) '(' e1= math_expression ( ',' e2= math_expression )? ')'
    			{
    			DebugLocation(332, 4);
    			if ((input.LA(1)>=137 && input.LA(1)<=139))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(332, 30);
    			Match(input,46,Follow._46_in_internal_function1737); 
    			DebugLocation(332, 36);
    			PushFollow(Follow._math_expression_in_internal_function1741);
    			e1=math_expression();
    			PopFollow();

    			DebugLocation(332, 53);
    			// C:\\output\\HoldingRule.g:332:53: ( ',' e2= math_expression )?
    			int alt32=2;
    			try { DebugEnterSubRule(32);
    			try { DebugEnterDecision(32, decisionCanBacktrack[32]);
    			int LA32_0 = input.LA(1);

    			if ((LA32_0==36))
    			{
    				alt32=1;
    			}
    			} finally { DebugExitDecision(32); }
    			switch (alt32)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:332:54: ',' e2= math_expression
    				{
    				DebugLocation(332, 54);
    				Match(input,36,Follow._36_in_internal_function1744); 
    				DebugLocation(332, 60);
    				PushFollow(Follow._math_expression_in_internal_function1748);
    				e2=math_expression();
    				PopFollow();


    				}
    				break;

    			}
    			} finally { DebugExitSubRule(32); }

    			DebugLocation(332, 79);
    			Match(input,47,Follow._47_in_internal_function1752); 
    			DebugLocation(333, 3);
    			 Value = Function.Round(e1, e2); 

    			}
    			break;
    		case 9:
    			DebugEnterAlt(9);
    			// C:\\output\\HoldingRule.g:334:4: ( 'SUM' | 'Sum' | 'sum' ) '(' asset= expression ',' field= IDENTIFIER ')'
    			{
    			DebugLocation(334, 4);
    			if ((input.LA(1)>=140 && input.LA(1)<=142))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(334, 24);
    			Match(input,46,Follow._46_in_internal_function1769); 
    			DebugLocation(334, 33);
    			PushFollow(Follow._expression_in_internal_function1773);
    			asset=expression();
    			PopFollow();

    			DebugLocation(334, 45);
    			Match(input,36,Follow._36_in_internal_function1775); 
    			DebugLocation(334, 54);
    			field=(IToken)Match(input,IDENTIFIER,Follow._IDENTIFIER_in_internal_function1779); 
    			DebugLocation(334, 66);
    			Match(input,47,Follow._47_in_internal_function1781); 
    			DebugLocation(335, 3);
    			 Value = Function.Sum(asset, (field!=null?field.Text:null)); 

    			}
    			break;
    		case 10:
    			DebugEnterAlt(10);
    			// C:\\output\\HoldingRule.g:336:4: ( 'FOUND' | 'Found' | 'found' ) '(' ')'
    			{
    			DebugLocation(336, 4);
    			if ((input.LA(1)>=143 && input.LA(1)<=145))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(336, 30);
    			Match(input,46,Follow._46_in_internal_function1798); 
    			DebugLocation(336, 34);
    			Match(input,47,Follow._47_in_internal_function1800); 
    			DebugLocation(337, 3);
    			 Value = Function.Found(); 

    			}
    			break;
    		case 11:
    			DebugEnterAlt(11);
    			// C:\\output\\HoldingRule.g:338:4: ( 'DISTRIBUTED' | 'Distributed' | 'distributed' ) '(' ')'
    			{
    			DebugLocation(338, 4);
    			if ((input.LA(1)>=146 && input.LA(1)<=148))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(338, 48);
    			Match(input,46,Follow._46_in_internal_function1817); 
    			DebugLocation(338, 52);
    			Match(input,47,Follow._47_in_internal_function1819); 
    			DebugLocation(339, 3);
    			 Value = Function.Distributed(); 

    			}
    			break;
    		case 12:
    			DebugEnterAlt(12);
    			// C:\\output\\HoldingRule.g:340:4: ( 'TIME' | 'Time' | 'time' ) '(' ')'
    			{
    			DebugLocation(340, 4);
    			if ((input.LA(1)>=149 && input.LA(1)<=151))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(340, 27);
    			Match(input,46,Follow._46_in_internal_function1836); 
    			DebugLocation(340, 31);
    			Match(input,47,Follow._47_in_internal_function1838); 
    			DebugLocation(341, 3);
    			 Value = Function.Time(); 

    			}
    			break;

    		}
    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(342, 1);
    	} finally { DebugExitRule(GrammarFileName, "internal_function"); }
    	return Value;

    }
    // $ANTLR end "internal_function"


    protected virtual void Enter_constant() {}
    protected virtual void Leave_constant() {}

    // $ANTLR start "constant"
    // C:\\output\\HoldingRule.g:345:1: constant returns [Expression Value] : ( DEC | INT | STRING | ( 'NULL' | 'Null' | 'null' ) | ( 'TRUE' | 'True' | 'true' ) | ( 'FALSE' | 'False' | 'false' ) );
    [GrammarRule("constant")]
    private Expression constant()
    {

        Expression Value = default(Expression);

        IToken DEC30=null;
        IToken INT31=null;
        IToken STRING32=null;

    	try { DebugEnterRule(GrammarFileName, "constant");
    	DebugLocation(345, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:347:2: ( DEC | INT | STRING | ( 'NULL' | 'Null' | 'null' ) | ( 'TRUE' | 'True' | 'true' ) | ( 'FALSE' | 'False' | 'false' ) )
    		int alt34=6;
    		try { DebugEnterDecision(34, decisionCanBacktrack[34]);
    		switch (input.LA(1))
    		{
    		case DEC:
    			{
    			alt34=1;
    			}
    			break;
    		case INT:
    			{
    			alt34=2;
    			}
    			break;
    		case STRING:
    			{
    			alt34=3;
    			}
    			break;
    		case 152:
    		case 153:
    		case 154:
    			{
    			alt34=4;
    			}
    			break;
    		case 155:
    		case 156:
    		case 157:
    			{
    			alt34=5;
    			}
    			break;
    		case 158:
    		case 159:
    		case 160:
    			{
    			alt34=6;
    			}
    			break;
    		default:
    			{
    				NoViableAltException nvae = new NoViableAltException("", 34, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}

    		} finally { DebugExitDecision(34); }
    		switch (alt34)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:347:4: DEC
    			{
    			DebugLocation(347, 4);
    			DEC30=(IToken)Match(input,DEC,Follow._DEC_in_constant1859); 
    			DebugLocation(348, 3);
    			 Value = Expression.Value(ExpressionType.Decimal, (DEC30!=null?DEC30.Text:null)); 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:349:4: INT
    			{
    			DebugLocation(349, 4);
    			INT31=(IToken)Match(input,INT,Follow._INT_in_constant1868); 
    			DebugLocation(350, 3);
    			 Value = Expression.Value((INT31!=null?INT31.Text:null).Length > 9 ? ExpressionType.Long: ExpressionType.Integer, (INT31!=null?INT31.Text:null)); 

    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:351:4: STRING
    			{
    			DebugLocation(351, 4);
    			STRING32=(IToken)Match(input,STRING,Follow._STRING_in_constant1877); 
    			DebugLocation(351, 11);
    			  
    			DebugLocation(352, 3);
    			 Value = Expression.Value(ExpressionType.String, (STRING32!=null?STRING32.Text:null)); 

    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:353:4: ( 'NULL' | 'Null' | 'null' )
    			{
    			DebugLocation(353, 4);
    			if ((input.LA(1)>=152 && input.LA(1)<=154))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(354, 3);
    			 Value = Expression.Value(ExpressionType.Undefined, "null"); 

    			}
    			break;
    		case 5:
    			DebugEnterAlt(5);
    			// C:\\output\\HoldingRule.g:355:4: ( 'TRUE' | 'True' | 'true' )
    			{
    			DebugLocation(355, 4);
    			if ((input.LA(1)>=155 && input.LA(1)<=157))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(356, 3);
    			 Value = Expression.Value(ExpressionType.Boolean, "1"); 

    			}
    			break;
    		case 6:
    			DebugEnterAlt(6);
    			// C:\\output\\HoldingRule.g:357:4: ( 'FALSE' | 'False' | 'false' )
    			{
    			DebugLocation(357, 4);
    			if ((input.LA(1)>=158 && input.LA(1)<=160))
    			{
    				input.Consume();
    				state.errorRecovery=false;
    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				throw mse;
    			}

    			DebugLocation(358, 3);
    			 Value = Expression.Value(ExpressionType.Boolean, "0"); 

    			}
    			break;

    		}
    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(359, 1);
    	} finally { DebugExitRule(GrammarFileName, "constant"); }
    	return Value;

    }
    // $ANTLR end "constant"


    protected virtual void Enter_identifier() {}
    protected virtual void Leave_identifier() {}

    // $ANTLR start "identifier"
    // C:\\output\\HoldingRule.g:361:1: identifier returns [Identifier Value] : (i= TRANSACT_FIELD | i= IDENTIFIER );
    [GrammarRule("identifier")]
    private Identifier identifier()
    {

        Identifier Value = default(Identifier);

        IToken i=null;

    	try { DebugEnterRule(GrammarFileName, "identifier");
    	DebugLocation(361, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:363:2: (i= TRANSACT_FIELD | i= IDENTIFIER )
    		int alt35=2;
    		try { DebugEnterDecision(35, decisionCanBacktrack[35]);
    		int LA35_0 = input.LA(1);

    		if ((LA35_0==TRANSACT_FIELD))
    		{
    			alt35=1;
    		}
    		else if ((LA35_0==IDENTIFIER))
    		{
    			alt35=2;
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 35, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(35); }
    		switch (alt35)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:363:4: i= TRANSACT_FIELD
    			{
    			DebugLocation(363, 5);
    			i=(IToken)Match(input,TRANSACT_FIELD,Follow._TRANSACT_FIELD_in_identifier1946); 
    			DebugLocation(364, 3);
    			 Value = Context.Declaration.FindIdentifier((i!=null?i.Text:null)); 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:365:4: i= IDENTIFIER
    			{
    			DebugLocation(365, 5);
    			i=(IToken)Match(input,IDENTIFIER,Follow._IDENTIFIER_in_identifier1957); 
    			DebugLocation(366, 3);
    			 Value = Context.Declaration.FindIdentifier((i!=null?i.Text:null)); 

    			}
    			break;

    		}
    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(367, 1);
    	} finally { DebugExitRule(GrammarFileName, "identifier"); }
    	return Value;

    }
    // $ANTLR end "identifier"


    protected virtual void Enter_number() {}
    protected virtual void Leave_number() {}

    // $ANTLR start "number"
    // C:\\output\\HoldingRule.g:373:1: number : ( DEC | INT );
    [GrammarRule("number")]
    private void number()
    {

    	try { DebugEnterRule(GrammarFileName, "number");
    	DebugLocation(373, 1);
    	try
    	{
    		// C:\\output\\HoldingRule.g:374:2: ( DEC | INT )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:
    		{
    		DebugLocation(374, 2);
    		if (input.LA(1)==INT||input.LA(1)==DEC)
    		{
    			input.Consume();
    			state.errorRecovery=false;
    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			throw mse;
    		}


    		}

    	}
    	catch (RecognitionException re)
    	{
    		ReportError(re);
    		Recover(input,re);
    	}
    	finally
    	{
        }
     	DebugLocation(376, 1);
    	} finally { DebugExitRule(GrammarFileName, "number"); }
    	return;

    }
    // $ANTLR end "number"

	#region DFA
	DFA2 dfa2;
	DFA10 dfa10;
	DFA15 dfa15;

	protected override void InitDFAs()
	{
		base.InitDFAs();
		dfa2 = new DFA2( this );
		dfa10 = new DFA10( this );
		dfa15 = new DFA15( this );
	}

	private class DFA2 : DFA
	{
		private const string DFA2_eotS =
			"\x0c\uffff";
		private const string DFA2_eofS =
			"\x0c\uffff";
		private const string DFA2_minS =
			"\x01\x04\x03\uffff\x01\x07\x07\uffff";
		private const string DFA2_maxS =
			"\x01\x73\x03\uffff\x01\x42\x07\uffff";
		private const string DFA2_acceptS =
			"\x01\uffff\x01\x01\x01\x02\x01\x03\x01\uffff\x01\x04\x01\x05\x01\x07"+
			"\x01\x08\x01\x09\x01\x0a\x01\x06";
		private const string DFA2_specialS =
			"\x0c\uffff}>";
		private static readonly string[] DFA2_transitionS =
			{
				"\x01\x04\x0b\uffff\x01\x03\x06\uffff\x01\x01\x03\x0a\x01\x09\x01\uffff"+
				"\x04\x06\x03\x02\x0a\uffff\x01\x03\x14\uffff\x03\x07\x03\uffff\x28\x05"+
				"\x03\x08",
				"",
				"",
				"",
				"\x02\x03\x39\uffff\x01\x0b",
				"",
				"",
				"",
				"",
				"",
				"",
				""
			};

		private static readonly short[] DFA2_eot = DFA.UnpackEncodedString(DFA2_eotS);
		private static readonly short[] DFA2_eof = DFA.UnpackEncodedString(DFA2_eofS);
		private static readonly char[] DFA2_min = DFA.UnpackEncodedStringToUnsignedChars(DFA2_minS);
		private static readonly char[] DFA2_max = DFA.UnpackEncodedStringToUnsignedChars(DFA2_maxS);
		private static readonly short[] DFA2_accept = DFA.UnpackEncodedString(DFA2_acceptS);
		private static readonly short[] DFA2_special = DFA.UnpackEncodedString(DFA2_specialS);
		private static readonly short[][] DFA2_transition;

		static DFA2()
		{
			int numStates = DFA2_transitionS.Length;
			DFA2_transition = new short[numStates][];
			for ( int i=0; i < numStates; i++ )
			{
				DFA2_transition[i] = DFA.UnpackEncodedString(DFA2_transitionS[i]);
			}
		}

		public DFA2( BaseRecognizer recognizer )
		{
			this.recognizer = recognizer;
			this.decisionNumber = 2;
			this.eot = DFA2_eot;
			this.eof = DFA2_eof;
			this.min = DFA2_min;
			this.max = DFA2_max;
			this.accept = DFA2_accept;
			this.special = DFA2_special;
			this.transition = DFA2_transition;
		}

		public override string Description { get { return "52:1: statement returns [Statement Value] : ( empty_stmt | declaration_stmt | assign_stmt | call_stmt | goto_stmt | label_stmt | if_stmt | distribute_stmt | block_stmt | return_stmt );"; } }

		public override void Error(NoViableAltException nvae)
		{
			DebugRecognitionException(nvae);
		}
	}

	private class DFA10 : DFA
	{
		private const string DFA10_eotS =
			"\x18\uffff";
		private const string DFA10_eofS =
			"\x01\x02\x17\uffff";
		private const string DFA10_minS =
			"\x01\x04\x17\uffff";
		private const string DFA10_maxS =
			"\x01\x73\x17\uffff";
		private const string DFA10_acceptS =
			"\x01\uffff\x01\x01\x01\x02\x15\uffff";
		private const string DFA10_specialS =
			"\x18\uffff}>";
		private static readonly string[] DFA10_transitionS =
			{
				"\x01\x02\x0b\uffff\x01\x02\x06\uffff\x0d\x02\x0a\uffff\x01\x02\x14"+
				"\uffff\x03\x02\x03\x01\x2b\x02",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				""
			};

		private static readonly short[] DFA10_eot = DFA.UnpackEncodedString(DFA10_eotS);
		private static readonly short[] DFA10_eof = DFA.UnpackEncodedString(DFA10_eofS);
		private static readonly char[] DFA10_min = DFA.UnpackEncodedStringToUnsignedChars(DFA10_minS);
		private static readonly char[] DFA10_max = DFA.UnpackEncodedStringToUnsignedChars(DFA10_maxS);
		private static readonly short[] DFA10_accept = DFA.UnpackEncodedString(DFA10_acceptS);
		private static readonly short[] DFA10_special = DFA.UnpackEncodedString(DFA10_specialS);
		private static readonly short[][] DFA10_transition;

		static DFA10()
		{
			int numStates = DFA10_transitionS.Length;
			DFA10_transition = new short[numStates][];
			for ( int i=0; i < numStates; i++ )
			{
				DFA10_transition[i] = DFA.UnpackEncodedString(DFA10_transitionS[i]);
			}
		}

		public DFA10( BaseRecognizer recognizer )
		{
			this.recognizer = recognizer;
			this.decisionNumber = 10;
			this.eot = DFA10_eot;
			this.eof = DFA10_eof;
			this.min = DFA10_min;
			this.max = DFA10_max;
			this.accept = DFA10_accept;
			this.special = DFA10_special;
			this.transition = DFA10_transition;
		}

		public override string Description { get { return "159:3: ( options {k=1; } : ( 'ELSE' | 'Else' | 'else' ) elseStmt= statement )?"; } }

		public override void Error(NoViableAltException nvae)
		{
			DebugRecognitionException(nvae);
		}
	}

	private class DFA15 : DFA
	{
		private const string DFA15_eotS =
			"\x0e\uffff";
		private const string DFA15_eofS =
			"\x0e\uffff";
		private const string DFA15_minS =
			"\x01\x49\x02\uffff\x01\x04\x08\uffff\x01\x04\x01\uffff";
		private const string DFA15_maxS =
			"\x01\x70\x02\uffff\x01\u00a0\x08\uffff\x01\u00a0\x01\uffff";
		private const string DFA15_acceptS =
			"\x01\uffff\x01\x01\x01\x02\x01\uffff\x01\x05\x01\x06\x01\x07\x01\x08"+
			"\x01\x09\x01\x0a\x01\x0b\x01\x03\x01\uffff\x01\x04";
		private const string DFA15_specialS =
			"\x0e\uffff}>";
		private static readonly string[] DFA15_transitionS =
			{
				"\x03\x01\x03\x02\x04\x03\x04\x04\x06\x05\x05\x06\x04\x07\x04\x08\x03"+
				"\x09\x04\x0a",
				"",
				"",
				"\x03\x0b\x05\uffff\x01\x0b\x02\uffff\x02\x0b\x06\uffff\x01\x0d\x16"+
				"\uffff\x01\x0c\x0d\uffff\x06\x0b\x01\uffff\x03\x0b\x30\uffff\x01\x0b"+
				"\x02\uffff\x28\x0b",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"\x03\x0b\x05\uffff\x01\x0b\x02\uffff\x02\x0b\x1d\uffff\x01\x0b\x01"+
				"\x0d\x0c\uffff\x06\x0b\x01\uffff\x03\x0b\x30\uffff\x01\x0b\x02\uffff"+
				"\x28\x0b",
				""
			};

		private static readonly short[] DFA15_eot = DFA.UnpackEncodedString(DFA15_eotS);
		private static readonly short[] DFA15_eof = DFA.UnpackEncodedString(DFA15_eofS);
		private static readonly char[] DFA15_min = DFA.UnpackEncodedStringToUnsignedChars(DFA15_minS);
		private static readonly char[] DFA15_max = DFA.UnpackEncodedStringToUnsignedChars(DFA15_maxS);
		private static readonly short[] DFA15_accept = DFA.UnpackEncodedString(DFA15_acceptS);
		private static readonly short[] DFA15_special = DFA.UnpackEncodedString(DFA15_specialS);
		private static readonly short[][] DFA15_transition;

		static DFA15()
		{
			int numStates = DFA15_transitionS.Length;
			DFA15_transition = new short[numStates][];
			for ( int i=0; i < numStates; i++ )
			{
				DFA15_transition[i] = DFA.UnpackEncodedString(DFA15_transitionS[i]);
			}
		}

		public DFA15( BaseRecognizer recognizer )
		{
			this.recognizer = recognizer;
			this.decisionNumber = 15;
			this.eot = DFA15_eot;
			this.eof = DFA15_eof;
			this.min = DFA15_min;
			this.max = DFA15_max;
			this.accept = DFA15_accept;
			this.special = DFA15_special;
			this.transition = DFA15_transition;
		}

		public override string Description { get { return "164:1: call_stmt returns [Statement Value] : ( ( 'PRINT' | 'Print' | 'print' ) expression ';' | ( 'ERROR' | 'Error' | 'error' ) ( '(' ')' | expression ) ';' | ( 'CREATELOT' | 'CreateLot' | 'createLot' | 'createlot' ) expression ';' | ( 'CREATELOT' | 'CreateLot' | 'createLot' | 'createlot' ) zero_expression ';' | ( 'CLOSELOT' | 'CloseLot' | 'closeLot' | 'closelot' ) zero_expression ';' | ( 'SPLITCOSTBASIS' | 'SplitCostBasis' | 'SplitCostbasis' | 'splitCostBasis' | 'splitCostbasis' | 'splitcostbasis' ) zero_expression ';' | ( 'FINDLAST' | 'FindLast' | 'Findlast' | 'findLast' | 'findlast' ) expression ';' | ( 'UPDATECASH' | 'UpdateCash' | 'updateCash' | 'updatecash' ) (cash= expression )? ';' | ( 'INLINEQUERY' | 'InlineQuery' | 'inlineQuery' | 'inlinequery' ) ( '(' query= STRING ')' | query= STRING ) ';' | ( 'RULE' | 'Rule' | 'rule' ) ( '(' rule= INT ',' count= expression ',' currency= expression ',' asset= expression ')' | rule= INT ',' count= expression ',' currency= expression ',' asset= expression ) ';' | ( 'MOVEASSETS' | 'MoveAssets' | 'moveAssets' | 'moveassets' ) source= expression ';' );"; } }

		public override void Error(NoViableAltException nvae)
		{
			DebugRecognitionException(nvae);
		}
	}


	#endregion DFA

	#region Follow sets
	private static class Follow
	{
		public static readonly BitSet _statements_in_program50 = new BitSet(new ulong[]{0x0000000000000000UL});
		public static readonly BitSet _EOF_in_program52 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _statement_in_statements74 = new BitSet(new ulong[]{0x0000400FEF810012UL,0x000FFFFFFFFFFE38UL});
		public static readonly BitSet _empty_stmt_in_statement97 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _declaration_stmt_in_statement106 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _assign_stmt_in_statement115 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _call_stmt_in_statement124 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _goto_stmt_in_statement133 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _label_stmt_in_statement142 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _if_stmt_in_statement151 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _distribute_stmt_in_statement160 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _block_stmt_in_statement169 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _return_stmt_in_statement178 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _23_in_empty_stmt198 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_return_stmt214 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_return_stmt222 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _27_in_block_stmt242 = new BitSet(new ulong[]{0x0000400FEF810010UL,0x000FFFFFFFFFFE38UL});
		public static readonly BitSet _statements_in_block_stmt244 = new BitSet(new ulong[]{0x0000000010000000UL});
		public static readonly BitSet _28_in_block_stmt246 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_goto_stmt266 = new BitSet(new ulong[]{0x0000000000000010UL});
		public static readonly BitSet _IDENTIFIER_in_goto_stmt278 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_goto_stmt280 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_declaration_stmt306 = new BitSet(new ulong[]{0x0000000000000010UL});
		public static readonly BitSet _declaration_in_declaration_stmt316 = new BitSet(new ulong[]{0x0000001000800000UL});
		public static readonly BitSet _36_in_declaration_stmt324 = new BitSet(new ulong[]{0x0000000000000010UL});
		public static readonly BitSet _declaration_in_declaration_stmt328 = new BitSet(new ulong[]{0x0000001000800000UL});
		public static readonly BitSet _23_in_declaration_stmt335 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _IDENTIFIER_in_declaration357 = new BitSet(new ulong[]{0x7FFF3FE000000082UL});
		public static readonly BitSet _type_in_declaration361 = new BitSet(new ulong[]{0x0000000000000082UL});
		public static readonly BitSet _EQUAL_in_declaration365 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_declaration369 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_type392 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_type407 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_type422 = new BitSet(new ulong[]{0x0000400000000002UL});
		public static readonly BitSet _46_in_type432 = new BitSet(new ulong[]{0x8000000000000020UL,0x0000000000000003UL});
		public static readonly BitSet _length_in_type434 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_type436 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_type448 = new BitSet(new ulong[]{0x0000400000000002UL});
		public static readonly BitSet _46_in_type458 = new BitSet(new ulong[]{0x8000000000000020UL,0x0000000000000003UL});
		public static readonly BitSet _length_in_type460 = new BitSet(new ulong[]{0x0000801000000000UL});
		public static readonly BitSet _36_in_type463 = new BitSet(new ulong[]{0x0000000000000020UL});
		public static readonly BitSet _precision_in_type465 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_type469 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_type481 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_type496 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_type511 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_type526 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_length0 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _INT_in_precision569 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _IDENTIFIER_in_label_stmt585 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000004UL});
		public static readonly BitSet _66_in_label_stmt587 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_if_stmt607 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_if_stmt615 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_if_stmt619 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_if_stmt621 = new BitSet(new ulong[]{0x0000400FEF810010UL,0x000FFFFFFFFFFFF8UL});
		public static readonly BitSet _statement_in_if_stmt625 = new BitSet(new ulong[]{0x0000000000000002UL,0x00000000000001C0UL});
		public static readonly BitSet _set_in_if_stmt638 = new BitSet(new ulong[]{0x0000400FEF810010UL,0x000FFFFFFFFFFE38UL});
		public static readonly BitSet _statement_in_if_stmt648 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt671 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt679 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt681 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt690 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _46_in_call_stmt699 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_call_stmt701 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _expression_in_call_stmt705 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt708 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt717 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt727 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt729 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt738 = new BitSet(new ulong[]{0x0000400000800000UL});
		public static readonly BitSet _zero_expression_in_call_stmt748 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt750 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt759 = new BitSet(new ulong[]{0x0000400000800000UL});
		public static readonly BitSet _zero_expression_in_call_stmt769 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt771 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt780 = new BitSet(new ulong[]{0x0000400000800000UL});
		public static readonly BitSet _zero_expression_in_call_stmt794 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt796 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt805 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt817 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt819 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt828 = new BitSet(new ulong[]{0xF000400000819070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt840 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt843 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt852 = new BitSet(new ulong[]{0x0000400000000040UL});
		public static readonly BitSet _46_in_call_stmt863 = new BitSet(new ulong[]{0x0000000000000040UL});
		public static readonly BitSet _STRING_in_call_stmt867 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_call_stmt869 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _STRING_in_call_stmt875 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt878 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt887 = new BitSet(new ulong[]{0x0000400000000020UL});
		public static readonly BitSet _46_in_call_stmt896 = new BitSet(new ulong[]{0x0000000000000020UL});
		public static readonly BitSet _INT_in_call_stmt900 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_call_stmt902 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt906 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_call_stmt908 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt912 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_call_stmt914 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt918 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_call_stmt920 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _INT_in_call_stmt926 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_call_stmt928 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt932 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_call_stmt934 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt938 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_call_stmt940 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt944 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt947 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_call_stmt956 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_call_stmt968 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_call_stmt970 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_distribute_stmt990 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_distribute_stmt1000 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_distribute_stmt1004 = new BitSet(new ulong[]{0x0000801000000000UL});
		public static readonly BitSet _36_in_distribute_stmt1007 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE5000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _116_in_distribute_stmt1010 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _expression_in_distribute_stmt1016 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_distribute_stmt1021 = new BitSet(new ulong[]{0x0000400FEF810010UL,0x000FFFFFFFFFFE38UL});
		public static readonly BitSet _statement_in_distribute_stmt1023 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _46_in_zero_expression1039 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_zero_expression1041 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _left_value_in_assign_stmt1059 = new BitSet(new ulong[]{0x0000000000000180UL});
		public static readonly BitSet _set_in_assign_stmt1063 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_assign_stmt1071 = new BitSet(new ulong[]{0x0000000000800000UL});
		public static readonly BitSet _23_in_assign_stmt1073 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _identifier_in_left_value1095 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _46_in_left_value1105 = new BitSet(new ulong[]{0x0000000000010010UL});
		public static readonly BitSet _identifier_in_left_value1109 = new BitSet(new ulong[]{0x0000801000000000UL});
		public static readonly BitSet _36_in_left_value1120 = new BitSet(new ulong[]{0x0000000000010010UL});
		public static readonly BitSet _identifier_in_left_value1124 = new BitSet(new ulong[]{0x0000801000000000UL});
		public static readonly BitSet _47_in_left_value1134 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _and_expression_in_expression1152 = new BitSet(new ulong[]{0x0000000000000202UL});
		public static readonly BitSet _OR_in_expression1161 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _and_expression_in_expression1165 = new BitSet(new ulong[]{0x0000000000000202UL});
		public static readonly BitSet _test_expression_in_and_expression1190 = new BitSet(new ulong[]{0x0000000000000402UL});
		public static readonly BitSet _AND_in_and_expression1198 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _test_expression_in_and_expression1202 = new BitSet(new ulong[]{0x0000000000000402UL});
		public static readonly BitSet _math_expression_in_test_expression1227 = new BitSet(new ulong[]{0x0000000000007882UL});
		public static readonly BitSet _EQUAL_in_test_expression1238 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _COMPARE_in_test_expression1244 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _NOT_in_test_expression1250 = new BitSet(new ulong[]{0x0000000000006000UL});
		public static readonly BitSet _set_in_test_expression1255 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_test_expression1264 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _mult_expression_in_math_expression1292 = new BitSet(new ulong[]{0x0000000000000002UL,0x0060000000000000UL});
		public static readonly BitSet _set_in_math_expression1304 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _mult_expression_in_math_expression1312 = new BitSet(new ulong[]{0x0000000000000002UL,0x0060000000000000UL});
		public static readonly BitSet _unary_expression_in_mult_expression1347 = new BitSet(new ulong[]{0x0000000000000002UL,0x0190000000000000UL});
		public static readonly BitSet _set_in_mult_expression1359 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _unary_expression_in_mult_expression1369 = new BitSet(new ulong[]{0x0000000000000002UL,0x0190000000000000UL});
		public static readonly BitSet _primary_expression_in_unary_expression1409 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _118_in_unary_expression1419 = new BitSet(new ulong[]{0xF000400000018070UL,0xFE0000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _NOT_in_unary_expression1426 = new BitSet(new ulong[]{0xF000400000018070UL,0xFE0000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _primary_expression_in_unary_expression1436 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _identifier_in_primary_expression1464 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _constant_in_primary_expression1473 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _internal_function_in_primary_expression1482 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _46_in_primary_expression1491 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_primary_expression1495 = new BitSet(new ulong[]{0x0000801000000000UL});
		public static readonly BitSet _36_in_primary_expression1504 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_primary_expression1508 = new BitSet(new ulong[]{0x0000801000000000UL});
		public static readonly BitSet _47_in_primary_expression1517 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1538 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1546 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_internal_function1550 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1552 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1561 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1573 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1575 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1584 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1592 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1594 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1603 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1615 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_internal_function1619 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_internal_function1621 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_internal_function1625 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1627 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1636 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1644 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_internal_function1648 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_internal_function1650 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_internal_function1654 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_internal_function1656 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_internal_function1660 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1662 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1671 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1679 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_internal_function1683 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_internal_function1685 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_internal_function1689 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1691 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1700 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1708 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_internal_function1712 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_internal_function1714 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_internal_function1718 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1720 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1729 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1737 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_internal_function1741 = new BitSet(new ulong[]{0x0000801000000000UL});
		public static readonly BitSet _36_in_internal_function1744 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _math_expression_in_internal_function1748 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1752 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1761 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1769 = new BitSet(new ulong[]{0xF000400000019070UL,0xFE4000000000003BUL,0x00000001FFFFFFFFUL});
		public static readonly BitSet _expression_in_internal_function1773 = new BitSet(new ulong[]{0x0000001000000000UL});
		public static readonly BitSet _36_in_internal_function1775 = new BitSet(new ulong[]{0x0000000000000010UL});
		public static readonly BitSet _IDENTIFIER_in_internal_function1779 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1781 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1790 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1798 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1800 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1809 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1817 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1819 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_internal_function1828 = new BitSet(new ulong[]{0x0000400000000000UL});
		public static readonly BitSet _46_in_internal_function1836 = new BitSet(new ulong[]{0x0000800000000000UL});
		public static readonly BitSet _47_in_internal_function1838 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _DEC_in_constant1859 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _INT_in_constant1868 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _STRING_in_constant1877 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_constant1888 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_constant1903 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_constant1918 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _TRANSACT_FIELD_in_identifier1946 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _IDENTIFIER_in_identifier1957 = new BitSet(new ulong[]{0x0000000000000002UL});
		public static readonly BitSet _set_in_number0 = new BitSet(new ulong[]{0x0000000000000002UL});

	}
	#endregion Follow sets
}
}