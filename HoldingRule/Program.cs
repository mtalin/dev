﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Holding
{
	public class Program
	{
		private readonly HashSet<string> _assigned = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
		private readonly List<SyntaxError> _errors = new List<SyntaxError>();
		private string _text;

		public Program()
		{
			Declaration = new Declaration();
		}

		public Program(Declaration declaration, StatementsList statements)
		{
			Declaration = declaration;
			Statements = statements;
		}

		public Declaration Declaration { get; set; }
		public StatementsList Statements { get; set; }

		public bool HasValue(string name)
		{
			return _assigned.Contains(name.ToUpperInvariant());
		}

		public void SetValue(string name, bool assigned)
		{
			if (assigned)
			{
				Declaration.UseVariable(name);
				_assigned.Add(name.ToUpperInvariant());
			}
			else
			{
				_assigned.Remove(name.ToUpperInvariant());
			}
		}

		public string GetVarText(string name)
		{
			return Declaration.FindIdentifier(name, true).GetText(this);
		}

		public string GetVarText(string name, bool use)
		{
			return Declaration.FindIdentifier(name, use).GetText(this);
		}

		public Identifier Declare(ExpressionType type, string name)
		{
			return Declaration.Declare(type, name);
		}

		public Identifier Temporary(ExpressionType type)
		{
			return Declaration.TemporaryIdentifier(type);
		}

		public HashSet<string> MarkValueSettings()
		{
			return new HashSet<string>(_assigned, StringComparer.CurrentCultureIgnoreCase);
		}

		public void AddValueSettings(HashSet<string> settings)
		{
			foreach (var item in settings)
			{
				_assigned.Add(item);
			}
		}

		public void RestoreValueSettings(HashSet<string> settings)
		{
			_assigned.Clear();
			AddValueSettings(settings);
		}

		public string Text => _text ?? (_text = GetText(0));

		public string GetText(int indent)
		{
			if (Statements == null)
				return null;
			if (!Declaration.Prepare(this))
				throw new ApplicationException("Cannot prepare Declaration statements");
			IList<Statement> list = Statements.List;
			bool prepared = true;
			int k = 0;
			do
			{
				for (int i = 0; i < list.Count; ++i)
				{
					if (list[i] != null && !list[i].Prepare(this))
						prepared = false;
				}
				if (k++ > list.Count)
					throw new ApplicationException("Cannot prepare statements");
			} while (!prepared);

			string nl	= IndentNewLineString(indent);
			string nl__ = IndentNewLineString(indent + 1);
			StringBuilder body = new StringBuilder();
			body.Append(NewLine).Append(NewLine);
			body.Append(Statements.GetText(this, indent))
				.Append(nl).Append("return 0;");

			string initialization = Declaration.GetInitialQuery(this, indent);
			// Error hadler
			if (HasValue("errorInfo"))
			{
				StringBuilder error = new StringBuilder();
				error.Append("if (${{[errorId]}} is null or ${{[errorId]}} = 0)\n" +
					"	set ${{[errorId]}} = 100;\n");
				if (HasValue("procedureName"))
					error.Append("set ${{[errorInfo]}} = ${{[procedureName]}} + ': ' + isnull(${{[errorInfo]}}, 'unspecified error') +");
				else
					error.Append("set ${{[errorInfo]}} = isnull(${{[errorInfo]}}, 'unspecified error') +");
				error.Append("\n	' (error code = ' + isnull(cast(${{[errorId]}} as varchar),'<null>') +");
				error.Append("\n	'; transaction ID = ' + isnull(cast(${{[t.ID]}} as varchar),'<null>') +");
				if (Declaration.IsDeclared("t.Type"))
					error.Append("\n	'; transaction type = ' + isnull(cast(${{[t.Type]}} as varchar),'<null>') +");
				if (Declaration.IsDeclared("t.Amount"))
					error.Append("\n	'; amount = ' + isnull(cast(${{[t.Amount]}} as varchar),'<null>') +");
				if (Declaration.IsDeclared("t.Asset"))
					error.Append("\n	'; asset ID = ' + isnull(cast(${{[t.Asset]}} as varchar),'<null>') +");
				if (Declaration.IsDeclared("t.UnitsNumber"))
					error.Append("\n	'; units number = ' + isnull(cast(${{[t.UnitsNumber]}} as varchar),'<null>') +");
				if (Declaration.IsDeclared("t.DtTransact"))
					error.Append("\n	'; transact date = ' + isnull(convert(varchar,${{[t.DtTransact]}},120),'<null>') +");
				if (Declaration.IsDeclared("t.Foundation"))
					error.Append("\n	'; foundation ID = ' + isnull(cast(${{[t.Foundation]}} as varchar),'<null>') +");
				if (Declaration.IsDeclared("t.FoundationAccount"))
					error.Append("\n	'; account ID = ' + isnull(cast(${{[t.FoundationAccount]}} as varchar),'<null>') +");
				error.Append(" ')';\n");
				error.Append("return -1;");
				body.Append(NewLine).Append(NewLine)
					.Append("Error:").Append(NewLine)
					.Append(MapTemplate(indent, error.ToString()));
			}
			return Declaration.GetText(this, indent) + initialization + body.ToString();
		}

		public void AddError(SyntaxError error)
		{
			_errors.Add(error);
		}

		public bool HasError
		{
			get { return _errors.Count > 0; }
		}

		public string GetErrorMessage(string delimiter, string fileName, int lineOffset)
		{
			string[] messages = new string[_errors.Count];
			for (int i = 0; i < _errors.Count; ++i)
			{
				messages[i] = _errors[i].ComposeMessage(fileName, lineOffset);
			}
			return String.Join(delimiter, messages);
		}

		public string NewLine
		{
			get { return Environment.NewLine; }
		}

		public string IndentString(int indent)
		{
			return new String('\t', indent);
		}

		public string IndentNewLineString(int indent)
		{
			return Environment.NewLine + new String('\t', indent);
		}

		public string MapTemplate(int indent, string template)
		{
			return MapTemplate(indent, template, null);
		}

		private static readonly Regex _idRex = new Regex(@"\$\{\{\[(?:(?<ref>[a-zA-Z0-9$\.]+)|\?(?<tmp>[a-zA-Z0-9$\.]+)(?:\:(?<sql>[^\]]+))?)\]\}\}");

		/// <summary>
		/// Map template.
		///   Replace ${{[Name]}} with variable reference.
		///   Replace ${{[?Name:sql_type]}} with temporary var.
		/// </summary>
		/// <param name="indent"></param>
		/// <param name="template"></param>
		/// <param name="locals"></param>
		/// <returns></returns>
		public string MapTemplate(int indent, string template, Dictionary<string, string> locals)
		{
			string newLine = IndentNewLineString(indent);
			string indentOne = IndentString(1);
			Dictionary<string, Identifier> tmp = null;
			string result = _idRex.Replace(template, m =>
			{
				if (m.Groups["ref"].Success)
				{
					return locals != null && locals.ContainsKey(m.Groups["ref"].Value) ?
						locals[m.Groups["ref"].Value]:
						GetVarText(m.Groups["ref"].Value);
				}
				else if (m.Groups["tmp"].Success)
				{
					string s = m.Groups["tmp"].Value.ToUpperInvariant();
					if (tmp == null)
						tmp = new Dictionary<string, Identifier>();
					if (!tmp.TryGetValue(s, out Identifier t))
					{
						t = m.Groups["sql"].Success ?
							Temporary(ExpressionType.Parse(m.Groups["sql"].Value)):
							new Identifier(ExpressionType.Undefined, "$tmp$", Declaration.TemporaryName(s[0] != '$'));
						tmp.Add(s, t);
					}
					return t.GetText(this);
				}
				else
				{
					return m.Value;
				}
			});
			return IndentString(indent) + result.Replace("\t", indentOne).Replace("\r", "").Replace("\n", newLine);
		}
	}

	public class SyntaxError
	{
		public int Line { get; }
		public int Column { get; }
		public string Message { get; }

		public SyntaxError(int line, int column, string message)
		{
			Line = line + 1;
			Column = column + 1;
			Message = message;
		}

		public string ComposeMessage(string fileName, int lineOffset)
		{
			return $"{fileName}({lineOffset + Line},{Column}): {Message}";
		}
	}

	public class SyntaxErrorException: Exception
	{
		public int Line { get; }
		public int Column { get; }

		public SyntaxErrorException(int line, int column, string message)
			: base(message)
		{
			Line = line;
			Column = column;
		}

		public SyntaxErrorException(int line, int column, string message, Exception innertException)
			: base(message, innertException)
		{
			Line = line;
			Column = column;
		}
	}
}
