﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Functions
{
	class CoalesceFunction: Function
	{
		private readonly List<Expression> _list;

		public CoalesceFunction(params Expression[] values)
			: base(ExpressionType.Decimal, "coalesce", null, values)
		{
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			List<Expression> list = new List<Expression>();
			foreach (var item in values)
			{
				if (item != null)
					list.Add(item);
			}
			_list = list;
		}

		public override string GetText(Program context)
		{
			return GetText(context, false);
		}

		public string GetText(Program context, bool list)
		{
			if (_list.Count == 0)
				return list ? "(null)": "null";
			if (_list.Count == 1)
				return "(" + _list[0].GetText(context) + ")";
			if (_list.Count == 2 && !list)
				return "isnull(" + _list[0].GetText(context) + ", " + _list[1].GetText(context) + ")";

			StringBuilder sb = new StringBuilder(list ? "(": "coalesce(");
			for (int i = 0; i < _list.Count; ++i)
			{
				sb.Append(_list[i].GetText(context)).Append(", ");
			}
			sb.Length = sb.Length - 2;
			sb.Append(")");
			return sb.ToString();
		}
	}
}
