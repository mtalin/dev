﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Functions
{
	class AbsFunction: Function
	{
		private readonly Expression _value;

		public AbsFunction(Expression value)
			: base(ExpressionType.Decimal, "abs", null, new[] { value })
		{
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			_value = value;
		}

		public override string GetText(Program context)
		{
			return "abs(" + _value.GetText(context) + ")";
		}
	}
}
