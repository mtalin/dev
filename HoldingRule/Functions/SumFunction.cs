﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Functions
{
	class SumFunction: Function
	{
		private readonly Expression _asset;
		private readonly string _field;

		public SumFunction(Expression asset, string field)
			: base(ExpressionType.Decimal, "Sum", null, new[] { asset })
		{
			if (field == null)
				throw new ArgumentNullException(nameof(field));
			_field = field;
			_asset = asset;
		}

		public override string GetText(Program context)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("isnull((select sum(isnull(" + _field + ", 0)) from ")
				.Append(context.GetVarText("$Holding"))
				.Append(" where Asset = ")
				.Append(_asset == null ? context.GetVarText("t.Asset"): _asset.GetText(context))
				.Append(" and FoundationAccount = ").Append(context.GetVarText("t.FoundationAccount"))
				.Append(" and DtClosed is null), 0)");
			return sb.ToString();
		}
	}
}
