﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Functions
{
	class FoundFunction: Function
	{
		private readonly string _field;

		private static readonly string[] _variables = new[] { "x.Found" };

		public FoundFunction(string field)
			: base(ExpressionType.Boolean, "found", null, null, _variables)
		{
			_field = field;
		}

		public override string GetText(Program context)
		{
			return "(isnull(" + context.GetVarText(_field) + ", 0) = 1)";
		}
	}
}
