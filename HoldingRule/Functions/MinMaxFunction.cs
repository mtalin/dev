﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Functions
{
	class MinMaxFunction: Function
	{
		private readonly Expression _x;
		private readonly Expression _y;
		private readonly bool _isMin;

		public MinMaxFunction(bool isMin, Expression x, Expression y)
			: base(x.Type, isMin ? "Min": "Max", null, new[] { x, y })
		{
			_x = x;
			_y = y;
			_isMin = isMin;
		}

		public override string GetText(Program context)
		{
			string xTxt = _x.GetText(context);
			string yTxt = _y.GetText(context);
			return "case when " + xTxt + (_isMin ? " > ": " < ") + yTxt + " then " + yTxt + " else " + xTxt + " end";
		}
	}
}
