﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Functions
{
	class FmvPriceFunction: Function
	{
		private readonly Expression _asset;
		private readonly Expression _date;

		public FmvPriceFunction(Expression asset, Expression date)
			: base(ExpressionType.Money, "FmvPrice", null, new[] { asset, date }, null)
		{
			_asset = asset;
			_date = date;
		}

		public override string GetText(Program context)
		{
			return "dbo.get_AssetFmvPrice(" + _asset.GetText(context) + ", " + _date.GetText(context) + ")";
		}
	}
}
