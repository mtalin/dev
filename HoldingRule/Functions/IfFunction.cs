﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Functions
{
	class IfFunction: Function
	{
		private readonly Expression _testExpression;
		private readonly Expression _thenPart;
		private readonly Expression _elsePart;

		public IfFunction(Expression testExpression, Expression thenPart, Expression elsePart)
			: base(ExpressionType.Money, "FmvPrice", null, new[] { thenPart, elsePart, testExpression })
		{
			if (testExpression == null)
				throw new ArgumentNullException(nameof(testExpression));
			if (thenPart == null)
				throw new ArgumentNullException(nameof(thenPart));
			if (elsePart == null)
				throw new ArgumentNullException(nameof(elsePart));
			_testExpression = testExpression;
			_thenPart = thenPart;
			_elsePart = elsePart;
		}

		public override string GetText(Program context)
		{
			return "case when " + _testExpression.GetText(context) + " then " + _thenPart.GetText(context) + " else " + _elsePart.GetText(context) + " end";
		}
	}
}
