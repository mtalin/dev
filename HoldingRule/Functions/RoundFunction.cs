﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Functions
{
	class RoundFunction: Function
	{
		private readonly Expression _value;
		private readonly Expression _digits;

		public RoundFunction(Expression value)
			: base(ExpressionType.Decimal, "round", null, new[] { value })
		{
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			_value = value;
		}

		public RoundFunction(Expression value, Expression digits)
			: base(ExpressionType.Decimal, "round", null, new[] { value, digits })
		{
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			_value = value;
			_digits = digits;
		}

		public override string GetText(Program context)
		{
			return _digits == null ?
				"round(" + _value.GetText(context) + ", 2)":
				"round(" + _value.GetText(context) + ", " + _digits.GetText(context) + ")";
		}
	}
}
