﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Antlr.Runtime;
using System.Diagnostics;
using System.Globalization;

namespace Holding
{
	class RulesOptions: IEnumerable<string>
	{
		Dictionary<string, string> _opts;

		public RulesOptions()
		{
			_opts = new Dictionary<string, string>();
			SetDefaults();
		}

		public RulesOptions(Dictionary<string, string> other)
		{
			_opts = new Dictionary<string, string>(other.Count + 8);
			foreach (var item in other)
			{
				_opts.Add(item.Key.ToUpperInvariant(), item.Value);
			}
			SetDefaults();
		}

		public RulesOptions(RulesOptions other)
		{
			_opts = new Dictionary<string, string>(other._opts);
			SetDefaults();
		}

		private void SetDefaults()
		{
			Default("USD", "1059");
			Default("$holding", "dbo.Holdings");
			Default("$schema", "xhc");
			Default("$prefix", "z_h_");
			Default("$go", "[" + this["$schema"] + "].[" + this["$prefix"] + "Go]");
			Default("$registry", "dbo.Registries");
		}

		public string this[string key]
		{
			get { return _opts[key.ToUpperInvariant()]; }
			set { _opts[key.ToUpperInvariant()] = value; }
		}

		public void Add(string key, string value)
		{
			_opts.Add(key.ToUpperInvariant(), value);
		}

		public void Default(string key, string value)
		{
			key = key.ToUpperInvariant();
			if (!_opts.ContainsKey(key))
				_opts.Add(key, value);
		}

		public bool Exists(string key)
		{
			return _opts.ContainsKey(key.ToUpperInvariant());
		}

		#region IEnumerable Members
		public IEnumerator<string> GetEnumerator()
		{
			return _opts.Keys.GetEnumerator();
		}
	
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return _opts.Keys.GetEnumerator();
		}
		#endregion
	}

	class RulesCollection: IEnumerable<RuleInfo>
	{
		private RuleInfo[] _rules;
		public string CompiledRules { get; private set; }
		public string ErrorMessage { get; private set; }
		public string Startup { get; private set; }
		public string FileName { get; }
		public string BatchHeader { get; private set; }
		public string BatchFooter { get; private set; }

		private RulesOptions _vars;
		private HashSet<string> _usedRules = new HashSet<string>();

		private RulesCollection(string fileName, RulesOptions vars)
		{
			FileName = fileName;
			_vars = new RulesOptions(vars);
		}

		public bool HasErrors
		{
			get { return ErrorMessage != null; }
		}

		public RuleInfo Find(string name)
		{
			name = name.Replace(" ", "");
			return Array.Find(_rules, x => String.Compare(x.Name.Replace(" ", ""), name, StringComparison.OrdinalIgnoreCase) == 0);
		}

		private void SetRules(RuleInfo[] rules)
		{
			Array.Sort(rules, (x, y) => String.Compare(x.Name, y.Name, StringComparison.OrdinalIgnoreCase));
			_rules = rules;
		}

		public static RulesCollection Parse(string fileName, RulesOptions vars)
		{
			if (fileName == null || fileName.Length == 0)
				throw new ArgumentNullException(nameof(fileName));
			if (!File.Exists(fileName))
				throw new ArgumentException("file does not exists");

			RulesCollection rc = new RulesCollection(fileName, vars);

			StreamReader reader = File.OpenText(fileName);
			List<RuleInfo> rules = new List<RuleInfo>();
			StringBuilder ruleText = new StringBuilder();
			int lineNo = 0;
			string ruleName = null;
			string transacts = null;
			int lineOffset = 0;
			string line;
			while ((line = reader.ReadLine()) != null)
			{
				++lineNo;
				if (line.Length > 0 && line[0] == '[')
				{
					if (ruleName != null)
					{
						rules.Add(new RuleInfo(ruleName, transacts, ruleText.ToString(), vars, fileName, lineOffset - 1));
						ruleText.Length = 0;
					}
					int i = line.IndexOf(']');
					if (i < 2)
					{
						rc.AddError(new SyntaxError(lineNo, 1, "Missing Rule Name"));
						ruleName = null;
					}
					else
					{
						transacts = i == line.Length - 1 ? "": line.Substring(i + 1).Trim();
						ruleName = line.Substring(1, i - 1).Trim();
						lineOffset = lineNo;
						if (ruleName.Length == 0)
						{
							rc.AddError(new SyntaxError(lineNo, 1, "Missing Rule Name"));
							ruleName = null;
						}
					}
				}
				else if (ruleName != null)
				{
					ruleText.Append(line).Append(nl);
				}
			}
			if (ruleName != null)
				rules.Add(new RuleInfo(ruleName, transacts, ruleText.ToString(), vars, fileName, lineOffset - 1));
			rc.SetRules(rules.ToArray());
			return rc;
		}

		private void AddError(SyntaxError error)
		{
			ErrorMessage = error.ComposeMessage(FileName, 0);
		}

		public void Compile()
		{
			BuildStartupCode();
			BuilbBatchHeader();
			BuildBatchFooter();
			StringBuilder errors = new StringBuilder();
			StringBuilder body = new StringBuilder();
			foreach (var item in _rules)
			{
				if (item.Compile(_usedRules.Contains(item.Name)))
				{
					body.Append(item.CompiledText);
				}
				else
				{
					if (errors.Length > 0)
						errors.Append(Environment.NewLine).Append(Environment.NewLine);
					errors.Append("Rule [").Append(item.Name).Append("]").Append(Environment.NewLine);
					errors.Append(item.ErrorMessage);
				}
			}
			ErrorMessage = errors.Length > 0 ? errors.ToString(): null;
			CompiledRules = body.Length > 0 ? body.ToString(): null;
		}

		private void BuilbBatchHeader()
		{
			StringBuilder batch = new StringBuilder();
			batch
				.Append("while (@@trancount > 0) rollback").Append(Environment.NewLine)
				.Append("go").Append(Environment.NewLine)
				.Append("use CharityPlanner").Append(Environment.NewLine);

			batch
				.Append("go").Append(Environment.NewLine)
				.Append("begin tran set nocount on set ansi_nulls on set quoted_identifier on").Append(Environment.NewLine)
				.Append("go").Append(Environment.NewLine);
			BatchHeader = batch.ToString();
		}

		private void BuildBatchFooter()
		{
			StringBuilder batch = new StringBuilder();
			batch
				.Append("if (@@trancount = 0) return declare @seed varchar(9) set @seed = 9000").Append(Environment.NewLine)
				.Append("--").Append(Environment.NewLine)
				.Append("").Append(Environment.NewLine);

			int seed = 9010;
			batch.Append("if not exists (select * from dbo.sysobjects where id = object_id('").Append(_vars["$go"]).Append("') and objectproperty(id, 'IsProcedure') = 1) begin set @seed = ").Append(seed).Append("; goto err; end;").Append(Environment.NewLine);

			foreach (RuleInfo rule in _rules)
			{
				if (_usedRules.Contains(rule.Name))
				{
					seed += 10;
					batch.Append("if not exists (select * from dbo.sysobjects where id = object_id('").Append(rule.SqlName).Append("') and objectproperty(id, 'IsProcedure') = 1) begin set @seed = ").Append(seed).Append("; goto err; end;").Append(Environment.NewLine);
				}
			}

			batch
				.Append("---").Append(Environment.NewLine)
				.Append("").Append(Environment.NewLine)
				.Append("return err: if (@@trancount > 0) rollback exec end_log @seed raiserror(14661, 11, 127, @seed) return").Append(Environment.NewLine)
				.Append("go").Append(Environment.NewLine)
				.Append("if (@@trancount = 0) return commit exec end_log return").Append(Environment.NewLine)
				.Append("go").Append(Environment.NewLine)
				.Append("while (@@trancount > 0) begin rollback print 'ROLLED BACK' end").Append(Environment.NewLine)
				.Append("go").Append(Environment.NewLine);
			BatchFooter = batch.ToString();
		}

		private void BuildStartupCode()
		{
			StringBuilder prog = new StringBuilder();

			prog.Append("exec drop_proc '" + _vars["$go"] + "'")
				.Append(nl)
				.Append(nl).Append("go")
				.Append(nl).Append("/****** Procedure \"").Append(_vars["$go"]).Append("\" [generated ").Append(_vars["$tool"]).Append(" at ").Append(DateTime.Now).Append("] ******/")
				.Append(nl)
				.Append(nl).Append("create proc " + _vars["$go"] + "(@t int, @t_asset int, @t_dtTransact date, @t_foundation int, @t_foundationAccount int, @t_type int, @t_unitsNumber decimal(19, 8), @t_amount money, @t_fiscalYear smallint, @errorId int out, @errorInfo varchar(500) out)")
				.Append(nl).Append("as")
				.Append(nl__).Append("begin")
				.Append(nl__).Append("set nocount on")
				.Append(nl__).Append("set @errorId = 0;")
				.Append(nl__).Append("set @errorInfo = null;")
				.Append(nl__).Append("begin try")
				.Append(nl____);

			foreach (var rule in SortRules())
			{
				_usedRules.Add(rule.Name);
				AddCall(prog, rule);
			}

			prog.Length = prog.Length - 1;
			RuleInfo defaultRule = Array.Find(_rules, x => x.IsDefaultRule);
			if (defaultRule != null)
			{
				_usedRules.Add(defaultRule.Name);
				prog.Append(nl______).Append("exec ").Append(defaultRule.SqlName).Append(" @t, @t_asset, @t_dtTransact, @t_foundation, @t_foundationAccount, @t_type, @t_unitsNumber, @t_amount, @t_fiscalYear, @errorId out, @errorInfo out");
			}
			else
			{
				prog.Append(nl______).Append("begin")
					.Append(nl______).Append("set @errorInfo = 'Transaction Type is not supported (transaction ID = ' + isnull(cast(@t as varchar(10)), '<null>') + " +
							"', transaction type = ' + isnull(cast(@t_type as varchar(10)), '<null>') + ')';")
					.Append(nl______).Append("raiserror(@errorInfo, 15, 5);")
					.Append(nl______).Append("return -1;")
					.Append(nl______).Append("end;");
			}

			prog
				.Append(nl__).Append("end try")
				.Append(nl__).Append("begin catch")
				.Append(nl____).Append("set @errorId = error_number();")
				.Append(nl____).Append("if (isnull(@errorId, 0) = 0)")
				.Append(nl______).Append("set @errorId = 1000;")
				.Append(nl____).Append("set @errorInfo = error_procedure() + '(' + cast(error_line() as varchar(10)) + ')' + ': ' +")
				.Append(nl______).Append("isnull(error_message(), 'unhandled error') +")
				.Append(nl______).Append("' (error code = ' + isnull(cast(@errorId as varchar),'<null>') +")
				.Append(nl______).Append("'; transaction ID = ' + isnull(cast(@t as varchar),'<null>') +")
				.Append(nl______).Append("'; transaction type = ' + isnull(cast(@t_type as varchar),'<null>') +")
				.Append(nl______).Append("'; amount = ' + isnull(cast(@t_amount as varchar),'<null>') +")
				.Append(nl______).Append("'; asset ID = ' + isnull(cast(@t_asset as varchar),'<null>') +")
				.Append(nl______).Append("'; units number = ' + isnull(cast(@t_unitsNumber as varchar),'<null>') +")
				.Append(nl______).Append("'; transact date = ' + isnull(convert(varchar,@t_dtTransact,120),'<null>') +")
				.Append(nl______).Append("'; foundation ID = ' + isnull(cast(@t_foundation as varchar),'<null>') +")
				.Append(nl______).Append("'; account ID = ' + isnull(cast(@t_foundationAccount as varchar),'<null>') + ')';")
				.Append(nl__).Append("end catch;")
				.Append(nl)
				.Append(nl__).Append("return case when @errorId <> 0 then -1 else 0 end;")
				.Append(nl__).Append("end;")
				.Append(nl).Append("go")
				.Append(nl);

			Startup = prog.ToString();
		}


		private static readonly string nl = Environment.NewLine;
		private static readonly string nl__ = nl + "\t";
		private static readonly string nl____ = nl + "\t\t";
		private static readonly string nl______ = nl + "\t\t\t";

		private static void AddCall(StringBuilder prog, RuleInfo rule)
		{
			prog.Append("if (@t_type");
			var transacts = rule.IsDefaultRule ? DefaultTransacts : rule.Transacts;
			if (transacts.Length == 1)
			{
				prog.Append(" = ").Append(transacts[0]);
			}
			else
			{
				prog.Append(" in (");
				for (int j = 0; j < transacts.Length; ++j)
				{
					if (j > 0)
						prog.Append(", ");
					prog.Append(transacts[j]);
				}
				prog.Append(")");
			}
			prog.Append(") -- ").Append(rule.Count);
			prog.Append(nl______)
				.Append("exec ").Append(rule.SqlName).Append(" @t, @t_asset, @t_dtTransact, @t_foundation, @t_foundationAccount, @t_type, @t_unitsNumber, @t_amount, @t_fiscalYear, @errorId out, @errorInfo out")
				.Append(nl____).Append("else ");
		}

		#region Statistics and Defaults
		/*
		declare @s varchar(max);
		set @s = 'private static readonly Dictionary<int, int> Statistics = new Dictionary<int, int>
		{
		';
		select @s = @s + '	{' + ltrim(str(Type)) + ',' + ltrim(str(Cnt)) + '},
		'
		from
		(select Type, count(*) Cnt from Transacts where DtTransact>=datefromparts(year(getdate() - 3), 1, 1) group by Type) x
		set @s += '};'
		print @s;
		 */

		//private static readonly Dictionary<int, int> Statistics = new Dictionary<int, int>
		//{
		//	{1,26870},
		//	{2,67696},
		//	{3,2596},
		//	{4,823},
		//	{6,71},
		//	{8,378},
		//	{9,949},
		//	{11,1799},
		//	{12,18},
		//	{13,5388},
		//	{14,181},
		//	{15,11},
		//	{20,528},
		//	{30,58174},
		//	{31,216},
		//	{50,1992},
		//	{51,13017},
		//	{53,71},
		//	{54,71},
		//	{58,2606},
		//	{59,2},
		//	{61,815},
		//	{62,5120},
		//	{63,4636},
		//	{67,4495},
		//	{68,1},
		//	{69,2202},
		//	{70,1504},
		//	{77,3},
		//	{79,76},
		//	{80,48497},
		//	{81,12},
		//	{83,405},
		//	{87,11},
		//	{88,42},
		//	{89,5260},
		//	{94,9},
		//	{100,482},
		//	{101,413},
		//	{263,545},
		//	{266,85},
		//	{267,141},
		//	{268,306},
		//	{269,500},
		//	{270,2938},
		//	{271,1075},
		//	{279,234},
		//	{280,155},
		//	{281,247},
		//	{282,104},
		//	{283,910},
		//	{284,201},
		//	{285,32},
		//	{286,41},
		//	{287,4},
		//	{288,2},
		//	{291,5},
		//	{294,3},
		//	{296,11},
		//	{298,76},
		//	{299,16},
		//	{300,8},
		//	{312,623},
		//	{313,2},
		//	{314,494},
		//	{315,208},
		//	{316,1},
		//	{317,295},
		//	{318,2},
		//	{319,40},
		//	{320,1},
		//	{321,1},
		//	{322,2},
		//	{324,207},
		//	{325,7},
		//	{326,3},
		//	{331,13},
		//	{332,1},
		//	{337,994},
		//	{338,6},
		//	{339,7},
		//	{342,1},
		//	{345,4},
		//	{348,1},
		//	{349,6},
		//	{352,21},
		//	{353,144},
		//	{356,1},
		//	{357,2},
		//	{358,2},
		//	{359,6},
		//	{371,103},
		//	{381,2},
		//	{382,2},
		//	{385,10},
		//	{387,38},
		//	{388,29},
		//	{389,25},
		//	{390,37},
		//	{391,5},
		//	{392,73},
		//	{427,3},
		//	{433,70},
		//};

		private static readonly Dictionary<int, int> Statistics = GetStatistics();
		private static int[] DefaultTransacts;

		private RuleInfo[] SortRules()
		{
			var allTypes = _rules.SelectMany(o => o.Transacts).ToList();
			DefaultTransacts = Statistics.Keys.Where(o => !allTypes.Contains(o)).ToArray();

			var result = new RuleInfo[_rules.Length];
			Array.Copy(_rules, result, result.Length);
			foreach (var item in result)
			{
				item.Count = (item.IsDefaultRule ? DefaultTransacts : item.Transacts).Sum(o => { Statistics.TryGetValue(o, out int count); return count; });
			}
			Array.Sort(result, StatComparer.Instance);
			return result;
		}

		private static Dictionary<int, int> GetStatistics()
		{
			string text = File.ReadAllText("stats.txt");
			int i = text.IndexOf("\nSTATS:", StringComparison.Ordinal);
			if (i < 0)
				throw new InvalidOperationException("Missing header in file stats.txt.");
			i = text.IndexOf('\n', i + 1);
			if (i < 0)
				throw new InvalidOperationException("Missing body in file stats.txt.");
			var r = new Regex(@"[\n\r\s]*(\d+)\s*\:\s*(\d+)\s*");
			var map = new Dictionary<int, int>();
			foreach (Match m in r.Matches(text, i))
			{
				int transact = int.Parse(m.Groups[1].Value);
				int count = int.Parse(m.Groups[2].Value);
				map[transact] = count;
			}
			return map;
		}

		private class StatComparer: IComparer<RuleInfo>
		{
			public static readonly StatComparer Instance = new StatComparer();

			private StatComparer()
			{
			}

			public int Compare(RuleInfo x, RuleInfo y)
			{
				if (x == null)
					throw new ArgumentNullException(nameof(x));
				if (y == null)
					throw new ArgumentNullException(nameof(y));
				return x.Count == y.Count ? x.Transacts.Length.CompareTo(y.Transacts.Length): y.Count.CompareTo(x.Count);
			}
		}
		#endregion

		#region IEnumerable Members
		public IEnumerator<RuleInfo> GetEnumerator()
		{
			return (IEnumerator<RuleInfo>)_rules.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return _rules.GetEnumerator();
		}
		#endregion
	}

	[DebuggerDisplay("{" + nameof(Name) + "}")]
	class RuleInfo
	{
		public string Name { get; }
		public string Text { get; }
		public int[] Transacts { get; }
		public string FileName { get; }
		public int LineOffset { get; }

		public string SqlName { get; }
		public string CompiledText { get; private set; }
		public string ErrorMessage { get; private set; }
		public int Count { get; set; }

		private readonly RulesOptions _vars;

		public RuleInfo(string name, string transacts, string text, RulesOptions vars)
			: this(name, transacts, text, vars, null, 0)
		{
		}

		public RuleInfo(string name, string transacts, string text, RulesOptions vars, string fileName, int lineOffset)
		{
			HashSet<int> tts = new HashSet<int>();
			if (transacts != null)
			{
				foreach (var item in transacts.Split(__numnersSeparator, StringSplitOptions.RemoveEmptyEntries))
				{
					int t;
					if (Int32.TryParse(item, out t) && t > 0)
					{
						tts.Add(t);
					}
					else if (item == "*")
					{
						tts.Clear();
						tts.Add(-1);
						break;
					}
					else if (item.Contains(".."))
					{
						string[] parts = item.Split(__rangeSeparator, 2, StringSplitOptions.None);
						int t2;
						if (Int32.TryParse(parts[0], out t) && t > 0 && Int32.TryParse(parts[1], out t2) && t2 >= t)
						{
							while (t <= t2)
							{
								tts.Add(t);
								++t;
							}
						}
					}
				}
			}
			int[] tta = tts.ToArray();
			Array.Sort(tta);

			_vars = new RulesOptions(vars);
			Name = name;
			Text = text;
			Transacts = tta;
			FileName = fileName;
			LineOffset = lineOffset;

			SqlName = "[" + FixName(_vars["$schema"]) + "].[" + FixName(_vars["$prefix"]) + FixName(Name) + "]";
			_vars["ProcedureName"] = "'" + SqlName.Replace("'", "''") + "'";
		}
		private static readonly char[] __numnersSeparator = { '/' };
		private static readonly string[] __rangeSeparator = { ".." };

		public bool HasTransact => Transacts.Length > 0 && Transacts[0] != -1;

		public bool IsDefaultRule => Transacts.Length > 0 && Transacts[0] == -1;

		private static string FixName(string name)
		{
			return name.Replace(' ', '_').Replace("]", "]]");
		}

		public bool HasError => ErrorMessage != null;

		public virtual bool Compile(bool needBody)
		{
			CompiledText = null;
			ErrorMessage = null;
			string nl = Environment.NewLine;
			StringBuilder prog = new StringBuilder();
			prog.Append("exec drop_proc '").Append(SqlName).Append("'")
				.Append(nl).Append("go")
				.Append(nl);

			if (needBody)
			{
				string body = CompileBody(1, new[] { "t.ID", "t.Foundation", "t.FoundationAccount", "t.Asset", "t.DtTransact", "t.Type", "t.UnitsNumber", "t.Amount", "t.FiscalYear", "errorId", "errorInfo" });
				if (body == null)
					return false;

				prog.Append("/****** Procedure \"").Append(Name).Append("\" [").Append(_vars["$tool"]).Append(" ").Append(DateTime.Now).Append("]")
					.Append(nl)
					.Append(nl).Append("\t").Append(Text.Trim().Replace("*/", "** /").Replace("/*", "/ **"))
					.Append(nl)
					.Append(nl).Append("******/")
					.Append(nl).Append("create proc ").Append(SqlName).Append("(@t int, @t_asset int, @t_dtTransact date, @t_foundation int, @t_foundationAccount int, @t_type int, @t_unitsNumber decimal(19, 8), @t_amount money, @t_fiscalYear smallint, @errorId int out, @errorInfo varchar(500) out)")
					.Append(nl).Append("as")
					.Append(nl).Append("	begin")
					.Append(nl).Append("	set nocount on")
					.Append(nl).Append(body)
					.Append(nl).Append("	end")
					.Append(nl).Append("go")
					.Append(nl)
					.Append(nl);
			}
			CompiledText = prog.ToString();
			return true;
		}

		public string CompileBody(int indent, IEnumerable<string> externals)
		{
			ErrorMessage = null;
			HoldingRuleLexer lexer = new HoldingRuleLexer(new ANTLRStringStream(Text));
			HoldingRuleParser parser = new HoldingRuleParser(new CommonTokenStream(lexer));
			parser.Context.Declaration.Externals = externals;
			Program prog = null;
			try
			{
				foreach (string key in _vars)
				{
					parser.Context.Declaration.Define(key, _vars[key]);
					parser.Context.SetValue(key, true);
				}
				parser.program();
				prog = parser.Context;
			}
			catch (Exception e)
			{
				if (prog == null)
					prog = parser.Context == null ? new Program(): parser.Context;
				prog.AddError(new SyntaxError(LineOffset, 1, e.ToString() + Environment.NewLine + e.StackTrace));
			}
			if (prog.HasError)
			{
				ErrorMessage = prog.GetErrorMessage(Environment.NewLine, FileName, LineOffset);
				return null;
			}
			return prog.GetText(indent);
		}
	}
}
