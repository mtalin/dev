// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\output\\HoldingRule.g 2011-08-30 16:14:29

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


using System;
using System.Collections.Generic;
using Antlr.Runtime;
using Stack = System.Collections.Generic.Stack<object>;
using List = System.Collections.IList;
using ArrayList = System.Collections.Generic.List<object>;

[System.CodeDom.Compiler.GeneratedCode("ANTLR", "3.3 Nov 30, 2010 12:45:30")]
[System.CLSCompliant(false)]
public partial class HoldingRuleLexer : Antlr.Runtime.Lexer
{
	public const int EOF=-1;
	public const int T__23=23;
	public const int T__24=24;
	public const int T__25=25;
	public const int T__26=26;
	public const int T__27=27;
	public const int T__28=28;
	public const int T__29=29;
	public const int T__30=30;
	public const int T__31=31;
	public const int T__32=32;
	public const int T__33=33;
	public const int T__34=34;
	public const int T__35=35;
	public const int T__36=36;
	public const int T__37=37;
	public const int T__38=38;
	public const int T__39=39;
	public const int T__40=40;
	public const int T__41=41;
	public const int T__42=42;
	public const int T__43=43;
	public const int T__44=44;
	public const int T__45=45;
	public const int T__46=46;
	public const int T__47=47;
	public const int T__48=48;
	public const int T__49=49;
	public const int T__50=50;
	public const int T__51=51;
	public const int T__52=52;
	public const int T__53=53;
	public const int T__54=54;
	public const int T__55=55;
	public const int T__56=56;
	public const int T__57=57;
	public const int T__58=58;
	public const int T__59=59;
	public const int T__60=60;
	public const int T__61=61;
	public const int T__62=62;
	public const int T__63=63;
	public const int T__64=64;
	public const int T__65=65;
	public const int T__66=66;
	public const int T__67=67;
	public const int T__68=68;
	public const int T__69=69;
	public const int T__70=70;
	public const int T__71=71;
	public const int T__72=72;
	public const int T__73=73;
	public const int T__74=74;
	public const int T__75=75;
	public const int T__76=76;
	public const int T__77=77;
	public const int T__78=78;
	public const int T__79=79;
	public const int T__80=80;
	public const int T__81=81;
	public const int T__82=82;
	public const int T__83=83;
	public const int T__84=84;
	public const int T__85=85;
	public const int T__86=86;
	public const int T__87=87;
	public const int T__88=88;
	public const int T__89=89;
	public const int T__90=90;
	public const int T__91=91;
	public const int T__92=92;
	public const int T__93=93;
	public const int T__94=94;
	public const int T__95=95;
	public const int T__96=96;
	public const int T__97=97;
	public const int T__98=98;
	public const int T__99=99;
	public const int T__100=100;
	public const int T__101=101;
	public const int T__102=102;
	public const int T__103=103;
	public const int T__104=104;
	public const int T__105=105;
	public const int T__106=106;
	public const int T__107=107;
	public const int T__108=108;
	public const int T__109=109;
	public const int T__110=110;
	public const int T__111=111;
	public const int T__112=112;
	public const int T__113=113;
	public const int T__114=114;
	public const int T__115=115;
	public const int T__116=116;
	public const int T__117=117;
	public const int T__118=118;
	public const int T__119=119;
	public const int T__120=120;
	public const int T__121=121;
	public const int T__122=122;
	public const int T__123=123;
	public const int T__124=124;
	public const int T__125=125;
	public const int T__126=126;
	public const int T__127=127;
	public const int T__128=128;
	public const int T__129=129;
	public const int T__130=130;
	public const int T__131=131;
	public const int T__132=132;
	public const int T__133=133;
	public const int T__134=134;
	public const int T__135=135;
	public const int T__136=136;
	public const int T__137=137;
	public const int T__138=138;
	public const int T__139=139;
	public const int T__140=140;
	public const int T__141=141;
	public const int T__142=142;
	public const int T__143=143;
	public const int T__144=144;
	public const int T__145=145;
	public const int T__146=146;
	public const int T__147=147;
	public const int T__148=148;
	public const int T__149=149;
	public const int T__150=150;
	public const int T__151=151;
	public const int T__152=152;
	public const int T__153=153;
	public const int T__154=154;
	public const int T__155=155;
	public const int T__156=156;
	public const int T__157=157;
	public const int T__158=158;
	public const int T__159=159;
	public const int T__160=160;
	public const int IDENTIFIER=4;
	public const int INT=5;
	public const int STRING=6;
	public const int EQUAL=7;
	public const int OPEQUAL=8;
	public const int OR=9;
	public const int AND=10;
	public const int COMPARE=11;
	public const int NOT=12;
	public const int IN=13;
	public const int CONTAINS=14;
	public const int DEC=15;
	public const int TRANSACT_FIELD=16;
	public const int STRING1=17;
	public const int STRING2=18;
	public const int STRINGCHAR1=19;
	public const int STRINGCHAR2=20;
	public const int WS=21;
	public const int COMMENTS=22;

    // delegates
    // delegators

	public HoldingRuleLexer()
	{
		OnCreated();
	}

	public HoldingRuleLexer(ICharStream input )
		: this(input, new RecognizerSharedState())
	{
	}

	public HoldingRuleLexer(ICharStream input, RecognizerSharedState state)
		: base(input, state)
	{


		OnCreated();
	}
	public override string GrammarFileName { get { return "C:\\output\\HoldingRule.g"; } }

	private static readonly bool[] decisionCanBacktrack = new bool[0];

 
	protected virtual void OnCreated() {}
	protected virtual void EnterRule(string ruleName, int ruleIndex) {}
	protected virtual void LeaveRule(string ruleName, int ruleIndex) {}

    protected virtual void Enter_T__23() {}
    protected virtual void Leave_T__23() {}

    // $ANTLR start "T__23"
    [GrammarRule("T__23")]
    private void mT__23()
    {

    		try
    		{
    		int _type = T__23;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:7:7: ( ';' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:7:9: ';'
    		{
    		DebugLocation(7, 9);
    		Match(';'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__23"

    protected virtual void Enter_T__24() {}
    protected virtual void Leave_T__24() {}

    // $ANTLR start "T__24"
    [GrammarRule("T__24")]
    private void mT__24()
    {

    		try
    		{
    		int _type = T__24;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:8:7: ( 'RETURN' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:8:9: 'RETURN'
    		{
    		DebugLocation(8, 9);
    		Match("RETURN"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__24"

    protected virtual void Enter_T__25() {}
    protected virtual void Leave_T__25() {}

    // $ANTLR start "T__25"
    [GrammarRule("T__25")]
    private void mT__25()
    {

    		try
    		{
    		int _type = T__25;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:9:7: ( 'Return' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:9:9: 'Return'
    		{
    		DebugLocation(9, 9);
    		Match("Return"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__25"

    protected virtual void Enter_T__26() {}
    protected virtual void Leave_T__26() {}

    // $ANTLR start "T__26"
    [GrammarRule("T__26")]
    private void mT__26()
    {

    		try
    		{
    		int _type = T__26;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:10:7: ( 'return' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:10:9: 'return'
    		{
    		DebugLocation(10, 9);
    		Match("return"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__26"

    protected virtual void Enter_T__27() {}
    protected virtual void Leave_T__27() {}

    // $ANTLR start "T__27"
    [GrammarRule("T__27")]
    private void mT__27()
    {

    		try
    		{
    		int _type = T__27;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:11:7: ( '{' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:11:9: '{'
    		{
    		DebugLocation(11, 9);
    		Match('{'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__27"

    protected virtual void Enter_T__28() {}
    protected virtual void Leave_T__28() {}

    // $ANTLR start "T__28"
    [GrammarRule("T__28")]
    private void mT__28()
    {

    		try
    		{
    		int _type = T__28;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:12:7: ( '}' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:12:9: '}'
    		{
    		DebugLocation(12, 9);
    		Match('}'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__28"

    protected virtual void Enter_T__29() {}
    protected virtual void Leave_T__29() {}

    // $ANTLR start "T__29"
    [GrammarRule("T__29")]
    private void mT__29()
    {

    		try
    		{
    		int _type = T__29;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:13:7: ( 'GOTO' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:13:9: 'GOTO'
    		{
    		DebugLocation(13, 9);
    		Match("GOTO"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__29"

    protected virtual void Enter_T__30() {}
    protected virtual void Leave_T__30() {}

    // $ANTLR start "T__30"
    [GrammarRule("T__30")]
    private void mT__30()
    {

    		try
    		{
    		int _type = T__30;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:14:7: ( 'GoTo' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:14:9: 'GoTo'
    		{
    		DebugLocation(14, 9);
    		Match("GoTo"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__30"

    protected virtual void Enter_T__31() {}
    protected virtual void Leave_T__31() {}

    // $ANTLR start "T__31"
    [GrammarRule("T__31")]
    private void mT__31()
    {

    		try
    		{
    		int _type = T__31;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:15:7: ( 'Goto' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:15:9: 'Goto'
    		{
    		DebugLocation(15, 9);
    		Match("Goto"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__31"

    protected virtual void Enter_T__32() {}
    protected virtual void Leave_T__32() {}

    // $ANTLR start "T__32"
    [GrammarRule("T__32")]
    private void mT__32()
    {

    		try
    		{
    		int _type = T__32;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:16:7: ( 'goto' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:16:9: 'goto'
    		{
    		DebugLocation(16, 9);
    		Match("goto"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__32"

    protected virtual void Enter_T__33() {}
    protected virtual void Leave_T__33() {}

    // $ANTLR start "T__33"
    [GrammarRule("T__33")]
    private void mT__33()
    {

    		try
    		{
    		int _type = T__33;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:17:7: ( 'VAR' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:17:9: 'VAR'
    		{
    		DebugLocation(17, 9);
    		Match("VAR"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__33"

    protected virtual void Enter_T__34() {}
    protected virtual void Leave_T__34() {}

    // $ANTLR start "T__34"
    [GrammarRule("T__34")]
    private void mT__34()
    {

    		try
    		{
    		int _type = T__34;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:18:7: ( 'Var' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:18:9: 'Var'
    		{
    		DebugLocation(18, 9);
    		Match("Var"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__34"

    protected virtual void Enter_T__35() {}
    protected virtual void Leave_T__35() {}

    // $ANTLR start "T__35"
    [GrammarRule("T__35")]
    private void mT__35()
    {

    		try
    		{
    		int _type = T__35;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:19:7: ( 'var' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:19:9: 'var'
    		{
    		DebugLocation(19, 9);
    		Match("var"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__35"

    protected virtual void Enter_T__36() {}
    protected virtual void Leave_T__36() {}

    // $ANTLR start "T__36"
    [GrammarRule("T__36")]
    private void mT__36()
    {

    		try
    		{
    		int _type = T__36;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:20:7: ( ',' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:20:9: ','
    		{
    		DebugLocation(20, 9);
    		Match(','); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__36"

    protected virtual void Enter_T__37() {}
    protected virtual void Leave_T__37() {}

    // $ANTLR start "T__37"
    [GrammarRule("T__37")]
    private void mT__37()
    {

    		try
    		{
    		int _type = T__37;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:21:7: ( 'INT' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:21:9: 'INT'
    		{
    		DebugLocation(21, 9);
    		Match("INT"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__37"

    protected virtual void Enter_T__38() {}
    protected virtual void Leave_T__38() {}

    // $ANTLR start "T__38"
    [GrammarRule("T__38")]
    private void mT__38()
    {

    		try
    		{
    		int _type = T__38;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:22:7: ( 'Int' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:22:9: 'Int'
    		{
    		DebugLocation(22, 9);
    		Match("Int"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__38"

    protected virtual void Enter_T__39() {}
    protected virtual void Leave_T__39() {}

    // $ANTLR start "T__39"
    [GrammarRule("T__39")]
    private void mT__39()
    {

    		try
    		{
    		int _type = T__39;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:23:7: ( 'int' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:23:9: 'int'
    		{
    		DebugLocation(23, 9);
    		Match("int"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__39"

    protected virtual void Enter_T__40() {}
    protected virtual void Leave_T__40() {}

    // $ANTLR start "T__40"
    [GrammarRule("T__40")]
    private void mT__40()
    {

    		try
    		{
    		int _type = T__40;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:24:7: ( 'LONG' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:24:9: 'LONG'
    		{
    		DebugLocation(24, 9);
    		Match("LONG"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__40"

    protected virtual void Enter_T__41() {}
    protected virtual void Leave_T__41() {}

    // $ANTLR start "T__41"
    [GrammarRule("T__41")]
    private void mT__41()
    {

    		try
    		{
    		int _type = T__41;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:25:7: ( 'Long' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:25:9: 'Long'
    		{
    		DebugLocation(25, 9);
    		Match("Long"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__41"

    protected virtual void Enter_T__42() {}
    protected virtual void Leave_T__42() {}

    // $ANTLR start "T__42"
    [GrammarRule("T__42")]
    private void mT__42()
    {

    		try
    		{
    		int _type = T__42;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:26:7: ( 'long' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:26:9: 'long'
    		{
    		DebugLocation(26, 9);
    		Match("long"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__42"

    protected virtual void Enter_T__43() {}
    protected virtual void Leave_T__43() {}

    // $ANTLR start "T__43"
    [GrammarRule("T__43")]
    private void mT__43()
    {

    		try
    		{
    		int _type = T__43;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:27:7: ( 'STRING' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:27:9: 'STRING'
    		{
    		DebugLocation(27, 9);
    		Match("STRING"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__43"

    protected virtual void Enter_T__44() {}
    protected virtual void Leave_T__44() {}

    // $ANTLR start "T__44"
    [GrammarRule("T__44")]
    private void mT__44()
    {

    		try
    		{
    		int _type = T__44;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:28:7: ( 'String' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:28:9: 'String'
    		{
    		DebugLocation(28, 9);
    		Match("String"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__44"

    protected virtual void Enter_T__45() {}
    protected virtual void Leave_T__45() {}

    // $ANTLR start "T__45"
    [GrammarRule("T__45")]
    private void mT__45()
    {

    		try
    		{
    		int _type = T__45;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:29:7: ( 'string' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:29:9: 'string'
    		{
    		DebugLocation(29, 9);
    		Match("string"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__45"

    protected virtual void Enter_T__46() {}
    protected virtual void Leave_T__46() {}

    // $ANTLR start "T__46"
    [GrammarRule("T__46")]
    private void mT__46()
    {

    		try
    		{
    		int _type = T__46;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:30:7: ( '(' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:30:9: '('
    		{
    		DebugLocation(30, 9);
    		Match('('); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__46"

    protected virtual void Enter_T__47() {}
    protected virtual void Leave_T__47() {}

    // $ANTLR start "T__47"
    [GrammarRule("T__47")]
    private void mT__47()
    {

    		try
    		{
    		int _type = T__47;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:31:7: ( ')' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:31:9: ')'
    		{
    		DebugLocation(31, 9);
    		Match(')'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__47"

    protected virtual void Enter_T__48() {}
    protected virtual void Leave_T__48() {}

    // $ANTLR start "T__48"
    [GrammarRule("T__48")]
    private void mT__48()
    {

    		try
    		{
    		int _type = T__48;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:32:7: ( 'DECIMAL' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:32:9: 'DECIMAL'
    		{
    		DebugLocation(32, 9);
    		Match("DECIMAL"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__48"

    protected virtual void Enter_T__49() {}
    protected virtual void Leave_T__49() {}

    // $ANTLR start "T__49"
    [GrammarRule("T__49")]
    private void mT__49()
    {

    		try
    		{
    		int _type = T__49;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:33:7: ( 'Decimal' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:33:9: 'Decimal'
    		{
    		DebugLocation(33, 9);
    		Match("Decimal"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__49"

    protected virtual void Enter_T__50() {}
    protected virtual void Leave_T__50() {}

    // $ANTLR start "T__50"
    [GrammarRule("T__50")]
    private void mT__50()
    {

    		try
    		{
    		int _type = T__50;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:34:7: ( 'decimal' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:34:9: 'decimal'
    		{
    		DebugLocation(34, 9);
    		Match("decimal"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__50"

    protected virtual void Enter_T__51() {}
    protected virtual void Leave_T__51() {}

    // $ANTLR start "T__51"
    [GrammarRule("T__51")]
    private void mT__51()
    {

    		try
    		{
    		int _type = T__51;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:35:7: ( 'DECIMAL2' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:35:9: 'DECIMAL2'
    		{
    		DebugLocation(35, 9);
    		Match("DECIMAL2"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__51"

    protected virtual void Enter_T__52() {}
    protected virtual void Leave_T__52() {}

    // $ANTLR start "T__52"
    [GrammarRule("T__52")]
    private void mT__52()
    {

    		try
    		{
    		int _type = T__52;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:36:7: ( 'Decimal2' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:36:9: 'Decimal2'
    		{
    		DebugLocation(36, 9);
    		Match("Decimal2"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__52"

    protected virtual void Enter_T__53() {}
    protected virtual void Leave_T__53() {}

    // $ANTLR start "T__53"
    [GrammarRule("T__53")]
    private void mT__53()
    {

    		try
    		{
    		int _type = T__53;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:37:7: ( 'decimal2' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:37:9: 'decimal2'
    		{
    		DebugLocation(37, 9);
    		Match("decimal2"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__53"

    protected virtual void Enter_T__54() {}
    protected virtual void Leave_T__54() {}

    // $ANTLR start "T__54"
    [GrammarRule("T__54")]
    private void mT__54()
    {

    		try
    		{
    		int _type = T__54;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:38:7: ( 'MONEY' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:38:9: 'MONEY'
    		{
    		DebugLocation(38, 9);
    		Match("MONEY"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__54"

    protected virtual void Enter_T__55() {}
    protected virtual void Leave_T__55() {}

    // $ANTLR start "T__55"
    [GrammarRule("T__55")]
    private void mT__55()
    {

    		try
    		{
    		int _type = T__55;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:39:7: ( 'Money' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:39:9: 'Money'
    		{
    		DebugLocation(39, 9);
    		Match("Money"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__55"

    protected virtual void Enter_T__56() {}
    protected virtual void Leave_T__56() {}

    // $ANTLR start "T__56"
    [GrammarRule("T__56")]
    private void mT__56()
    {

    		try
    		{
    		int _type = T__56;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:40:7: ( 'money' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:40:9: 'money'
    		{
    		DebugLocation(40, 9);
    		Match("money"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__56"

    protected virtual void Enter_T__57() {}
    protected virtual void Leave_T__57() {}

    // $ANTLR start "T__57"
    [GrammarRule("T__57")]
    private void mT__57()
    {

    		try
    		{
    		int _type = T__57;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:41:7: ( 'FLOAT' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:41:9: 'FLOAT'
    		{
    		DebugLocation(41, 9);
    		Match("FLOAT"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__57"

    protected virtual void Enter_T__58() {}
    protected virtual void Leave_T__58() {}

    // $ANTLR start "T__58"
    [GrammarRule("T__58")]
    private void mT__58()
    {

    		try
    		{
    		int _type = T__58;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:42:7: ( 'Float' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:42:9: 'Float'
    		{
    		DebugLocation(42, 9);
    		Match("Float"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__58"

    protected virtual void Enter_T__59() {}
    protected virtual void Leave_T__59() {}

    // $ANTLR start "T__59"
    [GrammarRule("T__59")]
    private void mT__59()
    {

    		try
    		{
    		int _type = T__59;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:43:7: ( 'float' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:43:9: 'float'
    		{
    		DebugLocation(43, 9);
    		Match("float"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__59"

    protected virtual void Enter_T__60() {}
    protected virtual void Leave_T__60() {}

    // $ANTLR start "T__60"
    [GrammarRule("T__60")]
    private void mT__60()
    {

    		try
    		{
    		int _type = T__60;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:44:7: ( 'DATETIME' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:44:9: 'DATETIME'
    		{
    		DebugLocation(44, 9);
    		Match("DATETIME"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__60"

    protected virtual void Enter_T__61() {}
    protected virtual void Leave_T__61() {}

    // $ANTLR start "T__61"
    [GrammarRule("T__61")]
    private void mT__61()
    {

    		try
    		{
    		int _type = T__61;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:45:7: ( 'Datetime' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:45:9: 'Datetime'
    		{
    		DebugLocation(45, 9);
    		Match("Datetime"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__61"

    protected virtual void Enter_T__62() {}
    protected virtual void Leave_T__62() {}

    // $ANTLR start "T__62"
    [GrammarRule("T__62")]
    private void mT__62()
    {

    		try
    		{
    		int _type = T__62;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:46:7: ( 'datetime' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:46:9: 'datetime'
    		{
    		DebugLocation(46, 9);
    		Match("datetime"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__62"

    protected virtual void Enter_T__63() {}
    protected virtual void Leave_T__63() {}

    // $ANTLR start "T__63"
    [GrammarRule("T__63")]
    private void mT__63()
    {

    		try
    		{
    		int _type = T__63;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:47:7: ( 'MAX' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:47:9: 'MAX'
    		{
    		DebugLocation(47, 9);
    		Match("MAX"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__63"

    protected virtual void Enter_T__64() {}
    protected virtual void Leave_T__64() {}

    // $ANTLR start "T__64"
    [GrammarRule("T__64")]
    private void mT__64()
    {

    		try
    		{
    		int _type = T__64;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:48:7: ( 'Max' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:48:9: 'Max'
    		{
    		DebugLocation(48, 9);
    		Match("Max"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__64"

    protected virtual void Enter_T__65() {}
    protected virtual void Leave_T__65() {}

    // $ANTLR start "T__65"
    [GrammarRule("T__65")]
    private void mT__65()
    {

    		try
    		{
    		int _type = T__65;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:49:7: ( 'max' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:49:9: 'max'
    		{
    		DebugLocation(49, 9);
    		Match("max"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__65"

    protected virtual void Enter_T__66() {}
    protected virtual void Leave_T__66() {}

    // $ANTLR start "T__66"
    [GrammarRule("T__66")]
    private void mT__66()
    {

    		try
    		{
    		int _type = T__66;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:50:7: ( ':' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:50:9: ':'
    		{
    		DebugLocation(50, 9);
    		Match(':'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__66"

    protected virtual void Enter_T__67() {}
    protected virtual void Leave_T__67() {}

    // $ANTLR start "T__67"
    [GrammarRule("T__67")]
    private void mT__67()
    {

    		try
    		{
    		int _type = T__67;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:51:7: ( 'IF' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:51:9: 'IF'
    		{
    		DebugLocation(51, 9);
    		Match("IF"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__67"

    protected virtual void Enter_T__68() {}
    protected virtual void Leave_T__68() {}

    // $ANTLR start "T__68"
    [GrammarRule("T__68")]
    private void mT__68()
    {

    		try
    		{
    		int _type = T__68;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:52:7: ( 'If' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:52:9: 'If'
    		{
    		DebugLocation(52, 9);
    		Match("If"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__68"

    protected virtual void Enter_T__69() {}
    protected virtual void Leave_T__69() {}

    // $ANTLR start "T__69"
    [GrammarRule("T__69")]
    private void mT__69()
    {

    		try
    		{
    		int _type = T__69;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:53:7: ( 'if' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:53:9: 'if'
    		{
    		DebugLocation(53, 9);
    		Match("if"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__69"

    protected virtual void Enter_T__70() {}
    protected virtual void Leave_T__70() {}

    // $ANTLR start "T__70"
    [GrammarRule("T__70")]
    private void mT__70()
    {

    		try
    		{
    		int _type = T__70;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:54:7: ( 'ELSE' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:54:9: 'ELSE'
    		{
    		DebugLocation(54, 9);
    		Match("ELSE"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__70"

    protected virtual void Enter_T__71() {}
    protected virtual void Leave_T__71() {}

    // $ANTLR start "T__71"
    [GrammarRule("T__71")]
    private void mT__71()
    {

    		try
    		{
    		int _type = T__71;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:55:7: ( 'Else' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:55:9: 'Else'
    		{
    		DebugLocation(55, 9);
    		Match("Else"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__71"

    protected virtual void Enter_T__72() {}
    protected virtual void Leave_T__72() {}

    // $ANTLR start "T__72"
    [GrammarRule("T__72")]
    private void mT__72()
    {

    		try
    		{
    		int _type = T__72;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:56:7: ( 'else' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:56:9: 'else'
    		{
    		DebugLocation(56, 9);
    		Match("else"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__72"

    protected virtual void Enter_T__73() {}
    protected virtual void Leave_T__73() {}

    // $ANTLR start "T__73"
    [GrammarRule("T__73")]
    private void mT__73()
    {

    		try
    		{
    		int _type = T__73;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:57:7: ( 'PRINT' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:57:9: 'PRINT'
    		{
    		DebugLocation(57, 9);
    		Match("PRINT"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__73"

    protected virtual void Enter_T__74() {}
    protected virtual void Leave_T__74() {}

    // $ANTLR start "T__74"
    [GrammarRule("T__74")]
    private void mT__74()
    {

    		try
    		{
    		int _type = T__74;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:58:7: ( 'Print' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:58:9: 'Print'
    		{
    		DebugLocation(58, 9);
    		Match("Print"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__74"

    protected virtual void Enter_T__75() {}
    protected virtual void Leave_T__75() {}

    // $ANTLR start "T__75"
    [GrammarRule("T__75")]
    private void mT__75()
    {

    		try
    		{
    		int _type = T__75;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:59:7: ( 'print' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:59:9: 'print'
    		{
    		DebugLocation(59, 9);
    		Match("print"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__75"

    protected virtual void Enter_T__76() {}
    protected virtual void Leave_T__76() {}

    // $ANTLR start "T__76"
    [GrammarRule("T__76")]
    private void mT__76()
    {

    		try
    		{
    		int _type = T__76;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:60:7: ( 'ERROR' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:60:9: 'ERROR'
    		{
    		DebugLocation(60, 9);
    		Match("ERROR"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__76"

    protected virtual void Enter_T__77() {}
    protected virtual void Leave_T__77() {}

    // $ANTLR start "T__77"
    [GrammarRule("T__77")]
    private void mT__77()
    {

    		try
    		{
    		int _type = T__77;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:61:7: ( 'Error' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:61:9: 'Error'
    		{
    		DebugLocation(61, 9);
    		Match("Error"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__77"

    protected virtual void Enter_T__78() {}
    protected virtual void Leave_T__78() {}

    // $ANTLR start "T__78"
    [GrammarRule("T__78")]
    private void mT__78()
    {

    		try
    		{
    		int _type = T__78;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:62:7: ( 'error' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:62:9: 'error'
    		{
    		DebugLocation(62, 9);
    		Match("error"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__78"

    protected virtual void Enter_T__79() {}
    protected virtual void Leave_T__79() {}

    // $ANTLR start "T__79"
    [GrammarRule("T__79")]
    private void mT__79()
    {

    		try
    		{
    		int _type = T__79;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:63:7: ( 'CREATELOT' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:63:9: 'CREATELOT'
    		{
    		DebugLocation(63, 9);
    		Match("CREATELOT"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__79"

    protected virtual void Enter_T__80() {}
    protected virtual void Leave_T__80() {}

    // $ANTLR start "T__80"
    [GrammarRule("T__80")]
    private void mT__80()
    {

    		try
    		{
    		int _type = T__80;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:64:7: ( 'CreateLot' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:64:9: 'CreateLot'
    		{
    		DebugLocation(64, 9);
    		Match("CreateLot"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__80"

    protected virtual void Enter_T__81() {}
    protected virtual void Leave_T__81() {}

    // $ANTLR start "T__81"
    [GrammarRule("T__81")]
    private void mT__81()
    {

    		try
    		{
    		int _type = T__81;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:65:7: ( 'createLot' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:65:9: 'createLot'
    		{
    		DebugLocation(65, 9);
    		Match("createLot"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__81"

    protected virtual void Enter_T__82() {}
    protected virtual void Leave_T__82() {}

    // $ANTLR start "T__82"
    [GrammarRule("T__82")]
    private void mT__82()
    {

    		try
    		{
    		int _type = T__82;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:66:7: ( 'createlot' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:66:9: 'createlot'
    		{
    		DebugLocation(66, 9);
    		Match("createlot"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__82"

    protected virtual void Enter_T__83() {}
    protected virtual void Leave_T__83() {}

    // $ANTLR start "T__83"
    [GrammarRule("T__83")]
    private void mT__83()
    {

    		try
    		{
    		int _type = T__83;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:67:7: ( 'CLOSELOT' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:67:9: 'CLOSELOT'
    		{
    		DebugLocation(67, 9);
    		Match("CLOSELOT"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__83"

    protected virtual void Enter_T__84() {}
    protected virtual void Leave_T__84() {}

    // $ANTLR start "T__84"
    [GrammarRule("T__84")]
    private void mT__84()
    {

    		try
    		{
    		int _type = T__84;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:68:7: ( 'CloseLot' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:68:9: 'CloseLot'
    		{
    		DebugLocation(68, 9);
    		Match("CloseLot"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__84"

    protected virtual void Enter_T__85() {}
    protected virtual void Leave_T__85() {}

    // $ANTLR start "T__85"
    [GrammarRule("T__85")]
    private void mT__85()
    {

    		try
    		{
    		int _type = T__85;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:69:7: ( 'closeLot' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:69:9: 'closeLot'
    		{
    		DebugLocation(69, 9);
    		Match("closeLot"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__85"

    protected virtual void Enter_T__86() {}
    protected virtual void Leave_T__86() {}

    // $ANTLR start "T__86"
    [GrammarRule("T__86")]
    private void mT__86()
    {

    		try
    		{
    		int _type = T__86;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:70:7: ( 'closelot' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:70:9: 'closelot'
    		{
    		DebugLocation(70, 9);
    		Match("closelot"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__86"

    protected virtual void Enter_T__87() {}
    protected virtual void Leave_T__87() {}

    // $ANTLR start "T__87"
    [GrammarRule("T__87")]
    private void mT__87()
    {

    		try
    		{
    		int _type = T__87;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:71:7: ( 'SPLITCOSTBASIS' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:71:9: 'SPLITCOSTBASIS'
    		{
    		DebugLocation(71, 9);
    		Match("SPLITCOSTBASIS"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__87"

    protected virtual void Enter_T__88() {}
    protected virtual void Leave_T__88() {}

    // $ANTLR start "T__88"
    [GrammarRule("T__88")]
    private void mT__88()
    {

    		try
    		{
    		int _type = T__88;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:72:7: ( 'SplitCostBasis' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:72:9: 'SplitCostBasis'
    		{
    		DebugLocation(72, 9);
    		Match("SplitCostBasis"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__88"

    protected virtual void Enter_T__89() {}
    protected virtual void Leave_T__89() {}

    // $ANTLR start "T__89"
    [GrammarRule("T__89")]
    private void mT__89()
    {

    		try
    		{
    		int _type = T__89;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:73:7: ( 'SplitCostbasis' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:73:9: 'SplitCostbasis'
    		{
    		DebugLocation(73, 9);
    		Match("SplitCostbasis"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__89"

    protected virtual void Enter_T__90() {}
    protected virtual void Leave_T__90() {}

    // $ANTLR start "T__90"
    [GrammarRule("T__90")]
    private void mT__90()
    {

    		try
    		{
    		int _type = T__90;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:74:7: ( 'splitCostBasis' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:74:9: 'splitCostBasis'
    		{
    		DebugLocation(74, 9);
    		Match("splitCostBasis"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__90"

    protected virtual void Enter_T__91() {}
    protected virtual void Leave_T__91() {}

    // $ANTLR start "T__91"
    [GrammarRule("T__91")]
    private void mT__91()
    {

    		try
    		{
    		int _type = T__91;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:75:7: ( 'splitCostbasis' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:75:9: 'splitCostbasis'
    		{
    		DebugLocation(75, 9);
    		Match("splitCostbasis"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__91"

    protected virtual void Enter_T__92() {}
    protected virtual void Leave_T__92() {}

    // $ANTLR start "T__92"
    [GrammarRule("T__92")]
    private void mT__92()
    {

    		try
    		{
    		int _type = T__92;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:76:7: ( 'splitcostbasis' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:76:9: 'splitcostbasis'
    		{
    		DebugLocation(76, 9);
    		Match("splitcostbasis"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__92"

    protected virtual void Enter_T__93() {}
    protected virtual void Leave_T__93() {}

    // $ANTLR start "T__93"
    [GrammarRule("T__93")]
    private void mT__93()
    {

    		try
    		{
    		int _type = T__93;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:77:7: ( 'FINDLAST' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:77:9: 'FINDLAST'
    		{
    		DebugLocation(77, 9);
    		Match("FINDLAST"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__93"

    protected virtual void Enter_T__94() {}
    protected virtual void Leave_T__94() {}

    // $ANTLR start "T__94"
    [GrammarRule("T__94")]
    private void mT__94()
    {

    		try
    		{
    		int _type = T__94;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:78:7: ( 'FindLast' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:78:9: 'FindLast'
    		{
    		DebugLocation(78, 9);
    		Match("FindLast"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__94"

    protected virtual void Enter_T__95() {}
    protected virtual void Leave_T__95() {}

    // $ANTLR start "T__95"
    [GrammarRule("T__95")]
    private void mT__95()
    {

    		try
    		{
    		int _type = T__95;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:79:7: ( 'Findlast' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:79:9: 'Findlast'
    		{
    		DebugLocation(79, 9);
    		Match("Findlast"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__95"

    protected virtual void Enter_T__96() {}
    protected virtual void Leave_T__96() {}

    // $ANTLR start "T__96"
    [GrammarRule("T__96")]
    private void mT__96()
    {

    		try
    		{
    		int _type = T__96;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:80:7: ( 'findLast' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:80:9: 'findLast'
    		{
    		DebugLocation(80, 9);
    		Match("findLast"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__96"

    protected virtual void Enter_T__97() {}
    protected virtual void Leave_T__97() {}

    // $ANTLR start "T__97"
    [GrammarRule("T__97")]
    private void mT__97()
    {

    		try
    		{
    		int _type = T__97;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:81:7: ( 'findlast' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:81:9: 'findlast'
    		{
    		DebugLocation(81, 9);
    		Match("findlast"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__97"

    protected virtual void Enter_T__98() {}
    protected virtual void Leave_T__98() {}

    // $ANTLR start "T__98"
    [GrammarRule("T__98")]
    private void mT__98()
    {

    		try
    		{
    		int _type = T__98;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:82:7: ( 'UPDATECASH' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:82:9: 'UPDATECASH'
    		{
    		DebugLocation(82, 9);
    		Match("UPDATECASH"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__98"

    protected virtual void Enter_T__99() {}
    protected virtual void Leave_T__99() {}

    // $ANTLR start "T__99"
    [GrammarRule("T__99")]
    private void mT__99()
    {

    		try
    		{
    		int _type = T__99;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:83:7: ( 'UpdateCash' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:83:9: 'UpdateCash'
    		{
    		DebugLocation(83, 9);
    		Match("UpdateCash"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__99"

    protected virtual void Enter_T__100() {}
    protected virtual void Leave_T__100() {}

    // $ANTLR start "T__100"
    [GrammarRule("T__100")]
    private void mT__100()
    {

    		try
    		{
    		int _type = T__100;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:84:8: ( 'updateCash' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:84:10: 'updateCash'
    		{
    		DebugLocation(84, 10);
    		Match("updateCash"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__100"

    protected virtual void Enter_T__101() {}
    protected virtual void Leave_T__101() {}

    // $ANTLR start "T__101"
    [GrammarRule("T__101")]
    private void mT__101()
    {

    		try
    		{
    		int _type = T__101;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:85:8: ( 'updatecash' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:85:10: 'updatecash'
    		{
    		DebugLocation(85, 10);
    		Match("updatecash"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__101"

    protected virtual void Enter_T__102() {}
    protected virtual void Leave_T__102() {}

    // $ANTLR start "T__102"
    [GrammarRule("T__102")]
    private void mT__102()
    {

    		try
    		{
    		int _type = T__102;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:86:8: ( 'INLINEQUERY' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:86:10: 'INLINEQUERY'
    		{
    		DebugLocation(86, 10);
    		Match("INLINEQUERY"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__102"

    protected virtual void Enter_T__103() {}
    protected virtual void Leave_T__103() {}

    // $ANTLR start "T__103"
    [GrammarRule("T__103")]
    private void mT__103()
    {

    		try
    		{
    		int _type = T__103;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:87:8: ( 'InlineQuery' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:87:10: 'InlineQuery'
    		{
    		DebugLocation(87, 10);
    		Match("InlineQuery"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__103"

    protected virtual void Enter_T__104() {}
    protected virtual void Leave_T__104() {}

    // $ANTLR start "T__104"
    [GrammarRule("T__104")]
    private void mT__104()
    {

    		try
    		{
    		int _type = T__104;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:88:8: ( 'inlineQuery' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:88:10: 'inlineQuery'
    		{
    		DebugLocation(88, 10);
    		Match("inlineQuery"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__104"

    protected virtual void Enter_T__105() {}
    protected virtual void Leave_T__105() {}

    // $ANTLR start "T__105"
    [GrammarRule("T__105")]
    private void mT__105()
    {

    		try
    		{
    		int _type = T__105;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:89:8: ( 'inlinequery' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:89:10: 'inlinequery'
    		{
    		DebugLocation(89, 10);
    		Match("inlinequery"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__105"

    protected virtual void Enter_T__106() {}
    protected virtual void Leave_T__106() {}

    // $ANTLR start "T__106"
    [GrammarRule("T__106")]
    private void mT__106()
    {

    		try
    		{
    		int _type = T__106;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:90:8: ( 'RULE' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:90:10: 'RULE'
    		{
    		DebugLocation(90, 10);
    		Match("RULE"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__106"

    protected virtual void Enter_T__107() {}
    protected virtual void Leave_T__107() {}

    // $ANTLR start "T__107"
    [GrammarRule("T__107")]
    private void mT__107()
    {

    		try
    		{
    		int _type = T__107;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:91:8: ( 'Rule' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:91:10: 'Rule'
    		{
    		DebugLocation(91, 10);
    		Match("Rule"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__107"

    protected virtual void Enter_T__108() {}
    protected virtual void Leave_T__108() {}

    // $ANTLR start "T__108"
    [GrammarRule("T__108")]
    private void mT__108()
    {

    		try
    		{
    		int _type = T__108;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:92:8: ( 'rule' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:92:10: 'rule'
    		{
    		DebugLocation(92, 10);
    		Match("rule"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__108"

    protected virtual void Enter_T__109() {}
    protected virtual void Leave_T__109() {}

    // $ANTLR start "T__109"
    [GrammarRule("T__109")]
    private void mT__109()
    {

    		try
    		{
    		int _type = T__109;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:93:8: ( 'MOVEASSETS' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:93:10: 'MOVEASSETS'
    		{
    		DebugLocation(93, 10);
    		Match("MOVEASSETS"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__109"

    protected virtual void Enter_T__110() {}
    protected virtual void Leave_T__110() {}

    // $ANTLR start "T__110"
    [GrammarRule("T__110")]
    private void mT__110()
    {

    		try
    		{
    		int _type = T__110;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:94:8: ( 'MoveAssets' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:94:10: 'MoveAssets'
    		{
    		DebugLocation(94, 10);
    		Match("MoveAssets"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__110"

    protected virtual void Enter_T__111() {}
    protected virtual void Leave_T__111() {}

    // $ANTLR start "T__111"
    [GrammarRule("T__111")]
    private void mT__111()
    {

    		try
    		{
    		int _type = T__111;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:95:8: ( 'moveAssets' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:95:10: 'moveAssets'
    		{
    		DebugLocation(95, 10);
    		Match("moveAssets"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__111"

    protected virtual void Enter_T__112() {}
    protected virtual void Leave_T__112() {}

    // $ANTLR start "T__112"
    [GrammarRule("T__112")]
    private void mT__112()
    {

    		try
    		{
    		int _type = T__112;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:96:8: ( 'moveassets' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:96:10: 'moveassets'
    		{
    		DebugLocation(96, 10);
    		Match("moveassets"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__112"

    protected virtual void Enter_T__113() {}
    protected virtual void Leave_T__113() {}

    // $ANTLR start "T__113"
    [GrammarRule("T__113")]
    private void mT__113()
    {

    		try
    		{
    		int _type = T__113;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:97:8: ( 'DISTRIBUTE' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:97:10: 'DISTRIBUTE'
    		{
    		DebugLocation(97, 10);
    		Match("DISTRIBUTE"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__113"

    protected virtual void Enter_T__114() {}
    protected virtual void Leave_T__114() {}

    // $ANTLR start "T__114"
    [GrammarRule("T__114")]
    private void mT__114()
    {

    		try
    		{
    		int _type = T__114;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:98:8: ( 'Distribute' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:98:10: 'Distribute'
    		{
    		DebugLocation(98, 10);
    		Match("Distribute"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__114"

    protected virtual void Enter_T__115() {}
    protected virtual void Leave_T__115() {}

    // $ANTLR start "T__115"
    [GrammarRule("T__115")]
    private void mT__115()
    {

    		try
    		{
    		int _type = T__115;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:99:8: ( 'distribute' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:99:10: 'distribute'
    		{
    		DebugLocation(99, 10);
    		Match("distribute"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__115"

    protected virtual void Enter_T__116() {}
    protected virtual void Leave_T__116() {}

    // $ANTLR start "T__116"
    [GrammarRule("T__116")]
    private void mT__116()
    {

    		try
    		{
    		int _type = T__116;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:100:8: ( '*' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:100:10: '*'
    		{
    		DebugLocation(100, 10);
    		Match('*'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__116"

    protected virtual void Enter_T__117() {}
    protected virtual void Leave_T__117() {}

    // $ANTLR start "T__117"
    [GrammarRule("T__117")]
    private void mT__117()
    {

    		try
    		{
    		int _type = T__117;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:101:8: ( '+' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:101:10: '+'
    		{
    		DebugLocation(101, 10);
    		Match('+'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__117"

    protected virtual void Enter_T__118() {}
    protected virtual void Leave_T__118() {}

    // $ANTLR start "T__118"
    [GrammarRule("T__118")]
    private void mT__118()
    {

    		try
    		{
    		int _type = T__118;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:102:8: ( '-' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:102:10: '-'
    		{
    		DebugLocation(102, 10);
    		Match('-'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__118"

    protected virtual void Enter_T__119() {}
    protected virtual void Leave_T__119() {}

    // $ANTLR start "T__119"
    [GrammarRule("T__119")]
    private void mT__119()
    {

    		try
    		{
    		int _type = T__119;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:103:8: ( '/' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:103:10: '/'
    		{
    		DebugLocation(103, 10);
    		Match('/'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__119"

    protected virtual void Enter_T__120() {}
    protected virtual void Leave_T__120() {}

    // $ANTLR start "T__120"
    [GrammarRule("T__120")]
    private void mT__120()
    {

    		try
    		{
    		int _type = T__120;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:104:8: ( '%' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:104:10: '%'
    		{
    		DebugLocation(104, 10);
    		Match('%'); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__120"

    protected virtual void Enter_T__121() {}
    protected virtual void Leave_T__121() {}

    // $ANTLR start "T__121"
    [GrammarRule("T__121")]
    private void mT__121()
    {

    		try
    		{
    		int _type = T__121;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:105:8: ( 'ABS' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:105:10: 'ABS'
    		{
    		DebugLocation(105, 10);
    		Match("ABS"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__121"

    protected virtual void Enter_T__122() {}
    protected virtual void Leave_T__122() {}

    // $ANTLR start "T__122"
    [GrammarRule("T__122")]
    private void mT__122()
    {

    		try
    		{
    		int _type = T__122;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:106:8: ( 'Abs' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:106:10: 'Abs'
    		{
    		DebugLocation(106, 10);
    		Match("Abs"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__122"

    protected virtual void Enter_T__123() {}
    protected virtual void Leave_T__123() {}

    // $ANTLR start "T__123"
    [GrammarRule("T__123")]
    private void mT__123()
    {

    		try
    		{
    		int _type = T__123;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:107:8: ( 'abs' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:107:10: 'abs'
    		{
    		DebugLocation(107, 10);
    		Match("abs"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__123"

    protected virtual void Enter_T__124() {}
    protected virtual void Leave_T__124() {}

    // $ANTLR start "T__124"
    [GrammarRule("T__124")]
    private void mT__124()
    {

    		try
    		{
    		int _type = T__124;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:108:8: ( 'DateTime' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:108:10: 'DateTime'
    		{
    		DebugLocation(108, 10);
    		Match("DateTime"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__124"

    protected virtual void Enter_T__125() {}
    protected virtual void Leave_T__125() {}

    // $ANTLR start "T__125"
    [GrammarRule("T__125")]
    private void mT__125()
    {

    		try
    		{
    		int _type = T__125;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:109:8: ( 'dateTime' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:109:10: 'dateTime'
    		{
    		DebugLocation(109, 10);
    		Match("dateTime"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__125"

    protected virtual void Enter_T__126() {}
    protected virtual void Leave_T__126() {}

    // $ANTLR start "T__126"
    [GrammarRule("T__126")]
    private void mT__126()
    {

    		try
    		{
    		int _type = T__126;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:110:8: ( 'DATE' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:110:10: 'DATE'
    		{
    		DebugLocation(110, 10);
    		Match("DATE"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__126"

    protected virtual void Enter_T__127() {}
    protected virtual void Leave_T__127() {}

    // $ANTLR start "T__127"
    [GrammarRule("T__127")]
    private void mT__127()
    {

    		try
    		{
    		int _type = T__127;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:111:8: ( 'Date' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:111:10: 'Date'
    		{
    		DebugLocation(111, 10);
    		Match("Date"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__127"

    protected virtual void Enter_T__128() {}
    protected virtual void Leave_T__128() {}

    // $ANTLR start "T__128"
    [GrammarRule("T__128")]
    private void mT__128()
    {

    		try
    		{
    		int _type = T__128;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:112:8: ( 'date' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:112:10: 'date'
    		{
    		DebugLocation(112, 10);
    		Match("date"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__128"

    protected virtual void Enter_T__129() {}
    protected virtual void Leave_T__129() {}

    // $ANTLR start "T__129"
    [GrammarRule("T__129")]
    private void mT__129()
    {

    		try
    		{
    		int _type = T__129;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:113:8: ( 'FMVPRICE' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:113:10: 'FMVPRICE'
    		{
    		DebugLocation(113, 10);
    		Match("FMVPRICE"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__129"

    protected virtual void Enter_T__130() {}
    protected virtual void Leave_T__130() {}

    // $ANTLR start "T__130"
    [GrammarRule("T__130")]
    private void mT__130()
    {

    		try
    		{
    		int _type = T__130;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:114:8: ( 'FmvPrice' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:114:10: 'FmvPrice'
    		{
    		DebugLocation(114, 10);
    		Match("FmvPrice"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__130"

    protected virtual void Enter_T__131() {}
    protected virtual void Leave_T__131() {}

    // $ANTLR start "T__131"
    [GrammarRule("T__131")]
    private void mT__131()
    {

    		try
    		{
    		int _type = T__131;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:115:8: ( 'Fmvprice' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:115:10: 'Fmvprice'
    		{
    		DebugLocation(115, 10);
    		Match("Fmvprice"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__131"

    protected virtual void Enter_T__132() {}
    protected virtual void Leave_T__132() {}

    // $ANTLR start "T__132"
    [GrammarRule("T__132")]
    private void mT__132()
    {

    		try
    		{
    		int _type = T__132;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:116:8: ( 'fmvPrice' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:116:10: 'fmvPrice'
    		{
    		DebugLocation(116, 10);
    		Match("fmvPrice"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__132"

    protected virtual void Enter_T__133() {}
    protected virtual void Leave_T__133() {}

    // $ANTLR start "T__133"
    [GrammarRule("T__133")]
    private void mT__133()
    {

    		try
    		{
    		int _type = T__133;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:117:8: ( 'fmvprice' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:117:10: 'fmvprice'
    		{
    		DebugLocation(117, 10);
    		Match("fmvprice"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__133"

    protected virtual void Enter_T__134() {}
    protected virtual void Leave_T__134() {}

    // $ANTLR start "T__134"
    [GrammarRule("T__134")]
    private void mT__134()
    {

    		try
    		{
    		int _type = T__134;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:118:8: ( 'MIN' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:118:10: 'MIN'
    		{
    		DebugLocation(118, 10);
    		Match("MIN"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__134"

    protected virtual void Enter_T__135() {}
    protected virtual void Leave_T__135() {}

    // $ANTLR start "T__135"
    [GrammarRule("T__135")]
    private void mT__135()
    {

    		try
    		{
    		int _type = T__135;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:119:8: ( 'Min' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:119:10: 'Min'
    		{
    		DebugLocation(119, 10);
    		Match("Min"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__135"

    protected virtual void Enter_T__136() {}
    protected virtual void Leave_T__136() {}

    // $ANTLR start "T__136"
    [GrammarRule("T__136")]
    private void mT__136()
    {

    		try
    		{
    		int _type = T__136;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:120:8: ( 'min' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:120:10: 'min'
    		{
    		DebugLocation(120, 10);
    		Match("min"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__136"

    protected virtual void Enter_T__137() {}
    protected virtual void Leave_T__137() {}

    // $ANTLR start "T__137"
    [GrammarRule("T__137")]
    private void mT__137()
    {

    		try
    		{
    		int _type = T__137;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:121:8: ( 'ROUND' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:121:10: 'ROUND'
    		{
    		DebugLocation(121, 10);
    		Match("ROUND"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__137"

    protected virtual void Enter_T__138() {}
    protected virtual void Leave_T__138() {}

    // $ANTLR start "T__138"
    [GrammarRule("T__138")]
    private void mT__138()
    {

    		try
    		{
    		int _type = T__138;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:122:8: ( 'Round' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:122:10: 'Round'
    		{
    		DebugLocation(122, 10);
    		Match("Round"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__138"

    protected virtual void Enter_T__139() {}
    protected virtual void Leave_T__139() {}

    // $ANTLR start "T__139"
    [GrammarRule("T__139")]
    private void mT__139()
    {

    		try
    		{
    		int _type = T__139;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:123:8: ( 'round' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:123:10: 'round'
    		{
    		DebugLocation(123, 10);
    		Match("round"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__139"

    protected virtual void Enter_T__140() {}
    protected virtual void Leave_T__140() {}

    // $ANTLR start "T__140"
    [GrammarRule("T__140")]
    private void mT__140()
    {

    		try
    		{
    		int _type = T__140;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:124:8: ( 'SUM' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:124:10: 'SUM'
    		{
    		DebugLocation(124, 10);
    		Match("SUM"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__140"

    protected virtual void Enter_T__141() {}
    protected virtual void Leave_T__141() {}

    // $ANTLR start "T__141"
    [GrammarRule("T__141")]
    private void mT__141()
    {

    		try
    		{
    		int _type = T__141;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:125:8: ( 'Sum' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:125:10: 'Sum'
    		{
    		DebugLocation(125, 10);
    		Match("Sum"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__141"

    protected virtual void Enter_T__142() {}
    protected virtual void Leave_T__142() {}

    // $ANTLR start "T__142"
    [GrammarRule("T__142")]
    private void mT__142()
    {

    		try
    		{
    		int _type = T__142;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:126:8: ( 'sum' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:126:10: 'sum'
    		{
    		DebugLocation(126, 10);
    		Match("sum"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__142"

    protected virtual void Enter_T__143() {}
    protected virtual void Leave_T__143() {}

    // $ANTLR start "T__143"
    [GrammarRule("T__143")]
    private void mT__143()
    {

    		try
    		{
    		int _type = T__143;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:127:8: ( 'FOUND' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:127:10: 'FOUND'
    		{
    		DebugLocation(127, 10);
    		Match("FOUND"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__143"

    protected virtual void Enter_T__144() {}
    protected virtual void Leave_T__144() {}

    // $ANTLR start "T__144"
    [GrammarRule("T__144")]
    private void mT__144()
    {

    		try
    		{
    		int _type = T__144;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:128:8: ( 'Found' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:128:10: 'Found'
    		{
    		DebugLocation(128, 10);
    		Match("Found"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__144"

    protected virtual void Enter_T__145() {}
    protected virtual void Leave_T__145() {}

    // $ANTLR start "T__145"
    [GrammarRule("T__145")]
    private void mT__145()
    {

    		try
    		{
    		int _type = T__145;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:129:8: ( 'found' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:129:10: 'found'
    		{
    		DebugLocation(129, 10);
    		Match("found"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__145"

    protected virtual void Enter_T__146() {}
    protected virtual void Leave_T__146() {}

    // $ANTLR start "T__146"
    [GrammarRule("T__146")]
    private void mT__146()
    {

    		try
    		{
    		int _type = T__146;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:130:8: ( 'DISTRIBUTED' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:130:10: 'DISTRIBUTED'
    		{
    		DebugLocation(130, 10);
    		Match("DISTRIBUTED"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__146"

    protected virtual void Enter_T__147() {}
    protected virtual void Leave_T__147() {}

    // $ANTLR start "T__147"
    [GrammarRule("T__147")]
    private void mT__147()
    {

    		try
    		{
    		int _type = T__147;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:131:8: ( 'Distributed' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:131:10: 'Distributed'
    		{
    		DebugLocation(131, 10);
    		Match("Distributed"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__147"

    protected virtual void Enter_T__148() {}
    protected virtual void Leave_T__148() {}

    // $ANTLR start "T__148"
    [GrammarRule("T__148")]
    private void mT__148()
    {

    		try
    		{
    		int _type = T__148;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:132:8: ( 'distributed' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:132:10: 'distributed'
    		{
    		DebugLocation(132, 10);
    		Match("distributed"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__148"

    protected virtual void Enter_T__149() {}
    protected virtual void Leave_T__149() {}

    // $ANTLR start "T__149"
    [GrammarRule("T__149")]
    private void mT__149()
    {

    		try
    		{
    		int _type = T__149;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:133:8: ( 'TIME' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:133:10: 'TIME'
    		{
    		DebugLocation(133, 10);
    		Match("TIME"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__149"

    protected virtual void Enter_T__150() {}
    protected virtual void Leave_T__150() {}

    // $ANTLR start "T__150"
    [GrammarRule("T__150")]
    private void mT__150()
    {

    		try
    		{
    		int _type = T__150;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:134:8: ( 'Time' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:134:10: 'Time'
    		{
    		DebugLocation(134, 10);
    		Match("Time"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__150"

    protected virtual void Enter_T__151() {}
    protected virtual void Leave_T__151() {}

    // $ANTLR start "T__151"
    [GrammarRule("T__151")]
    private void mT__151()
    {

    		try
    		{
    		int _type = T__151;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:135:8: ( 'time' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:135:10: 'time'
    		{
    		DebugLocation(135, 10);
    		Match("time"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__151"

    protected virtual void Enter_T__152() {}
    protected virtual void Leave_T__152() {}

    // $ANTLR start "T__152"
    [GrammarRule("T__152")]
    private void mT__152()
    {

    		try
    		{
    		int _type = T__152;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:136:8: ( 'NULL' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:136:10: 'NULL'
    		{
    		DebugLocation(136, 10);
    		Match("NULL"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__152"

    protected virtual void Enter_T__153() {}
    protected virtual void Leave_T__153() {}

    // $ANTLR start "T__153"
    [GrammarRule("T__153")]
    private void mT__153()
    {

    		try
    		{
    		int _type = T__153;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:137:8: ( 'Null' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:137:10: 'Null'
    		{
    		DebugLocation(137, 10);
    		Match("Null"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__153"

    protected virtual void Enter_T__154() {}
    protected virtual void Leave_T__154() {}

    // $ANTLR start "T__154"
    [GrammarRule("T__154")]
    private void mT__154()
    {

    		try
    		{
    		int _type = T__154;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:138:8: ( 'null' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:138:10: 'null'
    		{
    		DebugLocation(138, 10);
    		Match("null"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__154"

    protected virtual void Enter_T__155() {}
    protected virtual void Leave_T__155() {}

    // $ANTLR start "T__155"
    [GrammarRule("T__155")]
    private void mT__155()
    {

    		try
    		{
    		int _type = T__155;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:139:8: ( 'TRUE' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:139:10: 'TRUE'
    		{
    		DebugLocation(139, 10);
    		Match("TRUE"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__155"

    protected virtual void Enter_T__156() {}
    protected virtual void Leave_T__156() {}

    // $ANTLR start "T__156"
    [GrammarRule("T__156")]
    private void mT__156()
    {

    		try
    		{
    		int _type = T__156;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:140:8: ( 'True' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:140:10: 'True'
    		{
    		DebugLocation(140, 10);
    		Match("True"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__156"

    protected virtual void Enter_T__157() {}
    protected virtual void Leave_T__157() {}

    // $ANTLR start "T__157"
    [GrammarRule("T__157")]
    private void mT__157()
    {

    		try
    		{
    		int _type = T__157;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:141:8: ( 'true' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:141:10: 'true'
    		{
    		DebugLocation(141, 10);
    		Match("true"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__157"

    protected virtual void Enter_T__158() {}
    protected virtual void Leave_T__158() {}

    // $ANTLR start "T__158"
    [GrammarRule("T__158")]
    private void mT__158()
    {

    		try
    		{
    		int _type = T__158;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:142:8: ( 'FALSE' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:142:10: 'FALSE'
    		{
    		DebugLocation(142, 10);
    		Match("FALSE"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__158"

    protected virtual void Enter_T__159() {}
    protected virtual void Leave_T__159() {}

    // $ANTLR start "T__159"
    [GrammarRule("T__159")]
    private void mT__159()
    {

    		try
    		{
    		int _type = T__159;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:143:8: ( 'False' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:143:10: 'False'
    		{
    		DebugLocation(143, 10);
    		Match("False"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__159"

    protected virtual void Enter_T__160() {}
    protected virtual void Leave_T__160() {}

    // $ANTLR start "T__160"
    [GrammarRule("T__160")]
    private void mT__160()
    {

    		try
    		{
    		int _type = T__160;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:144:8: ( 'false' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:144:10: 'false'
    		{
    		DebugLocation(144, 10);
    		Match("false"); 


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "T__160"

    protected virtual void Enter_TRANSACT_FIELD() {}
    protected virtual void Leave_TRANSACT_FIELD() {}

    // $ANTLR start "TRANSACT_FIELD"
    [GrammarRule("TRANSACT_FIELD")]
    private void mTRANSACT_FIELD()
    {

    		try
    		{
    		int _type = TRANSACT_FIELD;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:370:2: ( ( 't' | 'T' | 'h' | 'H' | 'w' | 'W' | 'v' | 'V' ) '.' ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:370:4: ( 't' | 'T' | 'h' | 'H' | 'w' | 'W' | 'v' | 'V' ) '.' ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
    		{
    		DebugLocation(370, 4);
    		if (input.LA(1)=='H'||input.LA(1)=='T'||(input.LA(1)>='V' && input.LA(1)<='W')||input.LA(1)=='h'||input.LA(1)=='t'||(input.LA(1)>='v' && input.LA(1)<='w'))
    		{
    			input.Consume();

    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			Recover(mse);
    			throw mse;}

    		DebugLocation(370, 37);
    		Match('.'); 
    		DebugLocation(370, 40);
    		if ((input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z'))
    		{
    			input.Consume();

    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			Recover(mse);
    			throw mse;}

    		DebugLocation(370, 64);
    		// C:\\output\\HoldingRule.g:370:64: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
    		try { DebugEnterSubRule(1);
    		while (true)
    		{
    			int alt1=2;
    			try { DebugEnterDecision(1, decisionCanBacktrack[1]);
    			int LA1_0 = input.LA(1);

    			if (((LA1_0>='0' && LA1_0<='9')||(LA1_0>='A' && LA1_0<='Z')||LA1_0=='_'||(LA1_0>='a' && LA1_0<='z')))
    			{
    				alt1=1;
    			}


    			} finally { DebugExitDecision(1); }
    			switch ( alt1 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:
    				{
    				DebugLocation(370, 64);
    				if ((input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z'))
    				{
    					input.Consume();

    				}
    				else
    				{
    					MismatchedSetException mse = new MismatchedSetException(null,input);
    					DebugRecognitionException(mse);
    					Recover(mse);
    					throw mse;}


    				}
    				break;

    			default:
    				goto loop1;
    			}
    		}

    		loop1:
    			;

    		} finally { DebugExitSubRule(1); }


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "TRANSACT_FIELD"

    protected virtual void Enter_OR() {}
    protected virtual void Leave_OR() {}

    // $ANTLR start "OR"
    [GrammarRule("OR")]
    private void mOR()
    {

    		try
    		{
    		int _type = OR;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:379:2: ( 'OR' | 'Or' | 'or' | '||' )
    		int alt2=4;
    		try { DebugEnterDecision(2, decisionCanBacktrack[2]);
    		switch (input.LA(1))
    		{
    		case 'O':
    			{
    			int LA2_1 = input.LA(2);

    			if ((LA2_1=='R'))
    			{
    				alt2=1;
    			}
    			else if ((LA2_1=='r'))
    			{
    				alt2=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 2, 1, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    			}
    			break;
    		case 'o':
    			{
    			alt2=3;
    			}
    			break;
    		case '|':
    			{
    			alt2=4;
    			}
    			break;
    		default:
    			{
    				NoViableAltException nvae = new NoViableAltException("", 2, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}

    		} finally { DebugExitDecision(2); }
    		switch (alt2)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:379:4: 'OR'
    			{
    			DebugLocation(379, 4);
    			Match("OR"); 


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:379:9: 'Or'
    			{
    			DebugLocation(379, 9);
    			Match("Or"); 


    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:379:14: 'or'
    			{
    			DebugLocation(379, 14);
    			Match("or"); 


    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:380:4: '||'
    			{
    			DebugLocation(380, 4);
    			Match("||"); 


    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "OR"

    protected virtual void Enter_AND() {}
    protected virtual void Leave_AND() {}

    // $ANTLR start "AND"
    [GrammarRule("AND")]
    private void mAND()
    {

    		try
    		{
    		int _type = AND;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:384:2: ( 'AND' | 'And' | 'and' | '&&' )
    		int alt3=4;
    		try { DebugEnterDecision(3, decisionCanBacktrack[3]);
    		switch (input.LA(1))
    		{
    		case 'A':
    			{
    			int LA3_1 = input.LA(2);

    			if ((LA3_1=='N'))
    			{
    				alt3=1;
    			}
    			else if ((LA3_1=='n'))
    			{
    				alt3=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 3, 1, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    			}
    			break;
    		case 'a':
    			{
    			alt3=3;
    			}
    			break;
    		case '&':
    			{
    			alt3=4;
    			}
    			break;
    		default:
    			{
    				NoViableAltException nvae = new NoViableAltException("", 3, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}

    		} finally { DebugExitDecision(3); }
    		switch (alt3)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:384:4: 'AND'
    			{
    			DebugLocation(384, 4);
    			Match("AND"); 


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:384:10: 'And'
    			{
    			DebugLocation(384, 10);
    			Match("And"); 


    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:384:16: 'and'
    			{
    			DebugLocation(384, 16);
    			Match("and"); 


    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:385:4: '&&'
    			{
    			DebugLocation(385, 4);
    			Match("&&"); 


    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "AND"

    protected virtual void Enter_NOT() {}
    protected virtual void Leave_NOT() {}

    // $ANTLR start "NOT"
    [GrammarRule("NOT")]
    private void mNOT()
    {

    		try
    		{
    		int _type = NOT;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:389:2: ( 'NOT' | 'Not' | 'not' | '!' )
    		int alt4=4;
    		try { DebugEnterDecision(4, decisionCanBacktrack[4]);
    		switch (input.LA(1))
    		{
    		case 'N':
    			{
    			int LA4_1 = input.LA(2);

    			if ((LA4_1=='O'))
    			{
    				alt4=1;
    			}
    			else if ((LA4_1=='o'))
    			{
    				alt4=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 4, 1, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    			}
    			break;
    		case 'n':
    			{
    			alt4=3;
    			}
    			break;
    		case '!':
    			{
    			alt4=4;
    			}
    			break;
    		default:
    			{
    				NoViableAltException nvae = new NoViableAltException("", 4, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}

    		} finally { DebugExitDecision(4); }
    		switch (alt4)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:389:4: 'NOT'
    			{
    			DebugLocation(389, 4);
    			Match("NOT"); 


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:389:12: 'Not'
    			{
    			DebugLocation(389, 12);
    			Match("Not"); 


    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:389:20: 'not'
    			{
    			DebugLocation(389, 20);
    			Match("not"); 


    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:390:4: '!'
    			{
    			DebugLocation(390, 4);
    			Match('!'); 

    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "NOT"

    protected virtual void Enter_INT() {}
    protected virtual void Leave_INT() {}

    // $ANTLR start "INT"
    [GrammarRule("INT")]
    private void mINT()
    {

    		try
    		{
    		int _type = INT;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:394:2: ( ( '0' .. '9' )+ )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:394:4: ( '0' .. '9' )+
    		{
    		DebugLocation(394, 4);
    		// C:\\output\\HoldingRule.g:394:4: ( '0' .. '9' )+
    		int cnt5=0;
    		try { DebugEnterSubRule(5);
    		while (true)
    		{
    			int alt5=2;
    			try { DebugEnterDecision(5, decisionCanBacktrack[5]);
    			int LA5_0 = input.LA(1);

    			if (((LA5_0>='0' && LA5_0<='9')))
    			{
    				alt5=1;
    			}


    			} finally { DebugExitDecision(5); }
    			switch (alt5)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:394:5: '0' .. '9'
    				{
    				DebugLocation(394, 5);
    				MatchRange('0','9'); 

    				}
    				break;

    			default:
    				if (cnt5 >= 1)
    					goto loop5;

    				EarlyExitException eee5 = new EarlyExitException( 5, input );
    				DebugRecognitionException(eee5);
    				throw eee5;
    			}
    			cnt5++;
    		}
    		loop5:
    			;

    		} finally { DebugExitSubRule(5); }


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "INT"

    protected virtual void Enter_DEC() {}
    protected virtual void Leave_DEC() {}

    // $ANTLR start "DEC"
    [GrammarRule("DEC")]
    private void mDEC()
    {

    		try
    		{
    		int _type = DEC;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:398:2: ( '.' ( '0' .. '9' )+ | ( '0' .. '9' )+ '.' ( '0' .. '9' )* )
    		int alt9=2;
    		try { DebugEnterDecision(9, decisionCanBacktrack[9]);
    		int LA9_0 = input.LA(1);

    		if ((LA9_0=='.'))
    		{
    			alt9=1;
    		}
    		else if (((LA9_0>='0' && LA9_0<='9')))
    		{
    			alt9=2;
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 9, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(9); }
    		switch (alt9)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:398:4: '.' ( '0' .. '9' )+
    			{
    			DebugLocation(398, 4);
    			Match('.'); 
    			DebugLocation(398, 7);
    			// C:\\output\\HoldingRule.g:398:7: ( '0' .. '9' )+
    			int cnt6=0;
    			try { DebugEnterSubRule(6);
    			while (true)
    			{
    				int alt6=2;
    				try { DebugEnterDecision(6, decisionCanBacktrack[6]);
    				int LA6_0 = input.LA(1);

    				if (((LA6_0>='0' && LA6_0<='9')))
    				{
    					alt6=1;
    				}


    				} finally { DebugExitDecision(6); }
    				switch (alt6)
    				{
    				case 1:
    					DebugEnterAlt(1);
    					// C:\\output\\HoldingRule.g:398:8: '0' .. '9'
    					{
    					DebugLocation(398, 8);
    					MatchRange('0','9'); 

    					}
    					break;

    				default:
    					if (cnt6 >= 1)
    						goto loop6;

    					EarlyExitException eee6 = new EarlyExitException( 6, input );
    					DebugRecognitionException(eee6);
    					throw eee6;
    				}
    				cnt6++;
    			}
    			loop6:
    				;

    			} finally { DebugExitSubRule(6); }


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:399:4: ( '0' .. '9' )+ '.' ( '0' .. '9' )*
    			{
    			DebugLocation(399, 4);
    			// C:\\output\\HoldingRule.g:399:4: ( '0' .. '9' )+
    			int cnt7=0;
    			try { DebugEnterSubRule(7);
    			while (true)
    			{
    				int alt7=2;
    				try { DebugEnterDecision(7, decisionCanBacktrack[7]);
    				int LA7_0 = input.LA(1);

    				if (((LA7_0>='0' && LA7_0<='9')))
    				{
    					alt7=1;
    				}


    				} finally { DebugExitDecision(7); }
    				switch (alt7)
    				{
    				case 1:
    					DebugEnterAlt(1);
    					// C:\\output\\HoldingRule.g:399:5: '0' .. '9'
    					{
    					DebugLocation(399, 5);
    					MatchRange('0','9'); 

    					}
    					break;

    				default:
    					if (cnt7 >= 1)
    						goto loop7;

    					EarlyExitException eee7 = new EarlyExitException( 7, input );
    					DebugRecognitionException(eee7);
    					throw eee7;
    				}
    				cnt7++;
    			}
    			loop7:
    				;

    			} finally { DebugExitSubRule(7); }

    			DebugLocation(399, 15);
    			Match('.'); 
    			DebugLocation(399, 18);
    			// C:\\output\\HoldingRule.g:399:18: ( '0' .. '9' )*
    			try { DebugEnterSubRule(8);
    			while (true)
    			{
    				int alt8=2;
    				try { DebugEnterDecision(8, decisionCanBacktrack[8]);
    				int LA8_0 = input.LA(1);

    				if (((LA8_0>='0' && LA8_0<='9')))
    				{
    					alt8=1;
    				}


    				} finally { DebugExitDecision(8); }
    				switch ( alt8 )
    				{
    				case 1:
    					DebugEnterAlt(1);
    					// C:\\output\\HoldingRule.g:399:19: '0' .. '9'
    					{
    					DebugLocation(399, 19);
    					MatchRange('0','9'); 

    					}
    					break;

    				default:
    					goto loop8;
    				}
    			}

    			loop8:
    				;

    			} finally { DebugExitSubRule(8); }


    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "DEC"

    protected virtual void Enter_STRING() {}
    protected virtual void Leave_STRING() {}

    // $ANTLR start "STRING"
    [GrammarRule("STRING")]
    private void mSTRING()
    {

    		try
    		{
    		int _type = STRING;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:403:2: ( STRING1 | STRING2 )
    		int alt10=2;
    		try { DebugEnterDecision(10, decisionCanBacktrack[10]);
    		int LA10_0 = input.LA(1);

    		if ((LA10_0=='\''))
    		{
    			alt10=1;
    		}
    		else if ((LA10_0=='\"'))
    		{
    			alt10=2;
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 10, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(10); }
    		switch (alt10)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:403:4: STRING1
    			{
    			DebugLocation(403, 4);
    			mSTRING1(); 

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:404:4: STRING2
    			{
    			DebugLocation(404, 4);
    			mSTRING2(); 

    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "STRING"

    protected virtual void Enter_STRING1() {}
    protected virtual void Leave_STRING1() {}

    // $ANTLR start "STRING1"
    [GrammarRule("STRING1")]
    private void mSTRING1()
    {

    		try
    		{
    		// C:\\output\\HoldingRule.g:409:2: ( '\\'' ( STRINGCHAR1 )* '\\'' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:409:4: '\\'' ( STRINGCHAR1 )* '\\''
    		{
    		DebugLocation(409, 4);
    		Match('\''); 
    		DebugLocation(409, 9);
    		// C:\\output\\HoldingRule.g:409:9: ( STRINGCHAR1 )*
    		try { DebugEnterSubRule(11);
    		while (true)
    		{
    			int alt11=2;
    			try { DebugEnterDecision(11, decisionCanBacktrack[11]);
    			int LA11_0 = input.LA(1);

    			if ((LA11_0=='\''))
    			{
    				int LA11_1 = input.LA(2);

    				if ((LA11_1=='\''))
    				{
    					alt11=1;
    				}


    			}
    			else if (((LA11_0>='\u0000' && LA11_0<='&')||(LA11_0>='(' && LA11_0<='\uFFFF')))
    			{
    				alt11=1;
    			}


    			} finally { DebugExitDecision(11); }
    			switch ( alt11 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:409:9: STRINGCHAR1
    				{
    				DebugLocation(409, 9);
    				mSTRINGCHAR1(); 

    				}
    				break;

    			default:
    				goto loop11;
    			}
    		}

    		loop11:
    			;

    		} finally { DebugExitSubRule(11); }

    		DebugLocation(409, 22);
    		Match('\''); 

    		}

    	}
    	finally
    	{
        }
    }
    // $ANTLR end "STRING1"

    protected virtual void Enter_STRING2() {}
    protected virtual void Leave_STRING2() {}

    // $ANTLR start "STRING2"
    [GrammarRule("STRING2")]
    private void mSTRING2()
    {

    		try
    		{
    		// C:\\output\\HoldingRule.g:413:2: ( '\"' ( STRINGCHAR2 )* '\"' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:413:4: '\"' ( STRINGCHAR2 )* '\"'
    		{
    		DebugLocation(413, 4);
    		Match('\"'); 
    		DebugLocation(413, 8);
    		// C:\\output\\HoldingRule.g:413:8: ( STRINGCHAR2 )*
    		try { DebugEnterSubRule(12);
    		while (true)
    		{
    			int alt12=2;
    			try { DebugEnterDecision(12, decisionCanBacktrack[12]);
    			int LA12_0 = input.LA(1);

    			if ((LA12_0=='\"'))
    			{
    				int LA12_1 = input.LA(2);

    				if ((LA12_1=='\"'))
    				{
    					alt12=1;
    				}


    			}
    			else if (((LA12_0>='\u0000' && LA12_0<='!')||(LA12_0>='#' && LA12_0<='\uFFFF')))
    			{
    				alt12=1;
    			}


    			} finally { DebugExitDecision(12); }
    			switch ( alt12 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:413:8: STRINGCHAR2
    				{
    				DebugLocation(413, 8);
    				mSTRINGCHAR2(); 

    				}
    				break;

    			default:
    				goto loop12;
    			}
    		}

    		loop12:
    			;

    		} finally { DebugExitSubRule(12); }

    		DebugLocation(413, 21);
    		Match('\"'); 

    		}

    	}
    	finally
    	{
        }
    }
    // $ANTLR end "STRING2"

    protected virtual void Enter_STRINGCHAR1() {}
    protected virtual void Leave_STRINGCHAR1() {}

    // $ANTLR start "STRINGCHAR1"
    [GrammarRule("STRINGCHAR1")]
    private void mSTRINGCHAR1()
    {

    		try
    		{
    		// C:\\output\\HoldingRule.g:417:2: (~ '\\'' | '\\'\\'' )
    		int alt13=2;
    		try { DebugEnterDecision(13, decisionCanBacktrack[13]);
    		int LA13_0 = input.LA(1);

    		if (((LA13_0>='\u0000' && LA13_0<='&')||(LA13_0>='(' && LA13_0<='\uFFFF')))
    		{
    			alt13=1;
    		}
    		else if ((LA13_0=='\''))
    		{
    			alt13=2;
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 13, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(13); }
    		switch (alt13)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:417:4: ~ '\\''
    			{
    			DebugLocation(417, 4);
    			if ((input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='\uFFFF'))
    			{
    				input.Consume();

    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				Recover(mse);
    				throw mse;}


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:418:4: '\\'\\''
    			{
    			DebugLocation(418, 4);
    			Match("''"); 


    			}
    			break;

    		}
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "STRINGCHAR1"

    protected virtual void Enter_STRINGCHAR2() {}
    protected virtual void Leave_STRINGCHAR2() {}

    // $ANTLR start "STRINGCHAR2"
    [GrammarRule("STRINGCHAR2")]
    private void mSTRINGCHAR2()
    {

    		try
    		{
    		// C:\\output\\HoldingRule.g:422:2: (~ '\"' | '\"\"' )
    		int alt14=2;
    		try { DebugEnterDecision(14, decisionCanBacktrack[14]);
    		int LA14_0 = input.LA(1);

    		if (((LA14_0>='\u0000' && LA14_0<='!')||(LA14_0>='#' && LA14_0<='\uFFFF')))
    		{
    			alt14=1;
    		}
    		else if ((LA14_0=='\"'))
    		{
    			alt14=2;
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 14, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(14); }
    		switch (alt14)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:422:4: ~ '\"'
    			{
    			DebugLocation(422, 4);
    			if ((input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='\uFFFF'))
    			{
    				input.Consume();

    			}
    			else
    			{
    				MismatchedSetException mse = new MismatchedSetException(null,input);
    				DebugRecognitionException(mse);
    				Recover(mse);
    				throw mse;}


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:423:4: '\"\"'
    			{
    			DebugLocation(423, 4);
    			Match("\"\""); 


    			}
    			break;

    		}
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "STRINGCHAR2"

    protected virtual void Enter_COMPARE() {}
    protected virtual void Leave_COMPARE() {}

    // $ANTLR start "COMPARE"
    [GrammarRule("COMPARE")]
    private void mCOMPARE()
    {

    		try
    		{
    		int _type = COMPARE;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:427:2: ( '==' | '!=' | '<>' | '<=' | '<' | '>=' | '>' )
    		int alt15=7;
    		try { DebugEnterDecision(15, decisionCanBacktrack[15]);
    		try
    		{
    			alt15 = dfa15.Predict(input);
    		}
    		catch (NoViableAltException nvae)
    		{
    			DebugRecognitionException(nvae);
    			throw;
    		}
    		} finally { DebugExitDecision(15); }
    		switch (alt15)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:427:4: '=='
    			{
    			DebugLocation(427, 4);
    			Match("=="); 


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:427:11: '!='
    			{
    			DebugLocation(427, 11);
    			Match("!="); 


    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:427:18: '<>'
    			{
    			DebugLocation(427, 18);
    			Match("<>"); 


    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:427:25: '<='
    			{
    			DebugLocation(427, 25);
    			Match("<="); 


    			}
    			break;
    		case 5:
    			DebugEnterAlt(5);
    			// C:\\output\\HoldingRule.g:427:32: '<'
    			{
    			DebugLocation(427, 32);
    			Match('<'); 

    			}
    			break;
    		case 6:
    			DebugEnterAlt(6);
    			// C:\\output\\HoldingRule.g:427:38: '>='
    			{
    			DebugLocation(427, 38);
    			Match(">="); 


    			}
    			break;
    		case 7:
    			DebugEnterAlt(7);
    			// C:\\output\\HoldingRule.g:427:45: '>'
    			{
    			DebugLocation(427, 45);
    			Match('>'); 

    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "COMPARE"

    protected virtual void Enter_IN() {}
    protected virtual void Leave_IN() {}

    // $ANTLR start "IN"
    [GrammarRule("IN")]
    private void mIN()
    {

    		try
    		{
    		int _type = IN;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:431:2: ( 'IN' | 'In' | 'in' )
    		int alt16=3;
    		try { DebugEnterDecision(16, decisionCanBacktrack[16]);
    		int LA16_0 = input.LA(1);

    		if ((LA16_0=='I'))
    		{
    			int LA16_1 = input.LA(2);

    			if ((LA16_1=='N'))
    			{
    				alt16=1;
    			}
    			else if ((LA16_1=='n'))
    			{
    				alt16=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 16, 1, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}
    		else if ((LA16_0=='i'))
    		{
    			alt16=3;
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 16, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(16); }
    		switch (alt16)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:431:4: 'IN'
    			{
    			DebugLocation(431, 4);
    			Match("IN"); 


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:431:9: 'In'
    			{
    			DebugLocation(431, 9);
    			Match("In"); 


    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:431:14: 'in'
    			{
    			DebugLocation(431, 14);
    			Match("in"); 


    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "IN"

    protected virtual void Enter_CONTAINS() {}
    protected virtual void Leave_CONTAINS() {}

    // $ANTLR start "CONTAINS"
    [GrammarRule("CONTAINS")]
    private void mCONTAINS()
    {

    		try
    		{
    		int _type = CONTAINS;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:435:2: ( 'CONTAINS' | 'Contains' | 'contains' )
    		int alt17=3;
    		try { DebugEnterDecision(17, decisionCanBacktrack[17]);
    		int LA17_0 = input.LA(1);

    		if ((LA17_0=='C'))
    		{
    			int LA17_1 = input.LA(2);

    			if ((LA17_1=='O'))
    			{
    				alt17=1;
    			}
    			else if ((LA17_1=='o'))
    			{
    				alt17=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 17, 1, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}
    		else if ((LA17_0=='c'))
    		{
    			alt17=3;
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 17, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(17); }
    		switch (alt17)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:435:4: 'CONTAINS'
    			{
    			DebugLocation(435, 4);
    			Match("CONTAINS"); 


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:435:15: 'Contains'
    			{
    			DebugLocation(435, 15);
    			Match("Contains"); 


    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:435:26: 'contains'
    			{
    			DebugLocation(435, 26);
    			Match("contains"); 


    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "CONTAINS"

    protected virtual void Enter_EQUAL() {}
    protected virtual void Leave_EQUAL() {}

    // $ANTLR start "EQUAL"
    [GrammarRule("EQUAL")]
    private void mEQUAL()
    {

    		try
    		{
    		int _type = EQUAL;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:439:2: ( '=' )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:439:4: '='
    		{
    		DebugLocation(439, 4);
    		Match('='); 

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "EQUAL"

    protected virtual void Enter_OPEQUAL() {}
    protected virtual void Leave_OPEQUAL() {}

    // $ANTLR start "OPEQUAL"
    [GrammarRule("OPEQUAL")]
    private void mOPEQUAL()
    {

    		try
    		{
    		int _type = OPEQUAL;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:443:2: ( '+=' | '-=' | '*=' | '/=' | '%=' )
    		int alt18=5;
    		try { DebugEnterDecision(18, decisionCanBacktrack[18]);
    		switch (input.LA(1))
    		{
    		case '+':
    			{
    			alt18=1;
    			}
    			break;
    		case '-':
    			{
    			alt18=2;
    			}
    			break;
    		case '*':
    			{
    			alt18=3;
    			}
    			break;
    		case '/':
    			{
    			alt18=4;
    			}
    			break;
    		case '%':
    			{
    			alt18=5;
    			}
    			break;
    		default:
    			{
    				NoViableAltException nvae = new NoViableAltException("", 18, 0, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}

    		} finally { DebugExitDecision(18); }
    		switch (alt18)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:443:4: '+='
    			{
    			DebugLocation(443, 4);
    			Match("+="); 


    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:443:11: '-='
    			{
    			DebugLocation(443, 11);
    			Match("-="); 


    			}
    			break;
    		case 3:
    			DebugEnterAlt(3);
    			// C:\\output\\HoldingRule.g:443:18: '*='
    			{
    			DebugLocation(443, 18);
    			Match("*="); 


    			}
    			break;
    		case 4:
    			DebugEnterAlt(4);
    			// C:\\output\\HoldingRule.g:443:25: '/='
    			{
    			DebugLocation(443, 25);
    			Match("/="); 


    			}
    			break;
    		case 5:
    			DebugEnterAlt(5);
    			// C:\\output\\HoldingRule.g:443:32: '%='
    			{
    			DebugLocation(443, 32);
    			Match("%="); 


    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "OPEQUAL"

    protected virtual void Enter_IDENTIFIER() {}
    protected virtual void Leave_IDENTIFIER() {}

    // $ANTLR start "IDENTIFIER"
    [GrammarRule("IDENTIFIER")]
    private void mIDENTIFIER()
    {

    		try
    		{
    		int _type = IDENTIFIER;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:456:2: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:456:4: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
    		{
    		DebugLocation(456, 4);
    		if ((input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z'))
    		{
    			input.Consume();

    		}
    		else
    		{
    			MismatchedSetException mse = new MismatchedSetException(null,input);
    			DebugRecognitionException(mse);
    			Recover(mse);
    			throw mse;}

    		DebugLocation(456, 28);
    		// C:\\output\\HoldingRule.g:456:28: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
    		try { DebugEnterSubRule(19);
    		while (true)
    		{
    			int alt19=2;
    			try { DebugEnterDecision(19, decisionCanBacktrack[19]);
    			int LA19_0 = input.LA(1);

    			if (((LA19_0>='0' && LA19_0<='9')||(LA19_0>='A' && LA19_0<='Z')||LA19_0=='_'||(LA19_0>='a' && LA19_0<='z')))
    			{
    				alt19=1;
    			}


    			} finally { DebugExitDecision(19); }
    			switch ( alt19 )
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:
    				{
    				DebugLocation(456, 28);
    				if ((input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z'))
    				{
    					input.Consume();

    				}
    				else
    				{
    					MismatchedSetException mse = new MismatchedSetException(null,input);
    					DebugRecognitionException(mse);
    					Recover(mse);
    					throw mse;}


    				}
    				break;

    			default:
    				goto loop19;
    			}
    		}

    		loop19:
    			;

    		} finally { DebugExitSubRule(19); }


    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "IDENTIFIER"

    protected virtual void Enter_WS() {}
    protected virtual void Leave_WS() {}

    // $ANTLR start "WS"
    [GrammarRule("WS")]
    private void mWS()
    {

    		try
    		{
    		int _type = WS;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:461:2: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:461:4: ( ' ' | '\\t' | '\\n' | '\\r' )+
    		{
    		DebugLocation(461, 4);
    		// C:\\output\\HoldingRule.g:461:4: ( ' ' | '\\t' | '\\n' | '\\r' )+
    		int cnt20=0;
    		try { DebugEnterSubRule(20);
    		while (true)
    		{
    			int alt20=2;
    			try { DebugEnterDecision(20, decisionCanBacktrack[20]);
    			int LA20_0 = input.LA(1);

    			if (((LA20_0>='\t' && LA20_0<='\n')||LA20_0=='\r'||LA20_0==' '))
    			{
    				alt20=1;
    			}


    			} finally { DebugExitDecision(20); }
    			switch (alt20)
    			{
    			case 1:
    				DebugEnterAlt(1);
    				// C:\\output\\HoldingRule.g:
    				{
    				DebugLocation(461, 4);
    				if ((input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ')
    				{
    					input.Consume();

    				}
    				else
    				{
    					MismatchedSetException mse = new MismatchedSetException(null,input);
    					DebugRecognitionException(mse);
    					Recover(mse);
    					throw mse;}


    				}
    				break;

    			default:
    				if (cnt20 >= 1)
    					goto loop20;

    				EarlyExitException eee20 = new EarlyExitException( 20, input );
    				DebugRecognitionException(eee20);
    				throw eee20;
    			}
    			cnt20++;
    		}
    		loop20:
    			;

    		} finally { DebugExitSubRule(20); }

    		DebugLocation(461, 26);
    		_channel=Hidden;

    		}

    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "WS"

    protected virtual void Enter_COMMENTS() {}
    protected virtual void Leave_COMMENTS() {}

    // $ANTLR start "COMMENTS"
    [GrammarRule("COMMENTS")]
    private void mCOMMENTS()
    {

    		try
    		{
    		int _type = COMMENTS;
    		int _channel = DefaultTokenChannel;
    		// C:\\output\\HoldingRule.g:465:2: ( '//' (~ '\\n' )* '\\n' | '/*' ( options {greedy=false; } : . )* '*/' )
    		int alt23=2;
    		try { DebugEnterDecision(23, decisionCanBacktrack[23]);
    		int LA23_0 = input.LA(1);

    		if ((LA23_0=='/'))
    		{
    			int LA23_1 = input.LA(2);

    			if ((LA23_1=='/'))
    			{
    				alt23=1;
    			}
    			else if ((LA23_1=='*'))
    			{
    				alt23=2;
    			}
    			else
    			{
    				NoViableAltException nvae = new NoViableAltException("", 23, 1, input);

    				DebugRecognitionException(nvae);
    				throw nvae;
    			}
    		}
    		else
    		{
    			NoViableAltException nvae = new NoViableAltException("", 23, 0, input);

    			DebugRecognitionException(nvae);
    			throw nvae;
    		}
    		} finally { DebugExitDecision(23); }
    		switch (alt23)
    		{
    		case 1:
    			DebugEnterAlt(1);
    			// C:\\output\\HoldingRule.g:465:4: '//' (~ '\\n' )* '\\n'
    			{
    			DebugLocation(465, 4);
    			Match("//"); 

    			DebugLocation(465, 9);
    			// C:\\output\\HoldingRule.g:465:9: (~ '\\n' )*
    			try { DebugEnterSubRule(21);
    			while (true)
    			{
    				int alt21=2;
    				try { DebugEnterDecision(21, decisionCanBacktrack[21]);
    				int LA21_0 = input.LA(1);

    				if (((LA21_0>='\u0000' && LA21_0<='\t')||(LA21_0>='\u000B' && LA21_0<='\uFFFF')))
    				{
    					alt21=1;
    				}


    				} finally { DebugExitDecision(21); }
    				switch ( alt21 )
    				{
    				case 1:
    					DebugEnterAlt(1);
    					// C:\\output\\HoldingRule.g:465:10: ~ '\\n'
    					{
    					DebugLocation(465, 10);
    					if ((input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\uFFFF'))
    					{
    						input.Consume();

    					}
    					else
    					{
    						MismatchedSetException mse = new MismatchedSetException(null,input);
    						DebugRecognitionException(mse);
    						Recover(mse);
    						throw mse;}


    					}
    					break;

    				default:
    					goto loop21;
    				}
    			}

    			loop21:
    				;

    			} finally { DebugExitSubRule(21); }

    			DebugLocation(465, 18);
    			Match('\n'); 
    			DebugLocation(465, 23);
    			_channel=Hidden;

    			}
    			break;
    		case 2:
    			DebugEnterAlt(2);
    			// C:\\output\\HoldingRule.g:466:4: '/*' ( options {greedy=false; } : . )* '*/'
    			{
    			DebugLocation(466, 4);
    			Match("/*"); 

    			DebugLocation(466, 9);
    			// C:\\output\\HoldingRule.g:466:9: ( options {greedy=false; } : . )*
    			try { DebugEnterSubRule(22);
    			while (true)
    			{
    				int alt22=2;
    				try { DebugEnterDecision(22, decisionCanBacktrack[22]);
    				int LA22_0 = input.LA(1);

    				if ((LA22_0=='*'))
    				{
    					int LA22_1 = input.LA(2);

    					if ((LA22_1=='/'))
    					{
    						alt22=2;
    					}
    					else if (((LA22_1>='\u0000' && LA22_1<='.')||(LA22_1>='0' && LA22_1<='\uFFFF')))
    					{
    						alt22=1;
    					}


    				}
    				else if (((LA22_0>='\u0000' && LA22_0<=')')||(LA22_0>='+' && LA22_0<='\uFFFF')))
    				{
    					alt22=1;
    				}


    				} finally { DebugExitDecision(22); }
    				switch ( alt22 )
    				{
    				case 1:
    					DebugEnterAlt(1);
    					// C:\\output\\HoldingRule.g:466:36: .
    					{
    					DebugLocation(466, 36);
    					MatchAny(); 

    					}
    					break;

    				default:
    					goto loop22;
    				}
    			}

    			loop22:
    				;

    			} finally { DebugExitSubRule(22); }

    			DebugLocation(466, 40);
    			Match("*/"); 

    			DebugLocation(466, 45);
    			_channel=Hidden;

    			}
    			break;

    		}
    		state.type = _type;
    		state.channel = _channel;
    	}
    	finally
    	{
        }
    }
    // $ANTLR end "COMMENTS"

    public override void mTokens()
    {
    	// C:\\output\\HoldingRule.g:1:8: ( T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | T__116 | T__117 | T__118 | T__119 | T__120 | T__121 | T__122 | T__123 | T__124 | T__125 | T__126 | T__127 | T__128 | T__129 | T__130 | T__131 | T__132 | T__133 | T__134 | T__135 | T__136 | T__137 | T__138 | T__139 | T__140 | T__141 | T__142 | T__143 | T__144 | T__145 | T__146 | T__147 | T__148 | T__149 | T__150 | T__151 | T__152 | T__153 | T__154 | T__155 | T__156 | T__157 | T__158 | T__159 | T__160 | TRANSACT_FIELD | OR | AND | NOT | INT | DEC | STRING | COMPARE | IN | CONTAINS | EQUAL | OPEQUAL | IDENTIFIER | WS | COMMENTS )
    	int alt24=153;
    	try { DebugEnterDecision(24, decisionCanBacktrack[24]);
    	try
    	{
    		alt24 = dfa24.Predict(input);
    	}
    	catch (NoViableAltException nvae)
    	{
    		DebugRecognitionException(nvae);
    		throw;
    	}
    	} finally { DebugExitDecision(24); }
    	switch (alt24)
    	{
    	case 1:
    		DebugEnterAlt(1);
    		// C:\\output\\HoldingRule.g:1:10: T__23
    		{
    		DebugLocation(1, 10);
    		mT__23(); 

    		}
    		break;
    	case 2:
    		DebugEnterAlt(2);
    		// C:\\output\\HoldingRule.g:1:16: T__24
    		{
    		DebugLocation(1, 16);
    		mT__24(); 

    		}
    		break;
    	case 3:
    		DebugEnterAlt(3);
    		// C:\\output\\HoldingRule.g:1:22: T__25
    		{
    		DebugLocation(1, 22);
    		mT__25(); 

    		}
    		break;
    	case 4:
    		DebugEnterAlt(4);
    		// C:\\output\\HoldingRule.g:1:28: T__26
    		{
    		DebugLocation(1, 28);
    		mT__26(); 

    		}
    		break;
    	case 5:
    		DebugEnterAlt(5);
    		// C:\\output\\HoldingRule.g:1:34: T__27
    		{
    		DebugLocation(1, 34);
    		mT__27(); 

    		}
    		break;
    	case 6:
    		DebugEnterAlt(6);
    		// C:\\output\\HoldingRule.g:1:40: T__28
    		{
    		DebugLocation(1, 40);
    		mT__28(); 

    		}
    		break;
    	case 7:
    		DebugEnterAlt(7);
    		// C:\\output\\HoldingRule.g:1:46: T__29
    		{
    		DebugLocation(1, 46);
    		mT__29(); 

    		}
    		break;
    	case 8:
    		DebugEnterAlt(8);
    		// C:\\output\\HoldingRule.g:1:52: T__30
    		{
    		DebugLocation(1, 52);
    		mT__30(); 

    		}
    		break;
    	case 9:
    		DebugEnterAlt(9);
    		// C:\\output\\HoldingRule.g:1:58: T__31
    		{
    		DebugLocation(1, 58);
    		mT__31(); 

    		}
    		break;
    	case 10:
    		DebugEnterAlt(10);
    		// C:\\output\\HoldingRule.g:1:64: T__32
    		{
    		DebugLocation(1, 64);
    		mT__32(); 

    		}
    		break;
    	case 11:
    		DebugEnterAlt(11);
    		// C:\\output\\HoldingRule.g:1:70: T__33
    		{
    		DebugLocation(1, 70);
    		mT__33(); 

    		}
    		break;
    	case 12:
    		DebugEnterAlt(12);
    		// C:\\output\\HoldingRule.g:1:76: T__34
    		{
    		DebugLocation(1, 76);
    		mT__34(); 

    		}
    		break;
    	case 13:
    		DebugEnterAlt(13);
    		// C:\\output\\HoldingRule.g:1:82: T__35
    		{
    		DebugLocation(1, 82);
    		mT__35(); 

    		}
    		break;
    	case 14:
    		DebugEnterAlt(14);
    		// C:\\output\\HoldingRule.g:1:88: T__36
    		{
    		DebugLocation(1, 88);
    		mT__36(); 

    		}
    		break;
    	case 15:
    		DebugEnterAlt(15);
    		// C:\\output\\HoldingRule.g:1:94: T__37
    		{
    		DebugLocation(1, 94);
    		mT__37(); 

    		}
    		break;
    	case 16:
    		DebugEnterAlt(16);
    		// C:\\output\\HoldingRule.g:1:100: T__38
    		{
    		DebugLocation(1, 100);
    		mT__38(); 

    		}
    		break;
    	case 17:
    		DebugEnterAlt(17);
    		// C:\\output\\HoldingRule.g:1:106: T__39
    		{
    		DebugLocation(1, 106);
    		mT__39(); 

    		}
    		break;
    	case 18:
    		DebugEnterAlt(18);
    		// C:\\output\\HoldingRule.g:1:112: T__40
    		{
    		DebugLocation(1, 112);
    		mT__40(); 

    		}
    		break;
    	case 19:
    		DebugEnterAlt(19);
    		// C:\\output\\HoldingRule.g:1:118: T__41
    		{
    		DebugLocation(1, 118);
    		mT__41(); 

    		}
    		break;
    	case 20:
    		DebugEnterAlt(20);
    		// C:\\output\\HoldingRule.g:1:124: T__42
    		{
    		DebugLocation(1, 124);
    		mT__42(); 

    		}
    		break;
    	case 21:
    		DebugEnterAlt(21);
    		// C:\\output\\HoldingRule.g:1:130: T__43
    		{
    		DebugLocation(1, 130);
    		mT__43(); 

    		}
    		break;
    	case 22:
    		DebugEnterAlt(22);
    		// C:\\output\\HoldingRule.g:1:136: T__44
    		{
    		DebugLocation(1, 136);
    		mT__44(); 

    		}
    		break;
    	case 23:
    		DebugEnterAlt(23);
    		// C:\\output\\HoldingRule.g:1:142: T__45
    		{
    		DebugLocation(1, 142);
    		mT__45(); 

    		}
    		break;
    	case 24:
    		DebugEnterAlt(24);
    		// C:\\output\\HoldingRule.g:1:148: T__46
    		{
    		DebugLocation(1, 148);
    		mT__46(); 

    		}
    		break;
    	case 25:
    		DebugEnterAlt(25);
    		// C:\\output\\HoldingRule.g:1:154: T__47
    		{
    		DebugLocation(1, 154);
    		mT__47(); 

    		}
    		break;
    	case 26:
    		DebugEnterAlt(26);
    		// C:\\output\\HoldingRule.g:1:160: T__48
    		{
    		DebugLocation(1, 160);
    		mT__48(); 

    		}
    		break;
    	case 27:
    		DebugEnterAlt(27);
    		// C:\\output\\HoldingRule.g:1:166: T__49
    		{
    		DebugLocation(1, 166);
    		mT__49(); 

    		}
    		break;
    	case 28:
    		DebugEnterAlt(28);
    		// C:\\output\\HoldingRule.g:1:172: T__50
    		{
    		DebugLocation(1, 172);
    		mT__50(); 

    		}
    		break;
    	case 29:
    		DebugEnterAlt(29);
    		// C:\\output\\HoldingRule.g:1:178: T__51
    		{
    		DebugLocation(1, 178);
    		mT__51(); 

    		}
    		break;
    	case 30:
    		DebugEnterAlt(30);
    		// C:\\output\\HoldingRule.g:1:184: T__52
    		{
    		DebugLocation(1, 184);
    		mT__52(); 

    		}
    		break;
    	case 31:
    		DebugEnterAlt(31);
    		// C:\\output\\HoldingRule.g:1:190: T__53
    		{
    		DebugLocation(1, 190);
    		mT__53(); 

    		}
    		break;
    	case 32:
    		DebugEnterAlt(32);
    		// C:\\output\\HoldingRule.g:1:196: T__54
    		{
    		DebugLocation(1, 196);
    		mT__54(); 

    		}
    		break;
    	case 33:
    		DebugEnterAlt(33);
    		// C:\\output\\HoldingRule.g:1:202: T__55
    		{
    		DebugLocation(1, 202);
    		mT__55(); 

    		}
    		break;
    	case 34:
    		DebugEnterAlt(34);
    		// C:\\output\\HoldingRule.g:1:208: T__56
    		{
    		DebugLocation(1, 208);
    		mT__56(); 

    		}
    		break;
    	case 35:
    		DebugEnterAlt(35);
    		// C:\\output\\HoldingRule.g:1:214: T__57
    		{
    		DebugLocation(1, 214);
    		mT__57(); 

    		}
    		break;
    	case 36:
    		DebugEnterAlt(36);
    		// C:\\output\\HoldingRule.g:1:220: T__58
    		{
    		DebugLocation(1, 220);
    		mT__58(); 

    		}
    		break;
    	case 37:
    		DebugEnterAlt(37);
    		// C:\\output\\HoldingRule.g:1:226: T__59
    		{
    		DebugLocation(1, 226);
    		mT__59(); 

    		}
    		break;
    	case 38:
    		DebugEnterAlt(38);
    		// C:\\output\\HoldingRule.g:1:232: T__60
    		{
    		DebugLocation(1, 232);
    		mT__60(); 

    		}
    		break;
    	case 39:
    		DebugEnterAlt(39);
    		// C:\\output\\HoldingRule.g:1:238: T__61
    		{
    		DebugLocation(1, 238);
    		mT__61(); 

    		}
    		break;
    	case 40:
    		DebugEnterAlt(40);
    		// C:\\output\\HoldingRule.g:1:244: T__62
    		{
    		DebugLocation(1, 244);
    		mT__62(); 

    		}
    		break;
    	case 41:
    		DebugEnterAlt(41);
    		// C:\\output\\HoldingRule.g:1:250: T__63
    		{
    		DebugLocation(1, 250);
    		mT__63(); 

    		}
    		break;
    	case 42:
    		DebugEnterAlt(42);
    		// C:\\output\\HoldingRule.g:1:256: T__64
    		{
    		DebugLocation(1, 256);
    		mT__64(); 

    		}
    		break;
    	case 43:
    		DebugEnterAlt(43);
    		// C:\\output\\HoldingRule.g:1:262: T__65
    		{
    		DebugLocation(1, 262);
    		mT__65(); 

    		}
    		break;
    	case 44:
    		DebugEnterAlt(44);
    		// C:\\output\\HoldingRule.g:1:268: T__66
    		{
    		DebugLocation(1, 268);
    		mT__66(); 

    		}
    		break;
    	case 45:
    		DebugEnterAlt(45);
    		// C:\\output\\HoldingRule.g:1:274: T__67
    		{
    		DebugLocation(1, 274);
    		mT__67(); 

    		}
    		break;
    	case 46:
    		DebugEnterAlt(46);
    		// C:\\output\\HoldingRule.g:1:280: T__68
    		{
    		DebugLocation(1, 280);
    		mT__68(); 

    		}
    		break;
    	case 47:
    		DebugEnterAlt(47);
    		// C:\\output\\HoldingRule.g:1:286: T__69
    		{
    		DebugLocation(1, 286);
    		mT__69(); 

    		}
    		break;
    	case 48:
    		DebugEnterAlt(48);
    		// C:\\output\\HoldingRule.g:1:292: T__70
    		{
    		DebugLocation(1, 292);
    		mT__70(); 

    		}
    		break;
    	case 49:
    		DebugEnterAlt(49);
    		// C:\\output\\HoldingRule.g:1:298: T__71
    		{
    		DebugLocation(1, 298);
    		mT__71(); 

    		}
    		break;
    	case 50:
    		DebugEnterAlt(50);
    		// C:\\output\\HoldingRule.g:1:304: T__72
    		{
    		DebugLocation(1, 304);
    		mT__72(); 

    		}
    		break;
    	case 51:
    		DebugEnterAlt(51);
    		// C:\\output\\HoldingRule.g:1:310: T__73
    		{
    		DebugLocation(1, 310);
    		mT__73(); 

    		}
    		break;
    	case 52:
    		DebugEnterAlt(52);
    		// C:\\output\\HoldingRule.g:1:316: T__74
    		{
    		DebugLocation(1, 316);
    		mT__74(); 

    		}
    		break;
    	case 53:
    		DebugEnterAlt(53);
    		// C:\\output\\HoldingRule.g:1:322: T__75
    		{
    		DebugLocation(1, 322);
    		mT__75(); 

    		}
    		break;
    	case 54:
    		DebugEnterAlt(54);
    		// C:\\output\\HoldingRule.g:1:328: T__76
    		{
    		DebugLocation(1, 328);
    		mT__76(); 

    		}
    		break;
    	case 55:
    		DebugEnterAlt(55);
    		// C:\\output\\HoldingRule.g:1:334: T__77
    		{
    		DebugLocation(1, 334);
    		mT__77(); 

    		}
    		break;
    	case 56:
    		DebugEnterAlt(56);
    		// C:\\output\\HoldingRule.g:1:340: T__78
    		{
    		DebugLocation(1, 340);
    		mT__78(); 

    		}
    		break;
    	case 57:
    		DebugEnterAlt(57);
    		// C:\\output\\HoldingRule.g:1:346: T__79
    		{
    		DebugLocation(1, 346);
    		mT__79(); 

    		}
    		break;
    	case 58:
    		DebugEnterAlt(58);
    		// C:\\output\\HoldingRule.g:1:352: T__80
    		{
    		DebugLocation(1, 352);
    		mT__80(); 

    		}
    		break;
    	case 59:
    		DebugEnterAlt(59);
    		// C:\\output\\HoldingRule.g:1:358: T__81
    		{
    		DebugLocation(1, 358);
    		mT__81(); 

    		}
    		break;
    	case 60:
    		DebugEnterAlt(60);
    		// C:\\output\\HoldingRule.g:1:364: T__82
    		{
    		DebugLocation(1, 364);
    		mT__82(); 

    		}
    		break;
    	case 61:
    		DebugEnterAlt(61);
    		// C:\\output\\HoldingRule.g:1:370: T__83
    		{
    		DebugLocation(1, 370);
    		mT__83(); 

    		}
    		break;
    	case 62:
    		DebugEnterAlt(62);
    		// C:\\output\\HoldingRule.g:1:376: T__84
    		{
    		DebugLocation(1, 376);
    		mT__84(); 

    		}
    		break;
    	case 63:
    		DebugEnterAlt(63);
    		// C:\\output\\HoldingRule.g:1:382: T__85
    		{
    		DebugLocation(1, 382);
    		mT__85(); 

    		}
    		break;
    	case 64:
    		DebugEnterAlt(64);
    		// C:\\output\\HoldingRule.g:1:388: T__86
    		{
    		DebugLocation(1, 388);
    		mT__86(); 

    		}
    		break;
    	case 65:
    		DebugEnterAlt(65);
    		// C:\\output\\HoldingRule.g:1:394: T__87
    		{
    		DebugLocation(1, 394);
    		mT__87(); 

    		}
    		break;
    	case 66:
    		DebugEnterAlt(66);
    		// C:\\output\\HoldingRule.g:1:400: T__88
    		{
    		DebugLocation(1, 400);
    		mT__88(); 

    		}
    		break;
    	case 67:
    		DebugEnterAlt(67);
    		// C:\\output\\HoldingRule.g:1:406: T__89
    		{
    		DebugLocation(1, 406);
    		mT__89(); 

    		}
    		break;
    	case 68:
    		DebugEnterAlt(68);
    		// C:\\output\\HoldingRule.g:1:412: T__90
    		{
    		DebugLocation(1, 412);
    		mT__90(); 

    		}
    		break;
    	case 69:
    		DebugEnterAlt(69);
    		// C:\\output\\HoldingRule.g:1:418: T__91
    		{
    		DebugLocation(1, 418);
    		mT__91(); 

    		}
    		break;
    	case 70:
    		DebugEnterAlt(70);
    		// C:\\output\\HoldingRule.g:1:424: T__92
    		{
    		DebugLocation(1, 424);
    		mT__92(); 

    		}
    		break;
    	case 71:
    		DebugEnterAlt(71);
    		// C:\\output\\HoldingRule.g:1:430: T__93
    		{
    		DebugLocation(1, 430);
    		mT__93(); 

    		}
    		break;
    	case 72:
    		DebugEnterAlt(72);
    		// C:\\output\\HoldingRule.g:1:436: T__94
    		{
    		DebugLocation(1, 436);
    		mT__94(); 

    		}
    		break;
    	case 73:
    		DebugEnterAlt(73);
    		// C:\\output\\HoldingRule.g:1:442: T__95
    		{
    		DebugLocation(1, 442);
    		mT__95(); 

    		}
    		break;
    	case 74:
    		DebugEnterAlt(74);
    		// C:\\output\\HoldingRule.g:1:448: T__96
    		{
    		DebugLocation(1, 448);
    		mT__96(); 

    		}
    		break;
    	case 75:
    		DebugEnterAlt(75);
    		// C:\\output\\HoldingRule.g:1:454: T__97
    		{
    		DebugLocation(1, 454);
    		mT__97(); 

    		}
    		break;
    	case 76:
    		DebugEnterAlt(76);
    		// C:\\output\\HoldingRule.g:1:460: T__98
    		{
    		DebugLocation(1, 460);
    		mT__98(); 

    		}
    		break;
    	case 77:
    		DebugEnterAlt(77);
    		// C:\\output\\HoldingRule.g:1:466: T__99
    		{
    		DebugLocation(1, 466);
    		mT__99(); 

    		}
    		break;
    	case 78:
    		DebugEnterAlt(78);
    		// C:\\output\\HoldingRule.g:1:472: T__100
    		{
    		DebugLocation(1, 472);
    		mT__100(); 

    		}
    		break;
    	case 79:
    		DebugEnterAlt(79);
    		// C:\\output\\HoldingRule.g:1:479: T__101
    		{
    		DebugLocation(1, 479);
    		mT__101(); 

    		}
    		break;
    	case 80:
    		DebugEnterAlt(80);
    		// C:\\output\\HoldingRule.g:1:486: T__102
    		{
    		DebugLocation(1, 486);
    		mT__102(); 

    		}
    		break;
    	case 81:
    		DebugEnterAlt(81);
    		// C:\\output\\HoldingRule.g:1:493: T__103
    		{
    		DebugLocation(1, 493);
    		mT__103(); 

    		}
    		break;
    	case 82:
    		DebugEnterAlt(82);
    		// C:\\output\\HoldingRule.g:1:500: T__104
    		{
    		DebugLocation(1, 500);
    		mT__104(); 

    		}
    		break;
    	case 83:
    		DebugEnterAlt(83);
    		// C:\\output\\HoldingRule.g:1:507: T__105
    		{
    		DebugLocation(1, 507);
    		mT__105(); 

    		}
    		break;
    	case 84:
    		DebugEnterAlt(84);
    		// C:\\output\\HoldingRule.g:1:514: T__106
    		{
    		DebugLocation(1, 514);
    		mT__106(); 

    		}
    		break;
    	case 85:
    		DebugEnterAlt(85);
    		// C:\\output\\HoldingRule.g:1:521: T__107
    		{
    		DebugLocation(1, 521);
    		mT__107(); 

    		}
    		break;
    	case 86:
    		DebugEnterAlt(86);
    		// C:\\output\\HoldingRule.g:1:528: T__108
    		{
    		DebugLocation(1, 528);
    		mT__108(); 

    		}
    		break;
    	case 87:
    		DebugEnterAlt(87);
    		// C:\\output\\HoldingRule.g:1:535: T__109
    		{
    		DebugLocation(1, 535);
    		mT__109(); 

    		}
    		break;
    	case 88:
    		DebugEnterAlt(88);
    		// C:\\output\\HoldingRule.g:1:542: T__110
    		{
    		DebugLocation(1, 542);
    		mT__110(); 

    		}
    		break;
    	case 89:
    		DebugEnterAlt(89);
    		// C:\\output\\HoldingRule.g:1:549: T__111
    		{
    		DebugLocation(1, 549);
    		mT__111(); 

    		}
    		break;
    	case 90:
    		DebugEnterAlt(90);
    		// C:\\output\\HoldingRule.g:1:556: T__112
    		{
    		DebugLocation(1, 556);
    		mT__112(); 

    		}
    		break;
    	case 91:
    		DebugEnterAlt(91);
    		// C:\\output\\HoldingRule.g:1:563: T__113
    		{
    		DebugLocation(1, 563);
    		mT__113(); 

    		}
    		break;
    	case 92:
    		DebugEnterAlt(92);
    		// C:\\output\\HoldingRule.g:1:570: T__114
    		{
    		DebugLocation(1, 570);
    		mT__114(); 

    		}
    		break;
    	case 93:
    		DebugEnterAlt(93);
    		// C:\\output\\HoldingRule.g:1:577: T__115
    		{
    		DebugLocation(1, 577);
    		mT__115(); 

    		}
    		break;
    	case 94:
    		DebugEnterAlt(94);
    		// C:\\output\\HoldingRule.g:1:584: T__116
    		{
    		DebugLocation(1, 584);
    		mT__116(); 

    		}
    		break;
    	case 95:
    		DebugEnterAlt(95);
    		// C:\\output\\HoldingRule.g:1:591: T__117
    		{
    		DebugLocation(1, 591);
    		mT__117(); 

    		}
    		break;
    	case 96:
    		DebugEnterAlt(96);
    		// C:\\output\\HoldingRule.g:1:598: T__118
    		{
    		DebugLocation(1, 598);
    		mT__118(); 

    		}
    		break;
    	case 97:
    		DebugEnterAlt(97);
    		// C:\\output\\HoldingRule.g:1:605: T__119
    		{
    		DebugLocation(1, 605);
    		mT__119(); 

    		}
    		break;
    	case 98:
    		DebugEnterAlt(98);
    		// C:\\output\\HoldingRule.g:1:612: T__120
    		{
    		DebugLocation(1, 612);
    		mT__120(); 

    		}
    		break;
    	case 99:
    		DebugEnterAlt(99);
    		// C:\\output\\HoldingRule.g:1:619: T__121
    		{
    		DebugLocation(1, 619);
    		mT__121(); 

    		}
    		break;
    	case 100:
    		DebugEnterAlt(100);
    		// C:\\output\\HoldingRule.g:1:626: T__122
    		{
    		DebugLocation(1, 626);
    		mT__122(); 

    		}
    		break;
    	case 101:
    		DebugEnterAlt(101);
    		// C:\\output\\HoldingRule.g:1:633: T__123
    		{
    		DebugLocation(1, 633);
    		mT__123(); 

    		}
    		break;
    	case 102:
    		DebugEnterAlt(102);
    		// C:\\output\\HoldingRule.g:1:640: T__124
    		{
    		DebugLocation(1, 640);
    		mT__124(); 

    		}
    		break;
    	case 103:
    		DebugEnterAlt(103);
    		// C:\\output\\HoldingRule.g:1:647: T__125
    		{
    		DebugLocation(1, 647);
    		mT__125(); 

    		}
    		break;
    	case 104:
    		DebugEnterAlt(104);
    		// C:\\output\\HoldingRule.g:1:654: T__126
    		{
    		DebugLocation(1, 654);
    		mT__126(); 

    		}
    		break;
    	case 105:
    		DebugEnterAlt(105);
    		// C:\\output\\HoldingRule.g:1:661: T__127
    		{
    		DebugLocation(1, 661);
    		mT__127(); 

    		}
    		break;
    	case 106:
    		DebugEnterAlt(106);
    		// C:\\output\\HoldingRule.g:1:668: T__128
    		{
    		DebugLocation(1, 668);
    		mT__128(); 

    		}
    		break;
    	case 107:
    		DebugEnterAlt(107);
    		// C:\\output\\HoldingRule.g:1:675: T__129
    		{
    		DebugLocation(1, 675);
    		mT__129(); 

    		}
    		break;
    	case 108:
    		DebugEnterAlt(108);
    		// C:\\output\\HoldingRule.g:1:682: T__130
    		{
    		DebugLocation(1, 682);
    		mT__130(); 

    		}
    		break;
    	case 109:
    		DebugEnterAlt(109);
    		// C:\\output\\HoldingRule.g:1:689: T__131
    		{
    		DebugLocation(1, 689);
    		mT__131(); 

    		}
    		break;
    	case 110:
    		DebugEnterAlt(110);
    		// C:\\output\\HoldingRule.g:1:696: T__132
    		{
    		DebugLocation(1, 696);
    		mT__132(); 

    		}
    		break;
    	case 111:
    		DebugEnterAlt(111);
    		// C:\\output\\HoldingRule.g:1:703: T__133
    		{
    		DebugLocation(1, 703);
    		mT__133(); 

    		}
    		break;
    	case 112:
    		DebugEnterAlt(112);
    		// C:\\output\\HoldingRule.g:1:710: T__134
    		{
    		DebugLocation(1, 710);
    		mT__134(); 

    		}
    		break;
    	case 113:
    		DebugEnterAlt(113);
    		// C:\\output\\HoldingRule.g:1:717: T__135
    		{
    		DebugLocation(1, 717);
    		mT__135(); 

    		}
    		break;
    	case 114:
    		DebugEnterAlt(114);
    		// C:\\output\\HoldingRule.g:1:724: T__136
    		{
    		DebugLocation(1, 724);
    		mT__136(); 

    		}
    		break;
    	case 115:
    		DebugEnterAlt(115);
    		// C:\\output\\HoldingRule.g:1:731: T__137
    		{
    		DebugLocation(1, 731);
    		mT__137(); 

    		}
    		break;
    	case 116:
    		DebugEnterAlt(116);
    		// C:\\output\\HoldingRule.g:1:738: T__138
    		{
    		DebugLocation(1, 738);
    		mT__138(); 

    		}
    		break;
    	case 117:
    		DebugEnterAlt(117);
    		// C:\\output\\HoldingRule.g:1:745: T__139
    		{
    		DebugLocation(1, 745);
    		mT__139(); 

    		}
    		break;
    	case 118:
    		DebugEnterAlt(118);
    		// C:\\output\\HoldingRule.g:1:752: T__140
    		{
    		DebugLocation(1, 752);
    		mT__140(); 

    		}
    		break;
    	case 119:
    		DebugEnterAlt(119);
    		// C:\\output\\HoldingRule.g:1:759: T__141
    		{
    		DebugLocation(1, 759);
    		mT__141(); 

    		}
    		break;
    	case 120:
    		DebugEnterAlt(120);
    		// C:\\output\\HoldingRule.g:1:766: T__142
    		{
    		DebugLocation(1, 766);
    		mT__142(); 

    		}
    		break;
    	case 121:
    		DebugEnterAlt(121);
    		// C:\\output\\HoldingRule.g:1:773: T__143
    		{
    		DebugLocation(1, 773);
    		mT__143(); 

    		}
    		break;
    	case 122:
    		DebugEnterAlt(122);
    		// C:\\output\\HoldingRule.g:1:780: T__144
    		{
    		DebugLocation(1, 780);
    		mT__144(); 

    		}
    		break;
    	case 123:
    		DebugEnterAlt(123);
    		// C:\\output\\HoldingRule.g:1:787: T__145
    		{
    		DebugLocation(1, 787);
    		mT__145(); 

    		}
    		break;
    	case 124:
    		DebugEnterAlt(124);
    		// C:\\output\\HoldingRule.g:1:794: T__146
    		{
    		DebugLocation(1, 794);
    		mT__146(); 

    		}
    		break;
    	case 125:
    		DebugEnterAlt(125);
    		// C:\\output\\HoldingRule.g:1:801: T__147
    		{
    		DebugLocation(1, 801);
    		mT__147(); 

    		}
    		break;
    	case 126:
    		DebugEnterAlt(126);
    		// C:\\output\\HoldingRule.g:1:808: T__148
    		{
    		DebugLocation(1, 808);
    		mT__148(); 

    		}
    		break;
    	case 127:
    		DebugEnterAlt(127);
    		// C:\\output\\HoldingRule.g:1:815: T__149
    		{
    		DebugLocation(1, 815);
    		mT__149(); 

    		}
    		break;
    	case 128:
    		DebugEnterAlt(128);
    		// C:\\output\\HoldingRule.g:1:822: T__150
    		{
    		DebugLocation(1, 822);
    		mT__150(); 

    		}
    		break;
    	case 129:
    		DebugEnterAlt(129);
    		// C:\\output\\HoldingRule.g:1:829: T__151
    		{
    		DebugLocation(1, 829);
    		mT__151(); 

    		}
    		break;
    	case 130:
    		DebugEnterAlt(130);
    		// C:\\output\\HoldingRule.g:1:836: T__152
    		{
    		DebugLocation(1, 836);
    		mT__152(); 

    		}
    		break;
    	case 131:
    		DebugEnterAlt(131);
    		// C:\\output\\HoldingRule.g:1:843: T__153
    		{
    		DebugLocation(1, 843);
    		mT__153(); 

    		}
    		break;
    	case 132:
    		DebugEnterAlt(132);
    		// C:\\output\\HoldingRule.g:1:850: T__154
    		{
    		DebugLocation(1, 850);
    		mT__154(); 

    		}
    		break;
    	case 133:
    		DebugEnterAlt(133);
    		// C:\\output\\HoldingRule.g:1:857: T__155
    		{
    		DebugLocation(1, 857);
    		mT__155(); 

    		}
    		break;
    	case 134:
    		DebugEnterAlt(134);
    		// C:\\output\\HoldingRule.g:1:864: T__156
    		{
    		DebugLocation(1, 864);
    		mT__156(); 

    		}
    		break;
    	case 135:
    		DebugEnterAlt(135);
    		// C:\\output\\HoldingRule.g:1:871: T__157
    		{
    		DebugLocation(1, 871);
    		mT__157(); 

    		}
    		break;
    	case 136:
    		DebugEnterAlt(136);
    		// C:\\output\\HoldingRule.g:1:878: T__158
    		{
    		DebugLocation(1, 878);
    		mT__158(); 

    		}
    		break;
    	case 137:
    		DebugEnterAlt(137);
    		// C:\\output\\HoldingRule.g:1:885: T__159
    		{
    		DebugLocation(1, 885);
    		mT__159(); 

    		}
    		break;
    	case 138:
    		DebugEnterAlt(138);
    		// C:\\output\\HoldingRule.g:1:892: T__160
    		{
    		DebugLocation(1, 892);
    		mT__160(); 

    		}
    		break;
    	case 139:
    		DebugEnterAlt(139);
    		// C:\\output\\HoldingRule.g:1:899: TRANSACT_FIELD
    		{
    		DebugLocation(1, 899);
    		mTRANSACT_FIELD(); 

    		}
    		break;
    	case 140:
    		DebugEnterAlt(140);
    		// C:\\output\\HoldingRule.g:1:914: OR
    		{
    		DebugLocation(1, 914);
    		mOR(); 

    		}
    		break;
    	case 141:
    		DebugEnterAlt(141);
    		// C:\\output\\HoldingRule.g:1:917: AND
    		{
    		DebugLocation(1, 917);
    		mAND(); 

    		}
    		break;
    	case 142:
    		DebugEnterAlt(142);
    		// C:\\output\\HoldingRule.g:1:921: NOT
    		{
    		DebugLocation(1, 921);
    		mNOT(); 

    		}
    		break;
    	case 143:
    		DebugEnterAlt(143);
    		// C:\\output\\HoldingRule.g:1:925: INT
    		{
    		DebugLocation(1, 925);
    		mINT(); 

    		}
    		break;
    	case 144:
    		DebugEnterAlt(144);
    		// C:\\output\\HoldingRule.g:1:929: DEC
    		{
    		DebugLocation(1, 929);
    		mDEC(); 

    		}
    		break;
    	case 145:
    		DebugEnterAlt(145);
    		// C:\\output\\HoldingRule.g:1:933: STRING
    		{
    		DebugLocation(1, 933);
    		mSTRING(); 

    		}
    		break;
    	case 146:
    		DebugEnterAlt(146);
    		// C:\\output\\HoldingRule.g:1:940: COMPARE
    		{
    		DebugLocation(1, 940);
    		mCOMPARE(); 

    		}
    		break;
    	case 147:
    		DebugEnterAlt(147);
    		// C:\\output\\HoldingRule.g:1:948: IN
    		{
    		DebugLocation(1, 948);
    		mIN(); 

    		}
    		break;
    	case 148:
    		DebugEnterAlt(148);
    		// C:\\output\\HoldingRule.g:1:951: CONTAINS
    		{
    		DebugLocation(1, 951);
    		mCONTAINS(); 

    		}
    		break;
    	case 149:
    		DebugEnterAlt(149);
    		// C:\\output\\HoldingRule.g:1:960: EQUAL
    		{
    		DebugLocation(1, 960);
    		mEQUAL(); 

    		}
    		break;
    	case 150:
    		DebugEnterAlt(150);
    		// C:\\output\\HoldingRule.g:1:966: OPEQUAL
    		{
    		DebugLocation(1, 966);
    		mOPEQUAL(); 

    		}
    		break;
    	case 151:
    		DebugEnterAlt(151);
    		// C:\\output\\HoldingRule.g:1:974: IDENTIFIER
    		{
    		DebugLocation(1, 974);
    		mIDENTIFIER(); 

    		}
    		break;
    	case 152:
    		DebugEnterAlt(152);
    		// C:\\output\\HoldingRule.g:1:985: WS
    		{
    		DebugLocation(1, 985);
    		mWS(); 

    		}
    		break;
    	case 153:
    		DebugEnterAlt(153);
    		// C:\\output\\HoldingRule.g:1:988: COMMENTS
    		{
    		DebugLocation(1, 988);
    		mCOMMENTS(); 

    		}
    		break;

    	}

    }


	#region DFA
	DFA15 dfa15;
	DFA24 dfa24;

	protected override void InitDFAs()
	{
		base.InitDFAs();
		dfa15 = new DFA15(this);
		dfa24 = new DFA24(this);
	}

	private class DFA15 : DFA
	{
		private const string DFA15_eotS =
			"\x03\uffff\x01\x07\x01\x09\x05\uffff";
		private const string DFA15_eofS =
			"\x0a\uffff";
		private const string DFA15_minS =
			"\x01\x21\x02\uffff\x02\x3d\x05\uffff";
		private const string DFA15_maxS =
			"\x01\x3e\x02\uffff\x01\x3e\x01\x3d\x05\uffff";
		private const string DFA15_acceptS =
			"\x01\uffff\x01\x01\x01\x02\x02\uffff\x01\x03\x01\x04\x01\x05\x01\x06"+
			"\x01\x07";
		private const string DFA15_specialS =
			"\x0a\uffff}>";
		private static readonly string[] DFA15_transitionS =
			{
				"\x01\x02\x1a\uffff\x01\x03\x01\x01\x01\x04",
				"",
				"",
				"\x01\x06\x01\x05",
				"\x01\x08",
				"",
				"",
				"",
				"",
				""
			};

		private static readonly short[] DFA15_eot = DFA.UnpackEncodedString(DFA15_eotS);
		private static readonly short[] DFA15_eof = DFA.UnpackEncodedString(DFA15_eofS);
		private static readonly char[] DFA15_min = DFA.UnpackEncodedStringToUnsignedChars(DFA15_minS);
		private static readonly char[] DFA15_max = DFA.UnpackEncodedStringToUnsignedChars(DFA15_maxS);
		private static readonly short[] DFA15_accept = DFA.UnpackEncodedString(DFA15_acceptS);
		private static readonly short[] DFA15_special = DFA.UnpackEncodedString(DFA15_specialS);
		private static readonly short[][] DFA15_transition;

		static DFA15()
		{
			int numStates = DFA15_transitionS.Length;
			DFA15_transition = new short[numStates][];
			for ( int i=0; i < numStates; i++ )
			{
				DFA15_transition[i] = DFA.UnpackEncodedString(DFA15_transitionS[i]);
			}
		}

		public DFA15( BaseRecognizer recognizer )
		{
			this.recognizer = recognizer;
			this.decisionNumber = 15;
			this.eot = DFA15_eot;
			this.eof = DFA15_eof;
			this.min = DFA15_min;
			this.max = DFA15_max;
			this.accept = DFA15_accept;
			this.special = DFA15_special;
			this.transition = DFA15_transition;
		}

		public override string Description { get { return "426:1: COMPARE : ( '==' | '!=' | '<>' | '<=' | '<' | '>=' | '>' );"; } }

		public override void Error(NoViableAltException nvae)
		{
			DebugRecognitionException(nvae);
		}
	}

	private class DFA24 : DFA
	{
		private const string DFA24_eotS =
			"\x02\uffff\x02\x38\x02\uffff\x04\x38\x01\uffff\x06\x38\x02\uffff\x06"+
			"\x38\x01\uffff\x08\x38\x01\u0093\x01\u0094\x01\u0095\x01\u0097\x01\u0098"+
			"\x09\x38\x02\uffff\x01\u00ae\x01\u00af\x02\uffff\x01\u00b0\x03\uffff"+
			"\x0e\x38\x01\uffff\x01\x38\x02\u00c3\x01\u00c6\x01\u00c7\x01\u00c3\x01"+
			"\u00ca\x42\x38\x07\uffff\x12\x38\x03\x30\x03\uffff\x0d\x38\x01\u012f"+
			"\x01\u0130\x01\u0131\x01\u0132\x01\x38\x01\uffff\x01\u0134\x01\x38\x02"+
			"\uffff\x01\u0136\x01\x38\x01\uffff\x07\x38\x01\u013f\x01\u0140\x02\x38"+
			"\x01\u0143\x0d\x38\x01\u0151\x01\u0152\x01\u0153\x01\u0154\x02\x38\x01"+
			"\u0157\x01\u0158\x24\x38\x01\u017f\x01\u0180\x02\x31\x01\u0181\x01\x31"+
			"\x08\x38\x02\u00ae\x01\x38\x01\u00ae\x02\x38\x01\u018d\x01\u018e\x03"+
			"\x38\x01\u0192\x01\x38\x01\u0194\x01\u0195\x01\u0196\x01\u0197\x04\uffff"+
			"\x01\x38\x01\uffff\x01\x38\x01\uffff\x01\x38\x01\u019b\x01\u019c\x01"+
			"\u019d\x04\x38\x02\uffff\x02\x38\x01\uffff\x02\x38\x01\u01a7\x01\u01aa"+
			"\x03\x38\x01\u01b0\x05\x38\x04\uffff\x02\x38\x02\uffff\x11\x38\x01\u01cc"+
			"\x01\u01cd\x02\x38\x01\u01d0\x10\x38\x03\uffff\x01\u01e1\x01\u01e2\x01"+
			"\u01e3\x01\u01e4\x01\u01e5\x01\u01e6\x01\u01e7\x01\u01e8\x01\u01e9\x02"+
			"\x38\x02\uffff\x01\u01ec\x01\u01ed\x01\x38\x01\uffff\x01\u01ef\x04\uffff"+
			"\x03\x38\x03\uffff\x09\x38\x01\uffff\x02\x38\x01\uffff\x05\x38\x01\uffff"+
			"\x01\x38\x01\u0205\x01\x38\x01\u0207\x01\x38\x01\u0209\x02\x38\x01\u020c"+
			"\x01\u020d\x06\x38\x01\u0214\x01\u0215\x01\u0216\x01\u0217\x01\u0218"+
			"\x04\x38\x01\u021d\x01\u021e\x02\uffff\x01\u021f\x01\u0220\x01\uffff"+
			"\x01\u0221\x01\u0222\x01\u0223\x01\u0224\x0c\x38\x09\uffff\x01\u0232"+
			"\x01\u0233\x02\uffff\x01\u0234\x01\uffff\x03\x38\x01\u0239\x01\u023a"+
			"\x02\x38\x01\u023d\x0d\x38\x01\uffff\x01\x38\x01\uffff\x01\x38\x01\uffff"+
			"\x02\x38\x02\uffff\x06\x38\x05\uffff\x04\x38\x08\uffff\x0d\x38\x03\uffff"+
			"\x04\x38\x02\uffff\x02\x38\x01\uffff\x02\x38\x01\u0271\x01\u0273\x05"+
			"\x38\x01\u027a\x28\x38\x01\u02a3\x01\uffff\x01\u02a4\x01\uffff\x01\u02a5"+
			"\x01\u02a6\x01\u02a7\x02\x38\x01\u02aa\x01\uffff\x01\u02ab\x01\u02ac"+
			"\x05\x38\x01\u02b2\x01\u02b3\x01\u02b4\x01\u02b5\x01\u02b6\x01\u02b7"+
			"\x01\u02b8\x01\u02b9\x01\u02ba\x01\u02bb\x02\x38\x01\u02be\x01\u02bf"+
			"\x02\u02c0\x02\x38\x01\u02c3\x01\u02c4\x01\u02c0\x0c\x38\x05\uffff\x02"+
			"\x38\x03\uffff\x05\x38\x0a\uffff\x01\u02da\x01\u02db\x03\uffff\x01\u02dc"+
			"\x01\u02dd\x02\uffff\x0e\x38\x01\u02ed\x01\u02ef\x01\u02f1\x01\u02f2"+
			"\x01\u02f3\x01\u02f4\x01\u02f5\x04\uffff\x01\u02f6\x01\u02f7\x01\u02f8"+
			"\x01\u02f9\x01\u02fa\x01\u02fb\x01\u02fc\x01\u02fd\x06\x38\x01\u0304"+
			"\x01\uffff\x01\u0305\x01\uffff\x01\u0306\x0d\uffff\x06\x38\x03\uffff"+
			"\x06\x38\x01\u0313\x01\u0314\x01\u0315\x01\u0316\x01\u0317\x01\u0318"+
			"\x06\uffff";
		private const string DFA24_eofS =
			"\u0319\uffff";
		private const string DFA24_minS =
			"\x01\x09\x01\uffff\x01\x45\x01\x65\x02\uffff\x01\x4f\x01\x6f\x02\x2e"+
			"\x01\uffff\x01\x46\x01\x66\x01\x4f\x01\x6f\x01\x50\x01\x70\x02\uffff"+
			"\x01\x41\x01\x61\x01\x41\x01\x61\x01\x41\x01\x61\x01\uffff\x01\x4c\x01"+
			"\x6c\x01\x52\x01\x72\x01\x4c\x01\x6c\x01\x50\x01\x70\x03\x3d\x01\x2a"+
			"\x01\x3d\x01\x42\x01\x62\x02\x2e\x01\x4f\x01\x6f\x01\x2e\x01\x52\x01"+
			"\x72\x02\uffff\x01\x3d\x01\x2e\x02\uffff\x01\x3d\x03\uffff\x01\x54\x01"+
			"\x74\x01\x4c\x01\x6c\x01\x55\x01\x75\x01\x74\x01\x6c\x01\x75\x02\x54"+
			"\x01\x74\x01\x52\x01\x72\x01\uffff\x01\x72\x06\x30\x01\x4e\x02\x6e\x01"+
			"\x52\x01\x72\x01\x4c\x01\x6c\x01\x4d\x01\x6d\x01\x72\x01\x6c\x01\x6d"+
			"\x01\x43\x01\x63\x01\x54\x01\x74\x01\x53\x01\x73\x01\x63\x01\x74\x01"+
			"\x73\x01\x4e\x01\x6e\x01\x58\x01\x78\x01\x4e\x02\x6e\x01\x78\x01\x6e"+
			"\x01\x4f\x01\x6f\x01\x4e\x01\x6e\x01\x56\x01\x76\x01\x55\x01\x75\x01"+
			"\x4c\x01\x6c\x01\x6f\x01\x6e\x01\x76\x01\x75\x01\x6c\x01\x53\x01\x73"+
			"\x01\x52\x01\x72\x01\x73\x01\x72\x01\x49\x02\x69\x01\x45\x01\x65\x01"+
			"\x4f\x01\x6f\x01\x4e\x01\x6e\x01\x65\x01\x6f\x01\x6e\x01\x44\x02\x64"+
			"\x07\uffff\x01\x53\x01\x73\x01\x44\x01\x64\x01\x73\x01\x64\x01\x4d\x01"+
			"\x6d\x01\x55\x01\x75\x01\x6d\x01\x75\x01\x4c\x01\x6c\x01\x54\x01\x74"+
			"\x01\x6c\x01\x74\x03\x30\x03\uffff\x01\x55\x01\x75\x01\x45\x01\x65\x01"+
			"\x4e\x01\x6e\x01\x75\x01\x65\x01\x6e\x01\x4f\x03\x6f\x04\x30\x01\x49"+
			"\x01\uffff\x01\x30\x01\x69\x02\uffff\x01\x30\x01\x69\x01\uffff\x01\x47"+
			"\x02\x67\x01\x49\x01\x69\x01\x49\x01\x69\x02\x30\x02\x69\x01\x30\x01"+
			"\x49\x01\x69\x01\x45\x01\x65\x01\x54\x01\x74\x01\x69\x01\x65\x01\x74"+
			"\x02\x45\x02\x65\x04\x30\x02\x65\x02\x30\x01\x41\x01\x61\x01\x44\x01"+
			"\x64\x02\x50\x01\x4e\x01\x6e\x01\x53\x01\x73\x01\x61\x01\x64\x01\x50"+
			"\x01\x6e\x01\x73\x01\x45\x01\x65\x01\x4f\x01\x6f\x01\x65\x01\x6f\x01"+
			"\x4e\x02\x6e\x01\x41\x01\x61\x01\x53\x01\x73\x01\x54\x01\x74\x01\x61"+
			"\x01\x73\x01\x74\x01\x41\x02\x61\x06\x30\x01\x45\x01\x65\x01\x45\x03"+
			"\x65\x01\x4c\x01\x6c\x02\x30\x01\x6c\x01\x30\x01\x52\x01\x72\x02\x30"+
			"\x01\x44\x01\x64\x01\x72\x01\x30\x01\x64\x04\x30\x04\uffff\x01\x4e\x01"+
			"\uffff\x01\x6e\x01\uffff\x01\x6e\x03\x30\x01\x4e\x01\x6e\x01\x54\x01"+
			"\x74\x02\uffff\x01\x6e\x01\x74\x01\uffff\x01\x4d\x01\x6d\x02\x30\x01"+
			"\x52\x01\x72\x01\x6d\x01\x30\x01\x72\x01\x59\x01\x41\x01\x79\x01\x41"+
			"\x04\uffff\x01\x79\x01\x41\x02\uffff\x01\x54\x01\x74\x02\x4c\x01\x52"+
			"\x02\x72\x01\x44\x01\x64\x01\x45\x01\x65\x01\x74\x01\x4c\x02\x72\x01"+
			"\x64\x01\x65\x02\x30\x01\x52\x01\x72\x01\x30\x01\x72\x01\x54\x02\x74"+
			"\x01\x54\x01\x74\x01\x45\x01\x65\x01\x41\x01\x61\x01\x74\x01\x65\x01"+
			"\x61\x01\x54\x02\x74\x03\uffff\x09\x30\x01\x4e\x01\x6e\x02\uffff\x02"+
			"\x30\x01\x6e\x01\uffff\x01\x30\x04\uffff\x01\x45\x02\x65\x03\uffff\x01"+
			"\x47\x01\x67\x02\x43\x01\x67\x01\x43\x01\x41\x01\x61\x01\x49\x01\uffff"+
			"\x02\x69\x01\uffff\x01\x49\x01\x69\x01\x61\x02\x69\x01\uffff\x01\x69"+
			"\x01\x30\x01\x53\x01\x30\x01\x73\x01\x30\x02\x73\x02\x30\x01\x41\x02"+
			"\x61\x01\x49\x02\x69\x05\x30\x02\x61\x02\x69\x02\x30\x02\uffff\x02\x30"+
			"\x01\uffff\x04\x30\x01\x45\x01\x65\x02\x4c\x01\x49\x01\x69\x01\x65\x01"+
			"\x4c\x01\x69\x01\x45\x02\x65\x09\uffff\x02\x30\x02\uffff\x01\x30\x01"+
			"\uffff\x03\x51\x02\x30\x01\x4f\x01\x6f\x01\x30\x02\x6f\x01\x4c\x01\x6c"+
			"\x01\x4d\x02\x6d\x01\x42\x01\x62\x01\x6c\x02\x6d\x01\x62\x01\uffff\x01"+
			"\x53\x01\uffff\x01\x73\x01\uffff\x02\x73\x02\uffff\x01\x53\x02\x73\x01"+
			"\x43\x02\x63\x05\uffff\x02\x73\x02\x63\x08\uffff\x02\x4c\x01\x4f\x01"+
			"\x6f\x01\x4e\x01\x6e\x01\x4c\x02\x6f\x01\x6e\x03\x43\x03\uffff\x01\x55"+
			"\x03\x75\x02\uffff\x01\x53\x01\x73\x01\uffff\x02\x73\x02\x30\x01\x45"+
			"\x02\x65\x01\x55\x01\x75\x01\x30\x02\x65\x01\x75\x01\x45\x03\x65\x01"+
			"\x54\x02\x74\x01\x45\x02\x65\x02\x74\x02\x65\x01\x4f\x01\x6f\x01\x54"+
			"\x01\x74\x01\x53\x01\x73\x02\x6f\x02\x74\x01\x73\x01\x41\x03\x61\x01"+
			"\x45\x03\x65\x01\x54\x03\x74\x01\x30\x01\uffff\x01\x30\x01\uffff\x03"+
			"\x30\x01\x54\x01\x74\x01\x30\x01\uffff\x02\x30\x01\x74\x01\x54\x03\x74"+
			"\x0a\x30\x01\x54\x01\x74\x04\x30\x02\x74\x03\x30\x01\x53\x03\x73\x01"+
			"\x52\x03\x72\x03\x42\x01\x62\x05\uffff\x01\x45\x01\x65\x03\uffff\x01"+
			"\x65\x01\x53\x03\x73\x0a\uffff\x02\x30\x03\uffff\x02\x30\x02\uffff\x01"+
			"\x48\x03\x68\x01\x59\x03\x79\x01\x41\x05\x61\x07\x30\x04\uffff\x08\x30"+
			"\x01\x53\x05\x73\x01\x30\x01\uffff\x01\x30\x01\uffff\x01\x30\x0d\uffff"+
			"\x01\x49\x05\x69\x03\uffff\x01\x53\x05\x73\x06\x30\x06\uffff";
		private const string DFA24_maxS =
			"\x01\x7d\x01\uffff\x02\x75\x02\uffff\x02\x6f\x02\x61\x01\uffff\x02\x6e"+
			"\x02\x6f\x02\x75\x02\uffff\x02\x69\x04\x6f\x01\uffff\x06\x72\x02\x70"+
			"\x05\x3d\x02\x6e\x02\x72\x02\x75\x01\x2e\x02\x72\x02\uffff\x01\x3d\x01"+
			"\x39\x02\uffff\x01\x3d\x03\uffff\x01\x54\x01\x74\x01\x4c\x01\x6c\x01"+
			"\x55\x01\x75\x01\x74\x01\x6c\x01\x75\x01\x54\x02\x74\x01\x52\x01\x72"+
			"\x01\uffff\x01\x72\x06\x7a\x01\x4e\x02\x6e\x01\x52\x01\x72\x01\x4c\x01"+
			"\x6c\x01\x4d\x01\x6d\x01\x72\x01\x6c\x01\x6d\x01\x43\x01\x63\x01\x54"+
			"\x01\x74\x01\x53\x01\x73\x01\x63\x01\x74\x01\x73\x01\x56\x01\x76\x01"+
			"\x58\x01\x78\x01\x4e\x01\x6e\x01\x76\x01\x78\x01\x6e\x01\x4f\x01\x6f"+
			"\x01\x4e\x01\x6e\x01\x56\x01\x76\x01\x55\x01\x75\x01\x4c\x01\x6c\x01"+
			"\x6f\x01\x6e\x01\x76\x01\x75\x01\x6c\x01\x53\x01\x73\x01\x52\x01\x72"+
			"\x01\x73\x01\x72\x01\x49\x02\x69\x01\x45\x01\x65\x01\x4f\x01\x6f\x01"+
			"\x4e\x01\x6e\x01\x65\x01\x6f\x01\x6e\x01\x44\x02\x64\x07\uffff\x01\x53"+
			"\x01\x73\x01\x44\x01\x64\x01\x73\x01\x64\x01\x4d\x01\x6d\x01\x55\x01"+
			"\x75\x01\x6d\x01\x75\x01\x4c\x01\x6c\x01\x54\x01\x74\x01\x6c\x01\x74"+
			"\x03\x7a\x03\uffff\x01\x55\x01\x75\x01\x45\x01\x65\x01\x4e\x01\x6e\x01"+
			"\x75\x01\x65\x01\x6e\x01\x4f\x03\x6f\x04\x7a\x01\x49\x01\uffff\x01\x7a"+
			"\x01\x69\x02\uffff\x01\x7a\x01\x69\x01\uffff\x01\x47\x02\x67\x01\x49"+
			"\x01\x69\x01\x49\x01\x69\x02\x7a\x02\x69\x01\x7a\x01\x49\x01\x69\x01"+
			"\x45\x01\x65\x01\x54\x01\x74\x01\x69\x01\x65\x01\x74\x02\x45\x02\x65"+
			"\x04\x7a\x02\x65\x02\x7a\x01\x41\x01\x61\x01\x44\x01\x64\x01\x50\x01"+
			"\x70\x01\x4e\x01\x6e\x01\x53\x01\x73\x01\x61\x01\x64\x01\x70\x01\x6e"+
			"\x01\x73\x01\x45\x01\x65\x01\x4f\x01\x6f\x01\x65\x01\x6f\x01\x4e\x02"+
			"\x6e\x01\x41\x01\x61\x01\x53\x01\x73\x01\x54\x01\x74\x01\x61\x01\x73"+
			"\x01\x74\x01\x41\x02\x61\x06\x7a\x01\x45\x01\x65\x01\x45\x03\x65\x01"+
			"\x4c\x01\x6c\x02\x7a\x01\x6c\x01\x7a\x01\x52\x01\x72\x02\x7a\x01\x44"+
			"\x01\x64\x01\x72\x01\x7a\x01\x64\x04\x7a\x04\uffff\x01\x4e\x01\uffff"+
			"\x01\x6e\x01\uffff\x01\x6e\x03\x7a\x01\x4e\x01\x6e\x01\x54\x01\x74\x02"+
			"\uffff\x01\x6e\x01\x74\x01\uffff\x01\x4d\x01\x6d\x02\x7a\x01\x52\x01"+
			"\x72\x01\x6d\x01\x7a\x01\x72\x01\x59\x01\x41\x01\x79\x01\x41\x04\uffff"+
			"\x01\x79\x01\x61\x02\uffff\x01\x54\x01\x74\x01\x4c\x01\x6c\x01\x52\x02"+
			"\x72\x01\x44\x01\x64\x01\x45\x01\x65\x01\x74\x01\x6c\x02\x72\x01\x64"+
			"\x01\x65\x02\x7a\x01\x52\x01\x72\x01\x7a\x01\x72\x01\x54\x02\x74\x01"+
			"\x54\x01\x74\x01\x45\x01\x65\x01\x41\x01\x61\x01\x74\x01\x65\x01\x61"+
			"\x01\x54\x02\x74\x03\uffff\x09\x7a\x01\x4e\x01\x6e\x02\uffff\x02\x7a"+
			"\x01\x6e\x01\uffff\x01\x7a\x04\uffff\x01\x45\x02\x65\x03\uffff\x01\x47"+
			"\x01\x67\x02\x43\x01\x67\x01\x63\x01\x41\x01\x61\x01\x49\x01\uffff\x02"+
			"\x69\x01\uffff\x01\x49\x01\x69\x01\x61\x02\x69\x01\uffff\x01\x69\x01"+
			"\x7a\x01\x53\x01\x7a\x01\x73\x01\x7a\x02\x73\x02\x7a\x01\x41\x02\x61"+
			"\x01\x49\x02\x69\x05\x7a\x02\x61\x02\x69\x02\x7a\x02\uffff\x02\x7a\x01"+
			"\uffff\x04\x7a\x01\x45\x01\x65\x02\x4c\x01\x49\x01\x69\x01\x65\x01\x6c"+
			"\x01\x69\x01\x45\x02\x65\x09\uffff\x02\x7a\x02\uffff\x01\x7a\x01\uffff"+
			"\x02\x51\x01\x71\x02\x7a\x01\x4f\x01\x6f\x01\x7a\x02\x6f\x01\x4c\x01"+
			"\x6c\x01\x4d\x02\x6d\x01\x42\x01\x62\x01\x6c\x02\x6d\x01\x62\x01\uffff"+
			"\x01\x53\x01\uffff\x01\x73\x01\uffff\x02\x73\x02\uffff\x01\x53\x02\x73"+
			"\x01\x43\x02\x63\x05\uffff\x02\x73\x02\x63\x08\uffff\x02\x4c\x01\x4f"+
			"\x01\x6f\x01\x4e\x01\x6e\x01\x6c\x02\x6f\x01\x6e\x02\x43\x01\x63\x03"+
			"\uffff\x01\x55\x03\x75\x02\uffff\x01\x53\x01\x73\x01\uffff\x02\x73\x02"+
			"\x7a\x01\x45\x02\x65\x01\x55\x01\x75\x01\x7a\x02\x65\x01\x75\x01\x45"+
			"\x03\x65\x01\x54\x02\x74\x01\x45\x02\x65\x02\x74\x02\x65\x01\x4f\x01"+
			"\x6f\x01\x54\x01\x74\x01\x53\x01\x73\x02\x6f\x02\x74\x01\x73\x01\x41"+
			"\x03\x61\x01\x45\x03\x65\x01\x54\x03\x74\x01\x7a\x01\uffff\x01\x7a\x01"+
			"\uffff\x03\x7a\x01\x54\x01\x74\x01\x7a\x01\uffff\x02\x7a\x01\x74\x01"+
			"\x54\x03\x74\x0a\x7a\x01\x54\x01\x74\x04\x7a\x02\x74\x03\x7a\x01\x53"+
			"\x03\x73\x01\x52\x03\x72\x01\x42\x03\x62\x05\uffff\x01\x45\x01\x65\x03"+
			"\uffff\x01\x65\x01\x53\x03\x73\x0a\uffff\x02\x7a\x03\uffff\x02\x7a\x02"+
			"\uffff\x01\x48\x03\x68\x01\x59\x03\x79\x01\x41\x05\x61\x07\x7a\x04\uffff"+
			"\x08\x7a\x01\x53\x05\x73\x01\x7a\x01\uffff\x01\x7a\x01\uffff\x01\x7a"+
			"\x0d\uffff\x01\x49\x05\x69\x03\uffff\x01\x53\x05\x73\x06\x7a\x06\uffff";
		private const string DFA24_acceptS =
			"\x01\uffff\x01\x01\x02\uffff\x01\x05\x01\x06\x04\uffff\x01\x0e\x06\uffff"+
			"\x01\x18\x01\x19\x06\uffff\x01\x2c\x16\uffff\x01\u008c\x01\u008d\x02"+
			"\uffff\x01\u0090\x01\u0091\x01\uffff\x01\u0092\x01\u0097\x01\u0098\x0e"+
			"\uffff\x01\u008b\x49\uffff\x01\u0096\x01\x5e\x01\x5f\x01\x60\x01\u0099"+
			"\x01\x61\x01\x62\x15\uffff\x01\u008e\x01\u008f\x01\u0095\x12\uffff\x01"+
			"\u0093\x02\uffff\x01\x2d\x01\x2e\x02\uffff\x01\x2f\x64\uffff\x01\x0b"+
			"\x01\x0c\x01\x0d\x01\x0f\x01\uffff\x01\x10\x01\uffff\x01\x11\x08\uffff"+
			"\x01\x76\x01\x77\x02\uffff\x01\x78\x0d\uffff\x01\x29\x01\x2a\x01\x70"+
			"\x01\x71\x02\uffff\x01\x2b\x01\x72\x26\uffff\x01\x63\x01\x64\x01\x65"+
			"\x0b\uffff\x01\x54\x01\x55\x03\uffff\x01\x56\x01\uffff\x01\x07\x01\x08"+
			"\x01\x09\x01\x0a\x03\uffff\x01\x12\x01\x13\x01\x14\x09\uffff\x01\x68"+
			"\x02\uffff\x01\x69\x05\uffff\x01\x6a\x1b\uffff\x01\x30\x01\x31\x02\uffff"+
			"\x01\x32\x10\uffff\x01\x7f\x01\u0080\x01\u0085\x01\u0086\x01\u0081\x01"+
			"\u0087\x01\u0082\x01\u0083\x01\u0084\x02\uffff\x01\x73\x01\x74\x01\uffff"+
			"\x01\x75\x15\uffff\x01\x20\x01\uffff\x01\x21\x01\uffff\x01\x22\x02\uffff"+
			"\x01\x23\x01\x24\x06\uffff\x01\x79\x01\x7a\x01\u0088\x01\u0089\x01\x25"+
			"\x04\uffff\x01\x7b\x01\u008a\x01\x36\x01\x37\x01\x38\x01\x33\x01\x34"+
			"\x01\x35\x0d\uffff\x01\x02\x01\x03\x01\x04\x04\uffff\x01\x15\x01\x16"+
			"\x02\uffff\x01\x17\x33\uffff\x01\x1a\x01\uffff\x01\x1b\x06\uffff\x01"+
			"\x1c\x28\uffff\x01\x1d\x01\x1e\x01\x26\x01\x27\x01\x66\x02\uffff\x01"+
			"\x1f\x01\x28\x01\x67\x05\uffff\x01\x47\x01\x48\x01\x49\x01\x6b\x01\x6c"+
			"\x01\x6d\x01\x4a\x01\x4b\x01\x6e\x01\x6f\x02\uffff\x01\x3d\x01\x3e\x01"+
			"\u0094\x02\uffff\x01\x3f\x01\x40\x15\uffff\x01\x39\x01\x3a\x01\x3b\x01"+
			"\x3c\x0f\uffff\x01\x5b\x01\uffff\x01\x5c\x01\uffff\x01\x5d\x01\x57\x01"+
			"\x58\x01\x59\x01\x5a\x01\x4c\x01\x4d\x01\x4e\x01\x4f\x01\x50\x01\x51"+
			"\x01\x52\x01\x53\x06\uffff\x01\x7c\x01\x7d\x01\x7e\x0c\uffff\x01\x41"+
			"\x01\x42\x01\x43\x01\x44\x01\x45\x01\x46";
		private const string DFA24_specialS =
			"\u0319\uffff}>";
		private static readonly string[] DFA24_transitionS =
			{
				"\x02\x39\x02\uffff\x01\x39\x12\uffff\x01\x39\x01\x32\x01\x35\x02\uffff"+
				"\x01\x26\x01\x31\x01\x35\x01\x11\x01\x12\x01\x22\x01\x23\x01\x0a\x01"+
				"\x24\x01\x34\x01\x25\x0a\x33\x01\x19\x01\x01\x01\x37\x01\x36\x01\x37"+
				"\x02\uffff\x01\x27\x01\x38\x01\x1e\x01\x13\x01\x1a\x01\x17\x01\x06\x01"+
				"\x2d\x01\x0b\x02\x38\x01\x0d\x01\x15\x01\x2b\x01\x2e\x01\x1c\x01\x38"+
				"\x01\x02\x01\x0f\x01\x29\x01\x20\x01\x08\x01\x2d\x03\x38\x04\uffff\x01"+
				"\x38\x01\uffff\x01\x28\x01\x38\x01\x1f\x01\x14\x01\x1b\x01\x18\x01\x07"+
				"\x01\x2d\x01\x0c\x02\x38\x01\x0e\x01\x16\x01\x2c\x01\x2f\x01\x1d\x01"+
				"\x38\x01\x03\x01\x10\x01\x2a\x01\x21\x01\x09\x01\x2d\x03\x38\x01\x04"+
				"\x01\x30\x01\x05",
				"",
				"\x01\x3a\x09\uffff\x01\x3e\x05\uffff\x01\x3c\x0f\uffff\x01\x3b\x09"+
				"\uffff\x01\x3f\x05\uffff\x01\x3d",
				"\x01\x40\x09\uffff\x01\x42\x05\uffff\x01\x41",
				"",
				"",
				"\x01\x43\x1f\uffff\x01\x44",
				"\x01\x45",
				"\x01\x48\x12\uffff\x01\x46\x1f\uffff\x01\x47",
				"\x01\x48\x32\uffff\x01\x49",
				"",
				"\x01\x4c\x07\uffff\x01\x4a\x17\uffff\x01\x4d\x07\uffff\x01\x4b",
				"\x01\x4f\x07\uffff\x01\x4e",
				"\x01\x50\x1f\uffff\x01\x51",
				"\x01\x52",
				"\x01\x55\x03\uffff\x01\x53\x01\x57\x1a\uffff\x01\x56\x03\uffff\x01"+
				"\x54\x01\x58",
				"\x01\x5a\x03\uffff\x01\x59\x01\x5b",
				"",
				"",
				"\x01\x5e\x03\uffff\x01\x5c\x03\uffff\x01\x60\x17\uffff\x01\x5f\x03"+
				"\uffff\x01\x5d\x03\uffff\x01\x61",
				"\x01\x63\x03\uffff\x01\x62\x03\uffff\x01\x64",
				"\x01\x67\x07\uffff\x01\x69\x05\uffff\x01\x65\x11\uffff\x01\x68\x07"+
				"\uffff\x01\x6a\x05\uffff\x01\x66",
				"\x01\x6c\x07\uffff\x01\x6d\x05\uffff\x01\x6b",
				"\x01\x76\x07\uffff\x01\x70\x02\uffff\x01\x6e\x01\x72\x01\uffff\x01"+
				"\x74\x11\uffff\x01\x77\x07\uffff\x01\x71\x02\uffff\x01\x6f\x01\x73\x01"+
				"\uffff\x01\x75",
				"\x01\x7c\x07\uffff\x01\x79\x02\uffff\x01\x78\x01\x7a\x01\uffff\x01"+
				"\x7b",
				"",
				"\x01\x7d\x05\uffff\x01\x7f\x19\uffff\x01\x7e\x05\uffff\x01\u0080",
				"\x01\u0081\x05\uffff\x01\u0082",
				"\x01\u0083\x1f\uffff\x01\u0084",
				"\x01\u0085",
				"\x01\u0088\x02\uffff\x01\u008a\x02\uffff\x01\u0086\x19\uffff\x01\u0089"+
				"\x02\uffff\x01\u008b\x02\uffff\x01\u0087",
				"\x01\u008d\x02\uffff\x01\u008e\x02\uffff\x01\u008c",
				"\x01\u008f\x1f\uffff\x01\u0090",
				"\x01\u0091",
				"\x01\u0092",
				"\x01\u0092",
				"\x01\u0092",
				"\x01\u0096\x04\uffff\x01\u0096\x0d\uffff\x01\u0092",
				"\x01\u0092",
				"\x01\u0099\x0b\uffff\x01\u009b\x13\uffff\x01\u009a\x0b\uffff\x01\u009c",
				"\x01\u009d\x0b\uffff\x01\u009e",
				"\x01\x48\x1a\uffff\x01\u009f\x08\uffff\x01\u00a1\x16\uffff\x01\u00a0"+
				"\x08\uffff\x01\u00a2",
				"\x01\x48\x3a\uffff\x01\u00a3\x08\uffff\x01\u00a4",
				"\x01\u00a7\x05\uffff\x01\u00a5\x19\uffff\x01\u00a8\x05\uffff\x01\u00a6",
				"\x01\u00aa\x05\uffff\x01\u00a9",
				"\x01\x48",
				"\x01\u00ab\x1f\uffff\x01\u00ac",
				"\x01\u00ad",
				"",
				"",
				"\x01\x37",
				"\x01\x34\x01\uffff\x0a\x33",
				"",
				"",
				"\x01\x37",
				"",
				"",
				"",
				"\x01\u00b1",
				"\x01\u00b2",
				"\x01\u00b3",
				"\x01\u00b4",
				"\x01\u00b5",
				"\x01\u00b6",
				"\x01\u00b7",
				"\x01\u00b8",
				"\x01\u00b9",
				"\x01\u00ba",
				"\x01\u00bb\x1f\uffff\x01\u00bc",
				"\x01\u00bd",
				"\x01\u00be",
				"\x01\u00bf",
				"",
				"\x01\u00c0",
				"\x0a\x38\x07\uffff\x0b\x38\x01\u00c2\x07\x38\x01\u00c1\x06\x38\x04"+
				"\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x0b\x38\x01"+
				"\u00c5\x07\x38\x01\u00c4\x06\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x0b\x38\x01"+
				"\u00c9\x07\x38\x01\u00c8\x06\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u00cb",
				"\x01\u00cc",
				"\x01\u00cd",
				"\x01\u00ce",
				"\x01\u00cf",
				"\x01\u00d0",
				"\x01\u00d1",
				"\x01\u00d2",
				"\x01\u00d3",
				"\x01\u00d4",
				"\x01\u00d5",
				"\x01\u00d6",
				"\x01\u00d7",
				"\x01\u00d8",
				"\x01\u00d9",
				"\x01\u00da",
				"\x01\u00db",
				"\x01\u00dc",
				"\x01\u00dd",
				"\x01\u00de",
				"\x01\u00df",
				"\x01\u00e0\x07\uffff\x01\u00e1",
				"\x01\u00e2\x07\uffff\x01\u00e3",
				"\x01\u00e4",
				"\x01\u00e5",
				"\x01\u00e6",
				"\x01\u00e7",
				"\x01\u00e8\x07\uffff\x01\u00e9",
				"\x01\u00ea",
				"\x01\u00eb",
				"\x01\u00ec",
				"\x01\u00ed",
				"\x01\u00ee",
				"\x01\u00ef",
				"\x01\u00f0",
				"\x01\u00f1",
				"\x01\u00f2",
				"\x01\u00f3",
				"\x01\u00f4",
				"\x01\u00f5",
				"\x01\u00f6",
				"\x01\u00f7",
				"\x01\u00f8",
				"\x01\u00f9",
				"\x01\u00fa",
				"\x01\u00fb",
				"\x01\u00fc",
				"\x01\u00fd",
				"\x01\u00fe",
				"\x01\u00ff",
				"\x01\u0100",
				"\x01\u0101",
				"\x01\u0102",
				"\x01\u0103",
				"\x01\u0104",
				"\x01\u0105",
				"\x01\u0106",
				"\x01\u0107",
				"\x01\u0108",
				"\x01\u0109",
				"\x01\u010a",
				"\x01\u010b",
				"\x01\u010c",
				"\x01\u010d",
				"\x01\u010e",
				"\x01\u010f",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"\x01\u0110",
				"\x01\u0111",
				"\x01\u0112",
				"\x01\u0113",
				"\x01\u0114",
				"\x01\u0115",
				"\x01\u0116",
				"\x01\u0117",
				"\x01\u0118",
				"\x01\u0119",
				"\x01\u011a",
				"\x01\u011b",
				"\x01\u011c",
				"\x01\u011d",
				"\x01\u011e",
				"\x01\u011f",
				"\x01\u0120",
				"\x01\u0121",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"",
				"\x01\u0122",
				"\x01\u0123",
				"\x01\u0124",
				"\x01\u0125",
				"\x01\u0126",
				"\x01\u0127",
				"\x01\u0128",
				"\x01\u0129",
				"\x01\u012a",
				"\x01\u012b",
				"\x01\u012c",
				"\x01\u012d",
				"\x01\u012e",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0133",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0135",
				"",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0137",
				"",
				"\x01\u0138",
				"\x01\u0139",
				"\x01\u013a",
				"\x01\u013b",
				"\x01\u013c",
				"\x01\u013d",
				"\x01\u013e",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0141",
				"\x01\u0142",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0144",
				"\x01\u0145",
				"\x01\u0146",
				"\x01\u0147",
				"\x01\u0148",
				"\x01\u0149",
				"\x01\u014a",
				"\x01\u014b",
				"\x01\u014c",
				"\x01\u014d",
				"\x01\u014e",
				"\x01\u014f",
				"\x01\u0150",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0155",
				"\x01\u0156",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0159",
				"\x01\u015a",
				"\x01\u015b",
				"\x01\u015c",
				"\x01\u015d",
				"\x01\u015e\x1f\uffff\x01\u015f",
				"\x01\u0160",
				"\x01\u0161",
				"\x01\u0162",
				"\x01\u0163",
				"\x01\u0164",
				"\x01\u0165",
				"\x01\u0166\x1f\uffff\x01\u0167",
				"\x01\u0168",
				"\x01\u0169",
				"\x01\u016a",
				"\x01\u016b",
				"\x01\u016c",
				"\x01\u016d",
				"\x01\u016e",
				"\x01\u016f",
				"\x01\u0170",
				"\x01\u0171",
				"\x01\u0172",
				"\x01\u0173",
				"\x01\u0174",
				"\x01\u0175",
				"\x01\u0176",
				"\x01\u0177",
				"\x01\u0178",
				"\x01\u0179",
				"\x01\u017a",
				"\x01\u017b",
				"\x01\u017c",
				"\x01\u017d",
				"\x01\u017e",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0182",
				"\x01\u0183",
				"\x01\u0184",
				"\x01\u0185",
				"\x01\u0186",
				"\x01\u0187",
				"\x01\u0188",
				"\x01\u0189",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u018a",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u018b",
				"\x01\u018c",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u018f",
				"\x01\u0190",
				"\x01\u0191",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0193",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"",
				"",
				"\x01\u0198",
				"",
				"\x01\u0199",
				"",
				"\x01\u019a",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u019e",
				"\x01\u019f",
				"\x01\u01a0",
				"\x01\u01a1",
				"",
				"",
				"\x01\u01a2",
				"\x01\u01a3",
				"",
				"\x01\u01a4",
				"\x01\u01a5",
				"\x0a\x38\x07\uffff\x13\x38\x01\u01a6\x06\x38\x04\uffff\x01\x38\x01"+
				"\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x13\x38\x01\u01a9\x06\x38\x04\uffff\x01\x38\x01"+
				"\uffff\x13\x38\x01\u01a8\x06\x38",
				"\x01\u01ab",
				"\x01\u01ac",
				"\x01\u01ad",
				"\x0a\x38\x07\uffff\x13\x38\x01\u01af\x06\x38\x04\uffff\x01\x38\x01"+
				"\uffff\x13\x38\x01\u01ae\x06\x38",
				"\x01\u01b1",
				"\x01\u01b2",
				"\x01\u01b3",
				"\x01\u01b4",
				"\x01\u01b5",
				"",
				"",
				"",
				"",
				"\x01\u01b6",
				"\x01\u01b7\x1f\uffff\x01\u01b8",
				"",
				"",
				"\x01\u01b9",
				"\x01\u01ba",
				"\x01\u01bb",
				"\x01\u01bc\x1f\uffff\x01\u01bd",
				"\x01\u01be",
				"\x01\u01bf",
				"\x01\u01c0",
				"\x01\u01c1",
				"\x01\u01c2",
				"\x01\u01c3",
				"\x01\u01c4",
				"\x01\u01c5",
				"\x01\u01c6\x1f\uffff\x01\u01c7",
				"\x01\u01c8",
				"\x01\u01c9",
				"\x01\u01ca",
				"\x01\u01cb",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u01ce",
				"\x01\u01cf",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u01d1",
				"\x01\u01d2",
				"\x01\u01d3",
				"\x01\u01d4",
				"\x01\u01d5",
				"\x01\u01d6",
				"\x01\u01d7",
				"\x01\u01d8",
				"\x01\u01d9",
				"\x01\u01da",
				"\x01\u01db",
				"\x01\u01dc",
				"\x01\u01dd",
				"\x01\u01de",
				"\x01\u01df",
				"\x01\u01e0",
				"",
				"",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u01ea",
				"\x01\u01eb",
				"",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u01ee",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"",
				"",
				"\x01\u01f0",
				"\x01\u01f1",
				"\x01\u01f2",
				"",
				"",
				"",
				"\x01\u01f3",
				"\x01\u01f4",
				"\x01\u01f5",
				"\x01\u01f6",
				"\x01\u01f7",
				"\x01\u01f8\x1f\uffff\x01\u01f9",
				"\x01\u01fa",
				"\x01\u01fb",
				"\x01\u01fc",
				"",
				"\x01\u01fd",
				"\x01\u01fe",
				"",
				"\x01\u01ff",
				"\x01\u0200",
				"\x01\u0201",
				"\x01\u0202",
				"\x01\u0203",
				"",
				"\x01\u0204",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0206",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0208",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u020a",
				"\x01\u020b",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u020e",
				"\x01\u020f",
				"\x01\u0210",
				"\x01\u0211",
				"\x01\u0212",
				"\x01\u0213",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0219",
				"\x01\u021a",
				"\x01\u021b",
				"\x01\u021c",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u0225",
				"\x01\u0226",
				"\x01\u0227",
				"\x01\u0228",
				"\x01\u0229",
				"\x01\u022a",
				"\x01\u022b",
				"\x01\u022c\x1f\uffff\x01\u022d",
				"\x01\u022e",
				"\x01\u022f",
				"\x01\u0230",
				"\x01\u0231",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"\x01\u0235",
				"\x01\u0236",
				"\x01\u0237\x1f\uffff\x01\u0238",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u023b",
				"\x01\u023c",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u023e",
				"\x01\u023f",
				"\x01\u0240",
				"\x01\u0241",
				"\x01\u0242",
				"\x01\u0243",
				"\x01\u0244",
				"\x01\u0245",
				"\x01\u0246",
				"\x01\u0247",
				"\x01\u0248",
				"\x01\u0249",
				"\x01\u024a",
				"",
				"\x01\u024b",
				"",
				"\x01\u024c",
				"",
				"\x01\u024d",
				"\x01\u024e",
				"",
				"",
				"\x01\u024f",
				"\x01\u0250",
				"\x01\u0251",
				"\x01\u0252",
				"\x01\u0253",
				"\x01\u0254",
				"",
				"",
				"",
				"",
				"",
				"\x01\u0255",
				"\x01\u0256",
				"\x01\u0257",
				"\x01\u0258",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"\x01\u0259",
				"\x01\u025a",
				"\x01\u025b",
				"\x01\u025c",
				"\x01\u025d",
				"\x01\u025e",
				"\x01\u025f\x1f\uffff\x01\u0260",
				"\x01\u0261",
				"\x01\u0262",
				"\x01\u0263",
				"\x01\u0264",
				"\x01\u0265",
				"\x01\u0266\x1f\uffff\x01\u0267",
				"",
				"",
				"",
				"\x01\u0268",
				"\x01\u0269",
				"\x01\u026a",
				"\x01\u026b",
				"",
				"",
				"\x01\u026c",
				"\x01\u026d",
				"",
				"\x01\u026e",
				"\x01\u026f",
				"\x02\x38\x01\u0270\x07\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01"+
				"\uffff\x1a\x38",
				"\x02\x38\x01\u0272\x07\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01"+
				"\uffff\x1a\x38",
				"\x01\u0274",
				"\x01\u0275",
				"\x01\u0276",
				"\x01\u0277",
				"\x01\u0278",
				"\x02\x38\x01\u0279\x07\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01"+
				"\uffff\x1a\x38",
				"\x01\u027b",
				"\x01\u027c",
				"\x01\u027d",
				"\x01\u027e",
				"\x01\u027f",
				"\x01\u0280",
				"\x01\u0281",
				"\x01\u0282",
				"\x01\u0283",
				"\x01\u0284",
				"\x01\u0285",
				"\x01\u0286",
				"\x01\u0287",
				"\x01\u0288",
				"\x01\u0289",
				"\x01\u028a",
				"\x01\u028b",
				"\x01\u028c",
				"\x01\u028d",
				"\x01\u028e",
				"\x01\u028f",
				"\x01\u0290",
				"\x01\u0291",
				"\x01\u0292",
				"\x01\u0293",
				"\x01\u0294",
				"\x01\u0295",
				"\x01\u0296",
				"\x01\u0297",
				"\x01\u0298",
				"\x01\u0299",
				"\x01\u029a",
				"\x01\u029b",
				"\x01\u029c",
				"\x01\u029d",
				"\x01\u029e",
				"\x01\u029f",
				"\x01\u02a0",
				"\x01\u02a1",
				"\x01\u02a2",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u02a8",
				"\x01\u02a9",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u02ad",
				"\x01\u02ae",
				"\x01\u02af",
				"\x01\u02b0",
				"\x01\u02b1",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u02bc",
				"\x01\u02bd",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u02c1",
				"\x01\u02c2",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u02c5",
				"\x01\u02c6",
				"\x01\u02c7",
				"\x01\u02c8",
				"\x01\u02c9",
				"\x01\u02ca",
				"\x01\u02cb",
				"\x01\u02cc",
				"\x01\u02cd",
				"\x01\u02ce\x1f\uffff\x01\u02cf",
				"\x01\u02d0\x1f\uffff\x01\u02d1",
				"\x01\u02d2",
				"",
				"",
				"",
				"",
				"",
				"\x01\u02d3",
				"\x01\u02d4",
				"",
				"",
				"",
				"\x01\u02d5",
				"\x01\u02d6",
				"\x01\u02d7",
				"\x01\u02d8",
				"\x01\u02d9",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"\x01\u02de",
				"\x01\u02df",
				"\x01\u02e0",
				"\x01\u02e1",
				"\x01\u02e2",
				"\x01\u02e3",
				"\x01\u02e4",
				"\x01\u02e5",
				"\x01\u02e6",
				"\x01\u02e7",
				"\x01\u02e8",
				"\x01\u02e9",
				"\x01\u02ea",
				"\x01\u02eb",
				"\x0a\x38\x07\uffff\x03\x38\x01\u02ec\x16\x38\x04\uffff\x01\x38\x01"+
				"\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x03\x38\x01"+
				"\u02ee\x16\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x03\x38\x01"+
				"\u02f0\x16\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x01\u02fe",
				"\x01\u02ff",
				"\x01\u0300",
				"\x01\u0301",
				"\x01\u0302",
				"\x01\u0303",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"\x01\u0307",
				"\x01\u0308",
				"\x01\u0309",
				"\x01\u030a",
				"\x01\u030b",
				"\x01\u030c",
				"",
				"",
				"",
				"\x01\u030d",
				"\x01\u030e",
				"\x01\u030f",
				"\x01\u0310",
				"\x01\u0311",
				"\x01\u0312",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"\x0a\x38\x07\uffff\x1a\x38\x04\uffff\x01\x38\x01\uffff\x1a\x38",
				"",
				"",
				"",
				"",
				"",
				""
			};

		private static readonly short[] DFA24_eot = DFA.UnpackEncodedString(DFA24_eotS);
		private static readonly short[] DFA24_eof = DFA.UnpackEncodedString(DFA24_eofS);
		private static readonly char[] DFA24_min = DFA.UnpackEncodedStringToUnsignedChars(DFA24_minS);
		private static readonly char[] DFA24_max = DFA.UnpackEncodedStringToUnsignedChars(DFA24_maxS);
		private static readonly short[] DFA24_accept = DFA.UnpackEncodedString(DFA24_acceptS);
		private static readonly short[] DFA24_special = DFA.UnpackEncodedString(DFA24_specialS);
		private static readonly short[][] DFA24_transition;

		static DFA24()
		{
			int numStates = DFA24_transitionS.Length;
			DFA24_transition = new short[numStates][];
			for ( int i=0; i < numStates; i++ )
			{
				DFA24_transition[i] = DFA.UnpackEncodedString(DFA24_transitionS[i]);
			}
		}

		public DFA24( BaseRecognizer recognizer )
		{
			this.recognizer = recognizer;
			this.decisionNumber = 24;
			this.eot = DFA24_eot;
			this.eof = DFA24_eof;
			this.min = DFA24_min;
			this.max = DFA24_max;
			this.accept = DFA24_accept;
			this.special = DFA24_special;
			this.transition = DFA24_transition;
		}

		public override string Description { get { return "1:1: Tokens : ( T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | T__116 | T__117 | T__118 | T__119 | T__120 | T__121 | T__122 | T__123 | T__124 | T__125 | T__126 | T__127 | T__128 | T__129 | T__130 | T__131 | T__132 | T__133 | T__134 | T__135 | T__136 | T__137 | T__138 | T__139 | T__140 | T__141 | T__142 | T__143 | T__144 | T__145 | T__146 | T__147 | T__148 | T__149 | T__150 | T__151 | T__152 | T__153 | T__154 | T__155 | T__156 | T__157 | T__158 | T__159 | T__160 | TRANSACT_FIELD | OR | AND | NOT | INT | DEC | STRING | COMPARE | IN | CONTAINS | EQUAL | OPEQUAL | IDENTIFIER | WS | COMMENTS );"; } }

		public override void Error(NoViableAltException nvae)
		{
			DebugRecognitionException(nvae);
		}
	}

 
	#endregion

}
