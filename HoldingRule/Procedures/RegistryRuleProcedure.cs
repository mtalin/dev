﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Holding.Procedures
{
	class RegistryRuleProcedure: Procedure
	{
		private readonly int _rule;
		private readonly Expression _count;
		private readonly Expression _currency;
		private readonly Expression _asset;

		public RegistryRuleProcedure(int rule, Expression count, Expression currency, Expression asset): base("InlineQuery")
		{
			_rule = rule;
			_count = count;
			_currency = currency;
			_asset = asset;
		}

		public override bool Prepare(Program context)
		{
			if (_asset.GetText(context) == context.GetVarText("t.Asset", false))
				context.Declaration.UseVariable("t.AssetType");
			else if (_asset.GetText(context) == context.GetVarText("t.Asset2", false))
				context.Declaration.UseVariable("t.Asset2Type");
			return base.Prepare(context);
		}

		public override string GetText(Program context, int indent)
		{
			string amount = _count.GetText(context);
			string amount2 = amount;
			string template;
			if (_count.IsUnarySimple)
			{
				template =
					"if (${{[AAmount]}} <> 0)\n" +
					"	insert into ${{[$Registry]}} (RegistryRule, Amount, Currency, Asset, AssetType,\n" +
					"		Transact, TransactType, DtTransact, Foundation, FoundationAccount, FiscalYear)\n" +
					"	values (${{[Rule]}}, ${{[Amount]}}, ${{[Currency]}}, ${{[Asset]}}, ${{[AssetType]}},\n" +
					"		${{[t.Id]}}, ${{[t.Type]}}, ${{[t.DtTransact]}}, ${{[t.Foundation]}}, ${{[t.FoundationAccount]}}, ${{[t.FiscalYear]}});";
				if (amount2.StartsWith("-"))
					amount2 = amount2.Substring(1);
			}
			else
			{
				template =
					"set ${{[r.Value]}} = ${{[Amount]}};\n" +
					"if (${{[r.Value]}} <> 0)\n" +
					"	insert into ${{[$Registry]}} (RegistryRule, Amount, Currency, Asset, AssetType,\n" +
					"		Transact, TransactType, DtTransact, Foundation, FoundationAccount, FiscalYear)\n" +
					"	values (${{[Rule]}}, ${{[r.Value]}}, ${{[Currency]}}, ${{[Asset]}}, ${{[AssetType]}},\n" +
					"		${{[t.Id]}}, ${{[t.Type]}}, ${{[t.DtTransact]}}, ${{[t.Foundation]}}, ${{[t.FoundationAccount]}}, ${{[t.FiscalYear]}});";
				context.Declaration.UseVariable("r.Value");
			}
			string asset = _asset.GetText(context);
			string assetType;
			if (asset == context.GetVarText("USD", false))
				assetType = context.GetVarText("CurrencyType");
			else if (asset == context.GetVarText("t.Asset", false))
				assetType = context.GetVarText("t.AssetType");
			else if (asset == context.GetVarText("t.Asset2", false))
				assetType = context.GetVarText("t.Asset2Type");
			else
				assetType = "isnull((select Type from Assets where ID=" + asset + "), 0)";

			Dictionary<string, string> locals = new Dictionary<string, string>()
			{
				{"Rule",		_rule.ToString()},
				{"Amount",		amount},
				{"AAmount",		amount2},
				{"Currency",	_currency.GetText(context)},
				{"Asset",		asset},
				{"AssetType",	assetType},
			};

			return context.MapTemplate(indent, template, locals);
		}
	}
}
