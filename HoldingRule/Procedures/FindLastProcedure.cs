﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Procedures
{
	class FindLastProcedure: Procedure
	{
		private readonly Expression _asset;

		public FindLastProcedure()
			: base("FindLast")
		{
		}

		public FindLastProcedure(Expression asset)
			: base("FindLast")
		{
			_asset = asset;
		}

		public override string GetText(Program context, int indent)
		{
			context.SetValue("x.FirstLot", true);
			context.SetValue("x.LastLot", true);
			context.SetValue("x.Found", true);
			context.SetValue("h.DtDeposit", true);
			context.SetValue("h.UnitsNumber", true);
			context.SetValue("h.SalePrice", true);
			context.SetValue("h.BookBasis", true);
			context.SetValue("h.CostBasis", true);
			context.SetValue("h.Interest", true);
			context.SetValue("h.Dividend", true);
			context.SetValue("h.CapitalGain", true);
			context.SetValue("h.PhantomGain", true);
			context.SetValue("h.InterestAdj", true);
			return context.MapTemplate(indent,
				"/*** begin FindLast procedure ***/\n" +
				"select\n" +
				"	${{[x.LastLot]}} = ID,\n" +
				"	${{[x.FirstLot]}} = FirstLot,\n" +
				"	${{[h.DtDeposit]}} = DtDeposit,\n" +
				"	${{[h.UnitsNumber]}} = UnitsNumber,\n" +
				"	${{[h.SalePrice]}} = V_SalePrice,\n" +
				"	${{[h.BookBasis]}} = V_BookBasis,\n" +
				"	${{[h.CostBasis]}} = V_CostBasis,\n" +
				"	${{[h.Interest]}} = V_Interest,\n" +
				"	${{[h.Dividend]}} = V_Dividend,\n" +
				"	${{[h.CapitalGain]}} = V_CapitalGain,\n" +
				"	${{[h.PhantomGain]}} = V_PhantomGain,\n" +
				"	${{[h.InterestAdj]}} = V_InterestAdj\n" +
				"from ${{[$Holding]}}\n" +
				"where ID = (select max(ID) from ${{[$Holding]}} where Asset = " + 
					(_asset == null ? (context.HasValue("h.Asset") ? "${{[h.Asset]}}": "${{[t.Asset]}}"): _asset.GetText(context)) +
					" and FoundationAccount = ${{[t.FoundationAccount]}} and DtClosed is null);\n" +
				"if (@@rowcount = 0)\n" +
				"	begin\n" +
				"	set ${{[x.Found]}} = 0;\n" +
				"	set ${{[x.LastLot]}} = null;\n" +
				"	set ${{[x.FirstLot]}} = null;\n" +
				"	set ${{[h.DtDeposit]}} = null;\n" +
				"	set ${{[h.UnitsNumber]}} = 0;\n" +
				"	set ${{[h.SalePrice]}} = 0;\n" +
				"	set ${{[h.BookBasis]}} = 0;\n" +
				"	set ${{[h.CostBasis]}} = 0;\n" +
				"	set ${{[h.Interest]}} = 0;\n" +
				"	set ${{[h.Dividend]}} = 0;\n" +
				"	set ${{[h.CapitalGain]}} = 0;\n" +
				"	set ${{[h.PhantomGain]}} = 0;\n" +
				"	set ${{[h.InterestAdj]}} = 0;\n" +
				"	end;\n" +
				"else\n" +
				"	set ${{[x.Found]}} = 1;\n" +
				"/*** end FindLast procedure ***/");
		}
	}
}
