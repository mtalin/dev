﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Procedures
{
	class ErrorProcedure: Procedure
	{
		private readonly Expression _message;

		public ErrorProcedure(Expression message)
			: base("Error")
		{
			_message = message;
		}

		private static int _errorId = 100;
		public override string GetText(Program context, int indent)
		{
			context.SetValue("ErrorInfo", true);
			context.SetValue("ErrorId", true);
			return context.MapTemplate(indent,
				"set ${{[ErrorInfo]}} = " + (_message == null ? "Enspecified Exception": _message.GetText(context)) + ";\n" +
				"set ${{[ErrorId]}} = " + (++_errorId).ToString() + ";\n" +
				"goto Error;"
				);
		}
	}
}
