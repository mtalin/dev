﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Holding.Procedures
{
	class InlineQueryProcedure: Procedure
	{
		private readonly string _query;

		public InlineQueryProcedure(string query): base("InlineQuery")
		{
			_query = (query ?? "").TrimEnd().TrimEnd(';');
		}

		private static readonly Regex _regVar = new Regex(@"\$\{((?:[a-zA-Z]\.)?[a-zA-Z_]\w*)\}");
		private static IList<string> ExpandVariables(string query)
		{
			List<string> vars = new List<string>();
			_regVar.Replace(query, m =>
				{
					string s = m.Groups[1].Value;
					if (!vars.Contains(s.ToUpperInvariant()))
						vars.Add(s);
					return "";
				});
			return vars;
		}

		private static string PrepareVariables(string query)
		{
			return _regVar.Replace(query, m => "${{[" + m.Groups[1].Value + "]}}");
		}

		public override string GetText(Program context, int indent)
		{
			return context.MapTemplate(indent, PrepareVariables(_query)) + ";";
		}
	}
}
