﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Holding.Procedures
{
	class DistributeProcedure: Procedure
	{
		private readonly string[] _fieldsToDistribute;

		private static readonly string[] _values = { "SalePrice", "CostBasis", "BookBasis", "Interest", "Dividend", "CapitalGain", "PhantomGain", "InterestAdj" };

		public DistributeProcedure(params string[] fieldsToDistribute): base("Distribute")
		{
			_fieldsToDistribute = fieldsToDistribute;
		}

		public override bool Prepare(Program context)
		{
			return base.Prepare(context);
		}

		private bool MustDistribute(Program context, string name)
		{
			if (_fieldsToDistribute == null || _fieldsToDistribute.Length == 0)
				return context.HasValue("w." + name);
			return 0 <= Array.FindIndex(_fieldsToDistribute, x => String.Compare(x, name, StringComparison.OrdinalIgnoreCase) == 0);
		}

		public override string GetText(Program context, int indent)
		{
			bool found = false;
			StringBuilder sb = new StringBuilder();
			sb.Append("/*** begin Distribute block ***/\n");
			string tem = "set ${{[w.#]}} = isnull(${{[h.#]}}, 0);\n";
			foreach (var item in _values)
			{
				if (MustDistribute(context, item))
				{
					sb.Append(tem.Replace("#", item));
					found = true;
				}
			}
			if (MustDistribute(context, "UnitsNumber"))
			{
				sb.Append("set ${{[w.UnitsNumber]}} = isnull(${{[h.UnitsNumber]}}, ${{[t.UnitsNumber]}});\n");
				found = true;
			}

			if (found)
			{
				sb.Append("set ${{[u.UnitsLeft]}} = isnull((select sum(isnull(UnitsNumber, 0)) from ${{[$Distribution]}}), 0);\n");
				sb.Append("update ${{[$Distribution]}}\nset ${{[u.lotSize]}} = UnitsNumber,\n");
				tem = "	${{[d.#]}} = case when ${{[u.UnitsLeft]}} > ${{[u.lotSize]}} then round(${{[u.lotSize]}} * ${{[w.#]}} / ${{[u.UnitsLeft]}}, %) else" +
					" ${{[w.#]}} end,\n" +
					"	V_# = V_# + ${{[d.#]}},\n" +
					"	${{[w.#]}} = ${{[w.#]}} - ${{[d.#]}},\n";
				foreach (var item in _values)
				{
					if (MustDistribute(context, item))
						sb.Append(tem.Replace('%', '2').Replace("#", item));
				}
				if (MustDistribute(context, "UnitsNumber"))
					sb.Append(tem.Replace('%', '4').Replace("V_#", "UnitsNumber").Replace("#", "UnitsNumber"));
				sb.Append("	${{[u.UnitsLeft]}} = ${{[u.UnitsLeft]}} - ${{[u.lotSize]}};\n");
				sb.Append("/*** end Distribute block ***/");
			}
			else
			{
				sb.Length = 0;
				sb.Append("/*** Distribute block: Nothing to destribute ***/");
			}

			return context.MapTemplate(indent, sb.ToString());
		}
	}

	class ApplyDistributionProcedure: Procedure
	{
		public ApplyDistributionProcedure(): base("ApplyDistribution")
		{
		}

		public override bool Prepare(Program context)
		{
			return base.Prepare(context);
		}

		public override string GetText(Program context, int indent)
		{
			string result =
				"/*** begin ApplyDistribution block ***/\n" +
				"insert into ${{[$Holding]}}\n" +
				"	(\n" +
				"	Foundation, FoundationAccount, Asset, AssetType,\n" +
				"	FirstLot, ParentLot,\n" +
				"	DtCreated, DtDeposit, DtClosed,\n" +
				"	HasChild, IsRoot,\n" +
				"	Transact, TransactType,\n" +
				"	UnitsNumber, PackageSize,\n" +
				"	V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,\n" +
				"	V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj\n" +
				"	)\n" +
				"select\n" +
				"	${{[t.Foundation]}}, ${{[t.FoundationAccount]}}, " +
					(context.HasValue("h.Asset") ? "${{[h.Asset]}}": "${{[t.Asset]}}") + ", " +
					(context.HasValue("h.AssetType") ? "${{[h.AssetType]}}" : "${{[t.AssetType]}}") + ",\n" +
				"	FirstLot, ParentLot,\n" +
				"	${{[t.DtTRansact]}}, DtDeposit, DtClosed,\n" +
				"	0, case when ParentLot is null then 1 else 0 end,\n" +
				"	${{[t.ID]}}, ${{[t.Type]}},\n" +
				"	UnitsNumber, PackageSize,\n" +
				"	V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,\n" +
				"	V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj\n" +
				"from ${{[$Distribution]}}\n" +
				"order by ID;\n" +
				"update ${{[$Holding]}}\n" +
				"set DtClosed = ${{[t.DtTransact]}}, HasChild = 1\n" +
				"where ID in (select ParentLot from @Distribution)\n" +
				"if (@@rowcount = 0)\n" +
				"	begin\n" +
				"	set ${{[x.firstLot]}} = scope_identity()\n" +
				"	update ${{[$Holding]}} set FirstLot = ${{[x.firstLot]}} where ID = ${{[x.firstLot]}} and FirstLot is null;\n" +
				"	end\n" +
				"/*** end ApplyDistribution block ***/";
			return context.MapTemplate(indent, result);
		}
	}
}
