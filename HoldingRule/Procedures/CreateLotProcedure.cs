﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Holding.Procedures
{
	class CreateLotProcedure: Procedure
	{
		private readonly Expression _asset;

		public CreateLotProcedure(): base("CreateLot")
		{
		}

		public CreateLotProcedure(Expression asset): base("CreateLot")
		{
			_asset = asset;
		}

		public override string GetText(Program context, int indent)
		{
			string template =
				"/*** begin CreateLot procedure ***/\n" +
				"insert into ${{[$Holding]}}\n" +
				"	(\n" +
				"	Foundation, FoundationAccount, Asset, AssetType,\n" +
				"	FirstLot, ParentLot,\n" +

				"	UnitsNumber, PackageSize,\n" +

				"	DtCreated, DtDeposit, DtClosed,\n" +
				"	HasChild, IsRoot,\n" +

				"	Transact, TransactType,\n" +

				"	V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,\n" +
				"	V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj\n" +
				"	)\n" +
				"values\n" +
				"	(\n" +
				"	${{[t.Foundation]}}, ${{[t.FoundationAccount]}}, " +
					(_asset == null ?
						(context.HasValue("h.Asset") ? "${{[h.Asset]}}": "${{[t.Asset]}}"):
						_asset.GetText(context)) + ", " +
					(_asset == null ?
						(context.HasValue("h.AssetType") ? "${{[h.AssetType]}}" : "${{[t.AssetType]}}"):
						_asset.GetText(context) == context.GetVarText("t.Asset") ? "${{[t.AssetType]}}":
						_asset.GetText(context) == context.GetVarText("h.Asset") ? "${{[h.AssetType]}}":
						"(select Type from Assets where ID=" + _asset.GetText(context) + ")") + ",\n" +
				"	${{[x.FirstLot]}}, ${{[x.LastLot]}},\n" +

				"	" + (context.HasValue("h.UnitsNumber") ? "isnull(${{[h.UnitsNumber]}}, ${{[t.UnitsNumber]}})": "${{[t.UnitsNumber]}}") + ", " +
						(context.HasValue("h.UnitsNumber") ? "isnull(${{[h.UnitsNumber]}}, ${{[t.UnitsNumber]}})": "${{[t.UnitsNumber]}}") + ",\n" +
				"	${{[t.DtTransact]}}, " +
						(context.HasValue("h.DtDeposit") ? "coalesce(${{[h.DtDeposit]}}, ${{[t.DtDeposit]}}, ${{[t.DtTransact]}}), " : "isnull(${{[t.DtDeposit]}}, ${{[t.DtTransact]}}), ") + "null,\n" +
				"	0, case when ${{[x.LastLot]}} is null then 1 else 0 end,\n" +

				"	${{[t.ID]}}, ${{[t.Type]}},\n" +

				"	" + (context.HasValue("h.SalePrice") ? "isnull(${{[h.SalePrice]}}, 0), ": "0, ") +
						(context.HasValue("h.CostBasis") ? "isnull(${{[h.CostBasis]}}, 0), " : "0, ") +
						(context.HasValue("h.BookBasis") ? "isnull(${{[h.BookBasis]}}, 0), " : "0, ") +
						(context.HasValue("h.Interest") ? "isnull(${{[h.Interest]}}, 0),\n" : "0,\n") +
				"	" + (context.HasValue("h.Dividend") ? "isnull(${{[h.Dividend]}}, 0), " : "0, ") +
						(context.HasValue("h.CapitalGain") ? "isnull(${{[h.CapitalGain]}}, 0), " : "0, ") +
						(context.HasValue("h.PhantomGain") ? "isnull(${{[h.PhantomGain]}}, 0), " : "0, ") +
						(context.HasValue("h.InterestAdj") ? "isnull(${{[h.InterestAdj]}}, 0)\n" : "0\n") +
				"	);\n" +

				"if (${{[x.FirstLot]}} is null)\n" +
				"	begin\n" +
				"	set ${{[x.LastLot]}} = scope_identity();" +
				"	update ${{[$Holding]}} set FirstLot = ID where ID = ${{[x.LastLot]}};\n" +
				"	set ${{[x.FirstLot]}} = ${{[x.LastLot]}};\n" +
				"	end\n" +
				"else\n" +
				"	begin\n" +
				"	update ${{[$Holding]}} set DtClosed = ${{[t.DtTransact]}}, HasChild = 1 where ID = ${{[x.LastLot]}};\n" +
				"	set ${{[x.LastLot]}} = scope_identity();" +
				"	end\n" +
				"\n" +
				"/*** end CreateLot procedure ***/\n";

			string result = context.MapTemplate(indent, template);
			context.SetValue("x.FirstLot", true);
			context.SetValue("x.LastLot", true);
			return result;
		}
	}
}
