﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Procedures
{
	class MoveAssetsProcedure: Procedure
	{
		private readonly Expression _source;

		public MoveAssetsProcedure(Expression source): base("MoveAssets")
		{
			_source = source;
		}

		public override string GetText(Program context, int indent)
		{
			return context.MapTemplate(indent, _template.Replace("${source}", _source.GetText(context)));
		}

		private static string _template =
@"
/*** begin MoveAssets(${source}) procedure ***/
insert into ${{[$Holding]}}
	(
	Foundation, FoundationAccount, Asset, AssetType,
	FirstLot, ParentLot,
	DtCreated, DtDeposit, DtClosed,
	HasChild, IsRoot,
	Transact, TransactType,
	UnitsNumber, PackageSize,
	V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,
	V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj
	)
select ${{[t.Foundation]}}, ${{[t.FoundationAccount]}}, Asset, AssetType,
	FirstLot, null,
	${{[t.DtTransact]}}, DtDeposit, null,
	0, 1,
	${{[t.ID]}}, ${{[t.Type]}},
	UnitsNumber, UnitsNumber,
	V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,
	V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj
from ${{[$Holding]}}
where ID in (select ParentLot from Holdings where Transact = ${source} and HasChild = 0 and DtClosed is not null);

insert into ${{[$Registry]}}
	(
	RegistryRule, Amount, Currency, Asset,
	Transact, TransactType, DtTransact, AssetType,
	Foundation, FoundationAccount, FiscalYear
	)
select
	RegistryRule, -Amount, Currency, Asset,
	${{[t.ID]}}, ${{[t.Type]}}, ${{[t.DtTransact]}}, AssetType,
	${{[t.Foundation]}}, ${{[t.FoundationAccount]}}, FiscalYear
from ${{[$Registry]}}
where Transact = ${source};
/*** end MoveAssets procedure ***/
".Replace(Environment.NewLine, "\n");
	}
}
