﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Procedures
{
	class CloseLotProcedure: Procedure
	{
		public CloseLotProcedure(): base("CloseLot")
		{
		}

		public override string GetText(Program context, int indent)
		{
			return context.MapTemplate(indent, "/*** begin CloseLot procedure ***/\n" +
				"if (${{[x.LastLot]}} is not null)\n" +
				"	update ${{[$Holding]}}\n" +
				"	set DtClosed = ${{[t.DtTransact]}}\n" +
				"	where id = ${{[x.LastLot]}};\n" +
				"/*** end CloseLot procedure ***/");
		}
	}
}
