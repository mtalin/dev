﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Procedures
{
	class SplitCostBasisProcedure: Procedure
	{
		private readonly DistributeProcedure _distribute;
		private readonly ApplyDistributionProcedure _apply;

		public SplitCostBasisProcedure(): base("SplitCostBasis")
		{
			_distribute = new DistributeProcedure("BookBasis", "Interest");
			_apply = new ApplyDistributionProcedure();
		}

		public override bool Prepare(Program context)
		{
			bool result = _distribute.Prepare(context);
			if (!_apply.Prepare(context))
				result = false;
			if (!base.Prepare(context))
				result = false;
			return result;
		}

		public override string GetText(Program context, int indent)
		{
			string nl = context.IndentNewLineString(indent);
			string s =
				"/*** begin SplitCostBasis procedure ***/\n" +
				"insert into ${{[$Distribution]}}\n" +
				"	(\n" +
				"	FirstLot, ParentLot,\n" +
				"	DtDeposit, DtClosed,\n" +
				"	UnitsNumber, PackageSize,\n" +
				"	V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,\n" +
				"	V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj\n" +
				"	)\n" +
				"select\n" +
				"	${{[x.FirstLot]}}, ${{[x.LastLot]}},\n" +
				"	coalesce(DtDeposit, " + (context.HasValue("h.DtDeposit") ? "${{[h.DtDeposit]}}, ": "") + "${{[t.DtDeposit]}}, ${{[t.DtTransact]}}), null,\n" +
				"	UnitsNumber, " + (context.HasValue("h.UnitsNumber") ? "isnull(${{[h.UnitsNumber]}}, ${{[t.UnitsNumber]}})": "${{[t.UnitsNumber]}}") + ",\n" +
				"	0, round(LotPrice, 2), 0, 0,\n" +
				"	0, 0, 0, 0\n" +
				"from CostBases\n" +
				"where Transact = ${{[t.ID]}}\n" +
				"order by SortOrder;\n" +

				"if (@@rowcount > 0)\n" +
				"	begin\n" +

				"	set ${{[u.UnitsLeft]}} = isnull((select sum(isnull(UnitsNumber, 0)) from ${{[$Distribution]}}), 0);\n" +

				"	if (${{[u.UnitsLeft]}} < ${{[t.UnitsNumber]}})\n" +
				"		insert into ${{[$Distribution]}}\n" +
				"			(\n" +
				"			FirstLot, ParentLot,\n" +
				"			DtDeposit, DtClosed,\n" +
				"			UnitsNumber, PackageSize,\n" +
				"			V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,\n" +
				"			V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj\n" +
				"			)\n" +
				"		values\n" +
				"			(\n" +
				"			${{[x.FirstLot]}}, ${{[x.LastLot]}},\n" +
				"			isnull(${{[t.DtDeposit]}}, ${{[t.DtTransact]}}), null,\n" +
				"			${{[t.UnitsNumber]}} - ${{[u.UnitsLeft]}}, ${{[u.UnitsNumber]}},\n" +
				"			0, 0, 0, 0,\n" +
				"			0, 0, 0, 0\n" +
				"			);\n"
				;
			StringBuilder sb = new StringBuilder(context.MapTemplate(indent, s));
			sb.Append(context.NewLine);
			sb.Append(context.NewLine).Append(_distribute.GetText(context, indent + 1));
			sb.Append(context.NewLine);
			sb.Append(context.NewLine).Append(_apply.GetText(context, indent + 1));
			sb.Append(context.NewLine);
			sb.Append(nl).Append("end;");
			sb.Append(nl).Append("/*** end SplitCostBasis procedure ***/");
			return sb.ToString();
		}
	}
}
