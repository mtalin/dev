﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Procedures
{
	class UpdateCashProcedure: Procedure
	{
		private readonly Expression _amount;
		private readonly Procedure _findLast;
		private readonly Procedure _createLot;

		public UpdateCashProcedure(Expression amount): base("UpdateCash")
		{
			_amount = amount;
			_findLast = new FindLastProcedure(Expression.Value(ExpressionType.Integer, "1059"));
			_createLot = new CreateLotProcedure();
		}

		public override bool Prepare(Program context)
		{
			_findLast.Prepare(context);
			_createLot.Prepare(context);
			return base.Prepare(context);
		}

		public override string GetText(Program context, int indent)
		{
			string nl = context.IndentNewLineString(indent);
			StringBuilder sb = new StringBuilder();
			sb.Append(context.IndentString(indent)).Append("/*** begin UpdateCash procedure ***/");
			sb.Append(context.NewLine);
			sb.Append(_findLast.GetText(context, indent));
			sb.Append(context.NewLine);
			sb.Append(context.MapTemplate(indent,
				"set ${{[h.UnitsNumber]}} = ${{[h.UnitsNumber]}} + " + (_amount == null ? "${{[t.Amount]}}": _amount.GetText(context)) + ";\n" +
				"set ${{[h.Asset]}} = ${{[USD]}};\n" +
				"set ${{[h.AssetType]}} = ${{[CurrencyType]}};\n"));
			context.SetValue("h.Asset", true);
			context.SetValue("h.AssetType", true);
			context.SetValue("h.UnitsNumber", true);
			sb.Append(context.NewLine);
			sb.Append(_createLot.GetText(context, indent));
			sb.Append(nl).Append("/*** end UpdateCash procedure ***/");
			return sb.ToString();
		}
	}
}
