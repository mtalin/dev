﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Procedures
{
	class PrintProcedure: Procedure
	{
		private readonly Expression _value;
		public PrintProcedure(Expression value): base("Print")
		{
			_value = value;
		}

		public override string GetText(Program context, int indent)
		{
			return context.IndentString(indent) + "print " + _value.GetText(context) + ";";
		}
	}
}
