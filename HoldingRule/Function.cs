﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding
{
	public class Function: Expression
	{
		private static readonly Expression[] NoParameters = new Expression[0];

		protected Function(ExpressionType type, string name)
			: base(type, name, ExpressionOp.PriorityTop, null)
		{
			Name = name;
			Parameters = NoParameters;
		}

		protected Function(ExpressionType type, string name, string text, ICollection<Expression> parameters)
			: base(type, text ?? name, ExpressionOp.PriorityTop, CollectVariables(null, parameters))
		{
			Name = name;
			Parameters = parameters ?? NoParameters;
		}

		protected Function(ExpressionType type, string name, string text, ICollection<Expression> parameters, ICollection<string> variables)
			: base(type, text ?? name, ExpressionOp.PriorityTop, CollectVariables(variables, parameters))
		{
			Name = name;
			Parameters = parameters ?? NoParameters;
		}

		public virtual string Name { get; }

		public ICollection<Expression> Parameters { get; }

		public override bool IsSimple => false;

		public override bool IsUnarySimple => false;

		private static ICollection<string> CollectVariables(ICollection<string> variables, ICollection<Expression> parameters)
		{
			if (parameters == null || parameters.Count == 0)
				return variables;
			var result = variables == null ? new List<string>(): new List<string>(variables);
			foreach (var parameter in parameters)
			{
				if (parameter?.Variables != null && parameter.Variables.Any())
				{
					foreach (var item in parameter.Variables)
					{
						if (item != null && !result.Contains(item))
							result.Add(item);
					}
				}
			}
			return result.Count > 0 ? result: null;
		}

		#region Known Functions
		public static Function Abs(Expression value)
		{
			return new Functions.AbsFunction(value);
		}

		public static Function Sum(Expression asset, string field)
		{
			return new Functions.SumFunction(asset, field);
		}

		public static Function Coalesce(params Expression[] values)
		{
			return new Functions.CoalesceFunction(values);
		}

		public static Function FmvPrice(Expression asset, Expression date)
		{
			return new Functions.FmvPriceFunction(asset, date);
		}

		public static Function If(Expression testExpression, Expression thenPart, Expression elsePart)
		{
			return new Functions.IfFunction(testExpression, thenPart, elsePart);
		}

		public static Function Round(Expression value, Expression digits)
		{
			return new Functions.RoundFunction(value, digits);
		}

		public static Function Round(Expression value)
		{
			return new Functions.RoundFunction(value);
		}

		public static Function Min(Expression x, Expression y)
		{
			return new Functions.MinMaxFunction(true, x, y);
		}

		public static Function Max(Expression x, Expression y)
		{
			return new Functions.MinMaxFunction(false, x, y);
		}

		public static Function DateTime()
		{
			return new Function(ExpressionType.DateTime, "datetime",
				"getdate()",
				null,
				null);
		}

		public static Function Date()
		{
			return new Function(ExpressionType.Date, "date",
				"convert(date, getdate())",
				null,
				null);
		}

		public static Function Time()
		{
			return new Function(ExpressionType.Time, "time",
				"convert(time, getdate())",
				null,
				null);
		}

		public static Function Found()
		{
			return new Functions.FoundFunction("x.Found");
		}

		public static Function Distributed()
		{
			return new Functions.FoundFunction("x.Distributed");
		}
		#endregion
	}
}
