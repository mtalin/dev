﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding
{
	public class Declaration
	{
		private readonly Dictionary<string, Identifier> _global = InitializeGlobal();
		private readonly List<Dictionary<string, Identifier>> _locals = new List<Dictionary<string, Identifier>>();
		private readonly HashSet<string> _used = new HashSet<string>();
		private int _localVarsCounter;

		public IEnumerable<string> Externals { get; set; }

		public void PushLocals(Dictionary<string, Identifier> locals)
		{
			_locals.Add(locals);
		}

		public Dictionary<string, Identifier> PopLocals()
		{
			Dictionary<string, Identifier> top = _locals[_locals.Count - 1];
			_locals.RemoveAt(_locals.Count - 1);
			return top;
		}

		public bool IsDeclared(string name)
		{
			return _global.ContainsKey(name.ToUpperInvariant());
		}

		public Identifier Declare(ExpressionType type, string name)
		{
			string key = name.ToUpperInvariant();
			Identifier id = new Identifier(type, name, "@i_" + name);
			if (_global.ContainsKey(key))
				throw new SyntaxErrorException(0, 0, $"Variable {name} has been declared already");
			_global[key] = id;
			if (id.Type.LocalIdentifiers != null)
			{
				foreach (var item in id.Type.LocalIdentifiers)
				{
					if (item != null)
					{
						key = item.Name.ToUpperInvariant();
						_global[key] = item;
					}
				}
			}
			return id;
		}

		public Identifier TemporaryIdentifier(ExpressionType type)
		{
			string tempName = (++_localVarsCounter).ToString("$0");
			Identifier temp = Declare(type, tempName);
			_used.Add(tempName);
			return temp;
		}

		public string TemporaryName(bool decorate)
		{
			return (++_localVarsCounter).ToString(decorate ? "@i__$0": "_$0");
		}


		public void Define(string name, string value)
		{
			string key = name.ToUpperInvariant();
			_global[key] = new Identifier(ExpressionType.Undefined, name, value, true);
			_used.Add(key);
		}

		public Identifier FindIdentifier(string name)
		{
			return FindIdentifier(name, true);
		}

		public Identifier FindIdentifier(string name, bool use)
		{
			Identifier id;
			string key = name.ToUpperInvariant();
			for (int i = _locals.Count - 1; i >= 0; --i)
			{
				if (_locals[i].TryGetValue(key, out id))
				{
					if (use)
						_used.Add(key);
					return id;
				}
			}
			if (!_global.TryGetValue(key, out id))
				throw new SyntaxErrorException(0, 0, $"Variable {name} has not been declared");
			if (use)
				_used.Add(key);
			return id;
		}

		public void UseVariable(string name)
		{
			FindIdentifier(name);
		}

		public void UseVariablesRange(IEnumerable<string> variables)
		{
			if (variables != null)
			{
				foreach (var item in variables)
				{
					if (item != null)
						UseVariable(item);
				}
			}
		}

		public bool IsUsed(string name)
		{
			return _used.Contains(name.ToUpperInvariant());
		}

		private static readonly List<string> _items1 = new List<string>()
			{ "t.QTransaction", "t.DtDeposit", "t.FoundationAccount2" };
		private static readonly List<string> _items2 = new List<string>()
			{ "t.Principal", "t.SplitNumerator", "t.SplitDenominator", "t.Ratio", "t.Interest", "t.CostBasis", "t.BookBasis" };
		private static readonly List<string> _items3 = new List<string>()
			{ "h.CostBasis", "h.BookBasis", "h.SalePrice", "h.Interest", "h.Dividend", "h.CapitalGain", "h.InterestAdj", "h.PhantomGain" };
		public string GetInitialQuery(Program context, int indent)
		{
			StringBuilder sb = new StringBuilder("select");
			bool transactFiled = false;
			foreach (var item in _items1)
			{
				if (IsUsed(item))
				{
					sb.Append("\n\t${{[").Append(item).Append("]}} = ").Append(item).Append(',');
					context.SetValue(item, true);
					transactFiled = true;
				}
			}
			foreach (var item in _items2)
			{
				if (IsUsed(item))
				{
					sb.Append("\n\t${{[").Append(item).Append("]}} = abs(").Append(item).Append("),");
					context.SetValue(item, true);
					transactFiled = true;
				}
			}
			if (IsUsed("t.Asset2"))
			{
				sb.Append("\n\t${{[t.Asset2]}} = isnull(t.Asset2, ${{[USD]}}),");
				context.SetValue("t.Asset2", true);
				transactFiled = true;
			}
			if (IsUsed("t.Asset2Type"))
			{
				sb.Append("\n\t${{[t.Asset2Type]}} = b.Type,");
				context.SetValue("t.Asset2Type", true);
				transactFiled = true;
			}
			if (IsUsed("t.Asset2Flags"))
			{
				sb.Append("\n\t${{[t.Asset2Flags]}} = y.TheValue,");
				context.SetValue("t.Asset2Flags", true);
				transactFiled = true;
			}
			if (IsUsed("t.AssetType"))
			{
				sb.Append("\n\t${{[t.AssetType]}} = a.Type,");
				context.SetValue("t.AssetType", true);
			}
			if (IsUsed("t.AssetFlags"))
			{
				sb.Append("\n\t${{[t.AssetFlags]}} = x.TheValue,");
				context.SetValue("t.AssetFlags", true);
			}

			if (sb.Length < 10)
				return "";

			sb.Length = sb.Length - 1;
			if (transactFiled)
			{
				sb.Append("\nfrom Transacts t");
				if (IsUsed("t.AssetFlags"))
					sb.Append("\n\tleft join Assets a on a.ID = isnull(t.Asset, ${{[USD]}})" +
						"\n\tleft join enums_AssetTypes x on x.ItemID = a.Type");
				else if (IsUsed("t.AssetType"))
					sb.Append("\n\tleft join Assets a on a.ID = isnull(t.Asset, ${{[USD]}})");
				if (IsUsed("t.Asset2Flags"))
					sb.Append("\n\tleft join Assets b on b.ID = isnull(t.Asset2, ${{[USD]}})" +
						"\n\tleft join enums_AssetTypes y on y.ItemID = b.Type");
				else if (IsUsed("t.Asset2Type"))
					sb.Append("\n\tleft join Assets b on b.ID = isnull(t.Asset2, ${{[USD]}})");
				sb.Append("\nwhere t.ID = ${{[t.ID]}};");
			}
			else
			{
				sb.Append("\nfrom Assets a" +
					"\n\tinner join enums_AssetTypes x on x.ItemID = a.Type" +
					"\nwhere a.ID = isnull(${{[t.Asset]}}, ${{[USD]}});");
			}

			sb.Append("\nif (@@rowcount = 0)\n" +
				"	begin\n" +
				"	set ${{[errorId]}} = 10;\n" +
				"	set ${{[errorInfo]}} = " + (transactFiled ? "'Cannot find transaction by ID'": "'Cannot find Asset by ID'") + ";\n" +
				"	goto Error;\n" +
				"	end");
			context.SetValue("errorInfo", true);
			context.SetValue("errorId", true);

			foreach (var item in _items3)
			{
				if (IsUsed(item))
				{
					sb.Append("\nset ${{[").Append(item).Append("]}} = 0;");
					context.SetValue(item, true);
				}
			}

			return context.MapTemplate(indent, sb.ToString());
		}

		public string GetText(Program context)
		{
			return GetText(context, 0);
		}

		public string GetText(Program context, int indent)
		{
			if (IsDeclared("h.Asset"))
				UseVariable("t.Asset");
			if (IsDeclared("h.UnitsNumber"))
				UseVariable("t.UnitsNumber");
			if (IsDeclared("h.Amount"))
				UseVariable("t.Amount");
			if (IsDeclared("h.DtDeposit"))
				UseVariable("t.DtDeposit");

			StringBuilder sb = new StringBuilder();
			if (_used.Count > 0)
			{
				string ind	= context.IndentString(indent);
				string nl	= context.IndentNewLineString(indent);
				string nl__ = context.IndentNewLineString(indent + 1);
				List<string> lines = new List<string>(_used.Count);
				List<Identifier> tables = new List<Identifier>();
				foreach (var item in _used)
				{
					Identifier id = _global[item];
					if (id.Type != ExpressionType.Undefined && (Externals == null || !Externals.Contains(id.Name, StringComparer.InvariantCultureIgnoreCase)))
						if (id.Type.TypeCode == ExpressionTypeCode.Table)
							tables.Add(id);
						else
							lines.Add(id.GetText(context) + " " + id.Type.SqlType);
				}
				if (tables.Count > 0)
				{
					tables.Sort((x, y) => String.Compare(x.Name, y.Name, StringComparison.OrdinalIgnoreCase));
					foreach (var item in tables)
					{
						sb.Append(ind).Append("declare ").Append(item.GetText(context))
							.Append(' ').Append(context.MapTemplate(indent, item.Type.SqlType).TrimStart())
							.Append(';').Append(context.NewLine);
					}

				}
				if (lines.Count > 0)
				{
					lines.Sort();
					sb.Append(ind).Append("declare");
					foreach (var item in lines)
					{
						sb.Append(nl__).Append(item).Append(',');
					}
					sb.Length = sb.Length - 1;
					sb.Append(';').Append(context.NewLine);
				}
			}

			return sb.ToString();
		}

		public bool Prepare(Program context)
		{
			return true;
		}

		#region Initialize Globals
		private static Dictionary<string, Identifier> _wellKnownVariables;

		private static Dictionary<string, Identifier> InitializeGlobal()
		{
			if (_wellKnownVariables == null)
			{
				_wellKnownVariables = new Dictionary<string, Identifier>();
				Add(ExpressionType.DistributionTable, "$Distribution", "@Distribution");
				Add(ExpressionType.Undefined, "$Holding", "dbo.Holdings");

				Add(ExpressionType.Money, "h.Amount", "@h_amount");
				Add(ExpressionType.Integer, "h.Asset", "@h_asset");
				Add(ExpressionType.Integer, "h.AssetType", "@h_assetType");
				Add(ExpressionType.Date, "h.DtDeposit", "@h_dtDeposit");
				Add(ExpressionType.Date, "h.DtClosed", "@h_dtClosed");
				Add(ExpressionType.Decimal, "h.UnitsNumber", "@h_unitsNumber");
				Add(ExpressionType.Decimal, "h.PackageSize", "@h_packageSize");

				Add(ExpressionType.Money, "h.SalePrice", "@h_salePrice");
				Add(ExpressionType.Money, "h.CostBasis", "@h_costBasis");
				Add(ExpressionType.Money, "h.BookBasis", "@h_bookBasis");
				Add(ExpressionType.Money, "h.Dividend", "@h_dividend");
				Add(ExpressionType.Money, "h.CapitalGain", "@h_capitalGain");
				Add(ExpressionType.Money, "h.PhantomGain", "@h_phantomGain");
				Add(ExpressionType.Money, "h.Interest", "@h_interest");
				Add(ExpressionType.Money, "h.InterestAdj", "@h_interestAdj");

				Add(ExpressionType.Decimal, "w.UnitsNumber", "@w_unitsNumber");
				Add(ExpressionType.Money, "w.SalePrice", "@w_salePrice");
				Add(ExpressionType.Money, "w.CostBasis", "@w_costBasis");
				Add(ExpressionType.Money, "w.BookBasis", "@w_bookBasis");
				Add(ExpressionType.Money, "w.Dividend", "@w_dividend");
				Add(ExpressionType.Money, "w.CapitalGain", "@w_capitalGain");
				Add(ExpressionType.Money, "w.PhantomGain", "@w_phantomGain");
				Add(ExpressionType.Money, "w.Interest", "@w_interest");
				Add(ExpressionType.Money, "w.InterestAdj", "@w_interestAdj");

				Add(ExpressionType.Decimal, "v.UnitsNumber", "@v_unitsNumber");
				Add(ExpressionType.Money, "v.SalePrice", "@v_salePrice", true);
				Add(ExpressionType.Money, "v.CostBasis", "@v_costBasis", true);
				Add(ExpressionType.Money, "v.BookBasis", "@v_bookBasis", true);
				Add(ExpressionType.Money, "v.Dividend", "@v_dividend", true);
				Add(ExpressionType.Money, "v.CapitalGain", "@v_capitalGain", true);
				Add(ExpressionType.Money, "v.PhantomGain", "@v_phantomGain", true);
				Add(ExpressionType.Money, "v.Interest", "@v_interest", true);
				Add(ExpressionType.Money, "v.InterestAdj", "@v_interestAdj", true);

				Add(ExpressionType.Money, "z.SalePrice", "@z_salePrice", true);
				Add(ExpressionType.Money, "z.CostBasis", "@z_costBasis", true);
				Add(ExpressionType.Money, "z.BookBasis", "@z_bookBasis", true);
				Add(ExpressionType.Money, "z.Dividend", "@z_dividend", true);
				Add(ExpressionType.Money, "z.CapitalGain", "@z_capitalGain", true);
				Add(ExpressionType.Money, "z.PhantomGain", "@z_phantomGain", true);
				Add(ExpressionType.Money, "z.Interest", "@z_interest", true);
				Add(ExpressionType.Money, "z.InterestAdj", "@z_interestAdj", true);

				Add(ExpressionType.Integer, "t.ID", "@t", true);
				Add(ExpressionType.Money, "t.Amount", "@t_amount", true);
				Add(ExpressionType.Integer, "t.Asset", "@t_asset", true);
				Add(ExpressionType.Integer, "t.AssetType", "@t_assetType", true);
				Add(ExpressionType.String150, "t.AssetFlags", "@t_assetFlags", true);
				Add(ExpressionType.Integer, "t.Asset2", "@t_asset2", true);
				Add(ExpressionType.Integer, "t.Asset2Type", "@t_asset2Type", true);
				Add(ExpressionType.String150, "t.Asset2Flags", "@t_asset2Flags", true);
				Add(ExpressionType.Decimal, "t.BookBasis", "@t_bookBasis", true);
				Add(ExpressionType.Decimal, "t.CostBasis", "@t_costBasis", true);
				Add(ExpressionType.Date, "t.DtDeposit", "@t_dtDeposit", true);
				Add(ExpressionType.Date, "t.DtTransact", "@t_dtTransact", true);
				Add(ExpressionType.Integer, "t.Foundation", "@t_foundation", true);
				Add(ExpressionType.Integer, "t.FoundationAccount", "@t_foundationAccount", true);
				Add(ExpressionType.Integer, "t.FoundationAccount2", "@t_foundationAccount2", true);
				Add(ExpressionType.Money, "t.Interest", "@t_interest", true);
				Add(ExpressionType.Money, "t.Principal", "@t_principal", true);
				Add(ExpressionType.Integer, "t.QTransaction", "@t_qTransaction", true);
				Add(ExpressionType.Float, "t.Ratio", "@t_ratio", true);
				Add(ExpressionType.Decimal, "t.SplitNumerator", "@t_splitNumerator", true);
				Add(ExpressionType.Decimal, "t.SplitDenominator", "@t_splitDenominator", true);
				Add(ExpressionType.Integer, "t.Type", "@t_type", true);
				Add(ExpressionType.Decimal, "t.UnitsNumber", "@t_unitsNumber", true);
				Add(ExpressionType.Decimal, "t.FiscalYear", "@t_fiscalYear", true);

				Add(ExpressionType.Integer, "x.FirstLot", "@x_firstLot", true);
				Add(ExpressionType.Integer, "x.LastLot", "@x_lastLot", true);
				Add(ExpressionType.Boolean, "x.Found", "@x_found", true);
				Add(ExpressionType.Boolean, "x.Distributed", "@x_distributed", true);

				Add(ExpressionType.Decimal, "r.Value", "@r_value", true);

				Add(ExpressionType.Integer, "ErrorId", "@errorId");
				Add(ExpressionType.String, "ErrorInfo", "@errorInfo");

				Add(ExpressionType.Undefined, "USD", "1059", true);
				Add(ExpressionType.Undefined, "CurrencyType", "10", true);
			}
			return new Dictionary<string, Identifier>(_wellKnownVariables);
		}

		private static void Add(ExpressionType type, string name, string text, bool readOnly = false)
		{
			string key = name.ToUpperInvariant();
			_wellKnownVariables.Add(key, new Identifier(type, name, text, readOnly));
			if (type.LocalIdentifiers != null)
			{
				foreach (var item in type.LocalIdentifiers)
				{
					if (item != null)
					{
						key = item.Name.ToUpperInvariant();
						_wellKnownVariables[key] = item;
					}
				}
			}
		}
		#endregion
	}
}
