﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Holding
{
	public class Statement
	{

		private static readonly string NnoText = String.Empty;
		private static readonly IEnumerable<string> NoVariables = new string[0];
		private static readonly IEnumerable<Statement> EmptyCollection = new Statement[0];

		private string Text { get; }
		public int Line { get; protected set; }
		public int Column { get; protected set; }

		protected Statement()
		{
			Text = NnoText;
		}

		protected Statement(int line)
		{
			Line = line;
		}

		protected Statement(string text)
		{
			Text = text;
		}

		protected virtual IEnumerable<string> Variables => NoVariables;

		public virtual IEnumerable<Statement> InnerStatements => EmptyCollection;

		public virtual bool Prepare(Program context)
		{
			context.Declaration.UseVariablesRange(Variables);
			bool prepared = true;
			foreach (var item in InnerStatements)
			{
				if (!item.Prepare(context))
					prepared = false;
			}
			return prepared;
		}

		public virtual void Reset()
		{
		}

		public virtual string GetText(Program context, int indent)
		{
			return context.MapTemplate(indent, Text);
		}

		#region Known statements
		private static readonly Statement _emptyStatement = new Statement();
		private static readonly Statement _returnStatement = new Statement("return 0;");

		public static Statement Empty()
		{
			return _emptyStatement;
		}

		public static Statement Return()
		{
			return _returnStatement;
		}

		public static Statement GoTo(string label)
		{
			return new Statement("goto " + label + ";");
		}

		public static Statement Label(string label)
		{
			return new Statement(label + ":");
		}

		public static Statements.AssignStatement Assign(List<Identifier> identifiers, char operation, Expression expression)
		{
			return new Statements.AssignStatement(identifiers, operation, expression);
		}

		public static Statements.AssignStatement Assign(Identifier identifier, Expression expression)
		{
			return new Statements.AssignStatement(new[] { identifier }, '=', expression);
		}

		public static Statements.DistributeStatement Distribute(Expression asset, Expression count, Statement body)
		{
			return new Statements.DistributeStatement(asset, count, body);
		}

		public static Statements.IfStatement If(Expression test, Statement thenPart, Statement elsePart)
		{
			return new Statements.IfStatement(test, thenPart, elsePart);
		}

		public static Statements.BlockStatement Block(StatementsList statements)
		{
			return new Statements.BlockStatement(statements);
		}
		#endregion
	}

	public class StatementsList: Statement, IEnumerable<Statement>
	{
		private readonly List<Statement> _list = new List<Statement>();

		public StatementsList()
		{
		}

		public StatementsList(int line)
		{
		}

		public override IEnumerable<Statement> InnerStatements
		{
			get { return _list; }
		}

		public void Add(Statement statement)
		{
			StatementsList lst = statement as StatementsList;
			if (lst != null)
			{
				foreach (var item in lst._list)
				{
					if (item != null)
					{
						if (Line == 0)
							Line = item.Line;
						_list.Add(item);
					}
				}
			}
			else if (statement != null) // TODO: Check that statement is not empty && statement.GetText().Length > 0)
			{
				if (Line == 0)
					Line = statement.Line;
				_list.Add(statement);
			}
		}

		public int Count
		{
			get { return _list.Count; }
		}

		public IList<Statement> List
		{
			get { return _list; }
		}

		public override string GetText(Program context, int indent)
		{
			StringBuilder sb = new StringBuilder();
			foreach (var item in _list)
			{
				string text = item.GetText(context, indent);
				if (text.Length > 0)
				{
					if (sb.Length > 0)
						sb.Append(context.NewLine);
					sb.Append(text);
				}
			}
			return sb.ToString();
		}

		#region IEnumerable<Statement> Members

		public IEnumerator<Statement> GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		#endregion
	}
}
