﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Statements
{
	public class BlockStatement: Statement
	{
		StatementsList _list;
		public BlockStatement(StatementsList statements)
		{
			_list = statements;
		}

		public override IEnumerable<Statement> InnerStatements => _list;

		public override string GetText(Program context, int indent)
		{
			return _list == null ? "": _list.GetText(context, indent);
		}

		public override bool Prepare(Program context)
		{
			return _list == null || _list.Prepare(context);
		}
	}
}
