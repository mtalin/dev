﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Holding.Statements
{
	public class AssignStatement: Statement
	{
		private readonly IList<Identifier> _identifiers;
		private readonly char _operation;
		private readonly Expression _expression;
		private Identifier _temp;

		public AssignStatement(IList<Identifier> identifiers, char operation, Expression expression)
		{
			if (identifiers == null)
				throw new ArgumentNullException(nameof(identifiers));
			if (identifiers.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(identifiers));
			if (!(operation == '+' || operation == '-' || operation == '*' || operation == '/' || operation == '='))
				throw new ArgumentOutOfRangeException(nameof(operation));
			if (expression == null)
				throw new ArgumentNullException(nameof(expression));
			List<string> ivars = new List<string>(expression.Variables);
			foreach (var item in identifiers)
			{
				if (!ivars.Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
					ivars.Add(item.Name);
			}
			Variables = ivars;
			_identifiers = identifiers;
			_expression = expression;
			_operation = operation;
		}

		protected override IEnumerable<string> Variables { get; }

		public override bool Prepare(Program context)
		{
			if (_identifiers.Count > 1 && _operation != '=' && !_expression.IsSimple)
			{
				_temp = context.Temporary(_identifiers[0].Type);
			}
			return base.Prepare(context);
		}

		public override string GetText(Program context, int indent)
		{
			return GetText(context, indent, false);
		}

		public string GetText(Program context, int indent, bool inner)
		{
			string ind = context.IndentString(indent);
			string indNL = context.IndentNewLineString(indent);
			StringBuilder sb = new StringBuilder();
			foreach (var item in _identifiers)
			{
				context.SetValue(item.Name, true);
			}
			string expressionText = _expression.GetText(context);

			if (inner)
			{
				foreach (var item in _identifiers)
				{
					sb.Append(indNL).Append(item.GetText(context)).Append(" = ").Append(expressionText).Append(",");
				}
				sb.Length = sb.Length - 1;
				return sb.ToString();
			}

			if (_identifiers.Count == 1)
			{
				sb.Append(ind).Append(BuildAssign(_identifiers[0].GetText(context), _operation, expressionText));
			}
			else if (_temp == null)
			{
				sb.Append(ind).Append(BuildAssign(_identifiers[0].GetText(context), _operation, expressionText));
				if (!_expression.IsSimple)
					expressionText = _identifiers[0].GetText(context);
				for (int i = 1; i < _identifiers.Count; ++i)
				{
					sb.Append(indNL).Append(BuildAssign(_identifiers[i].GetText(context), _operation, expressionText));
				}
			}
			else
			{
				sb.Append(ind).Append(BuildAssign(_temp.GetText(context), '=', expressionText));
				expressionText = _temp.GetText(context);
				foreach (var item in _identifiers)
				{
					sb.Append(indNL).Append(BuildAssign(item.GetText(context), _operation, expressionText));
				}
			}
			return sb.ToString();
		}

		private static string BuildAssign(string identifier, char operation, string expression)
		{
			return String.Format(CultureInfo.InvariantCulture, 
				operation == '=' ? "set {0} = {2};": "set {0} = {0} {1} ({2});", identifier, operation, expression);
		}
	}
}
