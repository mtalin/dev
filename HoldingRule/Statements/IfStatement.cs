﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Holding.Statements
{
	public class IfStatement: Statement
	{
		private readonly Expression _test;
		private readonly Statement _then;
		private readonly Statement _else;
		private List<Statement> _inners;

		public IfStatement(Expression test, Statement thenPart, Statement elsePart)
		{
			_test = test;
			_then = thenPart;
			_else = elsePart;
		}

		public override IEnumerable<Statement> InnerStatements
		{
			get
			{
				if (_inners == null)
				{
					_inners = new List<Statement>();
					if (_then is BlockStatement)
						_inners.AddRange(_then.InnerStatements);
					else
						_inners.Add(_then);
					if (_else is BlockStatement)
						_inners.AddRange(_else.InnerStatements);
					else
						_inners.Add(_else);
				}
				return _inners;
			}
		}

		private static bool BlockRequired(string text)
		{
			if (text.IndexOf(';') < 0)
				return false;
			text = text.Trim();
			if (!text.StartsWith("begin") || !text.EndsWith("end"))
				return true;
			return text.IndexOf("begin", 6, StringComparison.Ordinal) > 0;
		}

		public override string GetText(Program context, int indent)
		{
			string indentString = context.IndentString(indent);
			string nl = context.NewLine + indentString;
			string nl__ = context.NewLine + context.IndentString(indent + 1);
			StringBuilder sb = new StringBuilder();
			sb.Append(indentString)
				.Append("if (").Append(_test.GetText(context)).Append(")");
			HashSet<string> settings = context.MarkValueSettings();
			HashSet<string> settings2 = null;
			if (_then != null)
			{
				string s = _then.GetText(context, indent + 1);
				if (s.Length > 0)
				{
					bool block = BlockRequired(s);
					if (block)
						sb.Append(nl__).Append("begin");
					sb.Append(context.NewLine).Append(s);
					if (block)
						sb.Append(nl__).Append("end");
					settings2 = context.MarkValueSettings();
				}
			}
			if (_else != null)
			{
				context.RestoreValueSettings(settings);
				string s = _else.GetText(context, indent + 1);
				if (s.Length > 0)
				{
					bool block = BlockRequired(s);
					sb.Append(nl).Append("else");
					if (block)
						sb.Append(nl__).Append("begin");
					sb.Append(context.NewLine).Append(s);
					if (block)
						sb.Append(nl__).Append("end");
				}
				if (settings2 != null)
					context.AddValueSettings(settings2);
			}
			return sb.ToString();
		}

		public override bool Prepare(Program context)
		{
			return (_then == null || _then.Prepare(context)) && (_else == null || _else.Prepare(context));
		}
	}
}
