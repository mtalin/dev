﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Holding.Procedures;

namespace Holding.Statements
{
	public class DistributeStatement: Statement
	{
		private readonly Expression _asset;
		private readonly Expression _count;
		private readonly Statement _body;
		private readonly DistributeProcedure _distribute;
		private readonly ApplyDistributionProcedure _apply;
		private IEnumerable<Statement> _inners;

		static DistributeStatement()
		{
		}

		public DistributeStatement(Expression asset, Expression count, Statement body)
		{
			if (asset == null)
				throw new ArgumentNullException(nameof(asset));
			_asset = asset;
			_count = count;
			_body = body;
			_distribute = new DistributeProcedure(null);
			_apply = new ApplyDistributionProcedure();
		}

		public override IEnumerable<Statement> InnerStatements => _inners ?? (_inners = _body == null ? Array.Empty<Statement>(): new[] {_body});

		public override bool Prepare(Program context)
		{
			bool result = _apply.Prepare(context);
			if (!_distribute.Prepare(context))
				result = false;
			if (!base.Prepare(context))
				result = false;
			return result;
		}

		public override string GetText(Program context, int indent)
		{
			context.SetValue("x.FirstLot", true);
			context.SetValue("x.LastLot", true);
			context.SetValue("h.dtDeposit", true);
			context.SetValue("h.PackageSize", true);
			context.SetValue("h.unitsNumber", true);
			context.SetValue("h.salePrice", true);
			context.SetValue("h.costBasis", true);
			context.SetValue("h.bookBasis", true);
			context.SetValue("h.interest", true);
			context.SetValue("h.dividend", true);
			context.SetValue("h.capitalGain", true);
			context.SetValue("h.phantomGain", true);
			context.SetValue("h.interestAdj", true);

			string template = DistributionTemplate.Replace(Environment.NewLine, "\n")
				.Replace("#{{[INITIALIZE]}}", DistributeInitialize(context))
				.Replace("#{{[ASSET]}}", _asset.GetText(context))
				.Replace("#{{[COUNT]}}", _count == null ? "*": _count.GetText(context))
				.Replace("#{{[CHECK_MISSING]}}", CheckMissing())
				.Replace("#{{[DISTRIBUTE_ROUND]}}", DistributeRound(context))
				.Replace("#{{[DISTRIBUTE_REST]}}", DistributeRest(context));
			string assign0 = BuildAssign(context, indent);
			string assign1 = BuildAssign(context, indent + 1);
			string prog = context.MapTemplate(indent, template)
				.Replace("#{{[ASSIGN0]}}", assign0)
				.Replace("#{{[ASSIGN1]}}", assign1);
			return prog;
		}

		private static readonly string[] _values = new string[] { "SalePrice", "CostBasis", "BookBasis", "Interest", "Dividend", "CapitalGain", "PhantomGain", "InterestAdj" };

		private bool MustDistribute(Program context, string name)
		{
			return context.HasValue("w." + name);
		}

		private string DistributeInitialize(Program context)
		{
			StringBuilder sb = new StringBuilder();
			string tem = "set ${{[w.#]}} = isnull(${{[w.#]}}, %);\n";
			foreach (var item in _values)
			{
				if (MustDistribute(context, item))
					sb.Append(tem.Replace('%', '0').Replace("#", item));
			}
			if (MustDistribute(context, "UnitsNumber"))
				sb.Append(tem.Replace("%", "${{[t.UnitsNumber]}}").Replace("#", "UnitsNumber"));
			if (_count == null)
				sb.Append("set ${{[u.UnitsLeft]}} = isnull((select sum(UnitsNumber) from ${{[$Holding]}} where Asset=#{{[ASSET]}} and FoundationAccount = @t_foundationAccount and DtClosed is null), 0)\n");
			else
				sb.Append("set ${{[u.UnitsLeft]}} = ").Append(_count.GetText(context)).Append(";\n");
			return sb.ToString();
		}

		private string DistributeRound(Program context)
		{
			StringBuilder sb = new StringBuilder();
			string tem = "	set ${{[v.#]}} = round(${{[u.lotSize]}} * ${{[w.#]}} / ${{[u.unitsLeft]}}, %);\n" +
				"	set ${{[w.#]}} = ${{[w.#]}} - ${{[v.#]}};\n" +
				"	set ${{[h.#]}} = ${{[h.#]}} + ${{[v.#]}};\n";
			foreach (var item in _values)
			{
				if (MustDistribute(context, item))
					sb.Append(tem.Replace('%', '2').Replace("#", item));
			}
			if (MustDistribute(context, "UnitsNumber"))
				sb.Append(tem.Replace('%', '4').Replace("#", "UnitsNumber"));
			return sb.ToString();
		}

		private string DistributeRest(Program context)
		{
			StringBuilder sb = new StringBuilder();
			string tem = "set ${{[v.#]}} = ${{[w.#]}};\n" +
				"set ${{[h.#]}} = ${{[h.#]}} + ${{[v.#]}};\n";
			foreach (var item in _values)
			{
				if (MustDistribute(context, item))
					sb.Append(tem.Replace("#", item));
			}
			if (MustDistribute(context, "UnitsNumber"))
				sb.Append(tem.Replace("#", "UnitsNumber"));
			return sb.ToString();
		}

		private string BuildAssign(Program context, int indent)
		{
			return _body == null ? "": _body.GetText(context, indent);
		}

		private string CheckMissing()
		{
			if (_count == null)
				return "if (@@fetch_status <> 0)\n" +
					"	begin\n" +
					"	set ${{[x.Distributed]}} = 0;\n" +
					"	set ${{[x.firstLot]}} = null;\n" +
					"	set ${{[h.DtDeposit]}} = isnull(${{[t.DtDeposit]}}, ${{[t.DtTransact]}});\n" +
					"	set ${{[h.DtClosed]}} = ${{[t.DtTransact]}};\n" +
					"	set ${{[h.PackageSize]}} = 0;\n" +
					"	set ${{[h.UnitsNumber]}} = 0;\n" +
					"	set ${{[h.SalePrice]}} = 0\n" +
					"	set ${{[h.CostBasis]}} = 0\n" +
					"	set ${{[h.BookBasis]}} = 0\n" +
					"	set ${{[h.Interest]}} = 0\n" +
					"	set ${{[h.Dividend]}} = 0\n" +
					"	set ${{[h.CapitalGain]}} = 0\n" +
					"	set ${{[h.PhantomGain]}} = 0\n" +
					"	set ${{[h.InterestAdj]}} = 0\n" +
					"	close ${{[?$DSC]}};\n" +
					"	deallocate ${{[?$DSC]}};\n" +
					"	goto ${{[?$DistributeEmpty]}};\n" +
					"	end;\n" +
					"set ${{[x.Distributed]}} = 1;\n";
			else
				return
					"if (@@fetch_status <> 0)\n" +
					"	begin\n" +
					"	set ${{[errorInfo]}} = 'Missing asset';\n" +
					"	close ${{[?$DSC]}};\n" +
					"	deallocate ${{[?$DSC]}};\n" +
					"	goto Error;\n" +
					"	end;\n" +
					"set ${{[x.Distributed]}} = 1;\n";
		}

		#region Templates
		private const string DistributionTemplate =
@"
/* begin distribute(#{{[ASSET]}}, #{{[COUNT]}}) */

declare ${{[?$DSC]}} cursor static for
	select ID, FirstLot, DtDeposit, PackageSize, UnitsNumber,
		V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,
		V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj
	from ${{[$Holding]}}
	where Asset = #{{[ASSET]}}
		and	FoundationAccount = ${{[t.foundationAccount]}}
		and	DtClosed is null
	order by DtDeposit, FirstLot, ParentLot
for read only
open ${{[?$DSC]}}

set ${{[x.lastLot]}} = null;
#{{[INITIALIZE]}}
fetch next from ${{[?$DSC]}} into ${{[x.lastLot]}}, ${{[x.firstLot]}}, ${{[h.dtDeposit]}}, ${{[h.PackageSize]}}, ${{[h.unitsNumber]}}, ${{[h.salePrice]}}, ${{[h.costBasis]}}, ${{[h.bookBasis]}}, ${{[h.interest]}}, ${{[h.dividend]}}, ${{[h.capitalGain]}}, ${{[h.phantomGain]}}, ${{[h.interestAdj]}}

#{{[CHECK_MISSING]}}

while (@@fetch_status = 0)
	begin

	set ${{[h.dtClosed]}} = null;
	set ${{[u.lotSize]}} = ${{[h.unitsNumber]}};
	
	if (${{[u.unitsLeft]}} <= ${{[u.lotSize]}})
		break;

#{{[DISTRIBUTE_ROUND]}}
#{{[ASSIGN1]}}
	insert into ${{[$Holding]}}
		(
		Foundation, FoundationAccount, Asset, AssetType,
		FirstLot, ParentLot,
		DtCreated, DtDeposit, DtClosed,
		HasChild, IsRoot,
		Transact, TransactType,
		UnitsNumber, PackageSize,
		V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,
		V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj
		)
	values
		(
		${{[t.foundation]}}, ${{[t.foundationAccount]}}, ${{[t.asset]}}, ${{[t.assetType]}},
		${{[x.firstLot]}}, ${{[x.lastLot]}},
		${{[t.dtTransact]}}, ${{[h.dtDeposit]}}, ${{[h.dtClosed]}},
		0, 0,
		@t, ${{[t.type]}},
		${{[h.unitsNumber]}}, ${{[h.PackageSize]}},
		${{[h.salePrice]}}, ${{[h.costBasis]}}, ${{[h.bookBasis]}}, ${{[h.interest]}},
		${{[h.dividend]}}, ${{[h.capitalGain]}}, ${{[h.phantomGain]}}, ${{[h.interestAdj]}}
		)
	update ${{[$Holding]}} set DtClosed = ${{[t.dtTransact]}}, HasChild = 1 where ID = ${{[x.lastLot]}}

	set ${{[u.unitsLeft]}} = ${{[u.unitsLeft]}} - ${{[u.lotSize]}};

	fetch next from ${{[?$DSC]}} into ${{[x.lastLot]}}, ${{[x.firstLot]}}, ${{[h.dtDeposit]}}, ${{[h.PackageSize]}}, ${{[h.unitsNumber]}}, ${{[h.salePrice]}}, ${{[h.costBasis]}}, ${{[h.bookBasis]}}, ${{[h.interest]}}, ${{[h.dividend]}}, ${{[h.capitalGain]}}, ${{[h.phantomGain]}}, ${{[h.interestAdj]}}
	end

if (@@fetch_status <> 0)
	begin
	set ${{[errorInfo]}} = 'Negative Number of Securities (' + cast(-${{[u.unitsLeft]}} as varchar) + ')';
	close ${{[?$DSC]}};
	deallocate ${{[?$DSC]}};
	goto Error;
	end

close ${{[?$DSC]}};
deallocate ${{[?$DSC]}};

if (${{[u.unitsLeft]}} < ${{[h.unitsNumber]}})
	begin

	set ${{[u.lotSize]}} = ${{[u.unitsLeft]}};
	set ${{[u.unitsLeft]}} = ${{[h.unitsNumber]}};

	set ${{[z.salePrice]}} = ${{[h.salePrice]}};
	set ${{[z.costBasis]}} = ${{[h.costBasis]}};
	set ${{[z.bookBasis]}} = ${{[h.bookBasis]}};
	set ${{[z.interest]}} = ${{[h.interest]}};
	set ${{[z.dividend]}} = ${{[h.dividend]}};
	set ${{[z.capitalGain]}} = ${{[h.capitalGain]}};
	set ${{[z.phantomGain]}} = ${{[h.phantomGain]}};
	set ${{[z.interestAdj]}} = ${{[h.interestAdj]}};

	set ${{[h.unitsNumber]}} = ${{[u.lotSize]}}
	set ${{[h.salePrice]}} = round(${{[u.lotSize]}} * ${{[h.salePrice]}} / ${{[u.unitsLeft]}}, 2);
	set ${{[h.costBasis]}} = round(${{[u.lotSize]}} * ${{[h.costBasis]}} / ${{[u.unitsLeft]}}, 2);
	set ${{[h.bookBasis]}} = round(${{[u.lotSize]}} * ${{[h.bookBasis]}} / ${{[u.unitsLeft]}}, 2);
	set ${{[h.interest]}} = round(${{[u.lotSize]}} * ${{[h.interest]}} / ${{[u.unitsLeft]}}, 2);
	set ${{[h.dividend]}} = round(${{[u.lotSize]}} * ${{[h.dividend]}} / ${{[u.unitsLeft]}}, 2);
	set ${{[h.capitalGain]}} = round(${{[u.lotSize]}} * ${{[h.capitalGain]}} / ${{[u.unitsLeft]}}, 2);
	set ${{[h.phantomGain]}} = round(${{[u.lotSize]}} * ${{[h.phantomGain]}} / ${{[u.unitsLeft]}}, 2);
	set ${{[h.interestAdj]}} = round(${{[u.lotSize]}} * ${{[h.interestAdj]}} / ${{[u.unitsLeft]}}, 2);

	insert into ${{[$Holding]}}
		(
		Foundation, FoundationAccount, Asset, AssetType,
		FirstLot, ParentLot,
		DtCreated, DtDeposit, DtClosed,
		HasChild, IsRoot,
		Transact, TransactType,
		UnitsNumber, PackageSize,
		V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,
		V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj
		)
	values
		(
		${{[t.foundation]}}, ${{[t.foundationAccount]}}, ${{[t.asset]}}, ${{[t.assetType]}},
		${{[x.firstLot]}}, ${{[x.lastLot]}},
		${{[t.dtTransact]}}, ${{[h.dtDeposit]}}, null,
		0, 0,
		@t, ${{[t.type]}},
		${{[u.unitsLeft]}} - ${{[h.unitsNumber]}}, ${{[h.PackageSize]}},
		${{[z.salePrice]}} - ${{[h.salePrice]}}, ${{[z.costBasis]}} - ${{[h.costBasis]}}, ${{[z.bookBasis]}} - ${{[h.bookBasis]}}, ${{[z.interest]}} - ${{[h.interest]}},
		${{[z.dividend]}} - ${{[h.dividend]}}, ${{[z.capitalGain]}} - ${{[h.capitalGain]}}, ${{[z.phantomGain]}} - ${{[h.phantomGain]}}, ${{[z.interestAdj]}} - ${{[h.interestAdj]}}
		);
	end;


set ${{[h.dtClosed]}} = null;
${{[?$DistributeEmpty]}}:

#{{[DISTRIBUTE_REST]}}
#{{[ASSIGN0]}}
insert into ${{[$Holding]}}
	(
	Foundation, FoundationAccount, Asset, AssetType,
	FirstLot, ParentLot,
	DtCreated, DtDeposit, DtClosed,
	HasChild, IsRoot,
	Transact, TransactType,
	UnitsNumber, PackageSize,
	V_SalePrice, V_CostBasis, V_BookBasis, V_Interest,
	V_Dividend, V_CapitalGain, V_PhantomGain, V_InterestAdj
	)
values
	(
	${{[t.foundation]}}, ${{[t.foundationAccount]}}, ${{[t.asset]}}, ${{[t.assetType]}},
	${{[x.firstLot]}}, ${{[x.lastLot]}},
	${{[t.dtTransact]}}, ${{[h.dtDeposit]}}, ${{[h.dtClosed]}},
	0, case when ${{[x.FirstLot]}} is null then 1 else 0 end,
	@t, ${{[t.type]}},
	${{[h.unitsNumber]}}, ${{[h.PackageSize]}},
	${{[h.salePrice]}}, ${{[h.costBasis]}}, ${{[h.bookBasis]}}, ${{[h.interest]}},
	${{[h.dividend]}}, ${{[h.capitalGain]}}, ${{[h.phantomGain]}}, ${{[h.interestAdj]}}
	);
if (${{[x.FirstLot]}} is null)
	update ${{[$Holding]}} set FirstLot = ID where ID = scope_identity();
else
	update ${{[$Holding]}} set DtClosed = ${{[t.DtTransact]}}, HasChild = 1 where ID = ${{[x.lastLot]}};

/* end distribute */
";
		#endregion

	}
}
