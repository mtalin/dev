@echo off
set server=test\sql2005
set password=
set user_id=sa

if not "%1"=="" set server=%1
if not "%2"=="" set password=%2
set database=CharityPlanner

set creadntials=-U %user_id% -P %password%
if "%password%"=="" set creadntials=-E

hrc.exe Rules.ini -o:Rules.ini.sql
if errorlevel 1 goto :EOF
call :osql Rules.ini.sql
goto :EOF


:osql
	echo sqlcmd -n -S %server% -d %database% -i %1
	echo sqlcmd -n -S %server% -d %database% -i %1 >>out.log
	sqlcmd -S %server% -d %database% %creadntials% -I -x -m 1 -i %1 >>out.log
	if errorlevel 1 (
		echo ERROR! Press Ctrl+C to terminate
		pause
		)
	goto :EOF

