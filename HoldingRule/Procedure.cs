﻿using System;
using System.Linq;
using System.Text;

namespace Holding
{

	public abstract class Procedure: Statement
	{
		protected Procedure()
		{
		}

		protected Procedure(string name)
		{
			Name = name;
		}

		public string Name { get; }
	}
}
