﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Holding
{

	public enum ExpressionTypeCode
	{
		Undefined,
		Boolean,
		Numeric,
		String,
		DateTime,
		Table,
	}


	[DebuggerDisplay("{TypeCode} sql={SqlType}")]
	public class ExpressionType
	{
		public ExpressionTypeCode TypeCode { get; private set; }
		public string SqlType { get; private set; }
		public virtual IEnumerable<Identifier> LocalIdentifiers { get; private set; }

		private static Identifier[] _noIdentifiers = new Identifier[0];

		public ExpressionType(ExpressionTypeCode typeCode, string sqlType)
		{
			TypeCode = typeCode;
			SqlType = sqlType;
			LocalIdentifiers = _noIdentifiers;
		}

		public ExpressionType(ExpressionTypeCode typeCode, string sqlType, IEnumerable<Identifier> localIdentifiers)
		{
			TypeCode = typeCode;
			SqlType = sqlType;
			LocalIdentifiers = localIdentifiers ?? _noIdentifiers;
		}

		public void Use(Program context)
		{
			context.Declaration.UseVariablesRange(from i in LocalIdentifiers select i.Name);
		}

		public static ExpressionType Parse(string s)
		{
			if (s == null || s.Length == 0)
				return Decimal;
			s = s.Trim().ToUpperInvariant();
			ExpressionType result;
			if (_list.TryGetValue(s, out result))
				return result;
			return Undefined;
		}

		public static readonly ExpressionType Undefined	= new ExpressionType(ExpressionTypeCode.Undefined, "");
		public static readonly ExpressionType Integer	= new ExpressionType(ExpressionTypeCode.Numeric, "int");
		public static readonly ExpressionType Long		= new ExpressionType(ExpressionTypeCode.Numeric, "bigint");
		public static readonly ExpressionType Money		= new ExpressionType(ExpressionTypeCode.Numeric, "money");
		public static readonly ExpressionType Decimal	= new ExpressionType(ExpressionTypeCode.Numeric, "decimal(19,8)");
		public static readonly ExpressionType Decimal2	= new ExpressionType(ExpressionTypeCode.Numeric, "decimal(38,16)");
		public static readonly ExpressionType Float		= new ExpressionType(ExpressionTypeCode.Numeric, "float");
		public static readonly ExpressionType Boolean	= new ExpressionType(ExpressionTypeCode.Boolean, "bit");
		public static readonly ExpressionType Date		= new ExpressionType(ExpressionTypeCode.DateTime, "date");
		public static readonly ExpressionType DateTime	= new ExpressionType(ExpressionTypeCode.DateTime, "datetime");
		public static readonly ExpressionType Time		= new ExpressionType(ExpressionTypeCode.DateTime, "time");
		public static readonly ExpressionType String	= new ExpressionType(ExpressionTypeCode.String, "varchar(max)");
		public static readonly ExpressionType String50	= new ExpressionType(ExpressionTypeCode.String, "varchar(50)");
		public static readonly ExpressionType String100 = new ExpressionType(ExpressionTypeCode.String, "varchar(100)");
		public static readonly ExpressionType String150 = new ExpressionType(ExpressionTypeCode.String, "varchar(150)");
		public static readonly ExpressionType String200 = new ExpressionType(ExpressionTypeCode.String, "varchar(200)");

		private static Dictionary<string, ExpressionType> _list = new Dictionary<string,ExpressionType>()
			{
				{"INT",			Integer 	},
				{"LONG",		Long		},
				{"MONEY",		Money		},
				{"DECIMAL",		Decimal		},
				{"DECIMAL2",	Decimal2	},
				{"FLOAT",		Float		},
				{"BOOL",		Boolean		},
				{"DATE",		Date		},
				{"DATETIME",	DateTime	},
				{"TIME",		Time		},
				{"STRING",		String		},
				{"STRING50",	String50	},
				{"STRING100",	String100	},
				{"STRING150",	String150	},
				{"STRING200",	String200	}
			};

		public static readonly ExpressionType DistributionTable = new ExpressionType
			(
			ExpressionTypeCode.Table,
			"table" +
				"\n\t(" +
				"\n\tID int identity(1, 1) primary key," +
				"\n\tFirstLot int," +
				"\n\tParentLot int," +
				"\n\tDtDeposit date," +
				"\n\tDtClosed date," +
				"\n\tUnitsNumber decimal(19,8)," +
				"\n\tPackageSize decimal(19,8)," +
				"\n\tUnitsLeft decimal(19,8)," +
				"\n\tV_SalePrice money," +
				"\n\tV_CostBasis money," +
				"\n\tV_BookBasis money," +
				"\n\tV_Interest money," +
				"\n\tV_Dividend money," +
				"\n\tV_CapitalGain money," +
				"\n\tV_PhantomGain money," +
				"\n\tV_InterestAdj money" +
				"\n\t)",
			new[]
				{
					new Identifier(ExpressionType.Decimal, "u.UnitsLeft", "@u_unitsLeft"),
					new Identifier(ExpressionType.Decimal, "u.UnitsNumber", "@u_unitsNumber"),
					new Identifier(ExpressionType.Decimal2, "u.LotSize", "@u_lotSize"),

					new Identifier(ExpressionType.Decimal, "d.UnitsNumber", "@d_unitsNumber"),
					new Identifier(ExpressionType.Money, "d.SalePrice", "@d_salePrice"),
					new Identifier(ExpressionType.Money, "d.CostBasis", "@d_costBasis"),
					new Identifier(ExpressionType.Money, "d.BookBasis", "@d_bookBasis"),
					new Identifier(ExpressionType.Money, "d.Interest", "@d_interest"),
					new Identifier(ExpressionType.Money, "d.Dividend", "@d_dividend"),
					new Identifier(ExpressionType.Money, "d.CapitalGain", "@d_capitalGain"),
					new Identifier(ExpressionType.Money, "d.PhantomGain", "@d_phantomGain"),
					new Identifier(ExpressionType.Money, "d.InterestAdj", "@d_interestAdj"),
				}
			);
	}
}
