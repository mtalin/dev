﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Lexxys;
using Lexxys.Data;

namespace MigrateDocuments
{
	class Program
	{
		private static readonly Logger _log = new Logger(nameof(MigrateDocuments));
		private const int DEGREE_OF_PARALLELISM = 16;
		private static readonly object LOCK = new Object();
		private static volatile int _migratedCount;
		private const int TraceThreshold = 5000;
		private static int ProgressThreshold = 16;
		private static bool Silent = false;

		static void Main(string[] args)
		{
			if (args != null && args.Length > 0)
			{
				if (args.Length != 1 || (!args[0].StartsWith("-silent", StringComparison.OrdinalIgnoreCase) && !args[0].StartsWith("/silent", StringComparison.OrdinalIgnoreCase)))
				{
					Say("usage: MigrateDocuments [-silent]");
					return;
				}
				Silent = true;
			}

			List<int> docs = null;
			using (_log.Enter("Data fetching"))
			{
				Say("Fetching data...", true);
				docs = Dc.GetList<int>("select d.ID from Documents d left join Foundations f on f.ID=d.Foundation left join ShellCorporations s on s.ID=d.ShellCorporation where d.Body is not null and (s.Step is null or s.Step>=110 or f.Type<>0)");
				_log.Info($"{docs.Count} records found.");
				Say($"\r{docs.Count} documents to convert.");
				ProgressThreshold = Math.Max(docs.Count / 1000, 10);
			}

			if (docs.Count > 0)
			{
				var start = DateTime.UtcNow;
				_log.Info($"Use DegreeOfParallelism={Environment.ProcessorCount * DEGREE_OF_PARALLELISM}");
				Say();
				Say("Processing... ", true);
			
				docs.AsParallel().WithDegreeOfParallelism(Environment.ProcessorCount * DEGREE_OF_PARALLELISM).ForAll(id =>
				{
					var doc = FoundationSource.Admin.CPBase.Document.TryLoad(id, false);
					if (doc == null)
					{
						_log.Warning($"Cannot load a document #{id}.");
						return;
					}

					bool migrated = false;
					try
					{
						migrated = doc.Migrate();
					}
					catch (Exception ex)
					{
						_log.Error($"{_log.Source}.#{id}", ex);
						Say($"#{id}: {ex.Message}");
					}
					if (!migrated)
						return;

					int count = Interlocked.Increment(ref _migratedCount);
					if (count % TraceThreshold == 0)
						_log.Trace($"{count} documents has been migrated.");
					if (count % ProgressThreshold == 0)
					{
						lock (LOCK)
						{
							Say(GetProgressString(count, docs.Count, start), true);
						}
					}
				});

				Say();
				Say($"Done.  Time: {DateTime.UtcNow - start}");
			}
			else
			{
				Say("There is no records.");
			}
			if (Silent)
				return;

			Say();
			Console.Write("Press any key to continue...");
			Console.ReadKey(true);
		}

		private static void Say(string message = null, bool keepLine = false)
		{
			if (Silent) return;
			if (keepLine)
				Console.Write(message);
			else if (message == null)
				Console.WriteLine();
			else
				Console.WriteLine(message);
		}

		private static string GetProgressString(int processed, int total, DateTime start)
		{
			TimeSpan elapsedTime = DateTime.UtcNow - start;
			TimeSpan totalTime = TimeSpan.FromTicks(elapsedTime.Ticks * total / processed);
			var s = $"\r{Math.Min(Math.Round(processed / (double)total, 3), 1):P1} ({processed} of {total}) {TimeLeft(totalTime - elapsedTime)}";
			int last = _last;
			_last = s.Length;
			if (s.Length < last)
				s += new String(' ', last - s.Length);
			return s;
		}
		private static int _last;

		private static string TimeLeft(TimeSpan timeLeft)
		{
			string S(int value) => value == 1 ? "" : "s";
			var hour = (int)timeLeft.TotalHours;
			var min = timeLeft.Minutes;
			var sec = timeLeft.Seconds;
			var text = new StringBuilder();
			if (hour > 0)
				text.Append(hour).Append(" hour").Append(S(hour));
			if (min > 0)
			{
				if (hour > 0)
					text.Append(sec > 0 ? ", ": " and ");
				text.Append(min).Append(" minute").Append(S(min));
			}
			if (sec > 0)
			{
				if (hour > 0 || min > 0)
					text.Append(" and ");
				text.Append(sec).Append(" second").Append(S(sec));
			}
			return text.Length == 0 ? "": text.Append(" left").ToString();
		}
	}
}
