﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web
{
	public abstract class PanelBase : IDisposable
	{
		protected readonly AttributesBag A0 = new AttributesBag();
		protected AttributesBag A1;
		protected AttributesBag A2;

		private Action _rowEnding;
		private Action _cellEnding;
		private readonly PageCommon _page;
		private readonly bool _autoRowCellEnding;
		private bool _isRecording;

		public PanelBase(PageCommon page, bool autoRowCellEnding = true)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
			_autoRowCellEnding = autoRowCellEnding;
		}

		protected PageCommon Page
		{
			get { return _page; }
		}

		public bool IsRecording
		{
			get { return _isRecording; }
		}

		public abstract string Name { get; }

		public HtmlWriter Writer
		{
			get { return _page.Panel.Writer; }
		}

		public string CssClass { get; set; }

		protected int PanelId { get; set; }
		protected int GroupId { get; set; }
		protected int CellId { get; set; }

		public void Begin(params string[] args)
		{
			Writer.WriteComment("$" + Name);
			BeginCore(args);
		}
		protected abstract void BeginCore(params string[] args);

		public void End()
		{
			if (_autoRowCellEnding && _cellEnding != null)
				_cellEnding();
			if (_autoRowCellEnding && _rowEnding != null)
				_rowEnding();

			EndCore();
			Writer.WriteComment("/$" + Name);
		}
		protected abstract void EndCore();

		public void Row(params string[] args)
		{
			if (_autoRowCellEnding && _cellEnding != null)
				_cellEnding();
			if (_autoRowCellEnding && _rowEnding != null)
				_rowEnding();

			CellId = 0;
			++GroupId;
			RowCore(args);
		}
		protected abstract void RowCore(params string[] args);

		public void Cell(params string[] args)
		{
			if (_autoRowCellEnding && _cellEnding != null)
				_cellEnding();

			++CellId;
			CellCore(args);
		}
		protected abstract void CellCore(params string[] args);

		public void Dispose()
		{
			End();
		}


		protected virtual void SetRowEnding(Action action, bool runExisting = false)
		{
			if (runExisting && _rowEnding != null)
				_rowEnding();
			_rowEnding = action;
		}

		protected virtual void SetCellEnding(Action action, bool runExisting = false)
		{
			if (runExisting && _cellEnding != null)
				_cellEnding();
			_cellEnding = action;
		}

		protected internal void OnStartRecording()
		{
			_isRecording = true;

			OnStartRecordingCore();
		}
		protected virtual void OnStartRecordingCore()
		{
			;
		}

		protected internal void OnStopRecording()
		{
			_isRecording = true;

			OnStopRecordingCore();
		}
		protected virtual void OnStopRecordingCore()
		{
			;
		}

		protected internal virtual void OnPlayRecording(RecordedPanel record)
		{
			;
		}

		protected void PanelHLine(string line, string space, int columns, string attributes)
		{
			string cspan = columns > 1 ? HtmlUtility.AttributePad("colspan", columns.ToString()) : "";
			int ns = space != null
				? space.Equals("space", StringComparison.OrdinalIgnoreCase) ? 1 : JsCompatibility.ParseInt(space)
				: 0;
			string lineColor = "";
			if (!String.IsNullOrWhiteSpace(line))
			{
				string[] v = line.Split(new[] { ' ' }, 2);
				if (v.Length > 1)
				{
					line = v[0];
					lineColor = " " + v[1];
				}
				switch (line.ToUpperInvariant())
				{
					case "SOLID":
					case "LINE":
						line = "solid 1";
						break;
					case "DOUBLE":
						line = "double 3";
						break;
					case "DOTTED":
						line = "dotted 1";
						break;
					case "DASHED":
						line = "dashed 1";
						break;
					default:
						int n;
						line = JsCompatibility.TryParseInt(line, out n)
							? "solid" + n.ToString()
							: "";
						break;
				}
			}

			using (var writer = new HtmlWriter(_page.Writer))
			{
				if (line != null)
				{
					writer
						.Tag("tr")
							.Attr(cspan)
							.Attr("height", ns > 0 ? ns.ToString() : "1")
							.AttrStyle("border-bottom", String.Format("{0} {1}px;", lineColor, line))
							.Attr(attributes)
							.Tag("td")
								.Attr("src", _page.GetImageUrl("spacer.gif"))
								.Attr("width", "1")
								.Attr("height", ns > 0 ? ns.ToString() : "1")
								.Attr("alt", "")
								.TagSingle("img")
							.End()
						.End();
				}

				if (ns > 0)
				{
					writer
						.Tag("tr")
							.Attr(cspan)
							.Attr("height", ns.ToString())
							.AttrStyle("border-bottom", String.Format("{0} {1}px;", lineColor, line))
							.Attr(attributes)
							.Tag("td")
								.Attr("src", _page.GetImageUrl("spacer.gif"))
								.Attr("width", "1")
								.Attr("height", ns.ToString())
								.Attr("alt", "")
								.TagSingle("img")
							.End()
						.End();
				}
			}
		}


		#region Implementation
		#region JSObject helpers
		protected static bool eQ(string valueA, string valueB)
		{
			return valueA != null && valueA.Equals(valueB, StringComparison.OrdinalIgnoreCase);
		}

		protected static bool Has(string value)
		{
			return value != null;
		}

		protected static dynamic tE(params dynamic[] values)
		{
			for (int i = 0; i < values.Length; i++)
			{
				if (tQ(values[i]))
					return values[i];
			}
			return values[0];
		}

		protected static bool tQ(string value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		protected static bool tN(string value)
		{
			return !JsCompatibility.ToBoolean(value);
		}
		#endregion
		#endregion
	}

	public sealed class RecordedPanel : JsObject<object>
	{
		private readonly PanelBase _panel;

		public RecordedPanel(string html, PanelBase panel)
		{
			Html = html;
			_panel = panel;
		}

		public string Html { get; set; }

		public PanelBase Panel
		{
			get { return _panel; }
		}

		public RecordedPanel Clone()
		{
			return new RecordedPanel(Html, _panel);
		}
	}

	public class AttributesBag : System.Dynamic.DynamicObject
	{
		private Dictionary<string, string> _defaults;
		private readonly AttributesBag _parent;
		private readonly Dictionary<string, string> _attributes = new Dictionary<string,string>(StringComparer.InvariantCultureIgnoreCase);

		public AttributesBag(AttributesBag parent = null)
		{
			_parent = parent;
		}
		public AttributesBag(IDictionary<string, string> attrs, AttributesBag parent = null)
			: this(parent)
		{
			if (attrs == null)
				throw new ArgumentNullException("attrs");

			foreach (var item in attrs)
			{
				SetToStorage(item.Key, item.Value);
			}
		}

		public void SetDefaults(IEnumerable<KeyValuePair<string, string>> attrs)
		{
			if (attrs == null)
				throw new ArgumentNullException("attrs");

			_defaults = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
			foreach (var item in attrs)
			{
				_defaults.Add(item.Key, item.Value);
			}
		}

		public string GetDefaultFor(string name)
		{
			string value;
			return (_defaults != null && _defaults.TryGetValue(name, out value))
				? value
				: (_parent != null)
					? _parent.GetDefaultFor(name)
					: null;
		}

		public void Extend(IEnumerable<KeyValuePair<string, string>> attrs)
		{
			if (attrs == null)
				throw new ArgumentNullException("attrs");

			foreach (var item in attrs)
			{
				SetToStorage(item.Key, item.Value);
			}
		}

		public string Parse(params string[] args)
		{
			StringBuilder s = new StringBuilder();
			for (int i = 0; i < args.Length; i++)
			{
				var t = args[i].Trim();
				int j = t.IndexOf('=');
				string r;
				int k;
				if (j > 0)
				{
					r = t.Substring(0, j);
					string v;
					if (TryGetFromStorage(r, out v))
					{
						k = (v != null) ? v.IndexOf('=') : -1;
						SetToStorage(r, (k <= 0)
							? t.Substring(j + 1)
							: (j < t.Length - 1)
								? GetFromStorage(r).Substring(0, k) + t.Substring(j)
								: "");
					}
					else
						s.Append(' ').Append(t);
				}
				else
				{
					if (TryGetFromStorage(t, out r))
					{
						k = (r != null) ? r.IndexOf('=') : -1;
						if (k > 0)
							SetToStorage(r.Substring(0, k).Trim(), r);
						else
							SetToStorage(t, t);
					}
					else if (t.Length > 0)
						s.Append(' ').Append(t);
				}
			}
			return s.ToString();
		}

		public void ParseWithLog(PanelBase panel, params string[] args)
		{
			string s = Parse(args);
			if (s.Length > 0)
				; // ap.logStr(panelName, s.concat(" (", common__my_url__, ")"));
		}

		public string Attribute(string key, string name = null, string defaultName = null)
		{
			string arg;
			int i;
			if (key != null && TryGetFromStorage(key, out arg) && arg != null)
			{
				i = arg.IndexOf('=');
				if (i < 0)
					return HtmlUtility.AttributePad(name ?? key, arg);
				if (i < arg.Length - 1)
					return HtmlUtility.AttributePad(name ?? key, arg, arg.Substring(i + 1));
			}

			if (defaultName != null)
			{
				if ((arg = GetDefaultFor(defaultName)) != null)
				{
					i = arg.IndexOf('=');
					if (i < 0)
						return HtmlUtility.AttributePad(name ?? key, arg);
					if (i < arg.Length - 1)
						return HtmlUtility.AttributePad(name ?? key, arg, arg.Substring(i + 1));
				}

				arg = defaultName;
				i = arg.IndexOf('=');
				if (i < 0)
					return HtmlUtility.AttributePad(name ?? key, arg);
				if (i < arg.Length - 1)
					return HtmlUtility.AttributePad(name ?? key, arg, arg.Substring(i + 1));
			}
			return "";
		}

		public AttributesBag Clone(IDictionary<string, string> attrs = null)
		{
			var clone = new AttributesBag(_attributes, _parent);
			if (attrs != null)
				clone.Extend(attrs);
			return clone;
		}


		#region Implementation
		public override bool TryGetMember(System.Dynamic.GetMemberBinder binder, out object result)
		{
			string value;
			if (TryGetFromStorage(binder.Name, out value))
				result = value;
			else
				result = null;
			return true;
		}
		public override bool TrySetMember(System.Dynamic.SetMemberBinder binder, object value)
		{
			SetToStorage(binder.Name, (string)value);
			return true;
		}

		public override bool TrySetIndex(System.Dynamic.SetIndexBinder binder, object[] indexes, object value)
		{
			string key;
			if (indexes.Length == 1 && (key = indexes[0] as string) != null && typeof(string).IsAssignableFrom(value.GetType()))
			{
				SetToStorage(key, (string)value);
				return true;
			}
			return false;
		}
		public override bool TryGetIndex(System.Dynamic.GetIndexBinder binder, object[] indexes, out object result)
		{
			string key;
			if (indexes.Length == 1 && (key = indexes[0] as string) != null)
			{
				string result1;
				result = TryGetFromStorage(key, out result1)
					? result1
					: null;
				return true;
			}
			result = null;
			return false;
		}

		private bool TryGetFromStorage(string key, out string value)
		{
			AttributesBag bag = this;
			while (bag != null)
			{
				if (bag._attributes.TryGetValue(key, out value))
					return true;
				bag = bag._parent;
			}
			value = null;
			return false;
		}

		private string GetFromStorage(string key)
		{
			AttributesBag bag = this;
			string value = null;
			while (bag != null)
			{
				if (bag._attributes.TryGetValue(key, out value))
					break;
				bag = bag._parent;
			}
			return value;
		}

		private void SetToStorage(string key, string value)
		{
			_attributes[key] = value;

			//AttributesBag bag = this;
			//while (bag != null)
			//{
			//	if (bag._attributes.ContainsKey(key))
			//		break;
			//	bag = bag._parent;
			//}
			//(bag ?? this)._attributes[key] = value;
		}
		#endregion
	}
}
