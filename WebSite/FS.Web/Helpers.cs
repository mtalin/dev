﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace FS.Web
{
	public enum AttributeQuoteType
	{
		Double,
		Single,
		None
	}

	public static class HtmlUtility
	{
		// html escapes
		public const string NonBreakingSpacer = "&nbsp;";
		public const string EmDash = "&mdash;";
		public const string EnDash = "&ndash;";
		public static readonly string DoubleQuote = HtmlEncode("\"");
		public static readonly string SingleQuote = HtmlEncode("'");
		public static readonly string Ampersand = HtmlEncode("&");
		public static readonly string GreaterThan = HtmlEncode(">");
		public static readonly string LowerThan = HtmlEncode("<");
		// html tokens
		public const char EqualsChar = '=';
		public const char StyleEqualsChar = ':';
		public const char TagLeftChar = '<';
		public const string EndTagLeftChars = "</";
		public const char TagRightChar = '>';
		public const string SelfClosingTagEnd = " />";
		public const char SemicolonChar = ';';
		public const char AttributeQuoteSingle = '\'';
		public const char AttributeQuoteDouble = '"';
		public const char AttributeQuoteNone = '\0';

		public static AttributeQuoteType AttributeQuoteType { get; set; }

		public static string Attribute(string name, params string[] values)
		{
			return AttributeInternal(name, false, AttributeQuoteType, values);
		}
		public static string Attribute(string name, AttributeQuoteType quoteType, params string[] values)
		{
			return AttributeInternal(name, false, quoteType, values);
		}

		public static Tuple<string, string> ParseAttribute(string attr)
		{
			if (!String.IsNullOrWhiteSpace(attr))
			{
				int i = attr.IndexOf(HtmlUtility.EqualsChar);
				if (i > 0)
					return Tuple.Create(attr.Substring(0, i), attr.Substring(i + 1).Trim().Trim(new[] { AttributeQuoteSingle, AttributeQuoteDouble, AttributeQuoteNone }));
			}
			return null;
		}

		public static string GetStyleRule(string name, string value)
		{
			if (name == null)
				throw new ArgumentNullException("name");
			if (value == null)
				return "";

			name = name.TrimEnd(new[] { StyleEqualsChar });
			value = value.TrimStart(new[] { StyleEqualsChar }).TrimEnd(new[] { SemicolonChar });

			return name + StyleEqualsChar + value + SemicolonChar;
		}
		public static string GetStyleRule(params string[] rules)
		{
			if (rules == null || rules.Length == 0)
				throw new ArgumentException("Rules are empty", "rules");

			if (rules.Length % 2 != 0)
				throw new ArgumentException("Invalid style rule. Possibly name or value is missing", "rules");

			StringBuilder strBuilder = new StringBuilder();
			for (int i = 0; i < rules.Length; i += 2)
			{
				strBuilder.Append(GetStyleRule(rules[i], rules[i + 1]));
				if (i < rules.Length - 2)
					strBuilder.Append(" ");
			}
			return strBuilder.ToString();
		}

		public static string AttributePad(string name, params string[] values)
		{
			return AttributeInternal(name, true, AttributeQuoteType, values);
		}
		public static string AttributePad(string name, AttributeQuoteType quoteType, params string[] values)
		{
			return AttributeInternal(name, true, quoteType, values);
		}

		public static string AttributeList(params string[] attrs)
		{
			return AttributeListInternal(false, attrs);
		}
		public static string AttributeListPad(params string[] attrs)
		{
			return AttributeListInternal(true, attrs);
		}

		public static string Tag(string name, params string[] attrs)
		{
			string s = TagLeftChar + name;
			if (attrs != null)
				s += AttributeListPad(attrs);
			return s + SelfClosingTagEnd;
		}

		public static string BeginTag(string name, params string[] attrs)
		{
			string s = TagLeftChar + name;
			if (attrs != null)
				s += AttributeListPad(attrs);
			return s + TagRightChar;
		}
		public static string EndTag(string name)
		{
			return EndTagLeftChars + name + TagRightChar;
		}

		public static string OpenTag(string name, params string[] attrs)
		{
			string s = TagLeftChar + name;
			if (attrs != null)
				s += AttributeListPad(attrs);
			return s;
		}
		public static string CloseTag(bool single)
		{
			return (single) ? SelfClosingTagEnd : TagRightChar.ToString();
		}

		public static string HtmlEncode(string value)
		{
			return HttpUtility.HtmlEncode(value);
		}
		public static string HtmlDecode(string value)
		{
			return HttpUtility.HtmlDecode(value);
		}

		public static string HtmlAttributeEncode(string value)
		{
			return HttpUtility.HtmlAttributeEncode(value);
		}

		public static string GetComment(string text)
		{
			return "<!-- " + text + " -->";
		}

		#region Tags shortcut
		public static readonly string Br = Tag("br");

		public static string Img(string src, int? width = null, int? height = null, string alt = "", int border = 0, int? hSpace = null, int? vSpace = null, string style = null, string id = null)
		{
			if (height != null && height < 0)
				throw new ArgumentOutOfRangeException("height");
			if (width != null && width < 0)
				throw new ArgumentOutOfRangeException("width");
			if (hSpace != null && hSpace < 0)
				throw new ArgumentOutOfRangeException("hSpace");
			if (vSpace != null && vSpace < 0)
				throw new ArgumentOutOfRangeException("vSpace");

			return Tag("img",
						Attribute("src", src),
						Attribute("alt", alt),
						Attribute("width", (width != null) ? width.ToString() : null),
						Attribute("border", border.ToString()),
						Attribute("height", (height != null) ? height.ToString() : null),
						Attribute("hspace", (hSpace != null) ? hSpace.ToString() : null),
						Attribute("vspace", (vSpace != null) ? vSpace.ToString() : null),
						Attribute("style", style),
						Attribute("id", id)
					);
		}

		public static string Submit(string name, string value, string id = null, string cssClass = null)
		{
			return Tag("input",
						Attribute("type", "submit"),
						Attribute("id", id),
						Attribute("name", name),
						Attribute("value", value),
						Attribute("class", cssClass)
					);
		}

		public static string Hidden(string name, string value, string id = null)
		{
			return Tag("input",
						Attribute("type", "hidden"),
						Attribute("name", name),
						Attribute("id", id),
						Attribute("value", value)
					);
		}
		#endregion

		#region JScript
		public static string JValue(string value)
		{
			return "\"" + value + "\"";
		}

		public static string JValue(bool value)
		{
			return (value) ? "true" : "false";
		}

		public static string JValue(DateTime value)
		{
			return "new Date(" + JValue(value.ToString("d")) + ")";
		}

		public static string JValue(int value)
		{
			return value.ToString();
		}

		public static string JValue(double value)
		{
			return value.ToString();
		}
		#endregion


		#region Implementation
		private static string AttributeListInternal(bool pad, params string[] attrs)
		{
			if (attrs == null || attrs.Length == 0)
				return "";
			else if (attrs.Length == 1)
			{
				return !String.IsNullOrWhiteSpace(attrs[0])
					? (pad ? " " : "") + attrs[0].Trim()
					: "";
			}

			StringBuilder s = new StringBuilder();
			for (int i = 0; i < attrs.Length; i++)
			{
				if (!String.IsNullOrWhiteSpace(attrs[i]))
				{
					if (pad || s.Length > 0)
						s.Append(' ');
					s.Append(attrs[i].Trim());
				}
			}
			return s.ToString();
		}

		private static string AttributeInternal(string name, bool pad, AttributeQuoteType quoteType, params string[] values)
		{
			if (values == null || values.Length == 0)
				return "";
			else if (values.Length == 1)
			{
				return values[0] != null
					? (pad ? " " : "") + String.Format(GetAttributeFormatString(quoteType), name.Trim(), HttpUtility.HtmlAttributeEncode(values[0].Trim()))
					: "";
			}

			StringBuilder s = new StringBuilder();
			for (int i = 0; i < values.Length; i++)
			{
				if (values[i] != null)
				{
					if (pad || s.Length > 0)
						s.Append(' ');
					s.Append(HttpUtility.HtmlAttributeEncode(values[i].Trim()));
				}
			}
			return (s.Length > 0) ? String.Format(" " + GetAttributeFormatString(quoteType), name.Trim(), s.ToString()) : "";
		}

		private static readonly string AttributeQuoteSinglePattern = "{0}" + EqualsChar + AttributeQuoteSingle + "{1}" + AttributeQuoteSingle;
		private static readonly string AttributeQuoteDoublePattern = "{0}" + EqualsChar + AttributeQuoteDouble + "{1}" + AttributeQuoteDouble;
		private static readonly string AttributeQuoteNonePattern = "{0}" + EqualsChar + AttributeQuoteNone + "{1}" + AttributeQuoteNone;
		private static string GetAttributeFormatString(AttributeQuoteType quoteType)
		{
			switch (quoteType)
			{
				case AttributeQuoteType.Single:
					return AttributeQuoteSinglePattern;
				case AttributeQuoteType.None:
					return AttributeQuoteNonePattern;
				case AttributeQuoteType.Double:
				default:
					return AttributeQuoteDoublePattern;
			}
		}
		#endregion
	}

	public static class UrlUtility
	{
		public static readonly string SchemeHttp = Uri.UriSchemeHttp;
		public static readonly string SchemeHttps = Uri.UriSchemeHttps;
		public static readonly string SchemeDelimiter = Uri.SchemeDelimiter;

		public const string SegmentDelimiter = "/";
		public const string QueryDelimiter = "?";
		public const string QueryParamDelimiter = "&";

		public static bool HasQuery(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			return url.Contains(QueryDelimiter);
		}

		public static bool IsAbsolute(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			return new Uri(url, UriKind.RelativeOrAbsolute).IsAbsoluteUri;
		}

		public static string Scheme(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			return new Uri(url, UriKind.RelativeOrAbsolute).Scheme;
		}

		public static string ChangeScheme(string url, string scheme)
		{
			if (url == null)
				throw new ArgumentNullException("url");
			if (scheme == null)
				throw new ArgumentNullException("scheme");
			if (!Uri.CheckSchemeName(scheme))
				throw new ArgumentException("scheme");

			// TODO: bad code
			try
			{
				var uri = new UriBuilder(new Uri(url, UriKind.RelativeOrAbsolute));
				uri.Scheme = scheme;
				return uri.ToString();
			}
			catch (InvalidOperationException)
			{
				return scheme + SchemeDelimiter + url;
			}
		}

		public static bool IsHttpScheme(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			var uri = new Uri(url, UriKind.RelativeOrAbsolute);
			return (uri.IsAbsoluteUri && (uri.Scheme == SchemeHttp || uri.Scheme == SchemeHttps));
		}

		public static string GetPath(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			Uri uri = new Uri(url, UriKind.RelativeOrAbsolute);
			if (uri.IsAbsoluteUri)
			{
				return uri.PathAndQuery;
			}
			else
			{
				int i = url.IndexOf(QueryDelimiter);
				if (i >= 0)
					return url.Substring(0, i);
				return url;
			}
		}

		public static string GetPathAndQuery(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			Uri uri = new Uri(url, UriKind.RelativeOrAbsolute);
			if (uri.IsAbsoluteUri)
				return uri.PathAndQuery;
			else
				return url;
		}

		public static string GetQuery(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			int i = url.IndexOf(QueryDelimiter);
			if (i >= 0)
				return url.Substring(i + 1);
			return null;
		}

		public static string ChangeQuery(string url, string query)
		{
			if (url == null)
				throw new ArgumentNullException("url");
			if (query == null)
				throw new ArgumentNullException("query");

			var uri = new UriBuilder(new Uri(url, UriKind.RelativeOrAbsolute));
			uri.Query = query;
			return uri.ToString();
		}

		public static string SetQueryParam(string url, string name, string value, bool replaceExisting = false)
		{
			if (url == null)
				throw new ArgumentNullException("url");
			if (name == null)
				throw new ArgumentNullException("name");
			if (value == null)
				throw new ArgumentNullException("value");

			StringBuilder strBuilder = new StringBuilder();
			if (HasQuery(url))
			{
				strBuilder
					.Append("([")
					.Append(QueryDelimiter).Append(QueryParamDelimiter)
					.Append("]")
					.Append(name)
					.Append("=)[^")
					.Append(QueryDelimiter).Append(QueryParamDelimiter)
					.Append("]*([")
					.Append(QueryDelimiter).Append(QueryParamDelimiter)
					.Append("])?");
				Regex re = new Regex(strBuilder.ToString(), RegexOptions.IgnoreCase);
				if (re.IsMatch(url))
				{
					if (!replaceExisting)
						throw new InvalidOperationException("The query has already a parameter with the specified name: " + name);

					return re.Replace(url, "$1" + value + "$2");
				}
				else
				{
					return strBuilder.Clear()
						.Append(url).Append(QueryParamDelimiter).Append(name).Append("=").Append(value).ToString();
				}
			}
			return strBuilder.Append(url).Append(QueryDelimiter).Append(name).Append("=").Append(value).ToString();
		}

		public static string AddSegment(string url, string segment)
		{
			if (url == null)
				throw new ArgumentNullException("url");
			if (segment == null)
				throw new ArgumentNullException("segment");

			return url + SegmentDelimiter + segment;
		}
		public static string AddSegment(string url, params string[] segments)
		{
			if (url == null)
				throw new ArgumentNullException("url");
			if (segments == null || segments.Length == 0)
				throw new ArgumentException("segments");
			if (segments.Length == 1)
				return AddSegment(segments[0]);

			var strBuilder = new StringBuilder(url);
			for (int i = 0; i < segments.Length; i++)
			{
				strBuilder
					.Append(SegmentDelimiter)
					.Append(segments[i]);
			}
			return strBuilder.ToString();
		}

		public static string Escape(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			return Uri.EscapeDataString(url);
		}
		public static string Unescape(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			return Uri.UnescapeDataString(url);
		}

		private const string JScriptScheme = "javascript:";
		public static string MakeJScriptUrl(string action)
		{
			return JScriptScheme + action;
		}
		public static bool IsJScriptUrl(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			return url.StartsWith(JScriptScheme);
		}
	}

	internal static class IncludeUtility
	{
		private static readonly Dictionary<IncludeMetaKey, _IncludeMetaMembers> _includesMembers = new Dictionary<IncludeMetaKey, _IncludeMetaMembers>();

		public static T Get<T>(object from, string path)
			where T : class
		{
			if (path == null)
				throw new ArgumentNullException("path");
			if (String.IsNullOrWhiteSpace(path))
				throw new ArgumentException("Path is empty", "path");

			Predicate<IncludeMeta> predicate = (include) => include.Path.Equals(path, StringComparison.OrdinalIgnoreCase);

			IList<IncludeMeta> includes = GetIncludesInternal(from, breakOn: predicate);
			if (includes != null && includes.Count > 0)
			{
				IncludeMeta incl = includes[includes.Count - 1];
				if (predicate(incl))
					return incl.Instance as T;

				foreach (var include in includes)
				{
					if (include.Instance == null)
						continue;

					T found = Get<T>(include.Instance, path);
					if (found != null)
						return found;
				}
			}

			return null;
		}

		public static T GetOrCreate<T>(object from, string path, object[] ctorParams)
			where T : class
		{
			if (path == null)
				throw new ArgumentNullException("path");
			if (String.IsNullOrWhiteSpace(path))
				throw new ArgumentException("Path is empty", "path");

			Predicate<IncludeMeta> predicate = (include) => include.Path.Equals(path, StringComparison.OrdinalIgnoreCase);

			IList<IncludeMeta> includes = GetIncludesInternal(from, breakOn: predicate);
			if (includes != null && includes.Count > 0)
			{
				IncludeMeta incl = includes[includes.Count - 1];
				if (predicate(incl))
					return incl.Instance as T;

				foreach (var include in includes)
				{
					if (include.Instance == null)
						continue;

					T found = GetOrCreate<T>(include.Instance, path, ctorParams);
					if (found != null)
						return found;
				}
			}

			return Create<T>(ctorParams);
		}

		public static T Create<T>(object[] ctorParams)
			where T : class
		{
			return (T)Lexxys.Factory.TryConstruct(typeof(T), ctorParams);
		}

		public static IList<IncludeMeta> GetIncludes(object from)
		{
			if (from == null)
				throw new ArgumentNullException("from");

			return GetIncludesInternal(from);
		}

		public static bool TryInvoke(IncludeMeta include, string name, object[] parameters, out object result)
		{
			if (include.Instance == null)
			{
				result = null;
				return false;
			}

			_IncludeMetaMembers imm = GetIncludeMetaMembers(include);
			return imm.TryInvoke(include.Instance, name, parameters, out result);
		}

		public static bool TryGetMember(IncludeMeta include, string name, out object result)
		{
			if (include.Instance == null)
			{
				result = null;
				return false;
			}
			_IncludeMetaMembers imm = GetIncludeMetaMembers(include);
			return imm.TryGet(include.Instance, name, out result);
		}
		public static bool TrySetMember(IncludeMeta include, string name, object value)
		{
			if (include.Instance == null)
				return false;

			_IncludeMetaMembers imm = GetIncludeMetaMembers(include);
			return imm.TrySet(include.Instance, name, value);
		}


		#region Implementation
		private static readonly _IncludeAttributeComparer Comparer = new _IncludeAttributeComparer();

		private static IList<IncludeMeta> GetIncludesInternal(object from, Predicate<IncludeMeta> breakOn = null)
		{
			PropertyInfo[] props = from.GetType().GetProperties(BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
			if (props.Length > 0)
			{
				SortedDictionary<IncludeAttribute, PropertyInfo> f = null;
				foreach (var prop in props)
				{
					IncludeAttribute attr = prop.GetCustomAttribute<IncludeAttribute>(true);
					if (attr == null)
						continue;

					if (f == null)
						f = new SortedDictionary<IncludeAttribute, PropertyInfo>(Comparer);
					if (!f.ContainsKey(attr))
						f.Add(attr, prop);
				}
				if (f != null)
				{
					List<IncludeMeta> includes = new List<IncludeMeta>(8);
					foreach (var item in f)
					{
						if (item.Value == null)
							break;

						object include = item.Value.GetValue(from);
						var inclMeta = new IncludeMeta(item.Key.Path, include);
						includes.Add(inclMeta);
						if (breakOn != null && breakOn(inclMeta))
							break;
					}
					return includes.AsReadOnly();
				}
			}
			return null;
		}

		private static _IncludeMetaMembers GetIncludeMetaMembers(IncludeMeta include)
		{
			_IncludeMetaMembers imm;
			if (!_includesMembers.TryGetValue(include.GetKey(), out imm))
			{
				lock (_includesMembers)
				{
					if (!_includesMembers.TryGetValue(include.GetKey(), out imm))
					{
						imm = new _IncludeMetaMembers(include.Instance.GetType());
						_includesMembers.Add(include.GetKey(), imm);
					}
				}
			}
			return imm;
		}

		#region Nested Types
		private class _IncludeMetaMembers
		{
			private Dictionary<_MethodSignature, MethodInfo> _methods2 = new Dictionary<_MethodSignature, MethodInfo>();
			private FieldInfo[] _variables;
			private Type _includeType;

			public _IncludeMetaMembers(Type includeType)
			{
				if (includeType == null)
					throw new ArgumentNullException("includeType");

				_includeType = includeType;
			}

			public bool TryInvoke(object _include, string name, object[] parameters, out object result)
			{
				var key = new _MethodSignature(name, GetParametersTypes(parameters));
				MethodInfo method;
				if (!_methods2.TryGetValue(key, out method))
				{
					method = _includeType.GetMethod(name, key.Parameters);
					if (method == null)
					{
						MethodInfo[] methods = _includeType.GetMethods();
						for (int i = 0; i < methods.Length; i++)
						{
							ParameterInfo[] prms;
							if (!methods[i].Name.Equals(name, StringComparison.OrdinalIgnoreCase) || (prms = methods[i].GetParameters()).Length == 0)
								continue;

							bool hasParams = prms[prms.Length - 1].GetCustomAttributes(typeof(ParamArrayAttribute), false).Length > 0;
							if (hasParams)
							{
								parameters = new object[] { null };		// TODO: remove the hack for Lexxys.Factory.Invoke
								method = methods[i];
								break;
							}
						}

						if (method == null)
						{
							result = null;
							return false;
						}
					}

					_methods2.Add(key, method);
				}
				result = Lexxys.Factory.Invoke(_include, method, parameters);
				return true;
			}

			public bool TryGet(object _include, string name, out object result)
			{
				EnsureMembers();

				result = null;
				FieldInfo field = _variables.SingleOrDefault(f => f.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
				if (field == null)
					return false;

				try
				{
					result = field.GetValue(_include);
					return true;
				}
				catch
				{
					return false;
				}
			}
			public bool TrySet(object _include, string name, object value)
			{
				EnsureMembers();

				FieldInfo field = _variables.SingleOrDefault(f => f.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
				if (field == null)
					return false;

				try
				{
					field.SetValue(_include, value);
					return true;
				}
				catch
				{
					return false;
				}
			}


			#region Implementation
			private void EnsureMembers()
			{
				if (_variables == null)
					_variables = _includeType.GetFields();
			}

			private Type[] GetParametersTypes(object[] parameters)
			{
				if (parameters == null || parameters.Length == 0)
					return new Type[0];

				Type[] types = new Type[parameters.Length];
				for (int i = 0; i < parameters.Length; i++)
				{
					types[i] = parameters[i].GetType();
				}
				return types;
			}


			#region Nested Types
			class _MethodSignature
			{
				public _MethodSignature(string name, Type[] parameters)
				{
					if (name == null)
						throw new ArgumentNullException("name");
					if (parameters == null)
						throw new ArgumentNullException("parameters");

					Name = name;
					Parameters = parameters;
				}

				public string Name;

				public Type[] Parameters;

				public override bool Equals(object obj)
				{
					var asMethodSignature = obj as _MethodSignature;
					if (asMethodSignature == null)
						return false;
					if (Name.Equals(asMethodSignature.Name, StringComparison.OrdinalIgnoreCase) && Parameters.Length == asMethodSignature.Parameters.Length)
					{
						for (int i = 0; i < Parameters.Length; i++)
						{
							if (Parameters[i] != asMethodSignature.Parameters[i])
								return false;
						}
					}

					return true;
				}

				// override object.GetHashCode
				public override int GetHashCode()
				{
					return Name.GetHashCode() ^ Parameters.GetHashCode();
				}
			}
			#endregion
			#endregion
		}

		private class _IncludeAttributeComparer : IComparer<IncludeAttribute>
		{
			public int Compare(IncludeAttribute x, IncludeAttribute y)
			{
				return x.Order.CompareTo(y.Order);
			}
		}
		#endregion
		#endregion
	}
}
