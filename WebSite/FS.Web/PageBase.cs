﻿using FS.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Lexxys;

namespace FS.Web
{
	public interface IPage
	{
		HttpSessionState Session { get; }
		HttpRequest Request { get; }
		HttpResponse Response { get; }
		// common
		string MyUrl { get; }
		string MyParams { get; }
		// writing
		void Write(string html);
		void Write(string html1, string html2);
		void Write(string html1, string html2, string html3);
		void Write(params string[] htmls);
		// page
		void PageItem(string name, params string[] args);
		void BeginPage(int width = 780);
		void EndPage();
	}

	[Flags]
	public enum PageParts
	{
		HomeUrl = 1,
		Logout = 2,
		Print = 4,
		HeaderNavigation = 8,
		Menu = 16,
		Back = 32,
		Group = 64
	}

	public abstract class PageBase : ViewPage, IPage
	{
		private HtmlWriter _writer;
		private _Items<string> _items = new _Items<string>();
		private PageParts _parts;
		private string _navigation;
		private string _myUrl;
		private string _myParams;
		private readonly IncludesBag _includes;

		public PageBase()
		{
			_includes = new IncludesBag(this);
		}

		protected PageParts Parts
		{
			get { return _parts; }
		}

		public dynamic External
		{
			get { return _includes; }
		}

		protected string Navigation
		{
			get { return _navigation; }
			set { _navigation = value; }
		}

		public new _Items<string> Items
		{
			get { return _items; }
		}

		public string MyUrl
		{
			get { return _myUrl; }
		}

		public string MyParams
		{
			get { return _myParams; }
		}

		#region Writing
		public void WriteEncoded(string text)
		{
			Writer.WriteEncodedText(text);
		}

		public void Write(string html)
		{
			Writer.Write(html);
		}
		public void Write(string html1, string html2)
		{
			Writer.Write(html1, html2);
		}
		public void Write(string html1, string html2, string html3)
		{
			Writer.Write(html1, html2, html3);
		}
		public void Write(params string[] htmls)
		{
			Writer.Write(htmls);
		}
		#endregion

		public new ClientScripting ClientScript
		{
			get { return __clientScript ?? (__clientScript = new ClientScripting(this)); }
		}
		private ClientScripting __clientScript;

		public ClientStyling ClientStyle
		{
			get { return __clientStyling ?? (__clientStyling = new ClientStyling(this)); }
		}
		private ClientStyling __clientStyling;

		#region Page
		public void PageItem(string name, params string[] args)
		{
			Items.AddItem(name, args);
		}

		public abstract void BeginPage(int width = 780);

		public abstract void EndPage();
		#endregion

		#region Content Mappers
		internal string GetImageUrl(string name)
		{
			if (name.Contains("/"))
				throw new ArgumentException("Invalid character in the file name", "name");

			return Url.Content("~/content/images/" + name);
		}
		internal string GetImageUrl(string cat, string name)
		{
			if (cat.Contains("/"))
				throw new ArgumentException("Invalid character in the category name", "cat");
			if (name.Contains("/"))
				throw new ArgumentException("Invalid character in the file name", "name");

			return Url.Content("~/content/images/" + cat + "/" + name);
		}

		internal string GetScriptUrl(string name)
		{
			if (name.Contains("/"))
				throw new ArgumentException("Invalid character in the file name", "name");

			return Url.Content("~/content/scripts/" + name);
		}
		internal string GetScriptUrl(string cat, string name)
		{
			if (cat.Contains("/"))
				throw new ArgumentException("Invalid character in the category name", "cat");
			if (name.StartsWith("/"))
				throw new ArgumentException("Invalid character in the file name", "name");

			return Url.Content("~/content/scripts/" + cat + "/" + name);
		}

		internal string GetStyleUrl(string name)
		{
			if (name.StartsWith("/"))
				throw new ArgumentException("Invalid character in the file name", "name");

			return Url.Content("~/content/styles/" + name);
		}
		internal string GetStyleUrl(string cat, string name)
		{
			if (cat.Contains("/"))
				throw new ArgumentException("Invalid character in the category name", "cat");
			if (name.StartsWith("/"))
				throw new ArgumentException("Invalid character in the file name", "name");

			return Url.Content("~/content/styles/" + cat + "/" + name);
		}
		#endregion

		public IDisposable Page2()
		{
			return new _PageContainer(this);
		}
		public IDisposable Page2(int width)
		{
			return new _PageContainer(this, width);
		}


		protected void InitInclude(object instance)
		{
			IncludeMeta found = _includes.Find(incl => Object.ReferenceEquals(incl.Instance, instance));
			if (found == null)
				throw new InvalidOperationException("Include cannot be found be specified instance");
			InitializeInclude(found, force: true);
		}
		protected void InitInclude(string path)
		{
			IncludeMeta found = _includes.Find(incl => IncludeMeta.SamePath(incl, path));
			if (found == null)
				throw new InvalidOperationException("Include cannot be found be specified path");
			InitializeInclude(found, force: true);
		}

		protected void InitIncludes()
		{
			EnsureInternalIncludes();

			_includes.EnumerateAsInitialize((incl, index) => InitializeInclude(incl));
		}

		protected T Include<T>(string path, bool suppressInit = false)
			where T : class
		{
			if (path == null)
				throw new ArgumentNullException("path");
			if (String.IsNullOrWhiteSpace(path))
				throw new ArgumentException("Path is empty", "path");

			EnsureInternalIncludes();

			if (_includes.Find(incl => IncludeMeta.SamePath(incl, path)) != null)
				throw new InvalidOperationException("Include with the same path has been already added");

			T include = IncludeUtility.Create<T>(new[] { this });
			IncludeMeta includeMeta = new IncludeMeta(path, include);
			if (suppressInit)
				includeMeta.State = IncludeState.InitSuppressed;

			_includes.Add(includeMeta);
			RetrieveIncludes(include);

			return include;
		}

		//protected T Include<T>(string path, T include, object parent = null)
		//	where T : class
		//{
		//	if (path == null)
		//		throw new ArgumentNullException("path");
		//	if (String.IsNullOrWhiteSpace(path))
		//		throw new ArgumentException("Path is empty", "path");
		//	if (include == null)
		//		throw new ArgumentNullException("include");

		//	EnsureStaticIncludes(this);

		//	IncludeMeta parentIncludeMeta = (parent != null)
		//		? _includes.Find(incl => Object.ReferenceEquals(incl.Instance, parent))
		//		: null;

		//	IncludeMeta includeMeta = new IncludeMeta(path, include);
		//	IncludeMeta existed = _includes.Find(incl => IncludeMeta.SamePath(incl, path));
		//	if (existed != null)
		//	{
		//		if (existed.Instance != null)
		//			throw new InvalidOperationException("Include instance already exists");
		//		_includes.Replace(existed, includeMeta);
		//	}
		//	else
		//	{
		//		_includes.Add(includeMeta, parentIncludeMeta);
		//		RetrieveIncludes(include, parentIncludeMeta);
		//	}

		//	//InitializeInclude(includeMeta);
		//	EnsureIncludesInitialized();

		//	return include;
		//}


		#region Implementation
		protected virtual void InitInternal()
		{
			_myUrl = Request.ServerVariables["URL"];
			_myParams = Request.ServerVariables["QUERY_STRING"];
		}

		private void RetrieveIncludes(object from, IncludeMeta parent = null)
		{
			IList<IncludeMeta> includes = IncludeUtility.GetIncludes(from);
			if (includes == null || includes.Count == 0)
				return;

			if (parent == null)
				parent = _includes.Find(incl => Object.ReferenceEquals(incl.Instance, from));
			foreach (var incl in includes)
			{
				_includes.Add(incl, parent);
				if (incl.Instance != null)
					RetrieveIncludes(incl.Instance, incl);
			}
		}

		private void EnsureInternalIncludes()
		{
			if (_internalIncludesEnsured)
				return;
			_internalIncludesEnsured = true;

			RetrieveIncludes(this);
		}
		private bool _internalIncludesEnsured;

		private static void InitializeInclude(IncludeMeta include, bool force = false)
		{
			if (include == null)
				throw new ArgumentNullException("include");
			if (!force && (include.Instance == null || include.State >= IncludeState.Initialized))
				return;

			var asRequireInit = include.Instance as IRequireInit;
			if (asRequireInit != null)
				asRequireInit.Init();
			include.State = IncludeState.Initialized;
		}

		private void EnsureIncludesInitialized()
		{
			_includes.EnumerateAsInitialize((incl, index) => InitializeInclude(incl));
		}

		private const string StateSessionKey = "__Page__State";
		protected void SaveState()
		{
			Session[StateSessionKey] = new _PageState() { Parts = Parts, Navigation = Navigation };
		}
		protected void RestoreState()
		{
			var state = Session[StateSessionKey] as _PageState;
			if (state != null)
			{
				_parts = state.Parts;
				_navigation = state.Navigation;
			}
		}
		protected void PrepareState()
		{
			_parts = PageParts.HomeUrl | PageParts.Logout | PageParts.Print | PageParts.HeaderNavigation | PageParts.Menu | PageParts.Back | PageParts.Group;

			string[][] toShow = Items.GetItems("show");
			for (int i = 0; i < toShow.Length; i++)
			{
				for (int n = 0; n < toShow[i].Length; n++)
				{
					switch (toShow[i][n].ToUpperInvariant())
					{
						case "HOME":
							_parts = Parts | PageParts.HomeUrl;
							break;
						case "LOGOUT":
							_parts = Parts | PageParts.Logout;
							break;
						case "PRINT":
							_parts = Parts | PageParts.Print;
							break;
						case "NAVIGATION":
							_parts = Parts | PageParts.HeaderNavigation;
							break;
						case "HEADERMENU":
							_parts = Parts | PageParts.Menu;
							break;
						case "BACK":
							_parts = Parts | PageParts.Back;
							break;
						case "GROUP":
							_parts = Parts | PageParts.Group;
							break;
					}
				}
			}

			string[][] toHide = Items.GetItems("hide");
			for (int i = 0; i < toHide.Length; i++)
			{
				for (int n = 0; n < toHide[i].Length; n++)
				{
					switch (toHide[i][n].ToUpperInvariant())
					{
						case "HOME":
							_parts = Parts & ~PageParts.HomeUrl;
							break;
						case "LOGOUT":
							_parts = Parts & ~PageParts.Logout;
							break;
						case "PRINT":
							_parts = Parts & ~PageParts.Print;
							break;
						case "NAVIGATION":
							_parts = Parts & ~PageParts.HeaderNavigation;
							break;
						case "HEADERMENU":
							_parts = Parts & ~PageParts.Menu;
							break;
						case "BACK":
							_parts = Parts & ~PageParts.Back;
							break;
						case "GROUP":
							_parts = Parts & ~PageParts.Group;
							break;
						case "ALL":
							_parts = 0;
							break;
					}
				}
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			_writer = new HtmlWriter(writer);

			base.Render(writer);
		}

		protected sealed override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			InitInternal();
		}
		protected override void OnUnload(EventArgs e)
		{
			base.OnUnload(e);

			if (_writer != null)
				_writer.Dispose();
		}

		#region Nested Types
		private class _PageState
		{
			public PageParts Parts;

			public string Navigation;
		}

		private class _PageContainer : IDisposable
		{
			private readonly PageBase _page;

			public _PageContainer(PageBase page)
			{
				if (page == null)
					throw EX.ArgumentNull("page");
				
				_page = page;
				_page.BeginPage();
			}
			public _PageContainer(PageBase page, int width)
			{
				if (page == null)
					throw Lexxys.EX.ArgumentNull("page");

				_page = page;
				_page.BeginPage(width);
			}

			public void Dispose()
			{
				_page.EndPage();
			}
		}
		#endregion
		#endregion
	}

	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public sealed class IncludeAttribute : Attribute
	{
		private readonly string _path;
		private readonly int _order;

		public IncludeAttribute(string path, int order)
		{
			if (path == null)
				throw new ArgumentNullException("path");
			if (String.IsNullOrWhiteSpace(path))
				throw new ArgumentException("Path is empty", "path");

			_path = path;
			_order = order;
		}

		public string Path
		{
			get { return _path; }
		}

		public int Order
		{
			get { return _order; }
		}
	}

	class IncludesBag : System.Dynamic.DynamicObject
	{
		private List<IncludeMeta> _initialization = new List<IncludeMeta>(8);
		private List<IncludeMeta> _implementation = new List<IncludeMeta>(8);

		private readonly PageBase _page;
		public IncludesBag(PageBase page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public void Add(IncludeMeta include, IncludeMeta parent = null)
		{
			int initIndex;
			int implIndex;
			if (parent != null)
			{
				if ((initIndex = _initialization.IndexOf(parent)) < 0)
					throw new ArgumentException("Unknown include", "parent");

				if ((implIndex = _implementation.IndexOf(parent)) < 0)
					throw new ArgumentException("Unknown include", "parent");
				implIndex++;
			}
			else
			{
				initIndex = _initialization.Count;
				implIndex = 0;
			}

			_implementation.Insert(implIndex, include);
			_initialization.Insert(initIndex, include);
		}

		public IncludeMeta Find(Predicate<IncludeMeta> predicate)
		{
			return _initialization.Find(predicate);
		}

		public void Replace(IncludeMeta what, IncludeMeta with)
		{
			int implIndex = _implementation.FindIndex(x => Object.ReferenceEquals(x, with));
			int initIndex = _initialization.FindIndex(x => Object.ReferenceEquals(x, with));
			if (implIndex < 0 || initIndex < 0)
				throw new ArgumentException("Unknown include", "what");
			_implementation[implIndex] = with;
			_initialization[initIndex] = with;
		}

		public void EnumerateAsInitialize(Action<IncludeMeta, int> action)
		{
			if (action == null)
				throw new ArgumentNullException("action");

			for (int i = 0; i < _initialization.Count; i++)
			{
				action(_initialization[i], i);
			}
		}
		public void EnumerateAsImplement(Action<IncludeMeta, int> action)
		{
			if (action == null)
				throw new ArgumentNullException("action");

			for (int i = 0; i < _implementation.Count; i++)
			{
				action(_implementation[i], i);
			}
		}

		public override bool TryInvokeMember(System.Dynamic.InvokeMemberBinder binder, object[] args, out object result)
		{
			foreach (var incl in _implementation)
			{
				if (IncludeUtility.TryInvoke(incl, binder.Name, args, out result))
					return true;
			}

			result = null;
			return false;
		}

		public override bool TryGetMember(System.Dynamic.GetMemberBinder binder, out object result)
		{
			foreach (var incl in _implementation)
			{
				if (IncludeUtility.TryGetMember(incl, binder.Name, out result))
					return true;
			}

			result = null;
			return false;
		}

		public override bool TrySetMember(System.Dynamic.SetMemberBinder binder, object value)
		{
			foreach (var incl in _implementation)
			{
				if (IncludeUtility.TrySetMember(incl, binder.Name, value))
					return true;
			}

			return false;
		}
	}

	enum IncludeState
	{
		None,
		Initialized,
		InitSuppressed,
	}

	class IncludeMetaKey
	{
		private string _path;

		public IncludeMetaKey(string path)
		{
			_path = path;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
				return false;

			return _path.Equals(((IncludeMetaKey)obj)._path, StringComparison.OrdinalIgnoreCase);
		}
		public override int GetHashCode()
		{
			return _path.GetHashCode();
		}
	}
	class IncludeMeta
	{
		private readonly string _path;
		private readonly object _instance;

		internal IncludeMeta(string path, object instance)
		{
			_path = path;
			_instance = instance;
		}

		public string Path
		{
			get { return _path; }
		}

		public object Instance
		{
			get { return _instance; }
		}

		public IncludeState State { get; set; }

		public IncludeMetaKey GetKey()
		{
			return __key ?? (__key = new IncludeMetaKey(_path));
		}
		private IncludeMetaKey __key;

		public static bool SamePath(IncludeMeta meta, string path)
		{
			return SamePath(meta._path, path);
		}
		public static bool SamePath(string path1, string path2)
		{
			return path1.Equals(path2, StringComparison.OrdinalIgnoreCase);
		}
	}

	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class PageAttribute : Attribute
	{
		private readonly string _path;

		public PageAttribute(string path)
		{
			if (path == null)
				throw new ArgumentNullException("path");
			if (String.IsNullOrWhiteSpace(path))
				throw new ArgumentException("Path is empty", "path");

			_path = path;
		}

		public string Path
		{
			get { return _path; }
		}
	}
}
