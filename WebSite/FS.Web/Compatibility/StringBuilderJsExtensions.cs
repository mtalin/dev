﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Compatibility
{
	public static class StringBuilderJsExtensions
	{
		public static StringBuilder JValue(this StringBuilder strBuilder, string value)
		{
			return strBuilder.Append(HtmlUtility.JValue(value));
		}

		public static StringBuilder JValue(this StringBuilder strBuilder, bool value)
		{
			return strBuilder.Append(HtmlUtility.JValue(value));
		}

		public static StringBuilder JValue(this StringBuilder strBuilder, DateTime value)
		{
			return strBuilder.Append(HtmlUtility.JValue(value));
		}

		public static StringBuilder JValue(this StringBuilder strBuilder, int value)
		{
			return strBuilder.Append(HtmlUtility.JValue(value));
		}

		public static StringBuilder JValue(this StringBuilder strBuilder, double value)
		{
			return strBuilder.Append(HtmlUtility.JValue(value));
		}
	}
}
