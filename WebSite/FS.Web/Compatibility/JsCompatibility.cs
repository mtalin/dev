﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Compatibility
{
	internal static class JsCompatibility
	{
		public static string ToString(object value)
		{
			if (value == null)
				return "null";

			switch (Type.GetTypeCode(value.GetType()))
			{
				case TypeCode.String:
					return ToString((string)value);
				case TypeCode.Decimal:
					return ((decimal)value).ToString();
				case TypeCode.Double:
					return ToString((double)value);
				case TypeCode.Int16:
					return ((short)value).ToString();
				case TypeCode.Int32:
					return ToString((int)value);
				case TypeCode.Int64:
					return ((long)value).ToString();
				case TypeCode.Single:
					return ((float)value).ToString();
				case TypeCode.UInt16:
					return ((ushort)value).ToString();
				case TypeCode.UInt32:
					return ((uint)value).ToString();
				case TypeCode.UInt64:
					return ((ulong)value).ToString();
				case TypeCode.Boolean:
					return ToString((bool)value);
				default:
					return value.ToString();
			}
		}
		public static string ToString(string value)
		{
			return value;
		}
		public static string ToString(bool value)
		{
			return value ? "true" : "false";
		}
		public static string ToString(int value)
		{
			return value.ToString();
		}
		public static string ToString(double value)
		{
			return value.ToString();
		}

		/// <summary>
		/// js equivalent: value > ""
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool HasValue(object value)
		{
			switch (Type.GetTypeCode(value.GetType()))
			{
				case TypeCode.String:
					return HasValue((string)value);
				case TypeCode.Decimal:
					return (decimal)value > 0;
				case TypeCode.Double:
					return HasValue((double)value);
				case TypeCode.Int16:
					return (short)value > 0;
				case TypeCode.Int32:
					return HasValue((int)value);
				case TypeCode.Int64:
					return (long)value > 0;
				case TypeCode.Single:
					return (float)value > 0;
				case TypeCode.UInt16:
					return (ushort)value > 0;
				case TypeCode.UInt32:
					return (uint)value > 0;
				case TypeCode.UInt64:
					return (ulong)value > 0;
				case TypeCode.Boolean:
					return HasValue((bool)value);
				default:
					return value != null;
			}
		}
		public static bool HasValue(string value)
		{
			return !String.IsNullOrEmpty(value);
		}
		public static bool HasValue(bool value)
		{
			return value;
		}
		public static bool HasValue(int value)
		{
			return value > 0;
		}
		public static bool HasValue(double value)
		{
			return value > 0;
		}

		public static bool TryParseInt(string value, out int result)
		{
			if (!String.IsNullOrEmpty(value))
			{
				value = value.Trim();
				int i;
				for (i = 0; i < value.Length; i++)
				{
					if (!(Char.IsNumber(value[i]) || value[i] == '+' || value[i] == '-'))
						break;
				}
				value = value.Substring(0, i);
				if (Int32.TryParse(value, NumberStyles.AllowTrailingSign | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out i))
				{
					result = i;
					return true;
				}
			}
			result = 0;
			return false;
		}

		public static int ParseInt(string value)
		{
			int i;
			if (!TryParseInt(value, out i))
				throw new ArgumentException("value");
			return i;
		}

		public static T[] Arguments<T>(params T[] args)
		{
			return args;
		}
		public static T[] Arguments<T>(T arg1, params T[] args)
		{
			T[] res = new T[args.Length + 1];
			Array.Copy(args, 0, res, 1, args.Length);
			res[0] = arg1;
			return res;
		}
		public static T[] Arguments<T>(T arg1, T arg2, params T[] args)
		{
			T[] res = new T[args.Length + 2];
			Array.Copy(args, 0, res, 2, args.Length);
			res[0] = arg1;
			res[1] = arg2;
			return res;
		}
		public static T[] Arguments<T>(T arg1, T arg2, T arg3, params T[] args)
		{
			T[] res = new T[args.Length + 3];
			Array.Copy(args, 0, res, 3, args.Length);
			res[0] = arg1;
			res[1] = arg2;
			res[2] = arg3;
			return res;
		}
		public static T[] Arguments<T>(T arg1, T arg2, T arg3, T arg4, params T[] args)
		{
			T[] res = new T[args.Length + 4];
			Array.Copy(args, 0, res, 4, args.Length);
			res[0] = arg1;
			res[1] = arg2;
			res[2] = arg3;
			res[3] = arg4;
			return res;
		}

		/// <summary>
		/// js equivalent: !!value
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ToBoolean(object value)
		{
			if (value == null)
				return false;

			switch (Type.GetTypeCode(value.GetType()))
			{
				case TypeCode.String:
					return ToBoolean((string)value);
				case TypeCode.Decimal:
					return ToBoolean((decimal)value);
				case TypeCode.Double:
					return ToBoolean((double)value);
				case TypeCode.Int16:
					return ToBoolean((short)value);
				case TypeCode.Int32:
					return ToBoolean((int)value);
				case TypeCode.Int64:
					return ToBoolean((long)value);
				case TypeCode.Single:
					return ToBoolean((float)value);
				case TypeCode.UInt16:
					return ToBoolean((ushort)value);
				case TypeCode.UInt32:
					return ToBoolean((uint)value);
				case TypeCode.UInt64:
					return ToBoolean((ulong)value);
				case TypeCode.Boolean:
					return ToBoolean((bool)value);
				case TypeCode.DateTime:
					return ToBoolean((DateTime)value);
				default:
					return false;
			}
		}
		public static bool ToBoolean(string value)
		{
			return !String.IsNullOrEmpty(value);
		}
		public static bool ToBoolean(short value)
		{
			return value != 0;
		}
		public static bool ToBoolean(int value)
		{
			return value != 0;
		}
		public static bool ToBoolean(long value)
		{
			return value != 0;
		}
		public static bool ToBoolean(ushort value)
		{
			return value != 0;
		}
		public static bool ToBoolean(uint value)
		{
			return value != 0;
		}
		public static bool ToBoolean(ulong value)
		{
			return value != 0;
		}
		public static bool ToBoolean(decimal value)
		{
			return value != 0;
		}
		public static bool ToBoolean(double value)
		{
			return value != 0;
		}
		public static bool ToBoolean(float value)
		{
			return value != 0;
		}
		public static bool ToBoolean(bool value)
		{
			return value;
		}
		public static bool ToBoolean(DateTime value)
		{
			return value > DateTime.MinValue;
		}
	}
}
