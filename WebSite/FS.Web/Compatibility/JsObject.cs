﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Compatibility
{
	public abstract class JsObject<T> : System.Dynamic.DynamicObject, IEnumerable<string>
	{
		private readonly Dictionary<string, T> _storage;

		public JsObject()
			: this(StringComparer.InvariantCultureIgnoreCase)
		{ }
		public JsObject(IEqualityComparer<string> keyComparer)
		{
			if (keyComparer == null)
				throw new ArgumentNullException("keyComparer");

			_storage = new Dictionary<string, T>(keyComparer);
		}
		public JsObject(Dictionary<string, T> storage)
		{
			if (_storage == null)
				throw new ArgumentNullException("storage");

			_storage = storage;
		}

		protected Dictionary<string, T> Storage
		{
			get { return _storage; }
		}

		public override bool TryGetMember(System.Dynamic.GetMemberBinder binder, out object result)
		{
			T value;
			if (_storage.TryGetValue(binder.Name, out value))
				result = value;
			else/* if (!base.TryGetMember(binder, out result))*/
				result = null;
			return true;
		}
		public override bool TrySetMember(System.Dynamic.SetMemberBinder binder, object value)
		{
			_storage[binder.Name] = (T)value;
			return true;
		}

		public override bool TrySetIndex(System.Dynamic.SetIndexBinder binder, object[] indexes, object value)
		{
			string key;
			if (indexes.Length == 1 && (key = indexes[0] as string) != null && typeof(T).IsAssignableFrom(value.GetType()))
			{
				_storage.Add(key, (T)value);
				return true;
			}
			return false;
		}
		public override bool TryGetIndex(System.Dynamic.GetIndexBinder binder, object[] indexes, out object result)
		{
			string key;
			if (indexes.Length == 1 && (key = indexes[0] as string) != null)
			{
				T result1;
				result = _storage.TryGetValue(key, out result1)
					? result1
					: default(T);
				return true;
			}
			result = null;
			return false;
		}

		/// <summary>
		/// Returns a collection of properties names
		/// </summary>
		/// <returns></returns>
		public IEnumerator<string> GetEnumerator()
		{
			return _storage.Keys.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return _storage.Keys.GetEnumerator();
		}
	}
}
