﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FS.Web
{
	public enum DoctypeMode
	{
		All,
		StrictHtml,
		TransitionalHtml,
		FrameSetHtml,
		StrictXhtml,
		TransitionalXhtml,
		FrameSetXhtml,
		Xhtml11
	}

	[Flags]
	public enum HtmlWriterSettings
	{
		None = 0,
		FormattedHtml = 1,
	}

	public class HtmlWriter : IDisposable
	{
		public const string DefaultTabString = "\t";

		private TextWriter _writer;
		private static readonly IList<string> NoAttrValue = null;
		private readonly HtmlWriterSettings _settings;
		private int _indent;
		private int _indentBackup;
		private readonly bool _ownWriter;
		private readonly List<Tuple<string, IList<string>>> _attributes = new List<Tuple<string, IList<string>>>(4);
		private readonly Stack<string> _tags = new Stack<string>(16);

		public HtmlWriter(HtmlWriterSettings settings = HtmlWriterSettings.FormattedHtml)
			: this(new StringWriter(), settings)
		{
			_ownWriter = true;
		}
		public HtmlWriter(TextWriter writer, HtmlWriterSettings settings = HtmlWriterSettings.FormattedHtml)
		{
			if (writer == null)
				throw new ArgumentNullException("writer");

			_writer = writer;
			_settings = settings;
		}

		public TextWriter InnerWriter
		{
			get { return _writer; }
		}

		public int Indent
		{
			get { return _indent; }
			set
			{
				if ((_settings & HtmlWriterSettings.FormattedHtml) > 0)
					throw new InvalidOperationException("Indent cannot be set explicitly when FormattedHtml is set.");
				_indent = value;
			}
		}

		public HtmlWriter Doctype(DoctypeMode mode)
		{
			string doctype;
			switch (mode)
			{
				case DoctypeMode.StrictHtml:
					doctype = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">";
					break;
				case DoctypeMode.TransitionalHtml:
					doctype = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
					break;
				case DoctypeMode.FrameSetHtml:
					doctype = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\">";
					break;
				case DoctypeMode.StrictXhtml:
					doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";
					break;
				case DoctypeMode.TransitionalXhtml:
					doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
					break;
				case DoctypeMode.FrameSetXhtml:
					doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Frameset//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">";
					break;
				case DoctypeMode.Xhtml11:
					doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">";
					break;
				case DoctypeMode.All:
				default:
					doctype = "<!DOCTYPE html>";
					break;
			}
			_WriteLine(doctype);
			return this;
		}
		public HtmlWriter Doctype(string rootElement, string fpi, string uri)
		{
			_WriteLine(String.Format("<!DOCTYPE \"{0}\" PUBLIC \"{1}\" \"{2}\">", rootElement, fpi, uri));
			return this;
		}

		public HtmlWriter Tag(string name)
		{
			_Write(HtmlUtility.OpenTag(name));
			FlushAttributes();
			if (IsTagFormatSensitive(name))
			{
				_Write(HtmlUtility.CloseTag(false));
				_indentBackup = _indent;
				_indent = 0;
			}
			else
			{
				_WriteLine(HtmlUtility.CloseTag(false));
				if ((_settings & HtmlWriterSettings.FormattedHtml) > 0)
					_indent++;
			}
			_tags.Push(name);
			return this;
		}
		public HtmlWriter TagSingle(string name)
		{
			_Write(HtmlUtility.OpenTag(name));
			FlushAttributes();
			_WriteLine(HtmlUtility.CloseTag(true));
			return this;
		}

		public HtmlWriter End(string name = null)
		{
			if (_tags.Count == 0)
				throw new InvalidOperationException("There are no beginnning tags");
			if (name != null && !_tags.Peek().Equals(name, StringComparison.OrdinalIgnoreCase))
				throw new InvalidOperationException(String.Format("There is no corresponding beginning tag. Actual: {0}, expected: {1}", _tags.Peek(), name));

			if (IsTagFormatSensitive(_tags.Peek()))
				_indent = _indentBackup;
			else if ((_settings & HtmlWriterSettings.FormattedHtml) > 0)
				_indent--;
			_WriteLine(HtmlUtility.EndTag(_tags.Pop()));
			return this;
		}

		public HtmlWriter Attr(string attr)
		{
			var attrInfo = HtmlUtility.ParseAttribute(attr);
			if (attrInfo != null)
				Attr(attrInfo.Item1, attrInfo.Item2);
			else
				_attributes.Add(Tuple.Create(attr, NoAttrValue));
			return this;
		}
		public HtmlWriter Attr(string[] attrs)
		{
			if (attrs != null && attrs.Length > 0)
			{
				for (int i = 0; i < attrs.Length; i++)
				{
					Attr(attrs[i]);
				}
			}

			return this;
		}
		public HtmlWriter Attr(string name, string value)
		{
			_attributes.Add(Tuple.Create<string, IList<string>>(name, new[] { value }));
			return this;
		}
		public HtmlWriter Attr(string name, string value1, string value2)
		{
			_attributes.Add(Tuple.Create<string, IList<string>>(name, new[] { value1, value2 }));
			return this;
		}
		public HtmlWriter Attr(string name, string value1, string value2, string value3)
		{
			_attributes.Add(Tuple.Create<string, IList<string>>(name, new[] { value1, value2, value3 }));
			return this;
		}

		private const string StyleAttribute = "style";
		public HtmlWriter AttrStyle(string name, string value)
		{
			string styleAttr = HtmlUtility.GetStyleRule(name, value);
			var found = _attributes.Find(attr => attr.Item1.Equals(StyleAttribute, StringComparison.OrdinalIgnoreCase));
			if (found == null)
			{
				_attributes.Add(Tuple.Create<string, IList<string>>(StyleAttribute, new List<string>() { styleAttr }));
			}
			else
			{
				System.Diagnostics.Debug.Assert(!found.Item2.IsReadOnly);
				found.Item2.Add(styleAttr);
			}
			return this;
		}

		public HtmlWriter WriteComment(string text)
		{
			Write(HtmlUtility.GetComment(text));
			return this;
		}

		private static readonly Regex _r = new Regex("(.+)$", RegexOptions.Multiline | RegexOptions.Compiled);

		public HtmlWriter WriteEncoded(string text)
		{
			if (_tags.Count > 0 && IsTagFormatSensitive(_tags.Peek()))
				_Write(HtmlUtility.HtmlEncode(text));
			else
				_WriteLine(HtmlUtility.HtmlEncode(text));
			return this;
		}

		public HtmlWriter Write(string html)
		{
			_WriteLine(html);
			return this;
		}
		public HtmlWriter Write(string html1, string html2)
		{
			_WriteLine(html1, html2);
			return this;
		}
		public HtmlWriter Write(string html1, string html2, string html3)
		{
			_WriteLine(html1, html2, html3);
			return this;
		}
		public HtmlWriter Write(params string[] htmls)
		{
			if (htmls != null && htmls.Length > 0)
			{
				for (int i = 0; i < htmls.Length - 1; i++)
				{
					_Write(htmls[i]);
				}
				_WriteLine(htmls[htmls.Length - 1]);
			}
			else
			{
				_WriteLine();
			}
			return this;
		}
		
		public HtmlWriter WriteFormat(string pattern, params string[] args)
		{
			return Write(String.Format(pattern, args));
		}

		public bool HasAttributeInBuffer(string value)
		{
			if (_attributes.Count == 0)
				return false;

			return _attributes.Any(key => key.Item1.Equals(value, StringComparison.OrdinalIgnoreCase));
		}

		/// <summary>
		/// Gets html string.
		/// </summary>
		/// <exception cref="System.InvalidOperationException"></exception>
		/// <returns></returns>
		public string GetContent()
		{
			if (!_ownWriter)
				throw new InvalidOperationException("GetContent. Cannot get content when external writer is used.");

			StringWriter asStringWriter = _writer as StringWriter;
			if (asStringWriter == null)
				throw new InvalidOperationException("StringWriter type is expected");

			return asStringWriter.ToString();
		}


		#region Implementation
		private void FlushAttributes()
		{
			if (_attributes.Count > 0)
			{
				for (int i = 0; i < _attributes.Count; i++)
				{
					if (_attributes[i].Item2 != NoAttrValue)
						_Write(HtmlUtility.AttributePad(_attributes[i].Item1, _attributes[i].Item2.ToArray()));
					else
						_Write(HtmlUtility.AttributeListPad(_attributes[i].Item1));
				}
				_attributes.Clear();
			}
		}

		private bool _needIndentation;
		private string GetIndent()
		{
			if (_indent == 0 || !_needIndentation)
				return "";
			if (_indent == 1)
				return DefaultTabString;
			if (_indent == 2)
				return DefaultTabString + DefaultTabString;

			StringBuilder strBuilder = new StringBuilder();
			for (int i = 0; i < _indent; i++)
			{
				strBuilder.Append(DefaultTabString);
			}
			return strBuilder.ToString();
		}

		private void _Write(string html)
		{
			if (String.IsNullOrEmpty(html))
				return;

			if (_r.IsMatch(html))
				// TODO: the last line is not indented
				html = _r.Replace(html, GetIndent() + "$1");
			else
				_writer.Write(GetIndent());

			if ((_settings & HtmlWriterSettings.FormattedHtml) == 0 && (_tags.Count == 0 || !IsTagFormatSensitive(_tags.Peek())))
				html = html.Replace(Environment.NewLine, "");

			_writer.Write(html);
			_needIndentation = false;
		}

		private void _WriteLine(params string[] htmls)
		{
			for (int i = 0; i < htmls.Length; i++)
			{
				_Write(htmls[i]);
			}

			if ((_settings & HtmlWriterSettings.FormattedHtml) > 0)
			{
				_writer.WriteLine();
				_needIndentation = true;
			}
		}

		private bool IsTagFormatSensitive(string tagName)
		{
			return tagName.Equals("pre", StringComparison.OrdinalIgnoreCase);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && _writer != null)
			{
				_writer.Dispose();
				_writer = null;
			}
		}
		~HtmlWriter()
		{
			Dispose(false);
		}
		#endregion
	}

	public static class HtmlWriterExtensions
	{
		public static void Clear(this HtmlWriter writer)
		{
			var asStringWriter = writer.InnerWriter as StringWriter;
			if (asStringWriter == null)
				throw new InvalidOperationException("StringWriter type is expected");
			
			asStringWriter.GetStringBuilder().Clear();
		}

		public static HtmlWriter Img(this HtmlWriter writer, string src, int? width = null, int? height = null, string alt = "", int border = 0, int? hSpace = null, int? vSpace = null, string style = null, string id = null)
		{
			return writer.Write(HtmlUtility.Img(src, width, height, alt, border, hSpace, vSpace, style, id));
		}

		public static HtmlWriter Submit(this HtmlWriter writer, string name, string value, string id = null, string cssClass = null)
		{
			return writer.Write(HtmlUtility.Submit(name, value, id, cssClass));
		}

		public static HtmlWriter Hidden(this HtmlWriter writer, string name, string value, string id = null)
		{
			return writer.Write(HtmlUtility.Hidden(name, value, id));
		}

		public static HtmlWriter Br(this HtmlWriter writer)
		{
			return writer.Write(HtmlUtility.Br);
		}
	}
}
