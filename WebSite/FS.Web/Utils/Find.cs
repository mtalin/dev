﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Lexxys;

namespace FS.Web.Utils
{
	public sealed class Find : IRequireInit
	{
		private readonly PageCommon _page;

		public Find(PageCommon page)
		{
			if (page == null)
				throw EX.ArgumentNull("page");

			_page = page;
		}

		#region Foundation
		public string oFindFoundation(params string[] attrs)
		{
			RegisterMsAjax("find_Fnd_panel", "AjaxControlToolkit.DropDownBehavior, { \"dropDownControl\": getElement(\"find_Fnd_panel\") }, null, null, getElement(\"find_Fnd_caption\")");

			return new _FindFoundationMeta(_page).oInput(attrs);
		}

		public bool fmPressedFindFoundation(params string[] args)
		{
			return new _FindFoundationMeta(_page).fmPressed(args);
		}

		public string fmFindFoundation(params string[] args)
		{
			return new _FindFoundationMeta(_page).fmFind(args);
		}

		public const string FoundationNameToken = "nam";
		public const string FsAccountToken = "fid";
		public const string ShellCorpNumberToken = "shn";
		public const string AccountNumberToken = "acc";
		public const string EinToken = "ein";
		public const string PartnerToken = "par";
		public const string OfficerToken = "ofc";
		public const string PortfolioCodeToken = "pcd";
		public const string KeyInfoToken = "knf";
		public const string PhilanthropicIntToken = "pnt";

		private class _FindFoundationMeta : FindMetaBase
		{
			public _FindFoundationMeta(PageCommon page)
				: base(
					"Fnd", "FIND FOUNDATION",
					new FindMenuMetaBase(FoundationNameToken, "BY NAME", 200, DecorateTitle(" by Foundation Name ")),
					new FindMenuMetaBase(FsAccountToken, "BY ID", 20, DecorateTitle(" by FS Account # ")),
					new FindMenuMetaBase(ShellCorpNumberToken, "BY SHELL #", 20, DecorateTitle(" by Shell Corporation Number ")),
					new FindMenuMetaBase(AccountNumberToken, "BY ACCOUNT #", 20, DecorateTitle(" by Account Number ")),
					new FindMenuMetaBase(EinToken, "BY EIN", 10, DecorateTitle(" by EIN ")),
					new FindMenuMetaBase(PartnerToken, "BY PARTNER", 200, DecorateTitle(" by Financial Partner ")),
					new FindMenuMetaBase(OfficerToken, "BY OFFICER", 100, DecorateTitle(" by Foundation Officer ")),
					new FindMenuMetaBase(PortfolioCodeToken, "BY PORTFOLIO CODE", 200, DecorateTitle(" by Portfolio Code ")),
					new FindMenuMetaBase(KeyInfoToken, "BY KEY INFO", 200, DecorateTitle(" by Key Info ")),
					new FindMenuMetaBase(PhilanthropicIntToken, "BY INTEREST", 200, DecorateTitle(" by Philanthropic Interest "))
					)
			{
				Initialize(page);
			}

			public override string BuildSql()
			{
				switch (Token)
				{
					case FoundationNameToken:
						return @"
(select Id
	from Foundations
	where" + SqlAndLikeBegining("Name", Value) +
")";
					case FsAccountToken:
						return @"
(select Id
	from Foundations
	where Id = " + ap_dbid(Value) +
")";
					case ShellCorpNumberToken:
						return @"
(select f.Id
	from Foundations f
		join ShellCorporations sc on sc.Id = f.ShellCorporation
	where sc.ShellNumber = " + ap_dbid(Value) +
")";
					case AccountNumberToken:
						return @"
(select f.Id
	from Foundations f
		join FoundationAccounts fa on fa.Foundation = f.Id
	where fa.Account like " + ap_dbstr(Value + "%") +
")";
					case EinToken:
						return @"
(select Id
	from Foundations
	where ein like " + ap_dbstr(Value + "%") +
")";
					case PartnerToken:
						return @"
(select f.Id
	from Foundations f
		join FinancialPartners fp on fp.Id = f.FinancialPartner
	where" + SqlAndLikeBegining("fp.Name", Value) +
")";
					case OfficerToken:
						return @"
(select f.Id
	from Foundations f
		join Nominees nm on nm.Foundation=f.Id
		join Persons p on p.Id = nm.Person
	where" + SqlAndLikeBegining("p.FirstName + ' ' + p.LastName", Value) +
")";
					case PortfolioCodeToken:
						return @"
(select f.Id
	from Foundations f
		join FoundationAccounts fa on fa.Foundation = f.Id
	where fa.PortfolioCode like " + ap_dbstr(Value + "%") +
")";
					case KeyInfoToken:
						return @"
(select Id
	from Foundations
	where" + SqlAndLikeBegining("ClientKeyInfo", Value) +
")";
					case PhilanthropicIntToken:
						return @"
(select f.Id
	from Foundations f
		join Enums e on e.Category = 644 and (f.PIPrograms & power(2, (e.ItemId-1)) = power(2, (e.ItemId-1)))
	where" + SqlAndLikeBegining("e.name", Value) +
")";
				}

				return null;
			}


			/// <summary>
			/// STUB for ap.dbstr
			/// </summary>
			/// <param name="value"></param>
			/// <returns></returns>
			private static string ap_dbstr(string value)
			{
				return value;
			}

			/// <summary>
			/// STUB for ap.dbid
			/// </summary>
			/// <param name="value"></param>
			/// <returns></returns>
			private static string ap_dbid(string value)
			{
				return value;
			}

			private static string DecorateTitle(string title)
			{
				return HtmlUtility.NonBreakingSpacer + title + HtmlUtility.NonBreakingSpacer;
			}
		}
		#endregion

		#region User
		public string oFindUser(params string[] attrs)
		{
			RegisterMsAjax("find_Usr_panel", "AjaxControlToolkit.DropDownBehavior, { \"dropDownControl\": getElement(\"find_Usr_panel\") }, null, null, getElement(\"find_Usr_caption\")");

			return new _FindUserMeta(_page).oInput(attrs);
		}

		public bool fmPressedFindUser(params string[] args)
		{
			return new _FindUserMeta(_page).fmPressed(args);
		}

		public string fmFindUser(params string[] args)
		{
			return new _FindUserMeta(_page).fmFind(args);
		}

		private class _FindUserMeta : FindMetaBase
		{
			public _FindUserMeta(PageCommon page)
				: base(
					"Usr", "FIND USER",
					new FindMenuMetaBase("nam", "BY NAME", 50, DecorateTitle(" by User Name ")),
					new FindMenuMetaBase("ssn", "BY SSN", 10, DecorateTitle(" by Last 4 SSN Digits ")),
					new FindMenuMetaBase("lgn", "BY LOGIN NAME", 20, DecorateTitle(" by Login Name ")),
					new FindMenuMetaBase("uid", "BY ID", 10, DecorateTitle(" by User Id "))
				)
			{
				Initialize(page);
			}

			public override string BuildSql()
			{
				throw new NotImplementedException();
			}


			private static string DecorateTitle(string title)
			{
				return HtmlUtility.NonBreakingSpacer + title + HtmlUtility.NonBreakingSpacer;
			}
		}
		#endregion

		#region Contract
		public string oFindContract(params string[] attrs)
		{
			RegisterMsAjax("find_Ctr_panel", "AjaxControlToolkit.DropDownBehavior, { \"dropDownControl\": getElement(\"find_Ctr_panel\") }, null, null, getElement(\"find_Ctr_caption\")");

			return new _FindContractMeta(_page).oInput(attrs);
		}

		public bool fmPressedFindContract(params string[] args)
		{
			return new _FindContractMeta(_page).fmPressed(args);
		}

		public string fmFindContract(params string[] args)
		{
			return new _FindContractMeta(_page).fmFind(args);
		}

		private class _FindContractMeta : FindMetaBase
		{
			public _FindContractMeta(PageCommon page)
				: base(
					"Ctr", "FIND CONTRACT",
				new FindMenuMetaBase("cust", "BY CUSTOMER", 200, DecorateTitle(" by Customer Name ")),
				new FindMenuMetaBase("part", "BY PARTNER", 200, DecorateTitle(" by Strategic Partner Name ")),
				new FindMenuMetaBase("prov", "BY PROVIDER", 200, DecorateTitle(" by Provider Name ")),
				new FindMenuMetaBase("acc", "BY FND. ID", 20, DecorateTitle(" by ID of Foundation ")),
				new FindMenuMetaBase("nom", "BY NO", 20, DecorateTitle(" by Contract No ")),
				new FindMenuMetaBase("tname", "BY TEMPLATE NAME", 20, DecorateTitle(" by Template Name ")),
				new FindMenuMetaBase("rm", "BY R.M.", 200, DecorateTitle(" by Relationship Manager ")),
				new FindMenuMetaBase("auth", "BY AUTHOR", 200, DecorateTitle(" by Author of Contract "))
				)
			{
				Initialize(page);
			}

			public override string BuildSql()
			{
				throw new NotImplementedException();
			}


			private static string DecorateTitle(string title)
			{
				return HtmlUtility.NonBreakingSpacer + title + HtmlUtility.NonBreakingSpacer;
			}
		}
		#endregion

		#region Company
		public string oFindCompany(params string[] attrs)
		{
			RegisterMsAjax("find_Cmp_panel", "AjaxControlToolkit.DropDownBehavior, { \"dropDownControl\": getElement(\"find_Cmp_panel\") }, null, null, getElement(\"find_Cmp_caption\")");

			return new _FindCompanyMeta(_page).oInput(attrs);
		}

		public bool fmPressedFindCompany(params string[] args)
		{
			return new _FindCompanyMeta(_page).fmPressed(args);
		}

		public string fmFindCompany(params string[] args)
		{
			return new _FindCompanyMeta(_page).fmFind(args);
		}

		private class _FindCompanyMeta : FindMetaBase
		{
			public _FindCompanyMeta(PageCommon page)
				: base(
					"Cmp", "FIND COMPANY",
				new FindMenuMetaBase("nam", "BY NAME", 200, DecorateTitle(" by Company Name ")),
				new FindMenuMetaBase("ofc", "BY OFFICER", 200, DecorateTitle(" by Officer Name "))
				)
			{
				Initialize(page);
			}

			public override string BuildSql()
			{
				throw new NotImplementedException();
			}


			private static string DecorateTitle(string title)
			{
				return HtmlUtility.NonBreakingSpacer + title + HtmlUtility.NonBreakingSpacer;
			}
		}
		#endregion

		#region Application
		public string oFindApplication(params string[] attrs)
		{
			RegisterMsAjax("find_app_panel", "AjaxControlToolkit.DropDownBehavior, { \"dropDownControl\": getElement(\"find_app_panel\") }, null, null, getElement(\"find_app_caption\")");

			return new _FindApplication(_page).oInput(attrs);
		}

		public bool fmPressedFindApplication(params string[] args)
		{
			return new _FindApplication(_page).fmPressed(args);
		}

		public string fmFindApplication(params string[] args)
		{
			return new _FindApplication(_page).fmFind(args);
		}

		private class _FindApplication : FindMetaBase
		{
			public _FindApplication(PageCommon page)
				: base(
					"app", "FIND APPLICATION",
				new FindMenuMetaBase("nam", "BY NAME", 200, DecorateTitle(" by Application Name ")),
				new FindMenuMetaBase("fid", "BY ID", 20, DecorateTitle(" by FS Account # ")),
				new FindMenuMetaBase("par", "BY SERVICE PROVIDER", 200, DecorateTitle(" by Service Provider "))
				)
			{
				Initialize(page);
			}

			public override string BuildSql()
			{
				throw new NotImplementedException();
			}


			private static string DecorateTitle(string title)
			{
				return HtmlUtility.NonBreakingSpacer + title + HtmlUtility.NonBreakingSpacer;
			}
		}
		#endregion

		#region Company Element
		public string oFindCompanyElement(params string[] attrs)
		{
			RegisterMsAjax("find_ce_panel", "AjaxControlToolkit.DropDownBehavior, { \"dropDownControl\": getElement(\"find_ce_panel\") }, null, null, getElement(\"find_ce_caption\")");

			return new _FindCompanyElementMeta(_page).oInput(attrs);
		}

		public bool fmPressedFindCompanyElement(params string[] args)
		{
			return new _FindCompanyElementMeta(_page).fmPressed(args);
		}

		public string fmFindCompanyElement(params string[] args)
		{
			return new _FindCompanyElementMeta(_page).fmFind(args);
		}

		// was "getFindCompanyElement_Token"
		public string GetTokenFindCompanyElement()
		{
			var o = new _FindCompanyElementMeta(_page);
			o.fmFind();			// @imported: try to read stored values
			return o.Token;
		}

		private class _FindCompanyElementMeta : FindMetaBase
		{
			public _FindCompanyElementMeta(PageCommon page)
				: base(
					"ce", "FIND MEMBER",
				new FindMenuMetaBase("prov", "BY SERVICE PROVIDER", 200, DecorateTitle(" by Service Provider ")),
				new FindMenuMetaBase("nam", "BY PERSON NAME", 200, DecorateTitle(" by Person Name ")),
				new FindMenuMetaBase("lgn", "BY LOGIN NAME", 200, DecorateTitle(" by Login Name ")),
				new FindMenuMetaBase("uid", "BY USER ID", 20, DecorateTitle(" by User Id "))
				)
			{
				Initialize(page);
			}

			public override string BuildSql()
			{
				throw new NotImplementedException();
			}


			private static string DecorateTitle(string title)
			{
				return HtmlUtility.NonBreakingSpacer + title + HtmlUtility.NonBreakingSpacer;
			}
		}
		#endregion

		#region Charity Request
		public string oFindCHRequest(params string[] attrs)
		{
			RegisterMsAjax("find_CHRequest_panel", "AjaxControlToolkit.DropDownBehavior, { \"dropDownControl\": getElement(\"find_CHRequest_panel\") }, null, null, getElement(\"find_CHRequest_caption\")");

			return new _FindCharityRequest(_page).oInput(attrs);
		}

		public bool fmPressedFindCHRequest(params string[] args)
		{
			return new _FindCharityRequest(_page).fmPressed(args);
		}

		public string fmFindCHRequest(params string[] args)
		{
			return new _FindCharityRequest(_page).fmFind(args);
		}

		private class _FindCharityRequest : FindMetaBase
		{
			public _FindCharityRequest(PageCommon page)
				: base(
					"CHRequest", "FIND REQUEST",
				new FindMenuMetaBase("fname", "BY FndName", 200, DecorateTitle(" by Foundation Name ")),
				new FindMenuMetaBase("PCAname", "BY PCA Name", 200, DecorateTitle(" by PCA Name ")),
				new FindMenuMetaBase("reqid", "BY Request ID", 20, DecorateTitle(" by Charity Request ID ")),
				new FindMenuMetaBase("chname", "BY Charity Name", 200, DecorateTitle(" by Charity Name ")),
				new FindMenuMetaBase("author", "BY Author", 100, DecorateTitle(" Requested By "))
				)
			{
				Initialize(page);
			}

			public override string BuildSql()
			{
				throw new NotImplementedException();
			}


			private static string DecorateTitle(string title)
			{
				return HtmlUtility.NonBreakingSpacer + title + HtmlUtility.NonBreakingSpacer;
			}
		}
		#endregion

		#region Grant
		public string oFindGrant(params string[] attrs)
		{
			RegisterMsAjax("find_grant_panel", "AjaxControlToolkit.DropDownBehavior, { \"dropDownControl\": getElement(\"find_grant_panel\") }, null, null, getElement(\"find_grant_caption\")");

			return new _FindGrant(_page).oInput(attrs);
		}

		public bool fmPressedFindGrant(params string[] args)
		{
			return new _FindGrant(_page).fmPressed(args);
		}

		public string fmFindGrant(params string[] args)
		{
			return new _FindGrant(_page).fmFind(args);
		}

		private class _FindGrant : FindMetaBase
		{
			public _FindGrant(PageCommon page)
				: base(
					"Grant", "FIND GRANTS",
				new FindMenuMetaBase("recipient", "BY RECIPIENT", 200, DecorateTitle(" by Recipient ")),
				new FindMenuMetaBase("histRecipient", "BY RECIPIENT", 200, DecorateTitle(" by Recipient ")),
				new FindMenuMetaBase("grantor", "BY GRANTOR", 200, DecorateTitle(" by Grantor ")),
				new FindMenuMetaBase("histGrantor", "BY GRANTOR", 200, DecorateTitle(" by Grantor ")),
				new FindMenuMetaBase("project", "BY REPORT CODE", 200, DecorateTitle(" by Report Code "))
				)
			{
				Initialize(page);
			}

			public override string BuildSql()
			{
				throw new NotImplementedException();
			}


			private static string DecorateTitle(string title)
			{
				return HtmlUtility.NonBreakingSpacer + title + HtmlUtility.NonBreakingSpacer;
			}
		}
		#endregion

		public void Init()
		{
			_page.ClientScript.RequireMsAjax(true);
		}

		#region Implementation
		private void RegisterMsAjax(string id, params string[] startups)
		{
			_page.ClientScript.RegisterStartup("MsAjax", "Sys.Application.initialize();");

			if (startups != null && startups.Length > 0)
			{
				StringBuilder strBuilder = new StringBuilder()
					.AppendLine("Sys.Application.add_init(function() {");
				for (int i = 0; i < startups.Length; i++)
				{
					strBuilder
						.AppendFormat("$create({0});", startups[i])
						.AppendLine();
				}
				strBuilder.AppendLine("});");
				_page.ClientScript.RegisterStartup(id, strBuilder.ToString());
			}
		}

		#region Nested Types
		abstract class FindMetaBase
		{
			private PageCommon _page;
			private FindMenuMetaBase[] _menu;

			public FindMetaBase(string id, string title, params FindMenuMetaBase[] menu)
			{
				if (menu == null)
					throw new ArgumentNullException("menu");
				if (menu.Length == 0)
					throw new ArgumentException("", "menu");

				Id = id;
				Title = title;

				_menu = menu;
			}

			public void Initialize(PageCommon page)
			{
				if (page == null)
					throw new ArgumentNullException("page");

				_page = page;
			}

			public string Id { get; set; }

			public string Title { get; set; }

			public string Token { get; set; }

			public string Value { get; set; }

			public string Sql { get; set; }

			public FindMenuMetaBase[] Menu
			{
				get { return _menu; }
			}

			public virtual string oInput(params string[] attrs)
			{
				RegisterClientScript();

				string fullId = "find_" + Id;
				string title = Title;
				string toHide = "";
				string toShow = "";
				bool showAll = true;
				string defToken = null;
				bool isStatical = false;
				int fieldWidth = 100;
				bool showButtons = true;

				string cssClassPrefix = "FindMenu";

				if (attrs != null && attrs.Length > 0)
				{
					for (int i = 0; i < attrs.Length; i++)
					{
						Tuple<string, string> attrInfo = HtmlUtility.ParseAttribute(attrs[i]);
						if (attrInfo != null)
						{
							switch (attrInfo.Item1.ToUpperInvariant())
							{
								case "TITLE":
									title = attrInfo.Item2;
									break;
								case "HIDE":
									toHide = attrInfo.Item2;
									break;
								case "SHOW":
									toShow = attrInfo.Item2;
									break;
								case "SHOWALL":
									showAll = IsAttributeValueTrue(attrInfo.Item2);
									break;
								case "STATICAL":
									isStatical = IsAttributeValueTrue(attrInfo.Item2);
									break;
								case "DEFTOKEN":
									defToken = attrInfo.Item2;
									break;
								case "STYLE":
									cssClassPrefix = attrInfo.Item2;
									break;
								case "WIDTH":
									fieldWidth = Int32.Parse(attrInfo.Item2);
									break;
								case "SHOWBUTTONS":
									showButtons = IsAttributeValueTrue(attrInfo.Item2);
									break;
							}
						}
						else
						{
							if (attrs[i].Equals("showall", StringComparison.OrdinalIgnoreCase))
								showAll = true;
							else if (attrs[i].Equals("statical", StringComparison.OrdinalIgnoreCase))
								isStatical = true;
							else if (attrs[i].Equals("showButtons", StringComparison.OrdinalIgnoreCase))
								showButtons = true;
						}
					}
				}

				toHide = DecorateTokens(toHide, "pcd");		// @imported: portfolio code is hidden by default
				toShow = DecorateTokens(toShow);

				fmFind();
				string val = "";

				if (String.IsNullOrEmpty(Value))
					showAll = true;
				else
					val = HtmlUtility.HtmlEncode(Value);

				FindMenuMetaBase[] menuVisible = Menu.Where(item => CheckIfMenuItemVisible(item.Token, toShow, toHide)).ToArray();
				string sessionKey = "FindObject__" + Id + "__LastToken__";
				string tok = Token ?? defToken;
				if (tok == null)
				{
					string lastTok = _page.Session[sessionKey] as string;
					if (lastTok != null)
					{
						for (int i = 0; i < menuVisible.Length; i++)
						{
							if (lastTok.Equals(menuVisible[i].Token, StringComparison.OrdinalIgnoreCase))
							{
								tok = lastTok;
								break;
							}
						}
					}
				}

				string cap = null;
				if (tok != null)
				{
					FindMenuMetaBase foundMenu = Array.Find(Menu, item => tok.Equals(item.Token, StringComparison.OrdinalIgnoreCase));
					if (foundMenu != null)
					{
						cap = foundMenu.Caption;
						_page.Session[sessionKey] = tok;
					}
					else
					{
						tok = null;
					}
				}

				if (tok == null)
				{
					FindMenuMetaBase firstMenuItemVisible = GetFirstMenuItemVisible(Menu, toShow, toHide);
					tok = firstMenuItemVisible.Token;
					cap = firstMenuItemVisible.Caption;
				}

				tok = HtmlUtility.HtmlEncode(tok);
				cap = HtmlUtility.HtmlEncode(cap);

				string[] s = new[] { "", "", "", "" };

				string mTitle = "";
				int mLength = 0;
				if (!isStatical)
				{
					bool f = false;
					for (int i = 0; i < menuVisible.Length; i++)
					{
						FindMenuMetaBase menuItem = menuVisible[i];
						if (f)
						{
							s[0] += "\",\"" + menuItem.Title;
							s[1] += "\",\"" + menuItem.Caption;
							s[2] += "," + menuItem.Size.ToString();
							s[3] += "\",\"" + menuItem.Token;
						}
						else
						{
							s[0] += menuItem.Title;
							s[1] += menuItem.Caption;
							s[2] += menuItem.Size.ToString();
							s[3] += menuItem.Token;
							f = true;
						}

						if (menuItem.Caption.Length > mLength)
						{
							mLength = menuItem.Caption.Length;
							mTitle = menuItem.Caption;
						}
					}
				}
				else
				{
					mLength = fieldWidth;
				}

				using (HtmlWriter writer = new HtmlWriter())
				{
					_page.ClientScript.RegisterBlock("$find_" + fullId, String.Format("var o_{0} = new FindObject({1}, {2}, [ {3} ], [ {4} ], [ {5} ], [ {6} ]);", fullId, HtmlUtility.JValue(Id), HtmlUtility.JValue(title),
						HtmlUtility.JValue(s[0]), HtmlUtility.JValue(s[1]), HtmlUtility.JValue(s[2]), HtmlUtility.JValue(s[3])));
					if (!isStatical)
					{
						_page.ClientScript.RegisterBlock("resizeSearch_" + fullId, String.Format(
@"function resizeSearch_{0}()
{{
	var width = getElement(""{0}_hiden_max"").offsetWidth + 20;
	$(""#{0}_arrow"")
		.css(""width"", width)
		.css(""backgroundPosition"", (width - 8) + ""px 50%"");
	$(""#{0}_caption"").css(""width"", width + ""px"");
}}", fullId));
						_page.ClientScript.RegisterStartup("resizeSearch_" + fullId, String.Format("resizeSearch_{0}();", fullId));
					}

					_page.ClientScript.RegisterBlock("FindMenuClick_" + fullId, String.Format(
@"function FindMenuClick_{0} (item)
{{
	var $item = $(item);
	$(""#{0}_caption"").text($item.attr(""cap""));
	$(""#{0}_arrow"").css(""backgroundImage"", ""url({1})"");
	$(""#{0}_token"").val($item.attr(""token""));
	$(""#{0}"").focus();
}}", fullId, _page.GetImageUrl("ajax", "find_arrow-right.gif")));

					if (!isStatical)
					{
						writer
							.Attr("id", fullId + "_hiden_max")
							.Attr("class", "SmallBoldDarkFont")
							.AttrStyle("visibility", "hidden")
							.AttrStyle("position", "absolute")
							.Tag("div")
								.Write(mTitle)
							.End();
					}

					writer
						.Hidden(fullId + "_token", tok, id: fullId + "_token");

					writer
						.Attr("cellspacing", "0").Attr("cellpadding", "0")
						.Attr("border", "0")
						.Tag("table")
							.Tag("tr")
								.Attr("class", "SmallBoldDarkFont")
								.Attr("nowrap")
								.Tag("td")
									.Write(title)
								.End()
								.Attr("class", "SmallBoldDarkFont")
								.Attr("nowrap")
								.Attr("unselectable", "on")
								.Attr("align", "left")
								.AttrStyle("padding-left", "0")
								.Tag("td");

					writer
									.Attr("id", fullId + "_arrow")
									.Attr("class", cssClassPrefix + "-UnderLine");
					if (!isStatical)
					{
						writer
									.AttrStyle("width", mLength + "px")
									.AttrStyle("background-position", (mLength - 11).ToString() + "px 50%");
					}
					writer
									.Tag("div")
										.Attr("id", fullId + "_caption")
										.Attr("class", cssClassPrefix + "-Caption")
										.Attr("unselectable", "on");
					if (!isStatical)
					{
						writer
										.AttrStyle("width", (mLength - 3).ToString() + "px");
					}
					writer
										.Tag("div")
											.Write(cap)
										.End()
										.Attr("id", fullId + "_panel")
										.Attr("class", cssClassPrefix + "-Panel")
										.Tag("div");
					for (int i = 0; i < menuVisible.Length; i++)
					{
						FindMenuMetaBase menuItem = menuVisible[i];
						writer
											.Attr("onmouseup", "FindMenuClick_" + fullId + "(this)")
											.Attr("onmouseover", String.Format("this.className = \"{0}-HoverItem\"", cssClassPrefix))
											.Attr("onmouseout", String.Format("this.className = \"{0}-Item\"", cssClassPrefix))
											.Attr("class", cssClassPrefix + "-Item")
											.Attr("id", "Option")
											.Attr("token", menuItem.Token)
											.Attr("cap", menuItem.Caption)
											.Tag("div")
												.Write(menuItem.Title)
											.End();
					}

					writer
										.End("div")
									.End("div")
								.End("td")
								.Tag("td")
									.Img(_page.GetImageUrl("spacer.gif"), width: 10, height: 10)
									.Attr("id", fullId)
									.Attr("name", fullId)
									.Attr("class", "input-text")
									.Attr("size", "40")
									.Attr("value", val)
									.Attr("onkeypress", String.Format("checkEnter(\"{0}_btn\");", fullId))
									.TagSingle("input")
								.End();

					if (showButtons)
					{
						writer
								.Tag("td")
									.Img(_page.GetImageUrl("spacer.gif"), width: 5, height: 5)
									.Submit(fullId + "_btn", "Find", id: fullId + "_btn", cssClass: "input-button")
									.Img(_page.GetImageUrl("spacer.gif"), width: 5, height: 5)
									.Submit(fullId + "_btn_all", "Show All", id: fullId + "_btn_all", cssClass: "input-button")
								.End();
					}

					writer
							.End("tr")
						.End("table");

					return writer.GetContent();
				}
			}

			private static bool CheckIfMenuItemVisible(string token, string toShow, string toHide)
			{
				if (token == null)
					throw new ArgumentNullException("token");
				if (token == "")
					throw new ArgumentException("token");

				token = TokensDivider + token + TokensDivider;
				return toShow.Contains(token) || !toHide.Contains(token);
			}

			private static FindMenuMetaBase GetFirstMenuItemVisible(FindMenuMetaBase[] menu, string toShow, string toHide)
			{
				for (int i = 0; i < menu.Length; i++)
				{
					if (CheckIfMenuItemVisible(menu[i].Token, toShow, toHide))
						return menu[i];
				}
				return null;
			}

			private const char TokensDivider = ' ';
			private const string UserTokensDivider = ",;";
			private static string DecorateTokens(string rawTokens, params string[] defaultTokens)
			{
				string result = rawTokens;
				for (int i = 0; i < UserTokensDivider.Length; i++)
				{
					result = result.Replace(UserTokensDivider[i], TokensDivider);
				}
				result = result.Trim(new[] { TokensDivider });

				if (defaultTokens != null && defaultTokens.Length > 0)
				{
					for (int i = 0; i < defaultTokens.Length; i++)
					{
						if (result == "")
							result = defaultTokens[i];
						else
							result += TokensDivider + defaultTokens[i];
					}
				}

				result = TokensDivider + result + TokensDivider;

				return result;
			}

			private bool _fmFindGot;
			public virtual string fmFind(params string[] arguments)
			{
				if (Sql != null)
					return Sql;
				Value = null;
				Token = null;

				if (_fmFindGot)
					return null;

				string fullId = "find_" + Id;
				Token = _page.FormIn.GetString(fullId + "_token", null);
				if (Token == null)
					return null;
				if (fmPressed(fullId + "_btn_all"))
				{
					Sql = "";
					_fmFindGot = true;
					return "";
				}

				System.Collections.Specialized.NameValueCollection form = _page.Request.Form;
				for (int i = 0; i < form.Keys.Count; i++)
				{
					string value = form[form.Keys[i]];
					if (_r.IsMatch(value))
						return null;
				}

				if (!fmPressed(fullId + "_btn"))
				{
					for (int i = 0; i < form.Keys.Count; i++)
					{
						string value = form[form.Keys[i]];
						if (_r.IsMatch(value))
							return null;
					}
				}

				Value = _page.FormIn.GetString(fullId, null);
				if (Value == null)
					return null;

				Sql = BuildSql();
				if (Sql == null)
					; // TODO: ap.LogError(1001, "find_asp.fmFind(" + this.id + ")", "Undefiled token (token=" + ap_dbstr(this.token) + ", value=" + ap_dbstr(Value) + ")");

				_fmFindGot = true;
				return Sql;
			}
			private static readonly Regex _r = new Regex("find_.*?_btn=", RegexOptions.Compiled | RegexOptions.IgnoreCase);

			public virtual bool fmPressed(params string[] args)
			{
				bool all = false;
				if (args != null || args.Length > 0)
				{
					for (int i = 0; i < args.Length; i++)
					{
						if (args[i].Equals("showall", StringComparison.OrdinalIgnoreCase))
						{
							all = true;
							break;
						}
					}
				}

				return false;
				// Stack overflow: fmFind => fmPressed => fmFind ...
				if (fmFind() != null)
					return all ? String.IsNullOrEmpty(Sql) : !String.IsNullOrEmpty(Sql);
				return false;
			}

			public virtual string BuildSql()
			{
				return null;
			}


			protected string SqlAndLikeBegining(string source, string test)
			{
				throw new NotImplementedException();
			}


			#region Implementation
			private bool IsAttributeValueTrue(string value)
			{
				return value.Equals("true", StringComparison.OrdinalIgnoreCase) || value.Equals("1", StringComparison.OrdinalIgnoreCase);
			}

			private void RegisterClientScript()
			{
				_page.ClientScript.RegisterBlock("Find__behaviour",
	@"
function FindObject(id, title, menu, caption, size, token) {
	this.id = id;
	this.title = title;
	this.menu = menu;
	this.caption = caption;
	this.size = size;
	this.token = token;
}

function checkEnter(btn)
{
	if (window.event.keyCode === 13) {
		getElement(btn).click();
		return false;
	}
	return true;
}
");
			}
			#endregion
		}

		class FindMenuMetaBase
		{
			public FindMenuMetaBase(string token, string caption, int width, string text)
			{
				Token = token;
				Caption = caption;
				Size = width;
				Title = text;
			}

			public string Title;

			public string Caption;

			public int Size;

			public string Token;
		}
		#endregion
		#endregion
	}
}
