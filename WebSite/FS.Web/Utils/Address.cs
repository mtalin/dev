﻿using FS.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lexxys;

namespace FS.Web.Utils
{
	public sealed class Address
	{
		private readonly PageCommon _page;

		public Address(PageCommon page)
		{
			if (page == null)
				throw EX.ArgumentNull("page");

			_page = page;
		}

		public string PrintAddress(string addr1, string addr2, string city, string state, string zip, string country, bool lineByLine)
		{
			bool addr1_is = !String.IsNullOrEmpty(addr1);
			bool addr2_is = !String.IsNullOrEmpty(addr2);
			bool city_is = !String.IsNullOrEmpty(city);
			bool state_is = !String.IsNullOrEmpty(state);
			bool zip_is = !String.IsNullOrEmpty(zip);
			bool country_is = !String.IsNullOrEmpty(country);
			if (!(addr1_is || addr2_is || city_is || state_is || zip_is || country_is))
				return "";

			string br = (lineByLine) ? HtmlUtility.Br : " ";
			StringBuilder writer = new StringBuilder();
			if (addr1_is)
			{
				writer
					.Append(Common.oStr(addr1))
					.Append(br);
			}
			if (addr2_is)
			{
				writer
					.Append(Common.oStr(addr2))
					.Append(br);
			}
			if (city_is)
			{
				writer
					.Append(Common.oStr(city))
					.Append(", ");
			}
			if (state_is)
			{
				writer
					.Append(Common.oStr(state))
					.Append(" ");
			}
			;
			if (zip_is)
			{
				zip = zip.Trim();
				writer.Append(_page.Output.oZip(zip));
			}
			if (country_is && country != "US")
			{
				if (writer.ToString().Trim().EndsWith(",", StringComparison.OrdinalIgnoreCase))
					writer.Append(_page.Output.oCountry(country));
				else
				{
					if (writer.Length > 0 && country_is)
						writer.Append(", ");
					writer.Append(_page.Output.oCountry(country));
				}
			}
			return writer.ToString();
		}

		public string PrintAddressObj(object address, bool lineByLine)
		{
			throw new NotImplementedException();
		}
	}
}
