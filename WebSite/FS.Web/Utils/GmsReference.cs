﻿using Lexxys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.SessionState;

namespace FS.Web.Utils
{
	public sealed class GmsReference
	{
		private readonly PageCommon _page;

		public GmsReference(PageCommon page)
		{
			if (page == null)
				throw EX.ArgumentNull("page");

			_page = page;
		}

		public void Initialize()
		{
			string sessionId = _page.SessionBo.GetSessionID();
			string rootUrl;
			HttpSessionState session = _page.Session;
#if GMS_1_0_Enabled
			// @imported: GMS Version 1.0
			if (session[SesionKeysLegacy.Reference] == null)
			{
				rootUrl = "www.foundationsource.com"; //Config.GetValue<string>("gms/reference/@root");
				if (!String.IsNullOrEmpty(rootUrl))
				{
					if (!UrlUtility.IsHttpScheme(rootUrl))
						throw new Exception("Invalid uri scheme. Http or https are required");
					if (!UrlUtility.IsAbsolute(rootUrl))
						rootUrl = UrlUtility.ChangeScheme(rootUrl, (__page.SecureConfig.IsPageSecured) ? UrlUtility.SchemeHttps : UrlUtility.SchemeHttp);

					session[SesionKeysLegacy.Reference] = rootUrl;
					session[SesionKeysLegacy.Session] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/reference/@session", "fsol.do?session=$").Replace("$", sessionId));
					session[SesionKeysLegacy.KeepAlive] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/reference/@keepalive", "keepalive.do"));
					session[SesionKeysLegacy.PendingGrants] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/reference/@pending_grants", "fsol/grants/requests.do"));
					session[SesionKeysLegacy.LetterTemplate] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/reference/@letter_template", "fsol/admin/moap.do"));
					session[SesionKeysLegacy.OnlineApp] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/reference/@online_app", "fsol/admin/mct.do"));
					session[SesionKeysLegacy.Reports] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/reference/@reports", "fsol/reports/reports.do?category=$"));
					session[SesionKeysLegacy.DeclinedApp] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/reference/@declined_app", "fsol/grants/history/declined.do"));
					session[SesionKeysLegacy.ViewRequest] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/reference/@viewrequest", "fsol/grants/history/viewrequest.do"));

					session[SesionKeysLegacy.Spacer] = session[SesionKeysLegacy.Session];
				}
			}
			else
			{
				session.Remove(SesionKeysLegacy.Reference);
				session.Remove(SesionKeysLegacy.Session);
				session.Remove(SesionKeysLegacy.KeepAlive);
				session.Remove(SesionKeysLegacy.PendingGrants);
				session.Remove(SesionKeysLegacy.LetterTemplate);
				session.Remove(SesionKeysLegacy.OnlineApp);
				session.Remove(SesionKeysLegacy.Reports);
				session.Remove(SesionKeysLegacy.DeclinedApp);
				session.Remove(SesionKeysLegacy.ViewRequest);
			}

			//@imported: GMS Version 10.0
#endif

			if (session[SesionKeys.Reference] == null)
			{
				//rootUrl = Config.GetValue<string>("gms/A/reference/@root");
				rootUrl = "http://foundationsource.com";
				if (!String.IsNullOrEmpty(rootUrl))
				{
					if (!UrlUtility.IsHttpScheme(rootUrl))
						throw new Exception("Invalid uri scheme. Http or https are required");
					if (!UrlUtility.IsAbsolute(rootUrl))
						rootUrl = UrlUtility.ChangeScheme(rootUrl, (_page.SecureConfig.PageSecured) ? UrlUtility.SchemeHttps : UrlUtility.SchemeHttp);

					session[SesionKeys.Reference] = rootUrl;
					session[SesionKeys.Session] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@session", "fsbo/keepalive.do?session={session}&seed={seed}").Replace("{session}", sessionId));
					session[SesionKeys.DoLogin] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@doLogin", "fsbo/loginprocess.do?ss={session}").Replace("{session}", sessionId));
					session[SesionKeys.KeepAlive] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@keepalive", "fsbo/keepalive.do?seed={seed}"));
					session[SesionKeys.Home] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@home", "fsbo/home.do"));
					session[SesionKeys.Alerts] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@alerts", "fsbo/home.do"));
					session[SesionKeys.RequestsSiteAdministration] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@requestsAdm", "fsbo/admin/mf/sites.do?fID={foundation}"));
					session[SesionKeys.ProgramAreaAdministration] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@programAreaAdm", "fsbo/admin/mf/programs.do?fID={foundation}"));
					session[SesionKeys.OrganizationRightsLimits] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@organizationAdm", "fsbo/admin/mf/org.do?fID={foundation}"));
					session[SesionKeys.CommitteeAdministration] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@commiteeAdm", "fsbo/admin/mf/committees.do?fID={foundation}"));
					session[SesionKeys.TransactOnBehalfOf] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@transactOnBehalfOf", "fsbo/admin/mu/processobo.do?x={user}&f={foundation}&ss={session}"));
					session[SesionKeys.UsersLimits] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@userLimits", "fsbo/admin/mf/userslimits.do?fID={foundation}"));

					session[SesionKeys.ModifyGrant] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@modifyGrant", "fsbo/admin/mf/grants/editgrant.do?id={grant}"));
					session[SesionKeys.PreviewGrantLetter] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@previewGrantLetter", "fsbo/admin/mf/grants/previewgrantletter.do?id={grant}"));
					session[SesionKeys.EditGrantLetter] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@editGrantLetter", "fsbo/admin/mf/grants/processgrantletter.do?id={grant}"));
					session[SesionKeys.GrantRequestSearch] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@grantRequestSearch", "fsbo/admin/mf/requests/grantreqsearch.do?fID={foundation}"));
					session[SesionKeys.DocumentRepository] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@documentRepository", "fsbo/admin/mf/documents.do?fID={foundation}"));

					session[SesionKeys.ProcessCorrespondence] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@processCorrespondence", "fsbo/corr/pending.do?fID={foundation}"));
					session[SesionKeys.ClientSatisfaction] = UrlUtility.AddSegment(rootUrl, Config.GetValue<string>("gms/A/reference/@clientSatisfaction", "fsbo/satisfaction.do"));

					session[SesionKeys.Spacer] = session[SesionKeys.Session];
				}
				else
				{
					session.Remove(SesionKeys.Reference);
					session.Remove(SesionKeys.Session);
					session.Remove(SesionKeys.DoLogin);
					session.Remove(SesionKeys.KeepAlive);
					session.Remove(SesionKeys.Home);
					session.Remove(SesionKeys.Alerts);
					session.Remove(SesionKeys.RequestsSiteAdministration);
					session.Remove(SesionKeys.ProgramAreaAdministration);
					session.Remove(SesionKeys.OrganizationRightsLimits);
					session.Remove(SesionKeys.CommitteeAdministration);
					session.Remove(SesionKeys.TransactOnBehalfOf);
					session.Remove(SesionKeys.UsersLimits);

					session.Remove(SesionKeys.ModifyGrant);
					session.Remove(SesionKeys.PreviewGrantLetter);
					session.Remove(SesionKeys.EditGrantLetter);
					session.Remove(SesionKeys.GrantRequestSearch);
					session.Remove(SesionKeys.DocumentRepository);

					session.Remove(SesionKeys.ProcessCorrespondence);
					session.Remove(SesionKeys.ClientSatisfaction);

					session.Remove(SesionKeys.Spacer);
				}
			}
		}


		#region Nested Types
		public static class SesionKeys
		{
			public const string Prefix = "GmsA.";

			public const string Reference = Prefix + "Reference";
			public const string Session = Prefix + Prefix + "Session";
			public const string DoLogin = Prefix + Prefix + "DoLogin";
			public const string KeepAlive = Prefix + "KeepAlive";
			public const string Home = Prefix + "Home";
			public const string Alerts = Prefix + "Alerts";
			public const string RequestsSiteAdministration = Prefix + "RequestsSiteAdministration";
			public const string ProgramAreaAdministration = Prefix + "ProgramAreaAdministration";
			public const string OrganizationRightsLimits = Prefix + "OrganizationRightsLimits";
			public const string CommitteeAdministration = Prefix + "CommitteeAdministration";
			public const string TransactOnBehalfOf = Prefix + "TransactOnBehalfOf";
			public const string UsersLimits = Prefix + "UsersLimits";
			public const string ModifyGrant = Prefix + "ModifyGrant";
			public const string PreviewGrantLetter = Prefix + "PreviewGrantLetter";
			public const string EditGrantLetter = Prefix + "EditGrantLetter";
			public const string GrantRequestSearch = Prefix + "GrantRequestSearch";
			public const string DocumentRepository = Prefix + "DocumentRepository";
			public const string ProcessCorrespondence = Prefix + "ProcessCorrespondence";
			public const string ClientSatisfaction = Prefix + "ClientSatisfaction";
			public const string Spacer = Prefix + "Spacer";

			public static string GenerateKey(string name)
			{
				if (name == null)
					throw new ArgumentNullException("name");
				return Prefix + name;
			}
		}

		public static class SesionKeysLegacy
		{
			public const string Prefix = "Gms";

			public const string Reference = Prefix + "Reference";
			public const string Session = Prefix + "Session";
			public const string KeepAlive = Prefix + "KeepAlive";
			public const string PendingGrants = Prefix + "PendingGrants";
			public const string LetterTemplate = Prefix + "LetterTemplate";
			public const string OnlineApp = Prefix + "OnlineApp";
			public const string Reports = Prefix + "Reports";
			public const string DeclinedApp = Prefix + "DeclinedApp";
			public const string ViewRequest = Prefix + "ViewRequest";
			public const string Spacer = Prefix + "Spacer";

			public static string GenerateKey(string name)
			{
				if (name == null)
					throw new ArgumentNullException("name");
				return Prefix + name;
			}
		}
		#endregion
	}
}
