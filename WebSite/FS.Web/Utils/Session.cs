﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lexxys;

namespace FS.Web.Utils
{
	public sealed class Session_
	{
		[Include("~/utils/gms-reference", 0)]
		public readonly GmsReference GmsReference;

		private readonly PageCommon _page;

		public Session_(PageCommon page)
		{
			if (page == null)
				throw EX.ArgumentNull("page");

			_page = page;
			GmsReference = new GmsReference(_page);
		}

		public void KeepSession()
		{
			GmsReference.Initialize();
			//TODO: ap.SyncSession;
		}
	}
}
