﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lexxys;

namespace FS.Web.Utils
{
	public sealed class Windows
	{
		private readonly PageCommon _page;

		public Windows(PageCommon page)
		{
			if (page == null)
				throw EX.ArgumentNull("page");

			_page = page;
		}

		public string oMakeNote(string element, string text, params string[] args)
		{
			string cssClass = _page.Panel.CurrentPanel != null ? _page.Panel.CurrentPanel.CssClass : "";
			string url = UrlUtility.MakeJScriptUrl("makeNote(\"" + element + "\"" + makeWindowsParams(args) + ");");
			using (HtmlWriter writer = new HtmlWriter())
			{
				writer
					.Attr("border", "0")
					.Attr("cellspacing", "0").Attr("cellpadding", "0")
					.AttrStyle("width", "100")
					.AttrStyle("height", "50")
					.AttrStyle("margin", "10")
					.Tag("table")
						.Tag("tr")
							.Tag("td")
								.Write(_page.Common.oHref(url, _page.FormOut.Image("note")))
							.End()
							.Attr(cssClass)
							.Tag("td")
								.Write(_page.Common.oHref(url, text))
							.End()
						.End()
					.End();

				return writer.GetContent();
			}
		}

		public string oMakeEmail(string element, params string[] args)
		{
			string url = UrlUtility.MakeJScriptUrl("makeEmail(\"" + element + "\"" + makeWindowsParams(args) + ");");
			string text = "";
			if (args != null && args.Length > 0)
			{
				for (int i = 0; i < args.Length; i++)
				{
					if (args[i].StartsWith("title="))
					{
						text = args[i].Substring(6);
						break;
					}
				}
			}

			if (text == "")
			{
				text = "Error";
				for (int i = 0; i < args.Length; i++)
				{
					if (args[i].StartsWith("to="))
					{
						text = args[i].Substring(3);
						break;
					}
				}
			}
			return _page.Common.oHref(url, text);
		}

		public string oViewNoteScript(string id)
		{
			if (id == null)
				throw new ArgumentNullException("id");

			_page.Href.AddLegalTarget("/admin/email/viewnote.asp?id=" + id);	// TODO: url
			return "viewNote(" + id + ")";
		}

		public string oViewNote(string id, params string[] args)
		{
			if (id == null)
				throw new ArgumentNullException("id");

			string url = _page.Common.oHref(UrlUtility.MakeJScriptUrl(oViewNoteScript(id)), _page.FormOut.Image("note"));
			if (args != null || args.Length > 0)
			{
				StringBuilder strBuilder = new StringBuilder(url);
				for (int i = 0; i < args.Length; i++)
				{
					strBuilder
						.Append(HtmlUtility.NonBreakingSpacer)
						.Append(args[i]);
				}
				return strBuilder.ToString();
			}
			return url;
		}

		public string oViewEmailScript(string id)
		{
			if (id == null)
				throw new ArgumentNullException("id");

			_page.Href.AddLegalTarget("/admin/email/viewemail.asp?id=" + id);	// TODO: url
			return "viewEmail(" + id + ")";
		}

		public string oEmailScript(string id, string b)
		{
			if (id == null)
				throw new ArgumentNullException("id");

			_page.Href.AddLegalTarget("/admin/email/email2.asp?id=" + id + "&b=" + b);	// TODO: url
			return "Email2(" + id + ", '" + b + "')";
		}

		public string oViewEmail(string id, params string[] args)
		{
			if (id == null)
				throw new ArgumentNullException("id");

			string url = _page.Common.oHref(UrlUtility.MakeJScriptUrl(oViewEmailScript(id)), _page.FormOut.Image("mail"));
			if (args != null || args.Length > 0)
			{
				StringBuilder strBuilder = new StringBuilder(url);
				for (int i = 0; i < args.Length; i++)
				{
					strBuilder
						.Append(HtmlUtility.NonBreakingSpacer)
						.Append(args[i]);
				}
				return strBuilder.ToString();
			}
			return url;
		}

		public string OpenWindow(string url, params string[] args)
		{
			_page.Href.AddLegalTarget(url);
			return UrlUtility.MakeJScriptUrl("openWindow('" + url + "'") + makeWindowsParams(args) + ");";
		}


		#region Implementation
		private string makeWindowsParams(string[] args)
		{
			if (args == null || args.Length == 0)
				return "";

			StringBuilder strBuilder = new StringBuilder();
			for (int i = 0; i < args.Length; i++)
			{
				strBuilder
					.Append(", \"")
					.Append(args[i])
					.Append("\"");
			}
			return strBuilder.ToString();
		}
		#endregion
	}
}
