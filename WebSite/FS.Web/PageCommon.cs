﻿using Lexxys;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using FS.Web.Compatibility;
using FS.Web.Core;
using Newtonsoft.Json;

namespace FS.Web
{
	public interface IPageCommon : IPage
	{
		FoundationSource.Admin.CPMain.Ap Ap { get; }
		FoundationSource.Admin.IAspAuthor Author__ { get; }


		SessionBo SessionBo { get; }
		Href Href { get; }
		Common Common { get; }
		SecureConfig SecureConfig { get; }
		Access Access { get; }
		Output Output { get; }
		FormIn FormIn { get; }
		FormOut FormOut { get; }
		Errors Errors { get; }
		Panel Panel { get; }

		// redirect
		void ForceClose();
		void CloseMe();
		void Redirect(string url);
		string GetUrl(string url);
	}

	public enum PageTabState
	{
		Enabled,
		Disabled,
		Current
	}

	public class PageCommon : PageBase, IPageCommon
	{
		[Include("~/utils/session-bo", 0)]
		public SessionBo SessionBo { get; private set; }
		[Include("~/utils/href", 1)]
		public Href Href { get; private set; }
		[Include("~/utils/common", 2)]
		public Common Common { get; private set; }
		[Include("~/utils/secureconf", 3)]
		public SecureConfig SecureConfig { get; private set; }
		[Include("~/utils/access", 4)]
		public Access Access { get; private set; }
		[Include("~/utils/output", 5)]
		public Output Output { get; private set; }
		[Include("~/utils/form-in", 6)]
		public FormIn FormIn { get; private set; }
		[Include("~/utils/form-out", 7)]
		public FormOut FormOut { get; private set; }
		[Include("~/utils/errors", 8)]
		public Errors Errors { get; private set; }
		[Include("~/utils/panel", 9)]
		public Panel Panel { get; private set; }

		private readonly _Redirect _redirect;
		private readonly HttpCallback _callback = new HttpCallback();

		private const string TitleDefault = "FS: Administrative Tools";
		private static readonly string TitlePattern = TitleDefault + " - {0}";

		private _PageRenderer _pageRenderer;

		public PageCommon()
		{
			SessionBo = new SessionBo(this);
			SecureConfig = new SecureConfig(this);
			Href = new Href(this);
			Common = new Common(this);
			Access = new Access(this);
			Output = new Output(this);
			FormIn = new FormIn(this);
			FormOut = new FormOut(this);
			Errors = new Errors(this);
			Panel = new Panel(this);

			_redirect = new _Redirect(this);
			_pageRenderer = new _PageRenderer(this);
		}

		public override void BeginPage(int width = 780)
		{
			_pageRenderer.BeginPage(width);
		}

		public override void EndPage()
		{
			_pageRenderer.EndPage();
		}

		public FoundationSource.Admin.CPMain.Ap Ap
		{
			get { throw EX.NotImplemented(); }
		}
		public FoundationSource.Admin.IAspAuthor Author__
		{
			get { throw EX.NotImplemented(); }
		}

		#region Panels
		public void BeginPanel(string name, params string[] args)
		{
			Panel.BeginPanel(name, args);
		}

		public void Group(params string[] args)
		{
			Panel.Group(args);
		}

		public void Cell()
		{
			Panel.Cell(null);
		}
		public void Cell(string text, params string[] args)
		{
			Panel.Cell(text, args);
		}

		public void EndPanel()
		{
			Panel.EndPanel();
		}

		public IDisposable Panel2(string name, params string[] args)
		{
			return new _PanelContainer(this, name, args);
		}
		#endregion

		#region Redirect
		public void ForceClose()
		{
			_redirect.ForceClose();
		}
		public void CloseMe()
		{
			_redirect.CloseMe();
		}
		public void Redirect(string url)
		{
			_redirect.Redirect(url);
		}
		public string GetUrl(string url)
		{
			return _redirect.GetUrl(url);
		}
		#endregion

		public HttpCallback Callback
		{
			get { return _callback; }
		}


		protected override void InitInternal()
		{
			base.InitInternal();

			Callback.EnsurePageMode(Request);
		}


		#region Implementation
		#region Nested Types
		private class _PageRenderer
		{
			private readonly PageCommon _page;
			private HtmlWriter _writer;

			public _PageRenderer(PageCommon page)
			{
				if (page == null)
					throw new ArgumentNullException("page");

				_page = page;
			}

			public void BeginPage(int width)
			{
				if (_writer == null)
					_writer = new HtmlWriter(_page.Writer);

				_Items<string> items = _page.Items;

				items.AddItem("headermenu", "Change Password", "companies/changePassword.asp");

				string title = items.GetItem("title", 1);
				string group = items.GetItem("title", 0);
				string onload = items.GetItem("onload", 0);

				title = JsCompatibility.HasValue(title) ? String.Format(TitlePattern, title) : TitleDefault;
				ProcessHtmlHead(title);

				if (_page.Common.LoginMethod == LoginMethod.Close || items.HasItems("simple"))
				{
					if (items.HasItems("leave-last-state"))
						_page.RestoreState();
					else
						_page.SaveState();

					_writer
						.AttrStyle("margin", "0")
						.Tag("body");
					BeginForm(items.GetItemsList("form"));
					_writer
						.WriteComment("$$[PAGE]")
						.Attr("class", "ContentBackgroundNoBorder")
						.Attr("height", "100%").Attr("width", "100%")
						.Attr("cellspacing", "2").Attr("cellpadding", "2")
						.Tag("table");

					if (items.HasItems("form") && _page.Common.LoginMethod == LoginMethod.Close)
						_writer.Hidden("_lay", "popup");

					if (items.HasItems("header"))
					{
						_writer
							.Tag("tr")
								.Attr("valign", "top")
								.Attr("height", "1%")
								.Tag("td")
									.Attr("width", "100%")
									.Attr("border", "0")
									.Attr("cellspacing", "2")
									.Attr("cellpadding", "5")
									.Tag("table")
										.Tag("tr")
											.Attr("class", "popupTopColor")
											.Attr("align", "center")
											.Tag("td")
												.Attr("class", "popupHeader")
												.Tag("span")
													.Write(items.GetItem("header", 0).ToUpperInvariant())
												.End()
											.End()
										.End()
									.End()
								.End()
							.End();
					}
				}
				else
				{
					if (items.HasItems("leave-last-state"))
						_page.RestoreState();

					_page.PrepareState();

					if (items.HasItems("skip-my-state"))
						_page.SaveState();

					_writer
						.AttrStyle("margin", "0")
						.AttrStyle("background", "url(\"" + _page.GetImageUrl("bg-main.gif") + "\") white")
						.Tag("body");
					BeginForm(items.GetItemsList("form"));

					ProcessHeader(group);

					ProcessDefaultButton();

					_writer
						.WriteComment("$$[PAGE]")
						.Attr("width", width.ToString() + "px")
						.Attr("cellspacing", "0").Attr("cellpadding", "0")
						.Attr("align", "center")
						.Attr("border", "0")
						.Tag("table");

					if ((_page.Parts & PageParts.HeaderNavigation) > 0)
						ProcessNavigation();

					ProcessTabs();

					_writer
						.Attr("Id", "pageContent")
						.Tag("tr")
							.Attr("class", "ContentBackground")
							.Tag("td")
								.Attr("class", "NormalBackground")
								.Attr("width", width.ToString() + "px")
								.Attr("border", "0")
								.Attr("cellspacing", "2").Attr("cellpadding", "2")
								.Attr("align", "center")
								.Tag("table");
				}

				_writer
					.Tag("tr")
						.Attr("valign", "top")
						.Tag("td")
						.WriteComment("$$[BODY]");

				ProcessErrors();
			}

			public void EndPage()
			{
				_page.ClientScript.WriteBlocks();
				_page.ClientScript.WriteStartups();

				_writer
											.WriteComment("$$[/BODY]")
										.End("td")
									.End("tr");
				if (_page.Common.LoginMethod != LoginMethod.Close)
				{
					_writer
								.End("table")
							.End("td")
						.End("tr");
				}
				_writer
					.End("table")
					.WriteComment("$$[/PAGE]");

				EndForm();

				_writer
					.End("body")
					.End("html");
			}

			public string GetSuccessMessageHtml(string text)
			{
				return HtmlUtility.BeginTag("div", HtmlUtility.Attribute("class", "message message-success")) + text + HtmlUtility.EndTag("div");
			}

			public string GetErrorMessageHtml(string text)
			{
				return HtmlUtility.BeginTag("div", HtmlUtility.Attribute("class", "message message-error")) + text + HtmlUtility.EndTag("div");
			}

			public string GetInfoMessageHtml(string text)
			{
				return HtmlUtility.BeginTag("div", HtmlUtility.Attribute("class", "message message-info")) + text + HtmlUtility.EndTag("div");
			}


			#region Implementation
			private void BeginForm(string[] items)
			{
				string name = items.Length > 0 ? items[0] : "main";
				bool simple = false;
				if (items.Length > 1)
				{
					for (int i = 1; i < items.Length; i++)
					{
						string t = items[i];
						if (t == "simple")
							simple = true;
						else if (JsCompatibility.HasValue(t))
							_writer.Attr(t);
					}
				}
				if (!_writer.HasAttributeInBuffer("method"))
					_writer.Attr("method", "post");
				if (!_writer.HasAttributeInBuffer("action"))
					_writer.Attr("action", HtmlUtility.HtmlEncode(_page.MyUrl));
				if (!_writer.HasAttributeInBuffer("name"))
					_writer.Attr("name", name);

				_writer
					.WriteComment("$$[FORM]")
					.Tag("form");
				if (!simple)
					_writer.Write(_page.FormOut.GenerateFormIdHtml(name));
			}

			private void EndForm()
			{
				_writer
					.End("form")
					.WriteComment("$$[/FORM]");
			}

			private void ProcessHeader(string group)
			{
				_Items<string> items = _page.Items;

				_writer
					.WriteComment("$$[HEADER]")
					.Attr("id", "pageHeader")
					.Attr("width", "100%")
					.Attr("border", "0")
					.Attr("cellspacing", "0").Attr("cellpadding", "0")
					.AttrStyle("background", "url(\"" + _page.GetImageUrl("bg-header.gif") + "\") top repeat-x white")
					.Tag("table")
						.Attr("valign", "top")
						.Tag("tr")
							.Attr("width", "5px")
							.Tag("td")
								.Img(_page.GetImageUrl("spacer.gif"), width: 5, height: 1)
							.End()
							.Attr("align", "left")
							.Attr("width", "82px")
							.Tag("td")
								.Img(_page.GetImageUrl("icon-main.jpg"), width: 82, height: 79)
							.End()
							.Tag("td");
				int n = items.GetItemsCount("headermenu");
				if (n > 0 && (_page.Parts & PageParts.Menu) > 0)
					ProcessMenu();

				_writer
								.Attr("border", "0")
								.Attr("cellspacing", "0").Attr("cellpadding", "0")
								.Attr("align", "right")
								.Tag("table")
									.Attr("valign", "top")
									.Tag("tr")
										.Tag("td")
											.Attr("border", "0")
											.Attr("cellspacing", "0").Attr("cellpadding", "0")
											.Tag("table")
												.Tag("tr")
													.Tag("td")
														.Img(_page.GetImageUrl("spacer.gif"), width: 1, height: 13)
													.End()
												.End()
												.Tag("tr")
													.Attr("class", "SystemMenu")
													.Tag("td");

				string separator = "";
				if ((_page.Parts & PageParts.HomeUrl) > 0)
				{
					_writer
														.Write(separator)
														.Attr("href", _page.Url.Action("index", "root"))
														.Attr("class", "SystemMenu")
														.Tag("a")
															.Write("Home")
														.End();
					separator = "|";
				}
				if ((_page.Parts & PageParts.Logout) > 0)
				{
					_writer
														.Write(separator)
														.Attr("href", _page.Url.Action("logout", "root"))
														.Attr("class", "SystemMenu")
														.Tag("a")
															.Write("Log out")
														.End();
					separator = "|";
				}
				if ((_page.Parts & PageParts.Print) > 0)
				{
					_writer
														.Write(separator)
														.Attr("href", UrlUtility.MakeJScriptUrl("window.print();"))
														.Attr("class", "SystemMenu")
														.Tag("a")
															.Write("Print")
														.End();
				}
				if (separator == "")
					_writer.Write(HtmlUtility.NonBreakingSpacer);

				_writer
													.End("td")
												.End("tr")
											.End("table")
										.End("td")
										.Attr("align", "right")
										.Attr("width", "153px")
										.Tag("td")
											.Attr("href", Href.FsWebSiteUrl)
											.Attr("target", "_blank")
											.Tag("a")
												.Img(_page.GetImageUrl("fs-logo.gif"), width: 153, height: 48, hSpace: 20)
											.End()
										.End()
									.End("tr")
									.Tag("tr")
										.Attr("colspan", "2")
										.Tag("td")
											.Img(_page.GetImageUrl("spacer.gif"), width: 1, height: 4)
										.End()
									.End();

				if ((_page.Parts & PageParts.Group) > 0 && JsCompatibility.HasValue(group))
				{
					_writer
									.Tag("tr")
										.Attr("colspan", "2")
										.Attr("class", "PageTitle")
										.Attr("align", "right")
										.Tag("td")
											.Attr("border", "0")
											.Attr("cellspacing", "0").Attr("cellpadding", "4")
											.Tag("table")
												.Tag("tr")
													.Tag("td")
														.Img(_page.GetImageUrl("icon-title.gif"), width: 13, height: 13)
													.End()
													.Attr("class", "PageTitle")
													.Tag("td")
														.Write(group.ToUpperInvariant())
													.End()
													.Tag("td")
														.Img(_page.GetImageUrl("spacer.gif"), width: 13, height: 1)
													.End()
												.End()
											.End()
										.End()
									.End();
				}

				_writer
									.End("table")
								.End("td")
							.End("tr")
						.Tag("tr")
							.Tag("td")
								.Img(_page.GetImageUrl("spacer.gif"), width: 5, height: 1)
							.End()
							.Tag("td")
								.Img(_page.GetImageUrl("spacer.gif"), width: 82, height: 1)
							.End()
							.Tag("td")
								.Img(_page.GetImageUrl("spacer.gif"), width: 690, height: 1)
							.End()
						.End()
					.End("table")
					.WriteComment("$$[/HEADER]");
			}

			private void ProcessMenu()
			{
				_Items<string> items = _page.Items;
				int n = items.GetItemsCount("headermenu");

				_writer
					.WriteComment("$$[MENU]")
					.Attr("border", "0")
					.Attr("cellspacing", "0").Attr("cellpadding", "0")
					.Attr("align", "left")
					.Tag("table")
						.Tag("tr")
							.Attr("rowspan", "2")
							.Tag("td")
								.Img(_page.GetImageUrl("spacer.gif"), width: 10, height: 1)
							.End()
							.Tag("td")
								.Img(_page.GetImageUrl("txt-administrativetools.gif"), width: 172, height: 44)
							.End()
						.End()
						.Tag("tr")
							.Tag("td")
								.Img(_page.GetImageUrl("spacer.gif"), width: 5, height: 9)
					//.TagSingle("br")
								.Attr("border", "0")
								.Attr("cellspacing", "1").Attr("cellpadding", "2")
								.Tag("table")
									.Tag("tr");

				for (int i = 0; i < n; i++)
				{
					string name = items.GetItem("headermenu", i, 0);
					string url = items.GetItem("headermenu", i, 1);

					_writer
										.Attr("width", "13px")
										.Tag("td")
											.Img(_page.GetImageUrl("icon-submenu.gif"), width: 13, height: 14)
										.End()
										.Tag("td")
											.Attr("href", url)
											.Attr("class", "AdminMenu")
											.Tag("a")
												.WriteEncoded(name)
											.End()
										.End();
				}

				_writer
									.End("tr")
								.End("table")
							.End("td")
						.End("tr")
					.End("table")
					.WriteComment("$$[/MENU]");
			}

			private void ProcessHtmlHead(string title)
			{
				_Items<string> items = _page.Items;
				bool keepAlive = items.HasItems("keepAlive");

				_writer
					.Doctype("html", "-//W3C//DTD HTML 4.01 Transitional//EN", "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd")
					.Tag("html")
						.Tag("head")
							.Attr("http-equiv", "X-UA-Compatible")
							.Attr("content", "IE=EmulateIE7")
							.TagSingle("meta")
							.Tag("title")
								.WriteEncoded(title)
							.End();

				ProcessStyles();
				ProcessScripts();

				GmsKeepAliveScript();

				if (keepAlive)
					_page.ClientScript.RegisterStartup("keepAlive", "window.setInterval(keepMeAlive, 60000);");

				_writer.End("head");
			}

			private void ProcessErrors()
			{
				if (_page.Errors.NoErrors())
					return;
				_Items<string> items = _page.Items;
				if (!items.HasItems("errors"))
					return;

				string[] errors = (items.GetItems("errors").Length > 0) ? items.GetItems("errors")[0] : new string[0];
				string customText = null;
				bool showFields = false;
				if (errors.Length > 0)
					customText = errors[0];
				if (errors.Length > 1)
					showFields = !String.IsNullOrEmpty(errors[1]);

				string msg = _page.Errors.GenerateMessagesHtml(HtmlUtility.Br);
				if (String.IsNullOrEmpty(customText) && msg == null)
					_writer.Write(GetErrorMessageHtml("Please re-enter the fields marked with" + HtmlUtility.NonBreakingSpacer + HtmlUtility.Img(_page.GetImageUrl("icon_errorSM.gif"), width: 12)));
				else if (!String.IsNullOrEmpty(customText))
					_writer.Write(GetErrorMessageHtml(customText));

				if (msg != null)
					_writer.Write(GetErrorMessageHtml(msg));

				if (showFields)
					_writer.Write(GetErrorMessageHtml(String.Join(HtmlUtility.Br, _page.Errors.GetAllErrors())));
			}

			private void ProcessScripts()
			{
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("jsFramework", "firebugx.js"), true);
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("jsFramework", "fs.js"), true);
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("jsFramework", "fs.web.js"), true);
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("jsFramework", "fs.asp.js"), true);
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("jsFramework", "fs.formatters.js"), true);
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("windows.js"));
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("tools.js"));
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("popups.js"));
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("sort3.js"));
				_page.ClientScript.RegisterInclude(_page.GetScriptUrl("tabExtensions.js"));

				_page.ClientScript.WriteIncludes();

				_page.ClientScript.RegisterStartup("DecorateDisabledButtons", "checkDisabledButton(); dbg = '';");
			}

			private void ProcessStyles()
			{
				_page.ClientStyle.RegisterInclude(_page.GetStyleUrl("default.css"));
				_page.ClientStyle.RegisterInclude(_page.GetStyleUrl("panel.css"));
				_page.ClientStyle.RegisterInclude(_page.GetStyleUrl("ajax", "calendar.css"));
				_page.ClientStyle.RegisterInclude(_page.GetStyleUrl("ajax", "elements.css"));
				_page.ClientStyle.RegisterInclude(_page.GetStyleUrl("ajax", "dropdown.css"));
				_page.ClientStyle.RegisterInclude(_page.GetStyleUrl("ajax", "Menu.ContextMenu.css"));

				_page.ClientStyle.RegisterInclude(_page.GetStyleUrl("jquery", "jquery-ui.css"));

				_page.ClientStyle.WriteIncludes();
				_page.ClientStyle.WriteBlocks();

				_page.ClientScript.RequireJQuery(true);
				//_page.ClientScript.RequireMsAjax(false);
			}

			private void GmsKeepAliveScript()
			{
				if (!_page.Access.HasGmsReference())
					return;

				string s = (string)_page.Session[FS.Web.Utils.GmsReference.SesionKeys.Spacer];
				// @imported: keep session id in every keepalive call
				_page.Session[FS.Web.Utils.GmsReference.SesionKeys.Spacer] = _page.Session[FS.Web.Utils.GmsReference.SesionKeys.Session];

				string random = new Random().Next().ToString();
				StringBuilder script = new StringBuilder();
				script
					.AppendLine("var keepalivePage = new Image();")
					.AppendLine("keepalivePage.src = ").Append(s.Replace("{seed}", random)).Append(";");
				_page.ClientScript.RegisterBlock("GmsKeepAlive", script.ToString());
			}

			private void ProcessNavigation()
			{
				if (!_page.Items.HasItems("navigation"))
					return;

				string nav1 = _page.Items.GetItem("navigation", 0);
				string nav2 = _page.Items.GetItem("navigation", 1) ?? "";
				string backUrl;

				if (nav1 == null)
					nav1 = _page.Navigation;
				else
				{
					int i = nav1.IndexOf(';');
					int j = nav1.IndexOf('|');
					if (i > j)
						nav1 += "|" + _page.MyUrl;
					_page.Navigation = nav1;
				}

				if (!String.IsNullOrEmpty(nav2))
					nav1 += ";" + nav2;

				using (var writer2 = new HtmlWriter())
				{
					string[] nav1Parts = nav1.Split(new[] { ';' });
					for (int i = 0; i < nav1Parts.Length; i++)
					{
						string name, url;
						string[] n = nav1Parts[i].Split(new[] { '|' }, 2);
						if (n != null && n.Length == 2)
						{
							name = n[0];
							url = n[1];
						}
						else
						{
							name = nav1Parts[i];
							url = "";
						}
						if (i == nav1Parts.Length - 1)
						{
							writer2
								.Attr("class", "SmallDarkFont")
								.Tag("span")
									.Write(name)
								.End();
						}
						else
						{
							writer2
								.Write(_page.Common.oHref(url, name, HtmlUtility.Attribute("class", "SmallDarkFont"), "noback"))
								.Write(HtmlUtility.GreaterThan);
							if (i == nav1Parts.Length - 2)
							{
								backUrl = url;
								_page.Href.AddLegalTarget(backUrl);
							}
						}
					}

					_writer
						.WriteComment("$$[NAVIGATOR]")
						.Attr("id", "navigatorRow")
						.Tag("tr")
							.Attr("align", "right")
							.Tag("td")
								.Attr("cellspacing", "5").Attr("cellpadding", "0")
								.Attr("border", "0")
								.Attr("id", "pageNavigator")
								.Tag("table")
									.Tag("tr")
										.AttrStyle("height", "20px")
										.Attr("class", "SmallDarkFont")
										.Tag("td");

					string html = writer2.GetContent();
					if (!String.IsNullOrEmpty(html))
					{
						_writer
											.WriteEncoded("You are here: ")
											.Write(html);
					}

					_writer
										.End()
									.End()
								.End()
							.End()
						.End();
				}
			}

			private void ProcessDefaultButton()
			{
				string defaultButton = _page.Items.GetItem("defaultButton", 0);
				if (!String.IsNullOrEmpty(defaultButton))
				{
					_writer
						.WriteComment("$$[DEFAULT BUTTON IMPL]")
						.Submit(defaultButton, defaultButton, cssClass: "input-button-default")
						.Attr("type", "text")
						.Attr("name", defaultButton + "__txt1")
						.Attr("class", "input-button-default-txt")
						.TagSingle("input")
						.Attr("type", "text")
						.Attr("name", defaultButton + "__txt2")
						.Attr("class", "input-button-default-txt")
						.TagSingle("input")
						.WriteComment("$$[.DEFAULT BUTTON IMPL]");
				}
			}

			private void ProcessTabs()
			{
				_Items<string> items = _page.Items;

				int n = items.GetItemsCount("tab");
				if (n > 0)
				{
					string pageTitle = items.GetItem("title", 1);

					using (HtmlWriter htmlWriter1 = new HtmlWriter(HtmlWriterSettings.None))
					using (HtmlWriter htmlWriter2 = new HtmlWriter(HtmlWriterSettings.None))
					{
						for (int i = 0; i < n; i++)
						{
							string name = items.GetItem("tab", i, 0);
							string url = items.GetItem("tab", i, 1);
							PageTabState type;
							string title = items.GetItem("tab", i, 3);
							if (!Enum.TryParse<PageTabState>(items.GetItem("tab", i, 2), out type) && (name == pageTitle || url == null))
								type = PageTabState.Current;
							else if (type == PageTabState.Disabled)
								continue;

							if (JsCompatibility.HasValue(url))
								name = _page.Common.oHref(url, name, title, type == PageTabState.Enabled ? "class=TabNormal" : "class=TabCurrent");
							bool isActive = (type == PageTabState.Current);
							htmlWriter1
								.Tag("td")
									.Img(_page.GetImageUrl(isActive ? "tab_sel_left.gif" : "tab_nonsel_left.gif"), width: 17, height: 17)
								.End()
								.Attr("class", isActive ? "TabCurrent" : "TabNormal")
								.Attr("valign", "bottom")
								.Attr("nowrap", "nowrap")
								.Tag("td")
									.Write(name)
								.End()
								.Tag("td")
									.Img(_page.GetImageUrl(isActive ? "tab_sel_right.gif" : "tab_nonsel_right.gif"), width: 17, height: 17)
								.End();
							htmlWriter2
								.Attr("colspan", "3")
								.Tag("td")
									.Img(_page.GetImageUrl("spacer.gif"), width: 1, height: 1)
								.End();
						}

						_writer
							.Attr("id", "pageTabRow")
							.Tag("tr")
								.Tag("td")
									.Attr("id", "tabs")
									.Tag("div")
										.Attr("border", "0")
										.Attr("cellspacing", "0").Attr("cellpadding", "0")
										.Tag("table")
											.Tag("tr")
												.Write(htmlWriter1.GetContent())
											.End()
											.Tag("tr")
												.Write(htmlWriter2.GetContent())
											.End()
										.End()
									.End()
								.End()
							.End();
					}

					_page.ClientScript.RegisterStartup("TabScroller-initialization",
@"var page_tabs = $('#tabs').hide();
var page_contentWidth = $('#pageContent').outerWidth();
page_tabs.show();
var tabScroller = new TabScroller('tabs', page_contentWidth);
tabScroller.Init();");
				}
			}
			#endregion
		}

		private class _PanelContainer : IDisposable
		{
			private readonly PageCommon _page;

			public _PanelContainer(PageCommon page, string name, params string[] args)
			{
				if (page == null)
					throw EX.ArgumentNull("page");

				_page = page;
				_page.Panel.BeginPanel(name, args);
			}

			public void Dispose()
			{
				_page.Panel.EndPanel();
			}
		}
		#endregion
		#endregion
	}

	public sealed class HttpCallback
	{
		private bool _isCallback;
		private string _callbackInvoker;
		private string _callbackInitiator;
		private dynamic _callbackData;

		public bool IsCallback
		{
			get { return _isCallback; }
		}

		public string CallbackInvoker
		{
			get { return _callbackInvoker; }
		}

		public string CallbackInitiator
		{
			get { return _callbackInitiator; }
		}

		/// <summary>
		/// Returns JSON object
		/// </summary>
		public dynamic CallbackData
		{
			get { return _callbackData; }
		}

		public void EnsurePageMode(System.Web.HttpRequest Request)
		{
			string contentType = Request.ServerVariables["HTTP_CONTENT-TYPE"];
			if (contentType != null && !contentType.Equals("multipart/form-data", StringComparison.OrdinalIgnoreCase) && Request.Form["__CALLBACKID"] != null)
			{
				_callbackInvoker = Request.Form["__CALLBACKID"];
				_callbackInitiator = Request.Form["__CALLBACKID2"];
				SetPageAsyncData(Request.Form["__CALLBACKPARAM"]);
				_isCallback = true;
			}
			else
			{
				_isCallback = false;
			}
		}

		private void SetPageAsyncData(string data)
		{
			if (String.IsNullOrWhiteSpace(data))
				return;

			JsonSerializer jsonSerializer = JsonSerializer.Create(new JsonSerializerSettings() { Culture = System.Globalization.CultureInfo.InvariantCulture });
			_callbackData = jsonSerializer.Deserialize(new JsonTextReader(new StringReader(data)));
		}
	}
}
