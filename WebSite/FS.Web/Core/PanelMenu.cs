﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class PanelMenu : PanelBase
	{
		internal PanelMenu(PageCommon page)
			: base(page, false)
		{
			A0.Extend(new Dictionary<string, string>()
				{
					{ "Width", "250" },
					{ "Title", null },
					{ "NameClass", "MainMenuText" },
					{ "CountClass", "MainMenuBoldText" },
					{ "Background", "NormalBackground" },
					{ "InfoNameClass", "PanelHeader" },
					{ "TitleClass", "HeaderText" },
					{ "TitleBackground", "TitleBackground" },
					{ "Class", null }
				});
		}

		public override string Name
		{
			get { return "panel-menu"; }
		}

		protected override void BeginCore(params string[] args)
		{
			dynamic parms = A0;
			A0.Parse(args);

			HtmlWriter writer = Writer;
			writer.Attr(args);
			if (!writer.HasAttributeInBuffer("cellspacing"))
				writer.Attr("cellspacing", "0");
			if (!writer.HasAttributeInBuffer("cellpadding"))
				writer.Attr("cellpadding", "0");

			writer
				.Attr("border", "0")
				.Attr(A0.Attribute("width"))
				.Attr(A0.Attribute("background", "class"))
				.Attr(A0.Attribute("style", HtmlUtility.GetStyleRule("border", "white 1px solid", "padding", "3px")))
				.Tag("table");

			if (!String.IsNullOrWhiteSpace(parms.Title))
			{
				writer
					.Attr(A0.Attribute("titlebackground", "class"))
					.Tag("tr")
						.Attr("colspan", "3")
						.Attr("align", "center")
						.Attr(A0.Attribute("titleclass", "class"))
						.Tag("td")
							.Write(parms.Title)
						.End()
					.End();
			}

			writer
				.Tag("tr")
					.Attr("width", "10")
					.Tag("td")
						.Write(HtmlUtility.NonBreakingSpacer)
					.End()
					.Tag("td")
						.Attr("border", "0")
						.Attr("width", "100%")
						.Attr("align", "center")
						.Tag("table");
		}

		protected override void EndCore()
		{
			SetCellEnding(null, true);
			SetRowEnding(null, true);

			HtmlWriter writer = Writer;
			writer
				.End("table")
				.End("td")
					.Attr("width", "10")
					.Tag("td")
						.Write(HtmlUtility.NonBreakingSpacer)
					.End()
				.End("tr")
				.End("table");
		}

		protected override void RowCore(params string[] args)
		{
			; // nothing to do
		}

		/*
			@imported
			* Arguments
				1. Name
				2. Url
				3. Count
				4. Access
		*/
		protected override void CellCore(params string[] args)
		{
			dynamic parms = A0;
			string name = args[0];
			string url = args[1];
			int count = args.Length < 3 ? -1 : JsCompatibility.ParseInt(args[2]);
			bool access = args.Length < 4 ? true : JsCompatibility.ToBoolean(args[3]);

			SetCellEnding(null, true);
			SetRowEnding(null, true);

			if (access)
			{
				HtmlWriter writer = Writer;
				writer
					.Tag("tr")
						.Attr("width", "80%")
						.Attr(A0.Attribute("nameClass", "class"))
						.Tag("td");
				if (count > 0 || count == -1)
				{
					Page.Common.oHref(url, name);
					writer
						.Attr("href", url)
						.Tag("a")
							.Write(name)
						.End();
				}
				else
					writer.Write(name);
				writer
					.End("td")
					.Attr("width", "20%")
					.Attr("align", "right")
					.Attr(A0.Attribute("countClass", "class"))
					.Tag("td")
						.Write((count != -1) ? count.ToString() : HtmlUtility.NonBreakingSpacer);

				SetCellEnding(() => writer.End("td").End("tr"));
			}
		}
	}
}
