﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class _Items<T>
	{
		private Dictionary<string, List<T[]>> _storage;

		internal _Items()
		{
			_storage = new Dictionary<string, List<T[]>>();
		}
		internal _Items(int capacity)
		{
			if (capacity <= 0)
				throw new ArgumentOutOfRangeException("capacity");

			_storage = new Dictionary<string, List<T[]>>(capacity);
		}

		public void AddItem(string name)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			List<T[]> items;
			if (!_storage.TryGetValue(name, out items))
				_storage.Add(name, new List<T[]>());
		}
		public void AddItem(string name, params T[] args)
		{
			if (name == null)
				throw new ArgumentNullException("name");
			if (args == null || args.Length == 0)
			{
				AddItem(name);
				return;
			}

			List<T[]> items;
			if (_storage.TryGetValue(name, out items))
				items.Add(args);
			else
				_storage.Add(name, new List<T[]>() { args });
		}

		public bool HasItems(string name)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			return _storage.ContainsKey(name);
		}
		public int GetItemsCount(string name)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			List<T[]> items;
			return _storage.TryGetValue(name, out items) ? items.Count : 0;
		}

		public T[][] GetItems(string name)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			List<T[]> items;
			return _storage.TryGetValue(name, out items) ? items.ToArray() : new T[0][];
		}

		public T[] GetItemsList(string name)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			List<T[]> items;
			return (_storage.TryGetValue(name, out items) && items.Count > 0)
				? items[items.Count - 1]
				: new T[0];
		}
		public T[] GetItemsList(string name, int index)
		{
			if (name == null)
				throw new ArgumentNullException("name");
			if (index < 0)
				throw new ArgumentOutOfRangeException("index");

			List<T[]> items;
			return (_storage.TryGetValue(name, out items) && index < items.Count)
				? items[index]
				: new T[0];
		}

		public T GetItem(string name)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			T[] itemsList = GetItemsList(name);
			return (itemsList.Length > 0) ? itemsList[itemsList.Length - 1] : default(T);
		}
		public T GetItem(string name, int index2)
		{
			if (name == null)
				throw new ArgumentNullException("name");
			if (index2 < 0)
				throw new ArgumentOutOfRangeException("index2");

			T[] itemsList = GetItemsList(name);
			return (itemsList.Length > 0 && index2 < itemsList.Length) ? itemsList[index2] : default(T);
		}
		public T GetItem(string name, int index1, int index2)
		{
			if (name == null)
				throw new ArgumentNullException("name");
			if (index1 < 0)
				throw new ArgumentOutOfRangeException("index1");
			if (index2 < 0)
				throw new ArgumentOutOfRangeException("index2");

			T[] itemsList = GetItemsList(name, index1);
			return (itemsList.Length > 0 && index2 < itemsList.Length) ? itemsList[index2] : default(T);
		}
	}
}
