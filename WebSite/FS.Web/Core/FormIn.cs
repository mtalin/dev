﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace FS.Web.Core
{
	public sealed class FormIn
	{
		public const string FormIdLocalName = FormOut.FormIdLocalName;
		private readonly PageCommon _page;

		internal FormIn(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public bool QueryStringOnly { get; set; }

		public bool UseQueryString { get; set; }

		public HttpSessionState Session
		{
			get { return _page.Session; }
		}

		public HttpRequest Request
		{
			get { return _page.Request; }
		}

		// was fmList
		public string[] GetList(string name)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			string[] values = null;
			if (!QueryStringOnly)
			{
				values = Request.Form.GetValues(name);
				if (values != null && (values.Length > 0 || !UseQueryString))
					return values;
			}

			return Request.QueryString.GetValues(name) ?? new string[0];
		}

		// was fmCheekId
		public bool CheckId()
		{
			string id = _page.Common.GetLocal(FormIdLocalName) as string;
			_page.Common.RemoveLocal(FormIdLocalName);
			return JsCompatibility.HasValue(id) && Request.Form[FormIdLocalName] == id;
		}

		// was fmPeekId
		public bool PeekId()
		{
			string id = _page.Common.GetLocal(FormIdLocalName) as string;
			return JsCompatibility.HasValue(id) && Request.Form[FormIdLocalName] == id;
		}

		// was fmParamStr
		public string ParamStr(string name, string defaultValue = null, bool persist = false)
		{
			bool uqs = UseQueryString;
			UseQueryString = true;
			string x = GetString(name);
			UseQueryString = uqs;
			if (JsCompatibility.HasValue(x))
			{
				if (persist)
					_page.Common.SetLocal(name, x);
				return x;
			}

			if (persist)
			{
				x = _page.Common.GetLocal(name) as string;
				if (!String.IsNullOrWhiteSpace(x))
					return x.Trim();
			}
			return defaultValue;
		}

		// was fmParamInt
		public int? ParamInt(string name, int? defaultValue = null, bool persist = false)
		{
			bool uqs = UseQueryString;
			UseQueryString = true;
			string x = GetString(name);
			UseQueryString = uqs;
			int result;
			if (JsCompatibility.HasValue(x))
			{
				if (persist)
					_page.Common.SetLocal(name, x);
				return (JsCompatibility.TryParseInt(x, out result))
					? result
					: defaultValue;
			}

			if (persist)
			{
				x = _page.Common.GetLocal(name) as string;
				if (JsCompatibility.HasValue(x))
				{
					return (JsCompatibility.TryParseInt(x, out result))
						? result
						: defaultValue;
				}
			}
			return defaultValue;
		}

		public bool Exists(string name)
		{
			if (QueryStringOnly)
				return Request.QueryString[name] != null;
			else
				return Request.Form[name] != null || (UseQueryString && Request.QueryString[name] != null);
		}

		// was fmValue
		public string GetValue(string name, string defaultValue, bool required = false)
		{
			string x = GetValue(name);
			if (x == null)
			{
				if (required)
					_page.Errors.AddError(name);
				return defaultValue;
			}
			return x;
		}
		internal string GetValue(string name)
		{
			string x;
			string v;
			if (QueryStringOnly)
			{
				if (String.IsNullOrEmpty(x = Request.QueryString[name]))
					x = null;
			}
			else if (String.IsNullOrEmpty(x = Request.Form[name]))
			{
				if (!UseQueryString || String.IsNullOrEmpty(v = Request.QueryString[name]))
					x = null;
			}
			return x;
		}

		// was fmBool
		public bool GetBoolean(string name)
		{
			return GetValue(name) != null;
		}

		// was fmStr
		public string GetString(string name, string defaultValue, bool required = false)
		{
			string x = GetString(name);
			if (x == null)
			{
				if (required)
					_page.Errors.AddError(name);
				return defaultValue;
			}

			return x;
		}
		internal string GetString(string name)
		{
			string x = GetValue(name);
			return (!String.IsNullOrWhiteSpace(x)) ? x.Trim() : null;
		}

		// was fmInt
		public int? GetInt32(string name, bool required = false)
		{
			return GetInt32(name, _MissingValues.Int32, required);
		}
		public int? GetInt32(string name, int? defaultValue, bool required = false)
		{
			int? y;
			string x = GetString(name);
			if (x == null)
			{
				if (required)
					_page.Errors.AddError(name);
				return defaultValue;
			}
			y = Common.ToInt(x, null);
			if (y == null)
			{
				_page.Errors.AddError(name);
				return defaultValue;
			}
			return y;
		}

		// was fmCur
		public decimal? GetDecimal(string name, bool required = false, bool enableNegative = false)
		{
			return GetDecimal(name, _MissingValues.Decimal, required, enableNegative);
		}
		public decimal? GetDecimal(string name, decimal? defaultValue, bool required = false, bool enableNegative = false)
		{
			decimal? y;
			string x = GetString(name);
			if (x == null)
			{
				if (required)
					_page.Errors.AddError(name);
				return defaultValue;
			}
			x = x.Replace("$", "");
			y = Common.ToDecimal(x, null);
			if (y == null)
			{
				_page.Errors.AddError(name);
				return defaultValue;
			}
			if (y < 0 && !enableNegative)
				_page.Errors.AddError(name);
			return y;
		}

		// was fmFloat
		public double? GetSingle(string name, bool required = false)
		{
			return GetSingle(name, _MissingValues.Double, required);
		}
		public double? GetSingle(string name, double? defaultValue, bool required = false)
		{
			double? y;
			string x = GetString(name);
			if (x == null)
			{
				if (required)
					_page.Errors.AddError(name);
				return defaultValue;
			}
			y = Common.ToDouble(x, null);
			if (y == null)
			{
				_page.Errors.AddError(name);
				return defaultValue;
			}
			return y;
		}

		// was fmPercent
		public double? GetPercent(string name, bool required = false)
		{
			return GetPercent(name, _MissingValues.Double, required);
		}
		public double? GetPercent(string name, double? defaultValue, bool required = false)
		{
			double? y;
			string x = GetString(name);
			if (x == null)
			{
				if (required)
					_page.Errors.AddError(name);
				return defaultValue;
			}
			x = x.Replace("%", "");
			y = Common.ToDouble(x, null);
			if (y == null)
			{
				_page.Errors.AddError(name);
				return defaultValue;
			}
			return y.Value / 100;
		}

		// @imported: Use this function to obtain hidden input name for storing client data.
		public string GetClientStateName(string id)
		{
			if (id == null)
				throw new ArgumentNullException("id");

			return id + "__clientState";
		}

		public string GetViewState(string name)
		{
			string s = GetValue(GetClientStateName(name), "");
			if (s == "")
			{
				string x = _page.Common.GetLocal("__viewState$CTX") as string;
				// TODO:
				/*
				 *	var x = Session.Contents(common__local_name__ + "__viewState$CTX");
					if (x && x[name])
						s = x[name];
				*/
				if (!String.IsNullOrEmpty(x))
					s = x;
			}
			return s;
		}   

		// was fmDate
		public DateTime? GetDate(string name, bool required = false)
		{
			return GetDate(name, _MissingValues.DateTime, required);
		}
		public DateTime? GetDate(string name, DateTime? defaultValue, bool required = false)
		{
			string type = GetString(name + ":MASK");
			DateTime x;
			if (type != null && type.Length == 3)
			{
				string value = GetString(name);
				if (!DateTime.TryParse(value, out x))
				{
					if (value != null && required)
					{
						if (defaultValue == _MissingValues.DateTime)
						{
							_page.Errors.AddError(name);
							return null;
						}
						else
						{
							_page.Errors.AddError(name);
							return defaultValue.Value;
						}
					}
					else
					{
						if (defaultValue == _MissingValues.DateTime)
						{
							_page.Errors.AddError(name);
							return null;
						}
						else if (defaultValue != null)
						{
							return defaultValue;
						}
						else if (required)
						{
							_page.Errors.AddError(name);
							return defaultValue;
						}
						else
						{
							return defaultValue;
						}
					}
				}

				if (IsValidDbdate(x))
					return x;
				_page.Errors.AddError(name);
				return (defaultValue != _MissingValues.DateTime) ? defaultValue : null;
			}

			bool d_rq = true;
			bool m_rq = true;
			bool y_rq = true;

			if (type != null)
			{
				d_rq = type.Length == 0 || type.Contains(DateYearSuffix);
				m_rq = type.Length == 0 || type.Contains(DateMonthSuffix);
				y_rq = type.Length == 0 || type.Contains(DateDaySuffix);
			}

			string y = GetString(name + ":" + DateYearSuffix);
			string m = GetString(name + ":" + DateMonthSuffix);
			string d = GetString(name + ":" + DateDaySuffix);
			if ((y == null || !y_rq) && (m == null || !m_rq) && (d == null || !d_rq))
			{
				if (defaultValue == _MissingValues.DateTime || required == true)
					_page.Errors.AddError(name);
				return defaultValue;
			}

			int yNum, mNum, dNum;

			if (!JsCompatibility.TryParseInt(y, out yNum) || !JsCompatibility.TryParseInt(m, out mNum) || !JsCompatibility.TryParseInt(d, out dNum))
			{
				_page.Errors.AddError(name);
				return defaultValue;
			}

			x = new DateTime(yNum, mNum, dNum);
			if (IsValidDbdate(x))
				return x;
			_page.Errors.AddError(name);
			return (defaultValue != _MissingValues.DateTime) ? defaultValue : null;
		}

		// was fmDateRange
		public DateTime?[] GetDateRange(string name, DateTime? defaultFrom, bool requiredFrom, DateTime? defaultTo, bool requiredTo)
		{
			DateTime? dtFrom = GetDate(name + "_from", defaultFrom, requiredFrom);
			DateTime? dtTo = GetDate(name + "_to", defaultTo, requiredTo);

			if (_page.Errors.NoErrors())
			{
				if (dtFrom > dtTo)
				{
					_page.Errors.AddError(name + "_from");
					_page.Errors.AddError(name + "_to");
					_page.Errors.AddError(name);
				}
			}
			else
			{
				_page.Errors.AddError(name);
			}

			return new[] { dtFrom, dtTo };
		}

		// was fmDatePart
		public string GetDatePart(string name, int? dy = null, int? dm = null, int? dd = null, bool required = false)
		{
			if (dy < 1)
				throw new ArgumentOutOfRangeException("dy");
			if (dm < 1 || dm > 12)
				throw new ArgumentOutOfRangeException("dm");
			if (dd < 1 || dd > 31)
				throw new ArgumentOutOfRangeException("dd");

			int? y = dy;
			int tmp;
			if (y == null)
			{
				if (JsCompatibility.TryParseInt(GetString(name + ":" + DateYearSuffix), out tmp))
					y = tmp;
				else
					y = -1;
			}
			else if (y >= 0 && y < 100)		// incomplete year: 85, 05 etc.
			{
				y += dy > convert__Y2DCutOff__ ? 1900 : 2000;
			}

			int? m = dm;
			if (m == null)
			{
				if (JsCompatibility.TryParseInt(GetString(name + ":" + DateMonthSuffix), out tmp))
					m = tmp;
				else
					m = -1;
			}

			int? d = dd;
			if (d == null)
			{
				if (JsCompatibility.TryParseInt(GetString(name + ":" + DateDaySuffix), out tmp))
					d = tmp;
				else
					d = -1;
			}

			if ((y ?? m ?? d) == null)
			{
				if (required)
					_page.Errors.AddError(name);
				return null;
			}

			if (y < 0 || m < 0 || d < 0)
			{
				_page.Errors.AddError(name);
				return null;
			}

			return new DateTime(y.Value, m.Value, d.Value).ToShortDateString();
		}

		// was fmTime
		public double GetTime(string name, bool required = false)
		{
			return GetTime(name, _MissingValues.Double, required);
		}
		public double GetTime(string name, double defaultValue, bool required = false)
		{
			string h = GetString(name + ":" + TimeHourSuffix);
			string m = GetString(name + ":" + TimeMinuteSuffix);
			string s = GetString(name + ":" + TimeSecondSuffix);
			string p = GetString(name + ":" + TimeAmPmSuffix);
			if ((h ?? m ?? s) == null)
			{
				if (defaultValue == _MissingValues.Double || required)
					_page.Errors.AddError(name);
				return defaultValue;
			}

			int h2, m2, s2, p2;
			if (!JsCompatibility.TryParseInt(h, out h2) || !JsCompatibility.TryParseInt(m, out m2) || /*!JsCompatibility.TryParseInt(s, out s2) || */!JsCompatibility.TryParseInt(p, out p2))
			{
				_page.Errors.AddError(name);
				return defaultValue;
			}

			if (p2 > 0)
			{
				if (h2 == 12)
					h2 = 0;
				if (p2 == 2)
					h2 += 12;
			}

			return (h2 * 60 + m2) / (24 * 60);
		}

		// was form__fmSeriesStr__
		public string SeriesString(string name, int[] len, string pad, string delim, string defaultValue, bool required = false)
		{
			bool padLeft;
			if (pad.StartsWith(">"))
			{
				padLeft = true;
				pad = pad.Substring(1);
			}
			else if (pad.StartsWith("<"))
			{
				padLeft = true;
				pad = pad.Substring(1);
			}
			else
			{
				padLeft = false;
			}

			StringBuilder s = new StringBuilder();
			bool empty = true;
			for (int i = 0; i < len.Length; ++i)
			{
				string x = GetString(name + ":" + (i + 1).ToString());
				if (x == null)
				{
					x = "";
				}
				else
				{
					if (len[i] >= 0)
						empty = false;
					else
						len[i] = -len[i];
				}

				if (x.Length != len[i])
				{
					if (JsCompatibility.HasValue(pad) && len[i] > 0)
					{
						for (int j = x.Length; j < len[i]; j += pad.Length)
						{
							if (padLeft)
								x = pad + x;
							else
								x += pad;
						}

						if (x.Length > len[i])
						{
							if (padLeft)
								x = x.Substring(x.Length - len[i]);
							else
								x = x.Substring(0, len[i]);
						}
					}
				}

				s
					.Append(x)
					.Append(delim);
			}

			if (empty)
			{
				if (required)
					_page.Errors.AddError(name);
				return defaultValue;
			}
			return s.ToString();
		}

		// was fmPhone
		public string GetPhone(string name, string defaultValue, bool required = false)
		{
			return SeriesNumOnly(name, new[] { 3, 3, 4 }, defaultValue, required);
		}

		// was fmEIN
		public string GetEin(string name, string defaultValue, bool required = false)
		{
			return SeriesNumOnly(name, new[] { 2, 7 }, defaultValue, required);
		}

		// was fmSsn
		public string GetSsn(string name, int defaultValue, bool required = false)
		{
			return GetSsn(name, defaultValue.ToString(System.Globalization.NumberFormatInfo.InvariantInfo), required);

		}
		public string GetSsn(string name, string defaultValue, bool required = false)
		{
			string format = GetString(name + ":format");
			int[] len = (format != null && format.ToUpperInvariant().Contains("LONG"))
				? new[] { 3, 2, 4 }
				: new[] { 4 };

			string s = SeriesNumOnly(name, len, defaultValue, required);
			if (s.Length == len.Sum() || s.Length == 0)
				return s;
			_page.Errors.AddError(name);

			return null;
		}

		// was fmCmNumber
		public string GetCmNumber(string name, string defaultValue, bool required = false)
		{
			return SeriesNumOnly(name, new[] { 4, 4, 4, 4, 4 }, defaultValue, required);
		}

		// was fmZip
		public string GetZip(string name, string defaultValue, bool required = false)
		{
			return SeriesNumOnly(name, new[] { 5, -4 }, defaultValue, required);
		}

		// was fmPressed
		public bool IsPressed(string name)
		{
			return (GetValue(name) ?? GetValue(name + ".x")) != null;
		}

		// was fmClick
		public bool IsClicked(string name)
		{
			return IsPressed(name);
		}

		// TODO: originally was located in convert.asp
		private const int convert__Y2DCutOff__ = 49;

		private const string DateYearSuffix = "Y";
		private const string DateMonthSuffix = "M";
		private const string DateDaySuffix = "D";
		private const string TimeHourSuffix = "H";
		private const string TimeMinuteSuffix = "N";
		private const string TimeSecondSuffix = "S";
		private const string TimeAmPmSuffix = "A";


		#region Implementation
		private bool IsValidDbdate(DateTime value)
		{
			return value >= MinDBDate;
		}
		private static readonly DateTime MinDBDate = new DateTime(1753, 1, 1);

		private string SeriesNumOnly(string name, int[] len, string defaultValue, bool required = false)
		{
			string x = SeriesString(name, len, "", "", defaultValue, required);
			if (x != defaultValue)
			{
				for (int i = 0; i < x.Length; ++i)
				{
					if (!Char.IsDigit(x, i))
					{
						_page.Errors.AddError(name);
						return defaultValue;
					}
				}
			}
			return x;
		}

		#region Nested types
		private static class _MissingValues
		{
			public static readonly DateTime DateTime = DateTime.MinValue;
			public static readonly int Int32 = Int32.MinValue;
			public static readonly double Double = Double.NaN;
			public static readonly string String = null;
			public static readonly Decimal Decimal = Decimal.MinValue;
		}
		#endregion
		#endregion
	}
}
