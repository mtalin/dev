﻿using FS.Web.Compatibility;
using Lexxys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vb6;

namespace FS.Web.Core
{
	public enum LoginMethod
	{
		Login,
		Close
	}

	public sealed class Common : IRequireInit
	{
		public const string SESSION_HTMLBODY = "__PRINTR_CONTENT__";

		private string _activityLog;
		private const string Empty = "---";

		private readonly PageCommon _page;
		private string _localName;
		private bool _hrefRedirctMode = false;

		internal Common(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public LoginMethod LoginMethod { get; set; }

		public string LocalName
		{
			get { return _localName; }
		}

		public string ActivityLog
		{
			get { return _activityLog; }
		}

		public bool IsPopup
		{
			get
			{
				return LoginMethod == LoginMethod.Close;
			}
		}

		#region Locals Management
		public void SetLocal(string name, object value)
		{
			if (value == null)
				RemoveLocal(name);
			else
				_page.Session.Add(MakeSessionKey(name), value);
		}

		public object GetLocal(string name)
		{
			return _page.Session[MakeSessionKey(name)];
		}

		public void RemoveLocal(string name)
		{
			_page.Session.Remove(MakeSessionKey(name));
		}

		public void RemoveLocalByUrl(string name, string url)
		{
			_page.Session.Remove(GetFullQualifiedName(url) + name);
		}
		#endregion

		public static bool tQ(object value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(string value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(short value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(int value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(long value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(ushort value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(uint value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(ulong value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(decimal value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(double value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(float value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(bool value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		public static bool tQ(DateTime value)
		{
			return JsCompatibility.ToBoolean(value);
		}

		public static bool tN(object value)
		{
			return !tQ(value);
		}
		public static bool tN(string value)
		{
			return !tQ(value);
		}
		public static bool tN(short value)
		{
			return !tQ(value);
		}
		public static bool tN(int value)
		{
			return !tQ(value);
		}
		public static bool tN(long value)
		{
			return !tQ(value);
		}
		public static bool tN(ushort value)
		{
			return !tQ(value);
		}
		public static bool tN(uint value)
		{
			return !tQ(value);
		}
		public static bool tN(ulong value)
		{
			return !tQ(value);
		}
		public static bool tN(decimal value)
		{
			return !tQ(value);
		}
		public static bool tN(double value)
		{
			return !tQ(value);
		}
		public static bool tN(float value)
		{
			return !tQ(value);
		}
		public static bool tN(bool value)
		{
			return !tQ(value);
		}
		public static bool tN(DateTime value)
		{
			return !tQ(value);
		}

		public static object tIf(object x, object a, object b)
		{
			return tQ(x) ? a : b;
		}
		public static object tIf(bool x, object a, object b)
		{
			return x ? a : b;
		}
		public static T tIf<T>(object x, T a, T b)
		{
			return tQ(x) ? a : b;
		}
		public static T tIf<T>(bool x, T a, T b)
		{
			return x ? a : b;
		}

		public static object tE(object a, object b)
		{
			return tQ(a) ? a : b;
		}
		public static T tE<T>(T a, T b)
		{
			return tQ(a) ? a : b;
		}

		public void CloseMe()
		{
			using (var writer = new HtmlWriter(_page.Writer))
			{
				writer
					.Tag("html")
						.Attr("onload", "window.close();")
						.Tag("body")
						.End()
					.End();
			}
		}

		public static string oStr(object value, string defaultValue = "")
		{
			defaultValue = defaultValue ?? "";

			string str = JsCompatibility.ToString(value).Trim();
			return (str == "") ? defaultValue : HtmlUtility.HtmlEncode(str);
		}

		public static string oE()
		{
			return Empty;
		}

		public static string oNum(object x, int? d, string e = null)
		{
			if (e == null)
				e = Empty;
			if (d == null)
				d = 0;

			double? result = Common.ToDouble(x, null);
			return result != null
				? Strings.FormatNumber(result.Value, d.Value, Ternary.True, Ternary.False, Ternary.True)
				: e;
		}

		public static string oCur(object x, int? d, string e = null)
		{
			if (e == null)
				e = Empty;
			if (d == null)
				d = 2;

			decimal? result = Common.ToDecimal(x, null);
			return result != null
				? Strings.FormatCurrency(result.Value, d.Value, Ternary.True, Ternary.True, Ternary.True)
				: e;
		}

		public static string oDate(object x)
		{
			return null;//ap.format(x, "mm/dd/yyyy");
		}

		public string oPopup(string href, string text, params string[] args)
		{
			href = _page.Href.LegalHref(href);
			href = UrlUtility.SetQueryParam(href, "_lay", "popup", replaceExisting: true);
			StringBuilder strBuilder = new StringBuilder()
				.Append("openWindow(\"")
				.Append(href)
				.Append("\"");

			if (args != null && args.Length > 0)
			{
				for (int i = 0; i < args.Length; i++)
				{
					strBuilder
						.Append(", ")
						.Append(args[i]);
				}
			}
			strBuilder.Append(")");

			using (var writer = new HtmlWriter())
			{
				writer
					.Attr("href", UrlUtility.MakeJScriptUrl(strBuilder.ToString()))
					.Tag("a")
						.Write(text)
					.End();

				return writer.GetContent();
			}
		}

		public string oHref(string url, string text, params object[] args)
		{
			if (url == null)
				url = "#";
			bool back = true;
			bool hrefMode = _hrefRedirctMode;
			using (var writer = new HtmlWriter())
			{
				if (args != null && args.Length > 0)
				{
					for (int i = 0; i < args.Length; i++)
					{
						if (JsCompatibility.ToBoolean(args[i]))
						{
							string arg = JsCompatibility.ToString(args[i]);
							if (!arg.Contains("="))
							{
								switch (arg.ToUpperInvariant())
								{
									case "NOBACK":
										back = false;
										break;
									// @imported: a hit on the link should NOT cause updating of outer update panel if any exists
									case "NOUPDATE":
										writer.Attr("rel", "noupdate");
										break;
									default:
										writer.Attr("title", arg);
										break;
								}
							}
							else
							{
								writer.Attr(arg);
								if (arg.ToUpperInvariant().Contains("ONCLICK="))
									hrefMode = false;
							}
						}
					}
				}

				string href;
				bool isJsHref = UrlUtility.IsJScriptUrl(url);
				if (isJsHref)
					href = url;
				else
					href = back ? _page.Href.LegalHref(url) : _page.Href.LegalHrefNoBack(url);
				writer.Attr("href", href);

				if (hrefMode && !isJsHref)
					writer.Attr("onclick", "return $href.call(this);");

				writer
					.Tag("a")
						.Write(text)
					.End();

				return writer.GetContent();
			}
		}

		public string oHrefDoc(string id, string text, params string[] args)
		{
			string type = "";
			string mode = "";
			using (var writer = new HtmlWriter())
			{
				for (int i = 0; i < args.Length; i++)
				{
					if (args[i].StartsWith("type="))
						type = args[i].Substring(5);
					else if (args[i].StartsWith("mode="))
						mode = args[i].Substring(5);
					else
					{
						writer.Attr(args[i]);
					}
				}

				writer
					.Attr("target", "_blank")
					.Attr("href", oHrefView(id, type, mode))
					.Tag("a")
						.WriteEncoded(text)
					.End();

				return writer.GetContent();
			}
		}

		public string oHr(params string[] args)
		{
			string h0 = "5";
			string h1 = "1";
			string h2 = "5";
			string wd = "100%";
			string cl = "HrBackground";
			string cs = "";
			if (args != null && args.Length > 0)
			{
				for (int i = 0; i < args.Length; i++)
				{
					var attrInfo = HtmlUtility.ParseAttribute(args[i]);
					if (attrInfo != null)
					{
						switch (attrInfo.Item1.ToUpperInvariant())
						{
							case "SIZE":
							case "HEIGHT":
								h1 = attrInfo.Item2;
								break;
							case "WIDTH":
								wd = attrInfo.Item2;
								break;
							case "SPACEBEFORE":
								h0 = attrInfo.Item2;
								break;
							case "SPACEAFTER":
								h2 = attrInfo.Item2;
								break;
							case "LINECLASS":
								cl = attrInfo.Item2;
								break;
							case "SPACECLASS":
								cs = attrInfo.Item2;
								break;
							default:
								break;
						}
					}
				}
			}

			using (var writer = new HtmlWriter())
			{
				writer
					.Attr("border", "0")
					.Attr("cellspacing", "0").Attr("cellpadding", "0")
					.Attr("width", wd)
					.Tag("table");
				int temp;
				if (JsCompatibility.TryParseInt(h0, out temp) && temp > 0)
				{
					writer
						.Tag("tr")
							.Attr("height", h0)
							.Attr("class", cs)
							.Tag("td")
								.Img(_page.GetImageUrl("spacer.gif"), width: 1, height: temp)
							.End()
						.End();
				}
				if (JsCompatibility.TryParseInt(h1, out temp) && temp > 0)
				{
					writer
						.Tag("tr")
							.Attr("height", h1)
							.Attr("class", cl)
							.Tag("td")
								.Img(_page.GetImageUrl("spacer.gif"), width: 1, height: temp)
							.End()
						.End();
				}
				if (JsCompatibility.TryParseInt(h2, out temp) && temp > 0)
				{
					writer
						.Tag("tr")
							.Attr("height", h2)
							.Attr("class", cs)
							.Tag("td")
								.Img(_page.GetImageUrl("spacer.gif"), width: 1, height: temp)
							.End()
						.End();
				}
				writer.End("table");

				return writer.GetContent();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id">Document id</param>
		/// <param name="type">Document type (document/manager/publication/cmsevent/cmsbrochure/fapbrochure)</param>
		/// <param name="mode">Open mode (0 - view, 1 - download, 2 - convert to PDF, 8 - replace images to script, 12 - replace images to file path, -1 - undefined)</param>
		/// <returns></returns>
		public string oHrefView(string id, string type, string mode)
		{
			string url = "utils/view.asp?i=" + id;			// TODO: url
			if (JsCompatibility.HasValue(type))
				UrlUtility.SetQueryParam(url, "t", type);
			if (JsCompatibility.HasValue(mode))
				UrlUtility.SetQueryParam(url, "m", mode);
			_page.Href.AddLegalTarget(url);
			return url;
		}

		private const string PubInSessionKey = "_PublicInspection";
		// New method. should be used in "Admin/PublicInspection/index.asp"
		public void SetPublicInspectionMode(bool enabled)
		{
			if (enabled)
				_page.Session[PubInSessionKey] = PubInSessionKey;
			else
				_page.Session.Remove(PubInSessionKey);
		}
		public bool IsPublicInspectionMode()
		{
			return (string)_page.Session[PubInSessionKey] == PubInSessionKey;
		}


		internal static int? ToInt(object x, int? defaultValue = null)
		{
			try
			{
				return Convert.ToInt32(x);
			}
			catch (OverflowException)
			{
				return defaultValue;
			}
			catch (FormatException)
			{
				return defaultValue;
			}
			catch (InvalidCastException)
			{
				return defaultValue;
			}
		}

		internal static double? ToDouble(object x, double? defaultValue = null)
		{
			try
			{
				return Convert.ToDouble(x);
			}
			catch (OverflowException)
			{
				return defaultValue;
			}
			catch (FormatException)
			{
				return defaultValue;
			}
			catch (InvalidCastException)
			{
				return defaultValue;
			}
		}

		internal static decimal? ToDecimal(object x, decimal? defaultValue = null)
		{
			try
			{
				return Convert.ToDecimal(x);
			}
			catch (OverflowException)
			{
				return defaultValue;
			}
			catch (FormatException)
			{
				return defaultValue;
			}
			catch (InvalidCastException)
			{
				return defaultValue;
			}
		}


		#region Implementation
		private const string ActivityLogSessionKey = "ActivityLog";
		public void Init()
		{
			/* /utils/common.asp */

			_localName = GetFullQualifiedName(_page.MyUrl);

			LoginMethod = (_page.Request["_lay"] == "popup") ? LoginMethod.Close : LoginMethod.Login;

			/* /admin/utils/common.asp */
			
			//TODO: just for url query testing purposes. Uncomment it.
			if (!_page.Href.LegalReferer())
			{
				if (LoginMethod == LoginMethod.Close)
					_page.ForceClose();
				else
					_page.Redirect("home");
			}

			_activityLog = _page.Session[ActivityLogSessionKey] as string;

			// TODO: ap.login "load", getSessionID()
		}

		private static string GetFullQualifiedName(string url)
		{
			return "__" + url.ToLowerInvariant() + "|";
		}

		private string MakeSessionKey(string name)
		{
			return _localName + name;
		}
		#endregion
	}
}
