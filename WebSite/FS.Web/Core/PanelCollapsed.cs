﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class PanelCollapsed : PanelBase
	{
		private int _columns;
		private int _cspan;
		private int _rows;
		private bool _expanded;
		private string _scriptKey;
		private string _script;

		internal PanelCollapsed(PageCommon page)
			: base(page)
		{
			A0.Extend(new Dictionary<string, string>()
				{
					{ "Class", null },
					{ "Width", null },
					{ "Line", null },
					{ "Space", null },
					{ "Title", null },
					{ "CellClass", "SmallDarkFont" },
					{ "Background", "NormalBackground" },
					{ "Cols", null },
					{ "Colspan", null },
					{ "TitleClass", "SmallBoldLightFont" },
					{ "TitleBackground", "TitleBackground" },
					{ "Groupclass", "SmallBoldDarkFont" },
					{ "GroupBackground", "Stripe3Background" }
				});
		}

		public override string Name
		{
			get { return "panel-collapsed"; }
		}

		protected override void BeginCore(params string[] args)
		{
			dynamic parms = A0;
			A0.Parse(args);

			if (!JsCompatibility.TryParseInt(parms.Cols, out _columns))
				_columns = 1;

			HtmlWriter writer = Writer;
			writer.Attr(args);
			if (!writer.HasAttributeInBuffer("cellspacing"))
				writer.Attr("cellspacing", "0");
			if (!writer.HasAttributeInBuffer("cellpadding"))
				writer.Attr("cellpadding", "0");

			PrepareStartupScript();

			writer
				.Attr("border", "0")
				.Attr(A0.Attribute("width"))
				.Attr(A0.Attribute("background", "class"));
			if (_columns > 1)
				writer.Attr("cols", _columns.ToString());
			if (writer.HasAttributeInBuffer("noborder"))
			{
				writer
					.AttrStyle("border", "#30569D")
					.AttrStyle("padding", "2px");
			}
			else
			{
				writer
					.AttrStyle("border", "#30569D 1px solid")
					.AttrStyle("padding", "3px");
			}
			writer.Tag("table");

			if (Has(parms.Title))
			{
				writer
					.Attr(A0.Attribute("titlebackground", "class"))
					.Tag("tr")
						.Attr("align", "center")
						.Attr(A0.Attribute("titleclass"));
				if (_columns > 1)
					writer.Attr("colspan", _columns.ToString());
				writer
						.Tag("td")
							.Write(parms.Title)
						.End()
					.End();
			}
		}

		protected override void EndCore()
		{
			Writer.End("table");

			Page.ClientScript.RegisterBlock(_scriptKey, _script);
		}

		protected override void RowCore(params string[] args)
		{
			dynamic parms0 = A0;
			dynamic parms = A1 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "Class", null }
				}, A0);
			A1.ParseWithLog(this, args);
			HtmlWriter writer = Writer;
			writer.Attr(args);
			parms.Colspan = null;

			PanelHLine(parms.Line, parms.Space, _columns, A1.Attribute("class"));

			bool expandable = true;
			_expanded = writer.HasAttributeInBuffer("expanded");

			if (writer.HasAttributeInBuffer("no_content"))
			{
				expandable = _expanded = false;
			}

			parms.Row = _rows++.ToString();

			writer
				.Attr(A0.Attribute("class"))
				.Tag("tr");

			if (tQ(parms.Title))
			{
				string link = (expandable)
					?
						HtmlUtility.BeginTag("a", HtmlUtility.Attribute("href", UrlUtility.MakeJScriptUrl("toggle_panel(" + parms.Row + ")"))) +
							HtmlUtility.Img(Page.GetImageUrl(_expanded ? "minus.gif" : "plus.gif"), width: 13, height: 13, id: IconIdPrefix + parms.Row) +
						HtmlUtility.EndTag("a")
					: Page.FormOut.Spacer(13, 13);

				if (_columns > 1)
					writer.Attr("colspan", _columns.ToString());
				writer
					.Attr("width", "100%")
					.Tag("td")
						.Attr("width", "100%")
						.Attr("border", "0")
						.Attr("cellspacing", "0").Attr("cellpadding", "5")
						.Tag("table")
							.Attr(A1.Attribute("groupbackground", "class"))
							.Tag("tr")
								.Attr("align", "center")
								.Attr("width", "1%")
								.Tag("td")
									.Write(link)
								.End()
								.Attr(A1.Attribute("groupclass", "class"))
								.Attr("width", "99%")
								.Tag("td")
									.Write(parms.Title)
								.End()
							.End()
						.End()
					.End();
			}

			SetRowEnding(() => writer.End("tr"));
			SetCellEnding(null);

			_cspan = 1;
		}

		protected override void CellCore(params string[] args)
		{
			dynamic parms = A2 = new AttributesBag(A1);
			A1.ParseWithLog(this, args);

			// _cspan - 1 because CellId is already incremented by 1
			CellId += (_cspan - 1);
			if (CellId > _columns)
				_columns = CellId;

			HtmlWriter writer = Writer;
			if (tQ(parms.Colspan))
			{
				_cspan = JsCompatibility.ParseInt(parms.Colspan);
				writer.Attr("colspan", parms.Colspan);
			}
			else
			{
				_cspan = 1;
			}

			CssClass = A2.Attribute("class", null, parms.CellClass);
			writer
				.Attr(CssClass)
				.Attr("id", PanelIdPrefix + parms.Row)
				.AttrStyle("display", _expanded ? "display" : "none")
				.Tag("tr")
					.Attr(args);
			if (tQ(parms.Colspan))
			{
				writer
					.Attr("colspan", parms.Colspan);
			}

			writer
					.Attr(A2.Attribute("width"))
					.Tag("td");

			SetCellEnding(() => writer.End("td").End("tr"));
		}


		private void PrepareStartupScript()
		{
			_script =
@"function toggle_panel(id) {
	var $panel = $('#" + PanelIdPrefix + @"' + id);
	var $img = $('#" + IconIdPrefix + @"' + id);
	if ($panel.length == 0 || $img.length == 0)
		return;

	if ($panel.css('display') == 'none')
	{
		$img.attr('src', '" + Page.GetImageUrl("minus.gif") + @"');
		$panel.show();
	}
	else
	{
		$img.attr('src', '" + Page.GetImageUrl("plus.gif") + @"');
		$panel.hide();
	}
}";

			_scriptKey = "PanelCollapsed__behaviour";
		}

		private const string PanelIdPrefix = "panel-collapsed-";
		private const string IconIdPrefix = "panel-collapsed-icon-";
	}
}
