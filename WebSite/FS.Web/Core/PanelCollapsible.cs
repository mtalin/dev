﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FS.Web.Compatibility;

namespace FS.Web.Core
{
	public sealed class PanelCollapsible : PanelBase
	{
		private string _clientStateFieldId;
		private string _startupScript;
		private string _startupScriptKey;
		private string _id;

		private const string CollapsedToken = "col";
		private const string ExpandedToken = "exp";

		private bool _isExpanded = false;

		internal PanelCollapsible(PageCommon page)
			: base(page)
		{
			A0.Extend(new Dictionary<string, string>()
				{
					{ "ID", null },
					{ "IconUrl", null },
					{ "IconTitle", null },
					{ "nospacer", null },
					{ "noAnimation", null },
					{ "simple", null },					// @imported: (deprecated). Use "noAnimation"
					{ "imagealign", "right" },
					{ "animSpeed", "fast" },
					{ "title", null },
					{ "titleCssClass", null },
					{ "description", null },
					{ "descriptionCssClass", null },
					{ "expanded", null },
					{ "nostate", null },

					{ "collapsedText", "Expand" },
					{ "expandedText", "Collapse" }
				});
		}

		public override string Name
		{
			get { return "panel-collapsible"; }
		}

		protected override void BeginCore(params string[] args)
		{
			dynamic parms = A0;
			A0.ParseWithLog(this, args);

			_id = tE(parms.Id, "PanelCollapsible" + PanelId.ToString());
			_clientStateFieldId = Page.FormIn.GetClientStateName(_id);
			_isExpanded = IsExpanded(tQ(parms.Expanded));

			PrepareStartupScript();

			HtmlWriter writer = Writer;
			if (!Has(parms.NoState))
			{
				writer
					.Hidden(_clientStateFieldId, "")
					.Hidden("__viewState$CTX", _id);
			}

			writer
				.Attr("id", _id)
				.Attr("class", "ui-accordion", eQ(parms.ImageAlign, "left") ? "reversedIcon" : "")
				.AttrStyle("margin-bottom", Has(parms.NoSpacer) ? "0" : "10px")
				.Tag("div")
					.Attr("class", "ui-accordion-header")
					.Tag("h3");

			if (Has(parms.IconUrl))
			{
				writer
						.Attr("src", parms.IconUrl)
						.Attr("alt", parms.IconTitle)
						.Attr("title", parms.IconTitle)
						.AttrStyle("border", "none")
						.TagSingle("img");
			}

			writer
						.Attr("href", "#")
						.AttrStyle("text-align", "left")
						.Attr("class", parms.titleCssClass)
						.Tag("a")
							.Write(parms.Title)
						.End();

			if (tQ(parms.Description))
			{
				writer
						.Attr("class", "desc", parms.DescriptionCssClass)
						.Tag("div")
							.Write(parms.Description)
						.End();
			}

			writer
						.Attr("class", "ui-icon")
						.AttrStyle("float", "right")
						.Tag("span")
						.End()
					.End("h3")
					.Attr("class", "ui-accordion-content")
					.Tag("div");
		}

		//----------------------------------------
		// FOR PANEL-ACCORDION
		//----------------------------------------
		//protected override void RowCore(params string[] args)
		//{
		//	dynamic parms = A1 = new AttributesBag(new Dictionary<string, string>()
		//		{
		//			{ "title", "Accordion " + GroupId.ToString() },
		//			{ "align", "left" }
		//		});

		//	A1.ParseLogged(this, args);

		//	HtmlWriter writer = Writer;
		//	writer
		//		.Tag("div")
		//			.Tag("h3")
		//				.Attr("href", "#")
		//				.AttrStyle("text-align", parms.Align)
		//				.Tag("a")
		//					.Write(parms.Title)
		//				.End()
		//			.End();

		//	SetRowEnding(() => writer.End("div"));
		//	SetCellEnding(null);
		//}

		//protected override void CellCore(params string[] args)
		//{
		//	dynamic parms = A2 = new AttributesBag(new Dictionary<string, string>()
		//		{
		//			{ "cssClass", null }
		//		});

		//	A2.ParseLogged(this, args);

		//	HtmlWriter writer = Writer;
		//	writer
		//		.AttrStyle("font-size", "100%")
		//		.Tag("div");

		//	SetCellEnding(() => writer.End("div"));
		//}

		protected override void RowCore(params string[] args)
		{
			; // Nothing to do
		}

		protected override void CellCore(params string[] args)
		{
			; // Nothing to do;
		}

		protected override void EndCore()
		{
			Writer
					.End("div")
				.End("div");

			if (!IsRecording)
				Page.ClientScript.RegisterStartup(_startupScriptKey, _startupScript);
		}

		protected internal override void OnPlayRecording(RecordedPanel record)
		{
			base.OnPlayRecording(record);

			dynamic rec = record;
			if (Has(rec.Params))
			{
				Dictionary<string, string> parms = rec.Params;
				foreach (var item in parms)
				{
					_startupScript = _startupScript.Replace(item.Key, item.Value);
					_startupScriptKey = _startupScriptKey.Replace(item.Key, item.Value);
				}

				Page.ClientScript.RegisterStartup(_startupScriptKey, _startupScript);

				PrepareStartupScript();
			}
		}

		private void PrepareStartupScript()
		{
			Page.ClientScript.RequireJQuery(true);

			dynamic parms0 = A0;
			string clientInstance = _id + "__behavior";

			StringBuilder script = new StringBuilder();
			script
				.AppendFormat("var {0} = new $FS.Asp.PanelCollapsible($FS.Asp.PanelCollapsibleState.{1})", clientInstance, _isExpanded ? "Expanded" : "Collapsed")
				.AppendLine(";")
				.AppendFormat("{0}.WrapperId = ", clientInstance).JValue(_id)
				.AppendLine(";");

			if (!Has(parms0.NoState))
			{
				script
					.AppendFormat("{0}.ClientStateFieldId = ", clientInstance).JValue(_clientStateFieldId)
					.AppendLine(";");
			}

			if (Has(parms0.NoAnimation) || Has(parms0.Simple))
			{
				script
					.AppendFormat("{0}.SlideSpeed = ", clientInstance).JValue("none")
					.AppendLine(";");
			}
			else if (Has(parms0.AnimSpeed))
			{
				script
					.AppendFormat("{0}.SlideSpeed = ", clientInstance).JValue((string)parms0.AnimSpeed)
					.AppendLine(";");
			}

			if (Has(parms0.CollapsedText))
			{
				script
					.AppendFormat("{0}.CollapsedText = ", clientInstance).JValue((string)parms0.CollapsedText)
					.AppendLine(";");
			}

			if (Has(parms0.ExpandedText))
			{
				script
					.AppendFormat("{0}.ExpandedText = ", clientInstance).JValue((string)parms0.ExpandedText)
					.AppendLine(";");
			}

			script
				.AppendFormat("{0}.Init()", clientInstance)
				.AppendLine(";");

			_startupScript = script.ToString();
			_startupScriptKey = clientInstance;
		}

		private bool IsExpanded(bool defaultValue)
		{
			string value = Page.FormIn.GetViewState(_id);
			return value.Equals(ExpandedToken, StringComparison.OrdinalIgnoreCase)
				? true
				: value.Equals(CollapsedToken, StringComparison.OrdinalIgnoreCase)
					? false
					: defaultValue;
		}
	}
}
