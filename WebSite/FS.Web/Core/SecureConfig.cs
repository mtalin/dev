﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace FS.Web.Core
{
	public sealed class SecureConfig : IRequireInit
	{
		private const string FoundationSourceServerName = "foundationsource";
		private const string SessionKey = "SECURE__Config";
		private const char FlagYes = '1';
		private const char FlagNo = '0';
		private const char FlagUnknown = '?';
		private bool _secured;
		private string _serverName;
		private bool _onHttps;
		private string _http;
		private string _https;
		private bool _sslUnit;
		private bool _initialized;
		private readonly PageCommon _page;

		internal SecureConfig(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public bool PageSecured
		{
			get
			{
				return _secured;
			}
		}

		public void Initialize()
		{
			HttpSessionState session = _page.Session;
			bool met128 = false;
			string value = session[SessionKey] as string;
			if (!String.IsNullOrEmpty(value))
			{
				met128 = value[0] == FlagYes;
				_secured = value[1] == FlagYes;
				_sslUnit = value[2] == FlagYes;
				_serverName = value.Substring(3);
			}
			else
			{
				_serverName = _page.Request.ServerVariables["SERVER_NAME"].Trim().ToLowerInvariant();
				_secured = false; // ap.configValueBool("security/@useHttps")
				met128 = _sslUnit = false; // ap.configValueBool("security/@sslUnit")
				value = (_sslUnit ? FlagYes : FlagUnknown).ToString() + (_secured ? FlagYes : FlagNo).ToString() + (_sslUnit ? FlagYes : FlagNo).ToString() + _serverName;
				session[SessionKey] = value;
			}

			_http = UrlUtility.ChangeScheme(_serverName, UrlUtility.SchemeHttp);
			if (_secured)
				_https = UrlUtility.ChangeScheme(_serverName, UrlUtility.SchemeHttps);
			else
				_https = _http;

			if (_sslUnit)
			{
				_onHttps = _page.Request.ServerVariables["HTTP_Front-End-Https"].Equals("on", StringComparison.OrdinalIgnoreCase);
				met128 = true;
			}
			else
			{
				_onHttps = false;
			}
			if (!_onHttps)
				_onHttps = _page.Request["HTTPS"].Equals("on", StringComparison.OrdinalIgnoreCase);

			if (!met128)
			{
				if (_onHttps)
				{
					int n;
					Int32.TryParse(_page.Request.ServerVariables["HTTPS_KEYSIZE"], out n);
					if (n >= 128)
					{
						session[SessionKey] = FlagYes + value.Substring(1);
					}
					else
					{
						session.Abandon();
						// TODO: Response.Redirect secure__Http__ & "/securityerror.asp"
					}
				}
			}
			_initialized = true;
		}

		/// <summary>
		/// @imported: force current page to be under HTTPS.
		/// </summary>
		public void SecureMe()
		{
			if (!_initialized)
				throw new InvalidOperationException("Call \"Initialize\" method first.");

			if (_secured && !_onHttps)
			{
				_page.Session.Abandon();
				// Response.Redirect secure__HttpS__ & getMyUrl()
			}
		}

		/// <summary>
		/// @imported: move to HTTPS or HTTP depend to currenct server configuration.
		/// </summary>
		public void KeepSecured()
		{
			if (!_initialized)
				throw new InvalidOperationException("Call \"Initialize\" method first.");

			if (_secured ^ _onHttps)
			{
				_page.Session.Abandon();
				if (_onHttps)
					; // Response.Redirect secure__Http__ & getMyUrl()
				else
					; // Response.Redirect secure__HttpS__ & getMyUrl()
			}
		}

		/// <summary>
		/// @imported: returns "https://sever_name" & REF(url) for secured websites or "http://sever_name" & REF(url) for unsecured ones.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public string HTTPS_REF(string url)
		{
			return UrlUtility.ChangeScheme(url, UrlUtility.SchemeHttps);
		}

		/// <summary>
		/// @imported: always returns "http://sever_name" & REF(url)
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public string HTTP_REF(string url)
		{
			return UrlUtility.ChangeScheme(url, UrlUtility.SchemeHttp);
		}

		public void SetCookie(string name, string value)
		{
			HttpResponse response = _page.Response;
			response.Cookies.Add(new HttpCookie(name, value) { Path = "/", Secure = PageSecured });
		}


		#region Implementation
		private void CheckWww()
		{
			if (_serverName.StartsWith(FoundationSourceServerName + "."))
			{
				string url = _page.Request["QUERY_STRING"];
				if (!String.IsNullOrEmpty(url))
					url = UrlUtility.ChangeQuery(_page.Request["URL"], url);
				else
					url = _page.Request["URL"];
				_page.Session.Abandon();
				// TODO: Response.Redirect tIF(secure__OnHttps__, "https://", "http://") + "www." + secure__ServerName__ + s
			}
		}

		public void Init()
		{
			Initialize();
			CheckWww();
		}
		#endregion
	}
}
