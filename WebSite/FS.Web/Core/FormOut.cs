﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class FormOut
	{
		public const string FormIdLocalName = "_form_id";
		private const int DefaultStartDate = 1999;

		#region Images map
		private static readonly Dictionary<string, ImageInfo> _images = new Dictionary<string, ImageInfo>(StringComparer.OrdinalIgnoreCase)
		{
			/* Name							ButtonTitle							ButtonWidth		Flags					ImageUrl							Width	Height	Title					*/
			{ "add",						new ImageInfo("Add",				60,				ImageType.Button,		"",									0,		0,		"Add")					},
			{ "add.more",					new ImageInfo("Add More",			80,				ImageType.Button,		"",									70,		0,		"Add More")				},
			{ "approve",					new ImageInfo("Approve",			70,				ImageType.Button,		"",									70,		0,		"Approve")				},
			{ "arrow.dn",					new ImageInfo("",					0,				ImageType.Button,		"arrow_dn.gif",						8,		10,		"*")					},
			{ "arrow.up",					new ImageInfo("",					0,				ImageType.Button,		"arrow_up.gif",						8,		10,		"*")					},
			{ "move.dn",					new ImageInfo("",					0,				ImageType.Button,		"down.gif",							13,		13,		"Move Down")			},
			{ "move.up",					new ImageInfo("",					0,				ImageType.Button,		"up.gif",							13,		13,		"Move Up")				},
			{ "addNotes",					new ImageInfo("Note",				0,				ImageType.Button,		"addnotes.gif",						14,		16,		"Add Note")				},
			{ "arrowAlert",					new ImageInfo("",					0,				ImageType.Button,		"arrowalert.gif",					10,		11,		"Alert")				},
			{ "asterisk",					new ImageInfo("",					0,				ImageType.Button,		"im_asterisk.gif",					5,		10,		"Required")				},
			{ "attention",					new ImageInfo("",					0,				ImageType.Button,		"icon_attention.gif",				16,		16,		"Attention!")			},
			{ "backstep",					new ImageInfo("< Back",				70,				ImageType.Button,		"",									0,		0,		"Go Back")				},
			{ "cancel",						new ImageInfo("Cancel",				70,				ImageType.Button,		"",									70,		0,		"Cancel")				},
			{ "charity.verified",			new ImageInfo("",					0,				ImageType.Button,		"icon_ok.gif",						21,		21,		"")						},
			{ "charity.verified.small",		new ImageInfo("",					0,				ImageType.Button,		"icon_ok_small.gif",				14,		14,		"")						},
			{ "charity.unverified",			new ImageInfo("",					0,				ImageType.Button,		"icon_ok_lite.gif",					21,		21,		"")						},
			{ "charity.unverified.small",	new ImageInfo("",					0,				ImageType.Button,		"icon_ok_lite_small.gif",			14,		14,		"")						},
			{ "charity.problem",			new ImageInfo("",					0,				ImageType.Button,		"icon_stop.gif",					21,		21,		"")						},
			{ "charity.problem.small",		new ImageInfo("",					0,				ImageType.Button,		"icon_stop_small.gif",				14,		14,		"")						},
			{ "check0",						new ImageInfo("",					0,				ImageType.Button,		"b-check0.gif",						11,		11,		"Unchecked")			},
			{ "check1",						new ImageInfo("",					0,				ImageType.Button,		"b-check1.gif",						11,		11,		"Checked")				},
			{ "close",						new ImageInfo("Close",				70,				ImageType.Button,		"",									70,		0,		"Close")				},
			{ "close.window",				new ImageInfo("Close Window",		110,			ImageType.Button,		"",									0,		0,		"Close Window")			},
			{ "confirm",					new ImageInfo("",					0,				ImageType.Button,		"icon_confirmation.gif",			16,		16,		"Confirmation")			},
			{ "delete",						new ImageInfo("Delete",				70,				ImageType.Button,		"",									70,		0,		"Delete")				},
			{ "delete.cross",				new ImageInfo("",					0,				ImageType.Button,		"deleteCross.gif", 					16,		16,		"Remove")				},
			{ "edit",						new ImageInfo("Edit",				60,				ImageType.Button,		"",									0,		0,		"Edit")					},
			{ "error",						new ImageInfo("",					0,				ImageType.Button,		"icon_errorSM.gif",					17,		15,		"Error")				},
			{ "errorSpace",					new ImageInfo("",					0,				ImageType.Button,		"spacer.gif",						17,		15,		"")						},
			{ "finish",						new ImageInfo("Finish",				70,				ImageType.Button,		"",									0,		0,		"Finish")				},
			{ "flag",						new ImageInfo("",					0,				ImageType.Button,		"flag.gif",							12,		9,		"Flag")					},
			{ "go",							new ImageInfo("Go",					25,				ImageType.Button,		"",									25,		0,		"GO")					},
			{ "guide.star",					new ImageInfo("",					0,				ImageType.Button,		"lg_guidestar.gif",					108,	33,		"Guide Star")			},
			{ "nextStep",					new ImageInfo("Next >",				70,				ImageType.Button,		"",									0,		0,		"Go Next")				},
			{ "no",							new ImageInfo("No",					0,				ImageType.Button,		"",									0,		0,		"No")					},
			{ "note",						new ImageInfo("Note",				0,				ImageType.Button,		"notes.gif",						14,		16,		"Note")					},
			{ "notes",						new ImageInfo("Notes",				0,				ImageType.Button,		"note.gif",							15,		15,		"Notes")				},
			{ "addNote",					new ImageInfo("Add Note",			0,				ImageType.Button,		"addnotes.gif",						14,		16,		"Add Note")				},
			{ "mail",						new ImageInfo("Mail",				0,				ImageType.Button,		"mail.gif",							14,		11,		"Mail")					},
			{ "ok",							new ImageInfo("Ok",					50,				ImageType.Button,		"",									0,		0,		"")						},
			{ "print",						new ImageInfo("Print",				70,				ImageType.Button,		"",									70,		0,		"Print")				},
			{ "radio0",						new ImageInfo("",					0,				ImageType.Button,		"b-radio0.gif",						11,		11,		"Unchecked")			},
			{ "radio1",						new ImageInfo("",					0,				ImageType.Button,		"b-radio1.gif",						11,		11,		"Checked")				},
			{ "reject",						new ImageInfo("Reject",				70,				ImageType.Button,		"",									70,		0,		"Reject")				},
			{ "review",						new ImageInfo("Review",				70,				ImageType.Button,		"",									0,		0,		"Review")				},
			{ "showAll",					new ImageInfo("Show All",			110,			ImageType.Button,		"",									0,		0,		"Show All")				},
			{ "save",						new ImageInfo("Save",				70,				ImageType.Button,		"",									0,		0,		"Save")					},
			{ "view",						new ImageInfo("View",				60,				ImageType.Button,		"",									60,		0,		"Vote")					},
			{ "view.detail",				new ImageInfo("View Detail",		120,			ImageType.Button,		"",									0,		0,		"View Detail")			},
			{ "yes",						new ImageInfo("Yes",				0,				ImageType.Button,		"",									0,		0,		"Yes")					},
			{ "cn.logo",					new ImageInfo("",					0,				ImageType.Button,		"cn_logo.jpg",						99,		113,	"Charity Navigator")	},
			{ "ico.question",				new ImageInfo("",					0,				ImageType.Button,		"ico-question.gif",					20,		20,		"")						},
			{ "ico.monitoring-blink",		new ImageInfo("",					0,				ImageType.Button,		"ico-monitoring-blink.gif",			20,		20,		"Monitoring")			},
			{ "ico.monitoring",				new ImageInfo("",					0,				ImageType.Button,		"ico-monitoring.gif",				20,		20,		"Monitoring")			},
			{ "ico.acknowledgment",			new ImageInfo("",					0,				ImageType.Button,		"ico-acknowledgment.gif",			20,		20,		"Acknowledgment")		},
			{ "ico.acknowledgment-blink",	new ImageInfo("",					0,				ImageType.Button,		"ico-acknowledgment-blink.gif",		20,		20,		"Acknowledgment")		},
			{ "ico.externalaction",			new ImageInfo("",					0,				ImageType.Button,		"ico-externalaction.gif",			20,		20,		"External Action")		},
			{ "ico.externalaction-blink",	new ImageInfo("",					0,				ImageType.Button,		"ico-externalaction-blink.gif",		20,		20,		"External Action")		},
			{ "ico.systemaction",			new ImageInfo("",					0,				ImageType.Button,		"ico-systemaction.gif",				20,		20,		"System Action")		},
			{ "ico.systemaction-blink",		new ImageInfo("",					0,				ImageType.Button,		"ico-systemaction-blink.gif",		20,		20,		"System Action")		},
			{ "ico.underresearch",			new ImageInfo("",					0,				ImageType.Button,		"ico-underresearch.gif",			20,		20,		"Under Research")		},
			{ "ico.nonotes",				new ImageInfo("",					0,				ImageType.Button,		"ico-nonotes.gif",					5,		15,		"")						},
			{ "ico.nonotes-bw",				new ImageInfo("",					0,				ImageType.Button,		"ico-nonotes-bw.gif",				15,		15,		"")						}
		};
		#endregion

		private readonly PageCommon _page;
		private static readonly object Locker = new object();

		private string form__form_id__;

		private dynamic form__defs__ = new DynamicBag();
		private dynamic form__glob__ = new DynamicBag();
		private dynamic form__locl__ = new DynamicBag();

		private bool form__skip_local__;
		private string form__temp__;
		private object[] form__argv__;

		internal FormOut(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public void Options(params string[] args)
		{
			form__glob__.Parse(args);
		}

		public void ResetOptions()
		{
			form__glob__.Reset();
		}

		public void Makelocal__(object[] argv, int argc)
		{
			if (form__skip_local__)
			{
				form__skip_local__ = false;
			}
			else
			{
				form__argv__ = argv;
				form__locl__ = new DynamicBag();
				form__locl__.Parse(argv, argc);
			}
		}

		public bool Flag__(string name)
		{
			if (Has(form__locl__[name]))
				return JsCompatibility.HasValue(form__locl__[name]);
			return Has(form__glob__[name]) && !Has(form__locl__["not_" + name]);
		}

		public string ErrorMark(string name, params string[] args)
		{
			Makelocal__(JsCompatibility.Arguments(name, args), 1);
			return Form__Build__(name, "<<error>>");
		}

		public string Input(string name, object value, params string[] args)
		{
			value = TryToTrim(value);

			form__defs__ = new DynamicBag();
			form__defs__.Type = "text";

			if (Has(form__glob__.input_size))
				form__defs__.Size = form__glob__.input_size;

			Makelocal__(JsCompatibility.Arguments<object>(name, value, args), 2);

			form__defs__["class"] = Has(form__locl__.Type) ? "input-" + form__locl__.type : "input-text";
			if (Flag__("readonly") || JsCompatibility.ToBoolean(form__locl__.ReadOnly))
				form__locl__["class"] = String.Format("'{0} input-readonly'", form__locl__["class"]);

			if (!IsReset(name) && _page.FormIn.Exists(name))
			{
				value = _page.FormIn.GetValue(name);
				if (value == null)
					value = "";
			}

			return Form__Build__(name,
				HtmlUtility.OpenTag("input") +
					"<<1: name=\"$\">><<2: value=\"$\">><<2+: @>>" +
					"<<readonly: readonly=readonly>>" +
					"<<type: type=@>>" +
					"<<width: style=\"width: @\">>" +
					"<<class: class=@>>" +
					"<<size: size=@>>" +
				HtmlUtility.CloseTag(true) +
					"<<error>>");
		}

		public string Checkbox(string name, object isChecked, params string[] args)
		{
			form__defs__ = new DynamicBag();
			form__defs__.value = "on";
			form__defs__["class"] = "input-checkbox";

			Makelocal__(JsCompatibility.Arguments<object>(name, isChecked, args), 2);

			if (!IsReset(name))
			{
				if (_page.Request.Form.HasKeys() && _page.Request.Form[name].Length > 0)
				{
					if (Has(form__locl__.Value))
					{
						string formValue = _page.FormIn.GetString(name, null);
						if (formValue == null)
						{
							// TODO: ?? strange code ??
							form__locl__["checked"] = _page.FormIn.GetString(name, null) == form__locl__.Value;
						}
						else
						{
							// @imported: takes into account the case of multiple checkboxes sharing the same name and having different values
							formValue = "," + RemoveSpaces(formValue) + ",";
							form__locl__["checked"] = formValue.Contains("," + form__locl__.value.ToString() + ",");
						}
					}
					else
					{
						form__locl__["checked"] = _page.FormIn.Exists(name);
					}
				}
				else
				{
					form__locl__["checked"] = JsCompatibility.ToBoolean(isChecked);
				}
			}
			else
			{
				form__locl__["checked"] = JsCompatibility.ToBoolean(isChecked);
			}

			if (Flag__("readonly"))
			{
				string img = _page.GetImageUrl("b-check" + ((form__locl__["checked"]) ? "1" : "0") + ".gif");
				return Form__Build__(name,
					HtmlUtility.Img(_page.GetImageUrl("spacer.gif"), height: 11, width: 1) +
					HtmlUtility.Img(img, height: 11, width: 11) +
					HtmlUtility.Img(_page.GetImageUrl("spacer.gif"), height: 11, width: 5) +
					(form__locl__["checked"] ? HtmlUtility.Tag("input", HtmlUtility.Attribute("type", "hidden"), "<<1: name=\"$\">>", "<<value: value=\"$\">>") : "") +
					"<<error>>");
			}

			return Form__Build__(name,
				HtmlUtility.OpenTag("input", HtmlUtility.Attribute("type", "checkbox")) +
						"<<1: name=\"$\">><<2+: @>>" +
						"<<checked: checked>>" +
						"<<value: value=\"$\">>" +
						"<<class: class=@>>" +
						"<<auto_submit: onclick=\"DoAutoSubmit(this);\">>" +
						"<<auto_onsubmit: onclick=\"DoAutoSubmit(this, true);\">>" +
					HtmlUtility.CloseTag(true) +
					"<<error>>");
		}

		public string RadioButton(string name, string value, object isChecked, params string[] args)
		{
			form__defs__ = new DynamicBag();
			form__defs__["class"] = "input-radio";

			Makelocal__(JsCompatibility.Arguments<object>(name, value, isChecked, args), 3);

			if (!IsReset(name) && _page.FormIn.Exists(name))
				form__locl__["checked"] = (_page.FormIn.GetString(name, null) == value);
			else
				form__locl__["checked"] = JsCompatibility.ToBoolean(isChecked);

			if (Flag__("readonly"))
			{
				string img = _page.GetImageUrl("b-radio" + ((form__locl__["checked"]) ? "1" : "0") + ".gif");
				return Form__Build__(name,
					HtmlUtility.Img(_page.GetImageUrl("spacer.gif"), height: 11, width: 2) +
					HtmlUtility.Img(img, height: 11, width: 11) +
					HtmlUtility.Img(_page.GetImageUrl("spacer.gif"), height: 11, width: 5) +
					(form__locl__["checked"] ? HtmlUtility.Tag("input", HtmlUtility.Attribute("type", "hidden"), "<<1: name=\"$\">>", "<<2: value=\"$\">>>") : "") +
					"<<error>>");
			}

			return Form__Build__(name,
				HtmlUtility.OpenTag("input", HtmlUtility.Attribute("type", "radio")) +
						"<<1: name=\"$\">><<3+: @>>" +
						"<<checked: checked>>" +
						"<<2: value=\"$\">>" +
						"<<class: class=@>>" +
						"<<auto_submit: onclick=\"DoAutoSubmit(this);\">>" +
						"<<auto_onsubmit: onclick=\"DoAutoSubmit(this, true);\">>" +
					HtmlUtility.CloseTag(true) +
					"<<error>>");
		}

		public string TextArea(string name, string value, params string[] args)
		{
			form__defs__ = new DynamicBag();
			form__defs__["class"] = "input-textarea";

			// TODO: ?? strange code ??
			if (Has(form__locl__.size))
				form__locl__.cols = Convert.ToInt32(form__locl__.size) + 2;
			else if (Has(form__glob__.input_size) && !Has(form__locl__.cols))
				form__locl__.cols = Convert.ToInt32(form__locl__.input_size) + 2;

			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);

			if (Flag__("readonly") || Has(form__locl__["readonly"]))
				form__locl__["class"] = String.Format("'{0} input-readonly'", form__locl__["class"]);

			if (!IsReset(name) && _page.FormIn.Exists(name))
				value = _page.FormIn.GetString("name", "");

			return Form__Build__(name,
				HtmlUtility.OpenTag("textarea") +
					"<<1: name=\"$\">><<2+: @>>" +
					"<<readonly: readonly=readonly>>" +
					"<<width: style=\"width:@\">>" +
					"<<height: style=\"height:@\">>" +
					"<<class: class=@>>" +
					"<<cols: cols=@>>" +
					"<<rows: rows=@>>" +
					"<<size:>>" +
				HtmlUtility.CloseTag(false) +
				"<<2:@>>" +
				HtmlUtility.EndTag("textarea") +
				"<<error>>");
		}

		public string ListBox(string name, string selected, string items, params string[] args)
		{
			//items = HtmlUtility.MakeAttributeFirst(items, "value");

			form__defs__ = new DynamicBag();
			form__defs__["class"] = "input-select";

			if (Flag__("readonly") || Has(form__locl__["readonly"]))
				form__defs__["class"] = String.Format("'{0} input-readonly'", form__defs__["class"]);

			Makelocal__(JsCompatibility.Arguments(name, selected, items, args), 3);

			selected = (!IsReset(name) && _page.FormIn.Exists(name))
				? _page.FormIn.GetString(name, "")
				: selected ?? "";

			string sel, slist = "";
			string[] arrSelected = null;
			Regex regExpr;
			if (JsCompatibility.HasValue(selected))
			{
				items = _r1.Replace(items, ">");
				//sel = selected.Replace(", ", ";");
				arrSelected = selected.Split(new[] { ", ", ";", "," }, StringSplitOptions.None);
				for (int i = 0; i < arrSelected.Length; i++)
				{
					sel = HtmlUtility.HtmlEncode(arrSelected[i]);
					slist = MakeItemSelectedByValue(items, sel, AttributeQuoteType.Double);
					if (slist == items)
					{
						slist = MakeItemSelectedByValue(items, sel, AttributeQuoteType.Single);
						if (slist == items)
						{
							regExpr = new Regex("<option value=" + sel + "(?=\\s|>)");
							slist = regExpr.Replace(items, "<option value=" + sel + " selected");
							if (slist == items)
							{
								regExpr = new Regex("<option>" + sel + "(?=<)");
								slist = regExpr.Replace(items, "<option selected>" + sel);
							}
						}
					}
					items = slist;
				}
			}

			if (Flag__("readonly"))
			{
				sel = "";
				int i = 0;
				int m = 0;
				for (int k = 0; (i = slist.IndexOf(" selected", i)) >= 0; k++)
				{
					int z = items.IndexOf(">", i);
					int j = items.IndexOf("<", z);
					if (z > j)
						continue;

					if (j - z - 1 > m)
						m = j - z - 1;
					sel += Environment.NewLine + items.Substring(z + 1, j);
					i = j;
				}

				int size = 0;
				if (Has(form__locl__.Size) && JsCompatibility.TryParseInt(form__locl__.Size, out size) && size > 0)
				{
					items = sel.Substring(1);
					form__defs__.Size = size;
					form__defs__.Cols = m;
					if (size > 1)
					{
						form__defs__.Rows = size;
						return Form__Build__(name,
							HtmlUtility.OpenTag("textarea", HtmlUtility.Attribute("name", name), HtmlUtility.Attribute("type", "text"), HtmlUtility.Attribute("readonly", "readonly")) +
								"-ro\"<<3+: @>>" +
								"<<size: rows=@>>" +
								"<<cols: cols=@>>" +
								"<<class: class=@>>" +
								"<<auto_submit:>>" +
								"<<auto_onsubmit:>>" +
								"<<readonly:>>" +
								"<<abbrev:>>" +
								"<<required:>>>" +
							HtmlUtility.CloseTag(false) +
								items +
							HtmlUtility.EndTag("textarea"));
					}
					else
					{
						items = "";
						form__defs__.Cols = 8;
					}

					return Form__Build__(name,
							HtmlUtility.OpenTag("input", HtmlUtility.Attribute("name", name), HtmlUtility.Attribute("type", "text"), HtmlUtility.Attribute("value", items), HtmlUtility.Attribute("readonly", "readonly"), HtmlUtility.Attribute("style", HtmlUtility.GetStyleRule("border", "1px dotted"))) +
								"-ro\"<<3+: @>>" +
								"<<auto_submit:>>" +
								"<<auto_onsubmit:>>" +
								"<<cols: size=@>>" +
								"<<class: class=@>>" +
								"<<size:>>" +
								"<<readonly:>>" +
								"<<abbrev:>>" +
								"<<required:>>" +
							HtmlUtility.CloseTag(true) +
								items +
							HtmlUtility.Hidden(name, selected) +
							"<<error>>");
				}
			}

			return Form__Build__(name,
					HtmlUtility.OpenTag("select", HtmlUtility.Attribute("name", name)) +
						"<<3+: @>>" +
						"<<class: class=@>>" +
						"<<abbrev:>>" +
						"<<required:>>" +
						"<<auto_submit: onchange=\"DoAutoSubmit(this)\">>" +
						"<<auto_onsubmit: onchange=\"DoAutoSubmit(this, true)\">>" +
						HtmlUtility.CloseTag(false) +
						items +
						HtmlUtility.EndTag("select") +
						"<<error>>");
		}
		private static readonly Regex _r1 = new Regex("\\s selected>", RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);

		public string ListBoxStates(string name, string value, params string[] args)
		{
			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);

			bool required = IsRequired("state");
			bool abbrev = Has(form__locl__["abbrev"]) ? JsCompatibility.HasValue(form__locl__["abbrev"]) : Has(form__glob__["state_abbrev"]);

			string s = GenerateStatesItemsHtml(abbrev);
			form__skip_local__ = true;
			return ListBox(name, value, required ? s : EmptyListNocItemHtml + s);
		}

		public string ListBoxCountries(string name, string value, params string[] args)
		{
			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);

			bool required = IsRequired("country");
			bool abbrev = Has(form__locl__["abbrev"]) ? JsCompatibility.HasValue(form__locl__["abbrev"]) : Has(form__glob__["country_abbrev"]);

			string s = GenerateCountriesItemsHtml(abbrev);
			form__skip_local__ = true;
			return ListBox(name, value, required ? s : EmptyListNocItemHtml + s);
		}

		public string InputEnums(string name, string value, object enumCategory)
		{
			throw new NotImplementedException();
		}

		public string InputDesign(string name, string value)
		{
			throw new NotImplementedException();
		}

		public string DatePicker(string name, string value, params string[] args)
		{
			value = TryToTrim(value);
			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);

			form__defs__ = new DynamicBag();
			form__defs__["class"] = "input-text";
			form__defs__.Width = "70px";
			form__defs__.DrawIcon = true;
			form__defs__.ShowMonth = true;
			form__defs__.ShowYear = true;
			form__defs__.Watermark = null;
			form__defs__.GotoCurrent = false;
			form__defs__.NoAnimation = false;
			form__defs__.AnimSpeed = "fast";
			form__defs__.NumberOfMonths = 1;
			form__defs__.MinDate = null;
			form__defs__.MaxDate = null;
			form__defs__.ShowHolidays = true;

			bool required = IsRequired("date");
			bool abbrev = Has(form__locl__["abbrev"]) ? JsCompatibility.HasValue(form__locl__["abbrev"]) : Has(form__glob__["date_abbrev"]);

			string type = (Has(form__locl__.Type))
				? form__locl__.Type
				: (Has(form__glob__["date_type"]))
					// TODO: weird code. Must be
					// type = form__glob__["date_type"];
					? type = form__locl__.Type
					: "MDY";

			StringBuilder template = new StringBuilder();
			if (type.Length == 3)
			{
				DateTime dt;
				if (!IsReset(name) && _page.FormIn.Exists(name))
					value = _page.FormIn.GetString(name, "");
				else if (DateTime.TryParse(value, out dt))
					value = dt.ToString("d");

				template.Append(
					HtmlUtility.OpenTag("input",
						HtmlUtility.Attribute("type", "hidden"),
						HtmlUtility.Attribute("value", value),
						HtmlUtility.Attribute("id", name),
						HtmlUtility.Attribute("onchange", "fixInputDate(\"" + name + "\");")))
						.Append("<<1: name=\"$:MASK\">>")
					.Append(HtmlUtility.CloseTag(true))
					.Append(HtmlUtility.OpenTag("input"))
						.Append("<<1: name=\"$\">>")
						.Append("<<2: value=\"" + value + "\">>")
						.Append("<<2+: @>>")
						.Append("<<readonly: readonly=readonly " + HtmlUtility.Attribute("style", HtmlUtility.GetStyleRule("border-style", "dotted")) + ">>")
						.Append("<<type: type=@>>")
						.Append("<<width: " + HtmlUtility.Attribute("style", HtmlUtility.GetStyleRule("width", "@")) + ">>")
						.Append("<<class: class=@>>")
					.Append(HtmlUtility.CloseTag(true));

				if (Flag__("readonly"))
				{
					template.Append(HtmlUtility.Img(_page.GetImageUrl("icon_calendar.gif"),
						style: HtmlUtility.GetStyleRule("margin", "0 3px", "margin-bottom", "-2px", "filter", "progid:DXImageTransform.Microsoft.Alpha(style=0, opacity=50)", "opacity", "0.5", "-moz-opacity", "0.5")));
				}
				else
				{
					_page.ClientScript.RequireJQuery(true);

					string watermark = tE(form__locl__.Watermark, form__defs__.watermark);
					object minDate = tE(form__locl__.MinDate, form__defs__.MinDate);
					object maxDate = tE(form__locl__.MaxDate, form__defs__.MaxDate);
					object noAnimation = tE(form__locl__.NoAnimation, form__defs__.NoAnimation);
					string animSpeed = tE(form__locl__.AnimSpeed, form__defs__.AnimSpeed);
					object gotoCurrent = tE(form__locl__.GotoCurrent, form__defs__.GotoCurrent);
					object drawIcon = tE(form__locl__.DrawIcon, form__defs__.DrawIcon);

					object numberOfMonths = tE(form__locl__.NumberOfMonths, form__defs__.NumberOfMonths);
					object showMonth = tE(form__locl__.ShowMonth, form__defs__.ShowMonth);
					object showYear = tE(form__locl__.ShowYear, form__defs__.ShowYear);

					StringBuilder script = new StringBuilder()
						.Append("$('#").Append(name).Append("').datepicker({ ")
						.Append("changeMonth: ").JValue(tQ(showMonth))
						.Append(", ")
						.Append("changeYear: ").JValue(tQ(showYear))
						.Append(", ")
						.Append("numberOfMonths: ").Append(numberOfMonths);

					if (tQ(form__locl__.showHolidays))
					{
						string holidays = GetHolidaysData();
						_page.ClientScript.RegisterStartup("datepicker__holidays", "$FS.Asp.Holidays = " + holidays);
						script.Append(", ")
							.Append("beforeShowDay: ").Append("$FS.Asp.GetDatePickerHolidays($FS.Asp.Holidays)");
					}

					if (tQ(watermark))
						_page.ClientScript.RegisterStartup(name + "__watermark", "$(\"#" + name + "\").Watermark(\"" + watermark + "\");");

					if (tQ(minDate))
						script.Append(", ")
							.Append("minDate: ").Append(minDate);

					if (tQ(maxDate))
						script.Append(", ")
							.Append("maxDate: ").Append(maxDate);

					if (tQ(noAnimation))
						script.Append(", ")
							.Append("duration: ").JValue("");
					else if (tQ(animSpeed))
						script.Append(", ")
							.Append("duration: ").JValue(animSpeed);

					script.Append(", ")
						.Append("gotoCurrent: ").JValue(tQ(gotoCurrent));

					if (tQ(drawIcon))
					{
						script.Append(", ")
							.Append("showOn: ").JValue("button")
							.Append(", ")
							.Append("buttonImage: ").JValue(_page.GetImageUrl("icon_calendar.gif"))
							.Append(", ")
							.Append("buttonImageOnly: ").JValue(true)
							.Append(", ")
							.Append("buttonText: ").JValue("Show/Hide Calendar");
					}

					script.Append(" });");
					_page.ClientScript.RegisterStartup(name + "__behavior", script.ToString());
				}

				return Form__Build__(name, template
												.Append("<<error>>")
												.Append("<<readonly:>>")
												.Append("<<type:>>")
												.Append("<<required:>>").ToString());
			}

			string vD;
			string vM;
			string vY;
			DateTime dt1;
			if (!IsReset(name) && _page.FormIn.Exists(name + ":Y"))
			{
				vD = _page.FormIn.GetString(":D", "");
				vM = _page.FormIn.GetString(":M", "");
				vY = _page.FormIn.GetString(":Y", "");
			}
			else if (DateTime.TryParse(value, out dt1) || required)
			{
				if (dt1 == default(DateTime))
					dt1 = DateTime.Now;

				vD = dt1.Day.ToString();
				vM = dt1.Month.ToString();
				vY = (dt1.Year < 1000 ? dt1.Year + 1900 : dt1.Year).ToString();
			}
			else
			{
				value = vD = vM = vY = "";
			}

			string monthsList = GenerateMonthsItemsHtml(abbrev);
			if (!required)
				monthsList = EmptyListNocItemHtml + monthsList;
			string daysList = GenerateDaysItemsHtml();
			if (!required)
				daysList = EmptyListNocItemHtml + daysList;

			int startDate = Has(form__locl__.Start)
				? JsCompatibility.TryParseInt(form__locl__.Start, out startDate) ? startDate : DefaultStartDate
				: Has(form__glob__["date_start"])
					? JsCompatibility.TryParseInt(form__glob__["date_start"], out startDate) ? startDate : DefaultStartDate
					: DefaultStartDate;
			int endDate = Has(form__locl__.End)
				? JsCompatibility.TryParseInt(form__locl__.End, out endDate) ? endDate : DateTime.Now.Year
				: Has(form__glob__["date_end"])
					? JsCompatibility.TryParseInt(form__glob__["date_end"], out endDate) ? endDate : DateTime.Now.Year
					: DateTime.Now.Year;
			string yearsList = GenerateYearsItemsHtml(startDate, endDate);
			if (!required)
				yearsList = EmptyListNocItemHtml + yearsList;

			string s;
			if (JsCompatibility.HasValue(vM))
				monthsList = MakeItemSelectedByValue(monthsList, vM);
			else
				monthsList = monthsList.Replace(HtmlUtility.BeginTag("option"), HtmlUtility.BeginTag("option", "selected"));

			daysList = MakeItemSelectedByName(daysList, vD);
			yearsList = MakeItemSelectedByName(yearsList, vY);

			int i;
			template.Clear();
			if (Flag__("readonly"))
			{
				i = monthsList.IndexOf(" selected>");
				if (i >= 0)
					s = monthsList.Substring(i + 10, monthsList.IndexOf("<", i));
				else
					s = "";
				for (int n = 0; n < type.Length; n++)
				{
					switch (Char.ToUpperInvariant(type[n]))
					{
						case 'Y':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.Tag("input",
									HtmlUtility.Attribute("type", "text"),
									HtmlUtility.Attribute("name", name + ":Y"),
									"readonly",
									HtmlUtility.Attribute("style", HtmlUtility.GetStyleRule("border", "1px dotted")),
									HtmlUtility.Attribute("value", vY),
									HtmlUtility.Attribute("size", "6"),
									"<<class: class=@>>"));
							break;
						case 'M':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.Tag("input",
									HtmlUtility.Attribute("type", "text"),
									HtmlUtility.Attribute("name", name + "_ro:M"),
									"readonly",
									HtmlUtility.Attribute("style", "border: 1px dotted;"),
									HtmlUtility.Attribute("value", s),
									HtmlUtility.Attribute("size", "12"),
									"<<class: class=@>>"))
								.Append(HtmlUtility.Hidden(name + ":M", vM));
							break;
						case 'D':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.Tag("input",
									HtmlUtility.Attribute("type", "text"),
									HtmlUtility.Attribute("name", name + ":D"),
									"readonly",
									HtmlUtility.Attribute("style", "border: 1px dotted;"),
									HtmlUtility.Attribute("value", vD),
									HtmlUtility.Attribute("size", "3"),
									"<<class: class=@>>"));
							break;
					}
				}
			}
			else
			{
				for (int n = 0; n < type.Length; n++)
				{
					switch (Char.ToUpperInvariant(type[n]))
					{
						case 'Y':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.OpenTag("select") +
									"<<1: name=\"$:Y\">>" +
									"<<3+: @>>" +
									"<<class: class=@>>")
								.Append(HtmlUtility.CloseTag(false))
								.Append(yearsList)
								.Append(HtmlUtility.EndTag("select"));
							break;
						case 'M':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.OpenTag("select") +
									"<<1: name=\"$:M\">>" +
									"<<3+: @>>" +
									"<<class: class=@>>")
								.Append(HtmlUtility.CloseTag(false))
								.Append(monthsList)
								.Append(HtmlUtility.EndTag("select"));
							break;
						case 'D':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.OpenTag("select") +
									"<<1: name=\"$:D\">>" +
									"<<3+: @>>" +
									"<<class: class=@>>")
								.Append(HtmlUtility.CloseTag(false))
								.Append(daysList)
								.Append(HtmlUtility.EndTag("select"));
							break;
					}
				}
			}

			string tmp = template.ToString();
			if (JsCompatibility.HasValue(tmp))
				tmp = tmp.Substring(7);
			if (tmp.IndexOf('Y') < 0)
				tmp += HtmlUtility.Hidden(name + ":Y", "2000");
			if (tmp.IndexOf('M') < 0)
				tmp += HtmlUtility.Hidden(name + ":M", "1");
			if (tmp.IndexOf('D') < 0)
				tmp += HtmlUtility.Hidden(name + ":D", "1");

			if (type.Length > 0)
				tmp += HtmlUtility.Hidden(name + ":MASK", type);

			return Form__Build__(name, tmp + "<<error>><<readonly:>><<type:>><<required:>>");
		}

		public string DateRangePicker(string name, string fromValue, string toValue, params string[] args)
		{
			fromValue = TryToTrim(fromValue);
			toValue = TryToTrim(toValue);

			var argsLeft = new List<string>() { name + "_from", fromValue };
			var argsRight = new List<string>() { name + "_to", toValue };

			for (int i = 0; i < args.Length; i++)
			{
				string arg = args[i];
				int n = arg.IndexOf('=');
				if (n > 0)
				{
					string argName = arg.Substring(0, n);
					string argValue = arg.Substring(n + 1);
					switch (argName.ToUpperInvariant())
					{
						case "REQUIRED":
							if (argValue.Equals("left", StringComparison.OrdinalIgnoreCase))
								argsLeft.Add("required");
							else if (argValue.Equals("right", StringComparison.OrdinalIgnoreCase))
								argsRight.Add("required");
							continue;

						case "WATERMARK":
							n = argValue.IndexOf(":");
							if (n > 0)
							{
								string target = argValue.Substring(0, n);
								string text = argValue.Substring(n + 1);
								if (target.Equals("left", StringComparison.OrdinalIgnoreCase))
									argsLeft.Add("watermark=" + text);
								else if (target.Equals("right", StringComparison.OrdinalIgnoreCase))
									argsRight.Add("watermark=" + text);
							}
							continue;

						case "MINDATE":
							n = argValue.IndexOf(":");
							if (n > 0)
							{
								string target = argValue.Substring(0, n);
								string text = argValue.Substring(n + 1);
								if (target.Equals("left", StringComparison.OrdinalIgnoreCase))
									argsLeft.Add("minDate=" + text);
								else if (target.Equals("right", StringComparison.OrdinalIgnoreCase))
									argsRight.Add("minDate=" + text);
							}
							continue;

						case "MAXDATE":
							n = argValue.IndexOf(":");
							if (n > 0)
							{
								string target = argValue.Substring(0, n);
								string text = argValue.Substring(n + 1);
								if (target.Equals("left", StringComparison.OrdinalIgnoreCase))
									argsLeft.Add("maxDate=" + text);
								else if (target.Equals("right", StringComparison.OrdinalIgnoreCase))
									argsRight.Add("maxDate=" + text);
							}
							continue;
					}
				}
				argsLeft.Add(arg);
				argsRight.Add(arg);
			}

			Makelocal__(argsLeft.ToArray(), 2);
			form__skip_local__ = true;
			string fromDatePicker = DatePicker(name + "_from", fromValue);
			form__skip_local__ = false;

			Makelocal__(argsRight.ToArray(), 2);
			form__skip_local__ = true;
			string toDatePicker = DatePicker(name + "_to", toValue);
			form__skip_local__ = false;

			return
				fromDatePicker + HtmlUtility.NonBreakingSpacer + HtmlUtility.NonBreakingSpacer +
				"to" + HtmlUtility.NonBreakingSpacer + HtmlUtility.NonBreakingSpacer +
				toDatePicker;
		}

		public string TimePicker(string name, string value, params string[] args)
		{
			value = TryToTrim(value);

			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);

			form__defs__ = new DynamicBag();
			form__defs__["class"] = "input-select";

			bool required = IsRequired("time");
			string type = Has(form__locl__.Type)
				? form__locl__.Type
				: Has(form__glob__["time_type"])
				// TODO: weird code. Must be
				// type = form__glob__.Type;
					? form__locl__.Type
					: "HNA";
			bool is24H = !type.Contains('A');

			string vH, vM, vS, vA;
			DateTime dt;
			if (!IsReset(name) && _page.FormIn.Exists(name + ":H"))
			{
				vH = _page.FormIn.GetString(name + ":H", "");
				vM = _page.FormIn.GetString(name + ":N", "");
				vS = _page.FormIn.GetString(name + ":S", "");
				vA = _page.FormIn.GetString(name + ":A", "");
			}
			else if (DateTime.TryParse(value, out dt))
			{
				int vH2 = dt.Hour;
				vM = dt.Minute.ToString();
				vS = dt.Second.ToString();
				if (!is24H)
				{
					vA = vH2 >= 12 ? "2" : "1";
					vH = vH2 == 0
						? "12"
						: ((vH2 > 12)
							? vH2 - 12
							: vH2)
						.ToString();
				}
				else
				{
					vH = vH2.ToString();
					vA = "";
				}
			}
			else
			{
				vH = vM = vS = vA = "";
			}

			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);

			string hoursList = GenerateHoursItemsHtml(is24H);
			if (!required)
				hoursList = EmptyListNocItemHtml + hoursList;
			string minutesList = GenerateMinutesItemsHtml();
			if (!required)
				minutesList = EmptyListNocItemHtml + minutesList;
			string secondsList = minutesList;
			string ampmList = GenerateAmPmItemsHtml();
			if (!required)
				ampmList = EmptyListNocItemHtml + ampmList;

			StringBuilder template = new StringBuilder();
			hoursList = MakeItemSelectedByName(hoursList, vH);
			minutesList = MakeItemSelectedByValue(minutesList, vM);
			secondsList = MakeItemSelectedByValue(minutesList, vS);
			ampmList = MakeItemSelectedByValue(ampmList, vA);

			if (Flag__("readonly"))
			{
				string s = (vA == "1") ? "AM" : (vA == "2") ? "PM" : "\"\"";
				for (int i = 0; i < type.Length; i++)
				{
					switch (Char.ToUpperInvariant(type[i]))
					{
						case 'H':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.Tag("input",
									HtmlUtility.Attribute("type", "text"),
									HtmlUtility.Attribute("name", name + ":H"),
									"readonly",
									HtmlUtility.Attribute("style", "border: 1px dotted;"),
									HtmlUtility.Attribute("value", vH),
									HtmlUtility.Attribute("size", "3"),
									"<<class: class=@>>"));
							break;

						case 'N':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.Tag("input",
									HtmlUtility.Attribute("type", "text"),
									HtmlUtility.Attribute("name", name + ":N"),
									"readonly",
									HtmlUtility.Attribute("style", "border: 1px dotted;"),
									HtmlUtility.Attribute("value", (vM.Length < 2 ? "0" + vM : vM)),		// TODO: weird code
									HtmlUtility.Attribute("size", "3"),
									"<<class: class=@>>"));
							break;

						case 'S':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.Tag("input",
									HtmlUtility.Attribute("type", "text"),
									HtmlUtility.Attribute("name", name + ":S"),
									"readonly",
									HtmlUtility.Attribute("style", "border: 1px dotted;"),
									HtmlUtility.Attribute("value", (vS.Length < 2 ? "0" + vS : vS)),		// TODO: weird code
									HtmlUtility.Attribute("size", "3"),
									"<<class: class=@>>"));
							break;

						case 'A':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.Tag("input",
									HtmlUtility.Attribute("type", "text"),
									HtmlUtility.Attribute("name", name + "_ro:A"),
									"readonly",
									HtmlUtility.Attribute("style", "border: 1px dotted;"),
									HtmlUtility.Attribute("value", s),
									HtmlUtility.Attribute("size", "3"),
									"<<class: class=@>>"))
								.Append(HtmlUtility.Hidden(name + ":A", vA));
							break;
					}
				}
			}
			else
			{
				for (int i = 0; i < type.Length; i++)
				{
					switch (Char.ToUpperInvariant(type[i]))
					{
						case 'H':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.OpenTag("select") +
									"<<1: name=\"$:H\">>" +
									"<<3+: @>>" +
									"<<class: class=@>>")
								.Append(HtmlUtility.CloseTag(false))
								.Append(hoursList)
								.Append(HtmlUtility.EndTag("select"));
							break;

						case 'N':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.OpenTag("select") +
									"<<1: name=\"$:N\">>" +
									"<<3+: @>>" +
									"<<class: class=@>>")
								.Append(HtmlUtility.CloseTag(false))
								.Append(minutesList)
								.Append(HtmlUtility.EndTag("select"));
							break;

						case 'S':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.OpenTag("select") +
									"<<1: name=\"$:S\">>" +
									"<<3+: @>>" +
									"<<class: class=@>>")
								.Append(HtmlUtility.CloseTag(false))
								.Append(secondsList)
								.Append(HtmlUtility.EndTag("select"));
							break;

						case 'A':
							template
								.Append(" ")
								.Append(HtmlUtility.NonBreakingSpacer)
								.Append(HtmlUtility.OpenTag("select") +
									"<<1: name=\"$:A\">>" +
									"<<3+: @>>" +
									"<<class: class=@>>")
								.Append(HtmlUtility.CloseTag(false))
								.Append(ampmList)
								.Append(HtmlUtility.EndTag("select"));
							break;
					}
				}
			}

			string tmp = template.ToString();
			if (JsCompatibility.HasValue(tmp))
				tmp = tmp.Substring(7);
			if (tmp.IndexOf('H') < 0)
				tmp += HtmlUtility.Hidden(name + ":H", "0");
			if (tmp.IndexOf('N') < 0)
				tmp += HtmlUtility.Hidden(name + ":N", "1");
			if (tmp.IndexOf('S') < 0)
				tmp += HtmlUtility.Hidden(name + ":S", "1");
			if (tmp.IndexOf('A') < 0)
				tmp += HtmlUtility.Hidden(name + ":A", "1");

			return Form__Build__(name, tmp + "<<error>><<readonly:>><<type:>><<required:>>");
		}

		public string InputSsn(string name, string value, params string[] args)
		{
			form__defs__ = new DynamicBag();
			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);

			if (Has(form__locl__.Long))
				return InputSeries(name, value, new[] { 3, 2, 4 }, new[] { "" }) + HtmlUtility.Hidden(name + ":format", "long");

			if (value != null)
			{
				value = value.Trim();
				if (value.Length > 4)
					value = value.Substring(value.Length - 4, 4);
			}

			string divider = HtmlUtility.NonBreakingSpacer + "-" + HtmlUtility.NonBreakingSpacer;
			return InputSeries(name, value, new[] { 4 }, new[] { "XXX" + divider + "XX" + divider }) + HtmlUtility.Hidden(name + ":format", "short");
		}
		public string InputSsn(string name, int? value, params string[] args)
		{
			return InputSsn(name, value.HasValue ? value.Value.ToString() : null, args);
		}
		
		//public string InputSsn(string name, object value, params string[] args)
		//{
		//	form__defs__ = new _DynamicBag();
		//	Form__makelocal__(JsCompatibility.Arguments(name, value, args), 2);

		//	if (Has(form__locl__.Long))
		//		return InputSeries(name, value, new[] { 3, 2, 4 }, new[] { "" }) + HtmlUtility.Hidden(name + ":format", "long");

		//	if (value != null)
		//	{
		//		string v = JsCompatibility.ToString(value);
		//		v = v.Trim();
		//		if (v.Length > 4)
		//			v = v.Substring(v.Length - 4, 4);
		//		value = v;
		//	}

		//	string divider = HtmlUtility.NonBreakingSpacer + "-" + HtmlUtility.NonBreakingSpacer;
		//	return InputSeries(name, value, new[] { 4 }, new[] { "XXX" + divider + "XX" + divider }) + HtmlUtility.Hidden(name + ":format", "short");
		//}

		public string InputEIN(string name, object value, params string[] args)
		{
			form__defs__ = new DynamicBag();
			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);
			return InputSeries(name, value, new[] { 2, 7 }, new[] { "" });
		}

		public string InputPhone(string name, object value, params string[] args)
		{
			form__defs__ = new DynamicBag();
			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);
			return InputSeries(name, value, new[] { 3, 3, 4 }, new[] { "(" + HtmlUtility.NonBreakingSpacer, HtmlUtility.NonBreakingSpacer + ")" + HtmlUtility.NonBreakingSpacer });
		}

		public string InputCmNumber(string name, object value, params string[] args)
		{
			form__defs__ = new DynamicBag();
			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);
			return InputSeries(name, value, new[] { 4, 4, 4, 4, 4 }, new[] { "" });
		}

		public string InputZip(string name, object value, params string[] args)
		{
			form__defs__ = new DynamicBag();
			Makelocal__(JsCompatibility.Arguments(name, value, args), 2);
			return Has(form__locl__.Short)
				? InputSeries(name, value, new[] { 5 }, new[] { "" })
				: InputSeries(name, value, new[] { 5, 4 }, new[] { "" });
		}

		public string InputAmount(string name, object value, bool renderSign, params string[] args)
		{
			value = TryToTrim(value);

			if (JsCompatibility.ToBoolean(value) && !_page.Errors.HasError(name))
				value = renderSign ? Common.oCur(value, 2) : Common.oNum(value, 2);

			string onChangeHandlers = "$FS.Formatters.CurrencyFormatter.EventHandler_Input(this);";
			List<string> args2 = new List<string>();
			if (args != null && args.Length > 0)
			{
				for (int i = 0; i < args.Length; i++)
				{
					string arg = args[i];
					int m = arg.IndexOf('=');
					if (m > 0)
					{
						string argName = arg.Substring(0, m);
						string argValue = arg.Substring(m + 1);
						switch (argName.ToUpperInvariant())
						{
							case "ALIGN":
								if (argValue.Equals("left", StringComparison.OrdinalIgnoreCase))
									args2.Add("style='text-align: left;'");
								else if (argValue.Equals("right", StringComparison.OrdinalIgnoreCase))
									args2.Add("style='text-align: right;'");
								continue;

							case "ONCHANGE":
								if (!onChangeHandlers.EndsWith(";"))
									onChangeHandlers += ";";
								onChangeHandlers += argValue.Trim();
								continue;
						}
					}
					args2.Add(arg);
				}
			}
			args2.Add(HtmlUtility.Attribute("onchange", onChangeHandlers));

			return Input(name, value, (args2 != null) ? args2.ToArray() : null);
		}

		public string InputHidden(string name, object value)
		{
			return HtmlUtility.Hidden(name, (value != null) ? JsCompatibility.ToString(value) : "", id: name);
		}

		// was "oiFormID"
		public string GenerateFormIdHtml(string name)
		{
			using (var writer = new HtmlWriter())
			{
				if (JsCompatibility.HasValue(name))
					writer.Hidden("form", name);
				WriteFormIdHtml(writer);
				return writer.GetContent();
			}
		}

		public void BeginForm(string name, params string[] args)
		{
			if (!JsCompatibility.HasValue(name))
				name = "main";

			using (var writer = new HtmlWriter(_page.Writer))
			{
				writer.WriteComment("$$[FORM]");

				bool simple = false;
				for (int i = 0; i < args.Length; i++)
				{
					if (args[i].Equals("simple", StringComparison.OrdinalIgnoreCase))
						simple = true;
					else if (JsCompatibility.HasValue(args[i]))
						writer.Attr(args[i]);
				}

				if (writer.HasAttributeInBuffer("method"))
					writer.Attr("method", "post");
				if (writer.HasAttributeInBuffer("action"))
					writer.Attr("action", _page.MyUrl);
				writer
					.Attr("name", name)
					.Tag("form");

				if (!simple)
				{
					writer.Write(HtmlUtility.Hidden("form", name));
					WriteFormIdHtml(writer);
				}
			}
		}

		public void EndForm()
		{
			using (var writer = new HtmlWriter(_page.Writer))
			{
				writer
						.End("form")
					.WriteComment("$$[/FORM]");

				// TODO: clientScripting
				// form__ajax_write_inits__
			}
		}

		#region Images
		public string Spacer(int width, int height)
		{
			return HtmlUtility.Img(_page.GetImageUrl("spacer.gif"), width: width, height: height);
		}

		public string Image(string nameOrUrl, params string[] args)
		{
			string url;
			ImageInfo info = Find(nameOrUrl);
			if (info != null)
				url = _page.GetImageUrl(info.Url);
			else
			{
				if (UrlUtility.IsAbsolute(nameOrUrl))
					throw new ArgumentException("Url must be relative");
				url = _page.GetImageUrl(nameOrUrl);
			}

			bool isBlank = false;
			using (var writer = new HtmlWriter())
			{
				if (args != null && args.Length > 0)
				{
					for (int i = 0; i < args.Length; i++)
					{
						if (args[i] == "blank")
							isBlank = true;
						else
							writer.Attr(args[i]);
					}
				}

				writer
					.Attr("src", isBlank ? _page.GetImageUrl("spacer.gif") : url);

				if (!isBlank && JsCompatibility.HasValue(info.Title) && !writer.HasAttributeInBuffer("alt"))
					writer.Attr("alt", info.Title);
				if (!writer.HasAttributeInBuffer("width"))
					writer.Attr("width", info.Width.ToString());
				if (!writer.HasAttributeInBuffer("height"))
					writer.Attr("height", info.Height.ToString());
				if (!writer.HasAttributeInBuffer("border"))
					writer.Attr("border", "0");

				writer.TagSingle("img");
				return writer.GetContent();
			}
		}

		public string Button(string name, string imageOrTitle = null, params string[] args)
		{
			if (!JsCompatibility.HasValue(imageOrTitle))
				imageOrTitle = name;

			ButtonAction action = ButtonAction.Submit;
			ImageType flags = ImageType.Button;
			string title = null;
			string width = null;
			string height = null;
			string href = null;
			string onClick = null;
			string alt = null;
			bool compact = false;
			bool alert = false;
			using (var writer = new HtmlWriter())
			{
				if (args != null && args.Length > 0)
				{
					for (int i = 0; i < args.Length; i++)
					{
						if (Enum.TryParse<ButtonAction>(args[i], true, out action))
							continue;

						var attrInfo = HtmlUtility.ParseAttribute(args[i]);
						if (attrInfo != null)
						{
							switch (attrInfo.Item1.ToUpperInvariant())
							{
								case "WIDTH":
									width = attrInfo.Item2;
									break;
								case "HEIGHT":
									height = attrInfo.Item2;
									break;
								case "ALT":
									alt = attrInfo.Item2;
									break;
								case "HREF":
									href = attrInfo.Item2;
									break;
								case "ONCLICK":
									onClick = attrInfo.Item2;
									break;
								default:
									writer.Attr(attrInfo.Item1, attrInfo.Item2);
									break;
							}
						}
						else
						{
							switch (args[i].ToUpperInvariant())
							{
								case "COMPACT":
									compact = true;
									break;
								case "ALERT":
									alert = true;
									break;
								default:
									writer.Attr(args[i]);
									break;
							}
						}
					}
				}

				if (JsCompatibility.HasValue(href) && onClick == null)
				{
					_page.Href.AddLegalTarget(href);
					onClick = "return $href.call(this, \"" + HtmlUtility.HtmlEncode(href) + "\");";
					action = ButtonAction.Button;
				}

				var imageInfo = Find(imageOrTitle);
				if (imageInfo != null)
				{
					title = title ?? imageInfo.Title;
					imageOrTitle = imageInfo.ButtonTitle;
					if (String.IsNullOrEmpty(imageOrTitle) && !String.IsNullOrEmpty(imageInfo.Url))
					{
						writer
							.Attr("type", "image")
							.Attr("src", _page.GetImageUrl(imageInfo.Url))
							.Attr("border", "0")
							.Attr("width", imageInfo.Width.ToString())
							.Attr("height", imageInfo.Height.ToString())
							.Attr("name", HtmlUtility.HtmlEncode(name))
							.Attr("value", HtmlUtility.HtmlEncode(imageOrTitle));

						if (JsCompatibility.HasValue(title))
							writer.Attr("alt", HtmlUtility.HtmlEncode(title));
						if (action == ButtonAction.Cancel && onClick == null)
							writer.Attr("onclick", "this.form.reset(); return false;");

						writer.TagSingle("input");
						return writer.GetContent();
					}

					if (width == null && imageInfo.ButtonWidth > 0)
						width = imageInfo.ButtonWidth.ToString();
					flags = imageInfo.Flags;
				}

				if (JsCompatibility.HasValue(title))
					writer.Attr("title", HtmlUtility.HtmlEncode(title));

				if (!writer.HasAttributeInBuffer("class"))
					writer.Attr("class", ((flags & ImageType.Alert) > 0 || alert) ? "input-button-red" : "input-button");

				if (!writer.HasAttributeInBuffer("style"))
				{
					if (JsCompatibility.HasValue(width))
						writer.AttrStyle("width", width);
					if (JsCompatibility.HasValue(height))
						writer.AttrStyle("width", height);
					if (compact)
					{
						writer
							.AttrStyle("margin", "0")
							.AttrStyle("padding", "0")
							.AttrStyle("width", "auto");
					}
				}
				if (action == ButtonAction.Submit)
					writer.Attr("type", "submit");
				else if (action == ButtonAction.Cancel)
					writer.Attr("type", "reset");
				else
				{
					writer.Attr("type", "button");
					if (action == ButtonAction.Update)
						onClick = "$FS.Asp.PanelUpdateManager.GetWrappingUpdatePanel(this).Update();";
				}

				if (onClick != null)
					writer.Attr("onclick", HtmlUtility.HtmlEncode(onClick));

				writer
					.Attr("name", name)
					.Attr("name", HtmlUtility.HtmlEncode(name))
					.Attr("value", HtmlUtility.HtmlEncode(imageOrTitle))
					.TagSingle("input");

				return writer.GetContent();
			}
		}
		#endregion


		#region Implementation
		#region JSObject helpers
		protected static bool Has(string value)
		{
			return value != null;
		}

		protected dynamic tE(params object[] values)
		{
			for (int i = 0; i < values.Length; i++)
			{
				if (tQ(values[i]))
					return values[i];
			}
			return values[0];
		}

		protected static bool tQ(object value)
		{
			return JsCompatibility.ToBoolean(value);
		}
		protected static bool tN(object value)
		{
			return !JsCompatibility.ToBoolean(value);
		}
		#endregion

		#region ListBox items generators
		private static readonly string EmptyListNocItemHtml = HtmlUtility.BeginTag("option", HtmlUtility.Attribute("value", "")) + HtmlUtility.EndTag("option");

		private static readonly Dictionary<int, string> __generatedItems = new Dictionary<int,string>(8);
		private static readonly string[] _statesNames = new[] { "Alabama", "Alaska", "American Samoa", "Arizona", "Arkansas", "Armed Forces Americas", "Armed Forces Pacific", "California", "Colorado", "Connecticut", "Delaware", "District of Columbia", "Florida", "Georgia", "Guam", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Micronesia", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "North Marianas Islands", "Ohio", "Oklahoma", "Oregon", "Palua", "Pennsylvania", "Puerto Rico", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virgin Islands", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming" };
		private static readonly string[] _statesAbbrevs = new[] { "AL", "AK", "AS", "AZ", "AR", "AA", "AP", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "GU", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "FM", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "MP", "OH", "OK", "OR", "PW", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VI", "VA", "WA", "WV", "WI", "WY" };
		private static string GenerateStatesItemsHtml(bool abbrevOnly)
		{
			return (abbrevOnly)
				? GenerateListBoxItemsHtml(_statesAbbrevs)
				: GenerateListBoxItemsHtml(_statesNames, _statesAbbrevs);
		}

		private static readonly string[] _countriesNames = new[] { "Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia, Plurinational State Of", "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, The Democratic Republic Of The", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran, Islamic Republic Of", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia, The Former Yugoslav Republic Of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States Of", "Moldova, Republic Of", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Korea, Democratic People's Republic Of", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthelemy", "Saint Helena, Ascension And Tristan Da Cunha", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Korea, Republic Of", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard and Jan Mayen", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province Of China", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Holy See (Vatican City State)", "Venezuela, Bolivarian Republic Of", "Viet Nam", "Virgin Islands, British", "Virgin Islands, U.S.", "Wallis and Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe" };
		private static readonly string[] _countriesAbbrevs = new[] { "AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI", "HR", "CU", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW", "GY", "HT", "HM", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP", "JE", "JO", "KZ", "KE", "KI", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "ME", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "AN", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "KP", "MP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "BL", "SH", "KN", "LC", "MF", "PM", "VC", "WS", "SM", "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SK", "SI", "SB", "SO", "ZA", "GS", "KR", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VA", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW" };
		private static string GenerateCountriesItemsHtml(bool abbrevOnly)
		{
			return (abbrevOnly)
				? GenerateListBoxItemsHtml(_countriesAbbrevs)
				: GenerateListBoxItemsHtml(_countriesNames, _countriesAbbrevs);
		}

		private static readonly string[] _monthsNames = new[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
		private static readonly string[] _monthsAbbrevs = new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Au", "Sep", "Oct", "Nov", "Dec" };
		private static readonly string[] _monthsIndexes = new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
		private static string GenerateMonthsItemsHtml(bool abbrevOnly)
		{
			return (abbrevOnly)
				? GenerateListBoxItemsHtml(_monthsAbbrevs, _monthsIndexes)
				: GenerateListBoxItemsHtml(_monthsNames, _monthsIndexes);
		}

		private static readonly string[] _daysIndexes = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
		private static string GenerateDaysItemsHtml()
		{
			return GenerateListBoxItemsHtml(_daysIndexes);
		}

		private static string GenerateYearsItemsHtml(int startDate, int endDate)
		{
			return GenerateListBoxItemsHtml(
				(startDate <= endDate
					? Enumerable.Range(startDate, endDate - startDate + 1)
					: Enumerable.Range(endDate, startDate - endDate + 1))
				.Select(x => x.ToString()).ToArray()
				);
		}

		private static string GenerateHoursItemsHtml(bool is24H)
		{
			return GenerateListBoxItemsHtml(
				(is24H
					? Enumerable.Range(1, 12)
					: Enumerable.Range(1, 24))
				.Select(x => x.ToString()).ToArray()
				);
		}

		private static string GenerateMinutesItemsHtml()
		{
			return GenerateListBoxItemsHtml(
				Enumerable.Range(0, 60).Select(x => x.ToString("0#")).ToArray(),
				Enumerable.Range(0, 60).Select(x => x.ToString()).ToArray());
		}

		private static string GenerateAmPmItemsHtml()
		{
			return GenerateListBoxItemsHtml(new[] { "AM", "PM" }, new[] { "1", "2" });
		}

		private static string GenerateListBoxItemsHtml(string[] names, string[] values = null)
		{
			if (names == null)
				throw new ArgumentNullException("names");
			if (names.Length == 0)
				throw new ArgumentException("Array is empty", "names");
			if (values != null && names.Length != values.Length)
				throw new ArgumentException("values");

			int itemsHashCode = GetItemsHashCode(names, values);
			string html;
			if (!__generatedItems.TryGetValue(itemsHashCode, out html))
			{
				lock (Locker)
				{
					if (!__generatedItems.TryGetValue(itemsHashCode, out html))
					{
						var htmlWriter = new HtmlWriter(HtmlWriterSettings.None);
						for (int i = 0; i < names.Length; i++)
						{
							if (values == null)
							{
								htmlWriter
									.Tag("option")
									.WriteEncoded(names[i])
									.End();
							}
							else
							{
								htmlWriter
									.Attr("value", values[i])
									.Tag("option")
									.WriteEncoded(names[i])
									.End();
							}
						}

						html = htmlWriter.GetContent();
						__generatedItems.Add(itemsHashCode, html);
					}
				}
			}
			return html;
		}

		private static int GetItemsHashCode(string[] names, string[] values)
		{
			int r = names.GetHashCode();
			if (values != null)
				r ^= values.GetHashCode();
			return r;
		}
		#endregion

		private string InputSeries(string name, object value, int[] len, string[] pad)
		{
			form__defs__["class"] = "input-text";

			value = TryToTrim(value);

			bool here = false;
			string x;
			string[] v = new string[len.Length];
			if (!IsReset(name))
			{
				for (int i = 1; i < v.Length; i++)
				{
					string key = name + ":" + i.ToString();
					if (_page.FormIn.Exists(key))
					{
						here = true;
						x = ""; //_page.FormIn.FmValue(key);
						v[i - 1] = JsCompatibility.HasValue(x) ? x : "";
					}
					else
					{
						v[i - 1] = "";
					}
				}
			}
			if (!here)
			{
				string valueStr = JsCompatibility.ToString(value);
				for (int i = 0; i < v.Length; i++)
				{
					v[i] = valueStr.Substring(0, len[i]);
					valueStr = valueStr.Substring(len[i]);
				}
			}

			StringBuilder template = new StringBuilder();
			int n;	
			for (n = 0; n < v.Length; n++)
			{
				if (n < pad.Length)
				{
					template.Append(pad[n]);
				}
				else if (n != 0)
				{
					template
						.Append(HtmlUtility.NonBreakingSpacer)
						.Append('-')
						.Append(HtmlUtility.NonBreakingSpacer);
				}

				template
					.Append(HtmlUtility.OpenTag("input",
							HtmlUtility.Attribute("type", "text"),
							HtmlUtility.Attribute("name", name + ":" + (n + 1).ToString()),
							HtmlUtility.Attribute("value", HtmlUtility.HtmlEncode(v[n])),
							HtmlUtility.Attribute("size", len[n].ToString()),
							HtmlUtility.Attribute("maxlength", len[n].ToString())))
							.Append("<<auto_jump: onkeypress=\"return auto_jump(&quot;").Append(name).Append(":").Append(n + 2).Append("&quot;);\">>")
							.Append("<<2+: @>>")
							.Append("<<readonly: " + HtmlUtility.Attribute("style", HtmlUtility.GetStyleRule("border-style", "dotted")) + ">>")
					.Append(HtmlUtility.CloseTag(true));
			}
			if (n < pad.Length)
				template.Append(pad[n]);

			string tmp = template.ToString();
			return JsCompatibility.HasValue(tmp)
				? Form__Build__(name, template + "<<error>>")
				: "";
		}

		private static string MakeItemSelectedByValue(string items, string value)
		{
			return MakeItemSelectedByValue(items, value, HtmlUtility.AttributeQuoteType);
		}
		private static string MakeItemSelectedByValue(string items, string value, AttributeQuoteType attrQuoteType)
		{
			string html;
			if (attrQuoteType == AttributeQuoteType.None)
			{
				html = HtmlUtility.OpenTag("option", HtmlUtility.Attribute("value", AttributeQuoteType.None, value));
				var regExpr = new Regex(html + "(?=\\s|>)", RegexOptions.IgnoreCase);
				return regExpr.Replace(items, HtmlUtility.AttributeListPad(html, "selected"));
			}

			html = HtmlUtility.OpenTag("option", HtmlUtility.AttributePad("value", attrQuoteType, value));
			return items.Replace(html, HtmlUtility.AttributeList(html, "selected"));
		}
		private static string MakeItemSelectedByName(string items, string name)
		{
			string html = ">" + name + HtmlUtility.EndTag("option");
			return items.Replace(html, HtmlUtility.AttributeListPad("selected") + html);
		}

		private static object TryToTrim(object value)
		{
			var asStr = value as string;
			return (asStr != null)
				? asStr.Trim()
				: value;
		}
		private static string TryToTrim(string value)
		{
			return (value != null)
				? value.Trim()
				: null;
		}

		private string RemoveSpaces(string name)
		{
			var s = new List<char>();
			for (int i = 0; i < name.Length; i++)
			{
				if (name[i] != ' ')
					s.Add(name[i]);
			}
			return new String(s.ToArray());
		}

		private bool IsReset(string name)
		{
			return Has(form__glob__["form_reset"]) || Has(form__glob__[name + "!reset"]) || Has(form__locl__[name + "!reset"]);
		}

		private bool IsRequired(string prefix)
		{
			return Has(form__locl__["required"]) ? JsCompatibility.HasValue(form__locl__["required"]) : Has(form__glob__[prefix + "_required"]);
		}

		private static string GetHolidaysData()
		{
			throw new NotImplementedException();
		}

		private void WriteFormIdHtml(HtmlWriter writer)
		{
			if (!JsCompatibility.HasValue(form__form_id__))
			{
				form__form_id__ = ""; //TODO: ap.uuidString();
				_page.Common.SetLocal(FormIdLocalName, form__form_id__);
			}
			
			writer
				.Hidden(FormIdLocalName, form__form_id__)
				.Hidden("__method", "")
				.Hidden("__argument", "")
				.WriteComment("$$[errorcode:" + _page.Errors.SerializeErrors(";", "", "") + "]");
		}

		private string form__set(string str, string name, string value)
		{
			string r, t = "";
			object s = null;
			int j;
			int i;

			if (JsCompatibility.TryParseInt(name, out i))
			{
				if (name[name.Length - 1] == '+')
				{
					var strBuilder = new StringBuilder();
					for (; i < form__argv__.Length; i++)
					{
						t = form__argv__[i].ToString();
						if (!String.IsNullOrEmpty(t))
						{
							j = t.IndexOf('=');
							if (j > 0)
							{
								if (j == t.Length - 1)
									continue;
								r = t.Substring(0, j);
							}
							else
							{
								r = t;
							}

							if (!form__temp__.Contains("<<" + r + ":") && !form__temp__.Contains("<<" + r + ">"))
							{
								if (value.Contains('@'))
									strBuilder.Append(value.Replace("@", t));
							}
							else if (value.Contains("$"))
							{
								strBuilder.Append(value.Replace("$", HtmlUtility.HtmlEncode(t)));
							}
							else
							{
								strBuilder.Append(t);
							}
						}
					}
					return strBuilder.ToString();
				}
				else
					s = form__argv__[i - 1];
			}

			if ((s = tE(s, form__locl__[name], form__glob__[name], form__defs__[name])) == null)
			{
				return "";
			}

			string s1 = "";
			switch (Type.GetTypeCode(s.GetType()))
			{
				case TypeCode.String:
					s1 = (string)s;
					break;
				case TypeCode.Decimal:
				case TypeCode.Double:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.Single:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
					/* @imported
					/ Converting number to string explicitly cause rounding error on AMD proccesors (should be investigated more)
					/ Until you are really need this, do not uncomment the next line
					/ s = s.ToString();
					*/
					break;
				case TypeCode.Object:
					s1 = s.ToString();
					break;
				case TypeCode.Boolean:
					return (bool)s ? value : "";

				default:
					return "";
			}

			if (value.Contains('@'))
				return value.Replace("@", s1);
			if (value.Contains('$'))
				return value.Replace("$", HtmlUtility.HtmlEncode(s1));
			if (!str.Contains(':'))
				return s1;

			return value;
		}

		public string Form__Build__(string name, string template)
		{
			if (Has(form__locl__["error"]) || !Has(form__locl__["noerror"]) && !Has(form__glob__["noerror"]))
			//if (locl["error"] != null || !(locl["noerror"] != null || glob["noerror"] != null))
			{
				if (_page.Errors.HasOneError(name))
					form__locl__.Error = HtmlUtility.Img(_page.GetImageUrl("spacer.gif"), width: 17, height: 15, alt: "Error");
				else if (Has(form__locl__["no-space-error"]) || Has(form__glob__["no-space-error"]))
					form__locl__.Error = "";
				else if (Has(form__locl__["nospace"]) || Has(form__glob__["nospace"]))
					form__locl__.Error = "";
				else
					form__locl__.Error = HtmlUtility.Img(_page.GetImageUrl("spacer.gif"), width: 17, height: 15);
			}
			else
				form__locl__.Error = "";
			form__temp__ = template + "<<face:>><<noerror:>><<reset:>>";

			return _r.Replace(form__temp__, (match) =>
				{
					return form__set(match.Value, match.Groups["name"].Value, match.Groups["val"].Value);
				});
		}
		private static readonly Regex _r = new Regex("<<(?<name>[^:>]*):?(?<val>.*?)>>", RegexOptions.Compiled | RegexOptions.ExplicitCapture);

		private static ImageInfo Find(string name)
		{
			ImageInfo info;
			return (Path.HasExtension(name) || !_images.TryGetValue(ConvertLegacyName(name), out info)) ? null : info;
		}

		private static string ConvertLegacyName(string name)
		{
			return name.Replace('-', '.').Replace(' ', '.').Replace('_', '.');
		}


		#region Nested Types
		private class DynamicBag: JsObject<object>
		{
			public void Parse(object[] args, int count = 0)
			{
				if (args == null)
					throw new ArgumentNullException("args");
				if (count < 0)
					throw new ArgumentOutOfRangeException("count");

				if (args.Length == 0)
					return;

				dynamic th = this;
				for (int i = count; i < args.Length; i++)
				{
					string s = args[i].ToString();
					int j = s.IndexOf('=');
					if (j > 0)
						th[s.Substring(0, j)] = s.Substring(j + 1);
					else
						th[s] = s;
				}
			}

			public void Reset()
			{
				Storage.Clear();
			}
		}

		private enum ButtonAction
		{
			Button = 0,
			Submit = 1,
			Cancel = 2,
			Update = 3
		}

		[Flags]
		private enum ImageType
		{
			Button = 1,
			Alert = 2
		}

		private class ImageInfo
		{
			public readonly string ButtonTitle;
			public readonly int ButtonWidth;
			public readonly ImageType Flags;
			public readonly string Url;
			public readonly int Width;
			public readonly int Height;
			public readonly string Title;

			public ImageInfo(string buttonTitle, int buttonWidth, ImageType flags, string url, int width, int height, string title)
			{
				ButtonTitle = buttonTitle;
				ButtonWidth = buttonWidth;
				Flags = flags;
				Url = url;
				Width = width;
				Height = height;
				Title = title;
			}
		}
		#endregion
		#endregion
	}
}
