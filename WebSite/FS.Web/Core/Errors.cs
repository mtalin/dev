﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class Errors
	{
		private const string ErrorDelimiter = ";";
		private const string MessageStartToken = ";:";
		private const string MessageEndToken = ":;";
		private readonly PageCommon _page;

		private HashSet<string> _errors = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
		private HashSet<string> _messages = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

		internal Errors(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public string[] GetAllMessages()
		{
			return _messages.ToArray();
		}

		public string[] GetAllErrors()
		{
			return _errors.ToArray();
		}

		public void RemoveAllErrors()
		{
			_errors.Clear();
		}

		public void RemoveAllMessages()
		{
			_messages.Clear();
		}

		// was "oErrorIcon"
		public string GetErrorIcon(string name)
		{
			throw new NotImplementedException();
		}

		// was "oErrorIconWithoutSpace"
		// @imported: Same as oErrorIcon, but doesn't reserve space for error triangle
		public string GetErrorIconNoSpace(string name)
		{
			throw new NotImplementedException();
		}

		public string AttachPrefix(string prefix, params string[] errors)
		{
			if (prefix == null)
				throw new ArgumentNullException("prefix");

			if (errors != null && errors.Length > 0)
			{
				StringBuilder strBuilder = new StringBuilder();
				for (int i = 0; i < errors.Length; i++)
				{
					string[] errorSplitted = SplitErrors(errors[i]);
					for (int n = 0; n < errorSplitted.Length; n++)
					{
						strBuilder
							.Append(ErrorDelimiter)
							.Append(prefix)
							.Append(errorSplitted[n])
							.Append(ErrorDelimiter);
					}
				}
				return strBuilder.ToString();
			}
			return "";
		}

		/// <summary>
		/// Add errors codes and/or messages into current state if it isn't exists already.
		/// </summary>
		/// <param name="errors"></param>
		public void AddError(params string[] errors)
		{
			if (errors == null || errors.Length == 0)
				return;

			bool isCode = true;
			for (int i = 0; i < errors.Length; i++)
			{
				if (isCode)
				{
					string[] splittedError = SplitErrors(errors[i]);
					for (int n = 0; n < splittedError.Length; n++)
					{
						_errors.Add(splittedError[n]);
					}
				}
				else
				{
					_messages.Add(errors[i]);
				}
				isCode = !isCode;
			}
		}

		/// <summary>
		/// Add messages into current state if it isn't exists already.
		/// </summary>
		/// <param name="messages"></param>
		public void AddMessage(params string[] messages)
		{
			if (messages == null || messages.Length == 0)
				return;

			for (int i = 0; i < messages.Length; i++)
			{
				_messages.Add(messages[i]);
			}
		}

		public string CutMessages(string message)
		{
			int i1, i2;
			i1 = message.IndexOf(MessageStartToken);
			i2 = message.IndexOf(MessageEndToken, i1 + 2);
			while (i1 >= 0 && i2 > i1)
			{
				AddMessage(message.Substring(i1 + 2, i2));
				message = message.Substring(0, i1) + message.Substring(i2 + 1);
				i1 = message.IndexOf(MessageStartToken);
				i2 = message.IndexOf(MessageEndToken, i1 + 2);
			}
			return message;
		}

		/// <summary>
		/// Remove errors codes from local error state.
		/// </summary>
		/// <param name="errors"></param>
		public void RemoveError(params string[] errors)
		{
			if (errors == null || errors.Length == 0)
				return;

			for (int i = 0; i < errors.Length; i++)
			{
				_errors.Remove(errors[i]);
			}
		}

		/// <summary>
		/// Remove errors messages from local error state.
		/// </summary>
		/// <param name="messages"></param>
		public void RemoveMessage(params string[] messages)
		{
			if (messages == null || messages.Length == 0)
				return;

			for (int i = 0; i < messages.Length; i++)
			{
				_messages.Remove(messages[i]);
			}
		}

		/// <summary>
		/// Add message if errors code ALREADY EXISTS in the curret error state.
		/// </summary>
		/// <param name="errorsVsMessages"></param>
		public void SetMessage(params string[] errorsVsMessages)
		{
			if (errorsVsMessages == null || errorsVsMessages.Length == 0)
				return;
			if (errorsVsMessages.Length % 2 != 0)
				throw new ArgumentException("Errors and messages must be paired up", "errorsVsMessages");

		NextPair:
			for (int i = 0; i < errorsVsMessages.Length - 1; i += 2)
			{
				string error = errorsVsMessages[i];
				string message = errorsVsMessages[i + 1];
				if (JsCompatibility.HasValue(message))
				{
					string[] splittedError = SplitErrors(error);
					for (int n = 0; n < splittedError.Length; n++)
					{
						if (!_errors.Contains(splittedError[n]))
							goto NextPair;
					}
					_messages.Add(message);
				}
			}
		}

		/// <summary>
		/// Replace error code with a message.
		/// </summary>
		/// <param name="errorsVsMessages"></param>
		public void SetErrorMessage(params string[] errorsVsMessages)
		{
			if (errorsVsMessages == null || errorsVsMessages.Length == 0)
				return;
			if (errorsVsMessages.Length % 2 != 0)
				throw new ArgumentException("Errors and messages must be paired up", "errorsVsMessages");

		NextPair:
			for (int i = 0; i < errorsVsMessages.Length - 1; i += 2)
			{
				string error = errorsVsMessages[i];
				string message = errorsVsMessages[i + 1];
				if (JsCompatibility.HasValue(message))
				{
					string[] splittedError = SplitErrors(error);
					for (int n = 0; n < splittedError.Length; n++)
					{
						if (!_errors.Contains(splittedError[n]))
							goto NextPair;
					}
					for (int n = 0; n < splittedError.Length; n++)
					{
						if (!_errors.Contains(splittedError[n]))
							RemoveError(splittedError[n]);
					}
					_messages.Add(message);
				}
			}
		}

		/// <summary>
		/// Checks if given error codes are already set
		/// </summary>
		/// <param name="errors"></param>
		/// <returns></returns>
		public bool HasError(params string[] errors)
		{
			if (errors == null || errors.Length == 0)
				return false;

		NextPair:
			for (int i = 0; i < errors.Length - 1; i += 2)
			{
				string error = errors[i];
				string message = errors[i + 1];
				if (JsCompatibility.HasValue(message))
				{
					string[] splittedError = SplitErrors(error);
					for (int n = 0; n < splittedError.Length; n++)
					{
						if (!_errors.Contains(splittedError[n]))
							goto NextPair;
					}
					return true;
				}
			}
			return false;
		}

		public bool HasOneError(string error)
		{
			return _errors.Contains(error);
		}

		/// <summary>
		/// Check if there are no any error codes exist.
		/// </summary>
		/// <returns></returns>
		public bool NoErrors()
		{
			return _errors.Count == 0;
		}

		/// <summary>
		/// Check if there are no any messages exist.
		/// </summary>
		/// <returns></returns>
		public bool NoMessages()
		{
			return _messages.Count == 0;
		}
		// was "oMessages"
		public string GenerateMessagesHtml(string divider = null, string prefix = null, string suffix = null)
		{
			divider = divider ?? HtmlUtility.Br;
			prefix = prefix ?? "";
			prefix = suffix ?? "";

			return SerializeMessages(divider, prefix, suffix);
		}

		// was "error__oCodes__"
		internal string SerializeErrors(string divider, string prefix = null, string suffix = null)
		{
			if (divider == null)
				throw new ArgumentNullException("divider");

			string result = String.Join(divider, _errors); ;
			if (prefix != null)
				result = prefix + result;
			if (suffix != null)
				result = result + suffix;
			return result;
		}
		// was "error__oMessages__"
		internal string SerializeMessages(string divider, string prefix = null, string suffix = null)
		{
			if (divider == null)
				throw new ArgumentNullException("divider");

			if (_messages.Count == 0)
				return null;

			string result = String.Join(divider, _messages);
			if (prefix != null)
				result = prefix + result;
			if (suffix != null)
				result = result + suffix;
			return result;
		}


		#region Implementation
		private string[] SplitErrors(string errors)
		{
			if (!JsCompatibility.HasValue(errors))
				return new string[0];

			errors = _r.Replace(errors, ErrorDelimiter);
			return errors.Split(new[] { ErrorDelimiter }, StringSplitOptions.RemoveEmptyEntries);
		}
		private static readonly Regex _r = new Regex("\\s*" + ErrorDelimiter + "+\\s*", RegexOptions.Compiled);
		#endregion
	}
}
