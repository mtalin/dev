﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class PanelNameValue : PanelFrame
	{
		internal PanelNameValue(PageCommon page)
			: base(page)
		{
			A0.SetDefaults(new Dictionary<string, string>()
				{
					{ "PanelClass", "Panel" },
					{ "BorderClass", "PanelBorder" },
					{ "NoBorderClass", null },
					{ "TitleClass", "PanelTitle" },
					{ "FieldsetClass", "Fieldset" },
					{ "FieldsetBorderClass", null },
					{ "FieldsetNoBorderClass", "FieldsetNoBorder" },
					{ "FieldsetTitleClass", "FieldsetTitle" },
					{ "NameClass", "NameCell" },
					{ "ValueClass", "PanelCell" }
				});

			A0.Extend(new Dictionary<string, string>()
				{
					{ "ID", null },
					{ "title", null },
					{ "type", null },
					{ "class", null },
					{ "frameclass", null },
					{ "titleclass", null },
					{ "cellclass", null },
					{ "cellvalign", null },
					{ "nameclass", null },
					{ "valueclass", null },
					{ "namewidth", "132" },
					{ "valuewidth", null },
					{ "width", "100%" },
					{ "height", null },
					{ "border", null },
					{ "noborder", null },
					{ "cols", null },
					{ "padding", null },
					{ "namealign", null },
					{ "valuealign", null },

					// compatibility
					{ "fieldset", null }
				});

			_columns = 2;
		}

		public override string Name
		{
			get { return "panel-namevalue"; }
		}

		protected override void CellCore(params string[] args)
		{
			CellId += (_cspan - 1);

			bool isName = CellId % 2 == 1;
			dynamic parms0 = A0;
			dynamic parms2 = A1;
			dynamic parms = A2 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "class", null },
					{ "colspan", "1" },
					{ "rowspan", "1" },
					{ "width", null },
					{ "height", null },
					{ "nowrap", null },
					{ "align", isName ? parms0.NameAlign : parms0.ValueAlign },
					{ "valign", parms0.CellValign },
				}, A1);
			A2.ParseWithLog(this, args);

			if (CellId > _columns)
				_columns = CellId;

			int cspan = JsCompatibility.ParseInt(parms.Colspan);
			int rspan = JsCompatibility.ParseInt(parms.Rowspan);

			_cspan = (cspan > 1) ? cspan : 1;

			CssClass = tE(A2.Attribute("class"), A0.Attribute("nameclass", "class"), A0.Attribute("cellclass", "class", "nameclass"));
			HtmlWriter writer = Writer;
			if (isName)
			{
				writer
					.Attr(CssClass)
					.Attr(tE(A2.Attribute("width"), cspan < 2 ? A2.Attribute("namewidth", "width") : null))
					.Attr(tE(A2.Attribute("height"), rspan < 2 ? A2.Attribute("height") : null))
					.Attr(A2.Attribute("valign"));
				if (cspan > 1)
					writer.Attr("colspan", cspan.ToString());
				if (rspan > 1)
					writer.Attr("rowspan", rspan.ToString());
				if (Has(parms.NoWrap))
					writer.Attr("nowrap", "nowrap");
				if (Has(parms.Align))
					writer.AttrStyle("text-align", parms.Align);
				writer
					.Tag("td");
			}
			else
			{
				CssClass = tE(A2.Attribute("class"), A0.Attribute("valueclass", "class"), A0.Attribute("cellclass", "class", "valueclass"));
				writer
					.Attr(CssClass)
					.Attr(tE(A2.Attribute("width"), cspan < 2 ? A2.Attribute("valuewidth", "width") : null))
					.Attr(tE(A2.Attribute("height"), rspan < 2 ? A2.Attribute("height") : null))
					.Attr(A2.Attribute("valign"));
				if (cspan > 1)
					writer.Attr("colspan", cspan.ToString());
				if (rspan > 1)
					writer.Attr("rowspan", rspan.ToString());
				if (Has(parms.NoWrap))
					writer.Attr("nowrap", "nowrap");
				if (Has(parms.Align))
					writer.AttrStyle("text-align", parms.Align);
				writer
					.Tag("td");
			}
			SetCellEnding(() => writer.End("td"));
		}
	}
}
