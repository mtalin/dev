﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lexxys;

namespace FS.Web.Core
{
	public sealed class ClientScripting
	{
		private List<_ScriptInfo> _includes;
		private Dictionary<string, _ScriptInfo> _blocks;
		private Dictionary<string, _ScriptInfo> _startups;

		private bool _msAjaxRegistered;
		private bool _msAjaxUIRegistered;
		private bool _jqueryRegistered;
		private bool _jqueryUIRegistered;
		private bool _isCallback;

		private readonly PageBase _page;

		internal ClientScripting(PageBase page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public bool IsCallback
		{
			get { return _isCallback; }
			internal set { _isCallback = value; }
		}

		public void RegisterBlock(string key, string content, bool replaceExisting = false)
		{
			if (key == null)
				throw new ArgumentNullException("key");
			if (content == null)
				throw new ArgumentNullException("content");

			if (_blocks == null)
				_blocks = new Dictionary<string, _ScriptInfo>(StringComparer.OrdinalIgnoreCase);
			else if (_blocks.ContainsKey(key) && !replaceExisting)
				return;

			_blocks[key] = new _ScriptInfo(content);
		}

		public void RegisterStartup(string key, string content, bool replaceExisting = false)
		{
			if (key == null)
				throw new ArgumentNullException("key");
			if (content == null)
				throw new ArgumentNullException("content");

			if (_startups == null)
				_startups = new Dictionary<string, _ScriptInfo>(StringComparer.OrdinalIgnoreCase);
			else if (_startups.ContainsKey(key) && !replaceExisting)
				return;

			// we use jquery feature such as document onready handlers
			RequireJQuery(false);
			_startups[key] = new _ScriptInfo(content);
		}

		public void RegisterInclude(string path, bool isLibrary = false)
		{
			if (path == null)
				throw new ArgumentNullException("path");

			if (_includes == null)
				_includes = new List<_ScriptInfo>();
			else if (_includes.Exists(item => item.Path.Equals(path, StringComparison.OrdinalIgnoreCase)))
				return;

			if (!Path.HasExtension(path))
				path = Path.ChangeExtension(path, "js");
			_includes.Add(new _ScriptInfo(path, isLibrary));
		}

		public void RequireMsAjax(bool includeUI)
		{
			if (_msAjaxRegistered && (_msAjaxUIRegistered || !includeUI))
				return;

			if (!_msAjaxRegistered)
			{
				RegisterInclude(_page.GetScriptUrl("ajax", "core.min.js"), true);
				RegisterInclude(_page.GetScriptUrl("ajax", "common.min.js"), true);
				RegisterInclude(_page.GetScriptUrl("ajax", "base.min.js"), true);

				_msAjaxRegistered = true;
			}

			if (!_msAjaxUIRegistered || includeUI)
			{
				RegisterInclude(_page.GetScriptUrl("ajax", "ui.blocking.min.js"), true);
				RegisterInclude(_page.GetScriptUrl("ajax", "ui.animations.min.js"), true);
				RegisterInclude(_page.GetScriptUrl("ajax", "ui.popup.min.js"), true);
				RegisterInclude(_page.GetScriptUrl("ajax", "ui.hover.min.js"), true);
				RegisterInclude(_page.GetScriptUrl("ajax", "ui.dropdown.min.js"), true);

				_msAjaxUIRegistered = true;
			}
		}
		public void RequireJQuery(bool includeUI)
		{
			if (_jqueryRegistered && (_jqueryUIRegistered || !includeUI))
				return;

			if (!_jqueryRegistered)
			{
				RegisterInclude(_page.GetScriptUrl("jquery", "jquery.min.js"), true);
				RegisterInclude(_page.GetScriptUrl("jquery", "jquery.addons.js"), true);

				_jqueryRegistered = true;
			}

			if (!_jqueryUIRegistered || includeUI)
			{
				RegisterInclude(_page.GetScriptUrl("jquery", "jquery.ui.min.js"), true);

				_jqueryUIRegistered = true;
			}
		}

		public static void ScriptExternal(HtmlWriter writer, string url)
		{
			ScriptExternal(writer, new _ScriptInfo(url, false));
		}

		public static void BeginScript(HtmlWriter writer, string comment = null)
		{
			writer
				.WriteComment(comment ?? "script block")
				.Attr("type", "text/javascript")
				.Tag("script");
		}
		public static void EndScript(HtmlWriter writer)
		{
			writer.End("script");
		}


		internal void WriteStartups()
		{
			if (_startups != null && _startups.Count > 0)
			{
				using (var writer = new HtmlWriter(_page.Writer))
				{
					BeginScript(writer, "Startup scripts");
					writer.Write("$(document).ready(function() {");
					foreach (var item in _startups)
					{
						writer.Write(item.Value.Content);
					}
					writer.Write("});");
					EndScript(writer);
				}
			}
		}
		internal void WriteStartupByKey(string key)
		{
			if (key == null)
				throw new ArgumentNullException("key");

			if (_startups != null && _startups.Count > 0)
			{
				_ScriptInfo script;
				if (_startups.TryGetValue(key, out script))
				{
					using (var writer = new HtmlWriter(_page.Writer))
					{
						BeginScript(writer, "Startup scripts");
						writer.Write("$(document).ready(function() {");
						writer.Write(script.Content);
						writer.Write("});");
						EndScript(writer);
						_startups.Remove(key);
					}
				}
			}
		}

		internal void WriteIncludes()
		{
			if (_includes != null && _includes.Count > 0)
			{
				using (var writer = new HtmlWriter(_page.Writer))
				{
					foreach (var item in _includes.OrderByDescending(t => t.IsLibrary ? 1 : 0))
					{
						ScriptExternal(writer, item);
					}
				}
			}
		}

		internal void WriteBlocks()
		{
			if (_blocks != null && _blocks.Count > 0)
			{
				using (var writer = new HtmlWriter(_page.Writer))
				{
					BeginScript(writer);
					foreach (var item in _blocks)
					{
						writer.Write(item.Value.Content);
					}
					EndScript(writer);
				}
			}
		}


		#region Implementation
		private static void ScriptExternal(HtmlWriter writer, _ScriptInfo script)
		{
			writer
				.Attr("src", script.Path)
				.Attr("type", "text/javascript")
				.Tag("script")
				.End();
		}

		#region Nested Types
		private class _ScriptInfo
		{
			public _ScriptInfo(string content)
			{
				Content = content;
			}

			public _ScriptInfo(string path, bool isLibrary)
			{
				Path = path;
				IsLibrary = isLibrary;
			}

			public string Path;

			public string Content;

			public bool IsLibrary;
		}
		#endregion
		#endregion
	}

	public static class HtmlWriterExtensions
	{
		public static HtmlWriter ScriptExternal(this HtmlWriter writer, string url)
		{
			ClientScripting.ScriptExternal(writer, url);
			return writer;
		}

		public static HtmlWriter BeginScript(this HtmlWriter writer, string comment = null)
		{
			ClientScripting.BeginScript(writer, comment);
			return writer;
		}
		public static HtmlWriter EndScript(this HtmlWriter writer)
		{
			ClientScripting.EndScript(writer);
			return writer;
		}
	}
}
