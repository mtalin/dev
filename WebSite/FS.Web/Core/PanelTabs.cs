﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class PanelTabs : PanelBase
	{
		private PanelBase _updatePanel;
		private string _updatePanelId;
		private string _clientInstanceName;
		private string _script;
		private string _startupScript;
		private string  _scriptkey;
		private Dictionary<string, _TabInfo> _tabsInfo;
		private bool _writingTabs;
		private string _activeTab;

		private HtmlWriter _tabWriter1;
		private HtmlWriter _tabWriter2;

		internal PanelTabs(PageCommon page)
			: base(page, false)
		{
			A0.Extend(new Dictionary<string, string>()
				{
					{ "ID", null },
					{ "width", "\"auto\"" },
					{ "height", "\"auto\"" },
					{ "async", null }
				});
		}

		public override string Name
		{
			get { return "panel-tabs"; }
		}

		protected override void BeginCore(params string[] args)
		{
			dynamic parms = A0;
			A0.ParseWithLog(this, args);

			string id = tE(parms.Id, "PanelTabs" + PanelId.ToString());
			_updatePanelId = id + "__updatePanel";
			_clientInstanceName = id + "__behavior";

			if (Has(parms.Async))
				_updatePanel = new PanelUpdate(Page);

			Writer
				.Attr("id", id)
				.Tag("div")
					.Attr("border", "0")
					.Attr("cellspacing", "0")
					.Attr("cellpadding", "0")
					.AttrStyle("width", "100%")
					.Tag("table");
		}

		protected override void EndCore()
		{
			EnsureTabs();

			if (_updatePanel != null)
				_updatePanel.End();

			SetCellEnding(null, true);
			SetRowEnding(null, true);

			Writer
					.End("table")
				.End("div");

			PrepareStartupScript();

			Page.ClientScript.RegisterBlock(_scriptkey, _script);
			Page.ClientScript.RegisterStartup(_scriptkey, _startupScript);

			_tabWriter1.Dispose();
			_tabWriter2.Dispose();
		}

		protected override void RowCore(params string[] args)
		{
			dynamic parms = A1 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "type", null },			// @imported: values = header
					{ "active", null }
				});
			A1.ParseWithLog(this, args);

			_activeTab = parms.Active;
			EnsureTabs();

			HtmlWriter writer = Writer;
			if (_writingTabs = eQ(parms.Type, "header"))
			{
				if (_tabWriter1 != null)
					_tabWriter1.Clear();
				if (_tabWriter2 != null)
					_tabWriter2.Clear();

				writer
					.Tag("tr")
						.Tag("td")
							.Attr("border", "0")
							.Attr("cellspacing", "0")
							.Attr("cellpadding", "0")
							.Tag("table");

				SetRowEnding(() => writer.End("table").End("td").End("tr"));
			}
			else
			{
				writer
					.Tag("tr")
						.Attr("class", "ContentBackground")
						.AttrStyle("padding", "6px")
						.Tag("td");

				if (_updatePanel != null)
					_updatePanel.Begin("id=" + _updatePanelId);

				SetRowEnding(() => writer.End("td").End("tr"));
			}

			SetCellEnding(null);
		}

		protected override void CellCore(params string[] args)
		{
			dynamic parms = A2 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "name", null },
					{ "title", null },
					{ "id", null },
					{ "url", null },
					{ "active", null },
					{ "onSelect", null }
				});
			A2.ParseWithLog(this, args);

			if (_writingTabs && !String.IsNullOrWhiteSpace(parms.Name))
			{
				bool isActive = Has(parms.Active) || eQ(_activeTab, parms.Name);
				string linkId = RemoveInvalidChars(parms.Name) + "__link";
				//string link = oHref(localParams.url, localParams.name, (isActive) ? "class=TabCurrent" : "class=TabNormal", localParams.title, "id=" + linkId, $FS.StringFormat("onclick='return !{0}.SetActiveTab(\"{1}\");'", _clientInstanceName, localParams.name));
				string link = "<a href='" + tE(parms.Url, "") + "' class='" + (isActive ? "TabCurrent" : "TabNormal") + "' " + tE(parms.Title, "") + " id='" + linkId + "'>" + tE(parms.Name, "") + "</a>";

				if (_tabsInfo == null)
					_tabsInfo = new Dictionary<string, _TabInfo>(StringComparer.OrdinalIgnoreCase);
				_tabsInfo.Add(parms.Name, new _TabInfo()
					{
						Name = parms.Name,
						Title = parms.Title,
						IsActive = isActive,
						Url = parms.Url,
						OnSelect = parms.OnSelect,
						LinkId = linkId
					});

				if (_tabWriter1 == null)
					_tabWriter1 = new HtmlWriter(HtmlWriterSettings.None);
				if (_tabWriter2 == null)
					_tabWriter2 = new HtmlWriter(HtmlWriterSettings.None);

				_tabWriter1
					.Tag("td")
						.Img(Page.GetImageUrl(isActive ? "tab_sel_left.gif" : "tab_nonsel_left.gif"), width: 17, height: 17)
					.End()
					.Attr("class", isActive ? "TabCurrent" : "TabNormal")
					.Attr("valign", "bottom")
					.Attr("nowrap", "nowrap")
					.Tag("td")
						.Write(link)
					.End()
					.Tag("td")
						.Img(Page.GetImageUrl(isActive ? "tab_sel_right.gif" : "tab_nonsel_right.gif"), width: 17, height: 17)
					.End();
				_tabWriter2
					.Attr("colspan", "3")
					.Tag("td")
						.Img(Page.GetImageUrl("spacer.gif"), width: 1, height: 1)
					.End();
			}

			SetCellEnding(null, true);
		}


		#region Implementation

		private void PrepareStartupScript()
		{
			var strBuilder = new StringBuilder()
				.AppendFormat("{0} = new $FS.Asp.PanelTabs();", _clientInstanceName)
				.AppendLine()
				.AppendFormat("{0}.ActiveCssClass = 'TabCurrent';", _clientInstanceName)
				.AppendLine()
				.AppendFormat("{0}.NormalCssClass = 'TabNormal';", _clientInstanceName)
				.AppendLine();

			if (_updatePanel != null)
			{
				strBuilder
					.AppendFormat("{0}.UpdatePanelId = '{1}';", _clientInstanceName, _updatePanelId)
					.AppendLine();
			}

			if (_tabsInfo != null)
			{
				foreach (var item in _tabsInfo)
				{
					_TabInfo tabInfo = item.Value;
					strBuilder
						.AppendFormat("{0}.Tabs.Add({{ Name: \"{1}\", Url: \"{2}\", LinkId: \"{3}\", OnSelect: function() {{ return {4}; }} }});", _clientInstanceName, tabInfo.Name, tabInfo.Url, tabInfo.LinkId, tabInfo.OnSelect)
						.AppendLine();

					if (tabInfo.IsActive)
					{
						strBuilder
							.AppendFormat("{0}.SetActiveTab('{1}');", _clientInstanceName, tabInfo.Name)
							.AppendLine();
					}
				}
			}

			_script = strBuilder.ToString();
			strBuilder
				.Clear()
				.AppendFormat("{0}.Init();", _clientInstanceName)
				.AppendLine();
			_startupScript = strBuilder.ToString();
			_scriptkey = _clientInstanceName;
		}

		private void EnsureTabs()
		{
			if (_tabWriter1 == null || _tabWriter2 == null)
				return;

			Writer
				.Tag("tr")
					.Write(_tabWriter1.GetContent())
				.End()
				.Tag("tr")
					.Write(_tabWriter2.GetContent())
				.End();

			_tabWriter1.Clear();
			_tabWriter2.Clear();

			SetRowEnding(null, true);
		}

		private static string RemoveInvalidChars(string value)
		{
			var s = new List<char>();
			for (int i = 0; i < value.Length; i++)
			{
				if (Char.IsLetterOrDigit(value[i]) || value[i] == '_')
					s.Add(value[i]);
			}
			return new String(s.ToArray());
		}

		#region Nested Types
		private class _TabInfo
		{
			public string Name;

			public string Title;

			public bool IsActive;

			public string Url;

			public string OnSelect;

			public string LinkId;
		} 
		#endregion
		#endregion
	}
}
