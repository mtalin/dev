﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public class Include_1 : IRequireInit
	{
		[Include("~/include_4", 0)]
		public Include_4 Include_4;
		[Include("~/include_3", 1)]
		public Include_3 Include_3;

		private readonly PageBase _page;

		public Include_1(PageBase page)
		{
			_page = page;

			Include_4 = new Include_4(page);
			Include_3 = new Include_3(page);
		}

		public string XYZ()
		{
			return "Method XYZ from Include 1 rendered some html";
		}
		public string XYZ(string arg)
		{
			return "Method XYZ(" + arg + ") from Include 1 rendered some html";
		}

		public void Init()
		{
			_page.Response.Write("Include_1 rendered some html.<br/>");
		}
	}

	public class Include_2 : IRequireInit
	{
		private readonly PageBase _page;

		public Include_2(PageBase page)
		{
			_page = page;
		}

		public string XYZ()
		{
			return "Method XYZ from Include 2 rendered some html";
		}

		public void Init()
		{
			//var incl3 = _page.Include<Include_3>("~/include_3");
			//var incl1 = _page.Include<Include_1>("~/include_1");

			_page.Response.Write("Include_2 rendered some html.<br/>");
		}
	}

	public class Include_3 : IRequireInit
	{
		private readonly PageBase _page;

		public Include_3(PageBase page)
		{
			_page = page;
		}

		public string XYZ()
		{
			return "Method XYZ from Include 3 rendered some html";
		}
		public string XYZ(string arg)
		{
			return "Method XYZ(" + arg + ") from Include 3 rendered some html";
		}

		public void Init()
		{
			_page.Response.Write("Include_3 rendered some html.<br/>");
		}
	}

	public class Include_4 : IRequireInit
	{
		private readonly PageBase _page;

		public Include_4(PageBase page)
		{
			_page = page;
		}

		public void Init()
		{
			_page.Response.Write("Include_4 rendered some html.<br/>");

			_page.Response.Write("xyz from Include_4 says: " + _page.External.XYZ() + ".<br/>");
		}
	}

	public class Include_5 : IRequireInit
	{
		private readonly PageBase _page;

		public Include_5(PageBase page)
		{
			_page = page;
		}

		public string XYZ()
		{
			return "Method XYZ from Include 5 rendered some html";
		}

		public void Init()
		{
			_page.Response.Write("<p style='background-color: yellow; padding:0.5em;'>It's time for Include_5 to render some html.</p>");
		}
	}
}
