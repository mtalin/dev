﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class PanelReport : PanelBase
	{
		private PanelBase _updatePanel;
		private bool _isFieldset;
		private bool _stripe;
		private bool _odd = true;
		private string _stripe1;
		private string _stripe2;
		private int _cspan;
		private _GroupType _groupType = _GroupType.None;
		private _GroupType _lastGroupType = _GroupType.None;
		private bool _headerHappened;
		private int[] _colorGroups = new int[64];

		internal PanelReport(PageCommon page)
			: base(page)
		{
			A0.SetDefaults(new Dictionary<string, string>()
				{
					{ "PanelClass", "Panel" },
					{ "HeaderClass", "PanelHeader" },
					{ "FooterClass", "ReportFooter" },
					{ "BodyClass", "PanelCell" },
					{ "TitleClass", "PanelTitle" },
					{ "NavigatorClass", "Repor!Hasavigator" },

					{ "FieldsetClass", "Fieldset" },
					{ "FieldsetBorderClass", null },
					{ "Fieldse!HasoBorderClass", "Fieldse!HasoBorder" },
					{ "FieldsetTitleClass", "FieldsetTitle" },

					{ "BorderClass", "PanelBorder" },
					{ "NoBorderClass", null },

					{ "Stripe1Class", "Stripe2Background" },
					{ "Stripe2Class", "Stripe1Background" },
				});

			A0.Extend(new Dictionary<string, string>()
				{
					{ "ID", null },
					{ "type", null },
					{ "title", null },
					{ "navigator", null },
					{ "updatePanelId", null },
					{ "border", null },
					{ "noborder", null },
					{ "stripe", null },
					{ "nostripe", null },
					{ "width", "100%" },
					{ "padding", "3" },
					{ "align", null },

					{ "class", null },
					{ "titleclass", null },
					{ "headerclass", A0.GetDefaultFor("HeaderClass") },
					{ "footerclass", A0.GetDefaultFor("FooterClass") },
					{ "navigatorclass", A0.GetDefaultFor("NavigatorClass") },
					{ "cellclass", A0.GetDefaultFor("BodyClass") },
					{ "cellvalign", null },
					{ "sorting", "auto" },

					{ "stripe1class", A0.GetDefaultFor("Stripe1Class") },
					{ "stripe2class", A0.GetDefaultFor("Stripe2Class") },

					// compatibility
					{ "fieldset", null }
				});

			_stripe1 = A0.GetDefaultFor("Stripe1Class");
			_stripe2 = A0.GetDefaultFor("Stripe2Class");
		}

		public override string Name
		{
			get { return "panel-report"; }
		}

		protected override void BeginCore(params string[] args)
		{
			dynamic parms = A0;
			A0.ParseWithLog(this, args);

			string id = tE(parms.Id, "PanelReport" + PanelId);
			_isFieldset = eQ(parms.Type, "fieldset");
			_stripe = Has(parms.Stripe);
			bool border = !Has(parms.NoBorder);

			if (!Has(parms.UpdatePanelId))
			{
				_updatePanel = new PanelUpdate(Page);
				_updatePanel.Begin();
			}

			HtmlWriter writer = Writer;
			if (_isFieldset)
			{
				writer
					.Attr(A0.Attribute("id"))
					.Attr("class", tE(parms.FrameClass, A0.GetDefaultFor("FieldsetClass")), A0.GetDefaultFor(border ? "FieldsetBorderClass" : "FieldsetNoBorderClass"));
				if (Has(parms.Height))
					writer.AttrStyle("height", parms.Height);
				writer.Tag("fieldset");

				if (Has(parms.Title))
				{
					writer
						.Attr(A0.Attribute("titleClass", "class", "FieldsetTitleClass"))
						.Tag("legend")
							.Write(HtmlUtility.NonBreakingSpacer, parms.Title, HtmlUtility.NonBreakingSpacer)
						.End();
				}

			writer
					.Attr("border", "0")
					.Attr("cellspacing", "0")
					.Attr("cellpadding", "0")
					.Attr(A0.Attribute("width"))
					.Attr(A0.Attribute("align"))
					.Tag("table");

				border = false;
			}
			else
			{
				writer
					.Attr("border", "0")
					.Attr("cellspacing", "0")
					.Attr("cellpadding", "0")
					.Attr(A0.Attribute("id"))
					.Attr(A0.Attribute("width"))
					.Attr(A0.Attribute("align"))
					.Tag("table");

				if (!String.IsNullOrWhiteSpace(parms.Title))
				{
					writer
						.Tag("tr")
							.Attr("align", "center")
							.Attr(A0.Attribute("titleclass", "class", "TitleClass"))
							.Tag("td")
								.Write(parms.Title)
							.End()
						.End();
				}
			}

			writer
				.Tag("tr")
					.Tag("td")
						.Attr("width", "100%")
						.Attr("border", "0")
						.Attr("cellspacing", "1")
						.Attr("cellpadding", "0")
						.Attr("class", tE(parms["class"], A0.GetDefaultFor("PanelClass")), A0.GetDefaultFor(border ? "BorderClass" : "NoBorderClass"))
						.Tag("table");
		}

		protected override void EndCore()
		{
			dynamic parms = A0;
			HtmlWriter writer = Writer;

			writer
				.End(groupTags[(int)_groupType])
				.End("table")
				.End("td")
				.End("tr");

			DrawPager();

			writer.End("table");
			if (_isFieldset)
				writer.End("fieldset");

			if (_updatePanel != null)
				_updatePanel.End();
		}
		private static readonly string[] groupTags = new[] { null, "tbody", "thead", "tfoot" };

		protected override void RowCore(params string[] args)
		{
			CellId += (_cspan - 1);

			dynamic parms0 = A0;
			dynamic parms = A1 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "class", null },
					{ "header", null },
					{ "footer", null },
					{ "height", null }
				}, A0);

			A1.ParseWithLog(this, args);
			_GroupType thisGroup = _GroupType.None;

			if (Has(parms.Header))
			{
				_groupType = _GroupType.Head;
				if (_headerHappened)
				{
					; // ap.logStr(new String(common__my_url__).concat(" (panel-report GR)", "HEADER Dowbled"));
					thisGroup = _GroupType.Body;
				}
				else
				{
					thisGroup = _GroupType.Head;
					if (_lastGroupType != _GroupType.Head && _lastGroupType != _GroupType.None)
						; // ap.logStr(new String(common__my_url__).concat(" (panel-report GR)", "HEADER"));
				}
			}
			else if (Has(parms.Footer))
			{
				_groupType = _GroupType.Foot;
				if (_lastGroupType == _GroupType.Head)
					_headerHappened = true;
				thisGroup = _GroupType.Foot;
			}
			else
			{
				_groupType = _GroupType.Body;
				if (_lastGroupType == _GroupType.Head)
					_headerHappened = true;
				thisGroup = _GroupType.Body;
			}

			HtmlWriter writer = Writer;
			if (thisGroup != _lastGroupType)
			{
				if (_lastGroupType != _GroupType.None)
					writer.End(groupTags[(int)_lastGroupType]);
				Debug.Assert(thisGroup != _GroupType.None);
					writer.Tag(groupTags[(int)thisGroup]);

				_lastGroupType = thisGroup;
			}

			if (_stripe && _groupType == _GroupType.Body)
			{
				writer
					.Attr("class", parms.Class, _odd ? _stripe1 : _stripe2)
					.Tag("tr");
				_odd = !_odd;
			}
			else
			{
				writer
					.Attr(A1.Attribute("class"))
					.Tag("tr");
			}

			SetRowEnding(() => writer.End("tr"));
			SetCellEnding(null);
		}

		protected override void CellCore(params string[] args)
		{
			dynamic parms0 = A0;
			dynamic parms2 = A1;
			dynamic parms = A2 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "class", (_groupType == _GroupType.Foot) ? parms0.FooterClass : (_groupType == _GroupType.Head) ? parms0.HeaderClass : parms0.CellClass },
					{ "colspan", "1" },
					{ "width", null },
					{ "height", parms2.Height },
					{ "wrap", null },
					{ "nowrap", (_groupType == _GroupType.Head || _groupType == _GroupType.Foot) ? "nowrap" : null },
					{ "align", null },
					{ "valign", parms0.VAlign },
					{ "left", null },
					{ "right", null },
					{ "center", null },
					{ "sort", null },
					{ "sorting", parms0.Sorting },
					{ "type", null },
					{ "val", null },
					{ "group", "0" }
				}, A1);
			A2.ParseWithLog(this, args);

			int cspan = JsCompatibility.ParseInt(parms.Colspan);
			CssClass = parms["class"];
			string colorGroupCssClass = "";

			HtmlWriter writer = Writer;
			if (Has(parms.Wrap))
				parms.NoWrap = null;

			if (Has(parms.Left))
				parms.Align = "left";
			if (Has(parms.Right))
				parms.Align = "right";
			if (Has(parms.Center))
				parms.Align = "center";

			writer
				.Attr(A2.Attribute("width"))
				.Attr(A2.Attribute("height"))
				.Attr(A2.Attribute("nowrap"))
				.Attr(A2.Attribute("valign"))
				.Attr(A2.Attribute("val", "sortingval"))
				.AttrStyle("text-align", parms.Align);
			if (cspan > 1)
				writer.Attr("colspan", cspan.ToString());
			_cspan = (cspan > 1) ? cspan : 1;

			if (_groupType == _GroupType.Head)
			{
				int groupNo;
				colorGroupCssClass = "colorGroup-header-" + (_colorGroups[CellId] = JsCompatibility.TryParseInt(parms.Group, out groupNo) ? groupNo : 0);
				if (Has(parms.Sort))
				{
					string[] rr = null;// PrepareSortTitle(parms.Sort, parms.Sorting, parms.Type);
					writer
						.Attr("class", CssClass, rr[0], colorGroupCssClass)
						.Attr(rr[1])
						.Tag("th");
				}
				else
				{
					writer
						.Attr("class", CssClass, colorGroupCssClass)
						.Tag("th");
				}
				SetCellEnding(() => writer.End("th"));
			}
			else
			{
				colorGroupCssClass = "colorGroup-" + _colorGroups[CellId];
				if (_odd)
					writer.Attr("class", CssClass, colorGroupCssClass);
				else
					writer.Attr(A2.Attribute("class"));
				writer.Tag("td");
				SetCellEnding(() => writer.End("td"));
			}
		}


		#region Implementation
		private void DrawPager()
		{
			;
		}

		#region Nested Types
		private enum _GroupType
		{
			None = 0,
			Body = 1,
			Head = 2,
			Foot = 3
		}
		#endregion
		#endregion
	}
}
