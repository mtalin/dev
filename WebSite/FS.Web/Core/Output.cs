﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class Output
	{
		private readonly PageCommon _page;

		internal Output(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public string oQuarter(object value)
		{
			if (Common.tN(value))
				return "";

			DateTime dt = Convert.ToDateTime(value);
			string x = null;
			switch (Vb6.DateAndTime.DatePart("q", dt))
			{
				case 1:
					x = "First";
					break;
				case 2:
					x = "Second";
					break;
				case 3:
					x = "Third";
					break;
				case 4:
					x = "Fourth";
					break;
				default:
					System.Diagnostics.Debug.Assert(false, "Invalid quarter value");
					break;
			}
			return x + " Quarter " + dt.Year.ToString();
		}

		public string oRed(bool makeRed, string text)
		{
			return makeRed ? oColor("#FF0000", text) : text;
		}

		public string oColor(string color, string text)
		{
			if (!String.IsNullOrEmpty(text) && JsCompatibility.HasValue(color))
			{
				using (var writer = new HtmlWriter())
				{
					writer
						.Attr("color", color)
						.Tag("font")
							.Write(text)
						.End();
					return writer.GetContent();
				}
			}
			return text;
		}

		public string oZip(int? value)
		{
			return (value != null && value != 0)
				? oZip(value.Value.ToString())
				: "";
		}
		public string oZip(string value)
		{
			if (String.IsNullOrEmpty(value))
				return "";

			value = value.Trim();
			if (value.Length <= 5)
				value = value.PadLeft(5, '0');
			else
				value = value.Substring(0, 5) + "-" + value.Substring(5).PadRight(4, '0');

			return (value == "00000" || value == "00000-0000")
				? ""
				: HtmlUtility.HtmlEncode(value);
		}

		public string oSsn(int? value)
		{
			return (value != null && value != 0)
				? oSsn(value.Value.ToString())
				: "";
		}
		public string oSsn(string value)
		{
			return !String.IsNullOrEmpty(value)
				? HtmlUtility.HtmlEncode("XXX-XX-" + Vb6.Strings.Right(value.TrimEnd(new[] { ' ' }), 4))
				: Common.oE();
		}

		public string oSsn9(int? value)
		{
			return (value != null && value != 0)
				? oSsn9(value.Value.ToString())
				: "";
		}
		public string oSsn9(string value)
		{
			if (String.IsNullOrEmpty(value))
				return Common.oE();
			if (value.Length != 9)
				throw new ArgumentException("value");

			return HtmlUtility.HtmlEncode(value.Substring(0, 3)) + "-" + value.Substring(3, 2) + "-" + Vb6.Strings.Right(value, 4);
		}

		public string oCareOf(string value)
		{
			if (String.IsNullOrEmpty(value))
				return "";

			value = value.Trim();
			if (value.StartsWith("%"))
				value = value.Substring(1);
			return value;
		}

		public string oText(string value)
		{
			return !String.IsNullOrEmpty(value)
				? HtmlUtility.HtmlEncode(value).Replace(Environment.NewLine, HtmlUtility.Br)
				: "";
		}

		public string oUrl(string value)
		{
			return !String.IsNullOrEmpty(value)
				? UrlUtility.Escape(value.Trim())
				: "";
		}

		public string oTitle(string value, string defaultValue = null)
		{
			return !String.IsNullOrEmpty(value)
				? HtmlUtility.HtmlEncode(System.Globalization.CultureInfo.InvariantCulture.TextInfo.ToTitleCase(value))
				: defaultValue ?? "";

			//string[] splittedValue = value.Split(new[] { ' ' });
			//string w;
			//if (splittedValue.Length > 1)
			//{
			//	StringBuilder strBuilder = new StringBuilder();
			//	for (int i = 0; i < splittedValue.Length; i++)
			//	{
			//		w = splittedValue[i];
			//		if (JsCompatibility.HasValue(w))
			//		{
			//			if (strBuilder.Length > 0)
			//				strBuilder.Append(" ");
			//			strBuilder
			//				.Append(Char.ToUpperInvariant(w[0]))
			//				.Append(w.Substring(1).ToLowerInvariant());
			//		}
			//	}
			//	return HtmlUtility.HtmlEncode(strBuilder.ToString());
			//}
			//else
			//{
			//	return value;
			//}
		}

		public string oCountry(string value)
		{
			// TODO: get from enums
			return value;
			throw new NotImplementedException();
		}

		public string oPercent(double value, int numDecimal, string defaultValue)
		{
			return Vb6.Strings.FormatPercent(value, numDecimal, true, false, false);
		}
		public string oPercent(string value, int numDecimal, string defaultValue)
		{
			double temp;
			return Double.TryParse(value, out temp)
				? oPercent(value, numDecimal, defaultValue)
				: defaultValue;
		}

		public string oEin(int? value)
		{
			if (value == null || value == 0)
				return "";

			string s = oNumZ(value.Value, 9);
			return s.Substring(0, 2) + "-" + s.Substring(2);
		}
		public string oEin(string value)
		{
			return !String.IsNullOrEmpty(value)
				? oEin(JsCompatibility.ParseInt(value))
				: "";
		}

		public string oNumZ(double x, int? w = null, int? d = null)
		{
			w = w ?? 0;
			d = d ?? 0;

			if (d > 0)
				w = 1;
			return String.Format("{0:" + new String('0', w.Value) + (d > 0 ? "." + new String('0', d.Value) : "") + "}", x);
		}

		public string oPrc(string x, int? d = null, string e = null)
		{
			e = e ?? Common.oE();
			d = d ?? 2;
			return oPercent(x, d.Value, e);
		}
		public string oPrc(double x, int? d = null, string e = null)
		{
			e = e ?? Common.oE();
			d = d ?? 2;
			return oPercent(x, d.Value, e);
		}

		private static readonly Regex __r = new Regex("[^\\d]", RegexOptions.Compiled);
		private const string PhoneWithBraces = "()";
		private const string PhoneWithHyphens = "-";
		public string oPhone(object x, string f = PhoneWithBraces, string e = null)
		{
			if (Common.tN(x))
				return e ?? Common.oE();

			string sx = __r.Replace(Convert.ToString(x), "");
			bool isKnownFormat = (f == PhoneWithBraces || f == PhoneWithHyphens);
			string pattern = (f == PhoneWithHyphens)
				? "{0}-{1}-{2}"
				: "({0}) {1}-{2}";
			sx = String.Format(pattern, sx.Substring(0, 3), sx.Substring(3, 3), sx.Substring(6));
			if (!isKnownFormat)
				sx = HtmlUtility.HtmlEncode(sx);
			return sx;
		}

		private static readonly Regex __r2 = new Regex("</?[A-Za-z]+(\\s[^<]+)*>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		public string oNoHtml(object x, string e = null)
		{
			return Common.tN(x)
				? e ?? Common.oE()
				: __r2.Replace(Convert.ToString(x), "");
		}

		public string oJStr(string value)
		{
			if (value == null)
				throw new ArgumentException("value");
			if (String.IsNullOrEmpty(value))
				return value;

			return System.Web.HttpUtility.JavaScriptStringEncode(value);
		}
	}
}
