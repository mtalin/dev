﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class Access
	{
		private readonly PageCommon _page;

		internal Access(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public bool HasGmsReference()
		{
			return !String.IsNullOrEmpty(_page.Session[FS.Web.Utils.GmsReference.SesionKeys.Reference] as string);
		}

		private static Regex _r = new Regex(@"\{([a-zA-Z0-9\._-]+)\}", RegexOptions.Compiled);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="values">Format: "ref=value"</param>
		/// <returns></returns>
		public string GmsReference(string name, params string[] values)
		{
			if (name == null)
				throw new ArgumentNullException("name");

			if (!HasGmsReference())
				return "";
			string result = _page.Session[FS.Web.Utils.GmsReference.SesionKeys.GenerateKey(name)] as string;
			if (result == null)
			{
				// TODO: ap.logErr(10002, "access.asp GmsReference", "reference not found '".concat(name, "'."));
				throw new Exception("Gms reference couldn't be found: " + name);
			}

			Dictionary<string, string> valuesParsed = null;
			if (values != null && values.Length > 0)
			{
				valuesParsed = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
				for (int i = 0; i < values.Length; i++)
				{
					int n = values[i].IndexOf('=');
					if (n > 0)
						valuesParsed.Add(values[i].Substring(0, n), values[i].Substring(n + 1));
				}
			}

			return _r.Replace(result, m =>
				{
					string value;
					return valuesParsed.TryGetValue(m.Captures[0].Value, out value) ? value : "";
				});
		}
	}
}
