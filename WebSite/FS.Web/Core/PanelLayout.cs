﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class PanelLayout : PanelBase
	{
		internal PanelLayout(PageCommon page)
			: base(page)
		{ }

		public override string Name
		{
			get { return "panel-layout"; }
		}

		protected override void BeginCore(params string[] args)
		{
			HtmlWriter writer = Writer;
			writer.Attr(args);
			if (writer.HasAttributeInBuffer("border"))
				writer.Attr("border", "0");
			if (writer.HasAttributeInBuffer("cellspacing"))
				writer.Attr("cellspacing", "0");
			if (writer.HasAttributeInBuffer("cellpadding"))
				writer.Attr("cellpadding", "0");

			writer
				.Tag("table");
		}

		protected override void EndCore()
		{
			Writer.End("table");
		}

		protected override void RowCore(params string[] args)
		{
			HtmlWriter writer = Writer;
			writer
				.Attr(args)
				.Tag("tr");

			SetRowEnding(() => writer.End("tr"));
			SetCellEnding(null);
		}

		protected override void CellCore(params string[] args)
		{
			HtmlWriter writer = Writer;
			writer
				.Attr(args)
				.Tag("td");

			SetCellEnding(() => writer.End("td"));
		}
	}
}
