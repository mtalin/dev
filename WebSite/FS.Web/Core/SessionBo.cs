﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class SessionBo
	{
		private const string SessionKey = "FsboSession";
		private const string MultipleSesionValue = "*";
		private string _sessionId;
		private readonly PageCommon _page;

		internal SessionBo(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public string GetSessionID()
		{
			if (String.IsNullOrEmpty(_sessionId))
			{
				_sessionId = _page.Session[SessionKey] as string;
				if (_sessionId != _page.Request.Cookies[SessionKey].Value)
					_sessionId = MultipleSesionValue;
			}

			return _sessionId;
		}

		public void SetSessionID(string value)
		{
			_page.Session[SessionKey] = value;
			_page.SecureConfig.SetCookie(SessionKey, value);
			_sessionId = value;
		}
	}
}
