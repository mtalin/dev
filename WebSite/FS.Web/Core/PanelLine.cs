﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class PanelLine : PanelBase
	{
		internal PanelLine(PageCommon page)
			: base(page)
		{
			A0.SetDefaults(new Dictionary<string, string>()
				{
					{ "PanelClass", "Panel" },
					{ "NameClass", "NameCell" },
					{ "ValueClass", "PanelCell" },
					{ "BorderClass", "PanelBorder" },
					{ "InfoClass", "PanelHeader" },
					{ "InfoNameClass", "PanelHeader" },
					{ "InfoValueClass", "PanelHeader" },
					{ "Info2Class", "LinePanelButton" },
					{ "InfoName2Class", "NameCell" },
					{ "InfoValue2Class", "PanelCell" },
					{ "ButtonClass", "LinePanelButton" },
					{ "ButtonCellClass", "PanelCell" }
				});

			A0.Extend(new Dictionary<string, string>()
				{
					{ "ID", null },
					{ "type", null },
					{ "class", null },
					{ "nameclass", null },
					{ "valueclass", null },
					{ "cellclass", null },
					{ "border", null },
					{ "noborder", null },
					{ "width", "100%" },
					{ "lineheight", "170%" },
					{ "nospacer", null }
				});
		}

		public override string Name
		{
			get { return "panel-line"; }
		}

		protected override void BeginCore(params string[] args)
		{
			A0.ParseWithLog(this, args);
			dynamic parms = A0;

			string id = tE(parms.Id, "PanelLine" + Page.Panel.GetPanelNextId());
			string type = parms.Type;
			string pc, nc, vc;
			bool br;

			if (Has(type))
			{
				switch (type.ToUpperInvariant())
				{
					case "INFO":
						{
							pc = "InfoClass";
							nc = "InfoNameClass";
							vc = "InfoValueClass";
							br = true;
							break;
						}
					case "INFO2":
						{
							pc = "Info2Class";
							nc = "InfoName2Class";
							vc = "InfoValue2Class";
							br = false;
							break;
						}
					case "BUTTON":
					case "BUTTONS":
						{
							pc = "ButtonClass";
							nc = "ButtonCellClass";
							vc = "ButtonCellClass";
							br = false;
							break;
						}
					default:
						throw new ArgumentException("Invalid attribute value", "info");
				}
			}
			else
			{
				pc = "PanelClass";
				nc = "NameClass";
				vc = "ValueClass";
				br = false;
			}

			if (Has(parms.CellClass))
			{
				parms.NameClass = parms.CellClass;
				parms.ValueClass = parms.CellClass;
			}

			if (!Has(parms.Class))
				parms.Class = A0.GetDefaultFor(pc);
			if (!Has(parms.NameClass))
				parms.NameClass = A0.GetDefaultFor(nc);
			if (!Has(parms.ValueClass))
				parms.ValueClass = A0.GetDefaultFor(vc);
			if (!Has(parms.BorderClass))
				parms.BorderClass = A0.GetDefaultFor("BorderClass");

			if (Has(parms.Border))
				br = true;
			else if (Has(parms.NoBorder))
				br = false;

			CssClass = HtmlUtility.Attribute("class", parms["class"], br ? parms.BorderClass : "");
			HtmlWriter writer = Writer;
			writer
				.Attr("border", "0")
				.Attr("cellspacing", "0")
				.Attr("cellspacing", "4")
				.Attr(CssClass)
				.Attr(A0.Attribute("width"))
				.Tag("table")
					.Tag("tr");
		}

		protected override void EndCore()
		{
			HtmlWriter writer = Writer;
			writer
				.End("tr")
				.End("table");
		}

		protected override void RowCore(params string[] args)
		{
			dynamic parms = A0;
			dynamic parms2 = A1 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "align", "center" },
					{ "nospacer", parms.NoSpacer }
				});

			A1.ParseWithLog(this, args);

			HtmlWriter writer = Writer;
			if (Has(parms2.Align))
				writer.AttrStyle("text-align", parms2.Align);

			writer
				.Attr(A1.Attribute("width"))
				.Tag("td");

			SetRowEnding(() => writer.End("td"));
			SetCellEnding(null);
		}

		protected override void CellCore(params string[] args)
		{
			dynamic parms = A0;
			dynamic parms2 = A1;
			dynamic parms3 = A2 = new AttributesBag(new Dictionary<string, string>() {
				{ "class", "" }
			});
			A2.ParseWithLog(this, args);

			if (!Has(parms3["class"]))
				parms3["class"] = (CellId % 2 != 0) ? parms.NameClass : parms.ValueClass;

			if (CellId > 1 && !Has(parms2.NoSpacer))
				WriteCellSpacer();

			CssClass = A2.Attribute("class");
			HtmlWriter writer = Writer;
			writer
				.Attr(CssClass)
				.AttrStyle("line-height", parms.LineHeight)
				.Tag("span");

			SetCellEnding(() => writer.End("span"));
		}


		private void WriteCellSpacer()
		{
			HtmlWriter writer = Writer;
			writer
				.Attr("src", Page.GetImageUrl("spacer.gif"))
				.Attr("width", "10")
				.Attr("height", "10")
				.Attr("border", "0")
				.Attr("alt", "")
				.TagSingle("img");
		}
	}
}
