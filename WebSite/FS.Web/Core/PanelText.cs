﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class PanelText : PanelBase
	{
		private bool _isFieldSet;

		internal PanelText(PageCommon page)
			: base(page)
		{
			A0.SetDefaults(new Dictionary<string, string>()
				{
					{ "PanelClass", "Panel" },
					{ "BorderClass", "PanelBorder" },
					{ "NoBorderClass", null },
					{ "TitleClass", "PanelTitle" },
					{ "FieldsetClass", "Fieldset" },
					{ "FieldsetBorderClass", null },
					{ "FieldsetNoBorderClass", "FieldsetNoBorder" },
					{ "FieldsetTitleClass", "FieldsetTitle" },
					{ "CellClass", "PanelCell" }
				});

			A0.Extend(new Dictionary<string, string>()
				{
					{ "ID", null },
					{ "title", null },
					{ "type", null },
					{ "class", null },
					{ "frameclass", null },
					{ "titleclass", null },
					{ "cellclass", null },
					{ "border", null },
					{ "noborder", null },
					{ "width", null }
				});
		}

		public override string Name
		{
			get { return "panel-text"; }
		}

		protected override void BeginCore(params string[] args)
		{
			dynamic parms = A0;
			A0.ParseWithLog(this, args);

			string id = parms.Id;
			_isFieldSet = eQ(parms.Type, "fieldset");
			bool border = Has(parms.Border);

			CssClass = A0.Attribute("class", "class", "PanelClass");

			HtmlWriter writer = Writer;
			if (_isFieldSet)
			{
				writer
					.Attr(A0.Attribute("id"))
					.Attr("class", tE(parms.FrameClass, A0.GetDefaultFor("FieldsetClass")), A0.GetDefaultFor(border ? "FieldsetBorderClass" : "FieldsetNoBorderClass"))
					.Tag("fieldset");

				if (Has(parms.Title))
				{
					writer
						.Attr(A0.Attribute("titleClass", "class", "FieldsetTitleClass"))
						.Tag("legend")
							.Write(HtmlUtility.NonBreakingSpacer, parms.Title, HtmlUtility.NonBreakingSpacer)
						.End();
				}

				writer
					.Attr("border", "0")
					.Attr("cellspacing", "0")
					.Attr("cellpadding", "0")
					.Attr(A0.Attribute("id"))
					.Attr(CssClass)
					.Attr(A0.Attribute("style"))
					.Attr(A0.Attribute("width"))
					.Tag("table");
			}
			else
			{
				writer
					.Attr("border", "0")
					.Attr("cellspacing", "0")
					.Attr("cellpadding", "0")
					.Attr(A0.Attribute("id"))
					.Attr("class", tE(parms["class"], A0.GetDefaultFor("PanelClass")), A0.GetDefaultFor(border ? "FieldsetBorderClass" : "FieldsetNoBorderClass"))
					.Attr(A0.Attribute("style"))
					.Attr(A0.Attribute("width"))
					.Tag("table");

				if (Has(parms.Title))
				{
					writer
						.Tag("tr")
							.AttrStyle("text-align", "center")
							.Attr(A0.Attribute("TitleClass", "class", "TitleClass"))
							.Tag("td")
								.Write(parms.Title)
							.End()
						.End();
				}
			}
		}

		protected override void EndCore()
		{
			HtmlWriter writer = Writer;
			writer.End("table");
			if (_isFieldSet)
				writer.End("fieldset");
		}

		protected override void RowCore(params string[] args)
		{
			dynamic parms0 = A0;
			dynamic parms = A1 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "Class", parms0.CellClass },
					{ "CellClass", parms0.CellClass },
					{ "Space", null },
					{ "Line", null }
				}, A0);
			A1.ParseWithLog(this, args);

			if (Has(parms.Line) || Has(parms.Space))
			{
				PanelHLine(parms.Line, parms.Space, 1, A1.Attribute("class"));
				SetRowEnding(null);
			}
			else
			{
				HtmlWriter writer = Writer;
				writer.Tag("tr");
				writer
					.Attr(A1.Attribute("class", "class"))
					.Tag("td");

				SetRowEnding(() => writer.End("td").End("tr"));
			}
			this.SetCellEnding(null);
		}

		protected override void CellCore(params string[] args)
		{
			dynamic parms2 = A1;
			dynamic parms = A2 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "Id", null },
					{ "Class", parms2.CellClass },
					{ "Style", null }
				}, A1);
			A2.ParseWithLog(this, args);

			CssClass = A2.Attribute("class", "class", "CellClass");
			HtmlWriter writer = Writer;
			writer
				.Attr(CssClass)
				.Attr(A2.Attribute("style", "style"))
				.Attr(A2.Attribute("Id"))
				.Tag("div");

			SetCellEnding(() => writer.End("div"));
		}
	}
}
