﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lexxys;

namespace FS.Web.Core
{
	public sealed class _Redirect
	{
		private readonly PageCommon _page;

		internal _Redirect(PageCommon page)
		{
			if (page == null)
				throw EX.ArgumentNull("page");

			_page = page;
		}

		public void ForceClose()
		{
			if (_page.Response.Buffer)
				_page.Response.Clear();

			using (var writer = new HtmlWriter(_page.Writer))
			{
				writer
					.Doctype(DoctypeMode.StrictHtml)
						.Tag("html")
							.Tag("head")
								.Tag("title")
									.Write(".")
								.End()
							.End()
							.Tag("body")
								.BeginScript()
									.Write("window.close();")
								.EndScript()
							.End()
						.End();
			}

			_page.Response.End();
		}

		public void CloseMe()
		{
			if (_page.Common.LoginMethod == LoginMethod.Close)
				ForceClose();
			else
				Redirect("home");
		}

		public void Redirect(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			System.Web.HttpResponse response = _page.Response;
			if (response.Buffer)
				response.Clear();

			string x = url.ToUpperInvariant();
			if (x == "LOGIN" || x == "LOGOUT")
			{
				if (_page.Common.LoginMethod == LoginMethod.Close)
					ForceClose();
			}

			url = GetUrl(url);
			if (UrlUtility.HasQuery(url))
				_page.Href.AddLegalTarget(url);

			_page.Callback.EnsurePageMode(_page.Request);
			if (_page.Callback.IsCallback)
			{
				response.Clear();
				response.AddHeader("REDIRECT_URL", url);
				using (var writer = new HtmlWriter(_page.Writer))
				{
					writer
						.Attr("class", "redirect-box")
						.Tag("em")
							.WriteEncoded("If your browser doesn't redirect you, click ")
							.Attr("href", url)
							.Tag("a")
								.WriteEncoded("here")
							.End()
						.End();
					response.End();
				}
			}
			else
			{
				response.Redirect(url);
			}
		}

		public string GetUrl(string url)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			string query = UrlUtility.GetQuery(url);
			string path = UrlUtility.GetPath(url);
			switch (path.ToUpperInvariant())
			{
				case "HOME":
				case "BACK":
					path = _page.Url.Action("index", "root");
					break;
				case "LOGIN":
					path = _page.Url.Action("login", "root");
					break;
				case "ADMIN-HOME":
					path = _page.Url.Action("index", "root");
					break;
				case "TIMEOUT":
					path = Common.tE(_page.Session["timeoutURL"] as string, Common.tE(_page.Session["timeoutURL"] as string, _page.Url.Action("index_err", "root")));
					break;
				case "ERROR":
					path = Common.tE(_page.Session["errorURL"] as string, _page.Url.Action("index_err", "root"));
					break;
			}

			return (query != null) ? UrlUtility.ChangeQuery(path, query) : path;
		}
	}
}
