﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class ClientStyling
	{
		private List<StyleInfo> _includes;
		private Dictionary<string, StyleInfo> _blocks;
		private readonly PageBase _page;

		internal ClientStyling(PageBase page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public void RegisterInclude(string path, string mediaType = null)
		{
			if (path == null)
				throw new ArgumentNullException("path");

			if (_includes == null)
				_includes = new List<StyleInfo>();
			else if (_includes.Exists(item => item.Path.Equals(path, StringComparison.OrdinalIgnoreCase)))
				return;

			if (!Path.HasExtension(path))
				path = Path.ChangeExtension(path, "сss");
			_includes.Add(new StyleInfo(path, mediaType ?? "screen"));
		}

		public void RegisterBlock(string key, string content, bool replaceExisting = false)
		{
			if (key == null)
				throw new ArgumentNullException("key");
			if (content == null)
				throw new ArgumentNullException("content");

			if (_blocks == null)
				_blocks = new Dictionary<string, StyleInfo>(StringComparer.OrdinalIgnoreCase);
			else if (_blocks.ContainsKey(key) && !replaceExisting)
				return;

			_blocks[key] = new StyleInfo(content);
		}


		internal void WriteIncludes()
		{
			if (_includes != null && _includes.Count > 0)
			{
				using (var writer = new HtmlWriter(_page.Writer))
				{
					foreach (var item in _includes)
					{
						StyleExternal(writer, item);
					}
				}
			}
		}

		internal void WriteBlocks()
		{
			if (_blocks != null && _blocks.Count > 0)
			{
				using (var writer = new HtmlWriter(_page.Writer))
				{
					BeginStyle(writer);
					foreach (var item in _blocks)
					{
						writer.Write(item.Value.Content);
					}
					EndStyle(writer);
				}
			}
		}


		#region Implementation
		private void StyleExternal(HtmlWriter writer, StyleInfo style)
		{
			writer
				.Attr("href", style.Path)
				.Attr("rel", "stylesheet")
				.Attr("type", "text/css")
				.Attr("media", style.MediaType)
				.TagSingle("link");
		}

		private void BeginStyle(HtmlWriter writer, string comment = null)
		{
			writer
				.WriteComment(comment ?? "style block")
				.Attr("type", "text/css")
				.Tag("style");
		}
		private void EndStyle(HtmlWriter writer)
		{
			writer.End();
		}

		private class StyleInfo
		{
			public StyleInfo(string content)
			{
				Content = content;
			}

			public StyleInfo(string path, string mediaType)
			{
				Path = path;
				MediaType = mediaType;
			}

			public string Path;

			public string Content;

			public string MediaType;
		}
		#endregion
	}
}
