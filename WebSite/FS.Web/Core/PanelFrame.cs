﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public class PanelFrame : PanelBase
	{
		bool _stripe = false;
		bool _odd = true;
		string _stripe1;
		string _stripe2;
		protected int _columns;
		protected int _cspan = 1;
		bool _isFieldSet;

		internal PanelFrame(PageCommon page)
			: base(page)
		{
			A0.SetDefaults(new Dictionary<string, string>()
				{
					{ "PanelClass", "Panel" },
					{ "BorderClass", "PanelBorder" },
					{ "NoBorderClass", null },
					{ "TitleClass", "PanelTitle" },
					{ "FieldsetClass", "Fieldset" },
					{ "FieldsetBorderClass", null },
					{ "FieldsetNoBorderClass", "FieldsetNoBorder" },
					{ "FieldsetTitleClass", "FieldsetTitle" },
					{ "CellClass", "PanelCell" },
					{ "Stripe1Class", "Stripe2Background" },
					{ "Stripe2Class", "Stripe1Background" }
				});

			A0.Extend(new Dictionary<string, string>()
				{
					{ "ID", null },
					{ "title", null },
					{ "type", null },
					{ "class", null },
					{ "frameclass", null },
					{ "titleclass", null },
					{ "cellclass", null },
					{ "cellalign", null },
					{ "cellvalign", null },
					{ "width", "100%" },
					{ "height", null },
					{ "border", null },
					{ "noborder", null },
					{ "stripe", null },
					{ "cols", null },
					{ "padding", null },
					{ "cellspacing", null },
					{ "align", null },

					{ "stripe1class", A0.GetDefaultFor("Stripe1Class") },
					{ "stripe2class", A0.GetDefaultFor("Stripe2Class") },

					// compatibility
					{ "fieldset", null },
					{ "background", "NormalBackground" },
					{ "framebackground", "NormalBackground" },
					{ "titlebackground", "NormalBackground" },
				});

			_stripe1 = A0.GetDefaultFor("Stripe1Class");
			_stripe2 = A0.GetDefaultFor("Stripe2Class");
		}

		public override string Name
		{
			get { return "panel-frame"; }
		}

		protected override void BeginCore(params string[] args)
		{
			dynamic parms = A0;
			A0.ParseWithLog(this, args);

			string id = parms.Id;
			if (Has(parms.Cols))
				_columns = JsCompatibility.ParseInt(parms.Cols);
			if (_columns < 1)
				_columns = 1;
			_isFieldSet = eQ(parms.Type, "fieldset");
			_stripe = Has(parms.Stripe);
			bool border = Has(parms.Border);

			HtmlWriter writer = Writer;
			if (_isFieldSet)
			{
				writer
					.Attr(A0.Attribute("id"))
					.Attr("class", tE(parms.FrameClass, A0.GetDefaultFor("FieldsetClass")), A0.GetDefaultFor(border ? "FieldsetBorderClass" : "FieldsetNoBorderClass"));
				if (Has(parms.Height))
					writer.AttrStyle("height", parms.Height);
				writer
					.Tag("fieldset");

				if (Has(parms.Title))
				{
					writer
						.Attr(A0.Attribute("titleClass", "class", "FieldsetTitleClass"))
						.Tag("legend")
							.Write(HtmlUtility.NonBreakingSpacer, parms.Title, HtmlUtility.NonBreakingSpacer)
						.End();
				}

				writer
					.Attr("border", "0")
					.Attr("cellspacing", tE(parms.Cellspacing, "1"))
					.Attr("cellpadding", "0")
					.Attr(A0.Attribute("width"))
					.Attr(A0.Attribute("class", "class", "PanelClass"));
				if (Has(parms.Padding))
					writer.AttrStyle("padding", parms.Padding + "px");
				if (Has(parms.Align))
					writer.AttrStyle("text-align", parms.Align);
				writer
					.Tag("table");
			}
			else
			{
				writer
						.Attr("border", "0")
						.Attr("cellspacing", tE(parms.Cellspacing, "1"))
						.Attr("cellpadding", "0")
						.Attr(A0.Attribute("id"))
						.Attr("class", tE(parms["Class"], A0.GetDefaultFor("PanelClass")), A0.GetDefaultFor(border ? "BorderClass" : "NoBorderClass"))
						.Attr(A0.Attribute("align"))
						.Attr(A0.Attribute("width"))
						.Attr(A0.Attribute("height"))
						.Tag("table");

				if (Has(parms.Title))
				{
					writer
							.Tag("tr")
								.AttrStyle("text-align", "center")
								.Attr(A0.Attribute("TitleClass", "class", "TitleClass"));
					if (_columns > 1)
						writer.Attr("colspan", _columns.ToString());
					writer
								.Tag("td")
									.Write(parms.Title)
								.End()
							.End();
				}
			}
		}

		protected override void EndCore()
		{
			HtmlWriter writer = Writer;
			writer.End("table");
			if (_isFieldSet)
				writer.End("fieldset");
		}

		protected override void RowCore(params string[] args)
		{
			dynamic parms0 = A0;
			dynamic parms = A1 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "Class", parms0.CellClass },
					{ "Height", null },
					{ "Space", null },
					{ "Line", null }
				}, A0);
			A1.ParseWithLog(this, args);

			if (Has(parms.Line) || Has(parms.Space))
			{
				PanelHLine(parms.Line, parms.Space, _columns, A1.Attribute("class"));
				SetRowEnding(null);
			}
			else
			{
				HtmlWriter writer = Writer;
				if (_stripe)
				{
					writer.Attr("class", parms["class"], _odd ? _stripe1 : _stripe2);
					_odd = !_odd;
				}
				else
				{
					writer.Attr(A1.Attribute("class"));
				}
				writer.Tag("tr");

				SetRowEnding(() => writer.End("tr"));
			}
			this.SetCellEnding(null);
		}

		protected override void CellCore(params string[] args)
		{
			CellId += (_cspan - 1);

			dynamic parms0 = A0;
			dynamic parms2 = A1;
			dynamic parms = A2 = new AttributesBag(new Dictionary<string, string>()
				{
					{ "class", parms0.CellClass },
					{ "colspan", "1" },
					{ "rowspan", "1" },
					{ "width", null },
					{ "height", parms2.Height },
					{ "nowrap", null },
					{ "align", parms0.CellAlign },
					{ "valign", parms0.CellValign },
				}, A1);
			A2.ParseWithLog(this, args);

			if (CellId > _columns)
				_columns = CellId;

			int cspan = JsCompatibility.ParseInt(parms.Colspan);
			int rspan = JsCompatibility.ParseInt(parms.Rowspan);

			_cspan = (cspan > 1) ? cspan : 1;

			CssClass = A2.Attribute("class", "class", "CellClass");
			HtmlWriter writer = Writer;
			writer
				.Attr(CssClass)
				.Attr(A2.Attribute("width"))
				.Attr(A2.Attribute("height"))
				.Attr(A2.Attribute("valign"));
			if (cspan > 1)
				writer.Attr("colspan", cspan.ToString());
			if (rspan > 1)
				writer.Attr("rowspan", rspan.ToString());
			if (Has(parms.NoWrap))
				writer.Attr("nowrap", "nowrap");
			if (Has(parms.Align))
				writer.AttrStyle("text-align", parms.Align);
			writer
				.Tag("td");
			
			SetCellEnding(() => writer.End("td"));
		}
	}
}
