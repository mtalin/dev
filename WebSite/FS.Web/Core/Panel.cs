﻿using Lexxys;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class Panel
	{
		private readonly PageCommon _page;
		private bool _bufferEnabled;
		private HtmlWriter _writer;
		private HtmlWriter _writerBuffered;
		private readonly Stack<PanelBase> _panels = new Stack<PanelBase>(4);
		private RecordedPanel _record;
		private bool _pageBufferEnabled;
		private int _currentPanelId;

		private static readonly Dictionary<string, Func<PageCommon, PanelBase>> _factory = new Dictionary<string,Func<PageCommon,PanelBase>>(StringComparer.OrdinalIgnoreCase)
		{
			{ "layout", page => new PanelLayout(page) },
			{ "text", page => new PanelText(page) },
			{ "nameValue", page => new PanelNameValue(page) },
			{ "report", page => new PanelReport(page) },
			{ "update", page => new PanelUpdate(page) },
			{ "frame", page => new PanelFrame(page) },
			{ "line", page => new PanelLine(page) },
			{ "menu", page => new PanelMenu(page) },
			{ "tabs", page => new PanelTabs(page) },
			{ "dialog", page => null },
			{ "collapsed", page => new PanelCollapsed(page) },
			{ "collapsible", page => new PanelCollapsible(page) },
			{ "accordion", page => null },
		};

		internal Panel(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		#region Buffering
		public HtmlWriter Writer
		{
			get { return _bufferEnabled ? _writerBuffered : _writer; }
		}

		public bool BufferEnabled
		{
			get { return _bufferEnabled; }
			set
			{
				if (value && !_bufferEnabled)
				{
					if (_writerBuffered == null)
						_writerBuffered = new HtmlWriter();
					else
						ClearBuffer();
				}
				_bufferEnabled = value;
			}
		}

		public string GetBufferContent(bool? bufferEnabled = null)
		{
			if (_writerBuffered == null)
				return "";

			string content = _writerBuffered.GetContent();
			_writerBuffered.Clear();

			if (bufferEnabled != null)
				BufferEnabled = bufferEnabled.Value;

			return content;
		}

		public void ClearBuffer(bool? bufferEnabled = null)
		{
			if (_writerBuffered != null)
				_writerBuffered.Clear();

			if (bufferEnabled != null)
				BufferEnabled = bufferEnabled.Value;
		}

		public void FlushBuffer(bool? bufferEnabled = null)
		{
			if (_writerBuffered == null)
				return;

			_writerBuffered.InnerWriter.Flush();
			string content = _writerBuffered.GetContent();
			if (!String.IsNullOrEmpty(content))
			{
				_page.Write(content);
				ClearBuffer();
			}

			if (bufferEnabled != null)
				BufferEnabled = bufferEnabled.Value;
		}
		#endregion

		public PanelBase CurrentPanel
		{
			get
			{
				return _panels.Count > 0 ? _panels.Peek() : null;
			}
		}

		public int GetPanelNextId()
		{
			return ++_currentPanelId;
		}

		public void BeginPanel(string name, params string[] args)
		{
			if (_writer == null)
				_writer = new HtmlWriter(_page.Writer);

			PanelBase panel;
			Func<PageCommon, PanelBase> factory = null;
			if (_factory.TryGetValue(name, out factory))
				panel = factory(_page);
			else
				throw new ArgumentException("Cannot find a panel by specified name: " + name, "name");

			Debug.Assert(panel != null);
			panel.Begin(args);
			_panels.Push(panel);
		}

		public void EndPanel()
		{
			if (_panels.Count == 0)
				throw new InvalidOperationException("There is no panel to end");

			_panels.Pop().End();
		}

		public void Group(params string[] args)
		{
			if (_panels.Count == 0)
				throw new InvalidOperationException("There is no active panel");
			_panels.Peek().Row(args);
		}

		public void Cell(string text, params string[] args)
		{
			if (_panels.Count == 0)
				throw new InvalidOperationException("There is no active panel");
			_panels.Peek().Cell(args);
			if (text != null)
				Writer.Write(text);
		}

		public void Space()
		{
			Writer
				.Img(_page.GetImageUrl("spacer.gif"), width: 10, height: 10);
		}

		public void RecordingStart()
		{
			if (_panels.Count > 0)
				_panels.Peek().OnStartRecording();
			_record = null;
			if (_pageBufferEnabled = BufferEnabled)
				FlushBuffer(true);
			ClearBuffer(true);
		}

		public void RecordingStop()
		{
			if (_panels.Count > 0)
				_panels.Peek().OnStopRecording();
			_record = new RecordedPanel(GetBufferContent(_pageBufferEnabled), CurrentPanel);
		}

		public RecordedPanel GetRecorded()
		{
			return _record;
		}

		public void RegisterPanel(string name, Func<PageCommon, PanelBase> factory)
		{
			if (name == null)
				throw EX.ArgumentNull("name");
			if (name == "")
				throw EX.Argument("name");
			if (factory == null)
				throw EX.ArgumentNull("factory");

			_factory.Add(name, factory);
		}

		public void RecordingPlay(RecordedPanel record, params string[] args)
		{
			record = PrepareRecord(record, args);
			Debug.Assert(record.Panel != null);
			record.Panel.OnPlayRecording(record);
			_page.Write(record.Html);
		}

		public string GetRecordedHtml(RecordedPanel record, params string[] args)
		{
			record = PrepareRecord(record, args);
			Debug.Assert(record.Panel != null);
			record.Panel.OnPlayRecording(record);
			return record.Html;
		}

		public RecordedPanel PrepareRecord(RecordedPanel record, string[] args)
		{
			dynamic rec = record.Clone();
			string html = record.Html;
			Dictionary<string, string> prms = rec.Params ?? (rec.Params = new Dictionary<string, string>());
			for (int i = 1; i < args.Length; i += 2)
			{
				string name = args[i - 1];
				string value = args[i];
				prms.Add(name, value);

				html = html.Replace(name, value);
			}
			rec.Html = html;
			return rec;
		}
	}
}
