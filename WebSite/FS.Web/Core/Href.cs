﻿using FS.Web.Compatibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FS.Web.Core
{
	public sealed class Href : IRequireInit
	{
		public const string FsWebSiteUrl = "http://www.foundationsource.com";
		private const string LegalHrefsSessionKey = "legal_refs___";

		private readonly PageCommon _page;
		private int _rootOffset;
		private bool _isLegalReferer;
		private Dictionary<string, string> _legalHrefs;

		internal Href(PageCommon page)
		{
			if (page == null)
				throw new ArgumentNullException("page");

			_page = page;
		}

		public string SetParam(string url, string name, string value)
		{
			return UrlUtility.SetQueryParam(url, name, value, replaceExisting: true);
		}

		public bool LegalReferer()
		{
			return _isLegalReferer;
		}

		public string AddBackParam(string url)
		{
			return url;
		}

		public string LegalHref(string url, bool noCache = false)
		{
			url = AddBackParam(url);	// AddBackParam seems to be overriden in "admin/utils/back2.asp"
			if (noCache)
				url = AttachNoCache(url);

			AddLegalTarget(url);
			return url;
		}

		public string LegalHrefNoBack(string url)
		{
			AddLegalTarget(url);
			return url;
		}

		public void AddLegalTarget(string url)
		{
			url = url.Trim();
			if (UrlUtility.IsAbsolute(url))
				url = UrlUtility.GetPathAndQuery(url);

			url = __r.Replace(url.Replace(UrlUtility.QueryDelimiter, UrlUtility.QueryParamDelimiter).ToLowerInvariant(), "");
			int i = url.IndexOf(UrlUtility.QueryParamDelimiter);
			if (i > 0)
			{
				string path = GetUrlPath(url.Substring(0, i));
				string query = UnescapeUrlQuery(url.Substring(i + 1));
				string knownQuery; _legalHrefs.TryGetValue(path, out knownQuery);
				if (knownQuery == null || !knownQuery.Contains(WrapQuery(query)))
				{
					if (JsCompatibility.HasValue(knownQuery))
						_legalHrefs[path] = knownQuery + WrapQuery(query);
					else
						_legalHrefs[path] = WrapQuery(query);
				}
			}
		}
		// to match ... $_name=value& ...
		private static readonly Regex __r = new Regex("&_[^&]*", RegexOptions.Compiled);

		// was "getMyURL"
		public string GetMyUrlWithParams()
		{
			if (JsCompatibility.HasValue(_page.MyParams))
				return _page.MyUrl + "?" + _page.MyParams;
			return _page.MyUrl;
		}


		#region Implementation
		public void Init()
		{
			_rootOffset = _page.Server.MapPath("/").Length - 1;

			_legalHrefs = _page.Session[LegalHrefsSessionKey] as Dictionary<string, string>;
			if (_legalHrefs == null)
			{
				_legalHrefs = new Dictionary<string, string>(16);
				_page.Session[LegalHrefsSessionKey] = _legalHrefs;
			}

			CheckLegalReferer();
		}

		private const int MaxNoCacheSeed = 99000;
		private const int MinNoCacheSeed = 10000;
		/// <summary>
		/// @imported: Adds GET parameter to url to prevent IE from caching a page content
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		private string AttachNoCache(string url)
		{
			return UrlUtility.SetQueryParam(url, "_nocache", new Random().Next(MinNoCacheSeed, MaxNoCacheSeed).ToString());
		}

		private void CheckLegalReferer()
		{
			string path = GetUrlPath(_page.MyUrl);
			string query = (_page.MyParams.Length > 0) ? __r.Replace(UrlUtility.QueryParamDelimiter + _page.MyParams.ToLowerInvariant(), "").Substring(1) : "";

			if (_legalHrefs.ContainsKey(path))
			{
				if (JsCompatibility.HasValue(query.Trim()))
					_isLegalReferer = _legalHrefs[path].Contains(WrapQuery(UnescapeUrlQuery(query)));
				else
					_isLegalReferer = true;
			}
			else
			{
				_isLegalReferer = (query.Length == 0);
			}
		}

		private string GetUrlPath(string url)
		{
			return _page.Server.MapPath(url).Substring(_rootOffset).Trim().ToLowerInvariant();
		}

		private string UnescapeUrlQuery(string query)
		{
			return UrlUtility.Unescape(query).Replace(' ', '+');
		}

		private static string WrapQuery(string query)
		{
			return "?" + query + "?";
		}
		#endregion
	}
}
