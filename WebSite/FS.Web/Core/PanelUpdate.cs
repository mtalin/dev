﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FS.Web.Compatibility;

namespace FS.Web.Core
{
	public sealed class PanelUpdate : PanelBase
	{
		private string _clientStateFieldId;
		private string _startupScript;
		private string _scriptKey;
		private bool _updateMode;
		private string _id;
		private bool _bufferOld;

		internal PanelUpdate(PageCommon page)
			: base(page)
		{
			A0.Extend(new Dictionary<string, string>()
				{
					{ "ID", null },
					{ "Interval", null },				// @imported: sec
					{ "OnSuccess", null },				// @imported: function name
					{ "OnError", null },				// @imported: function name
					{ "NoAnimation", null }
				});
		}

		public override string Name
		{
			get { return "panel-update"; }
		}

		protected override void BeginCore(params string[] args)
		{
			dynamic parms = A0;
			A0.ParseWithLog(this, args);

			_id = tE(parms.Id, "PanelUpdate" + PanelId.ToString());
			_clientStateFieldId = Page.FormIn.GetClientStateName(_id);

			_updateMode = (Page.Callback.IsCallback && Page.Callback.CallbackInvoker != null && Page.Callback.CallbackInvoker.Equals(_id, StringComparison.OrdinalIgnoreCase));
			if (_updateMode)
			{
				// TODO: doesn't work
				Page.Response.Clear();
				_bufferOld = Page.Panel.BufferEnabled;
				Page.Panel.BufferEnabled = false;
				// TODO: doesn't work
			}
			else
			{
				PrepareStartupScript();

				HtmlWriter writer = Writer;
				if (!Has(parms.NoState))
				{
					writer
						.Hidden(_clientStateFieldId, "")
						.Hidden("__viewState$CTX", _id);
				}

				writer
					.Attr("id", _id)
					.Attr("class", "panel-update")
					.Tag("div");
			}
		}

		protected override void EndCore()
		{
			if (_updateMode)
			{
				Page.ClientScript.RegisterStartup("restoreFormId", "$('INPUT[name=_form_id]').val('" + Page.FormIn.PeekId() + "');");
				Page.ClientScript.RegisterStartup("decorateDisabledButtons", "checkDisabledButton();");

				Page.ClientScript.WriteBlocks();
				Page.ClientScript.WriteStartups();

				Page.Response.End();

				Page.Panel.BufferEnabled = _bufferOld;
			}

			Writer
				.End("div");

			Page.ClientScript.RegisterStartup(_scriptKey, _startupScript);
		}

		protected override void RowCore(params string[] args)
		{
			throw new NotImplementedException();
		}

		protected override void CellCore(params string[] args)
		{
			throw new NotImplementedException();
		}


		private void PrepareStartupScript()
		{
			Page.ClientScript.RequireJQuery(true);

			dynamic parms0 = A0;
			string clientInstance = _id + "__behavior";

			StringBuilder script = new StringBuilder();
			script
				.AppendFormat("var {0} = new $FS.Asp.PanelUpdate(", clientInstance).JValue(_id).Append(")")
				.AppendLine(";")
				.AppendFormat("{0}.WrapperId = ", clientInstance).JValue(_id)
				.AppendLine(";");

			if (Has(parms0.Interval))
			{
				script
					.AppendFormat("{0}.Interval = {1}s", clientInstance, parms0.Interval)
					.AppendLine(";");
			}

			script
				.AppendFormat("{0}.Animation = ", clientInstance).JValue((bool)Has(parms0.NoAnimation))
				.AppendLine(";");

			if (Has(parms0.OnSuccess))
			{
				script
					.AppendFormat("{0}.OnSuccess.AddListener({1})", clientInstance, parms0.OnSuccess)
					.AppendLine(";");
			}

			if (Has(parms0.OnError))
			{
				script
					.AppendFormat("{0}.OnError.AddListener({1})", clientInstance, parms0.OnError)
					.AppendLine(";");
			}

			script
				.AppendFormat("{0}.Init()", clientInstance)
				.AppendLine(";");

			_startupScript = script.ToString();
			_scriptKey = clientInstance;
		}
	}
}
