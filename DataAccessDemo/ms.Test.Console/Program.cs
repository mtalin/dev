﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ms.Model;
using ms.DAL;
using ms.DAL.EF;
using ms.BL;

namespace msTestConsole
{
	class Program
	{
		static IContextFactory factory = new ContextFactory(false);
		static IUnitOfWork unitOfWork = factory.CurrentUoW;
		static IRepository repository = new Repository(unitOfWork);

		static void Main(string[] args)
		{
			try
			{
				//ValidateDepartmentsFacade();
				//LoadDepartmentFacade();
				AddDepartmentFacade();
				LoadUpadteDepartmentFacade();

				//ValidateDepartments();
				//AddDepartments();
				//LoadDepartment();
				//LoadUpadteDepartment();
			}
			catch (Exception ex)
			{
				while (ex != null)
				{
					Console.WriteLine(ex.Message);
					ex = ex.InnerException;
				}
			}
			Console.WriteLine("Press enter...");
			Console.ReadLine();
		}

		#region Department Entity

		public static void AddDepartments()
		{
			unitOfWork.BeginTransaction();

			repository.AddEntity(new PrimDepartment { Name = "Dep1", Description = "Description of Dep1", DtCreated = DateTime.Now });
			repository.AddEntity(new PrimDepartment { Name = "Dep2", Description = "Description of Dep2", DtCreated = DateTime.Now });

			unitOfWork.CommitTransaction();

			//unitOfWork.SaveChanges();
			//unitOfWork.RollbackTransaction();
		}

		public static void LoadDepartment()
		{
			var department = repository.GetEntity<PrimDepartment>(1);
			if (department != null)
				Console.WriteLine("Loaded record> ID: {0}, Name: {1}, Description: {2}\r\n", department.ID, department.Name, department.Description);
			else
				Console.WriteLine("Load failed");
		}

		private static void LoadUpadteDepartment()
		{
			var department = repository.GetEntity<PrimDepartment>(4);
			if (department != null)
			{
				Console.WriteLine("Loaded record> ID: {0}, Name: {1}, Description: {2}\r\n", department.ID, department.Name, department.Description);

				unitOfWork.BeginTransaction();

				department.Name = "DepNew z";
				department.Description = "Description of DepNew z";
				Console.WriteLine("Loaded record> ID: {0}, Name: {1}, Description: {2}\r\n", department.ID, department.Name, department.Description);

				unitOfWork.CommitTransaction();
			}
		}

		public static void ValidateDepartments()
		{
			Tuple<bool, string> valRes;

			Console.WriteLine("1");
			var dep = new PrimDepartment { Name = new string('*', 60), Description = "1Aaz1" };
			valRes = repository.IsEntityValid<PrimDepartment>(dep);
			if (valRes.Item1)
				Console.WriteLine("Validation OK!");
			else
				Console.WriteLine(valRes.Item2);

			Console.WriteLine("2");
			dep = new PrimDepartment { Name = new string('*', 50), Description = "Aaz1" };
			valRes = repository.IsEntityValid<PrimDepartment>(dep);
			if (valRes.Item1)
				Console.WriteLine("Validation OK!");
			else
				Console.WriteLine(valRes.Item2);

			Console.WriteLine("3");
			dep = new PrimDepartment { Name = new string('*', 50), Description = "Aaz1", DtCreated = DateTime.Now };
			valRes = repository.IsEntityValid<PrimDepartment>(dep);
			if (valRes.Item1)
				Console.WriteLine("Validation OK!");
			else
				Console.WriteLine(valRes.Item2);

			Console.WriteLine();
		}
		
		#endregion

		#region DepartmentFacade

		public static void ValidateDepartmentsFacade()
		{
			var depFacade = new DepartmentFacade(repository);
			depFacade.Entity.Name = new string('*', 60);
			depFacade.Entity.Description = "1Aaz1";

			string err;

			Console.WriteLine("1");

			err = depFacade.Validate();
			if (string.IsNullOrEmpty(err))
				Console.WriteLine("Validation OK!");
			else
				Console.WriteLine(err);
		}

		private static void LoadDepartmentFacade()
		{
			int id = 1;
			var depFacade = new DepartmentFacade(repository);
			depFacade.Load(id);

			if (depFacade.Loaded)
				Console.WriteLine("Loaded record> ID: {0}, Name: {1}, Description: {2}\r\n", depFacade.Entity.ID, depFacade.Entity.Name, depFacade.Entity.Description);
			else
				Console.WriteLine("ID {0} not found", id);
		}

		public static void AddDepartmentFacade()
		{
			var depFacade = new DepartmentFacade(repository);
			string s = DateTime.Now.Ticks.ToString();
			depFacade.Entity.Name = "Dep " + s;
			depFacade.Entity.Description = "Description of Dep " + s;
			depFacade.Entity.DtCreated = DateTime.Now.AddDays(1);

			depFacade.Update();
		}

		private static void LoadUpadteDepartmentFacade()
		{
			int id = 4;
			var depFacade = new DepartmentFacade(repository);
			depFacade.Load(id);
			if (depFacade.Loaded)
			{
				Console.WriteLine("Loaded record> ID: {0}, Name: {1}, Description: {2}\r\n", depFacade.Entity.ID, depFacade.Entity.Name, depFacade.Entity.Description);

				depFacade.Entity.Name = "Dep x";
				depFacade.Entity.Description = "Description of Dep x";

				Console.WriteLine("Loaded record> ID: {0}, Name: {1}, Description: {2}\r\n", depFacade.Entity.ID, depFacade.Entity.Name, depFacade.Entity.Description);
				depFacade.Update();
			}
			else
				Console.WriteLine("ID {0} not found", id);
		}
	
		#endregion
	}
}
