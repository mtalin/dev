using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MailAppliesDocument
    {
        public MailAppliesDocument()
        {
            this.MailAppliesSelectedDocuments = new List<MailAppliesSelectedDocument>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public virtual ICollection<MailAppliesSelectedDocument> MailAppliesSelectedDocuments { get; set; }
    }
}
