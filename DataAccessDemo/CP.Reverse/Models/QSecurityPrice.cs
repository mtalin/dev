using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QSecurityPrice
    {
        public int ID { get; set; }
        public string Symbol { get; set; }
        public Nullable<System.DateTime> DtPrice { get; set; }
        public Nullable<decimal> ClsPrice { get; set; }
        public Nullable<decimal> LowPrice { get; set; }
        public Nullable<decimal> HiPrice { get; set; }
        public string ErrorInfo { get; set; }
        public bool IgnoreSymbol { get; set; }
        public string CUSIP { get; set; }
    }
}
