using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxesSnapshot
    {
        public int ID { get; set; }
        public int Instance { get; set; }
        public int Tax { get; set; }
        public decimal Amount { get; set; }
        public decimal Adjustment { get; set; }
        public int FeeType { get; set; }
        public int Foundation { get; set; }
        public short Step { get; set; }
        public System.DateTime StepDate { get; set; }
        public byte Operation { get; set; }
        public Nullable<int> Previous { get; set; }
        public bool IsLatest { get; set; }
        public virtual TaxesSnapshotInstance TaxesSnapshotInstance { get; set; }
    }
}
