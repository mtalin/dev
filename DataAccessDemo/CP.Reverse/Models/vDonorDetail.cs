using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vDonorDetail
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int DonorType { get; set; }
        public Nullable<int> Person { get; set; }
        public Nullable<int> Company { get; set; }
        public Nullable<int> LastSpouse { get; set; }
        public Nullable<int> NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DOD { get; set; }
        public string InCareOf { get; set; }
        public string CompanyName { get; set; }
        public Nullable<System.DateTime> DateOfTrust { get; set; }
        public Nullable<int> SpouseID { get; set; }
        public Nullable<int> SpouseNamePrefix { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseMiddleName { get; set; }
        public string SpouseLastName { get; set; }
        public string SpouseEmail { get; set; }
        public string SpouseAddress1 { get; set; }
        public string SpouseAddress2 { get; set; }
        public string SpouseCity { get; set; }
        public string SpouseState { get; set; }
        public string SpouseZip { get; set; }
        public string SpouseCountry { get; set; }
        public Nullable<bool> IsSpouseAlive { get; set; }
        public string LongName { get; set; }
        public string FullName { get; set; }
        public string FullAddress { get; set; }
        public string FullName2 { get; set; }
        public string DonorTypeName { get; set; }
        public int DeliveryMethod { get; set; }
    }
}
