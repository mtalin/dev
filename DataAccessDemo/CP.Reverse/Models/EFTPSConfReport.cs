using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EFTPSConfReport
    {
        public EFTPSConfReport()
        {
            this.EFTPSConfirmations = new List<EFTPSConfirmation>();
        }

        public int ID { get; set; }
        public Nullable<System.DateTime> dtCreated { get; set; }
        public int Author { get; set; }
        public string FileName { get; set; }
        public string Body { get; set; }
        public Nullable<int> TotalRecords { get; set; }
        public Nullable<int> IgnoredRecords { get; set; }
        public virtual ICollection<EFTPSConfirmation> EFTPSConfirmations { get; set; }
        public virtual Person Person { get; set; }
    }
}
