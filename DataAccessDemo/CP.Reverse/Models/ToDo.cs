using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ToDo
    {
        public ToDo()
        {
            this.ToDoNotes = new List<ToDoNote>();
        }

        public int ID { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<ToDoNote> ToDoNotes { get; set; }
    }
}
