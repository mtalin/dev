using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CharityRequest
    {
        public CharityRequest()
        {
            this.CharityRequestsSteps = new List<CharityRequestsStep>();
        }

        public int ID { get; set; }
        public string CharityName { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Phone { get; set; }
        public string URL { get; set; }
        public string GeneralOrgInfo { get; set; }
        public string AdditionalInfo { get; set; }
        public Nullable<int> CharityEdit { get; set; }
        public System.DateTime dtCreated { get; set; }
        public Nullable<System.DateTime> dtClosed { get; set; }
        public int Step { get; set; }
        public int Step2 { get; set; }
        public int Type { get; set; }
        public Nullable<int> CharityID { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> PublicEIN { get; set; }
        public Nullable<bool> DiscloseIdentity { get; set; }
        public Nullable<int> GrantDetails { get; set; }
        public Nullable<int> Person { get; set; }
        public int Pending { get; set; }
        public string Email { get; set; }
        public bool CreatedOnBackEnd { get; set; }
        public string Country { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual GrantDetail GrantDetail { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<CharityRequestsStep> CharityRequestsSteps { get; set; }
        public virtual CharityRespons CharityRespons { get; set; }
    }
}
