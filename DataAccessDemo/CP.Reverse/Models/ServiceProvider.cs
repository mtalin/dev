using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ServiceProvider
    {
        public ServiceProvider()
        {
            this.CheckingAccounts = new List<CheckingAccount>();
            this.Contracts = new List<Contract>();
            this.DisbursementAccounts = new List<DisbursementAccount>();
            this.DisbursementAccountSubscribers = new List<DisbursementAccountSubscriber>();
            this.FoundationAccounts = new List<FoundationAccount>();
            this.FoundationAccounts1 = new List<FoundationAccount>();
            this.PricingSchedules = new List<PricingSchedule>();
            this.ServicePackages = new List<ServicePackage>();
        }

        public int ID { get; set; }
        public int StructuredCompany { get; set; }
        public string URL { get; set; }
        public bool SingleSignOn { get; set; }
        public string Design { get; set; }
        public string CustomerServiceContacts { get; set; }
        public string CustomContactsHTML { get; set; }
        public string Abbrev { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual ICollection<CheckingAccount> CheckingAccounts { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
        public virtual ICollection<DisbursementAccount> DisbursementAccounts { get; set; }
        public virtual ICollection<DisbursementAccountSubscriber> DisbursementAccountSubscribers { get; set; }
        public virtual ICollection<FoundationAccount> FoundationAccounts { get; set; }
        public virtual ICollection<FoundationAccount> FoundationAccounts1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<PricingSchedule> PricingSchedules { get; set; }
        public virtual ICollection<ServicePackage> ServicePackages { get; set; }
        public virtual StructuredCompany StructuredCompany1 { get; set; }
    }
}
