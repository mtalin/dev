using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CharityRespons
    {
        public int CharityRequest { get; set; }
        public Nullable<int> PublicEIN { get; set; }
        public string OrgName { get; set; }
        public string AltName { get; set; }
        public string InCareOf { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipPlus4 { get; set; }
        public Nullable<short> GroupExemptNum { get; set; }
        public Nullable<byte> SubsectCode { get; set; }
        public Nullable<byte> AffiliationCode { get; set; }
        public Nullable<byte> ClassCode1 { get; set; }
        public Nullable<byte> ClassCode2 { get; set; }
        public Nullable<byte> ClassCode3 { get; set; }
        public Nullable<byte> ClassCode4 { get; set; }
        public Nullable<System.DateTime> RulingDate { get; set; }
        public Nullable<byte> DeductCode { get; set; }
        public Nullable<byte> FoundationCode { get; set; }
        public Nullable<short> ActivityCode1 { get; set; }
        public Nullable<short> ActivityCode2 { get; set; }
        public Nullable<short> ActivityCode3 { get; set; }
        public Nullable<byte> OrgCode { get; set; }
        public Nullable<byte> DOJCode { get; set; }
        public Nullable<System.DateTime> AdvRuleExpire { get; set; }
        public Nullable<System.DateTime> TaxPeriod { get; set; }
        public Nullable<byte> AssetCode { get; set; }
        public Nullable<byte> IncomeCode { get; set; }
        public Nullable<byte> FilingReqCode { get; set; }
        public Nullable<bool> FilingReqCode2 { get; set; }
        public Nullable<byte> AcctPeriod { get; set; }
        public Nullable<decimal> AssetAmount { get; set; }
        public Nullable<decimal> IncomeAmount { get; set; }
        public string NTEECode { get; set; }
        public string SortName { get; set; }
        public string Type { get; set; }
        public string Phone { get; set; }
        public string URL { get; set; }
        public Nullable<bool> PublicSch { get; set; }
        public Nullable<bool> PublicLib { get; set; }
        public Nullable<bool> Religious { get; set; }
        public Nullable<bool> SOI { get; set; }
        public Nullable<bool> IRB { get; set; }
        public Nullable<bool> Government { get; set; }
        public Nullable<bool> GuideStar { get; set; }
        public Nullable<bool> Removed { get; set; }
        public Nullable<bool> Hold { get; set; }
        public string HoldReason { get; set; }
        public string OrgInfo { get; set; }
        public virtual CharityRequest CharityRequest1 { get; set; }
    }
}
