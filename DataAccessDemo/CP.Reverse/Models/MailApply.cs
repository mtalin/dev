using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MailApply
    {
        public MailApply()
        {
            this.MailAppliesSelectedDocuments = new List<MailAppliesSelectedDocument>();
            this.MailAppliesSteps = new List<MailAppliesStep>();
        }

        public int ID { get; set; }
        public int ReferralSource { get; set; }
        public bool ForAdvisor { get; set; }
        public int ForClient { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitials { get; set; }
        public string LastName { get; set; }
        public int Address { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string EMail { get; set; }
        public Nullable<int> Message { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Step { get; set; }
        public string Note { get; set; }
        public string RejectNote { get; set; }
        public Nullable<int> RejectNoteID { get; set; }
        public int Source { get; set; }
        public Nullable<System.DateTime> DtAlert { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual ICollection<MailAppliesSelectedDocument> MailAppliesSelectedDocuments { get; set; }
        public virtual ICollection<MailAppliesStep> MailAppliesSteps { get; set; }
    }
}
