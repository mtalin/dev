using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class HouseholdAccount
    {
        public HouseholdAccount()
        {
            this.DonationRequests = new List<DonationRequest>();
            this.GrantDetails = new List<GrantDetail>();
            this.HouseholdAccountsSteps = new List<HouseholdAccountsStep>();
            this.HouseholdDocuments = new List<HouseholdDocument>();
            this.HouseholdMembers = new List<HouseholdMember>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public int Step { get; set; }
        public string Phone { get; set; }
        public int Flags { get; set; }
        public decimal Saving { get; set; }
        public decimal IRA { get; set; }
        public decimal ValueOfHouse { get; set; }
        public decimal OtherAssets { get; set; }
        public decimal CCDebt { get; set; }
        public decimal CarLoans { get; set; }
        public decimal Mortgage { get; set; }
        public decimal OtherLoans { get; set; }
        public Nullable<decimal> InitialAmount { get; set; }
        public Nullable<System.DateTime> DtAuthorized { get; set; }
        public Nullable<int> AuthorizedBy { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtApproved { get; set; }
        public Nullable<System.DateTime> DtExpired { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public bool AllowMailingCheckDirectly { get; set; }
        public Nullable<System.DateTime> dtDeleted { get; set; }
        public Nullable<int> old_AuthorizedBy { get; set; }
        public bool GmxRef { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public virtual ICollection<DonationRequest> DonationRequests { get; set; }
        public virtual FoundationProject FoundationProject1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<GrantDetail> GrantDetails { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<HouseholdAccountsStep> HouseholdAccountsSteps { get; set; }
        public virtual ICollection<HouseholdDocument> HouseholdDocuments { get; set; }
        public virtual ICollection<HouseholdMember> HouseholdMembers { get; set; }
    }
}
