using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class old_PendingTransactItems
    {
        public old_PendingTransactItems()
        {
            this.old_PendingTransactItemSteps = new List<old_PendingTransactItemSteps>();
        }

        public int ID { get; set; }
        public int PendingTransact { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public int Type { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Step { get; set; }
        public string CheckNo { get; set; }
        public Nullable<System.DateTime> DtCheck { get; set; }
        public virtual PendingTransact PendingTransact1 { get; set; }
        public virtual ICollection<old_PendingTransactItemSteps> old_PendingTransactItemSteps { get; set; }
    }
}
