using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Proposal
    {
        public Proposal()
        {
            this.GrantProposals = new List<GrantProposal>();
            this.MeetingsProposals = new List<MeetingsProposal>();
            this.Proposals1 = new List<Proposal>();
            this.ProposalsSteps = new List<ProposalsStep>();
            this.ProposalStaffs = new List<ProposalStaff>();
            this.StaffingProposals = new List<StaffingProposal>();
        }

        public int ID { get; set; }
        public Nullable<int> Voting { get; set; }
        public int Foundation { get; set; }
        public int Type { get; set; }
        public string Purpose { get; set; }
        public Nullable<int> LegalForm { get; set; }
        public int Step { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public int SpecialAuthor { get; set; }
        public Nullable<int> Original { get; set; }
        public virtual Document1 Document { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<GrantProposal> GrantProposals { get; set; }
        public virtual ICollection<MeetingsProposal> MeetingsProposals { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<Proposal> Proposals1 { get; set; }
        public virtual Proposal Proposal1 { get; set; }
        public virtual Voting Voting1 { get; set; }
        public virtual ICollection<ProposalsStep> ProposalsSteps { get; set; }
        public virtual ICollection<ProposalStaff> ProposalStaffs { get; set; }
        public virtual ICollection<StaffingProposal> StaffingProposals { get; set; }
    }
}
