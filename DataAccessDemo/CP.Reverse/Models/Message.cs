using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Message
    {
        public Message()
        {
            this.CharityRequestsSteps = new List<CharityRequestsStep>();
            this.ContactsSteps = new List<ContactsStep>();
            this.DonationCertificates = new List<DonationCertificate>();
            this.ExpensesSteps = new List<ExpensesStep>();
            this.FinancialAdvisorsRequestsSteps = new List<FinancialAdvisorsRequestsStep>();
            this.GrantMessages = new List<GrantMessage>();
            this.GrantsSteps = new List<GrantsStep>();
            this.HouseholdAccountsSteps = new List<HouseholdAccountsStep>();
            this.MailApplies = new List<MailApply>();
            this.MailAppliesSteps = new List<MailAppliesStep>();
            this.MeetingsSteps = new List<MeetingsStep>();
            this.PaymentsSteps = new List<PaymentsStep>();
            this.PendingTransactsSteps = new List<PendingTransactsStep>();
            this.ProposalsSteps = new List<ProposalsStep>();
            this.RequestDonorsSteps = new List<RequestDonorsStep>();
            this.ShellCorporationsSteps = new List<ShellCorporationsStep>();
            this.TaxesSteps = new List<TaxesStep>();
            this.UnallocatedSharesSteps = new List<UnallocatedSharesStep>();
            this.ContactsSteps1 = new List<ContactsStep>();
            this.MailAppliesSteps1 = new List<MailAppliesStep>();
        }

        public int ID { get; set; }
        public Nullable<int> Foundation { get; set; }
        public int Person { get; set; }
        public string SendFrom { get; set; }
        public string SendTo { get; set; }
        public string CopyTo { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public int Theme { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtSent { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public Nullable<int> ShellCorporation { get; set; }
        public string PrivateCopyTo { get; set; }
        public string TemplateItem { get; set; }
        public string XMLData { get; set; }
        public Nullable<int> Event { get; set; }
        public Nullable<int> State { get; set; }
        public Nullable<int> EmailTemplate { get; set; }
        public virtual ICollection<CharityRequestsStep> CharityRequestsSteps { get; set; }
        public virtual ICollection<ContactsStep> ContactsSteps { get; set; }
        public virtual ICollection<DonationCertificate> DonationCertificates { get; set; }
        public virtual EMail EMail { get; set; }
        public virtual EmailTemplate EmailTemplate1 { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual ICollection<ExpensesStep> ExpensesSteps { get; set; }
        public virtual ICollection<FinancialAdvisorsRequestsStep> FinancialAdvisorsRequestsSteps { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<GrantMessage> GrantMessages { get; set; }
        public virtual ICollection<GrantsStep> GrantsSteps { get; set; }
        public virtual ICollection<HouseholdAccountsStep> HouseholdAccountsSteps { get; set; }
        public virtual ICollection<MailApply> MailApplies { get; set; }
        public virtual ICollection<MailAppliesStep> MailAppliesSteps { get; set; }
        public virtual ICollection<MeetingsStep> MeetingsSteps { get; set; }
        public virtual PostalAddress PostalAddress1 { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<PaymentsStep> PaymentsSteps { get; set; }
        public virtual ICollection<PendingTransactsStep> PendingTransactsSteps { get; set; }
        public virtual ICollection<ProposalsStep> ProposalsSteps { get; set; }
        public virtual ICollection<RequestDonorsStep> RequestDonorsSteps { get; set; }
        public virtual ICollection<ShellCorporationsStep> ShellCorporationsSteps { get; set; }
        public virtual ICollection<TaxesStep> TaxesSteps { get; set; }
        public virtual ICollection<UnallocatedSharesStep> UnallocatedSharesSteps { get; set; }
        public virtual ICollection<ContactsStep> ContactsSteps1 { get; set; }
        public virtual ICollection<MailAppliesStep> MailAppliesSteps1 { get; set; }
    }
}
