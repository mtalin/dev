using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class enums_ValuationTypes
    {
        public int ID { get; set; }
        public int Category { get; set; }
        public int ItemID { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Abbrev { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public string TheValue { get; set; }
        public string Tag { get; set; }
        public Nullable<int> GroupID { get; set; }
    }
}
