using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ServicePackage
    {
        public ServicePackage()
        {
            this.ContractServicePackages = new List<ContractServicePackage>();
            this.ServicePackageServices = new List<ServicePackageService>();
        }

        public int ID { get; set; }
        public int ServiceProvider { get; set; }
        public string Name { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual ICollection<ContractServicePackage> ContractServicePackages { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ServiceProvider ServiceProvider1 { get; set; }
        public virtual ICollection<ServicePackageService> ServicePackageServices { get; set; }
    }
}
