using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class enums_ShellFounders
    {
        public int ID { get; set; }
        public Nullable<int> NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public Nullable<int> NameSuffix { get; set; }
        public Nullable<int> KeyQuestion { get; set; }
        public string KeyAnswer { get; set; }
        public string SSN { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<bool> Gender { get; set; }
        public Nullable<int> MaritalStatus { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessFax { get; set; }
        public string Employer { get; set; }
        public Nullable<int> SalaryRange { get; set; }
        public int RegistrationStep { get; set; }
        public Nullable<bool> MatchingGift { get; set; }
        public string AnotherPosition { get; set; }
        public string ShareHolder { get; set; }
        public Nullable<bool> StockExchange { get; set; }
        public Nullable<int> SHAddress { get; set; }
        public bool AllertMessage { get; set; }
        public int Rank { get; set; }
        public Nullable<int> ProgramCode { get; set; }
        public Nullable<System.DateTime> DOD { get; set; }
        public string EmployeeID { get; set; }
        public Nullable<int> DesignMode { get; set; }
        public string Rights { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public Nullable<int> PostalAddress2 { get; set; }
        public Nullable<int> PostalAddress3 { get; set; }
        public string CellPhone { get; set; }
        public string OtherPhone { get; set; }
        public string Assistant { get; set; }
        public string AsstPhone { get; set; }
        public string Dear { get; set; }
        public bool OnlineAccess { get; set; }
        public bool AllowEmail { get; set; }
        public bool AllowMailing { get; set; }
        public Nullable<int> MailingTax { get; set; }
        public Nullable<int> MailingTaxAddress { get; set; }
        public Nullable<int> MailingMarketing { get; set; }
        public Nullable<int> MailingMarketingAddress { get; set; }
        public Nullable<int> MailingInformal { get; set; }
        public Nullable<int> MailingInformalAddress { get; set; }
        public Nullable<int> MailingFormal { get; set; }
        public Nullable<int> MailingFormalAddress { get; set; }
        public Nullable<int> MailingNewServices { get; set; }
        public Nullable<int> MailingNewServicesAddress { get; set; }
        public string MailingDescription { get; set; }
        public bool AllowCall { get; set; }
        public bool CallGrantsQuestions { get; set; }
        public bool CallNewFeaturesReview { get; set; }
        public bool CallMarketing { get; set; }
        public bool CallMisc { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public Nullable<int> MoreThanMoney { get; set; }
        public Nullable<int> MoreThanMoneyAddress { get; set; }
        public bool ExpenseReinbursement { get; set; }
        public Nullable<int> ExpenseReinbursementAddress { get; set; }
        public string GmxRefs { get; set; }
        public string Position { get; set; }
    }
}
