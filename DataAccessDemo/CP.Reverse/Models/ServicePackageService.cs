using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ServicePackageService
    {
        public int ID { get; set; }
        public int ServicePackage { get; set; }
        public int Service { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ServicePackage ServicePackage1 { get; set; }
    }
}
