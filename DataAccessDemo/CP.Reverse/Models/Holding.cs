using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Holding
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int FoundationAccount { get; set; }
        public int Asset { get; set; }
        public Nullable<int> FirstLot { get; set; }
        public Nullable<int> ParentLot { get; set; }
        public System.DateTime DtCreated { get; set; }
        public System.DateTime DtDeposit { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public System.DateTime DtClosed2 { get; set; }
        public bool HasChild { get; set; }
        public bool IsRoot { get; set; }
        public int Transact { get; set; }
        public int TransactType { get; set; }
        public Nullable<decimal> UnitsNumber { get; set; }
        public Nullable<decimal> PackageSize { get; set; }
        public Nullable<decimal> V_SalePrice { get; set; }
        public Nullable<decimal> V_CostBasis { get; set; }
        public Nullable<decimal> V_BookBasis { get; set; }
        public Nullable<decimal> V_Interest { get; set; }
        public Nullable<decimal> V_Dividend { get; set; }
        public Nullable<decimal> V_CapitalGain { get; set; }
        public Nullable<decimal> V_PhantomGain { get; set; }
        public Nullable<decimal> V_InterestAdj { get; set; }
    }
}
