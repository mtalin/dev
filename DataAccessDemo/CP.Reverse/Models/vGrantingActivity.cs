using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vGrantingActivity
    {
        public Nullable<int> Person { get; set; }
        public Nullable<int> Foundation { get; set; }
        public int StructuredCompany { get; set; }
        public Nullable<decimal> CompletedAmount { get; set; }
        public Nullable<decimal> PendingAmount { get; set; }
        public Nullable<decimal> WaitingApprovalAmount { get; set; }
        public Nullable<decimal> WaitingRequestAmount { get; set; }
        public Nullable<decimal> GrantRecomendationAmount { get; set; }
        public System.DateTime GrantDate { get; set; }
    }
}
