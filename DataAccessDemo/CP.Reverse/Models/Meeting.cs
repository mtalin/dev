using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Meeting
    {
        public Meeting()
        {
            this.Meetings1 = new List<Meeting>();
            this.MeetingsAttendeds = new List<MeetingsAttended>();
            this.MeetingsProposals = new List<MeetingsProposal>();
            this.MeetingsSteps = new List<MeetingsStep>();
            this.VotingOffers = new List<VotingOffer>();
        }

        public int ID { get; set; }
        public Nullable<int> Original { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> Document { get; set; }
        public System.DateTime DtHeld { get; set; }
        public int Method { get; set; }
        public string City { get; set; }
        public Nullable<int> State { get; set; }
        public int Period { get; set; }
        public string Goals { get; set; }
        public int Step { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public bool IsCustom { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<Meeting> Meetings1 { get; set; }
        public virtual Meeting Meeting1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<MeetingsAttended> MeetingsAttendeds { get; set; }
        public virtual ICollection<MeetingsProposal> MeetingsProposals { get; set; }
        public virtual ICollection<MeetingsStep> MeetingsSteps { get; set; }
        public virtual ICollection<VotingOffer> VotingOffers { get; set; }
    }
}
