using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vCheckRegister
    {
        public int PaymentID { get; set; }
        public Nullable<int> CheckingAccount { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<System.DateTime> CheckDate { get; set; }
        public string CheckNo { get; set; }
        public Nullable<decimal> CheckAmount { get; set; }
        public string TypeName { get; set; }
        public string PayableTo { get; set; }
        public string FoundationName { get; set; }
        public string AccountInfo { get; set; }
        public Nullable<int> TaxID { get; set; }
        public Nullable<int> ExpenseID { get; set; }
        public Nullable<int> GrantID { get; set; }
        public Nullable<int> PendingTransactID { get; set; }
        public string VoidNote { get; set; }
        public Nullable<System.DateTime> DtVoid { get; set; }
        public Nullable<System.DateTime> DtStepCanceled { get; set; }
        public Nullable<System.DateTime> DtTransact { get; set; }
        public Nullable<System.DateTime> DtProcessed { get; set; }
        public Nullable<System.DateTime> SortCheckDate { get; set; }
        public int CheckID { get; set; }
        public int PaymentType { get; set; }
        public string FullAccountName { get; set; }
        public string FullLinkedAccountName { get; set; }
        public Nullable<decimal> GrantAmount { get; set; }
        public Nullable<decimal> TaxAmount { get; set; }
        public Nullable<decimal> ExpenseAmount { get; set; }
        public Nullable<decimal> PendingTransactAmount { get; set; }
        public Nullable<int> IsVoid { get; set; }
        public Nullable<bool> IsGrouped { get; set; }
        public Nullable<int> ManualPayment { get; set; }
        public Nullable<int> VoidType { get; set; }
        public bool IsModified { get; set; }
    }
}
