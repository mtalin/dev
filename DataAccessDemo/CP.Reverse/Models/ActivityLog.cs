using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ActivityLog
    {
        public ActivityLog()
        {
            this.ActivityLogItems = new List<ActivityLogItem>();
            this.TrustedActions = new List<TrustedAction>();
        }

        public int ID { get; set; }
        public int Person { get; set; }
        public Nullable<int> Foundation { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string UserAgent { get; set; }
        public string SessionId { get; set; }
        public virtual ICollection<ActivityLogItem> ActivityLogItems { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<TrustedAction> TrustedActions { get; set; }
    }
}
