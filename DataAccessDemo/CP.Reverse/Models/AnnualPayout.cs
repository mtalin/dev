using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AnnualPayout
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int TheYear { get; set; }
        public decimal MIR { get; set; }
        public decimal DA { get; set; }
        public decimal DA_PlusEGC { get; set; }
        public decimal EGC { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
