using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FinancialAdvisorGroup
    {
        public FinancialAdvisorGroup()
        {
            this.EmailTemplates = new List<EmailTemplate>();
            this.ExcludedEmailApplicants = new List<ExcludedEmailApplicant>();
            this.FinancialAdvisors = new List<FinancialAdvisor>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int FinancialPartner { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public string PrimaryContact { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Info { get; set; }
        public int Design { get; set; }
        public Nullable<int> Department { get; set; }
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
        public virtual ICollection<ExcludedEmailApplicant> ExcludedEmailApplicants { get; set; }
        public virtual PostalAddress PostalAddress1 { get; set; }
        public virtual ICollection<FinancialAdvisor> FinancialAdvisors { get; set; }
    }
}
