using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QPortfolio
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Foundation { get; set; }
        public string Security { get; set; }
        public Nullable<int> Shares { get; set; }
        public Nullable<decimal> Price { get; set; }
        public decimal CostBasis { get; set; }
        public decimal PreTaxGain { get; set; }
        public decimal Balance { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
