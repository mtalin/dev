using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Note
    {
        public Note()
        {
            this.AssetBooks = new List<AssetBook>();
            this.AssetValuations = new List<AssetValuation>();
            this.CharityRequestsSteps = new List<CharityRequestsStep>();
            this.ContactsSteps = new List<ContactsStep>();
            this.CRMTasksSteps = new List<CRMTasksStep>();
            this.EFTPSEnrollmentsSteps = new List<EFTPSEnrollmentsStep>();
            this.ExpensesSteps = new List<ExpensesStep>();
            this.FinancialAdvisorsRequestsSteps = new List<FinancialAdvisorsRequestsStep>();
            this.GrantsSteps = new List<GrantsStep>();
            this.HouseholdAccountsSteps = new List<HouseholdAccountsStep>();
            this.IndividualNotes = new List<IndividualNote>();
            this.MailApplies = new List<MailApply>();
            this.MailAppliesSteps = new List<MailAppliesStep>();
            this.MeetingsSteps = new List<MeetingsStep>();
            this.PaymentsSteps = new List<PaymentsStep>();
            this.PendingTransactsSteps = new List<PendingTransactsStep>();
            this.ProposalsSteps = new List<ProposalsStep>();
            this.RequestDonorsSteps = new List<RequestDonorsStep>();
            this.ShellCorporationsSteps = new List<ShellCorporationsStep>();
            this.TaxesSteps = new List<TaxesStep>();
            this.TaxReceipts = new List<TaxReceipt>();
            this.TaxReturnsSteps = new List<TaxReturnsStep>();
            this.TransactNotes = new List<TransactNote>();
            this.UnallocatedSharesSteps = new List<UnallocatedSharesStep>();
        }

        public int ID { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public Nullable<int> NoteCategory { get; set; }
        public Nullable<int> ShellCorporation { get; set; }
        public Nullable<int> Foundation { get; set; }
        public virtual ICollection<AssetBook> AssetBooks { get; set; }
        public virtual ICollection<AssetValuation> AssetValuations { get; set; }
        public virtual ICollection<CharityRequestsStep> CharityRequestsSteps { get; set; }
        public virtual ICollection<ContactsStep> ContactsSteps { get; set; }
        public virtual ICollection<CRMTasksStep> CRMTasksSteps { get; set; }
        public virtual ICollection<EFTPSEnrollmentsStep> EFTPSEnrollmentsSteps { get; set; }
        public virtual ICollection<ExpensesStep> ExpensesSteps { get; set; }
        public virtual ICollection<FinancialAdvisorsRequestsStep> FinancialAdvisorsRequestsSteps { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<GrantsStep> GrantsSteps { get; set; }
        public virtual ICollection<HouseholdAccountsStep> HouseholdAccountsSteps { get; set; }
        public virtual ICollection<IndividualNote> IndividualNotes { get; set; }
        public virtual ICollection<MailApply> MailApplies { get; set; }
        public virtual ICollection<MailAppliesStep> MailAppliesSteps { get; set; }
        public virtual ICollection<MeetingsStep> MeetingsSteps { get; set; }
        public virtual NoteCategory NoteCategory1 { get; set; }
        public virtual ShellCorporation ShellCorporation1 { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<PaymentsStep> PaymentsSteps { get; set; }
        public virtual ICollection<PendingTransactsStep> PendingTransactsSteps { get; set; }
        public virtual ICollection<ProposalsStep> ProposalsSteps { get; set; }
        public virtual ICollection<RequestDonorsStep> RequestDonorsSteps { get; set; }
        public virtual ICollection<ShellCorporationsStep> ShellCorporationsSteps { get; set; }
        public virtual ICollection<TaxesStep> TaxesSteps { get; set; }
        public virtual ICollection<TaxReceipt> TaxReceipts { get; set; }
        public virtual ICollection<TaxReturnsStep> TaxReturnsSteps { get; set; }
        public virtual ICollection<TransactNote> TransactNotes { get; set; }
        public virtual ICollection<UnallocatedSharesStep> UnallocatedSharesSteps { get; set; }
    }
}
