using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vQuestEachHasIssue
    {
        public int ID { get; set; }
        public Nullable<int> Section { get; set; }
        public string textQuestion { get; set; }
        public string numQuestion { get; set; }
        public bool hasIssues { get; set; }
        public Nullable<bool> Resolved { get; set; }
        public string ResolveNote { get; set; }
    }
}
