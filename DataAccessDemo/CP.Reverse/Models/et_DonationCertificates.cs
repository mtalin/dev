using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_DonationCertificates
    {
        public int ID { get; set; }
        public string FoundationName { get; set; }
        public string DateCreated { get; set; }
        public decimal InitialAmount { get; set; }
        public Nullable<decimal> AmountLeft { get; set; }
        public int Author { get; set; }
        public int Recipient { get; set; }
        public int isExpired { get; set; }
        public string DtExpiration { get; set; }
        public bool SingleGrant { get; set; }
    }
}
