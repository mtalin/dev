using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EmailRecipient
    {
        public int ID { get; set; }
        public int EmailTemplate { get; set; }
        public Nullable<int> Recipient { get; set; }
        public int AddressGroup { get; set; }
        public virtual EmailTemplate EmailTemplate1 { get; set; }
        public virtual Recipient Recipient1 { get; set; }
    }
}
