using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QualifyingExpendituresInProcess
    {
        public Nullable<int> Foundation { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Type { get; set; }
        public string TypeName { get; set; }
        public string Payee { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public int Step { get; set; }
        public string Category { get; set; }
    }
}
