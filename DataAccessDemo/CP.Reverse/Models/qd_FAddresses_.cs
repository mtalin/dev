using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class qd_FAddresses_
    {
        public int id { get; set; }
        public Nullable<System.DateTime> dtCreated { get; set; }
        public int Questionnaire { get; set; }
        public int Type { get; set; }
        public bool UseFsAddress { get; set; }
        public bool IsNew { get; set; }
        public bool Updated { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
    }
}
