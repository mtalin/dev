using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class SummaryAccount
    {
        public int ID { get; set; }
        public decimal BankBalance { get; set; }
        public decimal Balance1 { get; set; }
        public decimal Balance2 { get; set; }
        public decimal Balance3 { get; set; }
        public decimal CashBalace { get; set; }
        public decimal SecurityBalance { get; set; }
    }
}
