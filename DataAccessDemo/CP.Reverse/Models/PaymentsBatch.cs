using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PaymentsBatch
    {
        public PaymentsBatch()
        {
            this.Payments = new List<Payment>();
        }

        public int ID { get; set; }
        public int BatchNumber { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public Nullable<System.DateTime> DtSent { get; set; }
        public Nullable<int> ClosedBy { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
    }
}
