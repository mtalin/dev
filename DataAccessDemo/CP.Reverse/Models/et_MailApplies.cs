using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_MailApplies
    {
        public int ID { get; set; }
        public string DateCreated { get; set; }
        public int ReferralSource { get; set; }
        public bool ForAdvisor { get; set; }
        public int ForClient { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitials { get; set; }
        public string LastName { get; set; }
        public int PostalAddress { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string EMail { get; set; }
        public int Source { get; set; }
        public string FullName { get; set; }
    }
}
