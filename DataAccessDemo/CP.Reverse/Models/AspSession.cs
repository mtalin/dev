using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AspSession
    {
        public int Id { get; set; }
        public string SessionId { get; set; }
        public string AuthorInfo { get; set; }
        public System.DateTime DtCreated { get; set; }
        public System.DateTime DtUpdated { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public int SessionType { get; set; }
    }
}
