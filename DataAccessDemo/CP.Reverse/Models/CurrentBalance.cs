using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CurrentBalance
    {
        public int ID { get; set; }
        public int FinancialPartner { get; set; }
        public int Foundation { get; set; }
        public int FoundationAccount { get; set; }
        public decimal Cash { get; set; }
        public decimal Securities { get; set; }
        public System.DateTime DtBalance { get; set; }
        public int QBalance { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
    }
}
