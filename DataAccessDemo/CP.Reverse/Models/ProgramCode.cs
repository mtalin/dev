using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ProgramCode
    {
        public ProgramCode()
        {
            this.Foundations = new List<Foundation>();
            this.Persons = new List<Person>();
        }

        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual ICollection<Foundation> Foundations { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
    }
}
