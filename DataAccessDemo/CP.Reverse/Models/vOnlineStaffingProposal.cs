using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vOnlineStaffingProposal
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public string Purpose { get; set; }
        public Nullable<int> LegalForm { get; set; }
        public int Step { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public int VotingID { get; set; }
        public Nullable<int> VoteID { get; set; }
        public Nullable<int> VotePerson { get; set; }
        public Nullable<int> VoteAppointment { get; set; }
        public Nullable<int> VoteStep { get; set; }
        public Nullable<System.DateTime> VoteDtCreated { get; set; }
    }
}
