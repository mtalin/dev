using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FAPPackageRequest
    {
        public int ID { get; set; }
        public int Advisor { get; set; }
        public int Package { get; set; }
        public int DeliveryType { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtSent { get; set; }
        public int DestinationAddress { get; set; }
        public virtual FAPPackage FAPPackage { get; set; }
        public virtual FinancialAdvisor FinancialAdvisor { get; set; }
        public virtual PostalAddress PostalAddress { get; set; }
    }
}
