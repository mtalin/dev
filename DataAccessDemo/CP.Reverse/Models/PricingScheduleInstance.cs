using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PricingScheduleInstance
    {
        public PricingScheduleInstance()
        {
            this.ActivityPricings = new List<ActivityPricing>();
            this.FeePricings = new List<FeePricing>();
        }

        public int ID { get; set; }
        public Nullable<int> PricingSchedule { get; set; }
        public Nullable<int> ContractPricing { get; set; }
        public bool IsActive { get; set; }
        public int PricingSource { get; set; }
        public Nullable<int> BillingEvent { get; set; }
        public Nullable<int> BillingRecipient { get; set; }
        public Nullable<int> BillingDepartment { get; set; }
        public Nullable<int> BillingContact { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual ICollection<ActivityPricing> ActivityPricings { get; set; }
        public virtual CompanyElement CompanyElement { get; set; }
        public virtual CompanyPosition CompanyPosition { get; set; }
        public virtual ContractPricing ContractPricing1 { get; set; }
        public virtual Department Department { get; set; }
        public virtual ICollection<FeePricing> FeePricings { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual PricingSchedule PricingSchedule1 { get; set; }
    }
}
