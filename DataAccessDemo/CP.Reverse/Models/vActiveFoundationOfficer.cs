using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vActiveFoundationOfficer
    {
        public int Foundation { get; set; }
        public int Person { get; set; }
        public string Name { get; set; }
        public string PositionName { get; set; }
        public Nullable<int> Appointment { get; set; }
    }
}
