using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ObjectsPermission
    {
        public ObjectsPermission()
        {
            this.CompanyElements = new List<CompanyElement>();
            this.ContractPlayerRoles = new List<ContractPlayerRole>();
            this.ContractPlayers = new List<ContractPlayer>();
            this.StaffingPoliciesTemplates = new List<StaffingPoliciesTemplate>();
            this.StructuredCompanyTypes = new List<StructuredCompanyType>();
            this.SystemPositions = new List<SystemPosition>();
        }

        public int Id { get; set; }
        public string Object { get; set; }
        public string AppliedPermissions { get; set; }
        public string ResolvedPermissions { get; set; }
        public string NoteText { get; set; }
        public Nullable<int> NoteAuthor { get; set; }
        public virtual ICollection<CompanyElement> CompanyElements { get; set; }
        public virtual ICollection<ContractPlayerRole> ContractPlayerRoles { get; set; }
        public virtual ICollection<ContractPlayer> ContractPlayers { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<StaffingPoliciesTemplate> StaffingPoliciesTemplates { get; set; }
        public virtual ICollection<StructuredCompanyType> StructuredCompanyTypes { get; set; }
        public virtual ICollection<SystemPosition> SystemPositions { get; set; }
    }
}
