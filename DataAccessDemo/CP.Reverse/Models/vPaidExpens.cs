using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vPaidExpens
    {
        public byte partid { get; set; }
        public string partname { get; set; }
        public short LineID { get; set; }
        public Nullable<System.DateTime> dttransact { get; set; }
        public int foundation { get; set; }
        public Nullable<int> tid { get; set; }
        public Nullable<int> exid { get; set; }
        public Nullable<int> taxid { get; set; }
        public Nullable<int> PayeeID { get; set; }
        public Nullable<int> TrType { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public string Category { get; set; }
        public string Desription { get; set; }
        public string PayableTo { get; set; }
        public string CheckN { get; set; }
        public string CheckDate { get; set; }
        public string InvoiceNumber { get; set; }
        public Nullable<int> InvoiceDocument { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> assetsAmount { get; set; }
        public short registryrule { get; set; }
    }
}
