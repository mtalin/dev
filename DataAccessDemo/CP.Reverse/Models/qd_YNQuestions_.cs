using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class qd_YNQuestions_
    {
        public int id { get; set; }
        public Nullable<System.DateTime> dtCreated { get; set; }
        public int Questionnaire { get; set; }
        public int Section { get; set; }
        public int Question { get; set; }
        public bool Checked { get; set; }
    }
}
