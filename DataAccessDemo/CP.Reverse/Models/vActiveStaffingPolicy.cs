using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vActiveStaffingPolicy
    {
        public int ID { get; set; }
        public int StructuredCompany { get; set; }
        public int SystemPosition { get; set; }
        public Nullable<int> MinInstances { get; set; }
        public Nullable<int> MaxInstances { get; set; }
        public Nullable<int> PeriodType { get; set; }
        public Nullable<int> ExpirationPeriod { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string Name { get; set; }
        public Nullable<int> ObjectPermissions { get; set; }
    }
}
