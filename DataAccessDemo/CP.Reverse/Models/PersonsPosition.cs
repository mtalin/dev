using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PersonsPosition
    {
        public int ID { get; set; }
        public int Person { get; set; }
        public string Company { get; set; }
        public string Appointment { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
