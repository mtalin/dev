using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DefaultCharity
    {
        public int Foundation { get; set; }
        public int CharityEIN { get; set; }
        public double DonatePercent { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
