using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PartnershipAmount
    {
        public int Id { get; set; }
        public int Author { get; set; }
        public int Line { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> UBI { get; set; }
        public Nullable<decimal> BlockedUBI { get; set; }
        public int Partnership { get; set; }
        public virtual Partnership Partnership1 { get; set; }
        public virtual User User { get; set; }
    }
}
