using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vDocuments8566
    {
        public Nullable<int> DocId { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> ShellCorporation { get; set; }
    }
}
