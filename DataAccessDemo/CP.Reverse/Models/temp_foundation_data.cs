using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class temp_foundation_data
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Year { get; set; }
        public Nullable<int> UI_Year1 { get; set; }
        public Nullable<int> UI_Year2 { get; set; }
        public Nullable<int> UI_Year3 { get; set; }
        public Nullable<int> UI_Year4 { get; set; }
        public Nullable<int> UISP_Year1 { get; set; }
        public Nullable<int> UISP_Year2 { get; set; }
        public Nullable<int> UISP_Year3 { get; set; }
        public Nullable<int> UISP_Year4 { get; set; }
        public Nullable<decimal> TOTAL_UI { get; set; }
        public Nullable<int> UIA_Year1 { get; set; }
        public Nullable<int> UIA_Year2 { get; set; }
        public Nullable<int> UIA_Year3 { get; set; }
        public Nullable<int> UIA_Year4 { get; set; }
        public Nullable<decimal> UIA_Amount1 { get; set; }
        public Nullable<decimal> UIA_Amount2 { get; set; }
        public Nullable<decimal> UIA_Amount3 { get; set; }
        public Nullable<decimal> UIA_Amount4 { get; set; }
        public Nullable<bool> Percent_NII { get; set; }
        public Nullable<decimal> TotalDonations { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
