using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Contract
    {
        public Contract()
        {
            this.ContractPlayers = new List<ContractPlayer>();
            this.ContractPricings = new List<ContractPricing>();
            this.Contracts1 = new List<Contract>();
            this.ContractServicePackages = new List<ContractServicePackage>();
            this.ContractServices = new List<ContractService>();
            this.DisbursementAccountSubscribers = new List<DisbursementAccountSubscriber>();
            this.FeeCalculations = new List<FeeCalculation>();
            this.Taxes = new List<Tax>();
            this.TransferProfiles = new List<TransferProfile>();
        }

        public int ID { get; set; }
        public string ContractNo { get; set; }
        public int ServiceProvider { get; set; }
        public Nullable<int> Customer { get; set; }
        public Nullable<int> ParentContract { get; set; }
        public bool IsActive { get; set; }
        public bool IsTemplate { get; set; }
        public Nullable<System.DateTime> DtCommence { get; set; }
        public Nullable<System.DateTime> DtTerminate { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtActivated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual CompanyElement CompanyElement { get; set; }
        public virtual ICollection<ContractPlayer> ContractPlayers { get; set; }
        public virtual ICollection<ContractPricing> ContractPricings { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<Contract> Contracts1 { get; set; }
        public virtual Contract Contract1 { get; set; }
        public virtual ServiceProvider ServiceProvider1 { get; set; }
        public virtual ICollection<ContractServicePackage> ContractServicePackages { get; set; }
        public virtual ICollection<ContractService> ContractServices { get; set; }
        public virtual ICollection<DisbursementAccountSubscriber> DisbursementAccountSubscribers { get; set; }
        public virtual ICollection<FeeCalculation> FeeCalculations { get; set; }
        public virtual ICollection<Tax> Taxes { get; set; }
        public virtual ICollection<TransferProfile> TransferProfiles { get; set; }
    }
}
