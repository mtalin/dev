using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DistributableAmountsTemp
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Year { get; set; }
        public Nullable<decimal> AVC { get; set; }
        public Nullable<decimal> AVS { get; set; }
        public Nullable<decimal> AVA { get; set; }
        public Nullable<decimal> MPC { get; set; }
        public Nullable<decimal> MIR { get; set; }
        public Nullable<decimal> UI0 { get; set; }
        public Nullable<decimal> ACF { get; set; }
        public Nullable<decimal> QD { get; set; }
        public Nullable<decimal> RCV { get; set; }
        public Nullable<decimal> UI { get; set; }
        public Nullable<decimal> CF { get; set; }
        public Nullable<int> Scenario { get; set; }
        public Nullable<bool> Final { get; set; }
        public byte[] FoundationStamp { get; set; }
        public int RegistryStamp { get; set; }
        public int RegistryStamp2 { get; set; }
    }
}
