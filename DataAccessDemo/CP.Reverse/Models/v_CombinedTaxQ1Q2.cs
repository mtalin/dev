using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class v_CombinedTaxQ1Q2
    {
        public int ID { get; set; }
        public int Q1ID { get; set; }
        public int Q2ID { get; set; }
        public int Q1 { get; set; }
        public int Q2 { get; set; }
        public int Quarter { get; set; }
        public int Step { get; set; }
        public int Foundation { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public System.DateTime DtRepaymentTax { get; set; }
        public int Year { get; set; }
        public int Method { get; set; }
        public System.DateTime dtStartPeriod { get; set; }
        public Nullable<int> Tax { get; set; }
    }
}
