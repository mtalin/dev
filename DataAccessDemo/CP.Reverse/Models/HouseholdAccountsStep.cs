using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class HouseholdAccountsStep
    {
        public int ID { get; set; }
        public int HouseholdAccount { get; set; }
        public int Step { get; set; }
        public Nullable<int> Note { get; set; }
        public Nullable<int> Message { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Event { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual HouseholdAccount HouseholdAccount1 { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
