using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class BAConfig
    {
        public string Password { get; set; }
        public Nullable<System.DateTime> LastDateChangePassword { get; set; }
        public string PreviousPasswords { get; set; }
        public int CurrentConnections { get; set; }
        public int PasswordIsChanging { get; set; }
        public int PasswordUseCount { get; set; }
    }
}
