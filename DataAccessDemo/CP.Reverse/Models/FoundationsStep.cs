using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FoundationsStep
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public int Step { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual User User { get; set; }
    }
}
