using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TransferGroup
    {
        public TransferGroup()
        {
            this.PendingTransacts = new List<PendingTransact>();
        }

        public int ID { get; set; }
        public Nullable<System.DateTime> DtSent { get; set; }
        public int Step { get; set; }
        public int Foundation { get; set; }
        public int FoundationAccount { get; set; }
        public Nullable<int> FinancialPartnerAccount { get; set; }
        public bool ACHStatus { get; set; }
        public Nullable<int> Signer { get; set; }
        public Nullable<int> TransferProfile { get; set; }
        public Nullable<int> Document { get; set; }
        public bool IsModified { get; set; }
        public bool IsRevised { get; set; }
        public Nullable<System.DateTime> DtResend { get; set; }
        public bool IsSpecialHandling { get; set; }
        public bool IsUploaded { get; set; }
        public virtual Document1 Document1 { get; set; }
        public virtual FoundationAccount FoundationAccount1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<PendingTransact> PendingTransacts { get; set; }
        public virtual Person Person { get; set; }
        public virtual TransferProfile TransferProfile1 { get; set; }
    }
}
