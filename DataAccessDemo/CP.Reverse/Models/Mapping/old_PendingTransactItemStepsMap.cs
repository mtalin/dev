using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class old_PendingTransactItemStepsMap : EntityTypeConfiguration<old_PendingTransactItemSteps>
    {
        public old_PendingTransactItemStepsMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("old_PendingTransactItemSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PendingTransactItem).HasColumnName("PendingTransactItem");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.old_PendingTransactItems)
                .WithMany(t => t.old_PendingTransactItemSteps)
                .HasForeignKey(d => d.PendingTransactItem);

        }
    }
}
