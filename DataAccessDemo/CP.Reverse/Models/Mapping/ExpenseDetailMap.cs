using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ExpenseDetailMap : EntityTypeConfiguration<ExpenseDetail>
    {
        public ExpenseDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.AssetsAmount, t.Step, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CategoryName)
                .HasMaxLength(150);

            this.Property(t => t.TypeName)
                .HasMaxLength(150);

            this.Property(t => t.PayeeName)
                .HasMaxLength(200);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.AssetsAmount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SortPayeeName)
                .HasMaxLength(200);

            this.Property(t => t.InvoiceNumber)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ExpenseDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.CategoryName).HasColumnName("CategoryName");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.Officer).HasColumnName("Officer");
            this.Property(t => t.ContactInfo).HasColumnName("ContactInfo");
            this.Property(t => t.PayeeProfile).HasColumnName("PayeeProfile");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.PayeeName).HasColumnName("PayeeName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.AssetsAmount).HasColumnName("AssetsAmount");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
            this.Property(t => t.PendingTransact).HasColumnName("PendingTransact");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.SortPayeeName).HasColumnName("SortPayeeName");
            this.Property(t => t.InvoiceNumber).HasColumnName("InvoiceNumber");
            this.Property(t => t.InvoiceDocument).HasColumnName("InvoiceDocument");
            this.Property(t => t.Template).HasColumnName("Template");
            this.Property(t => t.TimeSchedule).HasColumnName("TimeSchedule");
        }
    }
}
