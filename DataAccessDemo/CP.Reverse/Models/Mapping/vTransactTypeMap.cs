using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vTransactTypeMap : EntityTypeConfiguration<vTransactType>
    {
        public vTransactTypeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.Name });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Text)
                .HasMaxLength(300);

            this.Property(t => t.Abbrev)
                .HasMaxLength(50);

            this.Property(t => t.Flags)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("vTransactTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.Abbrev).HasColumnName("Abbrev");
            this.Property(t => t.Flags).HasColumnName("Flags");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
        }
    }
}
