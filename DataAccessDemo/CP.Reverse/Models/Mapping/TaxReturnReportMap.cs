using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReturnReportMap : EntityTypeConfiguration<TaxReturnReport>
    {
        public TaxReturnReportMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ReportSnapshot)
                .IsRequired();

            this.Property(t => t.ReportFile)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TaxReturnReports");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.TaxReturn).HasColumnName("TaxReturn");
            this.Property(t => t.CustomReport).HasColumnName("CustomReport");
            this.Property(t => t.ReportSnapshot).HasColumnName("ReportSnapshot");
            this.Property(t => t.ReportFile).HasColumnName("ReportFile");
            this.Property(t => t.dtUpdated).HasColumnName("dtUpdated");

            // Relationships
            this.HasRequired(t => t.CustomReport1)
                .WithMany(t => t.TaxReturnReports)
                .HasForeignKey(d => d.CustomReport);
            this.HasRequired(t => t.TaxReturn1)
                .WithMany(t => t.TaxReturnReports)
                .HasForeignKey(d => d.TaxReturn);

        }
    }
}
