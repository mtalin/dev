using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RegistryRules990Map : EntityTypeConfiguration<RegistryRules990>
    {
        public RegistryRules990Map()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Line990)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("RegistryRules990");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Part).HasColumnName("Part");
            this.Property(t => t.TransactType).HasColumnName("TransactType");
            this.Property(t => t.AssetType).HasColumnName("AssetType");
            this.Property(t => t.LineId).HasColumnName("LineId");
            this.Property(t => t.ColumnA).HasColumnName("ColumnA");
            this.Property(t => t.ColumnB).HasColumnName("ColumnB");
            this.Property(t => t.ColumnC).HasColumnName("ColumnC");
            this.Property(t => t.ColumnD).HasColumnName("ColumnD");
            this.Property(t => t.ColumnE).HasColumnName("ColumnE");
            this.Property(t => t.ColumnF).HasColumnName("ColumnF");
            this.Property(t => t.Line990).HasColumnName("Line990");
        }
    }
}
