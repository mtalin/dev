using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class qd_YNQuestions_Map : EntityTypeConfiguration<qd_YNQuestions_>
    {
        public qd_YNQuestions_Map()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("qd_YNQuestions_");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.dtCreated).HasColumnName("dtCreated");
            this.Property(t => t.Questionnaire).HasColumnName("Questionnaire");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.Question).HasColumnName("Question");
            this.Property(t => t.Checked).HasColumnName("Checked");
        }
    }
}
