using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GrantDetailMap : EntityTypeConfiguration<GrantDetail>
    {
        public GrantDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TargetedDescription)
                .HasMaxLength(1000);

            this.Property(t => t.Purpose)
                .HasMaxLength(50);

            this.Property(t => t.PurposeDescription)
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeText1)
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeText2)
                .HasMaxLength(1000);

            this.Property(t => t.NoteForEmail)
                .HasMaxLength(1000);

            this.Property(t => t.ChOrgName)
                .HasMaxLength(200);

            this.Property(t => t.ChInCareOf)
                .HasMaxLength(100);

            this.Property(t => t.ChAddress)
                .HasMaxLength(100);

            this.Property(t => t.ChCity)
                .HasMaxLength(30);

            this.Property(t => t.ChState)
                .HasMaxLength(30);

            this.Property(t => t.ChZIP)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.ConfirmDesc)
                .HasMaxLength(50);

            this.Property(t => t.ProcessInstruction)
                .HasMaxLength(1000);

            this.Property(t => t.CheckMemo)
                .HasMaxLength(200);

            this.Property(t => t.ChAddress2)
                .HasMaxLength(100);

            this.Property(t => t.GrantReason)
                .HasMaxLength(4000);

            this.Property(t => t.ChCountry)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.CheckModReason)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("GrantDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Anonymous).HasColumnName("Anonymous");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.TargetedDescription).HasColumnName("TargetedDescription");
            this.Property(t => t.GrantLetter).HasColumnName("GrantLetter");
            this.Property(t => t.GrantLetterData).HasColumnName("GrantLetterData");
            this.Property(t => t.Priority).HasColumnName("Priority");
            this.Property(t => t.SpecialUserNote).HasColumnName("SpecialUserNote");
            this.Property(t => t.ReviewedByAdmin).HasColumnName("ReviewedByAdmin");
            this.Property(t => t.DonationRequest).HasColumnName("DonationRequest");
            this.Property(t => t.Certificate).HasColumnName("Certificate");
            this.Property(t => t.RecipientType).HasColumnName("RecipientType");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.HouseholdAccount).HasColumnName("HouseholdAccount");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.PurposeDescription).HasColumnName("PurposeDescription");
            this.Property(t => t.SpecPurposeType).HasColumnName("SpecPurposeType");
            this.Property(t => t.SpecPurposeText1).HasColumnName("SpecPurposeText1");
            this.Property(t => t.SpecPurposeText2).HasColumnName("SpecPurposeText2");
            this.Property(t => t.IsSpecialDelivery).HasColumnName("IsSpecialDelivery");
            this.Property(t => t.NoteForEmail).HasColumnName("NoteForEmail");
            this.Property(t => t.ChOrgName).HasColumnName("ChOrgName");
            this.Property(t => t.ChInCareOf).HasColumnName("ChInCareOf");
            this.Property(t => t.ChAddress).HasColumnName("ChAddress");
            this.Property(t => t.ChCity).HasColumnName("ChCity");
            this.Property(t => t.ChState).HasColumnName("ChState");
            this.Property(t => t.ChZIP).HasColumnName("ChZIP");
            this.Property(t => t.ConfirmGuideStar).HasColumnName("ConfirmGuideStar");
            this.Property(t => t.ConfirmIRSbulletins).HasColumnName("ConfirmIRSbulletins");
            this.Property(t => t.ConfirmIRSwebsite).HasColumnName("ConfirmIRSwebsite");
            this.Property(t => t.ConfirmDesc).HasColumnName("ConfirmDesc");
            this.Property(t => t.PermanentChanges).HasColumnName("PermanentChanges");
            this.Property(t => t.GrantLetterApproved).HasColumnName("GrantLetterApproved");
            this.Property(t => t.GrantType).HasColumnName("GrantType");
            this.Property(t => t.NumPayments).HasColumnName("NumPayments");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
            this.Property(t => t.PaymentNumber).HasColumnName("PaymentNumber");
            this.Property(t => t.ProcessInstruction).HasColumnName("ProcessInstruction");
            this.Property(t => t.Template).HasColumnName("Template");
            this.Property(t => t.CheckMemo).HasColumnName("CheckMemo");
            this.Property(t => t.ChAddress2).HasColumnName("ChAddress2");
            this.Property(t => t.GrantReason).HasColumnName("GrantReason");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.GmxRef).HasColumnName("GmxRef");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.ChCountry).HasColumnName("ChCountry");
            this.Property(t => t.CheckModReason).HasColumnName("CheckModReason");
            this.Property(t => t.DtBridger).HasColumnName("DtBridger");

            // Relationships
            this.HasOptional(t => t.Document)
                .WithMany(t => t.GrantDetails)
                .HasForeignKey(d => d.GrantLetter);
            this.HasOptional(t => t.DonationCertificate)
                .WithMany(t => t.GrantDetails)
                .HasForeignKey(d => d.Certificate);
            this.HasOptional(t => t.DonationRequest1)
                .WithMany(t => t.GrantDetails)
                .HasForeignKey(d => d.DonationRequest);
            this.HasOptional(t => t.FoundationProject1)
                .WithMany(t => t.GrantDetails)
                .HasForeignKey(d => d.FoundationProject);
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.GrantDetails)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.GrantDetail1)
                .WithMany(t => t.GrantDetails1)
                .HasForeignKey(d => d.Template);
            this.HasOptional(t => t.HouseholdAccount1)
                .WithMany(t => t.GrantDetails)
                .HasForeignKey(d => d.HouseholdAccount);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.GrantDetails)
                .HasForeignKey(d => d.Author);

        }
    }
}
