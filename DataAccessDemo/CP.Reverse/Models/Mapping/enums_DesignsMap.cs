using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class enums_DesignsMap : EntityTypeConfiguration<enums_Designs>
    {
        public enums_DesignsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ItemID, t.Name });

            // Properties
            this.Property(t => t.ItemID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Abbrev)
                .HasMaxLength(50);

            this.Property(t => t.Flags)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("enums_Designs");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Abbrev).HasColumnName("Abbrev");
            this.Property(t => t.Flags).HasColumnName("Flags");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
        }
    }
}
