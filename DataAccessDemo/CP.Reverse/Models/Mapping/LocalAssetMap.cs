using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class LocalAssetMap : EntityTypeConfiguration<LocalAsset>
    {
        public LocalAssetMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("LocalAssets");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.InvPercent).HasColumnName("InvPercent");
            this.Property(t => t.ChrPercent).HasColumnName("ChrPercent");

            // Relationships
            this.HasRequired(t => t.Asset)
                .WithOptional(t => t.LocalAsset);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.LocalAssets)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
