using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RecipientMap : EntityTypeConfiguration<Recipient>
    {
        public RecipientMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.CustomEmail)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Recipients");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.CustomEmail).HasColumnName("CustomEmail");
            this.Property(t => t.Person).HasColumnName("Person");

            // Relationships
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.Recipients)
                .HasForeignKey(d => d.Person);

        }
    }
}
