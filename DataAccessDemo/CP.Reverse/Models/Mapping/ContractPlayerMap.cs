using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ContractPlayerMap : EntityTypeConfiguration<ContractPlayer>
    {
        public ContractPlayerMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContractPlayers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.CompanyElement).HasColumnName("CompanyElement");
            this.Property(t => t.RoleInContract).HasColumnName("RoleInContract");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");

            // Relationships
            this.HasRequired(t => t.CompanyElement1)
                .WithMany(t => t.ContractPlayers)
                .HasForeignKey(d => d.CompanyElement);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ContractPlayers)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Contract1)
                .WithMany(t => t.ContractPlayers)
                .HasForeignKey(d => d.Contract);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.ContractPlayers1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasOptional(t => t.ObjectsPermission)
                .WithMany(t => t.ContractPlayers)
                .HasForeignKey(d => d.ObjectPermissions);

        }
    }
}
