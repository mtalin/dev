using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CMSContactMap : EntityTypeConfiguration<CMSContact>
    {
        public CMSContactMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(200);

            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.MiddleInitial)
                .HasMaxLength(15);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.BusinessPhone)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.BusinessFax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.PostalAddress)
                .HasMaxLength(200);

            this.Property(t => t.City)
                .IsFixedLength()
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZIP)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.Country)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("CMSContacts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.NamePrefix).HasColumnName("NamePrefix");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.MiddleInitial).HasColumnName("MiddleInitial");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.BusinessPhone).HasColumnName("BusinessPhone");
            this.Property(t => t.BusinessFax).HasColumnName("BusinessFax");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZIP).HasColumnName("ZIP");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Role).HasColumnName("Role");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.PressRelease).HasColumnName("PressRelease");
            this.Property(t => t.Country).HasColumnName("Country");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CMSContacts)
                .HasForeignKey(d => d.Author);

        }
    }
}
