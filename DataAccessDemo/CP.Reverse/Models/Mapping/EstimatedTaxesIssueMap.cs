using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EstimatedTaxesIssueMap : EntityTypeConfiguration<EstimatedTaxesIssue>
    {
        public EstimatedTaxesIssueMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("EstimatedTaxesIssues");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.EstimatedTax).HasColumnName("EstimatedTax");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.IssueType).HasColumnName("IssueType");
            this.Property(t => t.DtOpen).HasColumnName("DtOpen");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.ClosedBy).HasColumnName("ClosedBy");
            this.Property(t => t.dtLastRecalc).HasColumnName("dtLastRecalc");

            // Relationships
            this.HasOptional(t => t.EstimatedTax1)
                .WithMany(t => t.EstimatedTaxesIssues)
                .HasForeignKey(d => d.EstimatedTax);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.EstimatedTaxesIssues)
                .HasForeignKey(d => d.Author);

        }
    }
}
