using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReturnIssueMap : EntityTypeConfiguration<TaxReturnIssue>
    {
        public TaxReturnIssueMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TaxReturnIssues");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.TaxReturn).HasColumnName("TaxReturn");
            this.Property(t => t.IssueType).HasColumnName("IssueType");
            this.Property(t => t.Source).HasColumnName("Source");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.DtOpen).HasColumnName("DtOpen");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.ClosedBy).HasColumnName("ClosedBy");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.TaxReturnIssues)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.TaxReturnIssues1)
                .HasForeignKey(d => d.ClosedBy);
            this.HasRequired(t => t.TaxReturn1)
                .WithMany(t => t.TaxReturnIssues)
                .HasForeignKey(d => d.TaxReturn);

        }
    }
}
