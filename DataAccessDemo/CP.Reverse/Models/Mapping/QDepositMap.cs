using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QDepositMap : EntityTypeConfiguration<QDeposit>
    {
        public QDepositMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Security)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Type)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("QDeposits");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.Parent).HasColumnName("Parent");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Donor).HasColumnName("Donor");
            this.Property(t => t.Security).HasColumnName("Security");
            this.Property(t => t.Shares).HasColumnName("Shares");
            this.Property(t => t.Price).HasColumnName("Price");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Restricted).HasColumnName("Restricted");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.HighPrice).HasColumnName("HighPrice");
            this.Property(t => t.LowPrice).HasColumnName("LowPrice");
            this.Property(t => t.AveragePrice).HasColumnName("AveragePrice");
            this.Property(t => t.QAdmin).HasColumnName("QAdmin");
            this.Property(t => t.ReadOnly).HasColumnName("ReadOnly");
            this.Property(t => t.Allocated).HasColumnName("Allocated");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.QDeposits)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.User)
                .WithMany(t => t.QDeposits)
                .HasForeignKey(d => d.Donor);

        }
    }
}
