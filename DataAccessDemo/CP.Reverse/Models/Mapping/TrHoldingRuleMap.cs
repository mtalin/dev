using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TrHoldingRuleMap : EntityTypeConfiguration<TrHoldingRule>
    {
        public TrHoldingRuleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.RuleXX)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.RuleUSD)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.RuleMF)
                .HasMaxLength(200);

            this.Property(t => t.RuleMU)
                .HasMaxLength(200);

            this.Property(t => t.RuleFI)
                .HasMaxLength(200);

            this.Property(t => t.RuleAA)
                .HasMaxLength(200);

            this.Property(t => t.RuleCR)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TrHoldingRules");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.RuleXX).HasColumnName("RuleXX");
            this.Property(t => t.RuleUSD).HasColumnName("RuleUSD");
            this.Property(t => t.RuleMF).HasColumnName("RuleMF");
            this.Property(t => t.RuleMU).HasColumnName("RuleMU");
            this.Property(t => t.RuleFI).HasColumnName("RuleFI");
            this.Property(t => t.RuleAA).HasColumnName("RuleAA");
            this.Property(t => t.RuleCR).HasColumnName("RuleCR");
        }
    }
}
