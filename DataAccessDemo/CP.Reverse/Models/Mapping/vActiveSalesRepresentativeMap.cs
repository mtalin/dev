using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vActiveSalesRepresentativeMap : EntityTypeConfiguration<vActiveSalesRepresentative>
    {
        public vActiveSalesRepresentativeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.StaffingPolicy, t.HeadOfDepartment, t.StructuredCompany });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StaffingPolicy)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vActiveSalesRepresentative");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StaffingPolicy).HasColumnName("StaffingPolicy");
            this.Property(t => t.Department).HasColumnName("Department");
            this.Property(t => t.HeadOfDepartment).HasColumnName("HeadOfDepartment");
            this.Property(t => t.SubordinateTo).HasColumnName("SubordinateTo");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.DtNominated).HasColumnName("DtNominated");
            this.Property(t => t.DtAccepted).HasColumnName("DtAccepted");
            this.Property(t => t.DtRemoved).HasColumnName("DtRemoved");
            this.Property(t => t.RemovedBy).HasColumnName("RemovedBy");
            this.Property(t => t.DtExpired).HasColumnName("DtExpired");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.Note).HasColumnName("Note");
        }
    }
}
