using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ShellCorporationsStepMap : EntityTypeConfiguration<ShellCorporationsStep>
    {
        public ShellCorporationsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ShellCorporationsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ShellCorporation).HasColumnName("ShellCorporation");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Event).HasColumnName("Event");

            // Relationships
            this.HasOptional(t => t.Event1)
                .WithMany(t => t.ShellCorporationsSteps)
                .HasForeignKey(d => d.Event);
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.ShellCorporationsSteps)
                .HasForeignKey(d => d.Message);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.ShellCorporationsSteps)
                .HasForeignKey(d => d.Note);
            this.HasOptional(t => t.ShellCorporation1)
                .WithMany(t => t.ShellCorporationsSteps)
                .HasForeignKey(d => d.ShellCorporation);
            this.HasRequired(t => t.User)
                .WithMany(t => t.ShellCorporationsSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
