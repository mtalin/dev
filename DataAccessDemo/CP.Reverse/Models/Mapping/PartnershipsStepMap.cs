using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PartnershipsStepMap : EntityTypeConfiguration<PartnershipsStep>
    {
        public PartnershipsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PartnershipsSteps");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Partnership).HasColumnName("Partnership");
            this.Property(t => t.Step).HasColumnName("Step");

            // Relationships
            this.HasRequired(t => t.Partnership1)
                .WithMany(t => t.PartnershipsSteps)
                .HasForeignKey(d => d.Partnership);
            this.HasRequired(t => t.User)
                .WithMany(t => t.PartnershipsSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
