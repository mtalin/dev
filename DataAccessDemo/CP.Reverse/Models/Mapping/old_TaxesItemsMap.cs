using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class old_TaxesItemsMap : EntityTypeConfiguration<old_TaxesItems>
    {
        public old_TaxesItemsMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("old_TaxesItems");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Tax).HasColumnName("Tax");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Amount2).HasColumnName("Amount2");

            // Relationships
            this.HasRequired(t => t.Tax1)
                .WithMany(t => t.old_TaxesItems)
                .HasForeignKey(d => d.Tax);

        }
    }
}
