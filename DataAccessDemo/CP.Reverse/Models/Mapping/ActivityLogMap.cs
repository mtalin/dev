using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ActivityLogMap : EntityTypeConfiguration<ActivityLog>
    {
        public ActivityLogMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.UserAgent)
                .HasMaxLength(200);

            this.Property(t => t.SessionId)
                .IsFixedLength()
                .HasMaxLength(24);

            // Table & Column Mappings
            this.ToTable("ActivityLogs");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.UserAgent).HasColumnName("UserAgent");
            this.Property(t => t.SessionId).HasColumnName("SessionId");

            // Relationships
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.ActivityLogs)
                .HasForeignKey(d => d.Person);

        }
    }
}
