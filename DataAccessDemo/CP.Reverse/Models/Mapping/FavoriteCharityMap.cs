using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FavoriteCharityMap : EntityTypeConfiguration<FavoriteCharity>
    {
        public FavoriteCharityMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("FavoriteCharities");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.IsPublic).HasColumnName("IsPublic");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.old_Nominee).HasColumnName("old_Nominee");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.FavoriteCharities)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.FavoriteCharities)
                .HasForeignKey(d => d.Person);

        }
    }
}
