using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ContractServicePackageMap : EntityTypeConfiguration<ContractServicePackage>
    {
        public ContractServicePackageMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContractServicePackages");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.ServicePackage).HasColumnName("ServicePackage");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.Contract1)
                .WithMany(t => t.ContractServicePackages)
                .HasForeignKey(d => d.Contract);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ContractServicePackages)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.ContractServicePackages1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasRequired(t => t.ServicePackage1)
                .WithMany(t => t.ContractServicePackages)
                .HasForeignKey(d => d.ServicePackage);

        }
    }
}
