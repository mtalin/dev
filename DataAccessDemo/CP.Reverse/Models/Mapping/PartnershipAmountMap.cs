using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PartnershipAmountMap : EntityTypeConfiguration<PartnershipAmount>
    {
        public PartnershipAmountMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PartnershipAmounts");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Line).HasColumnName("Line");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.UBI).HasColumnName("UBI");
            this.Property(t => t.BlockedUBI).HasColumnName("BlockedUBI");
            this.Property(t => t.Partnership).HasColumnName("Partnership");

            // Relationships
            this.HasRequired(t => t.Partnership1)
                .WithMany(t => t.PartnershipAmounts)
                .HasForeignKey(d => d.Partnership);
            this.HasRequired(t => t.User)
                .WithMany(t => t.PartnershipAmounts)
                .HasForeignKey(d => d.Author);

        }
    }
}
