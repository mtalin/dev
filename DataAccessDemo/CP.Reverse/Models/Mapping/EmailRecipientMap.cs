using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EmailRecipientMap : EntityTypeConfiguration<EmailRecipient>
    {
        public EmailRecipientMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("EmailRecipients");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.EmailTemplate).HasColumnName("EmailTemplate");
            this.Property(t => t.Recipient).HasColumnName("Recipient");
            this.Property(t => t.AddressGroup).HasColumnName("AddressGroup");

            // Relationships
            this.HasRequired(t => t.EmailTemplate1)
                .WithMany(t => t.EmailRecipients)
                .HasForeignKey(d => d.EmailTemplate);
            this.HasOptional(t => t.Recipient1)
                .WithMany(t => t.EmailRecipients)
                .HasForeignKey(d => d.Recipient);

        }
    }
}
