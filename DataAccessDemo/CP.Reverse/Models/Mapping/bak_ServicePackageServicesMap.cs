using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class bak_ServicePackageServicesMap : EntityTypeConfiguration<bak_ServicePackageServices>
    {
        public bak_ServicePackageServicesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.ServicePackage, t.Service, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ServicePackage)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Service)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("bak_ServicePackageServices");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ServicePackage).HasColumnName("ServicePackage");
            this.Property(t => t.Service).HasColumnName("Service");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
        }
    }
}
