using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QuestSummaries_Map : EntityTypeConfiguration<QuestSummaries_>
    {
        public QuestSummaries_Map()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("QuestSummaries_");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.dtCreated).HasColumnName("dtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Questionnaire).HasColumnName("Questionnaire");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.HasIssues).HasColumnName("HasIssues");
            this.Property(t => t.Resolved).HasColumnName("Resolved");
            this.Property(t => t.Enabled).HasColumnName("Enabled");
            this.Property(t => t.ResolvedBy).HasColumnName("ResolvedBy");
            this.Property(t => t.dtResolved).HasColumnName("dtResolved");
            this.Property(t => t.fsAddrIssuesBy).HasColumnName("fsAddrIssuesBy");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.QuestSummaries_)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.QuestSummaries_1)
                .HasForeignKey(d => d.fsAddrIssuesBy);
            this.HasOptional(t => t.Person2)
                .WithMany(t => t.QuestSummaries_2)
                .HasForeignKey(d => d.ResolvedBy);

        }
    }
}
