using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CustomReportMap : EntityTypeConfiguration<CustomReport>
    {
        public CustomReportMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(400);

            this.Property(t => t.Data)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("CustomReports");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Data).HasColumnName("Data");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.AdminOnly).HasColumnName("AdminOnly");
            this.Property(t => t.ExtUserOnly).HasColumnName("ExtUserOnly");
            this.Property(t => t.ExclusionMode).HasColumnName("ExclusionMode");
            this.Property(t => t.MatrixFormat).HasColumnName("MatrixFormat");

            // Relationships
            this.HasMany(t => t.Foundations)
                .WithMany(t => t.CustomReports)
                .Map(m =>
                    {
                        m.ToTable("ReportsExcludedFoundations");
                        m.MapLeftKey("CustomReport");
                        m.MapRightKey("Foundation");
                    });

            this.HasRequired(t => t.CustomReportCategory)
                .WithMany(t => t.CustomReports)
                .HasForeignKey(d => d.Category);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CustomReports)
                .HasForeignKey(d => d.Author);

        }
    }
}
