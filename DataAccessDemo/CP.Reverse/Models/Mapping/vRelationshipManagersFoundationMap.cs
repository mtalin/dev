using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vRelationshipManagersFoundationMap : EntityTypeConfiguration<vRelationshipManagersFoundation>
    {
        public vRelationshipManagersFoundationMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FirstName, t.LastName, t.StructuredCompany, t.CompanyPosition, t.Foundation });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StructuredCompanyName)
                .HasMaxLength(500);

            this.Property(t => t.Position)
                .HasMaxLength(500);

            this.Property(t => t.CompanyPosition)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vRelationshipManagersFoundations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.StructuredCompanyName).HasColumnName("StructuredCompanyName");
            this.Property(t => t.Position).HasColumnName("Position");
            this.Property(t => t.CompanyPosition).HasColumnName("CompanyPosition");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
        }
    }
}
