using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EnumsCategoryMap : EntityTypeConfiguration<EnumsCategory>
    {
        public EnumsCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(16);

            this.Property(t => t.Path)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("EnumsCategories");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Parent).HasColumnName("Parent");
            this.Property(t => t.Path).HasColumnName("Path");

            // Relationships
            this.HasOptional(t => t.EnumsCategory1)
                .WithMany(t => t.EnumsCategories1)
                .HasForeignKey(d => d.Parent);

        }
    }
}
