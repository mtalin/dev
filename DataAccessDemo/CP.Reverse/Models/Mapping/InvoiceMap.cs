using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class InvoiceMap : EntityTypeConfiguration<Invoice>
    {
        public InvoiceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Invoices");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");

            // Relationships
            this.HasOptional(t => t.Company1)
                .WithMany(t => t.Invoices)
                .HasForeignKey(d => d.Company);
            this.HasOptional(t => t.StructuredCompany1)
                .WithMany(t => t.Invoices)
                .HasForeignKey(d => d.StructuredCompany);

        }
    }
}
