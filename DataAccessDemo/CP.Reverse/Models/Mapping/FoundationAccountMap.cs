using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FoundationAccountMap : EntityTypeConfiguration<FoundationAccount>
    {
        public FoundationAccountMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Account)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.BankName)
                .HasMaxLength(50);

            this.Property(t => t.PortfolioCode)
                .HasMaxLength(20);

            this.Property(t => t.AccountName)
                .HasMaxLength(255);

            this.Property(t => t.URL)
                .HasMaxLength(500);

            this.Property(t => t.RoutingNumber)
                .HasMaxLength(22);

            this.Property(t => t.BatchFilerID)
                .HasMaxLength(50);

            this.Property(t => t.MasterInquiryPIN)
                .HasMaxLength(50);

            this.Property(t => t.Note)
                .HasMaxLength(3000);

            // Table & Column Mappings
            this.ToTable("FoundationAccounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Account).HasColumnName("Account");
            this.Property(t => t.AccountType).HasColumnName("AccountType");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DeleteAuthor).HasColumnName("DeleteAuthor");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.BankName).HasColumnName("BankName");
            this.Property(t => t.BankAddress).HasColumnName("BankAddress");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DtReconciled).HasColumnName("DtReconciled");
            this.Property(t => t.Access).HasColumnName("Access");
            this.Property(t => t.PortfolioCode).HasColumnName("PortfolioCode");
            this.Property(t => t.OnlineAccess).HasColumnName("OnlineAccess");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");
            this.Property(t => t.AccountName).HasColumnName("AccountName");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.Department).HasColumnName("Department");
            this.Property(t => t.Statements).HasColumnName("Statements");
            this.Property(t => t.Frequency).HasColumnName("Frequency");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.CheckingAccount).HasColumnName("CheckingAccount");
            this.Property(t => t.RoutingNumber).HasColumnName("RoutingNumber");
            this.Property(t => t.EFTPSAccountType).HasColumnName("EFTPSAccountType");
            this.Property(t => t.BatchFilerID).HasColumnName("BatchFilerID");
            this.Property(t => t.MasterInquiryPIN).HasColumnName("MasterInquiryPIN");
            this.Property(t => t.PurposeType).HasColumnName("PurposeType");
            this.Property(t => t.DtOpened).HasColumnName("DtOpened");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.InvestmentCompany).HasColumnName("InvestmentCompany");
            this.Property(t => t.InvestmentCompanyDepartment).HasColumnName("InvestmentCompanyDepartment");
            this.Property(t => t.Division).HasColumnName("Division");
            this.Property(t => t.YearEnd).HasColumnName("YearEnd");

            // Relationships
            this.HasOptional(t => t.CheckingAccount1)
                .WithMany(t => t.FoundationAccounts)
                .HasForeignKey(d => d.CheckingAccount);
            this.HasOptional(t => t.Department1)
                .WithMany(t => t.FoundationAccounts)
                .HasForeignKey(d => d.Department);
            this.HasOptional(t => t.Department2)
                .WithMany(t => t.FoundationAccounts1)
                .HasForeignKey(d => d.InvestmentCompanyDepartment);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.FoundationAccounts)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.PostalAddress)
                .WithMany(t => t.FoundationAccounts)
                .HasForeignKey(d => d.BankAddress);
            this.HasRequired(t => t.ServiceProvider1)
                .WithMany(t => t.FoundationAccounts)
                .HasForeignKey(d => d.ServiceProvider);
            this.HasOptional(t => t.ServiceProvider2)
                .WithMany(t => t.FoundationAccounts1)
                .HasForeignKey(d => d.InvestmentCompany);
            this.HasRequired(t => t.User)
                .WithMany(t => t.FoundationAccounts)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.User1)
                .WithMany(t => t.FoundationAccounts1)
                .HasForeignKey(d => d.DeleteAuthor);

        }
    }
}
