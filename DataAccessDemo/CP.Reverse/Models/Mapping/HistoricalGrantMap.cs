using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class HistoricalGrantMap : EntityTypeConfiguration<HistoricalGrant>
    {
        public HistoricalGrantMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("HistoricalGrants");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.GrantDetails).HasColumnName("GrantDetails");
            this.Property(t => t.GrantDate).HasColumnName("GrantDate");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.GrantDetail)
                .WithMany(t => t.HistoricalGrants)
                .HasForeignKey(d => d.GrantDetails);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.HistoricalGrants)
                .HasForeignKey(d => d.Author);

        }
    }
}
