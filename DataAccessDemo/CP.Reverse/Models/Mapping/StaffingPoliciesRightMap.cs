using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class StaffingPoliciesRightMap : EntityTypeConfiguration<StaffingPoliciesRight>
    {
        public StaffingPoliciesRightMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Permissions)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.AppliedPermissions)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.InheritedPermissions)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("StaffingPoliciesRights", "rpt");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.SystemPosition).HasColumnName("SystemPosition");
            this.Property(t => t.StaffingPolicyTemplate).HasColumnName("StaffingPolicyTemplate");
            this.Property(t => t.Permissions).HasColumnName("Permissions");
            this.Property(t => t.AppliedPermissions).HasColumnName("AppliedPermissions");
            this.Property(t => t.InheritedPermissions).HasColumnName("InheritedPermissions");
        }
    }
}
