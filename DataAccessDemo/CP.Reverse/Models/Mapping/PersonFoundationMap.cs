using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PersonFoundationMap : EntityTypeConfiguration<PersonFoundation>
    {
        public PersonFoundationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Rights)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("PersonFoundations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Role).HasColumnName("Role");
            this.Property(t => t.Rights).HasColumnName("Rights");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.PersonFoundations)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.PersonRole)
                .WithMany(t => t.PersonFoundations)
                .HasForeignKey(d => d.Role);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.PersonFoundations)
                .HasForeignKey(d => d.Person);

        }
    }
}
