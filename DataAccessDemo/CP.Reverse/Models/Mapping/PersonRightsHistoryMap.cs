using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PersonRightsHistoryMap : EntityTypeConfiguration<PersonRightsHistory>
    {
        public PersonRightsHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Rights)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(255);

            this.Property(t => t.OldRights)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("PersonRightsHistory");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.PersonFoundation).HasColumnName("PersonFoundation");
            this.Property(t => t.Role).HasColumnName("Role");
            this.Property(t => t.OldRole).HasColumnName("OldRole");
            this.Property(t => t.Rights).HasColumnName("Rights");
            this.Property(t => t.OldRights).HasColumnName("OldRights");

            // Relationships
            this.HasRequired(t => t.PersonFoundation1)
                .WithMany(t => t.PersonRightsHistories)
                .HasForeignKey(d => d.PersonFoundation);
            this.HasRequired(t => t.PersonRole)
                .WithMany(t => t.PersonRightsHistories)
                .HasForeignKey(d => d.Role);
            this.HasOptional(t => t.PersonRole1)
                .WithMany(t => t.PersonRightsHistories1)
                .HasForeignKey(d => d.OldRole);
            this.HasRequired(t => t.User)
                .WithMany(t => t.PersonRightsHistories)
                .HasForeignKey(d => d.Author);

        }
    }
}
