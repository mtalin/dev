using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_FoundationAccountsMap : EntityTypeConfiguration<et_FoundationAccounts>
    {
        public et_FoundationAccountsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Number, t.IsPrimary, t.ServiceProvider, t.OnlineAccess });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DtCreated)
                .HasMaxLength(30);

            this.Property(t => t.DtReconciled)
                .HasMaxLength(30);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(255);

            this.Property(t => t.Number)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.IsPrimary)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ServiceProvider)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Division)
                .HasMaxLength(500);

            this.Property(t => t.PortfolioCode)
                .HasMaxLength(20);

            this.Property(t => t.DataAccess)
                .HasMaxLength(150);

            this.Property(t => t.OnlineAccessURL)
                .HasMaxLength(500);

            this.Property(t => t.Statements)
                .HasMaxLength(150);

            this.Property(t => t.Frequency)
                .HasMaxLength(150);

            this.Property(t => t.RoutingNumber)
                .HasMaxLength(22);

            this.Property(t => t.BatchFilerID)
                .HasMaxLength(50);

            this.Property(t => t.MasterInquiryPIN)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("et_FoundationAccounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtReconciled).HasColumnName("DtReconciled");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Number).HasColumnName("Number");
            this.Property(t => t.IsPrimary).HasColumnName("IsPrimary");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.Division).HasColumnName("Division");
            this.Property(t => t.PortfolioCode).HasColumnName("PortfolioCode");
            this.Property(t => t.DataAccess).HasColumnName("DataAccess");
            this.Property(t => t.OnlineAccess).HasColumnName("OnlineAccess");
            this.Property(t => t.OnlineAccessURL).HasColumnName("OnlineAccessURL");
            this.Property(t => t.Statements).HasColumnName("Statements");
            this.Property(t => t.Frequency).HasColumnName("Frequency");
            this.Property(t => t.RoutingNumber).HasColumnName("RoutingNumber");
            this.Property(t => t.BatchFilerID).HasColumnName("BatchFilerID");
            this.Property(t => t.MasterInquiryPIN).HasColumnName("MasterInquiryPIN");
            this.Property(t => t.NextCheckNo).HasColumnName("NextCheckNo");
        }
    }
}
