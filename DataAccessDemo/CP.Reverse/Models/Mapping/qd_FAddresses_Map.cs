using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class qd_FAddresses_Map : EntityTypeConfiguration<qd_FAddresses_>
    {
        public qd_FAddresses_Map()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(500);

            this.Property(t => t.CompanyName)
                .HasMaxLength(500);

            this.Property(t => t.Address1)
                .HasMaxLength(500);

            this.Property(t => t.Address2)
                .HasMaxLength(500);

            this.Property(t => t.City)
                .HasMaxLength(200);

            this.Property(t => t.State)
                .HasMaxLength(50);

            this.Property(t => t.Zip)
                .HasMaxLength(10);

            this.Property(t => t.Phone)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("qd_FAddresses_");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.dtCreated).HasColumnName("dtCreated");
            this.Property(t => t.Questionnaire).HasColumnName("Questionnaire");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.UseFsAddress).HasColumnName("UseFsAddress");
            this.Property(t => t.IsNew).HasColumnName("IsNew");
            this.Property(t => t.Updated).HasColumnName("Updated");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.Phone).HasColumnName("Phone");
        }
    }
}
