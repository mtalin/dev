using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EMailNotifyTransactMap : EntityTypeConfiguration<EMailNotifyTransact>
    {
        public EMailNotifyTransactMap()
        {
            // Primary Key
            this.HasKey(t => t.Task);

            // Properties
            this.Property(t => t.Task)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Address)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("EMailNotifyTransact");
            this.Property(t => t.Task).HasColumnName("Task");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Confirmation).HasColumnName("Confirmation");

            // Relationships
            this.HasRequired(t => t.Tasks_Old)
                .WithOptional(t => t.EMailNotifyTransact);

        }
    }
}
