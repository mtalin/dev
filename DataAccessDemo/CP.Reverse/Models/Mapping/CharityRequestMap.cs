using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CharityRequestMap : EntityTypeConfiguration<CharityRequest>
    {
        public CharityRequestMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.CharityName)
                .HasMaxLength(200);

            this.Property(t => t.ContactPerson)
                .IsFixedLength()
                .HasMaxLength(65);

            this.Property(t => t.Address)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .IsFixedLength()
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZIP)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.URL)
                .HasMaxLength(150);

            this.Property(t => t.GeneralOrgInfo)
                .HasMaxLength(3000);

            this.Property(t => t.AdditionalInfo)
                .HasMaxLength(3000);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.Country)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("CharityRequests");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.CharityName).HasColumnName("CharityName");
            this.Property(t => t.ContactPerson).HasColumnName("ContactPerson");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZIP).HasColumnName("ZIP");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.GeneralOrgInfo).HasColumnName("GeneralOrgInfo");
            this.Property(t => t.AdditionalInfo).HasColumnName("AdditionalInfo");
            this.Property(t => t.CharityEdit).HasColumnName("CharityEdit");
            this.Property(t => t.dtCreated).HasColumnName("dtCreated");
            this.Property(t => t.dtClosed).HasColumnName("dtClosed");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Step2).HasColumnName("Step2");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.CharityID).HasColumnName("CharityID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.PublicEIN).HasColumnName("PublicEIN");
            this.Property(t => t.DiscloseIdentity).HasColumnName("DiscloseIdentity");
            this.Property(t => t.GrantDetails).HasColumnName("GrantDetails");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Pending).HasColumnName("Pending");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.CreatedOnBackEnd).HasColumnName("CreatedOnBackEnd");
            this.Property(t => t.Country).HasColumnName("Country");

            // Relationships
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.CharityRequests)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.GrantDetail)
                .WithMany(t => t.CharityRequests)
                .HasForeignKey(d => d.GrantDetails);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.CharityRequests)
                .HasForeignKey(d => d.Person);

        }
    }
}
