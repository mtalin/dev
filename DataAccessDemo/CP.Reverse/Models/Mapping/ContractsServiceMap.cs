using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ContractsServiceMap : EntityTypeConfiguration<ContractsService>
    {
        public ContractsServiceMap()
        {
            // Primary Key
            this.HasKey(t => t.Contract);

            // Properties
            this.Property(t => t.Contract)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ContractsServices", "scm");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.Services).HasColumnName("Services");
        }
    }
}
