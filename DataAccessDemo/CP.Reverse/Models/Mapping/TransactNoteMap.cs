using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TransactNoteMap : EntityTypeConfiguration<TransactNote>
    {
        public TransactNoteMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TransactNotes");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Transact).HasColumnName("Transact");

            // Relationships
            this.HasRequired(t => t.Note1)
                .WithMany(t => t.TransactNotes)
                .HasForeignKey(d => d.Note);
            this.HasRequired(t => t.Transact1)
                .WithMany(t => t.TransactNotes)
                .HasForeignKey(d => d.Transact);

        }
    }
}
