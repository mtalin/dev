using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EventRecipientMap : EntityTypeConfiguration<EventRecipient>
    {
        public EventRecipientMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.XSLScript)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.Flags)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("EventRecipients");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.EventClass).HasColumnName("EventClass");
            this.Property(t => t.XSLScript).HasColumnName("XSLScript");
            this.Property(t => t.Flags).HasColumnName("Flags");

            // Relationships
            this.HasRequired(t => t.EventClass1)
                .WithMany(t => t.EventRecipients)
                .HasForeignKey(d => d.EventClass);
            this.HasRequired(t => t.Recipient)
                .WithOptional(t => t.EventRecipient);

        }
    }
}
