using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MQMap : EntityTypeConfiguration<MQ>
    {
        public MQMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Message)
                .IsRequired();

            this.Property(t => t.Error)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("MQ", "gmx");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Created).HasColumnName("Created");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Error).HasColumnName("Error");
            this.Property(t => t.LastActive).HasColumnName("LastActive");
            this.Property(t => t.Status).HasColumnName("Status");
        }
    }
}
