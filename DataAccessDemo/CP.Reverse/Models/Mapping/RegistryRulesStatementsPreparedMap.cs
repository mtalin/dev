using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RegistryRulesStatementsPreparedMap : EntityTypeConfiguration<RegistryRulesStatementsPrepared>
    {
        public RegistryRulesStatementsPreparedMap()
        {
            // Primary Key
            this.HasKey(t => new { t.BookId, t.PartId, t.LineId, t.TransactType, t.AssetType, t.RegistryRule });

            // Properties
            this.Property(t => t.BookId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LineId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TransactType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RegistryRule)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("RegistryRulesStatementsPrepared");
            this.Property(t => t.BookId).HasColumnName("BookId");
            this.Property(t => t.PartId).HasColumnName("PartId");
            this.Property(t => t.LineId).HasColumnName("LineId");
            this.Property(t => t.TransactType).HasColumnName("TransactType");
            this.Property(t => t.AssetType).HasColumnName("AssetType");
            this.Property(t => t.RegistryRule).HasColumnName("RegistryRule");
        }
    }
}
