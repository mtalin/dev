using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class BogusSSNMap : EntityTypeConfiguration<BogusSSN>
    {
        public BogusSSNMap()
        {
            // Primary Key
            this.HasKey(t => t.SSN);

            // Properties
            this.Property(t => t.SSN)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(9);

            // Table & Column Mappings
            this.ToTable("BogusSSNs");
            this.Property(t => t.SSN).HasColumnName("SSN");
        }
    }
}
