using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PartnersInfoMap : EntityTypeConfiguration<PartnersInfo>
    {
        public PartnersInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PartnersInfo");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AccountingStandard).HasColumnName("AccountingStandard");
            this.Property(t => t.EntityType).HasColumnName("EntityType");
            this.Property(t => t.Final).HasColumnName("Final");
            this.Property(t => t.Amended).HasColumnName("Amended");
            this.Property(t => t.InvestInPtp).HasColumnName("InvestInPtp");
            this.Property(t => t.InvestInForeign).HasColumnName("InvestInForeign");
            this.Property(t => t.InterestSold).HasColumnName("InterestSold");
            this.Property(t => t.Form8886Disclosed).HasColumnName("Form8886Disclosed");
            this.Property(t => t.ManualVerified).HasColumnName("ManualVerified");
            this.Property(t => t.UBIMoreThousand).HasColumnName("UBIMoreThousand");
            this.Property(t => t.OrdinaryGain).HasColumnName("OrdinaryGain");
            this.Property(t => t.BeginProfit).HasColumnName("BeginProfit");
            this.Property(t => t.EndProfit).HasColumnName("EndProfit");
            this.Property(t => t.BeginLoss).HasColumnName("BeginLoss");
            this.Property(t => t.EndLoss).HasColumnName("EndLoss");
            this.Property(t => t.BeginCapital).HasColumnName("BeginCapital");
            this.Property(t => t.EndCapital).HasColumnName("EndCapital");
            this.Property(t => t.Nonrecourse).HasColumnName("Nonrecourse");
            this.Property(t => t.QualifiedNonrecourse).HasColumnName("QualifiedNonrecourse");
            this.Property(t => t.Recourse).HasColumnName("Recourse");
            this.Property(t => t.BeginCapitalAccount).HasColumnName("BeginCapitalAccount");
            this.Property(t => t.EndCapitalAccount).HasColumnName("EndCapitalAccount");
            this.Property(t => t.WD).HasColumnName("WD");
            this.Property(t => t.CapitalContributedDuringYear).HasColumnName("CapitalContributedDuringYear");
            this.Property(t => t.CurrentYearChanges).HasColumnName("CurrentYearChanges");
            this.Property(t => t.ContributeBuiltInChanges).HasColumnName("ContributeBuiltInChanges");

            // Relationships
            this.HasRequired(t => t.Partnership)
                .WithOptional(t => t.PartnersInfo);

        }
    }
}
