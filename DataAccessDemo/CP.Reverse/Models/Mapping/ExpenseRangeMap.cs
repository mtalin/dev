using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ExpenseRangeMap : EntityTypeConfiguration<ExpenseRange>
    {
        public ExpenseRangeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ExpenseRanges");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.MealsHi).HasColumnName("MealsHi");
            this.Property(t => t.MealsLo).HasColumnName("MealsLo");
            this.Property(t => t.LodgingHi).HasColumnName("LodgingHi");
            this.Property(t => t.LodgingLo).HasColumnName("LodgingLo");
        }
    }
}
