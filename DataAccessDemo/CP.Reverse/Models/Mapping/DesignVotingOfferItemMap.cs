using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DesignVotingOfferItemMap : EntityTypeConfiguration<DesignVotingOfferItem>
    {
        public DesignVotingOfferItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.VotingOffer, t.Index });

            // Properties
            this.Property(t => t.VotingOffer)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Index)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IID)
                .HasMaxLength(50);

            this.Property(t => t.IReserved)
                .HasMaxLength(50);

            this.Property(t => t.ICurrFName)
                .HasMaxLength(50);

            this.Property(t => t.ICurrLName)
                .HasMaxLength(50);

            this.Property(t => t.ICurrEmail)
                .HasMaxLength(50);

            this.Property(t => t.IRemove)
                .HasMaxLength(50);

            this.Property(t => t.IReElect)
                .HasMaxLength(50);

            this.Property(t => t.INewFName)
                .HasMaxLength(50);

            this.Property(t => t.INewLName)
                .HasMaxLength(50);

            this.Property(t => t.INewEmail)
                .HasMaxLength(50);

            this.Property(t => t.INotAllowRemove)
                .HasMaxLength(50);

            this.Property(t => t.ICurrent)
                .HasMaxLength(50);

            this.Property(t => t.IStDate)
                .HasMaxLength(50);

            this.Property(t => t.INewNominee)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DesignVotingOfferItems");
            this.Property(t => t.VotingOffer).HasColumnName("VotingOffer");
            this.Property(t => t.Index).HasColumnName("Index");
            this.Property(t => t.IID).HasColumnName("IID");
            this.Property(t => t.IReserved).HasColumnName("IReserved");
            this.Property(t => t.ICurrFName).HasColumnName("ICurrFName");
            this.Property(t => t.ICurrLName).HasColumnName("ICurrLName");
            this.Property(t => t.ICurrEmail).HasColumnName("ICurrEmail");
            this.Property(t => t.IRemove).HasColumnName("IRemove");
            this.Property(t => t.IReElect).HasColumnName("IReElect");
            this.Property(t => t.INewFName).HasColumnName("INewFName");
            this.Property(t => t.INewLName).HasColumnName("INewLName");
            this.Property(t => t.INewEmail).HasColumnName("INewEmail");
            this.Property(t => t.INotAllowRemove).HasColumnName("INotAllowRemove");
            this.Property(t => t.ICurrent).HasColumnName("ICurrent");
            this.Property(t => t.IStDate).HasColumnName("IStDate");
            this.Property(t => t.INewNominee).HasColumnName("INewNominee");
        }
    }
}
