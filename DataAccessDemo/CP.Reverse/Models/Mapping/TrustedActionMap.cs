using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TrustedActionMap : EntityTypeConfiguration<TrustedAction>
    {
        public TrustedActionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.InstructedBy)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.Instruction)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("TrustedActions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Admin).HasColumnName("Admin");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtInstructed).HasColumnName("DtInstructed");
            this.Property(t => t.InstructedBy).HasColumnName("InstructedBy");
            this.Property(t => t.Instruction).HasColumnName("Instruction");
            this.Property(t => t.DtEntered).HasColumnName("DtEntered");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.ActivityLog).HasColumnName("ActivityLog");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.DeclaredAction).HasColumnName("DeclaredAction");

            // Relationships
            this.HasOptional(t => t.ActivityLog1)
                .WithMany(t => t.TrustedActions)
                .HasForeignKey(d => d.ActivityLog);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.TrustedActions)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.TrustedActions)
                .HasForeignKey(d => d.Admin);
            this.HasRequired(t => t.Person2)
                .WithMany(t => t.TrustedActions1)
                .HasForeignKey(d => d.Person);

        }
    }
}
