using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_DonationsMap : EntityTypeConfiguration<et_Donations>
    {
        public et_DonationsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.FoundationName, t.Amount, t.SpecPurposeType, t.SpecPurposeText1, t.SpecPurposeText2, t.IsSpecialDelivery, t.Anonymous, t.IsSpecPurpose, t.IsCertificate, t.IsAwaitngApproval });

            // Properties
            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.DateCreated)
                .HasMaxLength(30);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Attributes)
                .HasMaxLength(1070);

            this.Property(t => t.Purpose)
                .HasMaxLength(50);

            this.Property(t => t.PurposeDescription)
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SpecPurposeText1)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeText2)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.FullPurposeText)
                .HasMaxLength(2000);

            this.Property(t => t.IsSpecPurpose)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsCertificate)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsAwaitngApproval)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.grantreason)
                .HasMaxLength(4000);

            this.Property(t => t.ProgramArea)
                .HasMaxLength(606);

            // Table & Column Mappings
            this.ToTable("et_Donations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Attributes).HasColumnName("Attributes");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.PurposeDescription).HasColumnName("PurposeDescription");
            this.Property(t => t.SpecPurposeType).HasColumnName("SpecPurposeType");
            this.Property(t => t.SpecPurposeText1).HasColumnName("SpecPurposeText1");
            this.Property(t => t.SpecPurposeText2).HasColumnName("SpecPurposeText2");
            this.Property(t => t.FullPurposeText).HasColumnName("FullPurposeText");
            this.Property(t => t.IsSpecialDelivery).HasColumnName("IsSpecialDelivery");
            this.Property(t => t.Anonymous).HasColumnName("Anonymous");
            this.Property(t => t.IsSpecPurpose).HasColumnName("IsSpecPurpose");
            this.Property(t => t.IsCertificate).HasColumnName("IsCertificate");
            this.Property(t => t.IsAwaitngApproval).HasColumnName("IsAwaitngApproval");
            this.Property(t => t.HouseholdAccount).HasColumnName("HouseholdAccount");
            this.Property(t => t.IsMultiYear).HasColumnName("IsMultiYear");
            this.Property(t => t.NumPayments).HasColumnName("NumPayments");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
            this.Property(t => t.PaymentNumber).HasColumnName("PaymentNumber");
            this.Property(t => t.grantreason).HasColumnName("grantreason");
            this.Property(t => t.ProgramArea).HasColumnName("ProgramArea");
        }
    }
}
