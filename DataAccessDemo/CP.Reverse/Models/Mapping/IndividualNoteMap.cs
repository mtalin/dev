using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class IndividualNoteMap : EntityTypeConfiguration<IndividualNote>
    {
        public IndividualNoteMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("IndividualNotes");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasRequired(t => t.Note1)
                .WithMany(t => t.IndividualNotes)
                .HasForeignKey(d => d.Note);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.IndividualNotes)
                .HasForeignKey(d => d.Person);

        }
    }
}
