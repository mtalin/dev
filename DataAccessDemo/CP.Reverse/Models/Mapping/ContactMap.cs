using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ContactMap : EntityTypeConfiguration<Contact>
    {
        public ContactMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Address1)
                .HasMaxLength(50);

            this.Property(t => t.Address2)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZIP)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.HomePhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.BusinessPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Comments)
                .HasMaxLength(600);

            this.Property(t => t.Note)
                .HasMaxLength(550);

            this.Property(t => t.Organization)
                .HasMaxLength(250);

            this.Property(t => t.Country)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("Contacts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZIP).HasColumnName("ZIP");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.HomePhone).HasColumnName("HomePhone");
            this.Property(t => t.BusinessPhone).HasColumnName("BusinessPhone");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.Post).HasColumnName("Post");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.DtAlert).HasColumnName("DtAlert");
            this.Property(t => t.Organization).HasColumnName("Organization");
            this.Property(t => t.Country).HasColumnName("Country");

            // Relationships
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.Contacts)
                .HasForeignKey(d => d.Person);

        }
    }
}
