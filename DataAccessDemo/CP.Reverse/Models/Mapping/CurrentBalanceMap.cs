using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CurrentBalanceMap : EntityTypeConfiguration<CurrentBalance>
    {
        public CurrentBalanceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CurrentBalances");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.Cash).HasColumnName("Cash");
            this.Property(t => t.Securities).HasColumnName("Securities");
            this.Property(t => t.DtBalance).HasColumnName("DtBalance");
            this.Property(t => t.QBalance).HasColumnName("QBalance");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");
        }
    }
}
