using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EventMap : EntityTypeConfiguration<Event>
    {
        public EventMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.HotData)
                .IsRequired()
                .HasMaxLength(8000);

            // Table & Column Mappings
            this.ToTable("Events");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.EventClass).HasColumnName("EventClass");
            this.Property(t => t.HotData).HasColumnName("HotData");
            this.Property(t => t.PlainData).HasColumnName("PlainData");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtRegistered).HasColumnName("DtRegistered");
            this.Property(t => t.DtProcessed).HasColumnName("DtProcessed");
            this.Property(t => t.State).HasColumnName("State");

            // Relationships
            this.HasRequired(t => t.EventClass1)
                .WithMany(t => t.Events)
                .HasForeignKey(d => d.EventClass);

        }
    }
}
