using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PersonMap : EntityTypeConfiguration<Person>
    {
        public PersonMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleInitial)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.KeyAnswer)
                .HasMaxLength(50);

            this.Property(t => t.SSN)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.HomePhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.BusinessPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.BusinessFax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Employer)
                .HasMaxLength(100);

            this.Property(t => t.AnotherPosition)
                .HasMaxLength(256);

            this.Property(t => t.ShareHolder)
                .HasMaxLength(250);

            this.Property(t => t.EmployeeID)
                .HasMaxLength(30);

            this.Property(t => t.Rights)
                .IsRequired()
                .HasMaxLength(256);

            this.Property(t => t.Email2)
                .HasMaxLength(50);

            this.Property(t => t.Email3)
                .HasMaxLength(50);

            this.Property(t => t.CellPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.OtherPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Assistant)
                .HasMaxLength(50);

            this.Property(t => t.AsstPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Dear)
                .HasMaxLength(100);

            this.Property(t => t.MailingDescription)
                .HasMaxLength(500);

            this.Property(t => t.GmxRefs)
                .HasMaxLength(4000);

            // Table & Column Mappings
            this.ToTable("Persons");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.NamePrefix).HasColumnName("NamePrefix");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.MiddleInitial).HasColumnName("MiddleInitial");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.NameSuffix).HasColumnName("NameSuffix");
            this.Property(t => t.KeyQuestion).HasColumnName("KeyQuestion");
            this.Property(t => t.KeyAnswer).HasColumnName("KeyAnswer");
            this.Property(t => t.SSN).HasColumnName("SSN");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.DOB).HasColumnName("DOB");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.MaritalStatus).HasColumnName("MaritalStatus");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.HomePhone).HasColumnName("HomePhone");
            this.Property(t => t.BusinessPhone).HasColumnName("BusinessPhone");
            this.Property(t => t.BusinessFax).HasColumnName("BusinessFax");
            this.Property(t => t.Employer).HasColumnName("Employer");
            this.Property(t => t.SalaryRange).HasColumnName("SalaryRange");
            this.Property(t => t.RegistrationStep).HasColumnName("RegistrationStep");
            this.Property(t => t.MatchingGift).HasColumnName("MatchingGift");
            this.Property(t => t.AnotherPosition).HasColumnName("AnotherPosition");
            this.Property(t => t.ShareHolder).HasColumnName("ShareHolder");
            this.Property(t => t.StockExchange).HasColumnName("StockExchange");
            this.Property(t => t.SHAddress).HasColumnName("SHAddress");
            this.Property(t => t.AllertMessage).HasColumnName("AllertMessage");
            this.Property(t => t.Rank).HasColumnName("Rank");
            this.Property(t => t.ProgramCode).HasColumnName("ProgramCode");
            this.Property(t => t.DOD).HasColumnName("DOD");
            this.Property(t => t.EmployeeID).HasColumnName("EmployeeID");
            this.Property(t => t.DesignMode).HasColumnName("DesignMode");
            this.Property(t => t.Rights).HasColumnName("Rights");
            this.Property(t => t.Email2).HasColumnName("Email2");
            this.Property(t => t.Email3).HasColumnName("Email3");
            this.Property(t => t.PostalAddress2).HasColumnName("PostalAddress2");
            this.Property(t => t.PostalAddress3).HasColumnName("PostalAddress3");
            this.Property(t => t.CellPhone).HasColumnName("CellPhone");
            this.Property(t => t.OtherPhone).HasColumnName("OtherPhone");
            this.Property(t => t.Assistant).HasColumnName("Assistant");
            this.Property(t => t.AsstPhone).HasColumnName("AsstPhone");
            this.Property(t => t.Dear).HasColumnName("Dear");
            this.Property(t => t.OnlineAccess).HasColumnName("OnlineAccess");
            this.Property(t => t.AllowEmail).HasColumnName("AllowEmail");
            this.Property(t => t.AllowMailing).HasColumnName("AllowMailing");
            this.Property(t => t.MailingTax).HasColumnName("MailingTax");
            this.Property(t => t.MailingTaxAddress).HasColumnName("MailingTaxAddress");
            this.Property(t => t.MailingMarketing).HasColumnName("MailingMarketing");
            this.Property(t => t.MailingMarketingAddress).HasColumnName("MailingMarketingAddress");
            this.Property(t => t.MailingInformal).HasColumnName("MailingInformal");
            this.Property(t => t.MailingInformalAddress).HasColumnName("MailingInformalAddress");
            this.Property(t => t.MailingFormal).HasColumnName("MailingFormal");
            this.Property(t => t.MailingFormalAddress).HasColumnName("MailingFormalAddress");
            this.Property(t => t.MailingNewServices).HasColumnName("MailingNewServices");
            this.Property(t => t.MailingNewServicesAddress).HasColumnName("MailingNewServicesAddress");
            this.Property(t => t.MailingDescription).HasColumnName("MailingDescription");
            this.Property(t => t.AllowCall).HasColumnName("AllowCall");
            this.Property(t => t.CallGrantsQuestions).HasColumnName("CallGrantsQuestions");
            this.Property(t => t.CallNewFeaturesReview).HasColumnName("CallNewFeaturesReview");
            this.Property(t => t.CallMarketing).HasColumnName("CallMarketing");
            this.Property(t => t.CallMisc).HasColumnName("CallMisc");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");
            this.Property(t => t.MoreThanMoney).HasColumnName("MoreThanMoney");
            this.Property(t => t.MoreThanMoneyAddress).HasColumnName("MoreThanMoneyAddress");
            this.Property(t => t.ExpenseReinbursement).HasColumnName("ExpenseReinbursement");
            this.Property(t => t.ExpenseReinbursementAddress).HasColumnName("ExpenseReinbursementAddress");
            this.Property(t => t.GmxRefs).HasColumnName("GmxRefs");

            // Relationships
            this.HasOptional(t => t.PostalAddress1)
                .WithMany(t => t.Persons)
                .HasForeignKey(d => d.PostalAddress);
            this.HasOptional(t => t.PostalAddress4)
                .WithMany(t => t.Persons1)
                .HasForeignKey(d => d.SHAddress);
            this.HasOptional(t => t.ProgramCode1)
                .WithMany(t => t.Persons)
                .HasForeignKey(d => d.ProgramCode);
            this.HasRequired(t => t.User)
                .WithOptional(t => t.Person);

        }
    }
}
