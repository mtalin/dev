using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_DonationProposalsMap : EntityTypeConfiguration<et_DonationProposals>
    {
        public et_DonationProposalsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FoundationName, t.Reason, t.VotingCommunity, t.IsApproved, t.IsRejected, t.IsExpired, t.RejectionLanguage, t.Donation });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.ProposalDate)
                .HasMaxLength(30);

            this.Property(t => t.ProposalTime)
                .HasMaxLength(30);

            this.Property(t => t.Reason)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.ProposalCloseDate)
                .HasMaxLength(30);

            this.Property(t => t.VotingCommunity)
                .IsRequired()
                .HasMaxLength(17);

            this.Property(t => t.IsApproved)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsRejected)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsExpired)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RejectionLanguage)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.Donation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("et_DonationProposals");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.ProposalDate).HasColumnName("ProposalDate");
            this.Property(t => t.ProposalTime).HasColumnName("ProposalTime");
            this.Property(t => t.Reason).HasColumnName("Reason");
            this.Property(t => t.ProposalCloseDate).HasColumnName("ProposalCloseDate");
            this.Property(t => t.VotingCommunity).HasColumnName("VotingCommunity");
            this.Property(t => t.IsApproved).HasColumnName("IsApproved");
            this.Property(t => t.IsRejected).HasColumnName("IsRejected");
            this.Property(t => t.IsExpired).HasColumnName("IsExpired");
            this.Property(t => t.RejectionLanguage).HasColumnName("RejectionLanguage");
            this.Property(t => t.Donation).HasColumnName("Donation");
        }
    }
}
