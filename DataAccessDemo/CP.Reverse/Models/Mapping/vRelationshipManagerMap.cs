using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vRelationshipManagerMap : EntityTypeConfiguration<vRelationshipManager>
    {
        public vRelationshipManagerMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FirstName, t.LastName, t.StructuredCompany, t.CompanyPosition });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StructuredCompanyName)
                .HasMaxLength(500);

            this.Property(t => t.Position)
                .HasMaxLength(500);

            this.Property(t => t.CompanyPosition)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vRelationshipManagers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.StructuredCompanyName).HasColumnName("StructuredCompanyName");
            this.Property(t => t.Position).HasColumnName("Position");
            this.Property(t => t.CompanyPosition).HasColumnName("CompanyPosition");
        }
    }
}
