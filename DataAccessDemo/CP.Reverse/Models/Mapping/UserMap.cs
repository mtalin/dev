using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.LoginName)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Password)
                .IsFixedLength()
                .HasMaxLength(129);

            this.Property(t => t.SSOID)
                .HasMaxLength(50);

            this.Property(t => t.PartnerUID)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Users");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.LoginName).HasColumnName("LoginName");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.SID).HasColumnName("SID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.CanChangeName).HasColumnName("CanChangeName");
            this.Property(t => t.SSOID).HasColumnName("SSOID");
            this.Property(t => t.RealID).HasColumnName("RealID");
            this.Property(t => t.PartnerUID).HasColumnName("PartnerUID");
        }
    }
}
