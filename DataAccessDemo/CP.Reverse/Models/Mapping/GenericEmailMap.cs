using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GenericEmailMap : EntityTypeConfiguration<GenericEmail>
    {
        public GenericEmailMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GenericEmails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.EventClass).HasColumnName("EventClass");

            // Relationships
            this.HasRequired(t => t.EventClass1)
                .WithMany(t => t.GenericEmails)
                .HasForeignKey(d => d.EventClass);

        }
    }
}
