using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MQinMap : EntityTypeConfiguration<MQin>
    {
        public MQinMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ObjectType)
                .IsRequired()
                .HasMaxLength(64);

            // Table & Column Mappings
            this.ToTable("MQin", "gmx");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Created).HasColumnName("Created");
            this.Property(t => t.Action).HasColumnName("Action");
            this.Property(t => t.ObjectType).HasColumnName("ObjectType");
            this.Property(t => t.ObjectId).HasColumnName("ObjectId");
        }
    }
}
