using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TrHoldingMapMap : EntityTypeConfiguration<TrHoldingMap>
    {
        public TrHoldingMapMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.HoldingRule)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TrHoldingMaps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.HoldingRule).HasColumnName("HoldingRule");
            this.Property(t => t.Name).HasColumnName("Name");

            // Relationships
            this.HasRequired(t => t.TrHoldingRule)
                .WithMany(t => t.TrHoldingMaps)
                .HasForeignKey(d => d.HoldingRule);

        }
    }
}
