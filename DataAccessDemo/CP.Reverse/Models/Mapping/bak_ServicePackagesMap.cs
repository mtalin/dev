using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class bak_ServicePackagesMap : EntityTypeConfiguration<bak_ServicePackages>
    {
        public bak_ServicePackagesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.ServiceProvider, t.Name, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ServiceProvider)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("bak_ServicePackages");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
        }
    }
}
