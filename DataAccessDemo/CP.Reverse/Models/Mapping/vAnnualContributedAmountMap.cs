using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vAnnualContributedAmountMap : EntityTypeConfiguration<vAnnualContributedAmount>
    {
        public vAnnualContributedAmountMap()
        {
            // Primary Key
            this.HasKey(t => t.Foundation);

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vAnnualContributedAmounts");
            this.Property(t => t.Donor).HasColumnName("Donor");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.ActualAmount).HasColumnName("ActualAmount");
            this.Property(t => t.PhantomAmount).HasColumnName("PhantomAmount");
        }
    }
}
