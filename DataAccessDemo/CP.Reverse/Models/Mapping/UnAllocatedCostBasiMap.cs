using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class UnAllocatedCostBasiMap : EntityTypeConfiguration<UnAllocatedCostBasi>
    {
        public UnAllocatedCostBasiMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Name, t.Type, t.Transact, t.Foundation, t.Asset });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Name2)
                .HasMaxLength(100);

            this.Property(t => t.Name3)
                .HasMaxLength(100);

            this.Property(t => t.Name4)
                .HasMaxLength(100);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Transact)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Asset)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("UnAllocatedCostBasis");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Name2).HasColumnName("Name2");
            this.Property(t => t.Name3).HasColumnName("Name3");
            this.Property(t => t.Name4).HasColumnName("Name4");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Asset).HasColumnName("Asset");
        }
    }
}
