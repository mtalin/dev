using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vCheckingAccountMap : EntityTypeConfiguration<vCheckingAccount>
    {
        public vCheckingAccountMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Bank, t.FullAccountName, t.NextCheckNo });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Bank)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.BankName)
                .HasMaxLength(500);

            this.Property(t => t.BankAddress)
                .HasMaxLength(200);

            this.Property(t => t.AccountNumber)
                .HasMaxLength(50);

            this.Property(t => t.AccountName)
                .HasMaxLength(255);

            this.Property(t => t.RoutingNumber)
                .HasMaxLength(22);

            this.Property(t => t.FullAccountName)
                .IsRequired()
                .HasMaxLength(810);

            this.Property(t => t.PrintedAccountName)
                .HasMaxLength(577);

            this.Property(t => t.BatchFilerID)
                .HasMaxLength(50);

            this.Property(t => t.MasterInquiryPIN)
                .HasMaxLength(50);

            this.Property(t => t.NextCheckNo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LinkedBankName)
                .HasMaxLength(500);

            this.Property(t => t.LinkedBankAddress)
                .HasMaxLength(500);

            this.Property(t => t.LinkedAccountName)
                .HasMaxLength(50);

            this.Property(t => t.LinkedAccountNumber)
                .HasMaxLength(22);

            this.Property(t => t.LinkedRoutingNumber)
                .HasMaxLength(22);

            this.Property(t => t.FullLinkedAccountName)
                .HasMaxLength(577);

            // Table & Column Mappings
            this.ToTable("vCheckingAccounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DisbursementAccount).HasColumnName("DisbursementAccount");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.Bank).HasColumnName("Bank");
            this.Property(t => t.BankName).HasColumnName("BankName");
            this.Property(t => t.BankAddress).HasColumnName("BankAddress");
            this.Property(t => t.AccountNumber).HasColumnName("AccountNumber");
            this.Property(t => t.AccountName).HasColumnName("AccountName");
            this.Property(t => t.RoutingNumber).HasColumnName("RoutingNumber");
            this.Property(t => t.FullAccountName).HasColumnName("FullAccountName");
            this.Property(t => t.PrintedAccountName).HasColumnName("PrintedAccountName");
            this.Property(t => t.BatchFilerID).HasColumnName("BatchFilerID");
            this.Property(t => t.MasterInquiryPIN).HasColumnName("MasterInquiryPIN");
            this.Property(t => t.NextCheckNo).HasColumnName("NextCheckNo");
            this.Property(t => t.LinkedBank).HasColumnName("LinkedBank");
            this.Property(t => t.LinkedBankName).HasColumnName("LinkedBankName");
            this.Property(t => t.LinkedBankAddress).HasColumnName("LinkedBankAddress");
            this.Property(t => t.LinkedAccountName).HasColumnName("LinkedAccountName");
            this.Property(t => t.LinkedAccountNumber).HasColumnName("LinkedAccountNumber");
            this.Property(t => t.LinkedRoutingNumber).HasColumnName("LinkedRoutingNumber");
            this.Property(t => t.AccountType).HasColumnName("AccountType");
            this.Property(t => t.FullLinkedAccountName).HasColumnName("FullLinkedAccountName");
        }
    }
}
