using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vFinancialPartnerAccountMap : EntityTypeConfiguration<vFinancialPartnerAccount>
    {
        public vFinancialPartnerAccountMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.AccountName, t.AccountNumber, t.Transactions });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AccountName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.BankName)
                .HasMaxLength(500);

            this.Property(t => t.RoutingNumber)
                .HasMaxLength(22);

            this.Property(t => t.AccountNumber)
                .IsRequired()
                .HasMaxLength(22);

            this.Property(t => t.BankAddress)
                .HasMaxLength(200);

            this.Property(t => t.Transactions)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("vFinancialPartnerAccounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.AccountName).HasColumnName("AccountName");
            this.Property(t => t.BankName).HasColumnName("BankName");
            this.Property(t => t.RoutingNumber).HasColumnName("RoutingNumber");
            this.Property(t => t.AccountNumber).HasColumnName("AccountNumber");
            this.Property(t => t.BankAddress).HasColumnName("BankAddress");
            this.Property(t => t.Transactions).HasColumnName("Transactions");
        }
    }
}
