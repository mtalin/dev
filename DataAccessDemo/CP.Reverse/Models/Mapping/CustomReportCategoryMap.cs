using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CustomReportCategoryMap : EntityTypeConfiguration<CustomReportCategory>
    {
        public CustomReportCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(400);

            // Table & Column Mappings
            this.ToTable("CustomReportCategories");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Handler).HasColumnName("Handler");
        }
    }
}
