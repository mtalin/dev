using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vFoundationGeneralInfoMap : EntityTypeConfiguration<vFoundationGeneralInfo>
    {
        public vFoundationGeneralInfoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Foundation, t.FoundationName });

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.FoundationSortName)
                .HasMaxLength(106);

            this.Property(t => t.PcaName)
                .HasMaxLength(200);

            this.Property(t => t.RegionName)
                .HasMaxLength(500);

            this.Property(t => t.StrategicPartnerName)
                .HasMaxLength(500);

            this.Property(t => t.SalesRepresentativeName)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("vFoundationGeneralInfo");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.FoundationSortName).HasColumnName("FoundationSortName");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.Pca).HasColumnName("Pca");
            this.Property(t => t.PcaName).HasColumnName("PcaName");
            this.Property(t => t.DtCommence).HasColumnName("DtCommence");
            this.Property(t => t.Region).HasColumnName("Region");
            this.Property(t => t.RegionName).HasColumnName("RegionName");
            this.Property(t => t.StrategicPartnerName).HasColumnName("StrategicPartnerName");
            this.Property(t => t.SalesRepresentative).HasColumnName("SalesRepresentative");
            this.Property(t => t.SalesRepresentativeName).HasColumnName("SalesRepresentativeName");
        }
    }
}
