using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class enum_SystemAddressesMap : EntityTypeConfiguration<enum_SystemAddresses>
    {
        public enum_SystemAddressesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ItemID, t.GroupName });

            // Properties
            this.Property(t => t.ItemID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.GroupName)
                .IsRequired()
                .HasMaxLength(8);

            this.Property(t => t.Name)
                .HasMaxLength(100);

            this.Property(t => t.Address)
                .HasMaxLength(100);

            this.Property(t => t.Address1)
                .HasMaxLength(50);

            this.Property(t => t.Address2)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(2);

            this.Property(t => t.Zip)
                .HasMaxLength(9);

            this.Property(t => t.Phone)
                .HasMaxLength(10);

            this.Property(t => t.Fax)
                .HasMaxLength(10);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.CareOf)
                .HasMaxLength(100);

            this.Property(t => t.Company)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("enum_SystemAddresses");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.GroupName).HasColumnName("GroupName");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.CareOf).HasColumnName("CareOf");
            this.Property(t => t.Company).HasColumnName("Company");
        }
    }
}
