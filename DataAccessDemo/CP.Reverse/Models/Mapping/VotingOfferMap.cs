using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class VotingOfferMap : EntityTypeConfiguration<VotingOffer>
    {
        public VotingOfferMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Purpose)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("VotingOffers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.CloseReason).HasColumnName("CloseReason");
            this.Property(t => t.Meeting).HasColumnName("Meeting");
            this.Property(t => t.Step).HasColumnName("Step");

            // Relationships
            this.HasOptional(t => t.Meeting1)
                .WithMany(t => t.VotingOffers)
                .HasForeignKey(d => d.Meeting);

        }
    }
}
