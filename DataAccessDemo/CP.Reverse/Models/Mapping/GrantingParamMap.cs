using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GrantingParamMap : EntityTypeConfiguration<GrantingParam>
    {
        public GrantingParamMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("GrantingParams");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.CompanyElement).HasColumnName("CompanyElement");
            this.Property(t => t.GrantingLimit).HasColumnName("GrantingLimit");
            this.Property(t => t.ApprovableLimit).HasColumnName("ApprovableLimit");
            this.Property(t => t.GrantCommitteeMember).HasColumnName("GrantCommitteeMember");

            // Relationships
            this.HasRequired(t => t.CompanyElement1)
                .WithMany(t => t.GrantingParams)
                .HasForeignKey(d => d.CompanyElement);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.GrantingParams)
                .HasForeignKey(d => d.Author);

        }
    }
}
