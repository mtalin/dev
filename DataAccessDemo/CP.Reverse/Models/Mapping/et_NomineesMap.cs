using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_NomineesMap : EntityTypeConfiguration<et_Nominees>
    {
        public et_NomineesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Person, t.Foundation });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Person)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PositionName)
                .HasMaxLength(2000);

            this.Property(t => t.RoleName)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("et_Nominees");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Appointment).HasColumnName("Appointment");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.PositionName).HasColumnName("PositionName");
            this.Property(t => t.RoleName).HasColumnName("RoleName");
        }
    }
}
