using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MessageMap : EntityTypeConfiguration<Message>
    {
        public MessageMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.SendFrom)
                .HasMaxLength(150);

            this.Property(t => t.Subject)
                .HasMaxLength(300);

            this.Property(t => t.Description)
                .HasMaxLength(250);

            this.Property(t => t.TemplateItem)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Messages");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.SendFrom).HasColumnName("SendFrom");
            this.Property(t => t.SendTo).HasColumnName("SendTo");
            this.Property(t => t.CopyTo).HasColumnName("CopyTo");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.Theme).HasColumnName("Theme");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtSent).HasColumnName("DtSent");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.ShellCorporation).HasColumnName("ShellCorporation");
            this.Property(t => t.PrivateCopyTo).HasColumnName("PrivateCopyTo");
            this.Property(t => t.TemplateItem).HasColumnName("TemplateItem");
            this.Property(t => t.XMLData).HasColumnName("XMLData");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.EmailTemplate).HasColumnName("EmailTemplate");

            // Relationships
            this.HasOptional(t => t.EmailTemplate1)
                .WithMany(t => t.Messages)
                .HasForeignKey(d => d.EmailTemplate);
            this.HasOptional(t => t.Event1)
                .WithMany(t => t.Messages)
                .HasForeignKey(d => d.Event);
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.Messages)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.PostalAddress1)
                .WithMany(t => t.Messages)
                .HasForeignKey(d => d.PostalAddress);
            this.HasRequired(t => t.User)
                .WithMany(t => t.Messages)
                .HasForeignKey(d => d.Person);

        }
    }
}
