using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PersonsStepMap : EntityTypeConfiguration<PersonsStep>
    {
        public PersonsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("PersonsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");

            // Relationships
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.PersonsSteps)
                .HasForeignKey(d => d.Person);
            this.HasOptional(t => t.User)
                .WithMany(t => t.PersonsSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
