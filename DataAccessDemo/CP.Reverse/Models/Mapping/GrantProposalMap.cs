using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GrantProposalMap : EntityTypeConfiguration<GrantProposal>
    {
        public GrantProposalMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("GrantProposals");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Proposal).HasColumnName("Proposal");
            this.Property(t => t.GrantDetails).HasColumnName("GrantDetails");
            this.Property(t => t.Excluded).HasColumnName("Excluded");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");

            // Relationships
            this.HasRequired(t => t.GrantDetail)
                .WithMany(t => t.GrantProposals)
                .HasForeignKey(d => d.GrantDetails);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.GrantProposals)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Proposal1)
                .WithMany(t => t.GrantProposals)
                .HasForeignKey(d => d.Proposal);

        }
    }
}
