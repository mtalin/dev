using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ShellCorporationMap : EntityTypeConfiguration<ShellCorporation>
    {
        public ShellCorporationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.EIN)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.EINProfileNumber)
                .IsFixedLength()
                .HasMaxLength(12);

            this.Property(t => t.DCFProfileNumber)
                .IsFixedLength()
                .HasMaxLength(12);

            this.Property(t => t.Account)
                .HasMaxLength(50);

            this.Property(t => t.EFTPSTracer)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.EFTPSPin)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.EFTPSPassword)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.DocLocatorNumber)
                .HasMaxLength(25);

            this.Property(t => t.IRSContactName)
                .HasMaxLength(50);

            this.Property(t => t.IRScontactPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.IRSContactFax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.IRSContactEmail)
                .HasMaxLength(50);

            this.Property(t => t.IRSContactBadgeNo)
                .HasMaxLength(20);

            this.Property(t => t.IRSFeeCheckNo)
                .HasMaxLength(10);

            this.Property(t => t.IncorpState)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("ShellCorporations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.President).HasColumnName("President");
            this.Property(t => t.DtIncorporation).HasColumnName("DtIncorporation");
            this.Property(t => t.EIN).HasColumnName("EIN");
            this.Property(t => t.EINProfileNumber).HasColumnName("EINProfileNumber");
            this.Property(t => t.DCFProfileNumber).HasColumnName("DCFProfileNumber");
            this.Property(t => t.DtDCFReceived).HasColumnName("DtDCFReceived");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Account).HasColumnName("Account");
            this.Property(t => t.EFTPSTracer).HasColumnName("EFTPSTracer");
            this.Property(t => t.EFTPSPin).HasColumnName("EFTPSPin");
            this.Property(t => t.EFTPSPassword).HasColumnName("EFTPSPassword");
            this.Property(t => t.DtAssigned).HasColumnName("DtAssigned");
            this.Property(t => t.DtCOIFiled).HasColumnName("DtCOIFiled");
            this.Property(t => t.DtRegistration).HasColumnName("DtRegistration");
            this.Property(t => t.DtCOIReceived).HasColumnName("DtCOIReceived");
            this.Property(t => t.DtNameChangeFiled).HasColumnName("DtNameChangeFiled");
            this.Property(t => t.DtNameChangeReceived).HasColumnName("DtNameChangeReceived");
            this.Property(t => t.DtStartupMailed).HasColumnName("DtStartupMailed");
            this.Property(t => t.DtStartupReceived).HasColumnName("DtStartupReceived");
            this.Property(t => t.DtIRSPacketMailed).HasColumnName("DtIRSPacketMailed");
            this.Property(t => t.DtDocLocatorReceived).HasColumnName("DtDocLocatorReceived");
            this.Property(t => t.DocLocatorNumber).HasColumnName("DocLocatorNumber");
            this.Property(t => t.IRSContactName).HasColumnName("IRSContactName");
            this.Property(t => t.IRScontactPhone).HasColumnName("IRScontactPhone");
            this.Property(t => t.IRSContactFax).HasColumnName("IRSContactFax");
            this.Property(t => t.IRSContactEmail).HasColumnName("IRSContactEmail");
            this.Property(t => t.IRSContactBadgeNo).HasColumnName("IRSContactBadgeNo");
            this.Property(t => t.IRSContactAddress).HasColumnName("IRSContactAddress");
            this.Property(t => t.DtIRSApproval).HasColumnName("DtIRSApproval");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.AccountLocked).HasColumnName("AccountLocked");
            this.Property(t => t.Dt8655Mailed).HasColumnName("Dt8655Mailed");
            this.Property(t => t.DtCP575Filed).HasColumnName("DtCP575Filed");
            this.Property(t => t.Dt1023Mailed).HasColumnName("Dt1023Mailed");
            this.Property(t => t.DtTAD).HasColumnName("DtTAD");
            this.Property(t => t.Conversion).HasColumnName("Conversion");
            this.Property(t => t.ShellNumber).HasColumnName("ShellNumber");
            this.Property(t => t.IRSUserFee).HasColumnName("IRSUserFee");
            this.Property(t => t.IRSFeeCheckNo).HasColumnName("IRSFeeCheckNo");
            this.Property(t => t.dtIRSFeeCheck).HasColumnName("dtIRSFeeCheck");
            this.Property(t => t.IncorpState).HasColumnName("IncorpState");
            this.Property(t => t.LegalForm).HasColumnName("LegalForm");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");
        }
    }
}
