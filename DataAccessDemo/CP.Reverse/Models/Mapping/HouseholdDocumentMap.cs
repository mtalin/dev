using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class HouseholdDocumentMap : EntityTypeConfiguration<HouseholdDocument>
    {
        public HouseholdDocumentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("HouseholdDocuments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.HouseholdAccount).HasColumnName("HouseholdAccount");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.Document1)
                .WithMany(t => t.HouseholdDocuments)
                .HasForeignKey(d => d.Document);
            this.HasRequired(t => t.HouseholdAccount1)
                .WithMany(t => t.HouseholdDocuments)
                .HasForeignKey(d => d.HouseholdAccount);

        }
    }
}
