using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ObjectsPermissionMap : EntityTypeConfiguration<ObjectsPermission>
    {
        public ObjectsPermissionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Object)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.AppliedPermissions)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.ResolvedPermissions)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("ObjectsPermissions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Object).HasColumnName("Object");
            this.Property(t => t.AppliedPermissions).HasColumnName("AppliedPermissions");
            this.Property(t => t.ResolvedPermissions).HasColumnName("ResolvedPermissions");
            this.Property(t => t.NoteText).HasColumnName("NoteText");
            this.Property(t => t.NoteAuthor).HasColumnName("NoteAuthor");

            // Relationships
            this.HasOptional(t => t.Person)
                .WithMany(t => t.ObjectsPermissions)
                .HasForeignKey(d => d.NoteAuthor);

        }
    }
}
