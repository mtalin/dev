using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vGrantingActivityMap : EntityTypeConfiguration<vGrantingActivity>
    {
        public vGrantingActivityMap()
        {
            // Primary Key
            this.HasKey(t => new { t.StructuredCompany, t.GrantDate });

            // Properties
            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vGrantingActivity");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.CompletedAmount).HasColumnName("CompletedAmount");
            this.Property(t => t.PendingAmount).HasColumnName("PendingAmount");
            this.Property(t => t.WaitingApprovalAmount).HasColumnName("WaitingApprovalAmount");
            this.Property(t => t.WaitingRequestAmount).HasColumnName("WaitingRequestAmount");
            this.Property(t => t.GrantRecomendationAmount).HasColumnName("GrantRecomendationAmount");
            this.Property(t => t.GrantDate).HasColumnName("GrantDate");
        }
    }
}
