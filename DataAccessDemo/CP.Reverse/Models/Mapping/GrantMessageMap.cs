using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GrantMessageMap : EntityTypeConfiguration<GrantMessage>
    {
        public GrantMessageMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("GrantMessages");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.GrantDetails).HasColumnName("GrantDetails");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Event).HasColumnName("Event");

            // Relationships
            this.HasOptional(t => t.Event1)
                .WithMany(t => t.GrantMessages)
                .HasForeignKey(d => d.Event);
            this.HasRequired(t => t.GrantDetail)
                .WithMany(t => t.GrantMessages)
                .HasForeignKey(d => d.GrantDetails);
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.GrantMessages)
                .HasForeignKey(d => d.Message);

        }
    }
}
