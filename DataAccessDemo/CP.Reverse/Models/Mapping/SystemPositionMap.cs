using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class SystemPositionMap : EntityTypeConfiguration<SystemPosition>
    {
        public SystemPositionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Flags)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("SystemPositions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.dtDeleted).HasColumnName("dtDeleted");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.Flags).HasColumnName("Flags");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");

            // Relationships
            this.HasOptional(t => t.ObjectsPermission)
                .WithMany(t => t.SystemPositions)
                .HasForeignKey(d => d.ObjectPermissions);
            this.HasRequired(t => t.User)
                .WithMany(t => t.SystemPositions)
                .HasForeignKey(d => d.Author);

        }
    }
}
