using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReturnStateMap : EntityTypeConfiguration<TaxReturnState>
    {
        public TaxReturnStateMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.State)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Comments)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("TaxReturnStates");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Comments).HasColumnName("Comments");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.TaxReturnStates)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
