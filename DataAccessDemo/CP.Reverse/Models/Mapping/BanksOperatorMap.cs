using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class BanksOperatorMap : EntityTypeConfiguration<BanksOperator>
    {
        public BanksOperatorMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Bank, t.FinancialPartner });

            // Properties
            this.Property(t => t.Bank)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FinancialPartner)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("BanksOperators");
            this.Property(t => t.Bank).HasColumnName("Bank");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
        }
    }
}
