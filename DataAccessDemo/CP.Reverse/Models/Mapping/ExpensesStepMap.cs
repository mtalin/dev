using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ExpensesStepMap : EntityTypeConfiguration<ExpensesStep>
    {
        public ExpensesStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ExpensesSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Expense).HasColumnName("Expense");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.Expens)
                .WithMany(t => t.ExpensesSteps)
                .HasForeignKey(d => d.Expense);
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.ExpensesSteps)
                .HasForeignKey(d => d.Message);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.ExpensesSteps)
                .HasForeignKey(d => d.Note);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ExpensesSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
