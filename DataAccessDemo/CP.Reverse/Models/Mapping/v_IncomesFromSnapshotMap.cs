using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class v_IncomesFromSnapshotMap : EntityTypeConfiguration<v_IncomesFromSnapshot>
    {
        public v_IncomesFromSnapshotMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Year, t.Quarter, t.dtStartPeriod, t.dtEndPeriod });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Year)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Quarter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("v_IncomesFromSnapshot", "Tax");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.Quarter).HasColumnName("Quarter");
            this.Property(t => t.IntIncome).HasColumnName("IntIncome");
            this.Property(t => t.DivIncome).HasColumnName("DivIncome");
            this.Property(t => t.CapitalGains).HasColumnName("CapitalGains");
            this.Property(t => t.AssetManagementFees).HasColumnName("AssetManagementFees");
            this.Property(t => t.MiscCredits).HasColumnName("MiscCredits");
            this.Property(t => t.Line11Income).HasColumnName("Line11Income");
            this.Property(t => t.CrossInvInc).HasColumnName("CrossInvInc");
            this.Property(t => t.NetInvIncome).HasColumnName("NetInvIncome");
            this.Property(t => t.PartnershipIncome).HasColumnName("PartnershipIncome");
            this.Property(t => t.TaxRate).HasColumnName("TaxRate");
            this.Property(t => t.dtStartPeriod).HasColumnName("dtStartPeriod");
            this.Property(t => t.dtEndPeriod).HasColumnName("dtEndPeriod");
        }
    }
}
