using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vTransferDetailMap : EntityTypeConfiguration<vTransferDetail>
    {
        public vTransferDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.Foundation);

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FaxAttnCompany)
                .HasMaxLength(500);

            this.Property(t => t.FaxAttnName)
                .HasMaxLength(200);

            this.Property(t => t.FaxAttnPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.FaxAttnFax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Email2)
                .HasMaxLength(50);

            this.Property(t => t.Email3)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("vTransferDetails");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FaxAttnCompany).HasColumnName("FaxAttnCompany");
            this.Property(t => t.FaxAttnName).HasColumnName("FaxAttnName");
            this.Property(t => t.FaxAttnPhone).HasColumnName("FaxAttnPhone");
            this.Property(t => t.FaxAttnFax).HasColumnName("FaxAttnFax");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Email2).HasColumnName("Email2");
            this.Property(t => t.Email3).HasColumnName("Email3");
        }
    }
}
