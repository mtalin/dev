using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AllFinancialPartnerMap : EntityTypeConfiguration<AllFinancialPartner>
    {
        public AllFinancialPartnerMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.SupportsOnline, t.IsTestPartner, t.SingleSignOn, t.HideSuggestion, t.StructuredCompany });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Fax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Institution)
                .HasMaxLength(500);

            this.Property(t => t.URL)
                .HasMaxLength(500);

            this.Property(t => t.SupportsOnline)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsTestPartner)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.HideSuggestion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.UID)
                .HasMaxLength(20);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("AllFinancialPartners");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.PhoneExt).HasColumnName("PhoneExt");
            this.Property(t => t.Contact).HasColumnName("Contact");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Institution).HasColumnName("Institution");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.AccessURL).HasColumnName("AccessURL");
            this.Property(t => t.Step6Amount).HasColumnName("Step6Amount");
            this.Property(t => t.Step6Note).HasColumnName("Step6Note");
            this.Property(t => t.SupportsOnline).HasColumnName("SupportsOnline");
            this.Property(t => t.IsTestPartner).HasColumnName("IsTestPartner");
            this.Property(t => t.FlatPartnerRate).HasColumnName("FlatPartnerRate");
            this.Property(t => t.BrochureSheetTitle).HasColumnName("BrochureSheetTitle");
            this.Property(t => t.SingleSignOn).HasColumnName("SingleSignOn");
            this.Property(t => t.HideSuggestion).HasColumnName("HideSuggestion");
            this.Property(t => t.UID).HasColumnName("UID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
        }
    }
}
