using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MeetingsAttendedMap : EntityTypeConfiguration<MeetingsAttended>
    {
        public MeetingsAttendedMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.MiddleInitial)
                .HasMaxLength(15);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("MeetingsAttended");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Meeting).HasColumnName("Meeting");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.MiddleInitial).HasColumnName("MiddleInitial");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.IsSecretary).HasColumnName("IsSecretary");
            this.Property(t => t.WasPresent).HasColumnName("WasPresent");
            this.Property(t => t.Role).HasColumnName("Role");

            // Relationships
            this.HasRequired(t => t.Meeting1)
                .WithMany(t => t.MeetingsAttendeds)
                .HasForeignKey(d => d.Meeting);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.MeetingsAttendeds)
                .HasForeignKey(d => d.Person);

        }
    }
}
