using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RecentFoundationLookupMap : EntityTypeConfiguration<RecentFoundationLookup>
    {
        public RecentFoundationLookupMap()
        {
            // Primary Key
            this.HasKey(t => t.FoundationID);

            // Properties
            this.Property(t => t.FoundationID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("RecentFoundationLookups");
            this.Property(t => t.FoundationID).HasColumnName("FoundationID");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.LastUpdateDate).HasColumnName("LastUpdateDate");
        }
    }
}
