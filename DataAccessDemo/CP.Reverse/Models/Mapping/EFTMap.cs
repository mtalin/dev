using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EFTMap : EntityTypeConfiguration<EFT>
    {
        public EFTMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.BankName, t.RoutingNo, t.AccountNo });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.BankName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.RoutingNo)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AccountNo)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("EFTs");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.BankName).HasColumnName("BankName");
            this.Property(t => t.RoutingNo).HasColumnName("RoutingNo");
            this.Property(t => t.AccountNo).HasColumnName("AccountNo");
        }
    }
}
