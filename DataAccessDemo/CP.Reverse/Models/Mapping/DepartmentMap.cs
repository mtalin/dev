using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DepartmentMap : EntityTypeConfiguration<Department>
    {
        public DepartmentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Fax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Design)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Departments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.HigherDepartment).HasColumnName("HigherDepartment");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Design).HasColumnName("Design");

            // Relationships
            this.HasRequired(t => t.CompanyElement)
                .WithOptional(t => t.Department);
            this.HasOptional(t => t.Department1)
                .WithMany(t => t.Departments1)
                .HasForeignKey(d => d.HigherDepartment);
            this.HasOptional(t => t.PostalAddress1)
                .WithMany(t => t.Departments)
                .HasForeignKey(d => d.PostalAddress);
            this.HasRequired(t => t.StructuredCompany1)
                .WithMany(t => t.Departments)
                .HasForeignKey(d => d.StructuredCompany);

        }
    }
}
