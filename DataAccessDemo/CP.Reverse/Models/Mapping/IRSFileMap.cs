using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class IRSFileMap : EntityTypeConfiguration<IRSFile>
    {
        public IRSFileMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.State, t.ImportStep, t.ImportTotal, t.ImportFixedAddresses, t.ImportMatchedAuto, t.ImportMatchedManual, t.ImportRemovedAuto, t.ImportRemovedTotal, t.ImportCopiedTotal, t.ImportNewCharities, t.ImportChangesTotal, t.ImportChangesProcessed, t.ImportChangesProcessedPerSecond, t.ImportHoldByFoundationCode, t.ImportHoldByGroupExempt, t.Stat1VerifiedPublicCharities, t.Stat1MissingData, t.Stat1NonPublicCharities, t.Stat1SupportingOrgsCode17, t.Stat1SupportingOrgsTypeIII, t.Stat1GroupExemptOrgs, t.Stat2VerifiedPublicCharities, t.Stat2MissingData, t.Stat2NonPublicCharities, t.Stat2SupportingOrgsCode17, t.Stat2SupportingOrgsTypeIII, t.Stat2GroupExemptOrgs });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.State)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DownloadFolder)
                .HasMaxLength(100);

            this.Property(t => t.ImportTotal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportFixedAddresses)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportMatchedAuto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportMatchedManual)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportRemovedAuto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportRemovedTotal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportCopiedTotal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportNewCharities)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportChangesTotal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportChangesProcessed)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportChangesProcessedPerSecond)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportHoldByFoundationCode)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ImportHoldByGroupExempt)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat1VerifiedPublicCharities)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat1MissingData)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat1NonPublicCharities)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat1SupportingOrgsCode17)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat1SupportingOrgsTypeIII)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat1GroupExemptOrgs)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat2VerifiedPublicCharities)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat2MissingData)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat2NonPublicCharities)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat2SupportingOrgsCode17)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat2SupportingOrgsTypeIII)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Stat2GroupExemptOrgs)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("IRSFiles");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.IRSInstructionDate).HasColumnName("IRSInstructionDate");
            this.Property(t => t.IRSReleaseDate).HasColumnName("IRSReleaseDate");
            this.Property(t => t.IRSRecordsNumber).HasColumnName("IRSRecordsNumber");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DtLastChecked).HasColumnName("DtLastChecked");
            this.Property(t => t.DtDownloadStart).HasColumnName("DtDownloadStart");
            this.Property(t => t.DownloadAttempts).HasColumnName("DownloadAttempts");
            this.Property(t => t.TotalDownloadSize).HasColumnName("TotalDownloadSize");
            this.Property(t => t.CurrentDownloadSize).HasColumnName("CurrentDownloadSize");
            this.Property(t => t.DownloadFolder).HasColumnName("DownloadFolder");
            this.Property(t => t.DtDownloadFinish).HasColumnName("DtDownloadFinish");
            this.Property(t => t.DtParseStart).HasColumnName("DtParseStart");
            this.Property(t => t.GoodParsedRecordsNumber).HasColumnName("GoodParsedRecordsNumber");
            this.Property(t => t.BadParsedRecordsNumber).HasColumnName("BadParsedRecordsNumber");
            this.Property(t => t.ErrorFormatRecordsNumber).HasColumnName("ErrorFormatRecordsNumber");
            this.Property(t => t.FilteredRecordsNumber).HasColumnName("FilteredRecordsNumber");
            this.Property(t => t.ParsedRecordsInPerSecond).HasColumnName("ParsedRecordsInPerSecond");
            this.Property(t => t.CopiedCURecordsNumber).HasColumnName("CopiedCURecordsNumber");
            this.Property(t => t.CopiedBCRecordsNumber).HasColumnName("CopiedBCRecordsNumber");
            this.Property(t => t.DtParseFinish).HasColumnName("DtParseFinish");
            this.Property(t => t.DtImportStart).HasColumnName("DtImportStart");
            this.Property(t => t.DtImportFinish).HasColumnName("DtImportFinish");
            this.Property(t => t.ImportStep).HasColumnName("ImportStep");
            this.Property(t => t.ImportTotal).HasColumnName("ImportTotal");
            this.Property(t => t.ImportP1Start).HasColumnName("ImportP1Start");
            this.Property(t => t.ImportP2Start).HasColumnName("ImportP2Start");
            this.Property(t => t.ImportP3Start).HasColumnName("ImportP3Start");
            this.Property(t => t.ImportP4Start).HasColumnName("ImportP4Start");
            this.Property(t => t.ImportP5Start).HasColumnName("ImportP5Start");
            this.Property(t => t.ImportP6Start).HasColumnName("ImportP6Start");
            this.Property(t => t.ImportP7Start).HasColumnName("ImportP7Start");
            this.Property(t => t.ImportP8Start).HasColumnName("ImportP8Start");
            this.Property(t => t.ImportP9Start).HasColumnName("ImportP9Start");
            this.Property(t => t.ImportFixedAddresses).HasColumnName("ImportFixedAddresses");
            this.Property(t => t.ImportMatchedAuto).HasColumnName("ImportMatchedAuto");
            this.Property(t => t.ImportMatchedManual).HasColumnName("ImportMatchedManual");
            this.Property(t => t.ImportRemovedAuto).HasColumnName("ImportRemovedAuto");
            this.Property(t => t.ImportRemovedTotal).HasColumnName("ImportRemovedTotal");
            this.Property(t => t.ImportCopiedTotal).HasColumnName("ImportCopiedTotal");
            this.Property(t => t.ImportNewCharities).HasColumnName("ImportNewCharities");
            this.Property(t => t.ImportChangesTotal).HasColumnName("ImportChangesTotal");
            this.Property(t => t.ImportChangesProcessed).HasColumnName("ImportChangesProcessed");
            this.Property(t => t.ImportChangesProcessedPerSecond).HasColumnName("ImportChangesProcessedPerSecond");
            this.Property(t => t.ImportHoldByFoundationCode).HasColumnName("ImportHoldByFoundationCode");
            this.Property(t => t.ImportHoldByGroupExempt).HasColumnName("ImportHoldByGroupExempt");
            this.Property(t => t.Stat1VerifiedPublicCharities).HasColumnName("Stat1VerifiedPublicCharities");
            this.Property(t => t.Stat1MissingData).HasColumnName("Stat1MissingData");
            this.Property(t => t.Stat1NonPublicCharities).HasColumnName("Stat1NonPublicCharities");
            this.Property(t => t.Stat1SupportingOrgsCode17).HasColumnName("Stat1SupportingOrgsCode17");
            this.Property(t => t.Stat1SupportingOrgsTypeIII).HasColumnName("Stat1SupportingOrgsTypeIII");
            this.Property(t => t.Stat1GroupExemptOrgs).HasColumnName("Stat1GroupExemptOrgs");
            this.Property(t => t.Stat2VerifiedPublicCharities).HasColumnName("Stat2VerifiedPublicCharities");
            this.Property(t => t.Stat2MissingData).HasColumnName("Stat2MissingData");
            this.Property(t => t.Stat2NonPublicCharities).HasColumnName("Stat2NonPublicCharities");
            this.Property(t => t.Stat2SupportingOrgsCode17).HasColumnName("Stat2SupportingOrgsCode17");
            this.Property(t => t.Stat2SupportingOrgsTypeIII).HasColumnName("Stat2SupportingOrgsTypeIII");
            this.Property(t => t.Stat2GroupExemptOrgs).HasColumnName("Stat2GroupExemptOrgs");
        }
    }
}
