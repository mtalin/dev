using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CompanyPersonMap : EntityTypeConfiguration<CompanyPerson>
    {
        public CompanyPersonMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CompanyPersons");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.GrantingLimit).HasColumnName("GrantingLimit");
            this.Property(t => t.ApproveGrants).HasColumnName("ApproveGrants");
            this.Property(t => t.ApprovableLimit).HasColumnName("ApprovableLimit");
            this.Property(t => t.GrantCommitteeMember).HasColumnName("GrantCommitteeMember");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.CompanyPersons)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person2)
                .WithMany(t => t.CompanyPersons1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasRequired(t => t.Person3)
                .WithMany(t => t.CompanyPersons2)
                .HasForeignKey(d => d.Person);
            this.HasRequired(t => t.StructuredCompany1)
                .WithMany(t => t.CompanyPersons)
                .HasForeignKey(d => d.StructuredCompany);

        }
    }
}
