using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReceiptMap : EntityTypeConfiguration<TaxReceipt>
    {
        public TaxReceiptMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TaxReceipts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.RYear).HasColumnName("RYear");
            this.Property(t => t.Donor).HasColumnName("Donor");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtPrinted).HasColumnName("DtPrinted");
            this.Property(t => t.PrintedBy).HasColumnName("PrintedBy");
            this.Property(t => t.DtRevised).HasColumnName("DtRevised");
            this.Property(t => t.RevisedBy).HasColumnName("RevisedBy");
            this.Property(t => t.DeliveredBy).HasColumnName("DeliveredBy");

            // Relationships
            this.HasRequired(t => t.Donor1)
                .WithMany(t => t.TaxReceipts)
                .HasForeignKey(d => d.Donor);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.TaxReceipts)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.TaxReceipts)
                .HasForeignKey(d => d.Note);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.TaxReceipts)
                .HasForeignKey(d => d.PrintedBy);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.TaxReceipts1)
                .HasForeignKey(d => d.RevisedBy);

        }
    }
}
