using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PersonsNativeMap : EntityTypeConfiguration<PersonsNative>
    {
        public PersonsNativeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Person, t.Company, t.Position, t.SystemPosition, t.GrantCommitteeMember });

            // Properties
            this.Property(t => t.Person)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Company)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Position)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SystemPosition)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PersonsNatives", "scm");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.CompanyType).HasColumnName("CompanyType");
            this.Property(t => t.Position).HasColumnName("Position");
            this.Property(t => t.SystemPosition).HasColumnName("SystemPosition");
            this.Property(t => t.GrantingLimit).HasColumnName("GrantingLimit");
            this.Property(t => t.ApprovalLimit).HasColumnName("ApprovalLimit");
            this.Property(t => t.GrantCommitteeMember).HasColumnName("GrantCommitteeMember");
        }
    }
}
