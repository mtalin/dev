using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class old_FinancialPartnersMap : EntityTypeConfiguration<old_FinancialPartners>
    {
        public old_FinancialPartnersMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .HasMaxLength(20);

            this.Property(t => t.Fax)
                .HasMaxLength(20);

            this.Property(t => t.PhoneExt)
                .HasMaxLength(20);

            this.Property(t => t.Contact)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Institution)
                .HasMaxLength(50);

            this.Property(t => t.URL)
                .HasMaxLength(150);

            this.Property(t => t.AccessURL)
                .HasMaxLength(150);

            this.Property(t => t.Step6Note)
                .HasMaxLength(1500);

            this.Property(t => t.BrochureSheetTitle)
                .HasMaxLength(500);

            this.Property(t => t.UID)
                .HasMaxLength(50);

            this.Property(t => t.tmp_Action)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("old_FinancialPartners");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.PhoneExt).HasColumnName("PhoneExt");
            this.Property(t => t.Contact).HasColumnName("Contact");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Institution).HasColumnName("Institution");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.AccessURL).HasColumnName("AccessURL");
            this.Property(t => t.Step6Amount).HasColumnName("Step6Amount");
            this.Property(t => t.Step6Note).HasColumnName("Step6Note");
            this.Property(t => t.SupportsOnline).HasColumnName("SupportsOnline");
            this.Property(t => t.IsTestPartner).HasColumnName("IsTestPartner");
            this.Property(t => t.FlatPartnerRate).HasColumnName("FlatPartnerRate");
            this.Property(t => t.BrochureSheetTitle).HasColumnName("BrochureSheetTitle");
            this.Property(t => t.SingleSignOn).HasColumnName("SingleSignOn");
            this.Property(t => t.HideSuggestion).HasColumnName("HideSuggestion");
            this.Property(t => t.UID).HasColumnName("UID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.tmp_Action).HasColumnName("tmp_Action");
        }
    }
}
