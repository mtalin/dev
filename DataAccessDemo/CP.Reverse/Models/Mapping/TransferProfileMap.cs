using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TransferProfileMap : EntityTypeConfiguration<TransferProfile>
    {
        public TransferProfileMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.SignerOther)
                .HasMaxLength(1000);

            this.Property(t => t.EmailInstructionNote)
                .HasMaxLength(1000);

            this.Property(t => t.TransferMemo)
                .HasMaxLength(1000);

            this.Property(t => t.SpecialNote)
                .HasMaxLength(4000);

            // Table & Column Mappings
            this.ToTable("TransferProfiles");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.IsCharity).HasColumnName("IsCharity");
            this.Property(t => t.Signer1).HasColumnName("Signer1");
            this.Property(t => t.Signer2).HasColumnName("Signer2");
            this.Property(t => t.Signer3).HasColumnName("Signer3");
            this.Property(t => t.SignerOther).HasColumnName("SignerOther");
            this.Property(t => t.Method).HasColumnName("Method");
            this.Property(t => t.EmailInstructionNote).HasColumnName("EmailInstructionNote");
            this.Property(t => t.TransferMemo).HasColumnName("TransferMemo");
            this.Property(t => t.MinAmount).HasColumnName("MinAmount");
            this.Property(t => t.MaxAmount).HasColumnName("MaxAmount");
            this.Property(t => t.SpecialHandling).HasColumnName("SpecialHandling");
            this.Property(t => t.SpecialNote).HasColumnName("SpecialNote");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.IsDraft).HasColumnName("IsDraft");

            // Relationships
            this.HasOptional(t => t.Contract1)
                .WithMany(t => t.TransferProfiles)
                .HasForeignKey(d => d.Contract);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.TransferProfiles)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.TransferProfiles)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.TransferProfiles1)
                .HasForeignKey(d => d.Signer1);
            this.HasOptional(t => t.Person2)
                .WithMany(t => t.TransferProfiles2)
                .HasForeignKey(d => d.Signer2);
            this.HasOptional(t => t.Person3)
                .WithMany(t => t.TransferProfiles3)
                .HasForeignKey(d => d.Signer3);
            this.HasOptional(t => t.Person4)
                .WithMany(t => t.TransferProfiles4)
                .HasForeignKey(d => d.DeletedBy);

        }
    }
}
