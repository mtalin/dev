using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TransactMap : EntityTypeConfiguration<Transact>
    {
        public TransactMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FPTransactID)
                .HasMaxLength(120);

            this.Property(t => t.MergeTicker)
                .HasMaxLength(32);

            this.Property(t => t.Notes)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("Transacts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.FPTransactID).HasColumnName("FPTransactID");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Principal).HasColumnName("Principal");
            this.Property(t => t.Commision).HasColumnName("Commision");
            this.Property(t => t.Tax).HasColumnName("Tax");
            this.Property(t => t.SecFee).HasColumnName("SecFee");
            this.Property(t => t.MiscFee).HasColumnName("MiscFee");
            this.Property(t => t.OthFee).HasColumnName("OthFee");
            this.Property(t => t.ClearingCharge).HasColumnName("ClearingCharge");
            this.Property(t => t.BrokerageCharge).HasColumnName("BrokerageCharge");
            this.Property(t => t.InvestValue).HasColumnName("InvestValue");
            this.Property(t => t.Taxable).HasColumnName("Taxable");
            this.Property(t => t.UnitsNumber).HasColumnName("UnitsNumber");
            this.Property(t => t.SplitNumerator).HasColumnName("SplitNumerator");
            this.Property(t => t.SplitDenominator).HasColumnName("SplitDenominator");
            this.Property(t => t.MergeTicker).HasColumnName("MergeTicker");
            this.Property(t => t.Ratio).HasColumnName("Ratio");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Restricted).HasColumnName("Restricted");
            this.Property(t => t.Hold).HasColumnName("Hold");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.QTransaction).HasColumnName("QTransaction");
            this.Property(t => t.Interest).HasColumnName("Interest");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.Asset2).HasColumnName("Asset2");
            this.Property(t => t.CostBasis).HasColumnName("CostBasis");
            this.Property(t => t.BookBasis).HasColumnName("BookBasis");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
            this.Property(t => t.dtDeposit).HasColumnName("dtDeposit");
            this.Property(t => t.Ignore).HasColumnName("Ignore");

            // Relationships
            this.HasOptional(t => t.Asset1)
                .WithMany(t => t.Transacts)
                .HasForeignKey(d => d.Asset);
            this.HasOptional(t => t.Asset3)
                .WithMany(t => t.Transacts1)
                .HasForeignKey(d => d.Asset2);
            this.HasRequired(t => t.FoundationAccount1)
                .WithMany(t => t.Transacts)
                .HasForeignKey(d => d.FoundationAccount);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Transacts)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.QTransaction1)
                .WithMany(t => t.Transacts)
                .HasForeignKey(d => d.QTransaction);

        }
    }
}
