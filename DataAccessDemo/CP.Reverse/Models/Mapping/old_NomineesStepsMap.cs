using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class old_NomineesStepsMap : EntityTypeConfiguration<old_NomineesSteps>
    {
        public old_NomineesStepsMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("old_NomineesSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Nominee).HasColumnName("Nominee");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Appointment).HasColumnName("Appointment");
            this.Property(t => t.TypeOfChange).HasColumnName("TypeOfChange");
            this.Property(t => t.StaffingProposal).HasColumnName("StaffingProposal");
            this.Property(t => t.DtAccepted).HasColumnName("DtAccepted");
            this.Property(t => t.CompanyPosition).HasColumnName("CompanyPosition");

            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.old_NomineesSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
