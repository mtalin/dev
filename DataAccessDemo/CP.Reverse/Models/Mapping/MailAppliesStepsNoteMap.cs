using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MailAppliesStepsNoteMap : EntityTypeConfiguration<MailAppliesStepsNote>
    {
        public MailAppliesStepsNoteMap()
        {
            // Primary Key
            this.HasKey(t => t.StepID);

            // Properties
            this.Property(t => t.StepID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Note)
                .IsRequired()
                .HasMaxLength(550);

            // Table & Column Mappings
            this.ToTable("MailAppliesStepsNotes");
            this.Property(t => t.StepID).HasColumnName("StepID");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasRequired(t => t.MailAppliesStep)
                .WithOptional(t => t.MailAppliesStepsNote);

        }
    }
}
