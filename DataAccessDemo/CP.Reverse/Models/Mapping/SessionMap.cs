using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class SessionMap : EntityTypeConfiguration<Session>
    {
        public SessionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Sessions", "tbl");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.dtCreated).HasColumnName("dtCreated");
            this.Property(t => t.dtClosed).HasColumnName("dtClosed");
        }
    }
}
