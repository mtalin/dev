using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class temp_foundation_dataMap : EntityTypeConfiguration<temp_foundation_data>
    {
        public temp_foundation_dataMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("temp_foundation_data");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.UI_Year1).HasColumnName("UI_Year1");
            this.Property(t => t.UI_Year2).HasColumnName("UI_Year2");
            this.Property(t => t.UI_Year3).HasColumnName("UI_Year3");
            this.Property(t => t.UI_Year4).HasColumnName("UI_Year4");
            this.Property(t => t.UISP_Year1).HasColumnName("UISP_Year1");
            this.Property(t => t.UISP_Year2).HasColumnName("UISP_Year2");
            this.Property(t => t.UISP_Year3).HasColumnName("UISP_Year3");
            this.Property(t => t.UISP_Year4).HasColumnName("UISP_Year4");
            this.Property(t => t.TOTAL_UI).HasColumnName("TOTAL_UI");
            this.Property(t => t.UIA_Year1).HasColumnName("UIA_Year1");
            this.Property(t => t.UIA_Year2).HasColumnName("UIA_Year2");
            this.Property(t => t.UIA_Year3).HasColumnName("UIA_Year3");
            this.Property(t => t.UIA_Year4).HasColumnName("UIA_Year4");
            this.Property(t => t.UIA_Amount1).HasColumnName("UIA_Amount1");
            this.Property(t => t.UIA_Amount2).HasColumnName("UIA_Amount2");
            this.Property(t => t.UIA_Amount3).HasColumnName("UIA_Amount3");
            this.Property(t => t.UIA_Amount4).HasColumnName("UIA_Amount4");
            this.Property(t => t.Percent_NII).HasColumnName("Percent_NII");
            this.Property(t => t.TotalDonations).HasColumnName("TotalDonations");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.temp_foundation_data)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
