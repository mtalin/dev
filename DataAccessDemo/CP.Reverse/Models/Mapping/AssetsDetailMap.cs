using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AssetsDetailMap : EntityTypeConfiguration<AssetsDetail>
    {
        public AssetsDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Name, t.Type, t.CategoryName });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Name2)
                .HasMaxLength(100);

            this.Property(t => t.Name3)
                .HasMaxLength(100);

            this.Property(t => t.Name4)
                .HasMaxLength(100);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Symbol)
                .HasMaxLength(32);

            this.Property(t => t.CUSIP)
                .IsFixedLength()
                .HasMaxLength(12);

            this.Property(t => t.TypeFlags)
                .HasMaxLength(150);

            this.Property(t => t.TypeName)
                .HasMaxLength(150);

            this.Property(t => t.CategoryName)
                .IsRequired()
                .HasMaxLength(17);

            // Table & Column Mappings
            this.ToTable("AssetsDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Name2).HasColumnName("Name2");
            this.Property(t => t.Name3).HasColumnName("Name3");
            this.Property(t => t.Name4).HasColumnName("Name4");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Symbol).HasColumnName("Symbol");
            this.Property(t => t.CUSIP).HasColumnName("CUSIP");
            this.Property(t => t.TypeFlags).HasColumnName("TypeFlags");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.CategoryName).HasColumnName("CategoryName");
        }
    }
}
