using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReturnMap : EntityTypeConfiguration<TaxReturn>
    {
        public TaxReturnMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.IssuesCache)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(50);

            this.Property(t => t.XmlData)
                .IsRequired();

            this.Property(t => t.TrackingNumber)
                .HasMaxLength(50);

            this.Property(t => t.ReviewInstruction)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("TaxReturns");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtSentToSaber).HasColumnName("DtSentToSaber");
            this.Property(t => t.DtSentToIRS).HasColumnName("DtSentToIRS");
            this.Property(t => t.DtFinalized).HasColumnName("DtFinalized");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.IssuesCache).HasColumnName("IssuesCache");
            this.Property(t => t.XmlData).HasColumnName("XmlData");
            this.Property(t => t.TaxPackageRecalc).HasColumnName("TaxPackageRecalc");
            this.Property(t => t.dtRecalc).HasColumnName("dtRecalc");
            this.Property(t => t.ResponsiblePerson).HasColumnName("ResponsiblePerson");
            this.Property(t => t.State1).HasColumnName("State1");
            this.Property(t => t.State2).HasColumnName("State2");
            this.Property(t => t.State3).HasColumnName("State3");
            this.Property(t => t.State4).HasColumnName("State4");
            this.Property(t => t.FilingMethod).HasColumnName("FilingMethod");
            this.Property(t => t.MailCarrier).HasColumnName("MailCarrier");
            this.Property(t => t.TrackingNumber).HasColumnName("TrackingNumber");
            this.Property(t => t.DtSentToProcessing).HasColumnName("DtSentToProcessing");
            this.Property(t => t.TaxCenterState).HasColumnName("TaxCenterState");
            this.Property(t => t.DeliveredBy).HasColumnName("DeliveredBy");
            this.Property(t => t.TaxDue).HasColumnName("TaxDue");
            this.Property(t => t.Priority).HasColumnName("Priority");
            this.Property(t => t.ReviewInstruction).HasColumnName("ReviewInstruction");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.TaxReturns)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.TaxReturns)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.TaxReturns1)
                .HasForeignKey(d => d.ResponsiblePerson);

        }
    }
}
