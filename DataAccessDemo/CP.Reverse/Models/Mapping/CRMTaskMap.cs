using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CRMTaskMap : EntityTypeConfiguration<CRMTask>
    {
        public CRMTaskMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("CRMTasks");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Recipient).HasColumnName("Recipient");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.PersonClosed).HasColumnName("PersonClosed");
            this.Property(t => t.EmailTemplate).HasColumnName("EmailTemplate");
            this.Property(t => t.assignedPerson).HasColumnName("assignedPerson");
            this.Property(t => t.EventClass).HasColumnName("EventClass");
            this.Property(t => t.NoteExists).HasColumnName("NoteExists");
            this.Property(t => t.Priority).HasColumnName("Priority");

            // Relationships
            this.HasOptional(t => t.Person)
                .WithMany(t => t.CRMTasks)
                .HasForeignKey(d => d.assignedPerson);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.CRMTasks1)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.EmailTemplate1)
                .WithMany(t => t.CRMTasks)
                .HasForeignKey(d => d.EmailTemplate);
            this.HasOptional(t => t.EventClass1)
                .WithMany(t => t.CRMTasks)
                .HasForeignKey(d => d.EventClass);
            this.HasRequired(t => t.Event1)
                .WithMany(t => t.CRMTasks)
                .HasForeignKey(d => d.Event);
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.CRMTasks)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Person2)
                .WithMany(t => t.CRMTasks2)
                .HasForeignKey(d => d.PersonClosed);
            this.HasOptional(t => t.Person3)
                .WithMany(t => t.CRMTasks3)
                .HasForeignKey(d => d.Recipient);

        }
    }
}
