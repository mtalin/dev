using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ActivityLogItemMap : EntityTypeConfiguration<ActivityLogItem>
    {
        public ActivityLogItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.ActivityLog, t.LoggedAction, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.ActivityLog)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LoggedAction)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ActivityLogItems");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ActivityLog).HasColumnName("ActivityLog");
            this.Property(t => t.LoggedAction).HasColumnName("LoggedAction");
            this.Property(t => t.Object).HasColumnName("Object");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.ActivityLog1)
                .WithMany(t => t.ActivityLogItems)
                .HasForeignKey(d => d.ActivityLog);

        }
    }
}
