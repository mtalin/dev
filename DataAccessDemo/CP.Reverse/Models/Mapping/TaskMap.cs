using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaskMap : EntityTypeConfiguration<Task>
    {
        public TaskMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.PredAction)
                .HasMaxLength(50);

            this.Property(t => t.PredParam)
                .HasMaxLength(1024);

            this.Property(t => t.BodyAction)
                .HasMaxLength(50);

            this.Property(t => t.BodyParam)
                .HasMaxLength(1024);

            this.Property(t => t.PostAction)
                .HasMaxLength(50);

            this.Property(t => t.PostParam)
                .HasMaxLength(1024);

            this.Property(t => t.TaskParam)
                .HasMaxLength(1024);

            // Table & Column Mappings
            this.ToTable("Tasks");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtStart).HasColumnName("DtStart");
            this.Property(t => t.DtStop).HasColumnName("DtStop");
            this.Property(t => t.DtNext).HasColumnName("DtNext");
            this.Property(t => t.Template).HasColumnName("Template");
            this.Property(t => t.PredAction).HasColumnName("PredAction");
            this.Property(t => t.PredParam).HasColumnName("PredParam");
            this.Property(t => t.BodyAction).HasColumnName("BodyAction");
            this.Property(t => t.BodyParam).HasColumnName("BodyParam");
            this.Property(t => t.PostAction).HasColumnName("PostAction");
            this.Property(t => t.PostParam).HasColumnName("PostParam");
            this.Property(t => t.ReturnCode).HasColumnName("ReturnCode");
            this.Property(t => t.TaskParam).HasColumnName("TaskParam");
        }
    }
}
