using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TransferScheduleMap : EntityTypeConfiguration<TransferSchedule>
    {
        public TransferScheduleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Days)
                .IsRequired()
                .HasMaxLength(7);

            // Table & Column Mappings
            this.ToTable("TransferSchedules");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Days).HasColumnName("Days");
            this.Property(t => t.DtStart).HasColumnName("DtStart");
            this.Property(t => t.DtEnd).HasColumnName("DtEnd");
            this.Property(t => t.TransferProfile).HasColumnName("TransferProfile");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.TransferSchedules)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.TransferSchedules1)
                .HasForeignKey(d => d.DeletedBy);

        }
    }
}
