using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class old_NomineesMap : EntityTypeConfiguration<old_Nominees>
    {
        public old_NomineesMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("old_Nominees");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Appointment).HasColumnName("Appointment");
            this.Property(t => t.DonationsLimit).HasColumnName("DonationsLimit");
            this.Property(t => t.RegistrationStep).HasColumnName("RegistrationStep");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.NewPosition).HasColumnName("NewPosition");
            this.Property(t => t.StaffingProposal).HasColumnName("StaffingProposal");
            this.Property(t => t.DtAccDeadline).HasColumnName("DtAccDeadline");
            this.Property(t => t.DeclaredAppointment).HasColumnName("DeclaredAppointment");
            this.Property(t => t.DtAccepted).HasColumnName("DtAccepted");
            this.Property(t => t.Reelection).HasColumnName("Reelection");
            this.Property(t => t.ApprovableLimit).HasColumnName("ApprovableLimit");
            this.Property(t => t.GrantCommitteeMember).HasColumnName("GrantCommitteeMember");
            this.Property(t => t.DtTermsExpired).HasColumnName("DtTermsExpired");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.old_Nominees)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.old_Nominees)
                .HasForeignKey(d => d.Person);

        }
    }
}
