using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ActivityCodeMap : EntityTypeConfiguration<ActivityCode>
    {
        public ActivityCodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Activitycode1);

            // Properties
            this.Property(t => t.Activitycode1)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.Category1)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.Category2)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.Category3)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.Category4)
                .IsFixedLength()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("ActivityCodes");
            this.Property(t => t.Activitycode1).HasColumnName("Activitycode");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Category1).HasColumnName("Category1");
            this.Property(t => t.Category2).HasColumnName("Category2");
            this.Property(t => t.Category3).HasColumnName("Category3");
            this.Property(t => t.Category4).HasColumnName("Category4");
        }
    }
}
