using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ContractPricingMap : EntityTypeConfiguration<ContractPricing>
    {
        public ContractPricingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContractPricings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.PricingSchedule).HasColumnName("PricingSchedule");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ContractPricings)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Contract1)
                .WithMany(t => t.ContractPricings)
                .HasForeignKey(d => d.Contract);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.ContractPricings1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasRequired(t => t.PricingSchedule1)
                .WithMany(t => t.ContractPricings)
                .HasForeignKey(d => d.PricingSchedule);

        }
    }
}
