using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EstimatedTaxesStepMap : EntityTypeConfiguration<EstimatedTaxesStep>
    {
        public EstimatedTaxesStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("EstimatedTaxesSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.EstimatedTax).HasColumnName("EstimatedTax");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.PrevStep).HasColumnName("PrevStep");

            // Relationships
            this.HasRequired(t => t.EstimatedTax1)
                .WithMany(t => t.EstimatedTaxesSteps)
                .HasForeignKey(d => d.EstimatedTax);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.EstimatedTaxesSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
