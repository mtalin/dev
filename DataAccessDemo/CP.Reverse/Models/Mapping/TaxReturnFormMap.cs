using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReturnFormMap : EntityTypeConfiguration<TaxReturnForm>
    {
        public TaxReturnFormMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TaxReturnForms");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.TaxReturn).HasColumnName("TaxReturn");
            this.Property(t => t.Theme).HasColumnName("Theme");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.TaxReturnForms)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.TaxReturnForms1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasRequired(t => t.TaxReturn1)
                .WithMany(t => t.TaxReturnForms)
                .HasForeignKey(d => d.TaxReturn);

        }
    }
}
