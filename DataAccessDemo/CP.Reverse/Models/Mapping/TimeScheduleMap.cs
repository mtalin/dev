using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TimeScheduleMap : EntityTypeConfiguration<TimeSchedule>
    {
        public TimeScheduleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.DayList)
                .HasMaxLength(15);

            this.Property(t => t.MonthList)
                .HasMaxLength(30);

            this.Property(t => t.HotData)
                .HasMaxLength(4000);

            // Table & Column Mappings
            this.ToTable("TimeSchedules");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtStart).HasColumnName("DtStart");
            this.Property(t => t.DtStop).HasColumnName("DtStop");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.DayPeriod).HasColumnName("DayPeriod");
            this.Property(t => t.WeekPeriod).HasColumnName("WeekPeriod");
            this.Property(t => t.Day).HasColumnName("Day");
            this.Property(t => t.Week).HasColumnName("Week");
            this.Property(t => t.DayList).HasColumnName("DayList");
            this.Property(t => t.MonthList).HasColumnName("MonthList");
            this.Property(t => t.EventClass).HasColumnName("EventClass");
            this.Property(t => t.DtLastRun).HasColumnName("DtLastRun");
            this.Property(t => t.DtNextRun).HasColumnName("DtNextRun");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.HotData).HasColumnName("HotData");
            this.Property(t => t.Reminder).HasColumnName("Reminder");
            this.Property(t => t.OnHold).HasColumnName("OnHold");
            this.Property(t => t.SplitOverrides).HasColumnName("SplitOverrides");
            this.Property(t => t.InProcess).HasColumnName("InProcess");
            this.Property(t => t.BusinessDaysOnly).HasColumnName("BusinessDaysOnly");
            this.Property(t => t.DaysBefore).HasColumnName("DaysBefore");
            this.Property(t => t.ScheduledDate).HasColumnName("ScheduledDate");

            // Relationships
            this.HasRequired(t => t.EventClass1)
                .WithMany(t => t.TimeSchedules)
                .HasForeignKey(d => d.EventClass);

        }
    }
}
