using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CompanyElementMap : EntityTypeConfiguration<CompanyElement>
    {
        public CompanyElementMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("CompanyElements");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CompanyElements)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.CompanyElements1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasOptional(t => t.ObjectsPermission)
                .WithMany(t => t.CompanyElements)
                .HasForeignKey(d => d.ObjectPermissions);

        }
    }
}
