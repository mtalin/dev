using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GlobalAssetMap : EntityTypeConfiguration<GlobalAsset>
    {
        public GlobalAssetMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Symbol)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.CUSIP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(12);

            // Table & Column Mappings
            this.ToTable("GlobalAssets");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Symbol).HasColumnName("Symbol");
            this.Property(t => t.CUSIP).HasColumnName("CUSIP");

            // Relationships
            this.HasRequired(t => t.Asset)
                .WithOptional(t => t.GlobalAsset);

        }
    }
}
