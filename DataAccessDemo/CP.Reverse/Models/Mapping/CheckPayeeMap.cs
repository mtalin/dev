using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CheckPayeeMap : EntityTypeConfiguration<CheckPayee>
    {
        public CheckPayeeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CheckPayees");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CheckPayees)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.PostalAddress1)
                .WithMany(t => t.CheckPayees)
                .HasForeignKey(d => d.PostalAddress);

        }
    }
}
