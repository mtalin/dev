using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ProposalMap : EntityTypeConfiguration<Proposal>
    {
        public ProposalMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Purpose)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Proposals");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Voting).HasColumnName("Voting");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.LegalForm).HasColumnName("LegalForm");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.SpecialAuthor).HasColumnName("SpecialAuthor");
            this.Property(t => t.Original).HasColumnName("Original");

            // Relationships
            this.HasOptional(t => t.Document)
                .WithMany(t => t.Proposals)
                .HasForeignKey(d => d.LegalForm);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Proposals)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.Proposals)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Proposal1)
                .WithMany(t => t.Proposals1)
                .HasForeignKey(d => d.Original);
            this.HasOptional(t => t.Voting1)
                .WithMany(t => t.Proposals)
                .HasForeignKey(d => d.Voting);

        }
    }
}
