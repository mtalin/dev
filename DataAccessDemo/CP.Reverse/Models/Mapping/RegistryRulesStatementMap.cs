using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RegistryRulesStatementMap : EntityTypeConfiguration<RegistryRulesStatement>
    {
        public RegistryRulesStatementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("RegistryRulesStatements");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.BookId).HasColumnName("BookId");
            this.Property(t => t.PartId).HasColumnName("PartId");
            this.Property(t => t.LineId).HasColumnName("LineId");
            this.Property(t => t.TransactType).HasColumnName("TransactType");
            this.Property(t => t.AssetType).HasColumnName("AssetType");
            this.Property(t => t.RegistryRule).HasColumnName("RegistryRule");
        }
    }
}
