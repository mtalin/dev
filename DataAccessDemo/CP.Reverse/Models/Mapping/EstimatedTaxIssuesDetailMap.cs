using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EstimatedTaxIssuesDetailMap : EntityTypeConfiguration<EstimatedTaxIssuesDetail>
    {
        public EstimatedTaxIssuesDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FSAccount, t.Foundation, t.YE, t.Status, t.dtRepaymentTax, t.Quarter, t.FoundationName });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FSAccount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.PCA)
                .HasMaxLength(200);

            this.Property(t => t.Region)
                .HasMaxLength(500);

            this.Property(t => t.YE)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Status)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StatusName)
                .HasMaxLength(150);

            this.Property(t => t.Quarter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("EstimatedTaxIssuesDetails", "Tax");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FSAccount).HasColumnName("FSAccount");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.PcaId).HasColumnName("PcaId");
            this.Property(t => t.PCA).HasColumnName("PCA");
            this.Property(t => t.RegionId).HasColumnName("RegionId");
            this.Property(t => t.Region).HasColumnName("Region");
            this.Property(t => t.YE).HasColumnName("YE");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Reconciliation).HasColumnName("Reconciliation");
            this.Property(t => t.MCB).HasColumnName("MCB");
            this.Property(t => t.ULYM).HasColumnName("ULYM");
            this.Property(t => t.PCD).HasColumnName("PCD");
            this.Property(t => t.MC).HasColumnName("MC");
            this.Property(t => t.PXIN).HasColumnName("PXIN");
            this.Property(t => t.DataChange).HasColumnName("DataChange");
            this.Property(t => t.Other).HasColumnName("Other");
            this.Property(t => t.MissStatements).HasColumnName("MissStatements");
            this.Property(t => t.CapitalGainsReview).HasColumnName("CapitalGainsReview");
            this.Property(t => t.NotFunded).HasColumnName("NotFunded");
            this.Property(t => t.Upload990PF).HasColumnName("Upload990PF");
            this.Property(t => t.PartnershipCD).HasColumnName("PartnershipCD");
            this.Property(t => t.dtRepaymentTax).HasColumnName("dtRepaymentTax");
            this.Property(t => t.StatusName).HasColumnName("StatusName");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Quarter).HasColumnName("Quarter");
            this.Property(t => t.dtStatusChange).HasColumnName("dtStatusChange");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
        }
    }
}
