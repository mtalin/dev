using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FAPTrainingRegisteredMap : EntityTypeConfiguration<FAPTrainingRegistered>
    {
        public FAPTrainingRegisteredMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("FAPTrainingRegistered");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.FAPTraining)
                .WithMany(t => t.FAPTrainingRegistereds)
                .HasForeignKey(d => d.Event);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.FAPTrainingRegistereds)
                .HasForeignKey(d => d.Person);

        }
    }
}
