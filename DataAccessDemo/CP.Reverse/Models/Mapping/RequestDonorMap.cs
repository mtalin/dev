using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RequestDonorMap : EntityTypeConfiguration<RequestDonor>
    {
        public RequestDonorMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("RequestDonors");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.QTransaction).HasColumnName("QTransaction");
            this.Property(t => t.QDeposit).HasColumnName("QDeposit");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Document).HasColumnName("Document");

            // Relationships
            this.HasOptional(t => t.Document1)
                .WithMany(t => t.RequestDonors)
                .HasForeignKey(d => d.Document);

        }
    }
}
