using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DonationCertificateMap : EntityTypeConfiguration<DonationCertificate>
    {
        public DonationCertificateMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("DonationCertificates");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Recipient).HasColumnName("Recipient");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.SingleGrant).HasColumnName("SingleGrant");

            // Relationships
            this.HasOptional(t => t.Event1)
                .WithMany(t => t.DonationCertificates)
                .HasForeignKey(d => d.Event);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.DonationCertificates)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.DonationCertificates)
                .HasForeignKey(d => d.Message);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.DonationCertificates)
                .HasForeignKey(d => d.Person);
            this.HasRequired(t => t.Person2)
                .WithMany(t => t.DonationCertificates1)
                .HasForeignKey(d => d.Recipient);

        }
    }
}
