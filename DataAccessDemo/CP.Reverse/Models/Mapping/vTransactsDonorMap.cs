using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vTransactsDonorMap : EntityTypeConfiguration<vTransactsDonor>
    {
        public vTransactsDonorMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Transact, t.DtCreated, t.Foundation, t.dtTransact, t.type });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Transact)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.InCareOf)
                .HasMaxLength(200);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vTransactsDonors");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.ChildAmount).HasColumnName("ChildAmount");
            this.Property(t => t.Donor).HasColumnName("Donor");
            this.Property(t => t.PhantomDonor).HasColumnName("PhantomDonor");
            this.Property(t => t.Spouse).HasColumnName("Spouse");
            this.Property(t => t.InCareOf).HasColumnName("InCareOf");
            this.Property(t => t.IsDonorAlive).HasColumnName("IsDonorAlive");
            this.Property(t => t.IsSpouseAlive).HasColumnName("IsSpouseAlive");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.GoodsServices).HasColumnName("GoodsServices");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.dtTransact).HasColumnName("dtTransact");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.asset).HasColumnName("asset");
            this.Property(t => t.unitsNumber).HasColumnName("unitsNumber");
            this.Property(t => t.ActualAmount).HasColumnName("ActualAmount");
        }
    }
}
