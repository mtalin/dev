using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxesDetailMap : EntityTypeConfiguration<TaxesDetail>
    {
        public TaxesDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Type, t.Amount, t.DtCreated, t.PeriodType, t.PeriodCount, t.Step, t.Q1Covered, t.Q2Covered, t.Q3Covered, t.Q4Covered, t.TypeName, t.FoundationName });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PeriodType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PeriodCount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DataSnapShot)
                .HasMaxLength(3000);

            this.Property(t => t.TargetName)
                .HasMaxLength(200);

            this.Property(t => t.TypeName)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.LongTypeName)
                .HasMaxLength(300);

            this.Property(t => t.TypeInfo)
                .HasMaxLength(150);

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.FinancialPartnerName)
                .HasMaxLength(500);

            this.Property(t => t.FinancialAdvisorName)
                .HasMaxLength(152);

            this.Property(t => t.TaxCalcMethod)
                .HasMaxLength(150);

            this.Property(t => t.PeriodName)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("TaxesDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Amount2).HasColumnName("Amount2");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");
            this.Property(t => t.PeriodType).HasColumnName("PeriodType");
            this.Property(t => t.PeriodCount).HasColumnName("PeriodCount");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.PendingTransact).HasColumnName("PendingTransact");
            this.Property(t => t.DataSnapShot).HasColumnName("DataSnapShot");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.BillTo).HasColumnName("BillTo");
            this.Property(t => t.FeeCalculation).HasColumnName("FeeCalculation");
            this.Property(t => t.Q1Covered).HasColumnName("Q1Covered");
            this.Property(t => t.Q2Covered).HasColumnName("Q2Covered");
            this.Property(t => t.Q3Covered).HasColumnName("Q3Covered");
            this.Property(t => t.Q4Covered).HasColumnName("Q4Covered");
            this.Property(t => t.TargetName).HasColumnName("TargetName");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.LongTypeName).HasColumnName("LongTypeName");
            this.Property(t => t.TypeInfo).HasColumnName("TypeInfo");
            this.Property(t => t.Recurring).HasColumnName("Recurring");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.FinancialAdvisor).HasColumnName("FinancialAdvisor");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.FinancialPartnerName).HasColumnName("FinancialPartnerName");
            this.Property(t => t.FinancialAdvisorName).HasColumnName("FinancialAdvisorName");
            this.Property(t => t.TaxCalcMethod).HasColumnName("TaxCalcMethod");
            this.Property(t => t.Quarter).HasColumnName("Quarter");
            this.Property(t => t.QuarterCoverageMask).HasColumnName("QuarterCoverageMask");
            this.Property(t => t.PeriodName).HasColumnName("PeriodName");
            this.Property(t => t.TaxesResponsibleNote).HasColumnName("TaxesResponsibleNote");
        }
    }
}
