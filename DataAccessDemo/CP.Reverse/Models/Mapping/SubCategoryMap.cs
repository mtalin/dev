using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class SubCategoryMap : EntityTypeConfiguration<SubCategory>
    {
        public SubCategoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Activiticode, t.Name });

            // Properties
            this.Property(t => t.Activiticode)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.C1st)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.C2nd)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.C3rd)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.C4th)
                .IsFixedLength()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("SubCategories");
            this.Property(t => t.Activiticode).HasColumnName("Activiticode");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.C1st).HasColumnName("1st");
            this.Property(t => t.C2nd).HasColumnName("2nd");
            this.Property(t => t.C3rd).HasColumnName("3rd");
            this.Property(t => t.C4th).HasColumnName("4th");
        }
    }
}
