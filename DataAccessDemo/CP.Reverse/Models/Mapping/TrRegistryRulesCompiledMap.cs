using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TrRegistryRulesCompiledMap : EntityTypeConfiguration<TrRegistryRulesCompiled>
    {
        public TrRegistryRulesCompiledMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Compiled)
                .HasMaxLength(4000);

            // Table & Column Mappings
            this.ToTable("TrRegistryRulesCompiled");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Compiled).HasColumnName("Compiled");
        }
    }
}
