using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GenericTextMap : EntityTypeConfiguration<GenericText>
    {
        public GenericTextMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Text });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Text)
                .IsRequired()
                .HasMaxLength(4096);

            // Table & Column Mappings
            this.ToTable("GenericTexts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Text).HasColumnName("Text");
        }
    }
}
