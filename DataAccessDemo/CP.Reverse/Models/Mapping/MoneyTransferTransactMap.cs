using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MoneyTransferTransactMap : EntityTypeConfiguration<MoneyTransferTransact>
    {
        public MoneyTransferTransactMap()
        {
            // Primary Key
            this.HasKey(t => t.Task);

            // Properties
            this.Property(t => t.Task)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MoneyTransferTransact");
            this.Property(t => t.Task).HasColumnName("Task");
            this.Property(t => t.SourceAccount).HasColumnName("SourceAccount");
            this.Property(t => t.TargetAccount).HasColumnName("TargetAccount");
            this.Property(t => t.Amount).HasColumnName("Amount");

            // Relationships
            this.HasRequired(t => t.Tasks_Old)
                .WithOptional(t => t.MoneyTransferTransact);

        }
    }
}
