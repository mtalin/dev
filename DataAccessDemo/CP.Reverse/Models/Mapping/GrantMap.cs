using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GrantMap : EntityTypeConfiguration<Grant>
    {
        public GrantMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Grants");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.PendingTransact).HasColumnName("PendingTransact");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.GrantDetails).HasColumnName("GrantDetails");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Amount).HasColumnName("Amount");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Grants)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.GrantDetail)
                .WithMany(t => t.Grants)
                .HasForeignKey(d => d.GrantDetails);
            this.HasOptional(t => t.PendingTransact1)
                .WithMany(t => t.Grants)
                .HasForeignKey(d => d.PendingTransact);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.Grants)
                .HasForeignKey(d => d.Author);

        }
    }
}
