using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class StructuredCompanyTypeMap : EntityTypeConfiguration<StructuredCompanyType>
    {
        public StructuredCompanyTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("StructuredCompanyTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");

            // Relationships
            this.HasOptional(t => t.ObjectsPermission)
                .WithMany(t => t.StructuredCompanyTypes)
                .HasForeignKey(d => d.ObjectPermissions);

        }
    }
}
