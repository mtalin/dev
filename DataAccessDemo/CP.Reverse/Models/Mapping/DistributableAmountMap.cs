using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DistributableAmountMap : EntityTypeConfiguration<DistributableAmount>
    {
        public DistributableAmountMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TimeStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("DistributableAmounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.DA).HasColumnName("DA");
            this.Property(t => t.UI).HasColumnName("UI");
            this.Property(t => t.EGC).HasColumnName("EGC");
            this.Property(t => t.NCUA).HasColumnName("NCUA");
            this.Property(t => t.AQD).HasColumnName("AQD");
            this.Property(t => t.C1_NII).HasColumnName("1%NII");
            this.Property(t => t.QD).HasColumnName("QD");
            this.Property(t => t.TOC).HasColumnName("TOC");
            this.Property(t => t.RQE).HasColumnName("RQE");
            this.Property(t => t.DAB).HasColumnName("DAB");
            this.Property(t => t.DAE).HasColumnName("DAE");
            this.Property(t => t.ETY).HasColumnName("ETY");
            this.Property(t => t.ProjectedUI).HasColumnName("ProjectedUI");
            this.Property(t => t.ProjectedDA).HasColumnName("ProjectedDA");
            this.Property(t => t.UI_Year1).HasColumnName("UI_Year1");
            this.Property(t => t.UI_Year2).HasColumnName("UI_Year2");
            this.Property(t => t.UI_Year3).HasColumnName("UI_Year3");
            this.Property(t => t.UI_Year4).HasColumnName("UI_Year4");
            this.Property(t => t.UISP_Year1).HasColumnName("UISP_Year1");
            this.Property(t => t.UISP_Year2).HasColumnName("UISP_Year2");
            this.Property(t => t.UISP_Year3).HasColumnName("UISP_Year3");
            this.Property(t => t.UISP_Year4).HasColumnName("UISP_Year4");
            this.Property(t => t.TOTAL_UI).HasColumnName("TOTAL_UI");
            this.Property(t => t.UIA_Year1).HasColumnName("UIA_Year1");
            this.Property(t => t.UIA_Year2).HasColumnName("UIA_Year2");
            this.Property(t => t.UIA_Year3).HasColumnName("UIA_Year3");
            this.Property(t => t.UIA_Year4).HasColumnName("UIA_Year4");
            this.Property(t => t.UIA_Amount1).HasColumnName("UIA_Amount1");
            this.Property(t => t.UIA_Amount2).HasColumnName("UIA_Amount2");
            this.Property(t => t.UIA_Amount3).HasColumnName("UIA_Amount3");
            this.Property(t => t.UIA_Amount4).HasColumnName("UIA_Amount4");
            this.Property(t => t.Percent_NII).HasColumnName("Percent_NII");
            this.Property(t => t.TotalDonations).HasColumnName("TotalDonations");
            this.Property(t => t.Dt990SentIRS).HasColumnName("Dt990SentIRS");
            this.Property(t => t.TaxPaymentQuarter).HasColumnName("TaxPaymentQuarter");
            this.Property(t => t.Responsible).HasColumnName("Responsible");
            this.Property(t => t.DtPostEntry).HasColumnName("DtPostEntry");
            this.Property(t => t.FiledByClient).HasColumnName("FiledByClient");
            this.Property(t => t.Outsourced990PF).HasColumnName("Outsourced990PF");
            this.Property(t => t.OutsourcingCompany).HasColumnName("OutsourcingCompany");
            this.Property(t => t.Q1TaxesResponsible).HasColumnName("Q1TaxesResponsible");
            this.Property(t => t.Q2TaxesResponsible).HasColumnName("Q2TaxesResponsible");
            this.Property(t => t.Q3TaxesResponsible).HasColumnName("Q3TaxesResponsible");
            this.Property(t => t.Q4TaxesResponsible).HasColumnName("Q4TaxesResponsible");
            this.Property(t => t.Q1TaxesAmount).HasColumnName("Q1TaxesAmount");
            this.Property(t => t.Q2TaxesAmount).HasColumnName("Q2TaxesAmount");
            this.Property(t => t.Q3TaxesAmount).HasColumnName("Q3TaxesAmount");
            this.Property(t => t.Q4TaxesAmount).HasColumnName("Q4TaxesAmount");
            this.Property(t => t.DtSentExtOnly1).HasColumnName("DtSentExtOnly1");
            this.Property(t => t.DtSentExtOnly2).HasColumnName("DtSentExtOnly2");
            this.Property(t => t.File1Ext).HasColumnName("File1Ext");
            this.Property(t => t.File2Ext).HasColumnName("File2Ext");
            this.Property(t => t.NII).HasColumnName("NII");
            this.Property(t => t.TaxesResponsibleNote).HasColumnName("TaxesResponsibleNote");
            this.Property(t => t.TaxesResponsibleState).HasColumnName("TaxesResponsibleState");
            this.Property(t => t.StateFilingNote).HasColumnName("StateFilingNote");
            this.Property(t => t.ResponsibleNote).HasColumnName("ResponsibleNote");
            this.Property(t => t.Q1TaxCalcMeth).HasColumnName("Q1TaxCalcMeth");
            this.Property(t => t.Q2TaxCalcMeth).HasColumnName("Q2TaxCalcMeth");
            this.Property(t => t.Q3TaxCalcMeth).HasColumnName("Q3TaxCalcMeth");
            this.Property(t => t.Q4TaxCalcMeth).HasColumnName("Q4TaxCalcMeth");
            this.Property(t => t.TaxRefundRequested).HasColumnName("TaxRefundRequested");
            this.Property(t => t.UndistributedIncome).HasColumnName("UndistributedIncome");
            this.Property(t => t.OutOfCorpusDistributions).HasColumnName("OutOfCorpusDistributions");
            this.Property(t => t.OnePercentTaxRate).HasColumnName("OnePercentTaxRate");
            this.Property(t => t.Req990T).HasColumnName("Req990T");
            this.Property(t => t.File990TExt1).HasColumnName("File990TExt1");
            this.Property(t => t.File990TExt2).HasColumnName("File990TExt2");
            this.Property(t => t.DtSent990TExtOnly1).HasColumnName("DtSent990TExtOnly1");
            this.Property(t => t.DtSent990TExtOnly2).HasColumnName("DtSent990TExtOnly2");
            this.Property(t => t.DtSent990TIRS).HasColumnName("DtSent990TIRS");
            this.Property(t => t.Req4720).HasColumnName("Req4720");
            this.Property(t => t.File4720Ext1).HasColumnName("File4720Ext1");
            this.Property(t => t.File4720Ext2).HasColumnName("File4720Ext2");
            this.Property(t => t.DtSent4720ExtOnly1).HasColumnName("DtSent4720ExtOnly1");
            this.Property(t => t.DtSent4720ExtOnly2).HasColumnName("DtSent4720ExtOnly2");
            this.Property(t => t.DtSent4720IRS).HasColumnName("DtSent4720IRS");
            this.Property(t => t.ReqAmendedReturn).HasColumnName("ReqAmendedReturn");
            this.Property(t => t.DtAmendedReturn).HasColumnName("DtAmendedReturn");
            this.Property(t => t.Line9PaymentDue).HasColumnName("Line9PaymentDue");
            this.Property(t => t.TimeStamp).HasColumnName("TimeStamp");
            this.Property(t => t.Line6AEstPymt).HasColumnName("Line6AEstPymt");
            this.Property(t => t.Line6CTaxPaid).HasColumnName("Line6CTaxPaid");
            this.Property(t => t.Line3CExtAmountDue).HasColumnName("Line3CExtAmountDue");
            this.Property(t => t.Line8CExtAmountDue).HasColumnName("Line8CExtAmountDue");
            this.Property(t => t.SpecialNote).HasColumnName("SpecialNote");

            // Relationships
            this.HasOptional(t => t.Company)
                .WithMany(t => t.DistributableAmounts)
                .HasForeignKey(d => d.OutsourcingCompany);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.DistributableAmounts)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
