using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class VotingOfferItemMap : EntityTypeConfiguration<VotingOfferItem>
    {
        public VotingOfferItemMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.EMail)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("VotingOfferItems");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.VotingOffer).HasColumnName("VotingOffer");
            this.Property(t => t.TheAction).HasColumnName("TheAction");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.EMail).HasColumnName("EMail");
            this.Property(t => t.Appointment).HasColumnName("Appointment");

            // Relationships
            this.HasRequired(t => t.VotingOffer1)
                .WithMany(t => t.VotingOfferItems)
                .HasForeignKey(d => d.VotingOffer);

        }
    }
}
