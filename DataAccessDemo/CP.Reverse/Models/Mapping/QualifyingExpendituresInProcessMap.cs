using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QualifyingExpendituresInProcessMap : EntityTypeConfiguration<QualifyingExpendituresInProcess>
    {
        public QualifyingExpendituresInProcessMap()
        {
            // Primary Key
            this.HasKey(t => new { t.DtCreated, t.Step, t.Category });

            // Properties
            this.Property(t => t.TypeName)
                .HasMaxLength(150);

            this.Property(t => t.Payee)
                .HasMaxLength(200);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Category)
                .IsRequired()
                .HasMaxLength(7);

            // Table & Column Mappings
            this.ToTable("QualifyingExpendituresInProcess");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.Payee).HasColumnName("Payee");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Category).HasColumnName("Category");
        }
    }
}
