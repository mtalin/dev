using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class UnallocatedSharesStepMap : EntityTypeConfiguration<UnallocatedSharesStep>
    {
        public UnallocatedSharesStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("UnallocatedSharesSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.QDeposit).HasColumnName("QDeposit");
            this.Property(t => t.TrSecurity).HasColumnName("TrSecurity");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.UnallocatedSharesSteps)
                .HasForeignKey(d => d.Message);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.UnallocatedSharesSteps)
                .HasForeignKey(d => d.Note);

        }
    }
}
