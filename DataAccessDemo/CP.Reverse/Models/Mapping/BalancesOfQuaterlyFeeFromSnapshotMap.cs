using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class BalancesOfQuaterlyFeeFromSnapshotMap : EntityTypeConfiguration<BalancesOfQuaterlyFeeFromSnapshot>
    {
        public BalancesOfQuaterlyFeeFromSnapshotMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("BalancesOfQuaterlyFeeFromSnapshot");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Cash).HasColumnName("Cash");
            this.Property(t => t.Security).HasColumnName("Security");
            this.Property(t => t.AltAsset).HasColumnName("AltAsset");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");
        }
    }
}
