using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EventActionMap : EntityTypeConfiguration<EventAction>
    {
        public EventActionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("EventActions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.EventClass).HasColumnName("EventClass");
            this.Property(t => t.JobClass).HasColumnName("JobClass");
            this.Property(t => t.Priority).HasColumnName("Priority");

            // Relationships
            this.HasRequired(t => t.EventClass1)
                .WithMany(t => t.EventActions)
                .HasForeignKey(d => d.EventClass);
            this.HasRequired(t => t.JobClass1)
                .WithMany(t => t.EventActions)
                .HasForeignKey(d => d.JobClass);

        }
    }
}
