using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DonationsLogMap : EntityTypeConfiguration<DonationsLog>
    {
        public DonationsLogMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.GrantPurpose)
                .HasMaxLength(10);

            this.Property(t => t.PurposeDescription)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("DonationsLog");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Donate).HasColumnName("Donate");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtChecked).HasColumnName("DtChecked");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.GrantPurpose).HasColumnName("GrantPurpose");
            this.Property(t => t.PurposeDescription).HasColumnName("PurposeDescription");
        }
    }
}
