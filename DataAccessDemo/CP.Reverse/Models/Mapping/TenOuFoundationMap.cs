using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TenOuFoundationMap : EntityTypeConfiguration<TenOuFoundation>
    {
        public TenOuFoundationMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Foundation, t.Company });

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Company)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TenOuFoundations");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Company).HasColumnName("Company");
        }
    }
}
