using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PersonsPositionMap : EntityTypeConfiguration<PersonsPosition>
    {
        public PersonsPositionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Company)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Appointment)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("PersonsPositions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Appointment).HasColumnName("Appointment");

            // Relationships
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.PersonsPositions)
                .HasForeignKey(d => d.Person);

        }
    }
}
