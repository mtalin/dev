using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EFTPSConfReportMap : EntityTypeConfiguration<EFTPSConfReport>
    {
        public EFTPSConfReportMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FileName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("EFTPSConfReports");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.dtCreated).HasColumnName("dtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.FileName).HasColumnName("FileName");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.TotalRecords).HasColumnName("TotalRecords");
            this.Property(t => t.IgnoredRecords).HasColumnName("IgnoredRecords");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.EFTPSConfReports)
                .HasForeignKey(d => d.Author);

        }
    }
}
