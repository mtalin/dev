using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CheckingAccountMap : EntityTypeConfiguration<CheckingAccount>
    {
        public CheckingAccountMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.BatchFilerID)
                .HasMaxLength(50);

            this.Property(t => t.MasterInquiryPIN)
                .HasMaxLength(50);

            this.Property(t => t.LinkedBankAddress)
                .HasMaxLength(500);

            this.Property(t => t.LinkedAccountName)
                .HasMaxLength(50);

            this.Property(t => t.LinkedAccountNumber)
                .HasMaxLength(22);

            this.Property(t => t.LinkedRoutingNumber)
                .HasMaxLength(22);

            // Table & Column Mappings
            this.ToTable("CheckingAccounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.BatchFilerID).HasColumnName("BatchFilerID");
            this.Property(t => t.MasterInquiryPIN).HasColumnName("MasterInquiryPIN");
            this.Property(t => t.AccountType).HasColumnName("AccountType");
            this.Property(t => t.LinkedBank).HasColumnName("LinkedBank");
            this.Property(t => t.LinkedBankAddress).HasColumnName("LinkedBankAddress");
            this.Property(t => t.LinkedAccountName).HasColumnName("LinkedAccountName");
            this.Property(t => t.LinkedAccountNumber).HasColumnName("LinkedAccountNumber");
            this.Property(t => t.LinkedRoutingNumber).HasColumnName("LinkedRoutingNumber");
            this.Property(t => t.NextCheckNo).HasColumnName("NextCheckNo");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CheckingAccounts)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.CheckingAccounts1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasOptional(t => t.ServiceProvider)
                .WithMany(t => t.CheckingAccounts)
                .HasForeignKey(d => d.LinkedBank);

        }
    }
}
