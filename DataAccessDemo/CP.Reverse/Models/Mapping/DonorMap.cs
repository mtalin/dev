using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DonorMap : EntityTypeConfiguration<Donor>
    {
        public DonorMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.InCareOf)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Donors");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DonorType).HasColumnName("DonorType");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.LastSpouse).HasColumnName("LastSpouse");
            this.Property(t => t.InCareOf).HasColumnName("InCareOf");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DeliveryMethod).HasColumnName("DeliveryMethod");

            // Relationships
            this.HasOptional(t => t.Company1)
                .WithMany(t => t.Donors)
                .HasForeignKey(d => d.Company);
            this.HasOptional(t => t.Donor1)
                .WithMany(t => t.Donors1)
                .HasForeignKey(d => d.LastSpouse);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.Donors)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Donors)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Person2)
                .WithMany(t => t.Donors1)
                .HasForeignKey(d => d.Person);

        }
    }
}
