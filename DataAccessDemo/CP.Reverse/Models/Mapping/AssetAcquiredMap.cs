using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AssetAcquiredMap : EntityTypeConfiguration<AssetAcquired>
    {
        public AssetAcquiredMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Transact, t.Asset, t.HowAcq });

            // Properties
            this.Property(t => t.Transact)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Asset)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.HowAcq)
                .IsRequired()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("AssetAcquired");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.HowAcq).HasColumnName("HowAcq");
            this.Property(t => t.DateAcq).HasColumnName("DateAcq");
            this.Property(t => t.Purchase).HasColumnName("Purchase");
            this.Property(t => t.Donate).HasColumnName("Donate");
            this.Property(t => t.Begining).HasColumnName("Begining");
        }
    }
}
