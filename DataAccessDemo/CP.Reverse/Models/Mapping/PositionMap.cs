using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PositionMap : EntityTypeConfiguration<Position>
    {
        public PositionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Rights)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Positions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Rights).HasColumnName("Rights");
            this.Property(t => t.Priority).HasColumnName("Priority");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Positions)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
