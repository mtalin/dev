using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PersonRoleMap : EntityTypeConfiguration<PersonRole>
    {
        public PersonRoleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Rights)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(255);

            this.Property(t => t.DisplayingName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("PersonRoles");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.RoleCode).HasColumnName("RoleCode");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Partner).HasColumnName("Partner");
            this.Property(t => t.Rights).HasColumnName("Rights");
            this.Property(t => t.DisplayingName).HasColumnName("DisplayingName");
            this.Property(t => t.Foundation).HasColumnName("Foundation");

            // Relationships
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.PersonRoles)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
