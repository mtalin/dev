using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class enums_FoundationAccountTypeMap : EntityTypeConfiguration<enums_FoundationAccountType>
    {
        public enums_FoundationAccountTypeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Category, t.ItemID, t.Name });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Category)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ItemID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Text)
                .HasMaxLength(300);

            this.Property(t => t.Abbrev)
                .HasMaxLength(50);

            this.Property(t => t.TheValue)
                .HasMaxLength(150);

            this.Property(t => t.Tag)
                .HasMaxLength(256);

            // Table & Column Mappings
            this.ToTable("enums_FoundationAccountType");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.Abbrev).HasColumnName("Abbrev");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.TheValue).HasColumnName("TheValue");
            this.Property(t => t.Tag).HasColumnName("Tag");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
        }
    }
}
