using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FoundationEventMap : EntityTypeConfiguration<FoundationEvent>
    {
        public FoundationEventMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Note)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("FoundationEvent");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.BeginDate).HasColumnName("BeginDate");
            this.Property(t => t.BeginBy).HasColumnName("BeginBy");
            this.Property(t => t.CompleteDate).HasColumnName("CompleteDate");
            this.Property(t => t.CompleteBy).HasColumnName("CompleteBy");
        }
    }
}
