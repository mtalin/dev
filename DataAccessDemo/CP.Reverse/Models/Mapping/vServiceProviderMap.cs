using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vServiceProviderMap : EntityTypeConfiguration<vServiceProvider>
    {
        public vServiceProviderMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.StructuredCompany, t.SingleSignOn, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.URL)
                .HasMaxLength(500);

            this.Property(t => t.Design)
                .HasMaxLength(100);

            this.Property(t => t.Abbrev)
                .HasMaxLength(20);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Fax)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("vServiceProviders");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.SingleSignOn).HasColumnName("SingleSignOn");
            this.Property(t => t.Design).HasColumnName("Design");
            this.Property(t => t.CustomerServiceContacts).HasColumnName("CustomerServiceContacts");
            this.Property(t => t.CustomContactsHTML).HasColumnName("CustomContactsHTML");
            this.Property(t => t.Abbrev).HasColumnName("Abbrev");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");
        }
    }
}
