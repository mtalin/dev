using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vFoundationContractMap : EntityTypeConfiguration<vFoundationContract>
    {
        public vFoundationContractMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.ServiceProvider, t.IsActive, t.IsMainContract });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ServiceProvider)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsMainContract)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vFoundationContracts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.DtCommence).HasColumnName("DtCommence");
            this.Property(t => t.DtTerminate).HasColumnName("DtTerminate");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.ParentContract).HasColumnName("ParentContract");
            this.Property(t => t.IsMainContract).HasColumnName("IsMainContract");
        }
    }
}
