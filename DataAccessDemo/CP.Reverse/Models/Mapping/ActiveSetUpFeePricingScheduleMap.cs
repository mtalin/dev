using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ActiveSetUpFeePricingScheduleMap : EntityTypeConfiguration<ActiveSetUpFeePricingSchedule>
    {
        public ActiveSetUpFeePricingScheduleMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Contract, t.FeeType });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Contract)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FeeType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ActiveSetUpFeePricingSchedules");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.BillTo).HasColumnName("BillTo");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.FeeType).HasColumnName("FeeType");
        }
    }
}
