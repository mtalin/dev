using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ToDoNoteMap : EntityTypeConfiguration<ToDoNote>
    {
        public ToDoNoteMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Text)
                .IsRequired()
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("ToDoNotes");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ToDo).HasColumnName("ToDo");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ToDoNotes)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.ToDo1)
                .WithMany(t => t.ToDoNotes)
                .HasForeignKey(d => d.ToDo);

        }
    }
}
