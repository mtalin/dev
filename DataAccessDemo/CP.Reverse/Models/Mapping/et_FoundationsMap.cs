using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_FoundationsMap : EntityTypeConfiguration<et_Foundations>
    {
        public et_FoundationsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Name, t.isSSORequired, t.DesignCode });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.DesignCode)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("et_Foundations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Founder).HasColumnName("Founder");
            this.Property(t => t.President).HasColumnName("President");
            this.Property(t => t.isSSORequired).HasColumnName("isSSORequired");
            this.Property(t => t.FoundationRep).HasColumnName("FoundationRep");
            this.Property(t => t.RelationshipManager).HasColumnName("RelationshipManager");
            this.Property(t => t.DesignCode).HasColumnName("DesignCode");
            this.Property(t => t.SalesRep).HasColumnName("SalesRep");
            this.Property(t => t.PrimaryTaxMailing).HasColumnName("PrimaryTaxMailing");
            this.Property(t => t.SecondaryTaxMailing).HasColumnName("SecondaryTaxMailing");
            this.Property(t => t.PrimaryExpReimbursement).HasColumnName("PrimaryExpReimbursement");
            this.Property(t => t.SecondaryExpReimbursement).HasColumnName("SecondaryExpReimbursement");
        }
    }
}
