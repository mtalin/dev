using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PersonRolesHistoryMap : EntityTypeConfiguration<PersonRolesHistory>
    {
        public PersonRolesHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.OldName)
                .HasMaxLength(50);

            this.Property(t => t.Rights)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(255);

            this.Property(t => t.OldRights)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("PersonRolesHistory");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.PersonRole).HasColumnName("PersonRole");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.ApplyType).HasColumnName("ApplyType");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.OldName).HasColumnName("OldName");
            this.Property(t => t.Rights).HasColumnName("Rights");
            this.Property(t => t.OldRights).HasColumnName("OldRights");

            // Relationships
            this.HasRequired(t => t.PersonRole1)
                .WithMany(t => t.PersonRolesHistories)
                .HasForeignKey(d => d.PersonRole);

        }
    }
}
