using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DonationRequestMap : EntityTypeConfiguration<DonationRequest>
    {
        public DonationRequestMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.SpecPurposeText1)
                .HasMaxLength(200);

            this.Property(t => t.SpecPurposeText2)
                .HasMaxLength(200);

            this.Property(t => t.ContactFirstName)
                .HasMaxLength(50);

            this.Property(t => t.ContactMiddleName)
                .HasMaxLength(50);

            this.Property(t => t.ContactLastName)
                .HasMaxLength(50);

            this.Property(t => t.ContactTitle)
                .HasMaxLength(70);

            this.Property(t => t.ContactSalutation)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("DonationRequests");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.GrantRequestId).HasColumnName("GrantRequestId");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.SpecPurposeType).HasColumnName("SpecPurposeType");
            this.Property(t => t.SpecPurposeText1).HasColumnName("SpecPurposeText1");
            this.Property(t => t.SpecPurposeText2).HasColumnName("SpecPurposeText2");
            this.Property(t => t.ContactFirstName).HasColumnName("ContactFirstName");
            this.Property(t => t.ContactMiddleName).HasColumnName("ContactMiddleName");
            this.Property(t => t.ContactLastName).HasColumnName("ContactLastName");
            this.Property(t => t.ContactTitle).HasColumnName("ContactTitle");
            this.Property(t => t.ContactSalutation).HasColumnName("ContactSalutation");
            this.Property(t => t.HouseholdAccount).HasColumnName("HouseholdAccount");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.DonationRequests)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.HouseholdAccount1)
                .WithMany(t => t.DonationRequests)
                .HasForeignKey(d => d.HouseholdAccount);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.DonationRequests)
                .HasForeignKey(d => d.Author);

        }
    }
}
