using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_EstimatedTaxesMap : EntityTypeConfiguration<et_EstimatedTaxes>
    {
        public et_EstimatedTaxesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Quarter, t.dtStartPeriod, t.Year, t.IsFullYear, t.Method });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Quarter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Year)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FullNameQuarter)
                .HasMaxLength(14);

            this.Property(t => t.IsFullYear)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Method)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("et_EstimatedTaxes");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.NIIExcCapGain).HasColumnName("NIIExcCapGain");
            this.Property(t => t.AnnualizedNIIExcCapGain).HasColumnName("AnnualizedNIIExcCapGain");
            this.Property(t => t.CapitalGains).HasColumnName("CapitalGains");
            this.Property(t => t.AnnualizedIncome).HasColumnName("AnnualizedIncome");
            this.Property(t => t.AnnualizedTaxLiability).HasColumnName("AnnualizedTaxLiability");
            this.Property(t => t.QuarterlyTaxPayment).HasColumnName("QuarterlyTaxPayment");
            this.Property(t => t.Paid).HasColumnName("Paid");
            this.Property(t => t.ActEstTaxPayment).HasColumnName("ActEstTaxPayment");
            this.Property(t => t.RoundedPayment).HasColumnName("RoundedPayment");
            this.Property(t => t.Month).HasColumnName("Month");
            this.Property(t => t.Quarter).HasColumnName("Quarter");
            this.Property(t => t.Factor).HasColumnName("Factor");
            this.Property(t => t.dtStartPeriod).HasColumnName("dtStartPeriod");
            this.Property(t => t.dtEndPeriod).HasColumnName("dtEndPeriod");
            this.Property(t => t.Tax).HasColumnName("Tax");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.FullNameQuarter).HasColumnName("FullNameQuarter");
            this.Property(t => t.Percent).HasColumnName("Percent");
            this.Property(t => t.IntIncome).HasColumnName("IntIncome");
            this.Property(t => t.DivIncome).HasColumnName("DivIncome");
            this.Property(t => t.AssetManagementFees).HasColumnName("AssetManagementFees");
            this.Property(t => t.NetInvIncome).HasColumnName("NetInvIncome");
            this.Property(t => t.IsFullYear).HasColumnName("IsFullYear");
            this.Property(t => t.Method).HasColumnName("Method");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.FiscalYearEnd).HasColumnName("FiscalYearEnd");
            this.Property(t => t.TaxYearEnd).HasColumnName("TaxYearEnd");
            this.Property(t => t.Line11Income).HasColumnName("Line11Income");
            this.Property(t => t.PartnershipIncome).HasColumnName("PartnershipIncome");
        }
    }
}
