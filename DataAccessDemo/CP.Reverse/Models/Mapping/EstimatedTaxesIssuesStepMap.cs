using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EstimatedTaxesIssuesStepMap : EntityTypeConfiguration<EstimatedTaxesIssuesStep>
    {
        public EstimatedTaxesIssuesStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("EstimatedTaxesIssuesSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.EstimatedTaxIssue).HasColumnName("EstimatedTaxIssue");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.PrevStep).HasColumnName("PrevStep");

            // Relationships
            this.HasRequired(t => t.EstimatedTaxesIssue)
                .WithMany(t => t.EstimatedTaxesIssuesSteps)
                .HasForeignKey(d => d.EstimatedTaxIssue);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.EstimatedTaxesIssuesSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
