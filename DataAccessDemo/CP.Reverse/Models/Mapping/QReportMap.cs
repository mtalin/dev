using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QReportMap : EntityTypeConfiguration<QReport>
    {
        public QReportMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.SourceName)
                .HasMaxLength(100);

            this.Property(t => t.Note)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("QReports");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.QParser).HasColumnName("QParser");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtProcessed).HasColumnName("DtProcessed");
            this.Property(t => t.TotalRecords).HasColumnName("TotalRecords");
            this.Property(t => t.IgnoredRecords).HasColumnName("IgnoredRecords");
            this.Property(t => t.Source).HasColumnName("Source");
            this.Property(t => t.DtReport).HasColumnName("DtReport");
            this.Property(t => t.SourceName).HasColumnName("SourceName");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.QReports)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.QParser1)
                .WithMany(t => t.QReports)
                .HasForeignKey(d => d.QParser);

        }
    }
}
