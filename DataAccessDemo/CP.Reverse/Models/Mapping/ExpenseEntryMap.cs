using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ExpenseEntryMap : EntityTypeConfiguration<ExpenseEntry>
    {
        public ExpenseEntryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.CityFrom)
                .HasMaxLength(50);

            this.Property(t => t.StateFrom)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.CityTo)
                .HasMaxLength(50);

            this.Property(t => t.StateTo)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ExpenseEntries");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Expense).HasColumnName("Expense");
            this.Property(t => t.Officer).HasColumnName("Officer");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.DtFrom).HasColumnName("DtFrom");
            this.Property(t => t.DtTo).HasColumnName("DtTo");
            this.Property(t => t.CityFrom).HasColumnName("CityFrom");
            this.Property(t => t.StateFrom).HasColumnName("StateFrom");
            this.Property(t => t.CityTo).HasColumnName("CityTo");
            this.Property(t => t.StateTo).HasColumnName("StateTo");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.IDCConfirm).HasColumnName("IDCConfirm");

            // Relationships
            this.HasRequired(t => t.Expens)
                .WithMany(t => t.ExpenseEntries)
                .HasForeignKey(d => d.Expense);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ExpenseEntries)
                .HasForeignKey(d => d.Officer);

        }
    }
}
