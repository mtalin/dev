using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DefaultCharityMap : EntityTypeConfiguration<DefaultCharity>
    {
        public DefaultCharityMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Foundation, t.CharityEIN });

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CharityEIN)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("DefaultCharities");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.DonatePercent).HasColumnName("DonatePercent");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.DefaultCharities)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
