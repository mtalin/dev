using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FoundationsBoardMeetingMap : EntityTypeConfiguration<FoundationsBoardMeeting>
    {
        public FoundationsBoardMeetingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("FoundationsBoardMeetings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.MeetingMonth).HasColumnName("MeetingMonth");
            this.Property(t => t.MeetingDay).HasColumnName("MeetingDay");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.FoundationsBoardMeetings)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
