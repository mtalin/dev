using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_DonationCertificatesMap : EntityTypeConfiguration<et_DonationCertificates>
    {
        public et_DonationCertificatesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FoundationName, t.InitialAmount, t.Author, t.Recipient, t.isExpired, t.SingleGrant });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.DateCreated)
                .HasMaxLength(30);

            this.Property(t => t.InitialAmount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Recipient)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.isExpired)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DtExpiration)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("et_DonationCertificates");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.InitialAmount).HasColumnName("InitialAmount");
            this.Property(t => t.AmountLeft).HasColumnName("AmountLeft");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Recipient).HasColumnName("Recipient");
            this.Property(t => t.isExpired).HasColumnName("isExpired");
            this.Property(t => t.DtExpiration).HasColumnName("DtExpiration");
            this.Property(t => t.SingleGrant).HasColumnName("SingleGrant");
        }
    }
}
