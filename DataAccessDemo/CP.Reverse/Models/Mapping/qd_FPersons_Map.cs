using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class qd_FPersons_Map : EntityTypeConfiguration<qd_FPersons_>
    {
        public qd_FPersons_Map()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(500);

            this.Property(t => t.Address)
                .HasMaxLength(500);

            this.Property(t => t.Hours)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("qd_FPersons_");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.dtCreated).HasColumnName("dtCreated");
            this.Property(t => t.Questionnaire).HasColumnName("Questionnaire");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Excluded).HasColumnName("Excluded");
            this.Property(t => t.Hours).HasColumnName("Hours");
            this.Property(t => t.Comments).HasColumnName("Comments");
        }
    }
}
