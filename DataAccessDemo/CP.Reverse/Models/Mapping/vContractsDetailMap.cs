using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vContractsDetailMap : EntityTypeConfiguration<vContractsDetail>
    {
        public vContractsDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.ServiceProvider, t.IsActive, t.IsTemplate, t.Author, t.DtCreated, t.HasSubContracts });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ContractNo)
                .HasMaxLength(255);

            this.Property(t => t.ServiceProvider)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ParentContractNo)
                .HasMaxLength(255);

            this.Property(t => t.AuthorName)
                .HasMaxLength(200);

            this.Property(t => t.ServiceProviderName)
                .HasMaxLength(500);

            this.Property(t => t.CustomerName)
                .HasMaxLength(500);

            this.Property(t => t.RelationshipManagerName)
                .HasMaxLength(200);

            this.Property(t => t.HasSubContracts)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vContractsDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ContractNo).HasColumnName("ContractNo");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.Customer).HasColumnName("Customer");
            this.Property(t => t.ParentContract).HasColumnName("ParentContract");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.IsTemplate).HasColumnName("IsTemplate");
            this.Property(t => t.DtCommence).HasColumnName("DtCommence");
            this.Property(t => t.DtTerminate).HasColumnName("DtTerminate");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtActivated).HasColumnName("DtActivated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.ParentContractNo).HasColumnName("ParentContractNo");
            this.Property(t => t.AuthorName).HasColumnName("AuthorName");
            this.Property(t => t.ServiceProviderName).HasColumnName("ServiceProviderName");
            this.Property(t => t.CustomerName).HasColumnName("CustomerName");
            this.Property(t => t.RelationshipManager).HasColumnName("RelationshipManager");
            this.Property(t => t.RelationshipManagerName).HasColumnName("RelationshipManagerName");
            this.Property(t => t.HasSubContracts).HasColumnName("HasSubContracts");
        }
    }
}
