using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EventClassMap : EntityTypeConfiguration<EventClass>
    {
        public EventClassMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.EventData)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("EventClasses");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.EventData).HasColumnName("EventData");
            this.Property(t => t.EventSource).HasColumnName("EventSource");
            this.Property(t => t.EventType).HasColumnName("EventType");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.EventSource1)
                .WithMany(t => t.EventClasses)
                .HasForeignKey(d => d.EventSource);
            this.HasRequired(t => t.EventType1)
                .WithMany(t => t.EventClasses)
                .HasForeignKey(d => d.EventType);

        }
    }
}
