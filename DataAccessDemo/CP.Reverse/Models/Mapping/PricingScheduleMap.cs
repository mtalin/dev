using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PricingScheduleMap : EntityTypeConfiguration<PricingSchedule>
    {
        public PricingScheduleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.ScheduleNumber)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("PricingSchedules");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.ScheduleNumber).HasColumnName("ScheduleNumber");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.PricingSource).HasColumnName("PricingSource");
            this.Property(t => t.BillingEvent).HasColumnName("BillingEvent");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.PricingSchedules)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.PricingSchedules1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasRequired(t => t.ServiceProvider1)
                .WithMany(t => t.PricingSchedules)
                .HasForeignKey(d => d.ServiceProvider);

        }
    }
}
