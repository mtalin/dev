using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ExpenseLocationMap : EntityTypeConfiguration<ExpenseLocation>
    {
        public ExpenseLocationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Location)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ExpenseLocations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.DtFrom).HasColumnName("DtFrom");
            this.Property(t => t.DtTo).HasColumnName("DtTo");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
