using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_IrsFilesMap : EntityTypeConfiguration<et_IrsFiles>
    {
        public et_IrsFilesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("et_IrsFiles");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IrsInstructionDate).HasColumnName("IrsInstructionDate");
            this.Property(t => t.IrsReleaseDate).HasColumnName("IrsReleaseDate");
            this.Property(t => t.IrsRecordsNumber).HasColumnName("IrsRecordsNumber");
        }
    }
}
