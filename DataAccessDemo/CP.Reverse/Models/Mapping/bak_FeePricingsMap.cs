using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class bak_FeePricingsMap : EntityTypeConfiguration<bak_FeePricings>
    {
        public bak_FeePricingsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.PricingScheduleInstance, t.FeeType, t.Amount, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PricingScheduleInstance)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FeeType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("bak_FeePricings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PricingScheduleInstance).HasColumnName("PricingScheduleInstance");
            this.Property(t => t.FeeType).HasColumnName("FeeType");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
        }
    }
}
