using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class StructuredCompanyTypesRightMap : EntityTypeConfiguration<StructuredCompanyTypesRight>
    {
        public StructuredCompanyTypesRightMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Permissions)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.AppliedPermissions)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.InheritedPermissions)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("StructuredCompanyTypesRights", "rpt");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Permissions).HasColumnName("Permissions");
            this.Property(t => t.AppliedPermissions).HasColumnName("AppliedPermissions");
            this.Property(t => t.InheritedPermissions).HasColumnName("InheritedPermissions");
        }
    }
}
