using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vObjectsPermissionsWithObjectMap : EntityTypeConfiguration<vObjectsPermissionsWithObject>
    {
        public vObjectsPermissionsWithObjectMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.Object, t.AppliedPermissions });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Object)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.AppliedPermissions)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.ResolvedPermissions)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("vObjectsPermissionsWithObjects");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Object).HasColumnName("Object");
            this.Property(t => t.AppliedPermissions).HasColumnName("AppliedPermissions");
            this.Property(t => t.ResolvedPermissions).HasColumnName("ResolvedPermissions");
            this.Property(t => t.NoteText).HasColumnName("NoteText");
            this.Property(t => t.NoteAuthor).HasColumnName("NoteAuthor");
            this.Property(t => t.ObjectId).HasColumnName("ObjectId");
        }
    }
}
