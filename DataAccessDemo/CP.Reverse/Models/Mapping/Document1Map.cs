using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class Document1Map : EntityTypeConfiguration<Document1>
    {
        public Document1Map()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Extention)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(400);

            this.Property(t => t.FileName)
                .HasMaxLength(256);

            this.Property(t => t.Body)
                .IsRequired();

            this.Property(t => t.Notes)
                .HasMaxLength(400);

            // Table & Column Mappings
            this.ToTable("Documents");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.ShellCorporation).HasColumnName("ShellCorporation");
            this.Property(t => t.Theme).HasColumnName("Theme");
            this.Property(t => t.Extention).HasColumnName("Extention");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.FileName).HasColumnName("FileName");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");
            this.Property(t => t.PeriodType).HasColumnName("PeriodType");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtReceived).HasColumnName("DtReceived");
            this.Property(t => t.DtBookMark).HasColumnName("DtBookMark");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.XMLData).HasColumnName("XMLData");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.CharityEdit).HasColumnName("CharityEdit");
            this.Property(t => t.IsUploaded).HasColumnName("IsUploaded");

            // Relationships
            this.HasMany(t => t.TaxReceipts)
                .WithMany(t => t.Documents)
                .Map(m =>
                    {
                        m.ToTable("TaxReceiptDocuments");
                        m.MapLeftKey("Document");
                        m.MapRightKey("TaxReceipt");
                    });

            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.Documents)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.ShellCorporation1)
                .WithMany(t => t.Documents)
                .HasForeignKey(d => d.ShellCorporation);

        }
    }
}
