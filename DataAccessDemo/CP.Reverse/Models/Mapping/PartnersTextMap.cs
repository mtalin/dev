using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PartnersTextMap : EntityTypeConfiguration<PartnersText>
    {
        public PartnersTextMap()
        {
            // Primary Key
            this.HasKey(t => new { t.FinancialPartner, t.Name });

            // Properties
            this.Property(t => t.FinancialPartner)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.TextValue)
                .IsRequired()
                .HasMaxLength(7900);

            // Table & Column Mappings
            this.ToTable("PartnersTexts");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.TextValue).HasColumnName("TextValue");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");

            // Relationships
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.PartnersTexts)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
