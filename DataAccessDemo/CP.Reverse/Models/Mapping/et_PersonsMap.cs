using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_PersonsMap : EntityTypeConfiguration<et_Persons>
    {
        public et_PersonsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FirstName, t.LastName, t.LoginName, t.Password, t.IsFoundationOfficer, t.OnlineAccess });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NamePrefix)
                .HasMaxLength(150);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleInitial)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.NameSuffix)
                .HasMaxLength(150);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Email2)
                .HasMaxLength(50);

            this.Property(t => t.Email3)
                .HasMaxLength(50);

            this.Property(t => t.BusinessPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.HomePhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.LoginName)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Password)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.FullName)
                .HasMaxLength(200);

            this.Property(t => t.IsFoundationOfficer)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Dear)
                .HasMaxLength(100);

            this.Property(t => t.CellPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.OtherPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.AsstPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("et_Persons");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.NamePrefix).HasColumnName("NamePrefix");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.MiddleInitial).HasColumnName("MiddleInitial");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.NameSuffix).HasColumnName("NameSuffix");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Email2).HasColumnName("Email2");
            this.Property(t => t.Email3).HasColumnName("Email3");
            this.Property(t => t.BusinessPhone).HasColumnName("BusinessPhone");
            this.Property(t => t.HomePhone).HasColumnName("HomePhone");
            this.Property(t => t.LoginName).HasColumnName("LoginName");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.FullName).HasColumnName("FullName");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.IsFoundationOfficer).HasColumnName("IsFoundationOfficer");
            this.Property(t => t.OnlineAccess).HasColumnName("OnlineAccess");
            this.Property(t => t.Dear).HasColumnName("Dear");
            this.Property(t => t.CellPhone).HasColumnName("CellPhone");
            this.Property(t => t.OtherPhone).HasColumnName("OtherPhone");
            this.Property(t => t.AsstPhone).HasColumnName("AsstPhone");
        }
    }
}
