using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class StaffingPoliciesTemplateMap : EntityTypeConfiguration<StaffingPoliciesTemplate>
    {
        public StaffingPoliciesTemplateMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("StaffingPoliciesTemplates");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.CompanyType).HasColumnName("CompanyType");
            this.Property(t => t.SystemPosition).HasColumnName("SystemPosition");
            this.Property(t => t.MinInstances).HasColumnName("MinInstances");
            this.Property(t => t.MaxInstances).HasColumnName("MaxInstances");
            this.Property(t => t.PeriodType).HasColumnName("PeriodType");
            this.Property(t => t.ExpirationPeriod).HasColumnName("ExpirationPeriod");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");

            // Relationships
            this.HasOptional(t => t.ObjectsPermission)
                .WithMany(t => t.StaffingPoliciesTemplates)
                .HasForeignKey(d => d.ObjectPermissions);
            this.HasRequired(t => t.SystemPosition1)
                .WithMany(t => t.StaffingPoliciesTemplates)
                .HasForeignKey(d => d.SystemPosition);

        }
    }
}
