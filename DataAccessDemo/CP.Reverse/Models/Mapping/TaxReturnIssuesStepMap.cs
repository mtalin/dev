using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReturnIssuesStepMap : EntityTypeConfiguration<TaxReturnIssuesStep>
    {
        public TaxReturnIssuesStepMap()
        {
            // Primary Key
            this.HasKey(t => new { t.TaxReturnIssue, t.DtCreated, t.Author, t.Step });

            // Properties
            this.Property(t => t.TaxReturnIssue)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TaxReturnIssuesSteps");
            this.Property(t => t.TaxReturnIssue).HasColumnName("TaxReturnIssue");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.PrevStep).HasColumnName("PrevStep");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.TaxReturnIssuesSteps)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.TaxReturnIssue1)
                .WithMany(t => t.TaxReturnIssuesSteps)
                .HasForeignKey(d => d.TaxReturnIssue);

        }
    }
}
