using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class InvoicesItemMap : EntityTypeConfiguration<InvoicesItem>
    {
        public InvoicesItemMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("InvoicesItems");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Invoice).HasColumnName("Invoice");
            this.Property(t => t.Tax).HasColumnName("Tax");

            // Relationships
            this.HasRequired(t => t.Invoice1)
                .WithMany(t => t.InvoicesItems)
                .HasForeignKey(d => d.Invoice);
            this.HasRequired(t => t.Tax1)
                .WithMany(t => t.InvoicesItems)
                .HasForeignKey(d => d.Tax);

        }
    }
}
