using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class StaffingPolicyMap : EntityTypeConfiguration<StaffingPolicy>
    {
        public StaffingPolicyMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("StaffingPolicies");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.SystemPosition).HasColumnName("SystemPosition");
            this.Property(t => t.MinInstances).HasColumnName("MinInstances");
            this.Property(t => t.MaxInstances).HasColumnName("MaxInstances");
            this.Property(t => t.PeriodType).HasColumnName("PeriodType");
            this.Property(t => t.ExpirationPeriod).HasColumnName("ExpirationPeriod");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.CompanyElement)
                .WithOptional(t => t.StaffingPolicy);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.StaffingPolicies)
                .HasForeignKey(d => d.DeletedBy);
            this.HasRequired(t => t.StructuredCompany1)
                .WithMany(t => t.StaffingPolicies)
                .HasForeignKey(d => d.StructuredCompany);
            this.HasRequired(t => t.SystemPosition1)
                .WithMany(t => t.StaffingPolicies)
                .HasForeignKey(d => d.SystemPosition);

        }
    }
}
