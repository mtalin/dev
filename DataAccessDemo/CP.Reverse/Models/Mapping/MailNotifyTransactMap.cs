using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MailNotifyTransactMap : EntityTypeConfiguration<MailNotifyTransact>
    {
        public MailNotifyTransactMap()
        {
            // Primary Key
            this.HasKey(t => t.Task);

            // Properties
            this.Property(t => t.Task)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Address)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.City)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ZIP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("MailNotifyTransact");
            this.Property(t => t.Task).HasColumnName("Task");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZIP).HasColumnName("ZIP");
            this.Property(t => t.Confirmation).HasColumnName("Confirmation");

            // Relationships
            this.HasRequired(t => t.Tasks_Old)
                .WithOptional(t => t.MailNotifyTransact);

        }
    }
}
