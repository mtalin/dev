using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ServicePackageMap : EntityTypeConfiguration<ServicePackage>
    {
        public ServicePackageMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ServicePackages");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ServicePackages)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.ServicePackages1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasRequired(t => t.ServiceProvider1)
                .WithMany(t => t.ServicePackages)
                .HasForeignKey(d => d.ServiceProvider);

        }
    }
}
