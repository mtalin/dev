using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CMSManagerMap : EntityTypeConfiguration<CMSManager>
    {
        public CMSManagerMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleInitial)
                .HasMaxLength(15);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Company)
                .HasMaxLength(100);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.BusinessPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.BusinessFax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Description)
                .IsRequired();

            this.Property(t => t.PictureType)
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("CMSManagers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.NamePrefix).HasColumnName("NamePrefix");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.MiddleInitial).HasColumnName("MiddleInitial");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Role).HasColumnName("Role");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.BusinessPhone).HasColumnName("BusinessPhone");
            this.Property(t => t.BusinessFax).HasColumnName("BusinessFax");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Picture).HasColumnName("Picture");
            this.Property(t => t.PictureType).HasColumnName("PictureType");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CMSManagers)
                .HasForeignKey(d => d.Author);

        }
    }
}
