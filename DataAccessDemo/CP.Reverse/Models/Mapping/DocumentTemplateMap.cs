using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DocumentTemplateMap : EntityTypeConfiguration<DocumentTemplate>
    {
        public DocumentTemplateMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(400);

            this.Property(t => t.Text)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("DocumentTemplates");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Format).HasColumnName("Format");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Cache).HasColumnName("Cache");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.DocumentTemplates)
                .HasForeignKey(d => d.Author);

        }
    }
}
