using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class NoteCategoryMap : EntityTypeConfiguration<NoteCategory>
    {
        public NoteCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Flags)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("NoteCategories");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.Flags).HasColumnName("Flags");
            this.Property(t => t.Section).HasColumnName("Section");
        }
    }
}
