using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class SystemRecipientMap : EntityTypeConfiguration<SystemRecipient>
    {
        public SystemRecipientMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Address)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.Flags)
                .HasMaxLength(200);

            this.Property(t => t.TagName)
                .IsRequired()
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("SystemRecipients");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Flags).HasColumnName("Flags");
            this.Property(t => t.TagName).HasColumnName("TagName");

            // Relationships
            this.HasRequired(t => t.Recipient)
                .WithOptional(t => t.SystemRecipient);

        }
    }
}
