using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TransferGroupMap : EntityTypeConfiguration<TransferGroup>
    {
        public TransferGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TransferGroups");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtSent).HasColumnName("DtSent");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.FinancialPartnerAccount).HasColumnName("FinancialPartnerAccount");
            this.Property(t => t.ACHStatus).HasColumnName("ACHStatus");
            this.Property(t => t.Signer).HasColumnName("Signer");
            this.Property(t => t.TransferProfile).HasColumnName("TransferProfile");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.IsModified).HasColumnName("IsModified");
            this.Property(t => t.IsRevised).HasColumnName("IsRevised");
            this.Property(t => t.DtResend).HasColumnName("DtResend");
            this.Property(t => t.IsSpecialHandling).HasColumnName("IsSpecialHandling");
            this.Property(t => t.IsUploaded).HasColumnName("IsUploaded");

            // Relationships
            this.HasOptional(t => t.Document1)
                .WithMany(t => t.TransferGroups)
                .HasForeignKey(d => d.Document);
            this.HasRequired(t => t.FoundationAccount1)
                .WithMany(t => t.TransferGroups)
                .HasForeignKey(d => d.FoundationAccount);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.TransferGroups)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.TransferGroups)
                .HasForeignKey(d => d.Signer);
            this.HasOptional(t => t.TransferProfile1)
                .WithMany(t => t.TransferGroups)
                .HasForeignKey(d => d.TransferProfile);

        }
    }
}
