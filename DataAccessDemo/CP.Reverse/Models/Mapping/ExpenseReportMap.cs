using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ExpenseReportMap : EntityTypeConfiguration<ExpenseReport>
    {
        public ExpenseReportMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.SpecInstruct)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ExpenseReports");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.ReimbursableTo).HasColumnName("ReimbursableTo");
            this.Property(t => t.ReimbursableContactInfo).HasColumnName("ReimbursableContactInfo");
            this.Property(t => t.Report).HasColumnName("Report");
            this.Property(t => t.Coversheet).HasColumnName("Coversheet");
            this.Property(t => t.SpecInstruct).HasColumnName("SpecInstruct");
            this.Property(t => t.IsSentEmail).HasColumnName("IsSentEmail");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");

            // Relationships
            this.HasOptional(t => t.Document)
                .WithMany(t => t.ExpenseReports)
                .HasForeignKey(d => d.Coversheet);
            this.HasOptional(t => t.Document1)
                .WithMany(t => t.ExpenseReports1)
                .HasForeignKey(d => d.Report);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ExpenseReports)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.ExpenseReports)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.PostalAddress)
                .WithMany(t => t.ExpenseReports)
                .HasForeignKey(d => d.ReimbursableContactInfo);
            this.HasRequired(t => t.PayeeProfile)
                .WithMany(t => t.ExpenseReports)
                .HasForeignKey(d => d.ReimbursableTo);

        }
    }
}
