using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FinancialAdvisorGroupMap : EntityTypeConfiguration<FinancialAdvisorGroup>
    {
        public FinancialAdvisorGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.PrimaryContact)
                .HasMaxLength(50);

            this.Property(t => t.Phone1)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Phone2)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Fax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Info)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("FinancialAdvisorGroups");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.PrimaryContact).HasColumnName("PrimaryContact");
            this.Property(t => t.Phone1).HasColumnName("Phone1");
            this.Property(t => t.Phone2).HasColumnName("Phone2");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Info).HasColumnName("Info");
            this.Property(t => t.Design).HasColumnName("Design");
            this.Property(t => t.Department).HasColumnName("Department");

            // Relationships
            this.HasOptional(t => t.PostalAddress1)
                .WithMany(t => t.FinancialAdvisorGroups)
                .HasForeignKey(d => d.PostalAddress);

        }
    }
}
