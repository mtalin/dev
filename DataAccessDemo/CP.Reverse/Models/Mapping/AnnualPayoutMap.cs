using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AnnualPayoutMap : EntityTypeConfiguration<AnnualPayout>
    {
        public AnnualPayoutMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("AnnualPayouts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.TheYear).HasColumnName("TheYear");
            this.Property(t => t.MIR).HasColumnName("MIR");
            this.Property(t => t.DA).HasColumnName("DA");
            this.Property(t => t.DA_PlusEGC).HasColumnName("DA_PlusEGC");
            this.Property(t => t.EGC).HasColumnName("EGC");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.AnnualPayouts)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
