using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class C_dta_CrmTasksMap : EntityTypeConfiguration<C_dta_CrmTasks>
    {
        public C_dta_CrmTasksMap()
        {
            // Primary Key
            this.HasKey(t => new { t.C_col_3, t.C_col_4, t.C_col_5 });

            // Properties
            this.Property(t => t.C_col_3)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.C_col_5)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("_dta_CrmTasks");
            this.Property(t => t.C_col_1).HasColumnName("_col_1");
            this.Property(t => t.C_col_2).HasColumnName("_col_2");
            this.Property(t => t.C_col_3).HasColumnName("_col_3");
            this.Property(t => t.C_col_4).HasColumnName("_col_4");
            this.Property(t => t.C_col_5).HasColumnName("_col_5");
            this.Property(t => t.C_col_6).HasColumnName("_col_6");
            this.Property(t => t.C_col_7).HasColumnName("_col_7");
            this.Property(t => t.C_col_8).HasColumnName("_col_8");
            this.Property(t => t.C_col_9).HasColumnName("_col_9");
            this.Property(t => t.C_col_10).HasColumnName("_col_10");
        }
    }
}
