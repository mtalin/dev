using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MeetingsStepMap : EntityTypeConfiguration<MeetingsStep>
    {
        public MeetingsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("MeetingsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Meeting).HasColumnName("Meeting");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Event).HasColumnName("Event");

            // Relationships
            this.HasOptional(t => t.Event1)
                .WithMany(t => t.MeetingsSteps)
                .HasForeignKey(d => d.Event);
            this.HasRequired(t => t.Meeting1)
                .WithMany(t => t.MeetingsSteps)
                .HasForeignKey(d => d.Meeting);
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.MeetingsSteps)
                .HasForeignKey(d => d.Message);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.MeetingsSteps)
                .HasForeignKey(d => d.Note);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.MeetingsSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
