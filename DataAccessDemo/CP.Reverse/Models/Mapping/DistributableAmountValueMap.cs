using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DistributableAmountValueMap : EntityTypeConfiguration<DistributableAmountValue>
    {
        public DistributableAmountValueMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Year, t.Good });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Year)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Good)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("DistributableAmountValues");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.AVC).HasColumnName("AVC");
            this.Property(t => t.AVS).HasColumnName("AVS");
            this.Property(t => t.AVA).HasColumnName("AVA");
            this.Property(t => t.MPC).HasColumnName("MPC");
            this.Property(t => t.MIR).HasColumnName("MIR");
            this.Property(t => t.UI0).HasColumnName("UI0");
            this.Property(t => t.ACF).HasColumnName("ACF");
            this.Property(t => t.QD).HasColumnName("QD");
            this.Property(t => t.RCV).HasColumnName("RCV");
            this.Property(t => t.UI).HasColumnName("UI");
            this.Property(t => t.CF).HasColumnName("CF");
            this.Property(t => t.Scenario).HasColumnName("Scenario");
            this.Property(t => t.Final).HasColumnName("Final");
            this.Property(t => t.Good).HasColumnName("Good");
        }
    }
}
