using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class SubstantialContributorMap : EntityTypeConfiguration<SubstantialContributor>
    {
        public SubstantialContributorMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("SubstantialContributors");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Donor).HasColumnName("Donor");
            this.Property(t => t.DtDate).HasColumnName("DtDate");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.ReportIn990PF).HasColumnName("ReportIn990PF");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");

            // Relationships
            this.HasRequired(t => t.Donor1)
                .WithMany(t => t.SubstantialContributors)
                .HasForeignKey(d => d.Donor);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.SubstantialContributors)
                .HasForeignKey(d => d.Author);

        }
    }
}
