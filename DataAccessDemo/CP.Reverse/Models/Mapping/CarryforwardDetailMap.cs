using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CarryforwardDetailMap : EntityTypeConfiguration<CarryforwardDetail>
    {
        public CarryforwardDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CarryforwardDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FromYear).HasColumnName("FromYear");
            this.Property(t => t.ToYear).HasColumnName("ToYear");
            this.Property(t => t.EGC).HasColumnName("EGC");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.CarryforwardDetails)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
