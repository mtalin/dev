using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class JobMap : EntityTypeConfiguration<Job>
    {
        public JobMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Result)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Jobs");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.ParentJob).HasColumnName("ParentJob");
            this.Property(t => t.PreviousJob).HasColumnName("PreviousJob");
            this.Property(t => t.JobClass).HasColumnName("JobClass");
            this.Property(t => t.DtSchedule).HasColumnName("DtSchedule");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Result).HasColumnName("Result");
            this.Property(t => t.RetryNo).HasColumnName("RetryNo");
            this.Property(t => t.DtStart).HasColumnName("DtStart");
            this.Property(t => t.DtStop).HasColumnName("DtStop");
            this.Property(t => t.JobData).HasColumnName("JobData");
            this.Property(t => t.Priority).HasColumnName("Priority");

            // Relationships
            this.HasRequired(t => t.Event1)
                .WithMany(t => t.Jobs)
                .HasForeignKey(d => d.Event);
            this.HasRequired(t => t.JobClass1)
                .WithMany(t => t.Jobs)
                .HasForeignKey(d => d.JobClass);
            this.HasOptional(t => t.Job1)
                .WithMany(t => t.Jobs1)
                .HasForeignKey(d => d.ParentJob);
            this.HasOptional(t => t.Job2)
                .WithMany(t => t.Jobs11)
                .HasForeignKey(d => d.PreviousJob);

        }
    }
}
