using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vContributedAmountMap : EntityTypeConfiguration<vContributedAmount>
    {
        public vContributedAmountMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Foundation, t.DtTransact });

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vContributedAmounts");
            this.Property(t => t.Donor).HasColumnName("Donor");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
            this.Property(t => t.ActualAmount).HasColumnName("ActualAmount");
            this.Property(t => t.PhantomAmount).HasColumnName("PhantomAmount");
        }
    }
}
