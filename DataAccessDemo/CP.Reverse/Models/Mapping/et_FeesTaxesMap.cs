using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_FeesTaxesMap : EntityTypeConfiguration<et_FeesTaxes>
    {
        public et_FeesTaxesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FoundationId, t.Type, t.Amount, t.PeriodType, t.IsMultiQuarter, t.Q1Covered, t.Q2Covered, t.Q3Covered, t.Q4Covered, t.TaxRate });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TypeName)
                .HasMaxLength(300);

            this.Property(t => t.DateCreated)
                .HasMaxLength(30);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TargetName)
                .HasMaxLength(200);

            this.Property(t => t.PeriodType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PeriodName)
                .HasMaxLength(300);

            this.Property(t => t.ReasonForFee)
                .HasMaxLength(4000);

            this.Property(t => t.CalcMethod)
                .HasMaxLength(150);

            this.Property(t => t.IsMultiQuarter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.VerbosedQuarters)
                .HasMaxLength(255);

            this.Property(t => t.TaxRate)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ShortNameQuarters)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("et_FeesTaxes");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FoundationId).HasColumnName("FoundationId");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.AuthorId).HasColumnName("AuthorId");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Amount2).HasColumnName("Amount2");
            this.Property(t => t.TargetName).HasColumnName("TargetName");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");
            this.Property(t => t.PeriodType).HasColumnName("PeriodType");
            this.Property(t => t.PeriodName).HasColumnName("PeriodName");
            this.Property(t => t.ReasonForFee).HasColumnName("ReasonForFee");
            this.Property(t => t.CalcMethod).HasColumnName("CalcMethod");
            this.Property(t => t.IsMultiQuarter).HasColumnName("IsMultiQuarter");
            this.Property(t => t.Q1Covered).HasColumnName("Q1Covered");
            this.Property(t => t.Q2Covered).HasColumnName("Q2Covered");
            this.Property(t => t.Q3Covered).HasColumnName("Q3Covered");
            this.Property(t => t.Q4Covered).HasColumnName("Q4Covered");
            this.Property(t => t.TaxYear).HasColumnName("TaxYear");
            this.Property(t => t.TaxQuarter).HasColumnName("TaxQuarter");
            this.Property(t => t.TaxYearEnd).HasColumnName("TaxYearEnd");
            this.Property(t => t.VerbosedQuarters).HasColumnName("VerbosedQuarters");
            this.Property(t => t.TaxRate).HasColumnName("TaxRate");
            this.Property(t => t.ShortNameQuarters).HasColumnName("ShortNameQuarters");
        }
    }
}
