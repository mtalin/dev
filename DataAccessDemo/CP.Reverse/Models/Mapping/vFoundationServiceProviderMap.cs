using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vFoundationServiceProviderMap : EntityTypeConfiguration<vFoundationServiceProvider>
    {
        public vFoundationServiceProviderMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Foundation, t.ServiceProvider, t.ServiceProviderCompany });

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ServiceProvider)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ServiceProviderName)
                .HasMaxLength(500);

            this.Property(t => t.ServiceProviderCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vFoundationServiceProviders");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.ServiceProviderName).HasColumnName("ServiceProviderName");
            this.Property(t => t.ServiceProviderCompany).HasColumnName("ServiceProviderCompany");
        }
    }
}
