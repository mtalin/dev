using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AbbreviateAddressMap : EntityTypeConfiguration<AbbreviateAddress>
    {
        public AbbreviateAddressMap()
        {
            // Primary Key
            this.HasKey(t => t.Abbrev);

            // Properties
            this.Property(t => t.Abbrev)
                .IsRequired()
                .HasMaxLength(8);

            this.Property(t => t.FullName)
                .IsRequired()
                .HasMaxLength(75);

            // Table & Column Mappings
            this.ToTable("AbbreviateAddresses");
            this.Property(t => t.Abbrev).HasColumnName("Abbrev");
            this.Property(t => t.FullName).HasColumnName("FullName");
        }
    }
}
