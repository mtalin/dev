using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PaymentMap : EntityTypeConfiguration<Payment>
    {
        public PaymentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Memo)
                .HasMaxLength(500);

            this.Property(t => t.PayableTo)
                .HasMaxLength(1000);

            this.Property(t => t.ModReason)
                .HasMaxLength(1000);

            this.Property(t => t.PayeeLine2)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Payments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.CheckingAccount).HasColumnName("CheckingAccount");
            this.Property(t => t.PendingTransactData).HasColumnName("PendingTransactData");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.IsGrouped).HasColumnName("IsGrouped");
            this.Property(t => t.PayableTo).HasColumnName("PayableTo");
            this.Property(t => t.IsModified).HasColumnName("IsModified");
            this.Property(t => t.ModRequired).HasColumnName("ModRequired");
            this.Property(t => t.ModReason).HasColumnName("ModReason");
            this.Property(t => t.ProcessingOffice).HasColumnName("ProcessingOffice");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.PayeeLine2).HasColumnName("PayeeLine2");
            this.Property(t => t.Batch).HasColumnName("Batch");

            // Relationships
            this.HasOptional(t => t.CheckingAccount1)
                .WithMany(t => t.Payments)
                .HasForeignKey(d => d.CheckingAccount);
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.Payments)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.Payments)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.PaymentsBatch)
                .WithMany(t => t.Payments)
                .HasForeignKey(d => d.Batch);
            this.HasOptional(t => t.PostalAddress1)
                .WithMany(t => t.Payments)
                .HasForeignKey(d => d.PostalAddress);

        }
    }
}
