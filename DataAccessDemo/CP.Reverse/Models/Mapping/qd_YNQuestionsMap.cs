using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class qd_YNQuestionsMap : EntityTypeConfiguration<qd_YNQuestions>
    {
        public qd_YNQuestionsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.DtCreated, t.Questionnaire, t.Section, t.Checked, t.Comments, t.ResolveNote });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Questionnaire)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Section)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Comments)
                .IsRequired();

            this.Property(t => t.ResolveNote)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("qd_YNQuestions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Questionnaire).HasColumnName("Questionnaire");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.Question).HasColumnName("Question");
            this.Property(t => t.Checked).HasColumnName("Checked");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.ResolveNote).HasColumnName("ResolveNote");
        }
    }
}
