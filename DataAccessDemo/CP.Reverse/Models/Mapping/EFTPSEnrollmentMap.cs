using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EFTPSEnrollmentMap : EntityTypeConfiguration<EFTPSEnrollment>
    {
        public EFTPSEnrollmentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.EFTPSPin)
                .IsFixedLength()
                .HasMaxLength(4);

            // Table & Column Mappings
            this.ToTable("EFTPSEnrollments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.CheckingAccount).HasColumnName("CheckingAccount");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.EFTPSPin).HasColumnName("EFTPSPin");

            // Relationships
            this.HasOptional(t => t.CheckingAccount1)
                .WithMany(t => t.EFTPSEnrollments)
                .HasForeignKey(d => d.CheckingAccount);
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.EFTPSEnrollments)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
