using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vProjectMap : EntityTypeConfiguration<vProject>
    {
        public vProjectMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Area, t.AreaName, t.Foundation, t.Level, t.Name, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Area)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AreaName)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.ProgramName)
                .HasMaxLength(200);

            this.Property(t => t.ProjectName)
                .HasMaxLength(200);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Level)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vProjects");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Area).HasColumnName("Area");
            this.Property(t => t.Program).HasColumnName("Program");
            this.Property(t => t.Project).HasColumnName("Project");
            this.Property(t => t.AreaName).HasColumnName("AreaName");
            this.Property(t => t.ProgramName).HasColumnName("ProgramName");
            this.Property(t => t.ProjectName).HasColumnName("ProjectName");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Level).HasColumnName("Level");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.AuthorDeleted).HasColumnName("AuthorDeleted");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
        }
    }
}
