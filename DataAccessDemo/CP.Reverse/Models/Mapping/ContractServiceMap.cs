using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ContractServiceMap : EntityTypeConfiguration<ContractService>
    {
        public ContractServiceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContractServices");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.Service).HasColumnName("Service");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.Contract1)
                .WithMany(t => t.ContractServices)
                .HasForeignKey(d => d.Contract);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ContractServices)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.ContractServices1)
                .HasForeignKey(d => d.DeletedBy);

        }
    }
}
