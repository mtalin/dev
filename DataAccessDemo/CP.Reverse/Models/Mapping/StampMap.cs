using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class StampMap : EntityTypeConfiguration<Stamp>
    {
        public StampMap()
        {
            // Primary Key
            this.HasKey(t => t.StampName);

            // Properties
            this.Property(t => t.StampName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.StampValue)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Stamps");
            this.Property(t => t.StampName).HasColumnName("StampName");
            this.Property(t => t.StampValue).HasColumnName("StampValue");
        }
    }
}
