using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CMSEventMap : EntityTypeConfiguration<CMSEvent>
    {
        public CMSEventMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.EventDate, t.Name, t.Location, t.DtCreated, t.Author });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Location)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Link)
                .HasMaxLength(300);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CMSEvents");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.EventDate).HasColumnName("EventDate");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.PDF).HasColumnName("PDF");
            this.Property(t => t.Link).HasColumnName("Link");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.EventToDate).HasColumnName("EventToDate");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CMSEvents)
                .HasForeignKey(d => d.Author);

        }
    }
}
