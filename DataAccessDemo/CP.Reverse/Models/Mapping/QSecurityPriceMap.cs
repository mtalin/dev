using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QSecurityPriceMap : EntityTypeConfiguration<QSecurityPrice>
    {
        public QSecurityPriceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Symbol)
                .HasMaxLength(50);

            this.Property(t => t.ErrorInfo)
                .HasMaxLength(200);

            this.Property(t => t.CUSIP)
                .IsFixedLength()
                .HasMaxLength(12);

            // Table & Column Mappings
            this.ToTable("QSecurityPrices");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Symbol).HasColumnName("Symbol");
            this.Property(t => t.DtPrice).HasColumnName("DtPrice");
            this.Property(t => t.ClsPrice).HasColumnName("ClsPrice");
            this.Property(t => t.LowPrice).HasColumnName("LowPrice");
            this.Property(t => t.HiPrice).HasColumnName("HiPrice");
            this.Property(t => t.ErrorInfo).HasColumnName("ErrorInfo");
            this.Property(t => t.IgnoreSymbol).HasColumnName("IgnoreSymbol");
            this.Property(t => t.CUSIP).HasColumnName("CUSIP");
        }
    }
}
