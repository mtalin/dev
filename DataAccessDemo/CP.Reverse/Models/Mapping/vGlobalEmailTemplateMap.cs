using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vGlobalEmailTemplateMap : EntityTypeConfiguration<vGlobalEmailTemplate>
    {
        public vGlobalEmailTemplateMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.EventClass, t.TriggerName, t.Author, t.DtCreated, t.GenericEmail, t.Active, t.Type, t.TypeName, t.JobSubType });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(100);

            this.Property(t => t.EventClass)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TriggerName)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.GenericEmail)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Subject)
                .HasMaxLength(500);

            this.Property(t => t.FromAddress)
                .HasMaxLength(200);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TypeName)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.JobSubType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vGlobalEmailTemplates");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.EventClass).HasColumnName("EventClass");
            this.Property(t => t.TriggerName).HasColumnName("TriggerName");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Parent).HasColumnName("Parent");
            this.Property(t => t.GenericEmail).HasColumnName("GenericEmail");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.FromAddress).HasColumnName("FromAddress");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.JobSubType).HasColumnName("JobSubType");
        }
    }
}
