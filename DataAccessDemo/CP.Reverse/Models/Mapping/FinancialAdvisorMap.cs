using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FinancialAdvisorMap : EntityTypeConfiguration<FinancialAdvisor>
    {
        public FinancialAdvisorMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Alt_Contact)
                .HasMaxLength(50);

            this.Property(t => t.Alt_Phone)
                .HasMaxLength(22);

            this.Property(t => t.Alt_Email)
                .HasMaxLength(50);

            this.Property(t => t.URL)
                .HasMaxLength(150);

            this.Property(t => t.D_BankName)
                .HasMaxLength(50);

            this.Property(t => t.D_ABA)
                .HasMaxLength(22);

            this.Property(t => t.D_AccName)
                .HasMaxLength(50);

            this.Property(t => t.D_AccNumber)
                .HasMaxLength(22);

            this.Property(t => t.M_BankName)
                .HasMaxLength(50);

            this.Property(t => t.M_ABA)
                .HasMaxLength(22);

            this.Property(t => t.M_AccName)
                .HasMaxLength(50);

            this.Property(t => t.M_AccNumber)
                .HasMaxLength(22);

            this.Property(t => t.FirmName)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("FinancialAdvisors");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Alt_Contact).HasColumnName("Alt_Contact");
            this.Property(t => t.Alt_Phone).HasColumnName("Alt_Phone");
            this.Property(t => t.Alt_Email).HasColumnName("Alt_Email");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.D_BankName).HasColumnName("D_BankName");
            this.Property(t => t.D_ABA).HasColumnName("D_ABA");
            this.Property(t => t.D_AccName).HasColumnName("D_AccName");
            this.Property(t => t.D_AccNumber).HasColumnName("D_AccNumber");
            this.Property(t => t.M_BankName).HasColumnName("M_BankName");
            this.Property(t => t.M_ABA).HasColumnName("M_ABA");
            this.Property(t => t.M_AccName).HasColumnName("M_AccName");
            this.Property(t => t.M_AccNumber).HasColumnName("M_AccNumber");
            this.Property(t => t.DtAccepted).HasColumnName("DtAccepted");
            this.Property(t => t.FinancialAdvisorGroup).HasColumnName("FinancialAdvisorGroup");
            this.Property(t => t.FirmName).HasColumnName("FirmName");
            this.Property(t => t.CompanyPosition).HasColumnName("CompanyPosition");
            this.Property(t => t.StaffingPolicy).HasColumnName("StaffingPolicy");

            // Relationships
            this.HasOptional(t => t.FinancialAdvisorGroup1)
                .WithMany(t => t.FinancialAdvisors)
                .HasForeignKey(d => d.FinancialAdvisorGroup);
            this.HasRequired(t => t.Person)
                .WithOptional(t => t.FinancialAdvisor);

        }
    }
}
