using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class HouseholdMemberMap : EntityTypeConfiguration<HouseholdMember>
    {
        public HouseholdMemberMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SSN)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(9);

            // Table & Column Mappings
            this.ToTable("HouseholdMembers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.HouseholdAccount).HasColumnName("HouseholdAccount");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.SSN).HasColumnName("SSN");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.Income).HasColumnName("Income");
            this.Property(t => t.Primary).HasColumnName("Primary");
            this.Property(t => t.GmxRef).HasColumnName("GmxRef");

            // Relationships
            this.HasRequired(t => t.HouseholdAccount1)
                .WithMany(t => t.HouseholdMembers)
                .HasForeignKey(d => d.HouseholdAccount);

        }
    }
}
