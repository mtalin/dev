using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DocumentsDetailMap : EntityTypeConfiguration<DocumentsDetail>
    {
        public DocumentsDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Theme, t.Extention, t.Name, t.Author, t.DtCreated, t.ItemID, t.TitleName });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Theme)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Extention)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(400);

            this.Property(t => t.FileName)
                .HasMaxLength(256);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Notes)
                .HasMaxLength(400);

            this.Property(t => t.Title)
                .HasMaxLength(100);

            this.Property(t => t.ShortTitle)
                .HasMaxLength(100);

            this.Property(t => t.TitleWithDetails)
                .HasMaxLength(100);

            this.Property(t => t.ShortTitleWithDetails)
                .HasMaxLength(100);

            this.Property(t => t.Flags)
                .HasMaxLength(250);

            this.Property(t => t.SortName)
                .HasMaxLength(100);

            this.Property(t => t.ItemID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TitleName)
                .IsRequired()
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("DocumentsDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.ShellCorporation).HasColumnName("ShellCorporation");
            this.Property(t => t.Theme).HasColumnName("Theme");
            this.Property(t => t.Extention).HasColumnName("Extention");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.FileName).HasColumnName("FileName");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtReceived).HasColumnName("DtReceived");
            this.Property(t => t.DtBookMark).HasColumnName("DtBookMark");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.ShortTitle).HasColumnName("ShortTitle");
            this.Property(t => t.TitleWithDetails).HasColumnName("TitleWithDetails");
            this.Property(t => t.ShortTitleWithDetails).HasColumnName("ShortTitleWithDetails");
            this.Property(t => t.Flags).HasColumnName("Flags");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.SortName).HasColumnName("SortName");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.TitleName).HasColumnName("TitleName");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
        }
    }
}
