using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class tmp_GrantHistoryMap : EntityTypeConfiguration<tmp_GrantHistory>
    {
        public tmp_GrantHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Purpose)
                .HasMaxLength(50);

            this.Property(t => t.PurposeDescription)
                .HasMaxLength(200);

            this.Property(t => t.SpecPurposeText1)
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeText2)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tmp_GrantHistory");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtGrant).HasColumnName("DtGrant");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.Grantor).HasColumnName("Grantor");
            this.Property(t => t.Anonymous).HasColumnName("Anonymous");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.PurposeDescription).HasColumnName("PurposeDescription");
            this.Property(t => t.SpecPurposeType).HasColumnName("SpecPurposeType");
            this.Property(t => t.SpecPurposeText1).HasColumnName("SpecPurposeText1");
            this.Property(t => t.SpecPurposeText2).HasColumnName("SpecPurposeText2");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.GrantLetter).HasColumnName("GrantLetter");

            // Relationships
            this.HasOptional(t => t.FoundationProject1)
                .WithMany(t => t.tmp_GrantHistory)
                .HasForeignKey(d => d.FoundationProject);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.tmp_GrantHistory)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.tmp_GrantHistory)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.tmp_GrantHistory1)
                .HasForeignKey(d => d.Grantor);

        }
    }
}
