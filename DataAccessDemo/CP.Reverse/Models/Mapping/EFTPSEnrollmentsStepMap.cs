using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EFTPSEnrollmentsStepMap : EntityTypeConfiguration<EFTPSEnrollmentsStep>
    {
        public EFTPSEnrollmentsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.EFTPSPin)
                .IsFixedLength()
                .HasMaxLength(4);

            // Table & Column Mappings
            this.ToTable("EFTPSEnrollmentsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.EFTPSEnrollment).HasColumnName("EFTPSEnrollment");
            this.Property(t => t.CheckingAccount).HasColumnName("CheckingAccount");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.EFTPSPin).HasColumnName("EFTPSPin");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasOptional(t => t.CheckingAccount1)
                .WithMany(t => t.EFTPSEnrollmentsSteps)
                .HasForeignKey(d => d.CheckingAccount);
            this.HasOptional(t => t.EFTPSEnrollment1)
                .WithMany(t => t.EFTPSEnrollmentsSteps)
                .HasForeignKey(d => d.EFTPSEnrollment);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.EFTPSEnrollmentsSteps)
                .HasForeignKey(d => d.Note);

        }
    }
}
