using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class bak_PricingScheduleInstancesMap : EntityTypeConfiguration<bak_PricingScheduleInstances>
    {
        public bak_PricingScheduleInstancesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.IsActive, t.PricingSource, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PricingSource)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("bak_PricingScheduleInstances");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PricingSchedule).HasColumnName("PricingSchedule");
            this.Property(t => t.ContractPricing).HasColumnName("ContractPricing");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.PricingSource).HasColumnName("PricingSource");
            this.Property(t => t.BillingEvent).HasColumnName("BillingEvent");
            this.Property(t => t.BillingRecipient).HasColumnName("BillingRecipient");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
        }
    }
}
