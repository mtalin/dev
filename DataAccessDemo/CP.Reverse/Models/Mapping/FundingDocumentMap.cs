using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FundingDocumentMap : EntityTypeConfiguration<FundingDocument>
    {
        public FundingDocumentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.URL)
                .IsRequired()
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("FundingDocuments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.URL).HasColumnName("URL");
        }
    }
}
