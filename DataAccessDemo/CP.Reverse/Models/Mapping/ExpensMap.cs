using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ExpensMap : EntityTypeConfiguration<Expens>
    {
        public ExpensMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.InvoiceNumber)
                .HasMaxLength(50);

            this.Property(t => t.SpecInstruct)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Expenses");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.ContactInfo).HasColumnName("ContactInfo");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.AssetsAmount).HasColumnName("AssetsAmount");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.PendingTransact).HasColumnName("PendingTransact");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.InvoiceNumber).HasColumnName("InvoiceNumber");
            this.Property(t => t.InvoiceDocument).HasColumnName("InvoiceDocument");
            this.Property(t => t.PayeeProfile).HasColumnName("PayeeProfile");
            this.Property(t => t.ExpenseReport).HasColumnName("ExpenseReport");
            this.Property(t => t.AdditionalFiling).HasColumnName("AdditionalFiling");
            this.Property(t => t.AdditionalAmount).HasColumnName("AdditionalAmount");
            this.Property(t => t.ReimbursableTo).HasColumnName("ReimbursableTo");
            this.Property(t => t.ReimbursableContactInfo).HasColumnName("ReimbursableContactInfo");
            this.Property(t => t.SpecInstruct).HasColumnName("SpecInstruct");
            this.Property(t => t.IsSentEmail).HasColumnName("IsSentEmail");
            this.Property(t => t.Template).HasColumnName("Template");
            this.Property(t => t.TimeSchedule).HasColumnName("TimeSchedule");

            // Relationships
            this.HasOptional(t => t.Document1)
                .WithMany(t => t.Expenses)
                .HasForeignKey(d => d.Document);
            this.HasOptional(t => t.Document2)
                .WithMany(t => t.Expenses1)
                .HasForeignKey(d => d.InvoiceDocument);
            this.HasOptional(t => t.ExpenseReport1)
                .WithMany(t => t.Expenses)
                .HasForeignKey(d => d.ExpenseReport);
            this.HasOptional(t => t.PostalAddress)
                .WithMany(t => t.Expenses)
                .HasForeignKey(d => d.ContactInfo);
            this.HasOptional(t => t.FoundationProject1)
                .WithMany(t => t.Expenses)
                .HasForeignKey(d => d.FoundationProject);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Expenses)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.PayeeProfile1)
                .WithMany(t => t.Expenses)
                .HasForeignKey(d => d.PayeeProfile);
            this.HasOptional(t => t.PayeeProfile2)
                .WithMany(t => t.Expenses1)
                .HasForeignKey(d => d.ReimbursableTo);
            this.HasOptional(t => t.PendingTransact1)
                .WithMany(t => t.Expenses)
                .HasForeignKey(d => d.PendingTransact);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.Expenses)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.PostalAddress1)
                .WithMany(t => t.Expenses1)
                .HasForeignKey(d => d.ReimbursableContactInfo);
            this.HasOptional(t => t.Expens1)
                .WithMany(t => t.Expenses1)
                .HasForeignKey(d => d.Template);
            this.HasOptional(t => t.TimeSchedule1)
                .WithMany(t => t.Expenses)
                .HasForeignKey(d => d.TimeSchedule);

        }
    }
}
