using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_CharityRequestsMap : EntityTypeConfiguration<et_CharityRequests>
    {
        public et_CharityRequestsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.PendingID });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DateCreated)
                .HasMaxLength(30);

            this.Property(t => t.CharityName)
                .HasMaxLength(200);

            this.Property(t => t.Address)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .IsFixedLength()
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZIP)
                .HasMaxLength(9);

            this.Property(t => t.Country)
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.URL)
                .HasMaxLength(150);

            this.Property(t => t.ContactPerson)
                .IsFixedLength()
                .HasMaxLength(65);

            this.Property(t => t.GeneralInfo)
                .HasMaxLength(3000);

            this.Property(t => t.AdditionalInfo)
                .HasMaxLength(3000);

            this.Property(t => t.PendingID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PendingName)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("et_CharityRequests");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.RequestAuthor).HasColumnName("RequestAuthor");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.InitialGrant).HasColumnName("InitialGrant");
            this.Property(t => t.CharityName).HasColumnName("CharityName");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZIP).HasColumnName("ZIP");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.ContactPerson).HasColumnName("ContactPerson");
            this.Property(t => t.GeneralInfo).HasColumnName("GeneralInfo");
            this.Property(t => t.AdditionalInfo).HasColumnName("AdditionalInfo");
            this.Property(t => t.CharityEdit).HasColumnName("CharityEdit");
            this.Property(t => t.PublicEIN).HasColumnName("PublicEIN");
            this.Property(t => t.DiscloseIdentity).HasColumnName("DiscloseIdentity");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.PendingID).HasColumnName("PendingID");
            this.Property(t => t.PendingName).HasColumnName("PendingName");
        }
    }
}
