using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class VotingOfferResultMap : EntityTypeConfiguration<VotingOfferResult>
    {
        public VotingOfferResultMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("VotingOfferResults");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.VotingOffer).HasColumnName("VotingOffer");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Appointment).HasColumnName("Appointment");
            this.Property(t => t.Opinion).HasColumnName("Opinion");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.VotingOffer1)
                .WithMany(t => t.VotingOfferResults)
                .HasForeignKey(d => d.VotingOffer);

        }
    }
}
