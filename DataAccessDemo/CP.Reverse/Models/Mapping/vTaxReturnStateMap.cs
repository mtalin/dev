using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vTaxReturnStateMap : EntityTypeConfiguration<vTaxReturnState>
    {
        public vTaxReturnStateMap()
        {
            // Primary Key
            this.HasKey(t => t.Foundation);

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.State1)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.State2)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.State3)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("vTaxReturnStates");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.State1).HasColumnName("State1");
            this.Property(t => t.State2).HasColumnName("State2");
            this.Property(t => t.State3).HasColumnName("State3");
        }
    }
}
