using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QCapitalGainMap : EntityTypeConfiguration<QCapitalGain>
    {
        public QCapitalGainMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Security)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("QCapitalGains");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Security).HasColumnName("Security");
            this.Property(t => t.Shares).HasColumnName("Shares");
            this.Property(t => t.DtBought).HasColumnName("DtBought");
            this.Property(t => t.DtSold).HasColumnName("DtSold");
            this.Property(t => t.SalesPrice).HasColumnName("SalesPrice");
            this.Property(t => t.CostBasis).HasColumnName("CostBasis");
            this.Property(t => t.RealizedGain).HasColumnName("RealizedGain");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.QCapitalGains)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
