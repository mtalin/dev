using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FoundationAccountsStepMap : EntityTypeConfiguration<FoundationAccountsStep>
    {
        public FoundationAccountsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("FoundationAccountsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtReconciled_Prev).HasColumnName("DtReconciled_Prev");
            this.Property(t => t.DtReconciled_New).HasColumnName("DtReconciled_New");
            this.Property(t => t.Status_Prev).HasColumnName("Status_Prev");
            this.Property(t => t.Status_New).HasColumnName("Status_New");
            this.Property(t => t.AccountType_Prev).HasColumnName("AccountType_Prev");
            this.Property(t => t.AccountType_New).HasColumnName("AccountType_New");
            this.Property(t => t.FinancialPartner_Prev).HasColumnName("FinancialPartner_Prev");
            this.Property(t => t.FinancialPartner_New).HasColumnName("FinancialPartner_New");
            this.Property(t => t.ServiceProvider_Prev).HasColumnName("ServiceProvider_Prev");
            this.Property(t => t.ServiceProvider_New).HasColumnName("ServiceProvider_New");
            this.Property(t => t.Department_Prev).HasColumnName("Department_Prev");
            this.Property(t => t.Department_New).HasColumnName("Department_New");

            // Relationships
            this.HasRequired(t => t.FoundationAccount1)
                .WithMany(t => t.FoundationAccountsSteps)
                .HasForeignKey(d => d.FoundationAccount);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.FoundationAccountsSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
