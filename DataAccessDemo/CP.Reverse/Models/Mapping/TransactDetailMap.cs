using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TransactDetailMap : EntityTypeConfiguration<TransactDetail>
    {
        public TransactDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FinancialPartner, t.Foundation, t.FoundationAccount, t.DtTransact, t.Type, t.Amount, t.Tax, t.SecFee, t.MiscFee, t.OthFee, t.ClearingCharge, t.BrokerageCharge, t.InvestValue, t.DtCreated, t.Restricted, t.Hold, t.Interest, t.TypeName, t.TargetName });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FinancialPartner)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationAccount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FPTransactID)
                .HasMaxLength(120);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Tax)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SecFee)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MiscFee)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.OthFee)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ClearingCharge)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.BrokerageCharge)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.InvestValue)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MergeTicker)
                .HasMaxLength(32);

            this.Property(t => t.Notes)
                .HasMaxLength(300);

            this.Property(t => t.Interest)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TypeName)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.LongTypeName)
                .HasMaxLength(300);

            this.Property(t => t.TypeFlags)
                .HasMaxLength(150);

            this.Property(t => t.AssetName)
                .HasMaxLength(100);

            this.Property(t => t.AssetSymbol)
                .HasMaxLength(32);

            this.Property(t => t.AssetTypeName)
                .HasMaxLength(150);

            this.Property(t => t.AssetTypeFlags)
                .HasMaxLength(150);

            this.Property(t => t.TargetName)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TransactDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.FPTransactID).HasColumnName("FPTransactID");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Principal).HasColumnName("Principal");
            this.Property(t => t.Commision).HasColumnName("Commision");
            this.Property(t => t.Tax).HasColumnName("Tax");
            this.Property(t => t.SecFee).HasColumnName("SecFee");
            this.Property(t => t.MiscFee).HasColumnName("MiscFee");
            this.Property(t => t.OthFee).HasColumnName("OthFee");
            this.Property(t => t.ClearingCharge).HasColumnName("ClearingCharge");
            this.Property(t => t.BrokerageCharge).HasColumnName("BrokerageCharge");
            this.Property(t => t.InvestValue).HasColumnName("InvestValue");
            this.Property(t => t.Taxable).HasColumnName("Taxable");
            this.Property(t => t.UnitsNumber).HasColumnName("UnitsNumber");
            this.Property(t => t.SplitNumerator).HasColumnName("SplitNumerator");
            this.Property(t => t.SplitDenominator).HasColumnName("SplitDenominator");
            this.Property(t => t.MergeTicker).HasColumnName("MergeTicker");
            this.Property(t => t.Ratio).HasColumnName("Ratio");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Restricted).HasColumnName("Restricted");
            this.Property(t => t.Hold).HasColumnName("Hold");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.QTransaction).HasColumnName("QTransaction");
            this.Property(t => t.Interest).HasColumnName("Interest");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.Asset2).HasColumnName("Asset2");
            this.Property(t => t.CostBasis).HasColumnName("CostBasis");
            this.Property(t => t.BookBasis).HasColumnName("BookBasis");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
            this.Property(t => t.dtDeposit).HasColumnName("dtDeposit");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.LongTypeName).HasColumnName("LongTypeName");
            this.Property(t => t.TypeFlags).HasColumnName("TypeFlags");
            this.Property(t => t.AssetName).HasColumnName("AssetName");
            this.Property(t => t.AssetSymbol).HasColumnName("AssetSymbol");
            this.Property(t => t.AssetType).HasColumnName("AssetType");
            this.Property(t => t.AssetTypeName).HasColumnName("AssetTypeName");
            this.Property(t => t.AssetTypeFlags).HasColumnName("AssetTypeFlags");
            this.Property(t => t.TargetName).HasColumnName("TargetName");
        }
    }
}
