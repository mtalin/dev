using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class VotingMap : EntityTypeConfiguration<Voting>
    {
        public VotingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Votings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Filter).HasColumnName("Filter");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Votings)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.Votings)
                .HasForeignKey(d => d.Author);

        }
    }
}
