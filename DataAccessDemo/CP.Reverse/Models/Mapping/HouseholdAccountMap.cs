using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class HouseholdAccountMap : EntityTypeConfiguration<HouseholdAccount>
    {
        public HouseholdAccountMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("HouseholdAccounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Flags).HasColumnName("Flags");
            this.Property(t => t.Saving).HasColumnName("Saving");
            this.Property(t => t.IRA).HasColumnName("IRA");
            this.Property(t => t.ValueOfHouse).HasColumnName("ValueOfHouse");
            this.Property(t => t.OtherAssets).HasColumnName("OtherAssets");
            this.Property(t => t.CCDebt).HasColumnName("CCDebt");
            this.Property(t => t.CarLoans).HasColumnName("CarLoans");
            this.Property(t => t.Mortgage).HasColumnName("Mortgage");
            this.Property(t => t.OtherLoans).HasColumnName("OtherLoans");
            this.Property(t => t.InitialAmount).HasColumnName("InitialAmount");
            this.Property(t => t.DtAuthorized).HasColumnName("DtAuthorized");
            this.Property(t => t.AuthorizedBy).HasColumnName("AuthorizedBy");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtApproved).HasColumnName("DtApproved");
            this.Property(t => t.DtExpired).HasColumnName("DtExpired");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.AllowMailingCheckDirectly).HasColumnName("AllowMailingCheckDirectly");
            this.Property(t => t.dtDeleted).HasColumnName("dtDeleted");
            this.Property(t => t.old_AuthorizedBy).HasColumnName("old_AuthorizedBy");
            this.Property(t => t.GmxRef).HasColumnName("GmxRef");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");

            // Relationships
            this.HasOptional(t => t.FoundationProject1)
                .WithMany(t => t.HouseholdAccounts)
                .HasForeignKey(d => d.FoundationProject);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.HouseholdAccounts)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.HouseholdAccounts)
                .HasForeignKey(d => d.AuthorizedBy);

        }
    }
}
