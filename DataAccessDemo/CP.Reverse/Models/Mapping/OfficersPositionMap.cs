using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class OfficersPositionMap : EntityTypeConfiguration<OfficersPosition>
    {
        public OfficersPositionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("OfficersPositions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Position).HasColumnName("Position");

            // Relationships
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.OfficersPositions)
                .HasForeignKey(d => d.Person);
            this.HasRequired(t => t.Position1)
                .WithMany(t => t.OfficersPositions)
                .HasForeignKey(d => d.Position);

        }
    }
}
