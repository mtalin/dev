using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FAPPackageFormMap : EntityTypeConfiguration<FAPPackageForm>
    {
        public FAPPackageFormMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.LinkText)
                .HasMaxLength(256);

            this.Property(t => t.Introduction)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("FAPPackageForms");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Partner).HasColumnName("Partner");
            this.Property(t => t.DisplayPackageLink).HasColumnName("DisplayPackageLink");
            this.Property(t => t.LinkText).HasColumnName("LinkText");
            this.Property(t => t.Introduction).HasColumnName("Introduction");
            this.Property(t => t.SendToAdvisor).HasColumnName("SendToAdvisor");
            this.Property(t => t.SendToDistributionCenter).HasColumnName("SendToDistributionCenter");
            this.Property(t => t.DCAddress).HasColumnName("DCAddress");
            this.Property(t => t.SendToSpecificRecipient).HasColumnName("SendToSpecificRecipient");

            // Relationships
            this.HasOptional(t => t.PostalAddress)
                .WithMany(t => t.FAPPackageForms)
                .HasForeignKey(d => d.DCAddress);

        }
    }
}
