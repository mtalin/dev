using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AssetBookMap : EntityTypeConfiguration<AssetBook>
    {
        public AssetBookMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("AssetBooks");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.UBTI).HasColumnName("UBTI");
            this.Property(t => t.UBTIDocument).HasColumnName("UBTIDocument");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtExpired).HasColumnName("DtExpired");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.UBTINote).HasColumnName("UBTINote");

            // Relationships
            this.HasOptional(t => t.Document)
                .WithMany(t => t.AssetBooks)
                .HasForeignKey(d => d.UBTIDocument);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.AssetBooks)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.LocalAsset)
                .WithOptional(t => t.AssetBook);
            this.HasOptional(t => t.Note)
                .WithMany(t => t.AssetBooks)
                .HasForeignKey(d => d.UBTINote);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.AssetBooks)
                .HasForeignKey(d => d.Author);

        }
    }
}
