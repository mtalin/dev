using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GrantsStepMap : EntityTypeConfiguration<GrantsStep>
    {
        public GrantsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("GrantsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Grant).HasColumnName("Grant");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasOptional(t => t.Event1)
                .WithMany(t => t.GrantsSteps)
                .HasForeignKey(d => d.Event);
            this.HasRequired(t => t.Grant1)
                .WithMany(t => t.GrantsSteps)
                .HasForeignKey(d => d.Grant);
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.GrantsSteps)
                .HasForeignKey(d => d.Message);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.GrantsSteps)
                .HasForeignKey(d => d.Note);
            this.HasRequired(t => t.User)
                .WithMany(t => t.GrantsSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
