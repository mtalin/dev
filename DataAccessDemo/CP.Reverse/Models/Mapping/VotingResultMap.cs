using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class VotingResultMap : EntityTypeConfiguration<VotingResult>
    {
        public VotingResultMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("VotingResults");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Voting).HasColumnName("Voting");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Appointment).HasColumnName("Appointment");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.Voting1)
                .WithMany(t => t.VotingResults)
                .HasForeignKey(d => d.Voting);

        }
    }
}
