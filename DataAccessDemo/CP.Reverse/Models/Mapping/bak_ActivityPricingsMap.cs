using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class bak_ActivityPricingsMap : EntityTypeConfiguration<bak_ActivityPricings>
    {
        public bak_ActivityPricingsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.PricingScheduleInstance, t.BaseAmount, t.FeeValue, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PricingScheduleInstance)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.BaseAmount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FeeValue)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("bak_ActivityPricings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PricingScheduleInstance).HasColumnName("PricingScheduleInstance");
            this.Property(t => t.UptoAmount).HasColumnName("UptoAmount");
            this.Property(t => t.BaseAmount).HasColumnName("BaseAmount");
            this.Property(t => t.FeeValue).HasColumnName("FeeValue");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
        }
    }
}
