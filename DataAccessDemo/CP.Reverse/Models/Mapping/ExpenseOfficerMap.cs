using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ExpenseOfficerMap : EntityTypeConfiguration<ExpenseOfficer>
    {
        public ExpenseOfficerMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ExpenseOfficers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Expense).HasColumnName("Expense");
            this.Property(t => t.Person).HasColumnName("Person");

            // Relationships
            this.HasRequired(t => t.Expens)
                .WithMany(t => t.ExpenseOfficers)
                .HasForeignKey(d => d.Expense);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.ExpenseOfficers)
                .HasForeignKey(d => d.Person);

        }
    }
}
