using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ContactsStepsContactMap : EntityTypeConfiguration<ContactsStepsContact>
    {
        public ContactsStepsContactMap()
        {
            // Primary Key
            this.HasKey(t => t.StepID);

            // Properties
            this.Property(t => t.StepID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Note)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ContactsStepsContacts");
            this.Property(t => t.StepID).HasColumnName("StepID");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasRequired(t => t.ContactsStep)
                .WithOptional(t => t.ContactsStepsContact);

        }
    }
}
