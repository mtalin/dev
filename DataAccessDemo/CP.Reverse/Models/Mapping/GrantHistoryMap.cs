using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GrantHistoryMap : EntityTypeConfiguration<GrantHistory>
    {
        public GrantHistoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Amount, t.Anonymous, t.GrantDetails });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Purpose)
                .HasMaxLength(50);

            this.Property(t => t.PurposeDescription)
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeText1)
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeText2)
                .HasMaxLength(1000);

            this.Property(t => t.GrantDetails)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("GrantHistory");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtGrant).HasColumnName("DtGrant");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.Grantor).HasColumnName("Grantor");
            this.Property(t => t.Anonymous).HasColumnName("Anonymous");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.PurposeDescription).HasColumnName("PurposeDescription");
            this.Property(t => t.SpecPurposeType).HasColumnName("SpecPurposeType");
            this.Property(t => t.SpecPurposeText1).HasColumnName("SpecPurposeText1");
            this.Property(t => t.SpecPurposeText2).HasColumnName("SpecPurposeText2");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.GrantLetter).HasColumnName("GrantLetter");
            this.Property(t => t.GrantDetails).HasColumnName("GrantDetails");
        }
    }
}
