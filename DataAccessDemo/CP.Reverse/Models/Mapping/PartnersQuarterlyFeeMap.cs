using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PartnersQuarterlyFeeMap : EntityTypeConfiguration<PartnersQuarterlyFee>
    {
        public PartnersQuarterlyFeeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.FinancialPartner, t.Amount, t.CPFee, t.PartnerFee, t.CPFlat, t.PartnerFlat });

            // Properties
            this.Property(t => t.FinancialPartner)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CPFee)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PartnerFee)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PartnersQuarterlyFees");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.CPFee).HasColumnName("CPFee");
            this.Property(t => t.PartnerFee).HasColumnName("PartnerFee");
            this.Property(t => t.CPFlat).HasColumnName("CPFlat");
            this.Property(t => t.PartnerFlat).HasColumnName("PartnerFlat");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");

            // Relationships
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.PartnersQuarterlyFees)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
