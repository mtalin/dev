using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ServiceUseCasMap : EntityTypeConfiguration<ServiceUseCas>
    {
        public ServiceUseCasMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Tag)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.Type)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Flags)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("ServiceUseCases");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Service).HasColumnName("Service");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Tag).HasColumnName("Tag");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.Flags).HasColumnName("Flags");
        }
    }
}
