using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RegistryMap : EntityTypeConfiguration<Registry>
    {
        public RegistryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Registries");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Currency).HasColumnName("Currency");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.RegistryRule).HasColumnName("RegistryRule");
            this.Property(t => t.TransactType).HasColumnName("TransactType");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.AssetType).HasColumnName("AssetType");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
        }
    }
}
