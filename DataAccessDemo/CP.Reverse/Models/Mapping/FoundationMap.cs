using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FoundationMap : EntityTypeConfiguration<Foundation>
    {
        public FoundationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.EIN)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.KeyInfo)
                .HasMaxLength(512);

            this.Property(t => t.ClientKeyInfo)
                .HasMaxLength(1000);

            this.Property(t => t.Design)
                .HasMaxLength(100);

            this.Property(t => t.PhilanthropicInterest)
                .HasMaxLength(1000);

            this.Property(t => t.TimeStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Foundations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.EIN).HasColumnName("EIN");
            this.Property(t => t.Founder).HasColumnName("Founder");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.RegistrationStep).HasColumnName("RegistrationStep");
            this.Property(t => t.AutoDefaults).HasColumnName("AutoDefaults");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.PlannedFunding).HasColumnName("PlannedFunding");
            this.Property(t => t.CorpFilingDate).HasColumnName("CorpFilingDate");
            this.Property(t => t.IsOnlineApp).HasColumnName("IsOnlineApp");
            this.Property(t => t.ShellCorporation).HasColumnName("ShellCorporation");
            this.Property(t => t.SetupAdmin).HasColumnName("SetupAdmin");
            this.Property(t => t.Conversion).HasColumnName("Conversion");
            this.Property(t => t.HasOfficialStatus).HasColumnName("HasOfficialStatus");
            this.Property(t => t.ProgramCode).HasColumnName("ProgramCode");
            this.Property(t => t.DtBeginCalc).HasColumnName("DtBeginCalc");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.AllowRecommendations).HasColumnName("AllowRecommendations");
            this.Property(t => t.ApproveRecomUnanimously).HasColumnName("ApproveRecomUnanimously");
            this.Property(t => t.ApproveRecomByMeetingsOnly).HasColumnName("ApproveRecomByMeetingsOnly");
            this.Property(t => t.NotifyDirectorsOnNewRecom).HasColumnName("NotifyDirectorsOnNewRecom");
            this.Property(t => t.FiscalYearEnd).HasColumnName("FiscalYearEnd");
            this.Property(t => t.PresidentMayRequireApproveAllGrants).HasColumnName("PresidentMayRequireApproveAllGrants");
            this.Property(t => t.DtFirstBalance).HasColumnName("DtFirstBalance");
            this.Property(t => t.PublicRecordsAddress).HasColumnName("PublicRecordsAddress");
            this.Property(t => t.CorrespondenceAddress).HasColumnName("CorrespondenceAddress");
            this.Property(t => t.NoNominees).HasColumnName("NoNominees");
            this.Property(t => t.ClientContacted).HasColumnName("ClientContacted");
            this.Property(t => t.DtTerminated).HasColumnName("DtTerminated");
            this.Property(t => t.Rank).HasColumnName("Rank");
            this.Property(t => t.KeyInfo).HasColumnName("KeyInfo");
            this.Property(t => t.BalanceFile).HasColumnName("BalanceFile");
            this.Property(t => t.ExpectedFunding).HasColumnName("ExpectedFunding");
            this.Property(t => t.ClientKeyInfo).HasColumnName("ClientKeyInfo");
            this.Property(t => t.Design).HasColumnName("Design");
            this.Property(t => t.RevApprovGrants).HasColumnName("RevApprovGrants");
            this.Property(t => t.RevApprovGrantsCertRecipient).HasColumnName("RevApprovGrantsCertRecipient");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");
            this.Property(t => t.CustomerServiceContact).HasColumnName("CustomerServiceContact");
            this.Property(t => t.CustomerServiceContactText).HasColumnName("CustomerServiceContactText");
            this.Property(t => t.StrategicPartner).HasColumnName("StrategicPartner");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.CustomContactsHTML).HasColumnName("CustomContactsHTML");
            this.Property(t => t.Ovr_CustomContactsHTML).HasColumnName("Ovr_CustomContactsHTML");
            this.Property(t => t.FSPublicRecordsAddress).HasColumnName("FSPublicRecordsAddress");
            this.Property(t => t.FSCorrespondenceAddress).HasColumnName("FSCorrespondenceAddress");
            this.Property(t => t.CheckDesign).HasColumnName("CheckDesign");
            this.Property(t => t.GmxRef).HasColumnName("GmxRef");
            this.Property(t => t.PhilanthropicInterest).HasColumnName("PhilanthropicInterest");
            this.Property(t => t.UsesAccrual).HasColumnName("UsesAccrual");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.KeyTaxReturnNote).HasColumnName("KeyTaxReturnNote");
            this.Property(t => t.AnnualRevenue).HasColumnName("AnnualRevenue");
            this.Property(t => t.PIPrograms).HasColumnName("PIPrograms");
            this.Property(t => t.TimeStamp).HasColumnName("TimeStamp");

            // Relationships
            this.HasOptional(t => t.CompanyElement)
                .WithMany(t => t.Foundations)
                .HasForeignKey(d => d.StrategicPartner);
            this.HasOptional(t => t.PostalAddress)
                .WithMany(t => t.Foundations)
                .HasForeignKey(d => d.CorrespondenceAddress);
            this.HasOptional(t => t.ProgramCode1)
                .WithMany(t => t.Foundations)
                .HasForeignKey(d => d.ProgramCode);
            this.HasOptional(t => t.PostalAddress1)
                .WithMany(t => t.Foundations1)
                .HasForeignKey(d => d.PublicRecordsAddress);
            this.HasOptional(t => t.ShellCorporation1)
                .WithMany(t => t.Foundations)
                .HasForeignKey(d => d.ShellCorporation);
            this.HasRequired(t => t.StructuredCompany1)
                .WithMany(t => t.Foundations)
                .HasForeignKey(d => d.StructuredCompany);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.Foundations)
                .HasForeignKey(d => d.Founder);

        }
    }
}
