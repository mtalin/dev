using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MeetingMap : EntityTypeConfiguration<Meeting>
    {
        public MeetingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.City)
                .HasMaxLength(25);

            this.Property(t => t.Goals)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("Meetings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Original).HasColumnName("Original");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.DtHeld).HasColumnName("DtHeld");
            this.Property(t => t.Method).HasColumnName("Method");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Period).HasColumnName("Period");
            this.Property(t => t.Goals).HasColumnName("Goals");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.IsCustom).HasColumnName("IsCustom");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Meetings)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Meeting1)
                .WithMany(t => t.Meetings1)
                .HasForeignKey(d => d.Original);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.Meetings)
                .HasForeignKey(d => d.Author);

        }
    }
}
