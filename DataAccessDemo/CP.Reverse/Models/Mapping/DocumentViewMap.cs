using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DocumentViewMap : EntityTypeConfiguration<DocumentView>
    {
        public DocumentViewMap()
        {
            // Primary Key
            this.HasKey(t => t.DocumentID);

            // Properties
            this.Property(t => t.DocumentID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DocumentName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DocumentFileName)
                .HasMaxLength(50);

            this.Property(t => t.COMClassName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DocumentView");
            this.Property(t => t.DocumentID).HasColumnName("DocumentID");
            this.Property(t => t.DocumentName).HasColumnName("DocumentName");
            this.Property(t => t.DocumentFileName).HasColumnName("DocumentFileName");
            this.Property(t => t.COMClassName).HasColumnName("COMClassName");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
