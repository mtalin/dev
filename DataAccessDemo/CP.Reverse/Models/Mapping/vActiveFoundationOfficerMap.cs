using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vActiveFoundationOfficerMap : EntityTypeConfiguration<vActiveFoundationOfficer>
    {
        public vActiveFoundationOfficerMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Foundation, t.Person });

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Person)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(200);

            this.Property(t => t.PositionName)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("vActiveFoundationOfficers");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.PositionName).HasColumnName("PositionName");
            this.Property(t => t.Appointment).HasColumnName("Appointment");
        }
    }
}
