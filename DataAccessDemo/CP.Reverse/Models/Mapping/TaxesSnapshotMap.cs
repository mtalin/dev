using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxesSnapshotMap : EntityTypeConfiguration<TaxesSnapshot>
    {
        public TaxesSnapshotMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TaxesSnapshot");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Instance).HasColumnName("Instance");
            this.Property(t => t.Tax).HasColumnName("Tax");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Adjustment).HasColumnName("Adjustment");
            this.Property(t => t.FeeType).HasColumnName("FeeType");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.StepDate).HasColumnName("StepDate");
            this.Property(t => t.Operation).HasColumnName("Operation");
            this.Property(t => t.Previous).HasColumnName("Previous");
            this.Property(t => t.IsLatest).HasColumnName("IsLatest");

            // Relationships
            this.HasRequired(t => t.TaxesSnapshotInstance)
                .WithMany(t => t.TaxesSnapshots)
                .HasForeignKey(d => d.Instance);

        }
    }
}
