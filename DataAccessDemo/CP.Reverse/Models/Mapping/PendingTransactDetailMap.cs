using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PendingTransactDetailMap : EntityTypeConfiguration<PendingTransactDetail>
    {
        public PendingTransactDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Amount, t.Type, t.DtCreated, t.Step, t.TargetName, t.TypeName, t.FoundationName });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Reason)
                .HasMaxLength(300);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TargetName)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.TypeName)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.LongTypeName)
                .HasMaxLength(300);

            this.Property(t => t.TypeInfo)
                .HasMaxLength(150);

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.FinancialPartnerName)
                .HasMaxLength(500);

            this.Property(t => t.FinancialAdvisorName)
                .HasMaxLength(152);

            // Table & Column Mappings
            this.ToTable("PendingTransactDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Reason).HasColumnName("Reason");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtFaxGenerated).HasColumnName("DtFaxGenerated");
            this.Property(t => t.DtFaxAuthor).HasColumnName("DtFaxAuthor");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");
            this.Property(t => t.PeriodType).HasColumnName("PeriodType");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.TransferGroup).HasColumnName("TransferGroup");
            this.Property(t => t.Payment).HasColumnName("Payment");
            this.Property(t => t.LastQTransaction).HasColumnName("LastQTransaction");
            this.Property(t => t.ACHTransact).HasColumnName("ACHTransact");
            this.Property(t => t.Outside).HasColumnName("Outside");
            this.Property(t => t.TargetName).HasColumnName("TargetName");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.LongTypeName).HasColumnName("LongTypeName");
            this.Property(t => t.TypeInfo).HasColumnName("TypeInfo");
            this.Property(t => t.Recurring).HasColumnName("Recurring");
            this.Property(t => t.Donation).HasColumnName("Donation");
            this.Property(t => t.Tax).HasColumnName("Tax");
            this.Property(t => t.Expense).HasColumnName("Expense");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.FinancialAdvisor).HasColumnName("FinancialAdvisor");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.FinancialPartnerName).HasColumnName("FinancialPartnerName");
            this.Property(t => t.FinancialAdvisorName).HasColumnName("FinancialAdvisorName");
            this.Property(t => t.TaxPeriod).HasColumnName("TaxPeriod");
            this.Property(t => t.TaxPeriodType).HasColumnName("TaxPeriodType");
        }
    }
}
