using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QuestSummaryMap : EntityTypeConfiguration<QuestSummary>
    {
        public QuestSummaryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.DtCreated, t.Author, t.Questionnaire });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Questionnaire)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.dtResolved)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("QuestSummaries");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Questionnaire).HasColumnName("Questionnaire");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.HasIssues).HasColumnName("HasIssues");
            this.Property(t => t.Resolved).HasColumnName("Resolved");
            this.Property(t => t.Enabled).HasColumnName("Enabled");
            this.Property(t => t.ResolvedBy).HasColumnName("ResolvedBy");
            this.Property(t => t.dtResolved).HasColumnName("dtResolved");
            this.Property(t => t.fsAddrIssuesBy).HasColumnName("fsAddrIssuesBy");
        }
    }
}
