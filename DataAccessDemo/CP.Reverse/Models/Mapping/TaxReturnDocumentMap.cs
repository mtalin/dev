using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReturnDocumentMap : EntityTypeConfiguration<TaxReturnDocument>
    {
        public TaxReturnDocumentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TaxReturnDocuments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.TaxReturn).HasColumnName("TaxReturn");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.XmlData).HasColumnName("XmlData");
            this.Property(t => t.Template).HasColumnName("Template");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.Theme).HasColumnName("Theme");
            this.Property(t => t.TaxReturnForm).HasColumnName("TaxReturnForm");

            // Relationships
            this.HasRequired(t => t.Document1)
                .WithMany(t => t.TaxReturnDocuments)
                .HasForeignKey(d => d.Document);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.TaxReturnDocuments)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.TaxReturnDocuments1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasOptional(t => t.TaxReturnForm1)
                .WithMany(t => t.TaxReturnDocuments)
                .HasForeignKey(d => d.TaxReturnForm);
            this.HasRequired(t => t.TaxReturn1)
                .WithMany(t => t.TaxReturnDocuments)
                .HasForeignKey(d => d.TaxReturn);

        }
    }
}
