using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class StructuredCompanyMap : EntityTypeConfiguration<StructuredCompany>
    {
        public StructuredCompanyMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Fax)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("StructuredCompanies");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.dtActivated).HasColumnName("dtActivated");

            // Relationships
            this.HasRequired(t => t.CompanyElement)
                .WithOptional(t => t.StructuredCompany);
            this.HasOptional(t => t.PostalAddress1)
                .WithMany(t => t.StructuredCompanies)
                .HasForeignKey(d => d.PostalAddress);

        }
    }
}
