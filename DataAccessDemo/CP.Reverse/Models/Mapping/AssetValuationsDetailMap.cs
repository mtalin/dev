using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AssetValuationsDetailMap : EntityTypeConfiguration<AssetValuationsDetail>
    {
        public AssetValuationsDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.AssetBook, t.Operation, t.DtOperation, t.Amount, t.DtCreated, t.Author, t.Foundation, t.AssetType, t.dtTransact });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AssetBook)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Operation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AppCompany)
                .HasMaxLength(150);

            this.Property(t => t.AppContact)
                .HasMaxLength(150);

            this.Property(t => t.AppPhone)
                .HasMaxLength(10);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AssetType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AssetTypeName)
                .HasMaxLength(150);

            this.Property(t => t.AssetTypeFlags)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("AssetValuationsDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.AssetBook).HasColumnName("AssetBook");
            this.Property(t => t.Operation).HasColumnName("Operation");
            this.Property(t => t.DtOperation).HasColumnName("DtOperation");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.AppCompany).HasColumnName("AppCompany");
            this.Property(t => t.AppContact).HasColumnName("AppContact");
            this.Property(t => t.AppPhone).HasColumnName("AppPhone");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtAppraisal).HasColumnName("DtAppraisal");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.AssetType).HasColumnName("AssetType");
            this.Property(t => t.AssetTypeName).HasColumnName("AssetTypeName");
            this.Property(t => t.AssetTypeFlags).HasColumnName("AssetTypeFlags");
            this.Property(t => t.dtTransact).HasColumnName("dtTransact");
            this.Property(t => t.NextDtTransact).HasColumnName("NextDtTransact");
        }
    }
}
