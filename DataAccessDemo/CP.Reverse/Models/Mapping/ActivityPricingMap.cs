using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ActivityPricingMap : EntityTypeConfiguration<ActivityPricing>
    {
        public ActivityPricingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ActivityPricings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PricingScheduleInstance).HasColumnName("PricingScheduleInstance");
            this.Property(t => t.UptoAmount).HasColumnName("UptoAmount");
            this.Property(t => t.BaseAmount).HasColumnName("BaseAmount");
            this.Property(t => t.FeeValue).HasColumnName("FeeValue");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ActivityPricings)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.ActivityPricings1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasRequired(t => t.PricingScheduleInstance1)
                .WithMany(t => t.ActivityPricings)
                .HasForeignKey(d => d.PricingScheduleInstance);

        }
    }
}
