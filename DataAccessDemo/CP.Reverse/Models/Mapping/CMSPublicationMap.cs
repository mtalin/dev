using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CMSPublicationMap : EntityTypeConfiguration<CMSPublication>
    {
        public CMSPublicationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Header)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.SubHeader)
                .HasMaxLength(500);

            this.Property(t => t.Text)
                .IsRequired();

            this.Property(t => t.Link)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("CMSPublications");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.IssueDate).HasColumnName("IssueDate");
            this.Property(t => t.Header).HasColumnName("Header");
            this.Property(t => t.SubHeader).HasColumnName("SubHeader");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.PDF).HasColumnName("PDF");
            this.Property(t => t.Link).HasColumnName("Link");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CMSPublications)
                .HasForeignKey(d => d.Author);

        }
    }
}
