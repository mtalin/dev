using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class StaffingProposalMap : EntityTypeConfiguration<StaffingProposal>
    {
        public StaffingProposalMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("StaffingProposals");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Proposal).HasColumnName("Proposal");
            this.Property(t => t.Excluded).HasColumnName("Excluded");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.TheAction).HasColumnName("TheAction");
            this.Property(t => t.Appointment).HasColumnName("Appointment");
            this.Property(t => t.OldAppointment).HasColumnName("OldAppointment");
            this.Property(t => t.DetectedProblems).HasColumnName("DetectedProblems");

            // Relationships
            this.HasRequired(t => t.Proposal1)
                .WithMany(t => t.StaffingProposals)
                .HasForeignKey(d => d.Proposal);

        }
    }
}
