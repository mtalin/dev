using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class enum_CRPendingsMap : EntityTypeConfiguration<enum_CRPendings>
    {
        public enum_CRPendingsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ItemID, t.Name });

            // Properties
            this.Property(t => t.ItemID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("enum_CRPendings");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.sortOrder).HasColumnName("sortOrder");
        }
    }
}
