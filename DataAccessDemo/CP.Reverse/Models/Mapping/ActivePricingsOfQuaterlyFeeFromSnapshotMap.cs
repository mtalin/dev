using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ActivePricingsOfQuaterlyFeeFromSnapshotMap : EntityTypeConfiguration<ActivePricingsOfQuaterlyFeeFromSnapshot>
    {
        public ActivePricingsOfQuaterlyFeeFromSnapshotMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("ActivePricingsOfQuaterlyFeeFromSnapshot");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PsID).HasColumnName("PsID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.DtStart).HasColumnName("DtStart");
            this.Property(t => t.DtEnd).HasColumnName("DtEnd");
            this.Property(t => t.Limit).HasColumnName("Limit");
            this.Property(t => t.Addend).HasColumnName("Addend");
            this.Property(t => t.Multiplier).HasColumnName("Multiplier");
            this.Property(t => t.From).HasColumnName("From");
            this.Property(t => t.To).HasColumnName("To");
        }
    }
}
