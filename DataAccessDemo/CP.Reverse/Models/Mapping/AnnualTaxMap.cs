using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AnnualTaxMap : EntityTypeConfiguration<AnnualTax>
    {
        public AnnualTaxMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Type, t.Foundation, t.Amount });

            // Properties
            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("AnnualTaxes");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.dtPeriod).HasColumnName("dtPeriod");
        }
    }
}
