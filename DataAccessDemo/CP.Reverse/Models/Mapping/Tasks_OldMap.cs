using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class Tasks_OldMap : EntityTypeConfiguration<Tasks_Old>
    {
        public Tasks_OldMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Tasks_Old");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtStart).HasColumnName("DtStart");
            this.Property(t => t.DtStop).HasColumnName("DtStop");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.CndTime).HasColumnName("CndTime");
            this.Property(t => t.Condition).HasColumnName("Condition");
            this.Property(t => t.BodyAction).HasColumnName("BodyAction");
            this.Property(t => t.PostAction).HasColumnName("PostAction");
            this.Property(t => t.OLEClass).HasColumnName("OLEClass");
            this.Property(t => t.ReturnCode).HasColumnName("ReturnCode");

            // Relationships
            this.HasRequired(t => t.OLEClass1)
                .WithMany(t => t.Tasks_Old)
                .HasForeignKey(d => d.OLEClass);

        }
    }
}
