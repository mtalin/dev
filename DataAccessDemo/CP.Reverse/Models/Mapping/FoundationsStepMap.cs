using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FoundationsStepMap : EntityTypeConfiguration<FoundationsStep>
    {
        public FoundationsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("FoundationsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.FoundationsSteps)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.User)
                .WithMany(t => t.FoundationsSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
