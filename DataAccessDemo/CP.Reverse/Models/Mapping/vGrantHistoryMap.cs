using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vGrantHistoryMap : EntityTypeConfiguration<vGrantHistory>
    {
        public vGrantHistoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.RecipientType, t.GrantDetails });

            // Properties
            this.Property(t => t.RecipientType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RecipientName)
                .HasMaxLength(200);

            this.Property(t => t.SortRecipientName)
                .HasMaxLength(200);

            this.Property(t => t.ProgramName)
                .HasMaxLength(200);

            this.Property(t => t.SubProgramName)
                .HasMaxLength(200);

            this.Property(t => t.FullProgramName)
                .HasMaxLength(500);

            this.Property(t => t.SortProgramName)
                .HasMaxLength(500);

            this.Property(t => t.GrantorName)
                .HasMaxLength(200);

            this.Property(t => t.SortGrantorName)
                .HasMaxLength(200);

            this.Property(t => t.SpecificPurposeText1)
                .HasMaxLength(1000);

            this.Property(t => t.SpecificPurposeText2)
                .HasMaxLength(1000);

            this.Property(t => t.SpecificPurposeName)
                .HasMaxLength(500);

            this.Property(t => t.SortSpecificPurposeName)
                .HasMaxLength(500);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.Purpose)
                .HasMaxLength(50);

            this.Property(t => t.PurposeDescription)
                .HasMaxLength(1000);

            this.Property(t => t.GrantDetails)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vGrantHistory");
            this.Property(t => t.RecipientType).HasColumnName("RecipientType");
            this.Property(t => t.GrantID).HasColumnName("GrantID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.RecipientID).HasColumnName("RecipientID");
            this.Property(t => t.RecipientName).HasColumnName("RecipientName");
            this.Property(t => t.SortRecipientName).HasColumnName("SortRecipientName");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.ProgramID).HasColumnName("ProgramID");
            this.Property(t => t.SubProgramID).HasColumnName("SubProgramID");
            this.Property(t => t.ParentProgramID).HasColumnName("ParentProgramID");
            this.Property(t => t.ProgramName).HasColumnName("ProgramName");
            this.Property(t => t.SubProgramName).HasColumnName("SubProgramName");
            this.Property(t => t.FullProgramName).HasColumnName("FullProgramName");
            this.Property(t => t.SortProgramName).HasColumnName("SortProgramName");
            this.Property(t => t.GrantorID).HasColumnName("GrantorID");
            this.Property(t => t.GrantorName).HasColumnName("GrantorName");
            this.Property(t => t.SortGrantorName).HasColumnName("SortGrantorName");
            this.Property(t => t.GrantProposalID).HasColumnName("GrantProposalID");
            this.Property(t => t.SpecificPurposeType).HasColumnName("SpecificPurposeType");
            this.Property(t => t.SpecificPurposeText1).HasColumnName("SpecificPurposeText1");
            this.Property(t => t.SpecificPurposeText2).HasColumnName("SpecificPurposeText2");
            this.Property(t => t.SpecificPurposeName).HasColumnName("SpecificPurposeName");
            this.Property(t => t.SortSpecificPurposeName).HasColumnName("SortSpecificPurposeName");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.TransactID).HasColumnName("TransactID");
            this.Property(t => t.GrantLetter).HasColumnName("GrantLetter");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.PurposeDescription).HasColumnName("PurposeDescription");
            this.Property(t => t.GrantDetails).HasColumnName("GrantDetails");
        }
    }
}
