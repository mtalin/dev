using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CertifiedMailMap : EntityTypeConfiguration<CertifiedMail>
    {
        public CertifiedMailMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TrackingNumber)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("CertifiedMails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.TrackingNumber).HasColumnName("TrackingNumber");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.DtSent).HasColumnName("DtSent");
            this.Property(t => t.DtResponded).HasColumnName("DtResponded");
            this.Property(t => t.ReceiptFiled).HasColumnName("ReceiptFiled");
            this.Property(t => t.ResponseReceived).HasColumnName("ResponseReceived");
            this.Property(t => t.ResponseFiled).HasColumnName("ResponseFiled");

            // Relationships
            this.HasRequired(t => t.Document1)
                .WithMany(t => t.CertifiedMails)
                .HasForeignKey(d => d.Document);

        }
    }
}
