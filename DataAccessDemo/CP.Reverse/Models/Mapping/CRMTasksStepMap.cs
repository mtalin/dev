using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CRMTasksStepMap : EntityTypeConfiguration<CRMTasksStep>
    {
        public CRMTasksStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CRMTasksSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.CRMTask).HasColumnName("CRMTask");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.CRMTask1)
                .WithMany(t => t.CRMTasksSteps)
                .HasForeignKey(d => d.CRMTask);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.CRMTasksSteps)
                .HasForeignKey(d => d.Note);
            this.HasRequired(t => t.User)
                .WithMany(t => t.CRMTasksSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
