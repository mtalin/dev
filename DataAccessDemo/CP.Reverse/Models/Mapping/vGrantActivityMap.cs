using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vGrantActivityMap : EntityTypeConfiguration<vGrantActivity>
    {
        public vGrantActivityMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Nominee, t.CompanyPersonId, t.Person, t.StructuredCompany, t.ApproveGrants, t.GrantCommitteeMember });

            // Properties
            this.Property(t => t.Nominee)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CompanyPersonId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Person)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vGrantActivity");
            this.Property(t => t.Nominee).HasColumnName("Nominee");
            this.Property(t => t.CompanyPersonId).HasColumnName("CompanyPersonId");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.Made).HasColumnName("Made");
            this.Property(t => t.Completed).HasColumnName("Completed");
            this.Property(t => t.Pending).HasColumnName("Pending");
            this.Property(t => t.WaitingRequest).HasColumnName("WaitingRequest");
            this.Property(t => t.WaitingApproval).HasColumnName("WaitingApproval");
            this.Property(t => t.Remaining).HasColumnName("Remaining");
            this.Property(t => t.GrantingLimit).HasColumnName("GrantingLimit");
            this.Property(t => t.ApproveGrants).HasColumnName("ApproveGrants");
            this.Property(t => t.GrantCommitteeMember).HasColumnName("GrantCommitteeMember");
        }
    }
}
