using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EstimatedTaxMap : EntityTypeConfiguration<EstimatedTax>
    {
        public EstimatedTaxMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("EstimatedTaxes");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtStartPeriod).HasColumnName("DtStartPeriod");
            this.Property(t => t.DtEndPeriod).HasColumnName("DtEndPeriod");
            this.Property(t => t.DtRepaymentTax).HasColumnName("DtRepaymentTax");
            this.Property(t => t.DtSentToReview).HasColumnName("DtSentToReview");
            this.Property(t => t.DtSentToFinance).HasColumnName("DtSentToFinance");
            this.Property(t => t.DtSentToPaid).HasColumnName("DtSentToPaid");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.Quarter).HasColumnName("Quarter");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.NetInvInc).HasColumnName("NetInvInc");
            this.Property(t => t.AnnualizedTaxLiability).HasColumnName("AnnualizedTaxLiability");
            this.Property(t => t.AmountTaxProcessed).HasColumnName("AmountTaxProcessed");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Tax).HasColumnName("Tax");
            this.Property(t => t.Method).HasColumnName("Method");
            this.Property(t => t.XmlData).HasColumnName("XmlData");
            this.Property(t => t.dtLastRecalc).HasColumnName("dtLastRecalc");
            this.Property(t => t.dtSentToPendingConfirm).HasColumnName("dtSentToPendingConfirm");
            this.Property(t => t.dtSentToPendingHold).HasColumnName("dtSentToPendingHold");
            this.Property(t => t.dtSentToPendingReject).HasColumnName("dtSentToPendingReject");
            this.Property(t => t.QtrAmount).HasColumnName("QtrAmount");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.EstimatedTaxes)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.EstimatedTaxes)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Tax1)
                .WithMany(t => t.EstimatedTaxes)
                .HasForeignKey(d => d.Tax);

        }
    }
}
