using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vUnassignedDepositMap : EntityTypeConfiguration<vUnassignedDeposit>
    {
        public vUnassignedDepositMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.DtTransact, t.Type, t.FoundationName, t.Foundation, t.FinancialPartner });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Symbol)
                .HasMaxLength(32);

            this.Property(t => t.AssetName)
                .HasMaxLength(100);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TypeName)
                .HasMaxLength(17);

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FinancialPartner)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vUnassignedDeposits");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.Symbol).HasColumnName("Symbol");
            this.Property(t => t.AssetName).HasColumnName("AssetName");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Shares).HasColumnName("Shares");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.ChildID).HasColumnName("ChildID");
            this.Property(t => t.ChildAmount).HasColumnName("ChildAmount");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.CheckRequest).HasColumnName("CheckRequest");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
        }
    }
}
