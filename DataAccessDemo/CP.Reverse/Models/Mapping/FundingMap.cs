using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FundingMap : EntityTypeConfiguration<Funding>
    {
        public FundingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.EFTBankName)
                .HasMaxLength(250);

            this.Property(t => t.AccountNumber)
                .HasMaxLength(14);

            this.Property(t => t.RoutingNumber)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.StockTransferDestination)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Fundings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.RecurringID).HasColumnName("RecurringID");
            this.Property(t => t.RecurringFreq).HasColumnName("RecurringFreq");
            this.Property(t => t.EFTBankName).HasColumnName("EFTBankName");
            this.Property(t => t.AccountNumber).HasColumnName("AccountNumber");
            this.Property(t => t.RoutingNumber).HasColumnName("RoutingNumber");
            this.Property(t => t.StockTransferDestination).HasColumnName("StockTransferDestination");
            this.Property(t => t.Step).HasColumnName("Step");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Fundings)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
