using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class tmp_GrantsImportMap : EntityTypeConfiguration<tmp_GrantsImport>
    {
        public tmp_GrantsImportMap()
        {
            // Primary Key
            this.HasKey(t => new { t.DtGrant, t.Amount });

            // Properties
            this.Property(t => t.CharityName)
                .HasMaxLength(500);

            this.Property(t => t.Address)
                .HasMaxLength(255);

            this.Property(t => t.City)
                .HasMaxLength(255);

            this.Property(t => t.State)
                .HasMaxLength(255);

            this.Property(t => t.Zip)
                .HasMaxLength(9);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SpecPurposeText1)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("tmp_GrantsImport");
            this.Property(t => t.PublicEIN).HasColumnName("PublicEIN");
            this.Property(t => t.CharityName).HasColumnName("CharityName");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.GrantorID).HasColumnName("GrantorID");
            this.Property(t => t.DtGrant).HasColumnName("DtGrant");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.SpecPurposeType).HasColumnName("SpecPurposeType");
            this.Property(t => t.SpecPurposeText1).HasColumnName("SpecPurposeText1");
        }
    }
}
