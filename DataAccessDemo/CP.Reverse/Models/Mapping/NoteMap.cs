using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class NoteMap : EntityTypeConfiguration<Note>
    {
        public NoteMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Body)
                .IsRequired()
                .HasMaxLength(3000);

            // Table & Column Mappings
            this.ToTable("Notes");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.NoteCategory).HasColumnName("NoteCategory");
            this.Property(t => t.ShellCorporation).HasColumnName("ShellCorporation");
            this.Property(t => t.Foundation).HasColumnName("Foundation");

            // Relationships
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.Notes)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.NoteCategory1)
                .WithMany(t => t.Notes)
                .HasForeignKey(d => d.NoteCategory);
            this.HasOptional(t => t.ShellCorporation1)
                .WithMany(t => t.Notes)
                .HasForeignKey(d => d.ShellCorporation);
            this.HasOptional(t => t.User)
                .WithMany(t => t.Notes)
                .HasForeignKey(d => d.Author);

        }
    }
}
