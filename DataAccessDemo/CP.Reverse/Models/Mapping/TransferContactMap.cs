using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TransferContactMap : EntityTypeConfiguration<TransferContact>
    {
        public TransferContactMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TransferContacts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Contact).HasColumnName("Contact");
            this.Property(t => t.Primary).HasColumnName("Primary");
            this.Property(t => t.CC).HasColumnName("CC");
            this.Property(t => t.NotifyByFax).HasColumnName("NotifyByFax");
            this.Property(t => t.NotifyByEmail).HasColumnName("NotifyByEmail");
            this.Property(t => t.TransferProfile).HasColumnName("TransferProfile");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.TransferContacts)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.TransferContacts1)
                .HasForeignKey(d => d.Contact);
            this.HasOptional(t => t.Person2)
                .WithMany(t => t.TransferContacts2)
                .HasForeignKey(d => d.DeletedBy);

        }
    }
}
