using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AllTenOuFoundationMap : EntityTypeConfiguration<AllTenOuFoundation>
    {
        public AllTenOuFoundationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("AllTenOuFoundations");
            this.Property(t => t.ID).HasColumnName("ID");
        }
    }
}
