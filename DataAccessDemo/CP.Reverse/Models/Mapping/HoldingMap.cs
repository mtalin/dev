using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class HoldingMap : EntityTypeConfiguration<Holding>
    {
        public HoldingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Holdings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.FirstLot).HasColumnName("FirstLot");
            this.Property(t => t.ParentLot).HasColumnName("ParentLot");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeposit).HasColumnName("DtDeposit");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.DtClosed2).HasColumnName("DtClosed2");
            this.Property(t => t.HasChild).HasColumnName("HasChild");
            this.Property(t => t.IsRoot).HasColumnName("IsRoot");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.TransactType).HasColumnName("TransactType");
            this.Property(t => t.UnitsNumber).HasColumnName("UnitsNumber");
            this.Property(t => t.PackageSize).HasColumnName("PackageSize");
            this.Property(t => t.V_SalePrice).HasColumnName("V_SalePrice");
            this.Property(t => t.V_CostBasis).HasColumnName("V_CostBasis");
            this.Property(t => t.V_BookBasis).HasColumnName("V_BookBasis");
            this.Property(t => t.V_Interest).HasColumnName("V_Interest");
            this.Property(t => t.V_Dividend).HasColumnName("V_Dividend");
            this.Property(t => t.V_CapitalGain).HasColumnName("V_CapitalGain");
            this.Property(t => t.V_PhantomGain).HasColumnName("V_PhantomGain");
            this.Property(t => t.V_InterestAdj).HasColumnName("V_InterestAdj");
        }
    }
}
