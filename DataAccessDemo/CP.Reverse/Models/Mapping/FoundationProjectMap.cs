using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FoundationProjectMap : EntityTypeConfiguration<FoundationProject>
    {
        public FoundationProjectMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Code)
                .HasMaxLength(10);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("FoundationProjects");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Parent).HasColumnName("Parent");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.AuthorDeleted).HasColumnName("AuthorDeleted");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Type).HasColumnName("Type");

            // Relationships
            this.HasOptional(t => t.FoundationProject1)
                .WithMany(t => t.FoundationProjects1)
                .HasForeignKey(d => d.Parent);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.FoundationProjects)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.FoundationProjects)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.FoundationProjects1)
                .HasForeignKey(d => d.AuthorDeleted);

        }
    }
}
