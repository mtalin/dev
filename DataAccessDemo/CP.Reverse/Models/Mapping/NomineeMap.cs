using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class NomineeMap : EntityTypeConfiguration<Nominee>
    {
        public NomineeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Person, t.RegistrationStep, t.Reelection, t.GrantCommitteeMember, t.ApproveGrants });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Person)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RegistrationStep)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Reelection)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("Nominees");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Appointment).HasColumnName("Appointment");
            this.Property(t => t.DonationsLimit).HasColumnName("DonationsLimit");
            this.Property(t => t.RegistrationStep).HasColumnName("RegistrationStep");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.NewPosition).HasColumnName("NewPosition");
            this.Property(t => t.StaffingProposal).HasColumnName("StaffingProposal");
            this.Property(t => t.DtAccDeadline).HasColumnName("DtAccDeadline");
            this.Property(t => t.DeclaredAppointment).HasColumnName("DeclaredAppointment");
            this.Property(t => t.DtAccepted).HasColumnName("DtAccepted");
            this.Property(t => t.Reelection).HasColumnName("Reelection");
            this.Property(t => t.ApprovableLimit).HasColumnName("ApprovableLimit");
            this.Property(t => t.GrantCommitteeMember).HasColumnName("GrantCommitteeMember");
            this.Property(t => t.DtTermsExpired).HasColumnName("DtTermsExpired");
            this.Property(t => t.ApproveGrants).HasColumnName("ApproveGrants");
        }
    }
}
