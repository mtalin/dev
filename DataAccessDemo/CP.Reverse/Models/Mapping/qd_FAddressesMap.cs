using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class qd_FAddressesMap : EntityTypeConfiguration<qd_FAddresses>
    {
        public qd_FAddressesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.DtCreated, t.Questionnaire });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Questionnaire)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            this.Property(t => t.address1)
                .HasMaxLength(500);

            this.Property(t => t.address2)
                .HasMaxLength(500);

            this.Property(t => t.city)
                .HasMaxLength(200);

            this.Property(t => t.state)
                .HasMaxLength(50);

            this.Property(t => t.zip)
                .HasMaxLength(10);

            this.Property(t => t.country)
                .HasMaxLength(2);

            this.Property(t => t.phone)
                .HasMaxLength(20);

            this.Property(t => t.careof)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("qd_FAddresses");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Questionnaire).HasColumnName("Questionnaire");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.useFsAddress).HasColumnName("useFsAddress");
            this.Property(t => t.isNew).HasColumnName("isNew");
            this.Property(t => t.updated).HasColumnName("updated");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.address1).HasColumnName("address1");
            this.Property(t => t.address2).HasColumnName("address2");
            this.Property(t => t.city).HasColumnName("city");
            this.Property(t => t.state).HasColumnName("state");
            this.Property(t => t.zip).HasColumnName("zip");
            this.Property(t => t.country).HasColumnName("country");
            this.Property(t => t.phone).HasColumnName("phone");
            this.Property(t => t.careof).HasColumnName("careof");
        }
    }
}
