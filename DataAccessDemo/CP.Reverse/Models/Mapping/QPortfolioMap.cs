using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QPortfolioMap : EntityTypeConfiguration<QPortfolio>
    {
        public QPortfolioMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.DtCreated, t.Foundation, t.Security, t.CostBasis, t.PreTaxGain, t.Balance });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Security)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.CostBasis)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PreTaxGain)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Balance)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("QPortfolio");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Security).HasColumnName("Security");
            this.Property(t => t.Shares).HasColumnName("Shares");
            this.Property(t => t.Price).HasColumnName("Price");
            this.Property(t => t.CostBasis).HasColumnName("CostBasis");
            this.Property(t => t.PreTaxGain).HasColumnName("PreTaxGain");
            this.Property(t => t.Balance).HasColumnName("Balance");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.QPortfolios)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
