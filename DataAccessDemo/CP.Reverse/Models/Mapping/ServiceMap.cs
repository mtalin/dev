using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ServiceMap : EntityTypeConfiguration<Service>
    {
        public ServiceMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Name });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Text)
                .HasMaxLength(300);

            this.Property(t => t.Flags)
                .HasMaxLength(150);

            this.Property(t => t.Tag)
                .HasMaxLength(256);

            // Table & Column Mappings
            this.ToTable("Services");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.Flags).HasColumnName("Flags");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.Tag).HasColumnName("Tag");
        }
    }
}
