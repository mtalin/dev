using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vCompanyElementsWithTypeMap : EntityTypeConfiguration<vCompanyElementsWithType>
    {
        public vCompanyElementsWithTypeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            this.Property(t => t.Object)
                .HasMaxLength(19);

            // Table & Column Mappings
            this.ToTable("vCompanyElementsWithType");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");
            this.Property(t => t.Object).HasColumnName("Object");
        }
    }
}
