using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TransactsDonorMap : EntityTypeConfiguration<TransactsDonor>
    {
        public TransactsDonorMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.InCareOf)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TransactsDonors");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.ChildAmount).HasColumnName("ChildAmount");
            this.Property(t => t.Donor).HasColumnName("Donor");
            this.Property(t => t.PhantomDonor).HasColumnName("PhantomDonor");
            this.Property(t => t.Spouse).HasColumnName("Spouse");
            this.Property(t => t.InCareOf).HasColumnName("InCareOf");
            this.Property(t => t.IsDonorAlive).HasColumnName("IsDonorAlive");
            this.Property(t => t.IsSpouseAlive).HasColumnName("IsSpouseAlive");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.GoodsServices).HasColumnName("GoodsServices");

            // Relationships
            this.HasOptional(t => t.Donor1)
                .WithMany(t => t.TransactsDonors)
                .HasForeignKey(d => d.Donor);
            this.HasOptional(t => t.Donor2)
                .WithMany(t => t.TransactsDonors1)
                .HasForeignKey(d => d.PhantomDonor);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.TransactsDonors)
                .HasForeignKey(d => d.Spouse);
            this.HasRequired(t => t.Transact1)
                .WithMany(t => t.TransactsDonors)
                .HasForeignKey(d => d.Transact);

        }
    }
}
