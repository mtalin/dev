using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_CharitiesEditMap : EntityTypeConfiguration<et_CharitiesEdit>
    {
        public et_CharitiesEditMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.OrgName)
                .HasMaxLength(200);

            this.Property(t => t.AltName)
                .HasMaxLength(200);

            this.Property(t => t.InCareOf)
                .HasMaxLength(100);

            this.Property(t => t.Address)
                .HasMaxLength(100);

            this.Property(t => t.Address2)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .HasMaxLength(30);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZipPlus4)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.Country)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(15);

            this.Property(t => t.URL)
                .HasMaxLength(150);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.AltAddress)
                .HasMaxLength(100);

            this.Property(t => t.AltAddress2)
                .HasMaxLength(100);

            this.Property(t => t.AltCity)
                .HasMaxLength(30);

            this.Property(t => t.AltState)
                .HasMaxLength(30);

            this.Property(t => t.AltZipPlus4)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("et_CharitiesEdit");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.PublicEIN).HasColumnName("PublicEIN");
            this.Property(t => t.OrgName).HasColumnName("OrgName");
            this.Property(t => t.AltName).HasColumnName("AltName");
            this.Property(t => t.InCareOf).HasColumnName("InCareOf");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZipPlus4).HasColumnName("ZipPlus4");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.AltAddress).HasColumnName("AltAddress");
            this.Property(t => t.AltAddress2).HasColumnName("AltAddress2");
            this.Property(t => t.AltCity).HasColumnName("AltCity");
            this.Property(t => t.AltState).HasColumnName("AltState");
            this.Property(t => t.AltZipPlus4).HasColumnName("AltZipPlus4");
        }
    }
}
