using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class SubtaskMap : EntityTypeConfiguration<Subtask>
    {
        public SubtaskMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Task, t.Subtask1 });

            // Properties
            this.Property(t => t.Task)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Subtask1)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("Subtasks");
            this.Property(t => t.Task).HasColumnName("Task");
            this.Property(t => t.Subtask1).HasColumnName("Subtask");
            this.Property(t => t.SeqOrder).HasColumnName("SeqOrder");

            // Relationships
            this.HasRequired(t => t.Tasks_Old)
                .WithMany(t => t.Subtasks)
                .HasForeignKey(d => d.Task);
            this.HasRequired(t => t.Tasks_Old1)
                .WithMany(t => t.Subtasks1)
                .HasForeignKey(d => d.Subtask1);

        }
    }
}
