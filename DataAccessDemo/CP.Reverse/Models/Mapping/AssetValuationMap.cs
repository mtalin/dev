using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AssetValuationMap : EntityTypeConfiguration<AssetValuation>
    {
        public AssetValuationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.AppCompany)
                .HasMaxLength(150);

            this.Property(t => t.AppContact)
                .HasMaxLength(150);

            this.Property(t => t.AppPhone)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("AssetValuations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.AssetBook).HasColumnName("AssetBook");
            this.Property(t => t.Operation).HasColumnName("Operation");
            this.Property(t => t.DtOperation).HasColumnName("DtOperation");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.AppCompany).HasColumnName("AppCompany");
            this.Property(t => t.AppContact).HasColumnName("AppContact");
            this.Property(t => t.AppPhone).HasColumnName("AppPhone");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtAppraisal).HasColumnName("DtAppraisal");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasRequired(t => t.AssetBook1)
                .WithMany(t => t.AssetValuations)
                .HasForeignKey(d => d.AssetBook);
            this.HasOptional(t => t.Document1)
                .WithMany(t => t.AssetValuations)
                .HasForeignKey(d => d.Document);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.AssetValuations)
                .HasForeignKey(d => d.Note);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.AssetValuations)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Transact1)
                .WithMany(t => t.AssetValuations)
                .HasForeignKey(d => d.Transact);

        }
    }
}
