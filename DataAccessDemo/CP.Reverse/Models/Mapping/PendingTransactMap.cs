using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PendingTransactMap : EntityTypeConfiguration<PendingTransact>
    {
        public PendingTransactMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Reason)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("PendingTransacts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Reason).HasColumnName("Reason");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtFaxGenerated).HasColumnName("DtFaxGenerated");
            this.Property(t => t.DtFaxAuthor).HasColumnName("DtFaxAuthor");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");
            this.Property(t => t.PeriodType).HasColumnName("PeriodType");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.TransferGroup).HasColumnName("TransferGroup");
            this.Property(t => t.Payment).HasColumnName("Payment");
            this.Property(t => t.LastQTransaction).HasColumnName("LastQTransaction");
            this.Property(t => t.ACHTransact).HasColumnName("ACHTransact");
            this.Property(t => t.Outside).HasColumnName("Outside");

            // Relationships
            this.HasOptional(t => t.ACHTransact1)
                .WithMany(t => t.PendingTransacts)
                .HasForeignKey(d => d.ACHTransact);
            this.HasOptional(t => t.FoundationAccount1)
                .WithMany(t => t.PendingTransacts)
                .HasForeignKey(d => d.FoundationAccount);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.PendingTransacts)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Payment1)
                .WithMany(t => t.PendingTransacts)
                .HasForeignKey(d => d.Payment);
            this.HasOptional(t => t.Transact1)
                .WithMany(t => t.PendingTransacts)
                .HasForeignKey(d => d.Transact);
            this.HasOptional(t => t.TransferGroup1)
                .WithMany(t => t.PendingTransacts)
                .HasForeignKey(d => d.TransferGroup);

        }
    }
}
