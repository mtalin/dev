using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FAPPackageRequestMap : EntityTypeConfiguration<FAPPackageRequest>
    {
        public FAPPackageRequestMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("FAPPackageRequests");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Advisor).HasColumnName("Advisor");
            this.Property(t => t.Package).HasColumnName("Package");
            this.Property(t => t.DeliveryType).HasColumnName("DeliveryType");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtSent).HasColumnName("DtSent");
            this.Property(t => t.DestinationAddress).HasColumnName("DestinationAddress");

            // Relationships
            this.HasRequired(t => t.FAPPackage)
                .WithMany(t => t.FAPPackageRequests)
                .HasForeignKey(d => d.Package);
            this.HasRequired(t => t.FinancialAdvisor)
                .WithMany(t => t.FAPPackageRequests)
                .HasForeignKey(d => d.Advisor);
            this.HasRequired(t => t.PostalAddress)
                .WithMany(t => t.FAPPackageRequests)
                .HasForeignKey(d => d.DestinationAddress);

        }
    }
}
