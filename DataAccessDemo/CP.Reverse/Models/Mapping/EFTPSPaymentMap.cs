using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EFTPSPaymentMap : EntityTypeConfiguration<EFTPSPayment>
    {
        public EFTPSPaymentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.EFTNumber)
                .IsFixedLength()
                .HasMaxLength(15);

            this.Property(t => t.TaxPeriod)
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("EFTPSPayments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Payment).HasColumnName("Payment");
            this.Property(t => t.EFTNumber).HasColumnName("EFTNumber");
            this.Property(t => t.SettlementDate).HasColumnName("SettlementDate");
            this.Property(t => t.TaxPeriod).HasColumnName("TaxPeriod");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.EFTPSPayments)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Payment1)
                .WithMany(t => t.EFTPSPayments)
                .HasForeignKey(d => d.Payment);

        }
    }
}
