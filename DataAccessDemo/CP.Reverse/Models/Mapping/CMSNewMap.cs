using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CMSNewMap : EntityTypeConfiguration<CMSNew>
    {
        public CMSNewMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Header)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.SubHeader)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.FullText)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("CMSNews");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtNews).HasColumnName("DtNews");
            this.Property(t => t.Header).HasColumnName("Header");
            this.Property(t => t.SubHeader).HasColumnName("SubHeader");
            this.Property(t => t.FullText).HasColumnName("FullText");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CMSNews)
                .HasForeignKey(d => d.Author);

        }
    }
}
