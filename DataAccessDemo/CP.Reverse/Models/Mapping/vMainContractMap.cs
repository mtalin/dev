using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vMainContractMap : EntityTypeConfiguration<vMainContract>
    {
        public vMainContractMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.ServiceProvider, t.IsActive, t.IsTemplate, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.ContractNo)
                .HasMaxLength(255);

            this.Property(t => t.ServiceProvider)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vMainContracts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ContractNo).HasColumnName("ContractNo");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.Customer).HasColumnName("Customer");
            this.Property(t => t.ParentContract).HasColumnName("ParentContract");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.IsTemplate).HasColumnName("IsTemplate");
            this.Property(t => t.DtCommence).HasColumnName("DtCommence");
            this.Property(t => t.DtTerminate).HasColumnName("DtTerminate");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtActivated).HasColumnName("DtActivated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
        }
    }
}
