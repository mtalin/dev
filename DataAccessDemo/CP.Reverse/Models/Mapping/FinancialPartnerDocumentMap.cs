using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FinancialPartnerDocumentMap : EntityTypeConfiguration<FinancialPartnerDocument>
    {
        public FinancialPartnerDocumentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("FinancialPartnerDocuments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Theme).HasColumnName("Theme");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.Ignore).HasColumnName("Ignore");

            // Relationships
            this.HasOptional(t => t.Document1)
                .WithMany(t => t.FinancialPartnerDocuments)
                .HasForeignKey(d => d.Document);

        }
    }
}
