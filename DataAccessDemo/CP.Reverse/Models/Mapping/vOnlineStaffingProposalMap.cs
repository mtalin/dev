using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vOnlineStaffingProposalMap : EntityTypeConfiguration<vOnlineStaffingProposal>
    {
        public vOnlineStaffingProposalMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Purpose, t.Step, t.Author, t.DtCreated, t.VotingID });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Purpose)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.VotingID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vOnlineStaffingProposals");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.LegalForm).HasColumnName("LegalForm");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.VotingID).HasColumnName("VotingID");
            this.Property(t => t.VoteID).HasColumnName("VoteID");
            this.Property(t => t.VotePerson).HasColumnName("VotePerson");
            this.Property(t => t.VoteAppointment).HasColumnName("VoteAppointment");
            this.Property(t => t.VoteStep).HasColumnName("VoteStep");
            this.Property(t => t.VoteDtCreated).HasColumnName("VoteDtCreated");
        }
    }
}
