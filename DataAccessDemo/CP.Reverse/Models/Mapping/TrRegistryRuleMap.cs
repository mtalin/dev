using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TrRegistryRuleMap : EntityTypeConfiguration<TrRegistryRule>
    {
        public TrRegistryRuleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.RuleXX)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.RuleUSD)
                .HasMaxLength(500);

            this.Property(t => t.RuleMF)
                .HasMaxLength(500);

            this.Property(t => t.RuleMU)
                .HasMaxLength(500);

            this.Property(t => t.RuleFI)
                .HasMaxLength(500);

            this.Property(t => t.RuleAA)
                .HasMaxLength(500);

            this.Property(t => t.RuleCR)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("TrRegistryRules");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.RuleXX).HasColumnName("RuleXX");
            this.Property(t => t.RuleUSD).HasColumnName("RuleUSD");
            this.Property(t => t.RuleMF).HasColumnName("RuleMF");
            this.Property(t => t.RuleMU).HasColumnName("RuleMU");
            this.Property(t => t.RuleFI).HasColumnName("RuleFI");
            this.Property(t => t.RuleAA).HasColumnName("RuleAA");
            this.Property(t => t.RuleCR).HasColumnName("RuleCR");
        }
    }
}
