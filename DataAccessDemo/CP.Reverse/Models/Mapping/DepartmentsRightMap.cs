using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DepartmentsRightMap : EntityTypeConfiguration<DepartmentsRight>
    {
        public DepartmentsRightMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Permissions)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.AppliedPermissions)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.InheritedPermissions)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("DepartmentsRights", "rpt");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.HigherDepartment).HasColumnName("HigherDepartment");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Permissions).HasColumnName("Permissions");
            this.Property(t => t.AppliedPermissions).HasColumnName("AppliedPermissions");
            this.Property(t => t.InheritedPermissions).HasColumnName("InheritedPermissions");
        }
    }
}
