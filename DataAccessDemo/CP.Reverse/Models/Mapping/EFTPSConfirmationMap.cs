using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EFTPSConfirmationMap : EntityTypeConfiguration<EFTPSConfirmation>
    {
        public EFTPSConfirmationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.EIN)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.taxPeriod)
                .HasMaxLength(25);

            this.Property(t => t.errors)
                .HasMaxLength(5);

            this.Property(t => t.EFTNumber)
                .HasMaxLength(15);

            this.Property(t => t.CancelEFT)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("EFTPSConfirmations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.dtCreated).HasColumnName("dtCreated");
            this.Property(t => t.EFTPSConfReport).HasColumnName("EFTPSConfReport");
            this.Property(t => t.EFTPSPayment).HasColumnName("EFTPSPayment");
            this.Property(t => t.EIN).HasColumnName("EIN");
            this.Property(t => t.taxPeriod).HasColumnName("taxPeriod");
            this.Property(t => t.amount).HasColumnName("amount");
            this.Property(t => t.errors).HasColumnName("errors");
            this.Property(t => t.TaxType).HasColumnName("TaxType");
            this.Property(t => t.EFTNumber).HasColumnName("EFTNumber");
            this.Property(t => t.SettlementDate).HasColumnName("SettlementDate");
            this.Property(t => t.CancelEFT).HasColumnName("CancelEFT");
            this.Property(t => t.Line).HasColumnName("Line");

            // Relationships
            this.HasRequired(t => t.EFTPSConfReport1)
                .WithMany(t => t.EFTPSConfirmations)
                .HasForeignKey(d => d.EFTPSConfReport);
            this.HasOptional(t => t.EFTPSPayment1)
                .WithMany(t => t.EFTPSConfirmations)
                .HasForeignKey(d => d.EFTPSPayment);

        }
    }
}
