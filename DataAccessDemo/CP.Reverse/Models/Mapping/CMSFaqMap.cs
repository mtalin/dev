using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CMSFaqMap : EntityTypeConfiguration<CMSFaq>
    {
        public CMSFaqMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Question)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Answer)
                .IsRequired()
                .HasMaxLength(5000);

            // Table & Column Mappings
            this.ToTable("CMSFaqs");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Question).HasColumnName("Question");
            this.Property(t => t.Answer).HasColumnName("Answer");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.CMSFaqs)
                .HasForeignKey(d => d.Author);

        }
    }
}
