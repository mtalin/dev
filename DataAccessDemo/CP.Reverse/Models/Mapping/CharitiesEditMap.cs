using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CharitiesEditMap : EntityTypeConfiguration<CharitiesEdit>
    {
        public CharitiesEditMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Hold, t.Step, t.UseAltName, t.UseAltAddress, t.ExpenditureResponsibilities, t.CopyOfExistingCharity, t.SignificantChanges, t.FiscalAgent, t.UseSpecPurpose });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.OrgName)
                .HasMaxLength(200);

            this.Property(t => t.AltName)
                .HasMaxLength(200);

            this.Property(t => t.InCareOf)
                .HasMaxLength(100);

            this.Property(t => t.Address)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .HasMaxLength(30);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZipPlus4)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.NTEECode)
                .IsFixedLength()
                .HasMaxLength(4);

            this.Property(t => t.SortName)
                .HasMaxLength(35);

            this.Property(t => t.BankABA)
                .HasMaxLength(22);

            this.Property(t => t.BankAccountNo)
                .HasMaxLength(22);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(15);

            this.Property(t => t.URL)
                .HasMaxLength(150);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.Hold)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.OrgInfo)
                .HasMaxLength(300);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AltAddress)
                .HasMaxLength(100);

            this.Property(t => t.AltCity)
                .HasMaxLength(30);

            this.Property(t => t.AltState)
                .HasMaxLength(30);

            this.Property(t => t.AltZipPlus4)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Country)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.ContactPerson)
                .HasMaxLength(200);

            this.Property(t => t.CheckPayableTo)
                .HasMaxLength(200);

            this.Property(t => t.CheckMemo)
                .HasMaxLength(200);

            this.Property(t => t.SpecPurposeText1)
                .HasMaxLength(200);

            this.Property(t => t.SpecPurposeText2)
                .HasMaxLength(200);

            this.Property(t => t.AltInCareOf)
                .HasMaxLength(100);

            this.Property(t => t.Address2)
                .HasMaxLength(100);

            this.Property(t => t.AltAddress2)
                .HasMaxLength(100);

            this.Property(t => t.AssociatedFoundations)
                .HasMaxLength(150);

            this.Property(t => t.AltCountry)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("CharitiesEdit");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.PublicEIN).HasColumnName("PublicEIN");
            this.Property(t => t.OrgName).HasColumnName("OrgName");
            this.Property(t => t.AltName).HasColumnName("AltName");
            this.Property(t => t.InCareOf).HasColumnName("InCareOf");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZipPlus4).HasColumnName("ZipPlus4");
            this.Property(t => t.GroupExemptNum).HasColumnName("GroupExemptNum");
            this.Property(t => t.SubsectCode).HasColumnName("SubsectCode");
            this.Property(t => t.AffiliationCode).HasColumnName("AffiliationCode");
            this.Property(t => t.ClassCode1).HasColumnName("ClassCode1");
            this.Property(t => t.ClassCode2).HasColumnName("ClassCode2");
            this.Property(t => t.ClassCode3).HasColumnName("ClassCode3");
            this.Property(t => t.ClassCode4).HasColumnName("ClassCode4");
            this.Property(t => t.RulingDate).HasColumnName("RulingDate");
            this.Property(t => t.DeductCode).HasColumnName("DeductCode");
            this.Property(t => t.FoundationCode).HasColumnName("FoundationCode");
            this.Property(t => t.ActivityCode1).HasColumnName("ActivityCode1");
            this.Property(t => t.ActivityCode2).HasColumnName("ActivityCode2");
            this.Property(t => t.ActivityCode3).HasColumnName("ActivityCode3");
            this.Property(t => t.OrgCode).HasColumnName("OrgCode");
            this.Property(t => t.DOJCode).HasColumnName("DOJCode");
            this.Property(t => t.AdvRuleExpire).HasColumnName("AdvRuleExpire");
            this.Property(t => t.TaxPeriod).HasColumnName("TaxPeriod");
            this.Property(t => t.AssetCode).HasColumnName("AssetCode");
            this.Property(t => t.IncomeCode).HasColumnName("IncomeCode");
            this.Property(t => t.FilingReqCode).HasColumnName("FilingReqCode");
            this.Property(t => t.FilingReqCode2).HasColumnName("FilingReqCode2");
            this.Property(t => t.AcctPeriod).HasColumnName("AcctPeriod");
            this.Property(t => t.AssetAmount).HasColumnName("AssetAmount");
            this.Property(t => t.IncomeAmount).HasColumnName("IncomeAmount");
            this.Property(t => t.NTEECode).HasColumnName("NTEECode");
            this.Property(t => t.SortName).HasColumnName("SortName");
            this.Property(t => t.BankABA).HasColumnName("BankABA");
            this.Property(t => t.BankAccountNo).HasColumnName("BankAccountNo");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.PublicSch).HasColumnName("PublicSch");
            this.Property(t => t.PublicLib).HasColumnName("PublicLib");
            this.Property(t => t.Religious).HasColumnName("Religious");
            this.Property(t => t.SOI).HasColumnName("SOI");
            this.Property(t => t.IRB).HasColumnName("IRB");
            this.Property(t => t.Government).HasColumnName("Government");
            this.Property(t => t.GuideStar).HasColumnName("GuideStar");
            this.Property(t => t.FSChurchSubstantiation).HasColumnName("FSChurchSubstantiation");
            this.Property(t => t.FSIntegratedAuxiliaryForm).HasColumnName("FSIntegratedAuxiliaryForm");
            this.Property(t => t.InfoFromCharity).HasColumnName("InfoFromCharity");
            this.Property(t => t.Hold).HasColumnName("Hold");
            this.Property(t => t.OrgInfo).HasColumnName("OrgInfo");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtUpdated).HasColumnName("DtUpdated");
            this.Property(t => t.AltAddress).HasColumnName("AltAddress");
            this.Property(t => t.AltCity).HasColumnName("AltCity");
            this.Property(t => t.AltState).HasColumnName("AltState");
            this.Property(t => t.AltZipPlus4).HasColumnName("AltZipPlus4");
            this.Property(t => t.UseAltName).HasColumnName("UseAltName");
            this.Property(t => t.UseAltAddress).HasColumnName("UseAltAddress");
            this.Property(t => t.ExpenditureResponsibilities).HasColumnName("ExpenditureResponsibilities");
            this.Property(t => t.CopyOfExistingCharity).HasColumnName("CopyOfExistingCharity");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.DtIRBHold).HasColumnName("DtIRBHold");
            this.Property(t => t.SignificantChanges).HasColumnName("SignificantChanges");
            this.Property(t => t.ContactPerson).HasColumnName("ContactPerson");
            this.Property(t => t.CheckPayableTo).HasColumnName("CheckPayableTo");
            this.Property(t => t.CheckMemo).HasColumnName("CheckMemo");
            this.Property(t => t.FiscalAgent).HasColumnName("FiscalAgent");
            this.Property(t => t.CatholicDirectory).HasColumnName("CatholicDirectory");
            this.Property(t => t.UseSpecPurpose).HasColumnName("UseSpecPurpose");
            this.Property(t => t.SpecPurposeType).HasColumnName("SpecPurposeType");
            this.Property(t => t.SpecPurposeText1).HasColumnName("SpecPurposeText1");
            this.Property(t => t.SpecPurposeText2).HasColumnName("SpecPurposeText2");
            this.Property(t => t.AltInCareOf).HasColumnName("AltInCareOf");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.AltAddress2).HasColumnName("AltAddress2");
            this.Property(t => t.AssociatedFoundations).HasColumnName("AssociatedFoundations");
            this.Property(t => t.AltCountry).HasColumnName("AltCountry");
        }
    }
}
