using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AssetPriceMap : EntityTypeConfiguration<AssetPrice>
    {
        public AssetPriceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("AssetPrices");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.DtPrice).HasColumnName("DtPrice");
            this.Property(t => t.LowPrice).HasColumnName("LowPrice");
            this.Property(t => t.HiPrice).HasColumnName("HiPrice");
            this.Property(t => t.ClsPrice).HasColumnName("ClsPrice");
            this.Property(t => t.PriceAuthor).HasColumnName("PriceAuthor");

            // Relationships
            this.HasRequired(t => t.Asset1)
                .WithMany(t => t.AssetPrices)
                .HasForeignKey(d => d.Asset);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.AssetPrices)
                .HasForeignKey(d => d.PriceAuthor);

        }
    }
}
