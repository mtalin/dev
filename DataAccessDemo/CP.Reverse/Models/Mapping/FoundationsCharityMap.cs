using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FoundationsCharityMap : EntityTypeConfiguration<FoundationsCharity>
    {
        public FoundationsCharityMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("FoundationsCharities");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.FoundationsCharities)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.FoundationsCharities)
                .HasForeignKey(d => d.Author);

        }
    }
}
