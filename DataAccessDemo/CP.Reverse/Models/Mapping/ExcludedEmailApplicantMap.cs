using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ExcludedEmailApplicantMap : EntityTypeConfiguration<ExcludedEmailApplicant>
    {
        public ExcludedEmailApplicantMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ExcludedEmailApplicants");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.EmailTemplate).HasColumnName("EmailTemplate");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.FinancialAdvisorGroup).HasColumnName("FinancialAdvisorGroup");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");

            // Relationships
            this.HasRequired(t => t.EmailTemplate1)
                .WithMany(t => t.ExcludedEmailApplicants)
                .HasForeignKey(d => d.EmailTemplate);
            this.HasOptional(t => t.FinancialAdvisorGroup1)
                .WithMany(t => t.ExcludedEmailApplicants)
                .HasForeignKey(d => d.FinancialAdvisorGroup);
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.ExcludedEmailApplicants)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
