using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DisbursementAccountMap : EntityTypeConfiguration<DisbursementAccount>
    {
        public DisbursementAccountMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.AccountName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.AccountNumber)
                .IsRequired()
                .HasMaxLength(22);

            this.Property(t => t.RoutingNumber)
                .HasMaxLength(22);

            this.Property(t => t.BankAddress)
                .HasMaxLength(200);

            this.Property(t => t.BatchFilerID)
                .HasMaxLength(50);

            this.Property(t => t.MasterInquiryPIN)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DisbursementAccounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Bank).HasColumnName("Bank");
            this.Property(t => t.AccountName).HasColumnName("AccountName");
            this.Property(t => t.AccountNumber).HasColumnName("AccountNumber");
            this.Property(t => t.RoutingNumber).HasColumnName("RoutingNumber");
            this.Property(t => t.BankAddress).HasColumnName("BankAddress");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.BatchFilerID).HasColumnName("BatchFilerID");
            this.Property(t => t.MasterInquiryPIN).HasColumnName("MasterInquiryPIN");
            this.Property(t => t.CheckingAccount).HasColumnName("CheckingAccount");

            // Relationships
            this.HasOptional(t => t.CheckingAccount1)
                .WithMany(t => t.DisbursementAccounts)
                .HasForeignKey(d => d.CheckingAccount);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.DisbursementAccounts)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.ServiceProvider)
                .WithMany(t => t.DisbursementAccounts)
                .HasForeignKey(d => d.Bank);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.DisbursementAccounts1)
                .HasForeignKey(d => d.DeletedBy);

        }
    }
}
