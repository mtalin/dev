using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vChangedTaxReturnsStatusMap : EntityTypeConfiguration<vChangedTaxReturnsStatus>
    {
        public vChangedTaxReturnsStatusMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.Step2BeSet });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step2BeSet)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vChangedTaxReturnsStatuses", "matrix");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Step2BeSet).HasColumnName("Step2BeSet");
        }
    }
}
