using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QModifyReportMap : EntityTypeConfiguration<QModifyReport>
    {
        public QModifyReportMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TransactionTypes)
                .HasMaxLength(250);

            this.Property(t => t.SecuritySymbol)
                .HasMaxLength(50);

            this.Property(t => t.ShowFields)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("QModifyReports");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.QReport).HasColumnName("QReport");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.DtFrom).HasColumnName("DtFrom");
            this.Property(t => t.DtTo).HasColumnName("DtTo");
            this.Property(t => t.TransactionTypes).HasColumnName("TransactionTypes");
            this.Property(t => t.SecuritySymbol).HasColumnName("SecuritySymbol");
            this.Property(t => t.ShowFields).HasColumnName("ShowFields");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.AuthorProcess).HasColumnName("AuthorProcess");
            this.Property(t => t.DtProcessed).HasColumnName("DtProcessed");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.AuthorDeleted).HasColumnName("AuthorDeleted");

            // Relationships
            this.HasRequired(t => t.FoundationAccount1)
                .WithMany(t => t.QModifyReports)
                .HasForeignKey(d => d.FoundationAccount);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.QModifyReports)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.QModifyReports1)
                .HasForeignKey(d => d.AuthorProcess);
            this.HasRequired(t => t.QReport1)
                .WithMany(t => t.QModifyReports)
                .HasForeignKey(d => d.QReport);

        }
    }
}
