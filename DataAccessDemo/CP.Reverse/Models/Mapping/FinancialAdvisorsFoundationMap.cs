using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FinancialAdvisorsFoundationMap : EntityTypeConfiguration<FinancialAdvisorsFoundation>
    {
        public FinancialAdvisorsFoundationMap()
        {
            // Primary Key
            this.HasKey(t => new { t.FinancialAdvisor, t.Foundation });

            // Properties
            this.Property(t => t.FinancialAdvisor)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("FinancialAdvisorsFoundations");
            this.Property(t => t.FinancialAdvisor).HasColumnName("FinancialAdvisor");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Role).HasColumnName("Role");

            // Relationships
            this.HasRequired(t => t.FinancialAdvisor1)
                .WithMany(t => t.FinancialAdvisorsFoundations)
                .HasForeignKey(d => d.FinancialAdvisor);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.FinancialAdvisorsFoundations)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
