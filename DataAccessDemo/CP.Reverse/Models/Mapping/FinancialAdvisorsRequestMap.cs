using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FinancialAdvisorsRequestMap : EntityTypeConfiguration<FinancialAdvisorsRequest>
    {
        public FinancialAdvisorsRequestMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .HasMaxLength(20);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("FinancialAdvisorsRequests");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FinancialAdvisor).HasColumnName("FinancialAdvisor");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Step).HasColumnName("Step");

            // Relationships
            this.HasOptional(t => t.FinancialAdvisor1)
                .WithMany(t => t.FinancialAdvisorsRequests)
                .HasForeignKey(d => d.FinancialAdvisor);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.FinancialAdvisorsRequests)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.FinancialAdvisorsRequests)
                .HasForeignKey(d => d.Person);

        }
    }
}
