using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_AdvisorRoleKeepersMap : EntityTypeConfiguration<et_AdvisorRoleKeepers>
    {
        public et_AdvisorRoleKeepersMap()
        {
            // Primary Key
            this.HasKey(t => new { t.AdvisorType, t.Role, t.Foundation, t.FinancialPartner, t.Name });

            // Properties
            this.Property(t => t.AdvisorType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Role)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FinancialPartner)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(101);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Email2)
                .HasMaxLength(50);

            this.Property(t => t.Email3)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("et_AdvisorRoleKeepers");
            this.Property(t => t.AdvisorType).HasColumnName("AdvisorType");
            this.Property(t => t.Role).HasColumnName("Role");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FinancialAdvisorGroup).HasColumnName("FinancialAdvisorGroup");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Email2).HasColumnName("Email2");
            this.Property(t => t.Email3).HasColumnName("Email3");
        }
    }
}
