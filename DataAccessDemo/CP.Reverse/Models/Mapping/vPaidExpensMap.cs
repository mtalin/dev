using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vPaidExpensMap : EntityTypeConfiguration<vPaidExpens>
    {
        public vPaidExpensMap()
        {
            // Primary Key
            this.HasKey(t => new { t.partid, t.LineID, t.foundation, t.CheckN, t.CheckDate, t.registryrule });

            // Properties
            this.Property(t => t.partname)
                .HasMaxLength(255);

            this.Property(t => t.LineID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Category)
                .HasMaxLength(255);

            this.Property(t => t.Desription)
                .HasMaxLength(1024);

            this.Property(t => t.PayableTo)
                .HasMaxLength(1024);

            this.Property(t => t.CheckN)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.CheckDate)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.InvoiceNumber)
                .HasMaxLength(50);

            this.Property(t => t.registryrule)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vPaidExpenses");
            this.Property(t => t.partid).HasColumnName("partid");
            this.Property(t => t.partname).HasColumnName("partname");
            this.Property(t => t.LineID).HasColumnName("LineID");
            this.Property(t => t.dttransact).HasColumnName("dttransact");
            this.Property(t => t.foundation).HasColumnName("foundation");
            this.Property(t => t.tid).HasColumnName("tid");
            this.Property(t => t.exid).HasColumnName("exid");
            this.Property(t => t.taxid).HasColumnName("taxid");
            this.Property(t => t.PayeeID).HasColumnName("PayeeID");
            this.Property(t => t.TrType).HasColumnName("TrType");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.Desription).HasColumnName("Desription");
            this.Property(t => t.PayableTo).HasColumnName("PayableTo");
            this.Property(t => t.CheckN).HasColumnName("CheckN");
            this.Property(t => t.CheckDate).HasColumnName("CheckDate");
            this.Property(t => t.InvoiceNumber).HasColumnName("InvoiceNumber");
            this.Property(t => t.InvoiceDocument).HasColumnName("InvoiceDocument");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.assetsAmount).HasColumnName("assetsAmount");
            this.Property(t => t.registryrule).HasColumnName("registryrule");
        }
    }
}
