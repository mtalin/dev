using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class old_vFinancialPartnersMap : EntityTypeConfiguration<old_vFinancialPartners>
    {
        public old_vFinancialPartnersMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.HideSuggestion, t.IsTestPartner, t.Name, t.SingleSignOn, t.SupportsOnline, t.ASA_Addendum, t.OngoingFeeToThirdParty, t.SetupFeeToThirdParty, t.CombineRiderBFees, t.FC_Allowed, t.FO_Allowed, t.FS_Allowed, t.FW_Allowed, t.TrCheckSupport, t.Ovr_ASA, t.Ovr_Rider, t.Ovr_DAccounts, t.Ovr_BalanceFile, t.Ovr_Fax, t.Ovr_GeneralFees, t.Ovr_OngoingFees, t.Ovr_Billing, t.Ovr_CustomContacts });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AccessURL)
                .HasMaxLength(150);

            this.Property(t => t.BrochureSheetTitle)
                .HasMaxLength(500);

            this.Property(t => t.Contact)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Fax)
                .HasMaxLength(20);

            this.Property(t => t.Institution)
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .HasMaxLength(20);

            this.Property(t => t.PhoneExt)
                .HasMaxLength(20);

            this.Property(t => t.SingleSignOn)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step6Note)
                .HasMaxLength(1500);

            this.Property(t => t.UID)
                .HasMaxLength(50);

            this.Property(t => t.URL)
                .HasMaxLength(150);

            this.Property(t => t.CWTR_Email)
                .HasMaxLength(50);

            this.Property(t => t.CWTR_FirstName)
                .HasMaxLength(50);

            this.Property(t => t.CWTR_LastName)
                .HasMaxLength(50);

            this.Property(t => t.CWTR_MiddleName)
                .HasMaxLength(15);

            this.Property(t => t.CWTR_Phone)
                .HasMaxLength(20);

            this.Property(t => t.FaxAttnCompany)
                .HasMaxLength(200);

            this.Property(t => t.FaxAttnFax)
                .HasMaxLength(20);

            this.Property(t => t.FaxAttnName)
                .HasMaxLength(50);

            this.Property(t => t.FaxAttnPhone)
                .HasMaxLength(20);

            this.Property(t => t.CSG_Fax)
                .HasMaxLength(20);

            this.Property(t => t.CSG_Name)
                .HasMaxLength(50);

            this.Property(t => t.CSG_Phone)
                .HasMaxLength(20);

            this.Property(t => t.CombineRiderBFees)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FC_Address1)
                .HasMaxLength(50);

            this.Property(t => t.FC_Address2)
                .HasMaxLength(50);

            this.Property(t => t.FC_Address3)
                .HasMaxLength(50);

            this.Property(t => t.FC_City)
                .HasMaxLength(50);

            this.Property(t => t.FC_State)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.FC_To)
                .HasMaxLength(100);

            this.Property(t => t.FC_ZIP)
                .IsFixedLength()
                .HasMaxLength(5);

            this.Property(t => t.FO_Instructions)
                .HasMaxLength(1000);

            this.Property(t => t.FO_Name)
                .HasMaxLength(50);

            this.Property(t => t.FS_DTC)
                .HasMaxLength(500);

            this.Property(t => t.FS_Instructions)
                .HasMaxLength(1500);

            this.Property(t => t.FW_ABA)
                .HasMaxLength(50);

            this.Property(t => t.FW_AccName)
                .HasMaxLength(50);

            this.Property(t => t.FW_AccNumber)
                .HasMaxLength(50);

            this.Property(t => t.FW_Bank)
                .HasMaxLength(50);

            this.Property(t => t.FW_BankAddress)
                .HasMaxLength(200);

            this.Property(t => t.FW_Instructions)
                .HasMaxLength(500);

            this.Property(t => t.TrCheckSupport)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("old_vFinancialPartners");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.AccessURL).HasColumnName("AccessURL");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.BrochureSheetTitle).HasColumnName("BrochureSheetTitle");
            this.Property(t => t.Contact).HasColumnName("Contact");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.FlatPartnerRate).HasColumnName("FlatPartnerRate");
            this.Property(t => t.HideSuggestion).HasColumnName("HideSuggestion");
            this.Property(t => t.Institution).HasColumnName("Institution");
            this.Property(t => t.IsTestPartner).HasColumnName("IsTestPartner");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.PhoneExt).HasColumnName("PhoneExt");
            this.Property(t => t.PT_SetupFee).HasColumnName("PT_SetupFee");
            this.Property(t => t.SingleSignOn).HasColumnName("SingleSignOn");
            this.Property(t => t.Step6Amount).HasColumnName("Step6Amount");
            this.Property(t => t.Step6Note).HasColumnName("Step6Note");
            this.Property(t => t.SupportsOnline).HasColumnName("SupportsOnline");
            this.Property(t => t.UID).HasColumnName("UID");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.CheckFee).HasColumnName("CheckFee");
            this.Property(t => t.MinGrantSize).HasColumnName("MinGrantSize");
            this.Property(t => t.ASA_Addendum).HasColumnName("ASA_Addendum");
            this.Property(t => t.BalanceFile).HasColumnName("BalanceFile");
            this.Property(t => t.CWTR_ContactType).HasColumnName("CWTR_ContactType");
            this.Property(t => t.CWTR_ContactVia).HasColumnName("CWTR_ContactVia");
            this.Property(t => t.CWTR_Email).HasColumnName("CWTR_Email");
            this.Property(t => t.CWTR_FirstName).HasColumnName("CWTR_FirstName");
            this.Property(t => t.CWTR_LastName).HasColumnName("CWTR_LastName");
            this.Property(t => t.CWTR_MiddleName).HasColumnName("CWTR_MiddleName");
            this.Property(t => t.CWTR_Phone).HasColumnName("CWTR_Phone");
            this.Property(t => t.FaxAttnCompany).HasColumnName("FaxAttnCompany");
            this.Property(t => t.FaxAttnFax).HasColumnName("FaxAttnFax");
            this.Property(t => t.FaxAttnName).HasColumnName("FaxAttnName");
            this.Property(t => t.FaxAttnPhone).HasColumnName("FaxAttnPhone");
            this.Property(t => t.CSG_Fax).HasColumnName("CSG_Fax");
            this.Property(t => t.CSG_Name).HasColumnName("CSG_Name");
            this.Property(t => t.CSG_Phone).HasColumnName("CSG_Phone");
            this.Property(t => t.OngoingFeeBillingAddress).HasColumnName("OngoingFeeBillingAddress");
            this.Property(t => t.OngoingFeeToThirdParty).HasColumnName("OngoingFeeToThirdParty");
            this.Property(t => t.SetupFeeBillingAddress).HasColumnName("SetupFeeBillingAddress");
            this.Property(t => t.SetupFeeToThirdParty).HasColumnName("SetupFeeToThirdParty");
            this.Property(t => t.SetupFeeToThirdPartyAmount).HasColumnName("SetupFeeToThirdPartyAmount");
            this.Property(t => t.CP_SetupFee).HasColumnName("CP_SetupFee");
            this.Property(t => t.MinYearFee).HasColumnName("MinYearFee");
            this.Property(t => t.IncorporationFee).HasColumnName("IncorporationFee");
            this.Property(t => t.TerminationFee).HasColumnName("TerminationFee");
            this.Property(t => t.IRSFee).HasColumnName("IRSFee");
            this.Property(t => t.CombineRiderBFees).HasColumnName("CombineRiderBFees");
            this.Property(t => t.FlatFSRate).HasColumnName("FlatFSRate");
            this.Property(t => t.FlatOver).HasColumnName("FlatOver");
            this.Property(t => t.FlatFeePerQuarter).HasColumnName("FlatFeePerQuarter");
            this.Property(t => t.DiscountFlatFeePerQuarter).HasColumnName("DiscountFlatFeePerQuarter");
            this.Property(t => t.DiscountFlatFeePerQuarterUpTo).HasColumnName("DiscountFlatFeePerQuarterUpTo");
            this.Property(t => t.FC_Address1).HasColumnName("FC_Address1");
            this.Property(t => t.FC_Address2).HasColumnName("FC_Address2");
            this.Property(t => t.FC_Address3).HasColumnName("FC_Address3");
            this.Property(t => t.FC_Allowed).HasColumnName("FC_Allowed");
            this.Property(t => t.FC_City).HasColumnName("FC_City");
            this.Property(t => t.FC_State).HasColumnName("FC_State");
            this.Property(t => t.FC_To).HasColumnName("FC_To");
            this.Property(t => t.FC_ZIP).HasColumnName("FC_ZIP");
            this.Property(t => t.FO_Allowed).HasColumnName("FO_Allowed");
            this.Property(t => t.FO_Instructions).HasColumnName("FO_Instructions");
            this.Property(t => t.FO_Name).HasColumnName("FO_Name");
            this.Property(t => t.FS_Allowed).HasColumnName("FS_Allowed");
            this.Property(t => t.FS_DTC).HasColumnName("FS_DTC");
            this.Property(t => t.FS_Instructions).HasColumnName("FS_Instructions");
            this.Property(t => t.FW_ABA).HasColumnName("FW_ABA");
            this.Property(t => t.FW_AccName).HasColumnName("FW_AccName");
            this.Property(t => t.FW_AccNumber).HasColumnName("FW_AccNumber");
            this.Property(t => t.FW_Allowed).HasColumnName("FW_Allowed");
            this.Property(t => t.FW_Bank).HasColumnName("FW_Bank");
            this.Property(t => t.FW_BankAddress).HasColumnName("FW_BankAddress");
            this.Property(t => t.FW_Instructions).HasColumnName("FW_Instructions");
            this.Property(t => t.TrCheckSupport).HasColumnName("TrCheckSupport");
            this.Property(t => t.TrProcMethod).HasColumnName("TrProcMethod");
            this.Property(t => t.CustomContactsHTML).HasColumnName("CustomContactsHTML");
            this.Property(t => t.Ovr_ASA).HasColumnName("Ovr_ASA");
            this.Property(t => t.Ovr_Rider).HasColumnName("Ovr_Rider");
            this.Property(t => t.Ovr_DAccounts).HasColumnName("Ovr_DAccounts");
            this.Property(t => t.Ovr_BalanceFile).HasColumnName("Ovr_BalanceFile");
            this.Property(t => t.Ovr_Fax).HasColumnName("Ovr_Fax");
            this.Property(t => t.Ovr_GeneralFees).HasColumnName("Ovr_GeneralFees");
            this.Property(t => t.Ovr_OngoingFees).HasColumnName("Ovr_OngoingFees");
            this.Property(t => t.Ovr_Billing).HasColumnName("Ovr_Billing");
            this.Property(t => t.Ovr_CustomContacts).HasColumnName("Ovr_CustomContacts");
        }
    }
}
