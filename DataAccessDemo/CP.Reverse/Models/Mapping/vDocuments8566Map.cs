using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vDocuments8566Map : EntityTypeConfiguration<vDocuments8566>
    {
        public vDocuments8566Map()
        {
            // Primary Key
            this.HasKey(t => t.Foundation);

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vDocuments8566");
            this.Property(t => t.DocId).HasColumnName("DocId");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.ShellCorporation).HasColumnName("ShellCorporation");
        }
    }
}
