using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class IncompleteAssetBookMap : EntityTypeConfiguration<IncompleteAssetBook>
    {
        public IncompleteAssetBookMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.UBTI, t.DtCreated, t.Author });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .HasMaxLength(250);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("IncompleteAssetBooks");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.UBTI).HasColumnName("UBTI");
            this.Property(t => t.UBTIDocument).HasColumnName("UBTIDocument");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtExpired).HasColumnName("DtExpired");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.UBTINote).HasColumnName("UBTINote");
        }
    }
}
