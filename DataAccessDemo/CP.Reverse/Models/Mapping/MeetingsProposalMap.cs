using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MeetingsProposalMap : EntityTypeConfiguration<MeetingsProposal>
    {
        public MeetingsProposalMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("MeetingsProposals");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Meeting).HasColumnName("Meeting");
            this.Property(t => t.Proposal).HasColumnName("Proposal");

            // Relationships
            this.HasRequired(t => t.Meeting1)
                .WithMany(t => t.MeetingsProposals)
                .HasForeignKey(d => d.Meeting);
            this.HasRequired(t => t.Proposal1)
                .WithMany(t => t.MeetingsProposals)
                .HasForeignKey(d => d.Proposal);

        }
    }
}
