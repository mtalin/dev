using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_PostalAddressesMap : EntityTypeConfiguration<et_PostalAddresses>
    {
        public et_PostalAddressesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Address1, t.City, t.Country, t.Text1 });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Address1)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Address2)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZIP)
                .HasMaxLength(9);

            this.Property(t => t.Country)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Company)
                .HasMaxLength(100);

            this.Property(t => t.StateName)
                .HasMaxLength(150);

            this.Property(t => t.Text1)
                .IsRequired()
                .HasMaxLength(101);

            this.Property(t => t.Text2)
                .HasMaxLength(200);

            this.Property(t => t.FullAddress)
                .HasMaxLength(303);

            // Table & Column Mappings
            this.ToTable("et_PostalAddresses");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZIP).HasColumnName("ZIP");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.StateName).HasColumnName("StateName");
            this.Property(t => t.Text1).HasColumnName("Text1");
            this.Property(t => t.Text2).HasColumnName("Text2");
            this.Property(t => t.FullAddress).HasColumnName("FullAddress");
        }
    }
}
