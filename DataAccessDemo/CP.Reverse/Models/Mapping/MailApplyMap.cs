using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MailApplyMap : EntityTypeConfiguration<MailApply>
    {
        public MailApplyMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleInitials)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Phone1)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Phone2)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.EMail)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Note)
                .HasMaxLength(330);

            this.Property(t => t.RejectNote)
                .HasMaxLength(550);

            // Table & Column Mappings
            this.ToTable("MailApplies");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ReferralSource).HasColumnName("ReferralSource");
            this.Property(t => t.ForAdvisor).HasColumnName("ForAdvisor");
            this.Property(t => t.ForClient).HasColumnName("ForClient");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.MiddleInitials).HasColumnName("MiddleInitials");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Phone1).HasColumnName("Phone1");
            this.Property(t => t.Phone2).HasColumnName("Phone2");
            this.Property(t => t.EMail).HasColumnName("EMail");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.RejectNote).HasColumnName("RejectNote");
            this.Property(t => t.RejectNoteID).HasColumnName("RejectNoteID");
            this.Property(t => t.Source).HasColumnName("Source");
            this.Property(t => t.DtAlert).HasColumnName("DtAlert");

            // Relationships
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.MailApplies)
                .HasForeignKey(d => d.Message);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.MailApplies)
                .HasForeignKey(d => d.RejectNoteID);

        }
    }
}
