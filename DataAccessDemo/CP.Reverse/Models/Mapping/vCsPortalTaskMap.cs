using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vCsPortalTaskMap : EntityTypeConfiguration<vCsPortalTask>
    {
        public vCsPortalTaskMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Subject, t.DtCreated, t.NoteExists, t.Priority });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.ResponsibleDepartmentName)
                .HasMaxLength(500);

            this.Property(t => t.AssignedPersonFirstName)
                .HasMaxLength(50);

            this.Property(t => t.AssignedPersonLastName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("vCsPortalTasks");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.EventClass).HasColumnName("EventClass");
            this.Property(t => t.TaskCategory).HasColumnName("TaskCategory");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.DueDays).HasColumnName("DueDays");
            this.Property(t => t.ResponsibleDepartment).HasColumnName("ResponsibleDepartment");
            this.Property(t => t.ResponsibleDepartmentName).HasColumnName("ResponsibleDepartmentName");
            this.Property(t => t.assignedPerson).HasColumnName("assignedPerson");
            this.Property(t => t.AssignedPersonFirstName).HasColumnName("AssignedPersonFirstName");
            this.Property(t => t.AssignedPersonLastName).HasColumnName("AssignedPersonLastName");
            this.Property(t => t.NoteExists).HasColumnName("NoteExists");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Recipient).HasColumnName("Recipient");
            this.Property(t => t.Priority).HasColumnName("Priority");
        }
    }
}
