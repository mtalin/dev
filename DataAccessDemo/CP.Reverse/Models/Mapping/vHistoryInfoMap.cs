using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vHistoryInfoMap : EntityTypeConfiguration<vHistoryInfo>
    {
        public vHistoryInfoMap()
        {
            // Primary Key
            this.HasKey(t => t.Foundation);

            // Properties
            // Table & Column Mappings
            this.ToTable("vHistoryInfo");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.TotalDonations).HasColumnName("TotalDonations");
            this.Property(t => t.TotalAccounted).HasColumnName("TotalAccounted");
        }
    }
}
