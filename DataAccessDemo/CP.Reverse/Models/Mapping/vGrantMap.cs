using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vGrantMap : EntityTypeConfiguration<vGrant>
    {
        public vGrantMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Type, t.DtCreated, t.Step, t.State, t.Amount, t.Anonymous, t.ConfirmGuideStar, t.IsSpecialDelivery, t.PermanentChanges, t.Priority, t.RecipientType, t.ReviewedByAdmin, t.GrantLetterApproved });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.State)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ChAddress)
                .HasMaxLength(100);

            this.Property(t => t.ChAddress2)
                .HasMaxLength(100);

            this.Property(t => t.ChCity)
                .HasMaxLength(30);

            this.Property(t => t.ChInCareOf)
                .HasMaxLength(100);

            this.Property(t => t.ChOrgName)
                .HasMaxLength(200);

            this.Property(t => t.ChState)
                .HasMaxLength(30);

            this.Property(t => t.ChZIP)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.ChCountry)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.ConfirmDesc)
                .HasMaxLength(50);

            this.Property(t => t.NoteForEmail)
                .HasMaxLength(1000);

            this.Property(t => t.Priority)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Purpose)
                .HasMaxLength(50);

            this.Property(t => t.PurposeDescription)
                .HasMaxLength(1000);

            this.Property(t => t.RecipientType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SpecPurposeText1)
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeText2)
                .HasMaxLength(1000);

            this.Property(t => t.TargetedDescription)
                .HasMaxLength(1000);

            this.Property(t => t.ProcessInstruction)
                .HasMaxLength(1000);

            this.Property(t => t.GrantReason)
                .HasMaxLength(4000);

            // Table & Column Mappings
            this.ToTable("vGrants");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Grant).HasColumnName("Grant");
            this.Property(t => t.CharityRequest).HasColumnName("CharityRequest");
            this.Property(t => t.GrantProposal).HasColumnName("GrantProposal");
            this.Property(t => t.Proposal).HasColumnName("Proposal");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.GrantDetails).HasColumnName("GrantDetails");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.PendingTransact).HasColumnName("PendingTransact");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Anonymous).HasColumnName("Anonymous");
            this.Property(t => t.Certificate).HasColumnName("Certificate");
            this.Property(t => t.ChAddress).HasColumnName("ChAddress");
            this.Property(t => t.ChAddress2).HasColumnName("ChAddress2");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.ChCity).HasColumnName("ChCity");
            this.Property(t => t.ChInCareOf).HasColumnName("ChInCareOf");
            this.Property(t => t.ChOrgName).HasColumnName("ChOrgName");
            this.Property(t => t.ChState).HasColumnName("ChState");
            this.Property(t => t.ChZIP).HasColumnName("ChZIP");
            this.Property(t => t.ChCountry).HasColumnName("ChCountry");
            this.Property(t => t.ConfirmDesc).HasColumnName("ConfirmDesc");
            this.Property(t => t.ConfirmGuideStar).HasColumnName("ConfirmGuideStar");
            this.Property(t => t.ConfirmIRSbulletins).HasColumnName("ConfirmIRSbulletins");
            this.Property(t => t.ConfirmIRSwebsite).HasColumnName("ConfirmIRSwebsite");
            this.Property(t => t.DonationRequest).HasColumnName("DonationRequest");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.GrantLetter).HasColumnName("GrantLetter");
            this.Property(t => t.GrantLetterData).HasColumnName("GrantLetterData");
            this.Property(t => t.HouseholdAccount).HasColumnName("HouseholdAccount");
            this.Property(t => t.IsSpecialDelivery).HasColumnName("IsSpecialDelivery");
            this.Property(t => t.NoteForEmail).HasColumnName("NoteForEmail");
            this.Property(t => t.PermanentChanges).HasColumnName("PermanentChanges");
            this.Property(t => t.Priority).HasColumnName("Priority");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.PurposeDescription).HasColumnName("PurposeDescription");
            this.Property(t => t.RecipientType).HasColumnName("RecipientType");
            this.Property(t => t.ReviewedByAdmin).HasColumnName("ReviewedByAdmin");
            this.Property(t => t.SpecialUserNote).HasColumnName("SpecialUserNote");
            this.Property(t => t.SpecPurposeText1).HasColumnName("SpecPurposeText1");
            this.Property(t => t.SpecPurposeText2).HasColumnName("SpecPurposeText2");
            this.Property(t => t.SpecPurposeType).HasColumnName("SpecPurposeType");
            this.Property(t => t.TargetedDescription).HasColumnName("TargetedDescription");
            this.Property(t => t.GrantType).HasColumnName("GrantType");
            this.Property(t => t.NumPayments).HasColumnName("NumPayments");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
            this.Property(t => t.PaymentNumber).HasColumnName("PaymentNumber");
            this.Property(t => t.ProcessInstruction).HasColumnName("ProcessInstruction");
            this.Property(t => t.Template).HasColumnName("Template");
            this.Property(t => t.GrantLetterApproved).HasColumnName("GrantLetterApproved");
            this.Property(t => t.GrantReason).HasColumnName("GrantReason");
        }
    }
}
