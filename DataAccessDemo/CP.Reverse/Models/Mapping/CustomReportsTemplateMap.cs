using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CustomReportsTemplateMap : EntityTypeConfiguration<CustomReportsTemplate>
    {
        public CustomReportsTemplateMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CustomReport, t.Format });

            // Properties
            this.Property(t => t.CustomReport)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Format)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CustomReportsTemplates");
            this.Property(t => t.CustomReport).HasColumnName("CustomReport");
            this.Property(t => t.DocumentTemplate).HasColumnName("DocumentTemplate");
            this.Property(t => t.Format).HasColumnName("Format");

            // Relationships
            this.HasRequired(t => t.CustomReport1)
                .WithMany(t => t.CustomReportsTemplates)
                .HasForeignKey(d => d.CustomReport);
            this.HasOptional(t => t.DocumentTemplate1)
                .WithMany(t => t.CustomReportsTemplates)
                .HasForeignKey(d => d.DocumentTemplate);

        }
    }
}
