using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class JobClassMap : EntityTypeConfiguration<JobClass>
    {
        public JobClassMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Routine)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.RetryStrategy)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("JobClasses");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.RoutineType).HasColumnName("RoutineType");
            this.Property(t => t.Routine).HasColumnName("Routine");
            this.Property(t => t.Timeout).HasColumnName("Timeout");
            this.Property(t => t.RetryStrategy).HasColumnName("RetryStrategy");
        }
    }
}
