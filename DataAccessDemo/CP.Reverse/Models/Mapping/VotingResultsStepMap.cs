using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class VotingResultsStepMap : EntityTypeConfiguration<VotingResultsStep>
    {
        public VotingResultsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Comment)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("VotingResultsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.VotingResult).HasColumnName("VotingResult");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Comment).HasColumnName("Comment");

            // Relationships
            this.HasRequired(t => t.VotingResult1)
                .WithMany(t => t.VotingResultsSteps)
                .HasForeignKey(d => d.VotingResult);

        }
    }
}
