using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FinancialStatementsPartMap : EntityTypeConfiguration<FinancialStatementsPart>
    {
        public FinancialStatementsPartMap()
        {
            // Primary Key
            this.HasKey(t => new { t.BookId, t.PartId });

            // Properties
            this.Property(t => t.BookId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PartId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("FinancialStatementsParts");
            this.Property(t => t.BookId).HasColumnName("BookId");
            this.Property(t => t.PartId).HasColumnName("PartId");
            this.Property(t => t.LineId).HasColumnName("LineId");
            this.Property(t => t.Name).HasColumnName("Name");
        }
    }
}
