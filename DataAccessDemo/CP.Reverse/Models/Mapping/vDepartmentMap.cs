using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vDepartmentMap : EntityTypeConfiguration<vDepartment>
    {
        public vDepartmentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.StructuredCompany, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Fax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Design)
                .HasMaxLength(100);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("vDepartments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.HigherDepartment).HasColumnName("HigherDepartment");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Design).HasColumnName("Design");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");
        }
    }
}
