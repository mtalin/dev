using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GrantApplicationMap : EntityTypeConfiguration<GrantApplication>
    {
        public GrantApplicationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ApplicationName)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.Template)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Applicant)
                .HasMaxLength(200);

            this.Property(t => t.XMLData)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("GrantApplications");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.ApplicationName).HasColumnName("ApplicationName");
            this.Property(t => t.Template).HasColumnName("Template");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtSubmitted).HasColumnName("DtSubmitted");
            this.Property(t => t.Applicant).HasColumnName("Applicant");
            this.Property(t => t.RequestedAmount).HasColumnName("RequestedAmount");
            this.Property(t => t.ReceivedAmount).HasColumnName("ReceivedAmount");
            this.Property(t => t.XMLData).HasColumnName("XMLData");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.GrantApplications)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
