using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vQuestAllHasIssueMap : EntityTypeConfiguration<vQuestAllHasIssue>
    {
        public vQuestAllHasIssueMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.dtResolved)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("vQuestAllHasIssues");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.HasIssues).HasColumnName("HasIssues");
            this.Property(t => t.Resolved).HasColumnName("Resolved");
            this.Property(t => t.Enabled).HasColumnName("Enabled");
            this.Property(t => t.ResolvedBy).HasColumnName("ResolvedBy");
            this.Property(t => t.dtResolved).HasColumnName("dtResolved");
            this.Property(t => t.fsAddrIssuesBy).HasColumnName("fsAddrIssuesBy");
        }
    }
}
