using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ViewPrintReceiptMap : EntityTypeConfiguration<ViewPrintReceipt>
    {
        public ViewPrintReceiptMap()
        {
            // Primary Key
            this.HasKey(t => new { t.FoundationID, t.DonationAllocated, t.AccountReconciled, t.ReceiptStatus, t.Step, t.Revised, t.IsPriced, t.DeliveredBy, t.CntPendingXIN });

            // Properties
            this.Property(t => t.DonorName)
                .HasMaxLength(150);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.FoundationName)
                .HasMaxLength(100);

            this.Property(t => t.FoundationID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DonorLastName)
                .HasMaxLength(50);

            this.Property(t => t.DonationAllocated)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AccountReconciled)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ReceiptStatus)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Revised)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsPriced)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DeliveredBy)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CntPendingXIN)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ViewPrintReceipts");
            this.Property(t => t.DonorName).HasColumnName("DonorName");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.RYear).HasColumnName("RYear");
            this.Property(t => t.DonorID).HasColumnName("DonorID");
            this.Property(t => t.FoundationID).HasColumnName("FoundationID");
            this.Property(t => t.DonorLastName).HasColumnName("DonorLastName");
            this.Property(t => t.DonationAllocated).HasColumnName("DonationAllocated");
            this.Property(t => t.AccountReconciled).HasColumnName("AccountReconciled");
            this.Property(t => t.ReceiptStatus).HasColumnName("ReceiptStatus");
            this.Property(t => t.TaxReceiptID).HasColumnName("TaxReceiptID");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.LastDoc).HasColumnName("LastDoc");
            this.Property(t => t.Revised).HasColumnName("Revised");
            this.Property(t => t.IsPriced).HasColumnName("IsPriced");
            this.Property(t => t.DeliveryPrefence).HasColumnName("DeliveryPrefence");
            this.Property(t => t.DeliveredBy).HasColumnName("DeliveredBy");
            this.Property(t => t.CntPendingXIN).HasColumnName("CntPendingXIN");
        }
    }
}
