using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class YearRangeDateMap : EntityTypeConfiguration<YearRangeDate>
    {
        public YearRangeDateMap()
        {
            // Primary Key
            this.HasKey(t => t.Year);

            // Properties
            this.Property(t => t.Year)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("YearRangeDates");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.Day1).HasColumnName("Day1");
            this.Property(t => t.Day2).HasColumnName("Day2");
        }
    }
}
