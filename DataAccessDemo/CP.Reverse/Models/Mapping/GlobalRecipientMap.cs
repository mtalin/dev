using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GlobalRecipientMap : EntityTypeConfiguration<GlobalRecipient>
    {
        public GlobalRecipientMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SQLScript)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.Flags)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("GlobalRecipients");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.SQLScript).HasColumnName("SQLScript");
            this.Property(t => t.Flags).HasColumnName("Flags");

            // Relationships
            this.HasRequired(t => t.Recipient)
                .WithOptional(t => t.GlobalRecipient);

        }
    }
}
