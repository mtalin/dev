using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class v_Department_FoundationMap : EntityTypeConfiguration<v_Department_Foundation>
    {
        public v_Department_FoundationMap()
        {
            // Primary Key
            this.HasKey(t => t.Foundation);

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("v_Department_Foundation");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.PCA).HasColumnName("PCA");
            this.Property(t => t.Department).HasColumnName("Department");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Customer).HasColumnName("Customer");
        }
    }
}
