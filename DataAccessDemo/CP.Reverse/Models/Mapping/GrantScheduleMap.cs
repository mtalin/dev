using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class GrantScheduleMap : EntityTypeConfiguration<GrantSchedule>
    {
        public GrantScheduleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("GrantSchedules");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.GrantDetailsTemplate).HasColumnName("GrantDetailsTemplate");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Foundation).HasColumnName("Foundation");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.GrantSchedules)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.GrantDetail)
                .WithMany(t => t.GrantSchedules)
                .HasForeignKey(d => d.GrantDetailsTemplate);
            this.HasRequired(t => t.TimeSchedule)
                .WithOptional(t => t.GrantSchedule);

        }
    }
}
