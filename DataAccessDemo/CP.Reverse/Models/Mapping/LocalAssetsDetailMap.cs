using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class LocalAssetsDetailMap : EntityTypeConfiguration<LocalAssetsDetail>
    {
        public LocalAssetsDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Name, t.Type, t.CategoryName, t.Foundation, t.FoundationName, t.InvPercent, t.ChrPercent });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Name2)
                .HasMaxLength(100);

            this.Property(t => t.Name3)
                .HasMaxLength(100);

            this.Property(t => t.Name4)
                .HasMaxLength(100);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Symbol)
                .HasMaxLength(32);

            this.Property(t => t.CUSIP)
                .IsFixedLength()
                .HasMaxLength(12);

            this.Property(t => t.TypeFlags)
                .HasMaxLength(150);

            this.Property(t => t.TypeName)
                .HasMaxLength(150);

            this.Property(t => t.CategoryName)
                .IsRequired()
                .HasMaxLength(17);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Description)
                .HasMaxLength(250);

            this.Property(t => t.LastAcqMethod)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("LocalAssetsDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Name2).HasColumnName("Name2");
            this.Property(t => t.Name3).HasColumnName("Name3");
            this.Property(t => t.Name4).HasColumnName("Name4");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Symbol).HasColumnName("Symbol");
            this.Property(t => t.CUSIP).HasColumnName("CUSIP");
            this.Property(t => t.TypeFlags).HasColumnName("TypeFlags");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.CategoryName).HasColumnName("CategoryName");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.AssetBook).HasColumnName("AssetBook");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.UBTI).HasColumnName("UBTI");
            this.Property(t => t.UBTIDocument).HasColumnName("UBTIDocument");
            this.Property(t => t.UBTINote).HasColumnName("UBTINote");
            this.Property(t => t.DtExpired).HasColumnName("DtExpired");
            this.Property(t => t.DtLastAcquisition).HasColumnName("DtLastAcquisition");
            this.Property(t => t.LastAcqMethod).HasColumnName("LastAcqMethod");
            this.Property(t => t.DtLastValuation).HasColumnName("DtLastValuation");
            this.Property(t => t.InvPercent).HasColumnName("InvPercent");
            this.Property(t => t.ChrPercent).HasColumnName("ChrPercent");
        }
    }
}
