using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TrRegistryMapMap : EntityTypeConfiguration<TrRegistryMap>
    {
        public TrRegistryMapMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TrRegistryMaps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.TransactType).HasColumnName("TransactType");
            this.Property(t => t.RegistryRule).HasColumnName("RegistryRule");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
