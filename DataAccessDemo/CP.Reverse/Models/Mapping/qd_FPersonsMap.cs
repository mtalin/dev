using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class qd_FPersonsMap : EntityTypeConfiguration<qd_FPersons>
    {
        public qd_FPersonsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.DtCreated, t.Questionnaire, t.type });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Questionnaire)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            this.Property(t => t.Address)
                .HasMaxLength(500);

            this.Property(t => t.Hours)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("qd_FPersons");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Questionnaire).HasColumnName("Questionnaire");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Excluded).HasColumnName("Excluded");
            this.Property(t => t.Hours).HasColumnName("Hours");
            this.Property(t => t.Comments).HasColumnName("Comments");
        }
    }
}
