using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class BATempGrantListMap : EntityTypeConfiguration<BATempGrantList>
    {
        public BATempGrantListMap()
        {
            // Primary Key
            this.HasKey(t => new { t.GrantID, t.DtCreated });

            // Properties
            this.Property(t => t.GrantID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("BATempGrantList");
            this.Property(t => t.GrantID).HasColumnName("GrantID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
        }
    }
}
