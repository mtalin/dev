using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AspSessionMap : EntityTypeConfiguration<AspSession>
    {
        public AspSessionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.SessionId)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(24);

            this.Property(t => t.AuthorInfo)
                .IsRequired()
                .HasMaxLength(800);

            // Table & Column Mappings
            this.ToTable("AspSessions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(t => t.AuthorInfo).HasColumnName("AuthorInfo");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtUpdated).HasColumnName("DtUpdated");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.SessionType).HasColumnName("SessionType");
        }
    }
}
