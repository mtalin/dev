using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vExpenseHistoryMap : EntityTypeConfiguration<vExpenseHistory>
    {
        public vExpenseHistoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Date, t.PayeeType, t.TransactID });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PayeeType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PayeeName)
                .HasMaxLength(200);

            this.Property(t => t.SortPayeeName)
                .HasMaxLength(201);

            this.Property(t => t.CategoryName)
                .HasMaxLength(150);

            this.Property(t => t.SortCategoryName)
                .HasMaxLength(150);

            this.Property(t => t.TypeName)
                .HasMaxLength(150);

            this.Property(t => t.SortTypeName)
                .HasMaxLength(150);

            this.Property(t => t.ProgramName)
                .HasMaxLength(200);

            this.Property(t => t.SortProgramName)
                .HasMaxLength(200);

            this.Property(t => t.ProjectName)
                .HasMaxLength(200);

            this.Property(t => t.SortProjectName)
                .HasMaxLength(200);

            this.Property(t => t.TransactID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("vExpenseHistory");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.PayeeType).HasColumnName("PayeeType");
            this.Property(t => t.Payee).HasColumnName("Payee");
            this.Property(t => t.PayeeName).HasColumnName("PayeeName");
            this.Property(t => t.SortPayeeName).HasColumnName("SortPayeeName");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.CategoryName).HasColumnName("CategoryName");
            this.Property(t => t.SortCategoryName).HasColumnName("SortCategoryName");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.SortTypeName).HasColumnName("SortTypeName");
            this.Property(t => t.Program).HasColumnName("Program");
            this.Property(t => t.ProgramName).HasColumnName("ProgramName");
            this.Property(t => t.SortProgramName).HasColumnName("SortProgramName");
            this.Property(t => t.Project).HasColumnName("Project");
            this.Property(t => t.ProjectName).HasColumnName("ProjectName");
            this.Property(t => t.SortProjectName).HasColumnName("SortProjectName");
            this.Property(t => t.TransactID).HasColumnName("TransactID");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
