using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CompanyPositionMap : EntityTypeConfiguration<CompanyPosition>
    {
        public CompanyPositionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CompanyPositions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StaffingPolicy).HasColumnName("StaffingPolicy");
            this.Property(t => t.Department).HasColumnName("Department");
            this.Property(t => t.HeadOfDepartment).HasColumnName("HeadOfDepartment");
            this.Property(t => t.SubordinateTo).HasColumnName("SubordinateTo");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.DtNominated).HasColumnName("DtNominated");
            this.Property(t => t.DtAccepted).HasColumnName("DtAccepted");
            this.Property(t => t.DtRemoved).HasColumnName("DtRemoved");
            this.Property(t => t.RemovedBy).HasColumnName("RemovedBy");
            this.Property(t => t.DtExpired).HasColumnName("DtExpired");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasRequired(t => t.CompanyElement)
                .WithOptional(t => t.CompanyPosition);
            this.HasOptional(t => t.CompanyPosition1)
                .WithMany(t => t.CompanyPositions1)
                .HasForeignKey(d => d.SubordinateTo);
            this.HasOptional(t => t.Department1)
                .WithMany(t => t.CompanyPositions)
                .HasForeignKey(d => d.Department);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.CompanyPositions)
                .HasForeignKey(d => d.Person);
            this.HasOptional(t => t.Person2)
                .WithMany(t => t.CompanyPositions1)
                .HasForeignKey(d => d.RemovedBy);
            this.HasRequired(t => t.StaffingPolicy1)
                .WithMany(t => t.CompanyPositions)
                .HasForeignKey(d => d.StaffingPolicy);
            this.HasRequired(t => t.StructuredCompany1)
                .WithMany(t => t.CompanyPositions)
                .HasForeignKey(d => d.StructuredCompany);

        }
    }
}
