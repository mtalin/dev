using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vQuestEachHasIssueMap : EntityTypeConfiguration<vQuestEachHasIssue>
    {
        public vQuestEachHasIssueMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.hasIssues });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.textQuestion)
                .HasMaxLength(50);

            this.Property(t => t.numQuestion)
                .HasMaxLength(10);

            this.Property(t => t.ResolveNote)
                .HasMaxLength(3000);

            // Table & Column Mappings
            this.ToTable("vQuestEachHasIssues");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.textQuestion).HasColumnName("textQuestion");
            this.Property(t => t.numQuestion).HasColumnName("numQuestion");
            this.Property(t => t.hasIssues).HasColumnName("hasIssues");
            this.Property(t => t.Resolved).HasColumnName("Resolved");
            this.Property(t => t.ResolveNote).HasColumnName("ResolveNote");
        }
    }
}
