using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ServiceUseCaseToStaticPermissionMap : EntityTypeConfiguration<ServiceUseCaseToStaticPermission>
    {
        public ServiceUseCaseToStaticPermissionMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ServiceUseCase, t.StaticPermission });

            // Properties
            this.Property(t => t.ServiceUseCase)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StaticPermission)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Value)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ServiceUseCaseToStaticPermissions");
            this.Property(t => t.ServiceUseCase).HasColumnName("ServiceUseCase");
            this.Property(t => t.StaticPermission).HasColumnName("StaticPermission");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Priority).HasColumnName("Priority");
        }
    }
}
