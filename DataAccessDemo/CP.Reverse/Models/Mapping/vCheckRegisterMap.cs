using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vCheckRegisterMap : EntityTypeConfiguration<vCheckRegister>
    {
        public vCheckRegisterMap()
        {
            // Primary Key
            this.HasKey(t => new { t.PaymentID, t.CheckID, t.PaymentType, t.IsModified });

            // Properties
            this.Property(t => t.PaymentID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CheckNo)
                .HasMaxLength(52);

            this.Property(t => t.TypeName)
                .HasMaxLength(150);

            this.Property(t => t.PayableTo)
                .HasMaxLength(1000);

            this.Property(t => t.FoundationName)
                .HasMaxLength(100);

            this.Property(t => t.AccountInfo)
                .HasMaxLength(577);

            this.Property(t => t.VoidNote)
                .HasMaxLength(1000);

            this.Property(t => t.CheckID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PaymentType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FullAccountName)
                .HasMaxLength(810);

            this.Property(t => t.FullLinkedAccountName)
                .HasMaxLength(577);

            // Table & Column Mappings
            this.ToTable("vCheckRegister");
            this.Property(t => t.PaymentID).HasColumnName("PaymentID");
            this.Property(t => t.CheckingAccount).HasColumnName("CheckingAccount");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.CheckDate).HasColumnName("CheckDate");
            this.Property(t => t.CheckNo).HasColumnName("CheckNo");
            this.Property(t => t.CheckAmount).HasColumnName("CheckAmount");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.PayableTo).HasColumnName("PayableTo");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.AccountInfo).HasColumnName("AccountInfo");
            this.Property(t => t.TaxID).HasColumnName("TaxID");
            this.Property(t => t.ExpenseID).HasColumnName("ExpenseID");
            this.Property(t => t.GrantID).HasColumnName("GrantID");
            this.Property(t => t.PendingTransactID).HasColumnName("PendingTransactID");
            this.Property(t => t.VoidNote).HasColumnName("VoidNote");
            this.Property(t => t.DtVoid).HasColumnName("DtVoid");
            this.Property(t => t.DtStepCanceled).HasColumnName("DtStepCanceled");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.DtProcessed).HasColumnName("DtProcessed");
            this.Property(t => t.SortCheckDate).HasColumnName("SortCheckDate");
            this.Property(t => t.CheckID).HasColumnName("CheckID");
            this.Property(t => t.PaymentType).HasColumnName("PaymentType");
            this.Property(t => t.FullAccountName).HasColumnName("FullAccountName");
            this.Property(t => t.FullLinkedAccountName).HasColumnName("FullLinkedAccountName");
            this.Property(t => t.GrantAmount).HasColumnName("GrantAmount");
            this.Property(t => t.TaxAmount).HasColumnName("TaxAmount");
            this.Property(t => t.ExpenseAmount).HasColumnName("ExpenseAmount");
            this.Property(t => t.PendingTransactAmount).HasColumnName("PendingTransactAmount");
            this.Property(t => t.IsVoid).HasColumnName("IsVoid");
            this.Property(t => t.IsGrouped).HasColumnName("IsGrouped");
            this.Property(t => t.ManualPayment).HasColumnName("ManualPayment");
            this.Property(t => t.VoidType).HasColumnName("VoidType");
            this.Property(t => t.IsModified).HasColumnName("IsModified");
        }
    }
}
