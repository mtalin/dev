using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AssetMap : EntityTypeConfiguration<Asset>
    {
        public AssetMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Name2)
                .HasMaxLength(100);

            this.Property(t => t.Name3)
                .HasMaxLength(100);

            this.Property(t => t.Name4)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Assets");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Name2).HasColumnName("Name2");
            this.Property(t => t.Name3).HasColumnName("Name3");
            this.Property(t => t.Name4).HasColumnName("Name4");
            this.Property(t => t.Type).HasColumnName("Type");
        }
    }
}
