using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MailAppliesSelectedDocumentMap : EntityTypeConfiguration<MailAppliesSelectedDocument>
    {
        public MailAppliesSelectedDocumentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.MailApply, t.MailAppliesDocument, t.DtCreated });

            // Properties
            this.Property(t => t.MailApply)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MailAppliesDocument)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MailAppliesSelectedDocuments");
            this.Property(t => t.MailApply).HasColumnName("MailApply");
            this.Property(t => t.MailAppliesDocument).HasColumnName("MailAppliesDocument");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtSent).HasColumnName("DtSent");

            // Relationships
            this.HasRequired(t => t.MailApply1)
                .WithMany(t => t.MailAppliesSelectedDocuments)
                .HasForeignKey(d => d.MailApply);
            this.HasRequired(t => t.MailAppliesDocument1)
                .WithMany(t => t.MailAppliesSelectedDocuments)
                .HasForeignKey(d => d.MailAppliesDocument);
            this.HasOptional(t => t.User)
                .WithMany(t => t.MailAppliesSelectedDocuments)
                .HasForeignKey(d => d.Author);

        }
    }
}
