using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QHistoryTransactionMap : EntityTypeConfiguration<QHistoryTransaction>
    {
        public QHistoryTransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Account)
                .HasMaxLength(50);

            this.Property(t => t.FPTransactID)
                .HasMaxLength(120);

            this.Property(t => t.Type)
                .HasMaxLength(50);

            this.Property(t => t.SecurityName)
                .HasMaxLength(100);

            this.Property(t => t.SecuritySymbol)
                .HasMaxLength(32);

            this.Property(t => t.CUSIP)
                .IsFixedLength()
                .HasMaxLength(12);

            this.Property(t => t.MergeTicker)
                .HasMaxLength(32);

            this.Property(t => t.Memo)
                .HasMaxLength(1024);

            this.Property(t => t.Chain)
                .HasMaxLength(120);

            this.Property(t => t.ErrorInfo)
                .HasMaxLength(2000);

            this.Property(t => t.Account2)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("QHistoryTransactions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.QTransaction).HasColumnName("QTransaction");
            this.Property(t => t.QModifyReport).HasColumnName("QModifyReport");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.Account).HasColumnName("Account");
            this.Property(t => t.FPTransactID).HasColumnName("FPTransactID");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.TypeCode).HasColumnName("TypeCode");
            this.Property(t => t.Sign).HasColumnName("Sign");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.PrincipalCost).HasColumnName("PrincipalCost");
            this.Property(t => t.Price).HasColumnName("Price");
            this.Property(t => t.Shares).HasColumnName("Shares");
            this.Property(t => t.SecurityName).HasColumnName("SecurityName");
            this.Property(t => t.SecuritySymbol).HasColumnName("SecuritySymbol");
            this.Property(t => t.CUSIP).HasColumnName("CUSIP");
            this.Property(t => t.SecurityType).HasColumnName("SecurityType");
            this.Property(t => t.Commission).HasColumnName("Commission");
            this.Property(t => t.Interest).HasColumnName("Interest");
            this.Property(t => t.Tax).HasColumnName("Tax");
            this.Property(t => t.SecFee).HasColumnName("SecFee");
            this.Property(t => t.MiscFee).HasColumnName("MiscFee");
            this.Property(t => t.OthFee).HasColumnName("OthFee");
            this.Property(t => t.TefraWithholding).HasColumnName("TefraWithholding");
            this.Property(t => t.ClearingCharge).HasColumnName("ClearingCharge");
            this.Property(t => t.BrokerageCharge).HasColumnName("BrokerageCharge");
            this.Property(t => t.InvestValue).HasColumnName("InvestValue");
            this.Property(t => t.CostBasis).HasColumnName("CostBasis");
            this.Property(t => t.Taxable).HasColumnName("Taxable");
            this.Property(t => t.SplitNumerator).HasColumnName("SplitNumerator");
            this.Property(t => t.SplitDenominator).HasColumnName("SplitDenominator");
            this.Property(t => t.MergeTicker).HasColumnName("MergeTicker");
            this.Property(t => t.Ratio).HasColumnName("Ratio");
            this.Property(t => t.BookBasis).HasColumnName("BookBasis");
            this.Property(t => t.QReport).HasColumnName("QReport");
            this.Property(t => t.ReportLine).HasColumnName("ReportLine");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.Chain).HasColumnName("Chain");
            this.Property(t => t.Correction).HasColumnName("Correction");
            this.Property(t => t.ErrorInfo).HasColumnName("ErrorInfo");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeleteAuthor).HasColumnName("DeleteAuthor");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.Asset2).HasColumnName("Asset2");
            this.Property(t => t.Modified).HasColumnName("Modified");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");
            this.Property(t => t.dtDeposit).HasColumnName("dtDeposit");
            this.Property(t => t.FoundationAccount2).HasColumnName("FoundationAccount2");
            this.Property(t => t.Account2).HasColumnName("Account2");

            // Relationships
            this.HasRequired(t => t.QTransaction1)
                .WithMany(t => t.QHistoryTransactions)
                .HasForeignKey(d => d.QTransaction);

        }
    }
}
