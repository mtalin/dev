using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ShellNumberMap : EntityTypeConfiguration<ShellNumber>
    {
        public ShellNumberMap()
        {
            // Primary Key
            this.HasKey(t => t.Number);

            // Properties
            this.Property(t => t.Number)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ShellNumber");
            this.Property(t => t.Number).HasColumnName("Number");
        }
    }
}
