using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PayeeProfileMap : EntityTypeConfiguration<PayeeProfile>
    {
        public PayeeProfileMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("PayeeProfiles");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Trustee).HasColumnName("Trustee");
            this.Property(t => t.Requires1099).HasColumnName("Requires1099");
            this.Property(t => t.Officer).HasColumnName("Officer");
            this.Property(t => t.CompanyType).HasColumnName("CompanyType");
            this.Property(t => t.Disqualified).HasColumnName("Disqualified");
            this.Property(t => t.FormW9).HasColumnName("FormW9");
            this.Property(t => t.IsDisabled).HasColumnName("IsDisabled");

            // Relationships
            this.HasOptional(t => t.Company1)
                .WithMany(t => t.PayeeProfiles)
                .HasForeignKey(d => d.Company);
            this.HasOptional(t => t.Document)
                .WithMany(t => t.PayeeProfiles)
                .HasForeignKey(d => d.FormW9);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.PayeeProfiles)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.PayeeProfiles)
                .HasForeignKey(d => d.Officer);

        }
    }
}
