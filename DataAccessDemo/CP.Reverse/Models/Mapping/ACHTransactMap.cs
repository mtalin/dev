using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ACHTransactMap : EntityTypeConfiguration<ACHTransact>
    {
        public ACHTransactMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ACHTransacts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.dtCreated).HasColumnName("dtCreated");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Amount).HasColumnName("Amount");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ACHTransacts)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.ACHTransacts)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
