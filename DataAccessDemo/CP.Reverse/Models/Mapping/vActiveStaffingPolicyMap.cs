using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vActiveStaffingPolicyMap : EntityTypeConfiguration<vActiveStaffingPolicy>
    {
        public vActiveStaffingPolicyMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.StructuredCompany, t.SystemPosition, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SystemPosition)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("vActiveStaffingPolicies");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.SystemPosition).HasColumnName("SystemPosition");
            this.Property(t => t.MinInstances).HasColumnName("MinInstances");
            this.Property(t => t.MaxInstances).HasColumnName("MaxInstances");
            this.Property(t => t.PeriodType).HasColumnName("PeriodType");
            this.Property(t => t.ExpirationPeriod).HasColumnName("ExpirationPeriod");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");
        }
    }
}
