using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PaymentsStepMap : EntityTypeConfiguration<PaymentsStep>
    {
        public PaymentsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("PaymentsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtOverriden).HasColumnName("DtOverriden");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Payment).HasColumnName("Payment");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Event).HasColumnName("Event");

            // Relationships
            this.HasOptional(t => t.Event1)
                .WithMany(t => t.PaymentsSteps)
                .HasForeignKey(d => d.Event);
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.PaymentsSteps)
                .HasForeignKey(d => d.Message);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.PaymentsSteps)
                .HasForeignKey(d => d.Note);
            this.HasRequired(t => t.Payment1)
                .WithMany(t => t.PaymentsSteps)
                .HasForeignKey(d => d.Payment);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.PaymentsSteps)
                .HasForeignKey(d => d.Author);

        }
    }
}
