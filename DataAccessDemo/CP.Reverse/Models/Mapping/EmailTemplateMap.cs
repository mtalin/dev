using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EmailTemplateMap : EntityTypeConfiguration<EmailTemplate>
    {
        public EmailTemplateMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Subject)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("EmailTemplates");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Parent).HasColumnName("Parent");
            this.Property(t => t.GenericEmail).HasColumnName("GenericEmail");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.FinancialAdvisorGroup).HasColumnName("FinancialAdvisorGroup");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.isPlaceHolder).HasColumnName("isPlaceHolder");
            this.Property(t => t.JobSubType).HasColumnName("JobSubType");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");
            this.Property(t => t.TaskCategory).HasColumnName("TaskCategory");
            this.Property(t => t.DueDays).HasColumnName("DueDays");
            this.Property(t => t.ResponsibleDepartment).HasColumnName("ResponsibleDepartment");
            this.Property(t => t.PriorityLow).HasColumnName("PriorityLow");
            this.Property(t => t.PriorityMedium).HasColumnName("PriorityMedium");
            this.Property(t => t.PriorityHigh).HasColumnName("PriorityHigh");
            this.Property(t => t.PriorityCritical).HasColumnName("PriorityCritical");
            this.Property(t => t.AlwaysEmail).HasColumnName("AlwaysEmail");

            // Relationships
            this.HasOptional(t => t.Department)
                .WithMany(t => t.EmailTemplates)
                .HasForeignKey(d => d.ResponsibleDepartment);
            this.HasOptional(t => t.EmailTemplate1)
                .WithMany(t => t.EmailTemplates1)
                .HasForeignKey(d => d.Parent);
            this.HasOptional(t => t.FinancialAdvisorGroup1)
                .WithMany(t => t.EmailTemplates)
                .HasForeignKey(d => d.FinancialAdvisorGroup);
            this.HasOptional(t => t.Foundation1)
                .WithMany(t => t.EmailTemplates)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.GenericEmail1)
                .WithMany(t => t.EmailTemplates)
                .HasForeignKey(d => d.GenericEmail);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.EmailTemplates)
                .HasForeignKey(d => d.Person);
            this.HasRequired(t => t.User)
                .WithMany(t => t.EmailTemplates)
                .HasForeignKey(d => d.Author);

        }
    }
}
