using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PaymentsBatchMap : EntityTypeConfiguration<PaymentsBatch>
    {
        public PaymentsBatchMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("PaymentsBatches");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.BatchNumber).HasColumnName("BatchNumber");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.DtSent).HasColumnName("DtSent");
            this.Property(t => t.ClosedBy).HasColumnName("ClosedBy");
        }
    }
}
