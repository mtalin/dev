using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CostBasMap : EntityTypeConfiguration<CostBas>
    {
        public CostBasMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CostBases");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.LotPrice).HasColumnName("LotPrice");
            this.Property(t => t.UnitsNumber).HasColumnName("UnitsNumber");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.HasAllocated).HasColumnName("HasAllocated");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.dtDeposit).HasColumnName("dtDeposit");

            // Relationships
            this.HasRequired(t => t.Transact1)
                .WithMany(t => t.CostBases)
                .HasForeignKey(d => d.Transact);

        }
    }
}
