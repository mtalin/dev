using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class YahooQueryMap : EntityTypeConfiguration<YahooQuery>
    {
        public YahooQueryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("YahooQueries");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.DtMin).HasColumnName("DtMin");
            this.Property(t => t.DtMax).HasColumnName("DtMax");
            this.Property(t => t.DtLastQuery).HasColumnName("DtLastQuery");
            this.Property(t => t.DtLastSuccess).HasColumnName("DtLastSuccess");
        }
    }
}
