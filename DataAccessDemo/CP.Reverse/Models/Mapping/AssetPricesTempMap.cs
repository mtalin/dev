using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class AssetPricesTempMap : EntityTypeConfiguration<AssetPricesTemp>
    {
        public AssetPricesTempMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Asset, t.DtPrice, t.LowPrice, t.HiPrice, t.ClsPrice, t.PriceAuthor });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Asset)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LowPrice)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.HiPrice)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ClsPrice)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PriceAuthor)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("AssetPricesTemp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.DtPrice).HasColumnName("DtPrice");
            this.Property(t => t.LowPrice).HasColumnName("LowPrice");
            this.Property(t => t.HiPrice).HasColumnName("HiPrice");
            this.Property(t => t.ClsPrice).HasColumnName("ClsPrice");
            this.Property(t => t.PriceAuthor).HasColumnName("PriceAuthor");
        }
    }
}
