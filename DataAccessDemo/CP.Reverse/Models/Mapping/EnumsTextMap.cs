using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EnumsTextMap : EntityTypeConfiguration<EnumsText>
    {
        public EnumsTextMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("EnumsTexts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.LargeValue).HasColumnName("LargeValue");

            // Relationships
            this.HasRequired(t => t.Enum)
                .WithOptional(t => t.EnumsText);

        }
    }
}
