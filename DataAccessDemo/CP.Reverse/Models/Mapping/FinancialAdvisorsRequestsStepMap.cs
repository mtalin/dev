using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FinancialAdvisorsRequestsStepMap : EntityTypeConfiguration<FinancialAdvisorsRequestsStep>
    {
        public FinancialAdvisorsRequestsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("FinancialAdvisorsRequestsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FinancialAdvisorsRequest).HasColumnName("FinancialAdvisorsRequest");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasRequired(t => t.FinancialAdvisorsRequest1)
                .WithMany(t => t.FinancialAdvisorsRequestsSteps)
                .HasForeignKey(d => d.FinancialAdvisorsRequest);
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.FinancialAdvisorsRequestsSteps)
                .HasForeignKey(d => d.Message);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.FinancialAdvisorsRequestsSteps)
                .HasForeignKey(d => d.Note);

        }
    }
}
