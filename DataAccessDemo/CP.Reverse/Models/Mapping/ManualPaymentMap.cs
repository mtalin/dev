using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ManualPaymentMap : EntityTypeConfiguration<ManualPayment>
    {
        public ManualPaymentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ManualPayments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Payment).HasColumnName("Payment");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Payee).HasColumnName("Payee");

            // Relationships
            this.HasOptional(t => t.CheckPayee)
                .WithMany(t => t.ManualPayments)
                .HasForeignKey(d => d.Payee);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ManualPayments)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Payment1)
                .WithMany(t => t.ManualPayments)
                .HasForeignKey(d => d.Payment);

        }
    }
}
