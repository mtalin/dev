using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vContributedAmountsSUMMap : EntityTypeConfiguration<vContributedAmountsSUM>
    {
        public vContributedAmountsSUMMap()
        {
            // Primary Key
            this.HasKey(t => t.Foundation);

            // Properties
            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vContributedAmountsSUM");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.donor).HasColumnName("donor");
            this.Property(t => t.ActualAmount).HasColumnName("ActualAmount");
            this.Property(t => t.dtSC).HasColumnName("dtSC");
        }
    }
}
