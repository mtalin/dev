using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FAPPackageMap : EntityTypeConfiguration<FAPPackage>
    {
        public FAPPackageMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(256);

            // Table & Column Mappings
            this.ToTable("FAPPackages");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.PackageForm).HasColumnName("PackageForm");
            this.Property(t => t.Parent).HasColumnName("Parent");

            // Relationships
            this.HasOptional(t => t.Document1)
                .WithMany(t => t.FAPPackages)
                .HasForeignKey(d => d.Document);
            this.HasRequired(t => t.FAPPackageForm)
                .WithMany(t => t.FAPPackages)
                .HasForeignKey(d => d.PackageForm);
            this.HasOptional(t => t.FAPPackage1)
                .WithMany(t => t.FAPPackages1)
                .HasForeignKey(d => d.Parent);

        }
    }
}
