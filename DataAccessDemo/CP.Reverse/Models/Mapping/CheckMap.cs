using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CheckMap : EntityTypeConfiguration<Check>
    {
        public CheckMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.CheckNo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.VoidNote)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Checks");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Payment).HasColumnName("Payment");
            this.Property(t => t.CheckNo).HasColumnName("CheckNo");
            this.Property(t => t.CheckDate).HasColumnName("CheckDate");
            this.Property(t => t.DtProcessed).HasColumnName("DtProcessed");
            this.Property(t => t.ProcessedBy).HasColumnName("ProcessedBy");
            this.Property(t => t.DtVoid).HasColumnName("DtVoid");
            this.Property(t => t.VoidBy).HasColumnName("VoidBy");
            this.Property(t => t.VoidNote).HasColumnName("VoidNote");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.VoidType).HasColumnName("VoidType");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.Checks)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.Checks1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasRequired(t => t.Payment1)
                .WithMany(t => t.Checks)
                .HasForeignKey(d => d.Payment);
            this.HasOptional(t => t.Person2)
                .WithMany(t => t.Checks2)
                .HasForeignKey(d => d.ProcessedBy);
            this.HasOptional(t => t.Person3)
                .WithMany(t => t.Checks3)
                .HasForeignKey(d => d.VoidBy);

        }
    }
}
