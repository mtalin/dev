using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class LinkRepositoryMap : EntityTypeConfiguration<LinkRepository>
    {
        public LinkRepositoryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ParametersLinks)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("LinkRepository");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ParametersLinks).HasColumnName("ParametersLinks");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtExpired).HasColumnName("DtExpired");
            this.Property(t => t.DtVisited).HasColumnName("DtVisited");
            this.Property(t => t.Author).HasColumnName("Author");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.LinkRepositories)
                .HasForeignKey(d => d.Author);

        }
    }
}
