using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QBalanceMap : EntityTypeConfiguration<QBalance>
    {
        public QBalanceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Account)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ErrorInfo)
                .HasMaxLength(2000);

            this.Property(t => t.Memo)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("QBalances");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtBalance).HasColumnName("DtBalance");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.Account).HasColumnName("Account");
            this.Property(t => t.Cash).HasColumnName("Cash");
            this.Property(t => t.Securities).HasColumnName("Securities");
            this.Property(t => t.CashDiscrepancy).HasColumnName("CashDiscrepancy");
            this.Property(t => t.SecurityDiscrepancy).HasColumnName("SecurityDiscrepancy");
            this.Property(t => t.QReport).HasColumnName("QReport");
            this.Property(t => t.ReportLine).HasColumnName("ReportLine");
            this.Property(t => t.ErrorInfo).HasColumnName("ErrorInfo");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeleteAuthor).HasColumnName("DeleteAuthor");
            this.Property(t => t.Memo).HasColumnName("Memo");
            this.Property(t => t.old_FinancialPartner).HasColumnName("old_FinancialPartner");

            // Relationships
            this.HasRequired(t => t.QReport1)
                .WithMany(t => t.QBalances)
                .HasForeignKey(d => d.QReport);

        }
    }
}
