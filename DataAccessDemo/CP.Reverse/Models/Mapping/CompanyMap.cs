using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CompanyMap : EntityTypeConfiguration<Company>
    {
        public CompanyMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.ContactName)
                .HasMaxLength(150);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Fax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Ein)
                .HasMaxLength(9);

            // Table & Column Mappings
            this.ToTable("Companies");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.DtOpen).HasColumnName("DtOpen");
            this.Property(t => t.ContactName).HasColumnName("ContactName");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.OriginalCompany).HasColumnName("OriginalCompany");
            this.Property(t => t.Draft).HasColumnName("Draft");
            this.Property(t => t.Ein).HasColumnName("Ein");

            // Relationships
            this.HasOptional(t => t.PostalAddress1)
                .WithMany(t => t.Companies)
                .HasForeignKey(d => d.PostalAddress);
            this.HasOptional(t => t.Company1)
                .WithMany(t => t.Companies1)
                .HasForeignKey(d => d.OriginalCompany);

        }
    }
}
