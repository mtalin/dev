using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class FeeCalculationMap : EntityTypeConfiguration<FeeCalculation>
    {
        public FeeCalculationMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Note)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("FeeCalculations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FeeType).HasColumnName("FeeType");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.BillTo).HasColumnName("BillTo");
            this.Property(t => t.Snapshot).HasColumnName("Snapshot");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Note).HasColumnName("Note");

            // Relationships
            this.HasOptional(t => t.CompanyElement)
                .WithMany(t => t.FeeCalculations)
                .HasForeignKey(d => d.BillTo);
            this.HasOptional(t => t.Contract1)
                .WithMany(t => t.FeeCalculations)
                .HasForeignKey(d => d.Contract);
            this.HasRequired(t => t.User)
                .WithMany(t => t.FeeCalculations)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.FeeCalculations)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
