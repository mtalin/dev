using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class SatelliteDocumentMap : EntityTypeConfiguration<SatelliteDocument>
    {
        public SatelliteDocumentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("SatelliteDocuments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ParentDocument).HasColumnName("ParentDocument");
            this.Property(t => t.ChildDocument).HasColumnName("ChildDocument");

            // Relationships
            this.HasRequired(t => t.Document)
                .WithMany(t => t.SatelliteDocuments)
                .HasForeignKey(d => d.ChildDocument);
            this.HasRequired(t => t.Document1)
                .WithMany(t => t.SatelliteDocuments1)
                .HasForeignKey(d => d.ParentDocument);

        }
    }
}
