using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_ContactsMap : EntityTypeConfiguration<et_Contacts>
    {
        public et_ContactsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FirstName, t.LastName, t.Organization, t.Email, t.FullStateName });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DateCreated)
                .HasMaxLength(30);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Organization)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Address1)
                .HasMaxLength(50);

            this.Property(t => t.Address2)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZIP)
                .HasMaxLength(9);

            this.Property(t => t.Country)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.HomePhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.BusinessPhone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Comments)
                .HasMaxLength(600);

            this.Property(t => t.FullName)
                .HasMaxLength(200);

            this.Property(t => t.FullStateName)
                .IsRequired()
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("et_Contacts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Organization).HasColumnName("Organization");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZIP).HasColumnName("ZIP");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.HomePhone).HasColumnName("HomePhone");
            this.Property(t => t.BusinessPhone).HasColumnName("BusinessPhone");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.FullName).HasColumnName("FullName");
            this.Property(t => t.FullStateName).HasColumnName("FullStateName");
        }
    }
}
