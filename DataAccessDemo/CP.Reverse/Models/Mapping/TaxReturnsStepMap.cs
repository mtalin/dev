using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReturnsStepMap : EntityTypeConfiguration<TaxReturnsStep>
    {
        public TaxReturnsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TaxReturnsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.TaxReturn).HasColumnName("TaxReturn");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.State1).HasColumnName("State1");
            this.Property(t => t.State2).HasColumnName("State2");
            this.Property(t => t.State3).HasColumnName("State3");
            this.Property(t => t.State4).HasColumnName("State4");
            this.Property(t => t.ResponsiblePerson).HasColumnName("ResponsiblePerson");
            this.Property(t => t.ActualDate).HasColumnName("ActualDate");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.IsNoteClosed).HasColumnName("IsNoteClosed");
            this.Property(t => t.NoteType).HasColumnName("NoteType");

            // Relationships
            this.HasOptional(t => t.Event1)
                .WithMany(t => t.TaxReturnsSteps)
                .HasForeignKey(d => d.Event);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.TaxReturnsSteps)
                .HasForeignKey(d => d.Note);
            this.HasOptional(t => t.Person)
                .WithMany(t => t.TaxReturnsSteps)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.TaxReturnsSteps1)
                .HasForeignKey(d => d.ResponsiblePerson);
            this.HasOptional(t => t.TaxReturn1)
                .WithMany(t => t.TaxReturnsSteps)
                .HasForeignKey(d => d.TaxReturn);

        }
    }
}
