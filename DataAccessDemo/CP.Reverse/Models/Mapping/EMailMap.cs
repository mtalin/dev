using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class EMailMap : EntityTypeConfiguration<EMail>
    {
        public EMailMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SendTo)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.CopyTo)
                .HasMaxLength(250);

            this.Property(t => t.Body)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("EMails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.SendTo).HasColumnName("SendTo");
            this.Property(t => t.CopyTo).HasColumnName("CopyTo");
            this.Property(t => t.Body).HasColumnName("Body");

            // Relationships
            this.HasRequired(t => t.Message)
                .WithOptional(t => t.EMail);

        }
    }
}
