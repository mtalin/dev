using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ObjectsActionsLogMap : EntityTypeConfiguration<ObjectsActionsLog>
    {
        public ObjectsActionsLogMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ObjectName)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Action)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("ObjectsActionsLog", "scm");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ObjectName).HasColumnName("ObjectName");
            this.Property(t => t.ObjectID).HasColumnName("ObjectID");
            this.Property(t => t.Action).HasColumnName("Action");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
        }
    }
}
