using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ServiceProviderMap : EntityTypeConfiguration<ServiceProvider>
    {
        public ServiceProviderMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.URL)
                .HasMaxLength(500);

            this.Property(t => t.Design)
                .HasMaxLength(100);

            this.Property(t => t.Abbrev)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("ServiceProviders");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.SingleSignOn).HasColumnName("SingleSignOn");
            this.Property(t => t.Design).HasColumnName("Design");
            this.Property(t => t.CustomerServiceContacts).HasColumnName("CustomerServiceContacts");
            this.Property(t => t.CustomContactsHTML).HasColumnName("CustomContactsHTML");
            this.Property(t => t.Abbrev).HasColumnName("Abbrev");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasRequired(t => t.Person)
                .WithMany(t => t.ServiceProviders)
                .HasForeignKey(d => d.Author);
            this.HasRequired(t => t.StructuredCompany1)
                .WithMany(t => t.ServiceProviders)
                .HasForeignKey(d => d.StructuredCompany);

        }
    }
}
