using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vShellStatuMap : EntityTypeConfiguration<vShellStatu>
    {
        public vShellStatuMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Name, t.ThirdPartySetupFee });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.ThirdPartySetupFee)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vShellStatus");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.FinancialPartner).HasColumnName("FinancialPartner");
            this.Property(t => t.COIGenerated).HasColumnName("COIGenerated");
            this.Property(t => t.EnterDateCOI).HasColumnName("EnterDateCOI");
            this.Property(t => t.UploadDCF).HasColumnName("UploadDCF");
            this.Property(t => t.MBOfficialCOI).HasColumnName("MBOfficialCOI");
            this.Property(t => t.ObtainEIN).HasColumnName("ObtainEIN");
            this.Property(t => t.MBCP575).HasColumnName("MBCP575");
            this.Property(t => t.MailIRSForm8655).HasColumnName("MailIRSForm8655");
            this.Property(t => t.MB8655).HasColumnName("MB8655");
            this.Property(t => t.Assigned).HasColumnName("Assigned");
            this.Property(t => t.AssignAccount).HasColumnName("AssignAccount");
            this.Property(t => t.AMCOIGenerated).HasColumnName("AMCOIGenerated");
            this.Property(t => t.FilingAMCOI).HasColumnName("FilingAMCOI");
            this.Property(t => t.Resignation).HasColumnName("Resignation");
            this.Property(t => t.ReceiveRACOI).HasColumnName("ReceiveRACOI");
            this.Property(t => t.MBOfficialRestatedCOI).HasColumnName("MBOfficialRestatedCOI");
            this.Property(t => t.MBRess).HasColumnName("MBRess");
            this.Property(t => t.RequestIRSNameChange).HasColumnName("RequestIRSNameChange");
            this.Property(t => t.ReceivedIRSNameChange).HasColumnName("ReceivedIRSNameChange");
            this.Property(t => t.MBFIRSLetter).HasColumnName("MBFIRSLetter");
            this.Property(t => t.StartUpPacketMailing).HasColumnName("StartUpPacketMailing");
            this.Property(t => t.UploadSCAgreement).HasColumnName("UploadSCAgreement");
            this.Property(t => t.UploadSuccDirResolution).HasColumnName("UploadSuccDirResolution");
            this.Property(t => t.Upload3rdResolution).HasColumnName("Upload3rdResolution");
            this.Property(t => t.Upload2848).HasColumnName("Upload2848");
            this.Property(t => t.UploadStartUpPacket).HasColumnName("UploadStartUpPacket");
            this.Property(t => t.MBConfirmationSAsigned).HasColumnName("MBConfirmationSAsigned");
            this.Property(t => t.MBSuccessorSigned).HasColumnName("MBSuccessorSigned");
            this.Property(t => t.MBOrgRes3signed).HasColumnName("MBOrgRes3signed");
            this.Property(t => t.MBForm2848signed).HasColumnName("MBForm2848signed");
            this.Property(t => t.F1023Generated).HasColumnName("F1023Generated");
            this.Property(t => t.F1023PDF).HasColumnName("F1023PDF");
            this.Property(t => t.F1023).HasColumnName("F1023");
            this.Property(t => t.FORM8718).HasColumnName("FORM8718");
            this.Property(t => t.AttorneyReview1023).HasColumnName("AttorneyReview1023");
            this.Property(t => t.F1023Mailed).HasColumnName("F1023Mailed");
            this.Property(t => t.MailIRSPacket).HasColumnName("MailIRSPacket");
            this.Property(t => t.ReceiveDocLocNum).HasColumnName("ReceiveDocLocNum");
            this.Property(t => t.UploadIRSLetter).HasColumnName("UploadIRSLetter");
            this.Property(t => t.MBIRSAskLetter).HasColumnName("MBIRSAskLetter");
            this.Property(t => t.ReceiveIRSApproval).HasColumnName("ReceiveIRSApproval");
            this.Property(t => t.MBIRSTaxExemptLetter).HasColumnName("MBIRSTaxExemptLetter");
            this.Property(t => t.Dt4thResolutionPrepared).HasColumnName("Dt4thResolutionPrepared");
            this.Property(t => t.Dt4thResolutionReceived).HasColumnName("Dt4thResolutionReceived");
            this.Property(t => t.IRSUserFee).HasColumnName("IRSUserFee");
            this.Property(t => t.IRSUserFeeGenerate).HasColumnName("IRSUserFeeGenerate");
            this.Property(t => t.IRSUserFeeComplete).HasColumnName("IRSUserFeeComplete");
            this.Property(t => t.InitialFunding).HasColumnName("InitialFunding");
            this.Property(t => t.SetupFee).HasColumnName("SetupFee");
            this.Property(t => t.DtSetupFee).HasColumnName("DtSetupFee");
            this.Property(t => t.ThirdPartySetupFee).HasColumnName("ThirdPartySetupFee");
            this.Property(t => t.NominationProcess).HasColumnName("NominationProcess");
            this.Property(t => t.AcceptedNominees).HasColumnName("AcceptedNominees");
            this.Property(t => t.RejectedNominees).HasColumnName("RejectedNominees");
            this.Property(t => t.ProcessedNominees).HasColumnName("ProcessedNominees");
            this.Property(t => t.SetupFeeToThirdParty).HasColumnName("SetupFeeToThirdParty");
        }
    }
}
