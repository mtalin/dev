using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CTL_ErrorsMap : EntityTypeConfiguration<CTL_Errors>
    {
        public CTL_ErrorsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Transact });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Transact)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("CTL_Errors");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
