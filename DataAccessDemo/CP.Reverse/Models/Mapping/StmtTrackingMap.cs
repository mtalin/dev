using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class StmtTrackingMap : EntityTypeConfiguration<StmtTracking>
    {
        public StmtTrackingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("StmtTracking");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.Month).HasColumnName("Month");
            this.Property(t => t.CheckDate).HasColumnName("CheckDate");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");

            // Relationships
            this.HasRequired(t => t.FoundationAccount1)
                .WithMany(t => t.StmtTrackings)
                .HasForeignKey(d => d.FoundationAccount);

        }
    }
}
