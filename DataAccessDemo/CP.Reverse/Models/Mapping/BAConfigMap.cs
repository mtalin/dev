using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class BAConfigMap : EntityTypeConfiguration<BAConfig>
    {
        public BAConfigMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CurrentConnections, t.PasswordIsChanging, t.PasswordUseCount });

            // Properties
            this.Property(t => t.Password)
                .HasMaxLength(24);

            this.Property(t => t.PreviousPasswords)
                .HasMaxLength(500);

            this.Property(t => t.CurrentConnections)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PasswordIsChanging)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PasswordUseCount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("BAConfig");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.LastDateChangePassword).HasColumnName("LastDateChangePassword");
            this.Property(t => t.PreviousPasswords).HasColumnName("PreviousPasswords");
            this.Property(t => t.CurrentConnections).HasColumnName("CurrentConnections");
            this.Property(t => t.PasswordIsChanging).HasColumnName("PasswordIsChanging");
            this.Property(t => t.PasswordUseCount).HasColumnName("PasswordUseCount");
        }
    }
}
