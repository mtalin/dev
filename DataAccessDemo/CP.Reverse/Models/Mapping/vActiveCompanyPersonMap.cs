using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vActiveCompanyPersonMap : EntityTypeConfiguration<vActiveCompanyPerson>
    {
        public vActiveCompanyPersonMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.StructuredCompany, t.Person, t.Author, t.ApproveGrants, t.GrantCommitteeMember });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Person)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vActiveCompanyPersons");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.GrantingLimit).HasColumnName("GrantingLimit");
            this.Property(t => t.ApproveGrants).HasColumnName("ApproveGrants");
            this.Property(t => t.ApprovableLimit).HasColumnName("ApprovableLimit");
            this.Property(t => t.GrantCommitteeMember).HasColumnName("GrantCommitteeMember");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
        }
    }
}
