using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ProposalStaffMap : EntityTypeConfiguration<ProposalStaff>
    {
        public ProposalStaffMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Proposal, t.Person, t.Appointment, t.NomineeStep });

            // Properties
            this.Property(t => t.Proposal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Person)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Appointment)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NomineeStep)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ProposalStaffs");
            this.Property(t => t.Proposal).HasColumnName("Proposal");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Appointment).HasColumnName("Appointment");
            this.Property(t => t.NomineeStep).HasColumnName("NomineeStep");

            // Relationships
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.ProposalStaffs)
                .HasForeignKey(d => d.Person);
            this.HasRequired(t => t.Proposal1)
                .WithMany(t => t.ProposalStaffs)
                .HasForeignKey(d => d.Proposal);

        }
    }
}
