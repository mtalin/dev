using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PricingScheduleInstanceMap : EntityTypeConfiguration<PricingScheduleInstance>
    {
        public PricingScheduleInstanceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("PricingScheduleInstances");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PricingSchedule).HasColumnName("PricingSchedule");
            this.Property(t => t.ContractPricing).HasColumnName("ContractPricing");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.PricingSource).HasColumnName("PricingSource");
            this.Property(t => t.BillingEvent).HasColumnName("BillingEvent");
            this.Property(t => t.BillingRecipient).HasColumnName("BillingRecipient");
            this.Property(t => t.BillingDepartment).HasColumnName("BillingDepartment");
            this.Property(t => t.BillingContact).HasColumnName("BillingContact");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasOptional(t => t.CompanyElement)
                .WithMany(t => t.PricingScheduleInstances)
                .HasForeignKey(d => d.BillingRecipient);
            this.HasOptional(t => t.CompanyPosition)
                .WithMany(t => t.PricingScheduleInstances)
                .HasForeignKey(d => d.BillingContact);
            this.HasOptional(t => t.ContractPricing1)
                .WithMany(t => t.PricingScheduleInstances)
                .HasForeignKey(d => d.ContractPricing);
            this.HasOptional(t => t.Department)
                .WithMany(t => t.PricingScheduleInstances)
                .HasForeignKey(d => d.BillingDepartment);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.PricingScheduleInstances)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.PricingScheduleInstances1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasOptional(t => t.PricingSchedule1)
                .WithMany(t => t.PricingScheduleInstances)
                .HasForeignKey(d => d.PricingSchedule);

        }
    }
}
