using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CrossingMap : EntityTypeConfiguration<Crossing>
    {
        public CrossingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.AuthorInfo)
                .IsRequired()
                .HasMaxLength(1024);

            // Table & Column Mappings
            this.ToTable("Crossings");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.AuthorInfo).HasColumnName("AuthorInfo");
            this.Property(t => t.TrustedAction).HasColumnName("TrustedAction");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");

            // Relationships
            this.HasOptional(t => t.TrustedAction1)
                .WithMany(t => t.Crossings)
                .HasForeignKey(d => d.TrustedAction);

        }
    }
}
