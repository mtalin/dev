using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CharityRequestsStepMap : EntityTypeConfiguration<CharityRequestsStep>
    {
        public CharityRequestsStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CharityRequestsSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.CharityRequest).HasColumnName("CharityRequest");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Step2).HasColumnName("Step2");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Event).HasColumnName("Event");
            this.Property(t => t.Pending).HasColumnName("Pending");
            this.Property(t => t.PendingPrev).HasColumnName("PendingPrev");

            // Relationships
            this.HasRequired(t => t.CharityRequest1)
                .WithMany(t => t.CharityRequestsSteps)
                .HasForeignKey(d => d.CharityRequest);
            this.HasOptional(t => t.Event1)
                .WithMany(t => t.CharityRequestsSteps)
                .HasForeignKey(d => d.Event);
            this.HasOptional(t => t.Message1)
                .WithMany(t => t.CharityRequestsSteps)
                .HasForeignKey(d => d.Message);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.CharityRequestsSteps)
                .HasForeignKey(d => d.Note);

        }
    }
}
