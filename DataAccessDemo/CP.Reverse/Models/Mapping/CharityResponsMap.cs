using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class CharityResponsMap : EntityTypeConfiguration<CharityRespons>
    {
        public CharityResponsMap()
        {
            // Primary Key
            this.HasKey(t => t.CharityRequest);

            // Properties
            this.Property(t => t.CharityRequest)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.OrgName)
                .HasMaxLength(70);

            this.Property(t => t.AltName)
                .HasMaxLength(70);

            this.Property(t => t.InCareOf)
                .HasMaxLength(35);

            this.Property(t => t.Address)
                .HasMaxLength(35);

            this.Property(t => t.City)
                .HasMaxLength(22);

            this.Property(t => t.State)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.ZipPlus4)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.NTEECode)
                .IsFixedLength()
                .HasMaxLength(4);

            this.Property(t => t.SortName)
                .HasMaxLength(35);

            this.Property(t => t.Type)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(15);

            this.Property(t => t.URL)
                .HasMaxLength(50);

            this.Property(t => t.HoldReason)
                .HasMaxLength(300);

            this.Property(t => t.OrgInfo)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("CharityResponses");
            this.Property(t => t.CharityRequest).HasColumnName("CharityRequest");
            this.Property(t => t.PublicEIN).HasColumnName("PublicEIN");
            this.Property(t => t.OrgName).HasColumnName("OrgName");
            this.Property(t => t.AltName).HasColumnName("AltName");
            this.Property(t => t.InCareOf).HasColumnName("InCareOf");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZipPlus4).HasColumnName("ZipPlus4");
            this.Property(t => t.GroupExemptNum).HasColumnName("GroupExemptNum");
            this.Property(t => t.SubsectCode).HasColumnName("SubsectCode");
            this.Property(t => t.AffiliationCode).HasColumnName("AffiliationCode");
            this.Property(t => t.ClassCode1).HasColumnName("ClassCode1");
            this.Property(t => t.ClassCode2).HasColumnName("ClassCode2");
            this.Property(t => t.ClassCode3).HasColumnName("ClassCode3");
            this.Property(t => t.ClassCode4).HasColumnName("ClassCode4");
            this.Property(t => t.RulingDate).HasColumnName("RulingDate");
            this.Property(t => t.DeductCode).HasColumnName("DeductCode");
            this.Property(t => t.FoundationCode).HasColumnName("FoundationCode");
            this.Property(t => t.ActivityCode1).HasColumnName("ActivityCode1");
            this.Property(t => t.ActivityCode2).HasColumnName("ActivityCode2");
            this.Property(t => t.ActivityCode3).HasColumnName("ActivityCode3");
            this.Property(t => t.OrgCode).HasColumnName("OrgCode");
            this.Property(t => t.DOJCode).HasColumnName("DOJCode");
            this.Property(t => t.AdvRuleExpire).HasColumnName("AdvRuleExpire");
            this.Property(t => t.TaxPeriod).HasColumnName("TaxPeriod");
            this.Property(t => t.AssetCode).HasColumnName("AssetCode");
            this.Property(t => t.IncomeCode).HasColumnName("IncomeCode");
            this.Property(t => t.FilingReqCode).HasColumnName("FilingReqCode");
            this.Property(t => t.FilingReqCode2).HasColumnName("FilingReqCode2");
            this.Property(t => t.AcctPeriod).HasColumnName("AcctPeriod");
            this.Property(t => t.AssetAmount).HasColumnName("AssetAmount");
            this.Property(t => t.IncomeAmount).HasColumnName("IncomeAmount");
            this.Property(t => t.NTEECode).HasColumnName("NTEECode");
            this.Property(t => t.SortName).HasColumnName("SortName");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.PublicSch).HasColumnName("PublicSch");
            this.Property(t => t.PublicLib).HasColumnName("PublicLib");
            this.Property(t => t.Religious).HasColumnName("Religious");
            this.Property(t => t.SOI).HasColumnName("SOI");
            this.Property(t => t.IRB).HasColumnName("IRB");
            this.Property(t => t.Government).HasColumnName("Government");
            this.Property(t => t.GuideStar).HasColumnName("GuideStar");
            this.Property(t => t.Removed).HasColumnName("Removed");
            this.Property(t => t.Hold).HasColumnName("Hold");
            this.Property(t => t.HoldReason).HasColumnName("HoldReason");
            this.Property(t => t.OrgInfo).HasColumnName("OrgInfo");

            // Relationships
            this.HasRequired(t => t.CharityRequest1)
                .WithOptional(t => t.CharityRespons);

        }
    }
}
