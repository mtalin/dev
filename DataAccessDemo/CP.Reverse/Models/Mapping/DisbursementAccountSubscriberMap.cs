using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DisbursementAccountSubscriberMap : EntityTypeConfiguration<DisbursementAccountSubscriber>
    {
        public DisbursementAccountSubscriberMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Transactions)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DisbursementAccountSubscribers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.DisbursementAccount).HasColumnName("DisbursementAccount");
            this.Property(t => t.Transactions).HasColumnName("Transactions");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasOptional(t => t.Contract1)
                .WithMany(t => t.DisbursementAccountSubscribers)
                .HasForeignKey(d => d.Contract);
            this.HasRequired(t => t.DisbursementAccount1)
                .WithMany(t => t.DisbursementAccountSubscribers)
                .HasForeignKey(d => d.DisbursementAccount);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.DisbursementAccountSubscribers)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.DisbursementAccountSubscribers1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasOptional(t => t.ServiceProvider1)
                .WithMany(t => t.DisbursementAccountSubscribers)
                .HasForeignKey(d => d.ServiceProvider);

        }
    }
}
