using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class oldQuestionnaireMap : EntityTypeConfiguration<oldQuestionnaire>
    {
        public oldQuestionnaireMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Version, t.FiscalYear, t.FormData, t.Step, t.HasIssues, t.DtCreated, t.Author });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Version)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.FiscalYear)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FormData)
                .IsRequired();

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("oldQuestionnaires");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Version).HasColumnName("Version");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
            this.Property(t => t.FormData).HasColumnName("FormData");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.HasIssues).HasColumnName("HasIssues");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtSubmitted).HasColumnName("DtSubmitted");
            this.Property(t => t.DtCompleted).HasColumnName("DtCompleted");
            this.Property(t => t.Author).HasColumnName("Author");
        }
    }
}
