using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_FinancialPartnersMap : EntityTypeConfiguration<et_FinancialPartners>
    {
        public et_FinancialPartnersMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.isSSORequired });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Fax)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Institution)
                .HasMaxLength(500);

            this.Property(t => t.URL)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("et_FinancialPartners");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.isSSORequired).HasColumnName("isSSORequired");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.PhoneExt).HasColumnName("PhoneExt");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Institution).HasColumnName("Institution");
            this.Property(t => t.Contact).HasColumnName("Contact");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.AccessURL).HasColumnName("AccessURL");
        }
    }
}
