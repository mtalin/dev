using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vDisbursementAccountMap : EntityTypeConfiguration<vDisbursementAccount>
    {
        public vDisbursementAccountMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Author, t.DtCreated, t.Bank, t.AccountName, t.AccountNumber });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Bank)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AccountName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.AccountNumber)
                .IsRequired()
                .HasMaxLength(22);

            this.Property(t => t.RoutingNumber)
                .HasMaxLength(22);

            this.Property(t => t.BankAddress)
                .HasMaxLength(200);

            this.Property(t => t.BatchFilerID)
                .HasMaxLength(50);

            this.Property(t => t.MasterInquiryPIN)
                .HasMaxLength(50);

            this.Property(t => t.BankName)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("vDisbursementAccounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Bank).HasColumnName("Bank");
            this.Property(t => t.AccountName).HasColumnName("AccountName");
            this.Property(t => t.AccountNumber).HasColumnName("AccountNumber");
            this.Property(t => t.RoutingNumber).HasColumnName("RoutingNumber");
            this.Property(t => t.BankAddress).HasColumnName("BankAddress");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.BatchFilerID).HasColumnName("BatchFilerID");
            this.Property(t => t.MasterInquiryPIN).HasColumnName("MasterInquiryPIN");
            this.Property(t => t.CheckingAccount).HasColumnName("CheckingAccount");
            this.Property(t => t.BankName).HasColumnName("BankName");
        }
    }
}
