using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RightsGroupMap : EntityTypeConfiguration<RightsGroup>
    {
        public RightsGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Rights)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("RightsGroups");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Rights).HasColumnName("Rights");
        }
    }
}
