using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PostalAddressMap : EntityTypeConfiguration<PostalAddress>
    {
        public PostalAddressMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Address1)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Address2)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZIP)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.Company)
                .HasMaxLength(100);

            this.Property(t => t.Description)
                .HasMaxLength(200);

            this.Property(t => t.CareOf)
                .HasMaxLength(200);

            this.Property(t => t.Salutation)
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Country)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("PostalAddresses");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZIP).HasColumnName("ZIP");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.CareOf).HasColumnName("CareOf");
            this.Property(t => t.Salutation).HasColumnName("Salutation");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Country).HasColumnName("Country");
        }
    }
}
