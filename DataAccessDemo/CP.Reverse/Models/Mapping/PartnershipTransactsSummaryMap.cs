using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PartnershipTransactsSummaryMap : EntityTypeConfiguration<PartnershipTransactsSummary>
    {
        public PartnershipTransactsSummaryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Partnership, t.K1Line });

            // Properties
            this.Property(t => t.Partnership)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.K1Line)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PartnershipTransactsSummary");
            this.Property(t => t.Partnership).HasColumnName("Partnership");
            this.Property(t => t.PfLine).HasColumnName("PfLine");
            this.Property(t => t.K1Line).HasColumnName("K1Line");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.TType).HasColumnName("TType");
            this.Property(t => t.A).HasColumnName("A");
            this.Property(t => t.B).HasColumnName("B");
            this.Property(t => t.Ubi).HasColumnName("Ubi");
        }
    }
}
