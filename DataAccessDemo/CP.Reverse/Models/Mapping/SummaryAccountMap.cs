using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class SummaryAccountMap : EntityTypeConfiguration<SummaryAccount>
    {
        public SummaryAccountMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("SummaryAccounts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.BankBalance).HasColumnName("BankBalance");
            this.Property(t => t.Balance1).HasColumnName("Balance1");
            this.Property(t => t.Balance2).HasColumnName("Balance2");
            this.Property(t => t.Balance3).HasColumnName("Balance3");
            this.Property(t => t.CashBalace).HasColumnName("CashBalace");
            this.Property(t => t.SecurityBalance).HasColumnName("SecurityBalance");
        }
    }
}
