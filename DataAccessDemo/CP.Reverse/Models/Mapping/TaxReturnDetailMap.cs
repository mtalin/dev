using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxReturnDetailMap : EntityTypeConfiguration<TaxReturnDetail>
    {
        public TaxReturnDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.DtCreated, t.Author, t.Foundation, t.Year, t.Step, t.IssuesCache, t.XmlData, t.FoundationName, t.FiscalYearEnd, t.StepName, t.FileExt, t.Outsourced, t.State3, t.State4, t.Req990T, t.Req4720, t.File990PFExt1, t.File990PFExt2, t.File990TExt1, t.File990TExt2, t.File4720Ext1, t.File4720Ext2, t.Priority });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Year)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IssuesCache)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(50);

            this.Property(t => t.XmlData)
                .IsRequired();

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.FiscalYearEnd)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StepName)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.FileExt)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.State3)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.State4)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TrackingNumber)
                .HasMaxLength(50);

            this.Property(t => t.Priority)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TaxReturnDetails", "matrix");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.DtSentToSaber).HasColumnName("DtSentToSaber");
            this.Property(t => t.DtSentToIRS).HasColumnName("DtSentToIRS");
            this.Property(t => t.DtFinalized).HasColumnName("DtFinalized");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.IssuesCache).HasColumnName("IssuesCache");
            this.Property(t => t.XmlData).HasColumnName("XmlData");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.FiscalYearEnd).HasColumnName("FiscalYearEnd");
            this.Property(t => t.StepName).HasColumnName("StepName");
            this.Property(t => t.FileExt).HasColumnName("FileExt");
            this.Property(t => t.Outsourced).HasColumnName("Outsourced");
            this.Property(t => t.OutsourcingCompany).HasColumnName("OutsourcingCompany");
            this.Property(t => t.TaxReturnReport).HasColumnName("TaxReturnReport");
            this.Property(t => t.DtStatusChange).HasColumnName("DtStatusChange");
            this.Property(t => t.TaxPackageRecalc).HasColumnName("TaxPackageRecalc");
            this.Property(t => t.dtRecalc).HasColumnName("dtRecalc");
            this.Property(t => t.State3).HasColumnName("State3");
            this.Property(t => t.State4).HasColumnName("State4");
            this.Property(t => t.ResponsiblePerson).HasColumnName("ResponsiblePerson");
            this.Property(t => t.Req990T).HasColumnName("Req990T");
            this.Property(t => t.Req4720).HasColumnName("Req4720");
            this.Property(t => t.FilingNote).HasColumnName("FilingNote");
            this.Property(t => t.FilingMethod).HasColumnName("FilingMethod");
            this.Property(t => t.File990PFExt1).HasColumnName("File990PFExt1");
            this.Property(t => t.DtSent990PFExt1).HasColumnName("DtSent990PFExt1");
            this.Property(t => t.File990PFExt2).HasColumnName("File990PFExt2");
            this.Property(t => t.DtSent990PFExt2).HasColumnName("DtSent990PFExt2");
            this.Property(t => t.File990TExt1).HasColumnName("File990TExt1");
            this.Property(t => t.DtSent990TExt1).HasColumnName("DtSent990TExt1");
            this.Property(t => t.File990TExt2).HasColumnName("File990TExt2");
            this.Property(t => t.DtSent990TExt2).HasColumnName("DtSent990TExt2");
            this.Property(t => t.File4720Ext1).HasColumnName("File4720Ext1");
            this.Property(t => t.DtSent4720Ext1).HasColumnName("DtSent4720Ext1");
            this.Property(t => t.File4720Ext2).HasColumnName("File4720Ext2");
            this.Property(t => t.DtSent4720Ext2).HasColumnName("DtSent4720Ext2");
            this.Property(t => t.MailCarrier).HasColumnName("MailCarrier");
            this.Property(t => t.TrackingNumber).HasColumnName("TrackingNumber");
            this.Property(t => t.DtStatusChangeState3).HasColumnName("DtStatusChangeState3");
            this.Property(t => t.DtStatusChangeState4).HasColumnName("DtStatusChangeState4");
            this.Property(t => t.TaxDue).HasColumnName("TaxDue");
            this.Property(t => t.Priority).HasColumnName("Priority");
        }
    }
}
