using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vActiveContractPlayerMap : EntityTypeConfiguration<vActiveContractPlayer>
    {
        public vActiveContractPlayerMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Contract, t.CompanyElement, t.RoleInContract, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Contract)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CompanyElement)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RoleInContract)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vActiveContractPlayers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.CompanyElement).HasColumnName("CompanyElement");
            this.Property(t => t.RoleInContract).HasColumnName("RoleInContract");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");
        }
    }
}
