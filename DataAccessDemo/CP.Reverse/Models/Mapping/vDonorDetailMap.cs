using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vDonorDetailMap : EntityTypeConfiguration<vDonorDetail>
    {
        public vDonorDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.DonorType, t.FullAddress, t.DonorTypeName, t.DeliveryMethod });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DonorType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.MiddleName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.Address1)
                .HasMaxLength(50);

            this.Property(t => t.Address2)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.ZIP)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.Country)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.InCareOf)
                .HasMaxLength(200);

            this.Property(t => t.CompanyName)
                .HasMaxLength(150);

            this.Property(t => t.SpouseFirstName)
                .HasMaxLength(50);

            this.Property(t => t.SpouseMiddleName)
                .HasMaxLength(50);

            this.Property(t => t.SpouseLastName)
                .HasMaxLength(50);

            this.Property(t => t.SpouseEmail)
                .HasMaxLength(50);

            this.Property(t => t.SpouseAddress1)
                .HasMaxLength(50);

            this.Property(t => t.SpouseAddress2)
                .HasMaxLength(50);

            this.Property(t => t.SpouseCity)
                .HasMaxLength(25);

            this.Property(t => t.SpouseState)
                .HasMaxLength(30);

            this.Property(t => t.SpouseZip)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.SpouseCountry)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.LongName)
                .HasMaxLength(416);

            this.Property(t => t.FullName)
                .HasMaxLength(365);

            this.Property(t => t.FullAddress)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.FullName2)
                .HasMaxLength(365);

            this.Property(t => t.DonorTypeName)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.DeliveryMethod)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vDonorDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DonorType).HasColumnName("DonorType");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.LastSpouse).HasColumnName("LastSpouse");
            this.Property(t => t.NamePrefix).HasColumnName("NamePrefix");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.MiddleName).HasColumnName("MiddleName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZIP).HasColumnName("ZIP");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.DOD).HasColumnName("DOD");
            this.Property(t => t.InCareOf).HasColumnName("InCareOf");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.DateOfTrust).HasColumnName("DateOfTrust");
            this.Property(t => t.SpouseID).HasColumnName("SpouseID");
            this.Property(t => t.SpouseNamePrefix).HasColumnName("SpouseNamePrefix");
            this.Property(t => t.SpouseFirstName).HasColumnName("SpouseFirstName");
            this.Property(t => t.SpouseMiddleName).HasColumnName("SpouseMiddleName");
            this.Property(t => t.SpouseLastName).HasColumnName("SpouseLastName");
            this.Property(t => t.SpouseEmail).HasColumnName("SpouseEmail");
            this.Property(t => t.SpouseAddress1).HasColumnName("SpouseAddress1");
            this.Property(t => t.SpouseAddress2).HasColumnName("SpouseAddress2");
            this.Property(t => t.SpouseCity).HasColumnName("SpouseCity");
            this.Property(t => t.SpouseState).HasColumnName("SpouseState");
            this.Property(t => t.SpouseZip).HasColumnName("SpouseZip");
            this.Property(t => t.SpouseCountry).HasColumnName("SpouseCountry");
            this.Property(t => t.IsSpouseAlive).HasColumnName("IsSpouseAlive");
            this.Property(t => t.LongName).HasColumnName("LongName");
            this.Property(t => t.FullName).HasColumnName("FullName");
            this.Property(t => t.FullAddress).HasColumnName("FullAddress");
            this.Property(t => t.FullName2).HasColumnName("FullName2");
            this.Property(t => t.DonorTypeName).HasColumnName("DonorTypeName");
            this.Property(t => t.DeliveryMethod).HasColumnName("DeliveryMethod");
        }
    }
}
