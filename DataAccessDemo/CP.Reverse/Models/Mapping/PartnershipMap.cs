using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class PartnershipMap : EntityTypeConfiguration<Partnership>
    {
        public PartnershipMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Partnerships");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Document).HasColumnName("Document");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.QReport).HasColumnName("QReport");
            this.Property(t => t.DtTransaction).HasColumnName("DtTransaction");

            // Relationships
            this.HasRequired(t => t.Asset1)
                .WithMany(t => t.Partnerships)
                .HasForeignKey(d => d.Asset);
            this.HasOptional(t => t.Document1)
                .WithMany(t => t.Partnerships)
                .HasForeignKey(d => d.Document);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Partnerships)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.QReport1)
                .WithMany(t => t.Partnerships)
                .HasForeignKey(d => d.QReport);

        }
    }
}
