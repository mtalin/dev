using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_StaffingProposalsMap : EntityTypeConfiguration<et_StaffingProposals>
    {
        public et_StaffingProposalsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.FoundationName, t.IsApproved, t.IsRejected, t.IsSuspended, t.Purpose, t.Author });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.ProposalDate)
                .HasMaxLength(30);

            this.Property(t => t.ProposalTime)
                .HasMaxLength(30);

            this.Property(t => t.ProposalCloseDate)
                .HasMaxLength(30);

            this.Property(t => t.DeadlineDate)
                .HasMaxLength(30);

            this.Property(t => t.IsApproved)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsRejected)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsSuspended)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Purpose)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("et_StaffingProposals");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.ProposalDate).HasColumnName("ProposalDate");
            this.Property(t => t.ProposalTime).HasColumnName("ProposalTime");
            this.Property(t => t.ProposalCloseDate).HasColumnName("ProposalCloseDate");
            this.Property(t => t.DeadlineDate).HasColumnName("DeadlineDate");
            this.Property(t => t.IsApproved).HasColumnName("IsApproved");
            this.Property(t => t.IsRejected).HasColumnName("IsRejected");
            this.Property(t => t.IsSuspended).HasColumnName("IsSuspended");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.Author).HasColumnName("Author");
        }
    }
}
