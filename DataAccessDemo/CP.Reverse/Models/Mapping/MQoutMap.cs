using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class MQoutMap : EntityTypeConfiguration<MQout>
    {
        public MQoutMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Error)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("MQout", "gmx");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Created).HasColumnName("Created");
            this.Property(t => t.Error).HasColumnName("Error");

            // Relationships
            this.HasRequired(t => t.MQin)
                .WithOptional(t => t.MQout);

        }
    }
}
