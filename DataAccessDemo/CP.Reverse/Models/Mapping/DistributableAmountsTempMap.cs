using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class DistributableAmountsTempMap : EntityTypeConfiguration<DistributableAmountsTemp>
    {
        public DistributableAmountsTempMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FoundationStamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8);

            // Table & Column Mappings
            this.ToTable("DistributableAmountsTemp");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.AVC).HasColumnName("AVC");
            this.Property(t => t.AVS).HasColumnName("AVS");
            this.Property(t => t.AVA).HasColumnName("AVA");
            this.Property(t => t.MPC).HasColumnName("MPC");
            this.Property(t => t.MIR).HasColumnName("MIR");
            this.Property(t => t.UI0).HasColumnName("UI0");
            this.Property(t => t.ACF).HasColumnName("ACF");
            this.Property(t => t.QD).HasColumnName("QD");
            this.Property(t => t.RCV).HasColumnName("RCV");
            this.Property(t => t.UI).HasColumnName("UI");
            this.Property(t => t.CF).HasColumnName("CF");
            this.Property(t => t.Scenario).HasColumnName("Scenario");
            this.Property(t => t.Final).HasColumnName("Final");
            this.Property(t => t.FoundationStamp).HasColumnName("FoundationStamp");
            this.Property(t => t.RegistryStamp).HasColumnName("RegistryStamp");
            this.Property(t => t.RegistryStamp2).HasColumnName("RegistryStamp2");
        }
    }
}
