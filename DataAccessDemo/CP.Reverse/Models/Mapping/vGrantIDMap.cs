using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vGrantIDMap : EntityTypeConfiguration<vGrantID>
    {
        public vGrantIDMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ActivityType, t.DtCreated, t.Step });

            // Properties
            this.Property(t => t.ActivityType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vGrantIDs");
            this.Property(t => t.GrantDetails).HasColumnName("GrantDetails");
            this.Property(t => t.ActivityType).HasColumnName("ActivityType");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.PendingTransact).HasColumnName("PendingTransact");
            this.Property(t => t.Transact).HasColumnName("Transact");
        }
    }
}
