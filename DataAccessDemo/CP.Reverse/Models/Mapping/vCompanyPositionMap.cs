using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class vCompanyPositionMap : EntityTypeConfiguration<vCompanyPosition>
    {
        public vCompanyPositionMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.StaffingPolicy, t.HeadOfDepartment, t.StructuredCompany, t.Author, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StaffingPolicy)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StructuredCompany)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Author)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("vCompanyPositions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StaffingPolicy).HasColumnName("StaffingPolicy");
            this.Property(t => t.Department).HasColumnName("Department");
            this.Property(t => t.HeadOfDepartment).HasColumnName("HeadOfDepartment");
            this.Property(t => t.SubordinateTo).HasColumnName("SubordinateTo");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.DtNominated).HasColumnName("DtNominated");
            this.Property(t => t.DtAccepted).HasColumnName("DtAccepted");
            this.Property(t => t.DtRemoved).HasColumnName("DtRemoved");
            this.Property(t => t.RemovedBy).HasColumnName("RemovedBy");
            this.Property(t => t.DtExpired).HasColumnName("DtExpired");
            this.Property(t => t.StructuredCompany).HasColumnName("StructuredCompany");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.ObjectPermissions).HasColumnName("ObjectPermissions");
        }
    }
}
