using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RoundingMap : EntityTypeConfiguration<Rounding>
    {
        public RoundingMap()
        {
            // Primary Key
            this.HasKey(t => t.TID);

            // Properties
            this.Property(t => t.TID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("Rounding", "xhc");
            this.Property(t => t.TID).HasColumnName("TID");
            this.Property(t => t.UnitsNumber).HasColumnName("UnitsNumber");
            this.Property(t => t.FixedNumber).HasColumnName("FixedNumber");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.FixedAmount).HasColumnName("FixedAmount");
            this.Property(t => t.QID).HasColumnName("QID");
            this.Property(t => t.FID).HasColumnName("FID");
            this.Property(t => t.AID).HasColumnName("AID");
        }
    }
}
