using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class ContractMap : EntityTypeConfiguration<Contract>
    {
        public ContractMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ContractNo)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Contracts");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ContractNo).HasColumnName("ContractNo");
            this.Property(t => t.ServiceProvider).HasColumnName("ServiceProvider");
            this.Property(t => t.Customer).HasColumnName("Customer");
            this.Property(t => t.ParentContract).HasColumnName("ParentContract");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.IsTemplate).HasColumnName("IsTemplate");
            this.Property(t => t.DtCommence).HasColumnName("DtCommence");
            this.Property(t => t.DtTerminate).HasColumnName("DtTerminate");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtActivated).HasColumnName("DtActivated");
            this.Property(t => t.DtDeleted).HasColumnName("DtDeleted");
            this.Property(t => t.DeletedBy).HasColumnName("DeletedBy");

            // Relationships
            this.HasOptional(t => t.CompanyElement)
                .WithMany(t => t.Contracts)
                .HasForeignKey(d => d.Customer);
            this.HasRequired(t => t.Person)
                .WithMany(t => t.Contracts)
                .HasForeignKey(d => d.Author);
            this.HasOptional(t => t.Person1)
                .WithMany(t => t.Contracts1)
                .HasForeignKey(d => d.DeletedBy);
            this.HasOptional(t => t.Contract1)
                .WithMany(t => t.Contracts1)
                .HasForeignKey(d => d.ParentContract);
            this.HasRequired(t => t.ServiceProvider1)
                .WithMany(t => t.Contracts)
                .HasForeignKey(d => d.ServiceProvider);

        }
    }
}
