using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class SuccessorMap : EntityTypeConfiguration<Successor>
    {
        public SuccessorMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Person, t.President, t.Foundation, t.Relationship, t.DtCreated });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Person)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.President)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Relationship)
                .IsRequired()
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("Successors");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.President).HasColumnName("President");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Relationship).HasColumnName("Relationship");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Document).HasColumnName("Document");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Successors)
                .HasForeignKey(d => d.Foundation);
            this.HasRequired(t => t.Person1)
                .WithMany(t => t.Successors)
                .HasForeignKey(d => d.Person);
            this.HasRequired(t => t.Person2)
                .WithMany(t => t.Successors1)
                .HasForeignKey(d => d.President);

        }
    }
}
