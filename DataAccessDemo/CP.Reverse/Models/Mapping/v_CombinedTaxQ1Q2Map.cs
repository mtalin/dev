using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class v_CombinedTaxQ1Q2Map : EntityTypeConfiguration<v_CombinedTaxQ1Q2>
    {
        public v_CombinedTaxQ1Q2Map()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Q1ID, t.Q2ID, t.Q1, t.Q2, t.Quarter, t.Step, t.Foundation, t.DtRepaymentTax, t.Year, t.Method, t.dtStartPeriod });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Q1ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Q2ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Q1)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Q2)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Quarter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Year)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Method)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("v_CombinedTaxQ1Q2", "Tax");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Q1ID).HasColumnName("Q1ID");
            this.Property(t => t.Q2ID).HasColumnName("Q2ID");
            this.Property(t => t.Q1).HasColumnName("Q1");
            this.Property(t => t.Q2).HasColumnName("Q2");
            this.Property(t => t.Quarter).HasColumnName("Quarter");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.DtRepaymentTax).HasColumnName("DtRepaymentTax");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.Method).HasColumnName("Method");
            this.Property(t => t.dtStartPeriod).HasColumnName("dtStartPeriod");
            this.Property(t => t.Tax).HasColumnName("Tax");
        }
    }
}
