using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class HouseholdAccountDetailMap : EntityTypeConfiguration<HouseholdAccountDetail>
    {
        public HouseholdAccountDetailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Foundation, t.Type, t.Amount, t.Step, t.Flags, t.Saving, t.IRA, t.ValueOfHouse, t.OtherAssets, t.CCDebt, t.CarLoans, t.Mortgage, t.OtherLoans, t.DtCreated, t.AuthorizedByName, t.AllowMailingCheckDirectly });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Step)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Address)
                .HasMaxLength(50);

            this.Property(t => t.ApartmentNo)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(25);

            this.Property(t => t.State)
                .HasMaxLength(30);

            this.Property(t => t.Zip)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.Country)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Phone)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Flags)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Saving)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IRA)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ValueOfHouse)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.OtherAssets)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CCDebt)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CarLoans)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Mortgage)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.OtherLoans)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(248);

            this.Property(t => t.Name2)
                .HasMaxLength(376);

            this.Property(t => t.TypeName)
                .HasMaxLength(150);

            this.Property(t => t.ShortTypeName)
                .HasMaxLength(50);

            this.Property(t => t.ShortTypeName2)
                .HasMaxLength(150);

            this.Property(t => t.PrimaryApplicantFirstName)
                .HasMaxLength(50);

            this.Property(t => t.PrimaryApplicantLastName)
                .HasMaxLength(50);

            this.Property(t => t.StatusName)
                .HasMaxLength(150);

            this.Property(t => t.AuthorizedByName)
                .IsRequired()
                .HasMaxLength(2105);

            this.Property(t => t.AddressName)
                .HasMaxLength(222);

            // Table & Column Mappings
            this.ToTable("HouseholdAccountDetails");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.ApartmentNo).HasColumnName("ApartmentNo");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Flags).HasColumnName("Flags");
            this.Property(t => t.Saving).HasColumnName("Saving");
            this.Property(t => t.IRA).HasColumnName("IRA");
            this.Property(t => t.ValueOfHouse).HasColumnName("ValueOfHouse");
            this.Property(t => t.OtherAssets).HasColumnName("OtherAssets");
            this.Property(t => t.CCDebt).HasColumnName("CCDebt");
            this.Property(t => t.CarLoans).HasColumnName("CarLoans");
            this.Property(t => t.Mortgage).HasColumnName("Mortgage");
            this.Property(t => t.OtherLoans).HasColumnName("OtherLoans");
            this.Property(t => t.InitialAmount).HasColumnName("InitialAmount");
            this.Property(t => t.DtAuthorized).HasColumnName("DtAuthorized");
            this.Property(t => t.AuthorizedBy).HasColumnName("AuthorizedBy");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtApproved).HasColumnName("DtApproved");
            this.Property(t => t.DtExpired).HasColumnName("DtExpired");
            this.Property(t => t.DtClosed).HasColumnName("DtClosed");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Name2).HasColumnName("Name2");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Property(t => t.ShortTypeName).HasColumnName("ShortTypeName");
            this.Property(t => t.ShortTypeName2).HasColumnName("ShortTypeName2");
            this.Property(t => t.PrimaryApplicant).HasColumnName("PrimaryApplicant");
            this.Property(t => t.PrimaryApplicantFirstName).HasColumnName("PrimaryApplicantFirstName");
            this.Property(t => t.PrimaryApplicantLastName).HasColumnName("PrimaryApplicantLastName");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.StatusName).HasColumnName("StatusName");
            this.Property(t => t.AuthorizedByName).HasColumnName("AuthorizedByName");
            this.Property(t => t.AddressName).HasColumnName("AddressName");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.AllowMailingCheckDirectly).HasColumnName("AllowMailingCheckDirectly");
            this.Property(t => t.dtDeleted).HasColumnName("dtDeleted");
        }
    }
}
