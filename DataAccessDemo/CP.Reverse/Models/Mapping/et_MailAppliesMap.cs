using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_MailAppliesMap : EntityTypeConfiguration<et_MailApplies>
    {
        public et_MailAppliesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.ReferralSource, t.ForAdvisor, t.ForClient, t.FirstName, t.LastName, t.PostalAddress, t.Phone1, t.EMail, t.Source, t.FullName });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.DateCreated)
                .HasMaxLength(30);

            this.Property(t => t.ReferralSource)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ForClient)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleInitials)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.PostalAddress)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Phone1)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Phone2)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.EMail)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Source)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FullName)
                .IsRequired()
                .HasMaxLength(152);

            // Table & Column Mappings
            this.ToTable("et_MailApplies");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.ReferralSource).HasColumnName("ReferralSource");
            this.Property(t => t.ForAdvisor).HasColumnName("ForAdvisor");
            this.Property(t => t.ForClient).HasColumnName("ForClient");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.MiddleInitials).HasColumnName("MiddleInitials");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.PostalAddress).HasColumnName("PostalAddress");
            this.Property(t => t.Phone1).HasColumnName("Phone1");
            this.Property(t => t.Phone2).HasColumnName("Phone2");
            this.Property(t => t.EMail).HasColumnName("EMail");
            this.Property(t => t.Source).HasColumnName("Source");
            this.Property(t => t.FullName).HasColumnName("FullName");
        }
    }
}
