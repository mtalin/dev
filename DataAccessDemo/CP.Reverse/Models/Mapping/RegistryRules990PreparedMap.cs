using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class RegistryRules990PreparedMap : EntityTypeConfiguration<RegistryRules990Prepared>
    {
        public RegistryRules990PreparedMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Part, t.LineId, t.ColumnId, t.RegistryRule, t.TransactType, t.AssetType });

            // Properties
            this.Property(t => t.LineId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ColumnId)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.RegistryRule)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TransactType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("RegistryRules990Prepared");
            this.Property(t => t.Part).HasColumnName("Part");
            this.Property(t => t.LineId).HasColumnName("LineId");
            this.Property(t => t.ColumnId).HasColumnName("ColumnId");
            this.Property(t => t.RegistryRule).HasColumnName("RegistryRule");
            this.Property(t => t.TransactType).HasColumnName("TransactType");
            this.Property(t => t.AssetType).HasColumnName("AssetType");
        }
    }
}
