using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class TaxMap : EntityTypeConfiguration<Tax>
    {
        public TaxMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.DataSnapShot)
                .HasMaxLength(3000);

            // Table & Column Mappings
            this.ToTable("Taxes");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Amount2).HasColumnName("Amount2");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DtPeriod).HasColumnName("DtPeriod");
            this.Property(t => t.PeriodType).HasColumnName("PeriodType");
            this.Property(t => t.PeriodCount).HasColumnName("PeriodCount");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.PendingTransact).HasColumnName("PendingTransact");
            this.Property(t => t.DataSnapShot).HasColumnName("DataSnapShot");
            this.Property(t => t.Contract).HasColumnName("Contract");
            this.Property(t => t.BillTo).HasColumnName("BillTo");
            this.Property(t => t.FeeCalculation).HasColumnName("FeeCalculation");
            this.Property(t => t.Q1Covered).HasColumnName("Q1Covered");
            this.Property(t => t.Q2Covered).HasColumnName("Q2Covered");
            this.Property(t => t.Q3Covered).HasColumnName("Q3Covered");
            this.Property(t => t.Q4Covered).HasColumnName("Q4Covered");

            // Relationships
            this.HasOptional(t => t.Contract1)
                .WithMany(t => t.Taxes)
                .HasForeignKey(d => d.Contract);
            this.HasOptional(t => t.FeeCalculation1)
                .WithMany(t => t.Taxes)
                .HasForeignKey(d => d.FeeCalculation);
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Taxes)
                .HasForeignKey(d => d.Foundation);
            this.HasOptional(t => t.PendingTransact1)
                .WithMany(t => t.Taxes)
                .HasForeignKey(d => d.PendingTransact);
            this.HasOptional(t => t.StructuredCompany)
                .WithMany(t => t.Taxes)
                .HasForeignKey(d => d.BillTo);
            this.HasOptional(t => t.User)
                .WithMany(t => t.Taxes)
                .HasForeignKey(d => d.Author);

        }
    }
}
