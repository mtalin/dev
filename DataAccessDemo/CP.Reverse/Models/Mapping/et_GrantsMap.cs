using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class et_GrantsMap : EntityTypeConfiguration<et_Grants>
    {
        public et_GrantsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.FoundationName, t.DateCreated, t.DonationAmount, t.SpecPurposeType, t.SpecPurposeText1, t.SpecPurposeText2, t.SpecialDelivery, t.Anonymous, t.IsSpecPurpose, t.IsAwaitngApproval, t.isRecipient, t.UseRecipientAddress });

            // Properties
            this.Property(t => t.FoundationName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.DonationAmount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SpecPurposeType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SpecPurposeText1)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeText2)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.SpecPurposeTypeName)
                .HasMaxLength(150);

            this.Property(t => t.Purpose)
                .HasMaxLength(50);

            this.Property(t => t.PurposeDescription)
                .HasMaxLength(1000);

            this.Property(t => t.NoteForEmail)
                .HasMaxLength(1000);

            this.Property(t => t.IsSpecPurpose)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsAwaitngApproval)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.isRecipient)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.UseRecipientAddress)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TargetedDescription)
                .HasMaxLength(1000);

            this.Property(t => t.ProgramName)
                .HasMaxLength(500);

            this.Property(t => t.ShortProgramName)
                .HasMaxLength(200);

            this.Property(t => t.CharityName)
                .HasMaxLength(200);

            this.Property(t => t.CharityAddress)
                .HasMaxLength(100);

            this.Property(t => t.CharityAddress2)
                .HasMaxLength(100);

            this.Property(t => t.CharityCity)
                .HasMaxLength(30);

            this.Property(t => t.CharityCountry)
                .HasMaxLength(50);

            this.Property(t => t.CharityStateZIPCountry)
                .HasMaxLength(200);

            this.Property(t => t.AltName)
                .HasMaxLength(200);

            this.Property(t => t.CheckPayableTo)
                .HasMaxLength(200);

            this.Property(t => t.CheckMemo)
                .HasMaxLength(200);

            this.Property(t => t.OfficerName)
                .HasMaxLength(200);

            this.Property(t => t.OfficerAddress)
                .HasMaxLength(101);

            this.Property(t => t.OfficerCityStateZip)
                .HasMaxLength(223);

            this.Property(t => t.AddressOfc1)
                .HasMaxLength(50);

            this.Property(t => t.AddressOfc2)
                .HasMaxLength(50);

            this.Property(t => t.OfcCareOf)
                .HasMaxLength(200);

            this.Property(t => t.Address1)
                .HasMaxLength(50);

            this.Property(t => t.Address2)
                .HasMaxLength(50);

            this.Property(t => t.FndCareOf)
                .HasMaxLength(200);

            this.Property(t => t.FndAddress)
                .HasMaxLength(101);

            this.Property(t => t.FndCityStateZip)
                .HasMaxLength(223);

            this.Property(t => t.ReqContactFirstName)
                .HasMaxLength(50);

            this.Property(t => t.ReqContactMiddleName)
                .HasMaxLength(50);

            this.Property(t => t.ReqContactLastName)
                .HasMaxLength(50);

            this.Property(t => t.ReqContactTitle)
                .HasMaxLength(70);

            this.Property(t => t.ReqContactSalutation)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("et_Grants");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.CharityEIN).HasColumnName("CharityEIN");
            this.Property(t => t.HouseholdAccount).HasColumnName("HouseholdAccount");
            this.Property(t => t.FoundationName).HasColumnName("FoundationName");
            this.Property(t => t.Person).HasColumnName("Person");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.DonationAmount).HasColumnName("DonationAmount");
            this.Property(t => t.SpecPurposeType).HasColumnName("SpecPurposeType");
            this.Property(t => t.SpecPurposeText1).HasColumnName("SpecPurposeText1");
            this.Property(t => t.SpecPurposeText2).HasColumnName("SpecPurposeText2");
            this.Property(t => t.SpecialDelivery).HasColumnName("SpecialDelivery");
            this.Property(t => t.Anonymous).HasColumnName("Anonymous");
            this.Property(t => t.SpecPurposeTypeName).HasColumnName("SpecPurposeTypeName");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.PurposeDescription).HasColumnName("PurposeDescription");
            this.Property(t => t.NoteForEmail).HasColumnName("NoteForEmail");
            this.Property(t => t.IsSpecPurpose).HasColumnName("IsSpecPurpose");
            this.Property(t => t.IsAwaitngApproval).HasColumnName("IsAwaitngApproval");
            this.Property(t => t.IsMultiYear).HasColumnName("IsMultiYear");
            this.Property(t => t.isRecipient).HasColumnName("isRecipient");
            this.Property(t => t.UseRecipientAddress).HasColumnName("UseRecipientAddress");
            this.Property(t => t.NumPayments).HasColumnName("NumPayments");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
            this.Property(t => t.PaymentNumber).HasColumnName("PaymentNumber");
            this.Property(t => t.TargetedDescription).HasColumnName("TargetedDescription");
            this.Property(t => t.ProgramName).HasColumnName("ProgramName");
            this.Property(t => t.FoundationProject).HasColumnName("FoundationProject");
            this.Property(t => t.ParentProgram).HasColumnName("ParentProgram");
            this.Property(t => t.ShortProgramName).HasColumnName("ShortProgramName");
            this.Property(t => t.CharityName).HasColumnName("CharityName");
            this.Property(t => t.CharityAddress).HasColumnName("CharityAddress");
            this.Property(t => t.CharityAddress2).HasColumnName("CharityAddress2");
            this.Property(t => t.CharityCity).HasColumnName("CharityCity");
            this.Property(t => t.CharityCountry).HasColumnName("CharityCountry");
            this.Property(t => t.CharityStateZIPCountry).HasColumnName("CharityStateZIPCountry");
            this.Property(t => t.AltName).HasColumnName("AltName");
            this.Property(t => t.FoundationCode).HasColumnName("FoundationCode");
            this.Property(t => t.FiscalAgent).HasColumnName("FiscalAgent");
            this.Property(t => t.CheckPayableTo).HasColumnName("CheckPayableTo");
            this.Property(t => t.CheckMemo).HasColumnName("CheckMemo");
            this.Property(t => t.DonationID).HasColumnName("DonationID");
            this.Property(t => t.OfficerName).HasColumnName("OfficerName");
            this.Property(t => t.OfficerAddress).HasColumnName("OfficerAddress");
            this.Property(t => t.OfficerCityStateZip).HasColumnName("OfficerCityStateZip");
            this.Property(t => t.AddressOfc1).HasColumnName("AddressOfc1");
            this.Property(t => t.AddressOfc2).HasColumnName("AddressOfc2");
            this.Property(t => t.OfcCareOf).HasColumnName("OfcCareOf");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.Address2).HasColumnName("Address2");
            this.Property(t => t.FndCareOf).HasColumnName("FndCareOf");
            this.Property(t => t.FndAddress).HasColumnName("FndAddress");
            this.Property(t => t.FndCityStateZip).HasColumnName("FndCityStateZip");
            this.Property(t => t.ReqContactFirstName).HasColumnName("ReqContactFirstName");
            this.Property(t => t.ReqContactMiddleName).HasColumnName("ReqContactMiddleName");
            this.Property(t => t.ReqContactLastName).HasColumnName("ReqContactLastName");
            this.Property(t => t.ReqContactTitle).HasColumnName("ReqContactTitle");
            this.Property(t => t.ReqContactSalutation).HasColumnName("ReqContactSalutation");
            this.Property(t => t.FiscalEndDate).HasColumnName("FiscalEndDate");
        }
    }
}
