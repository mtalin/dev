using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class Registry990p3Map : EntityTypeConfiguration<Registry990p3>
    {
        public Registry990p3Map()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.Amount, t.Currency, t.Asset, t.Transact, t.RegistryRule, t.TransactType, t.DtTransact, t.AssetType, t.Foundation, t.FoundationAccount, t.LineId, t.Part, t.Type });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Currency)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Asset)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Transact)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RegistryRule)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TransactType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Foundation)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FoundationAccount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LineId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Part)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("Registry990p3");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Currency).HasColumnName("Currency");
            this.Property(t => t.Asset).HasColumnName("Asset");
            this.Property(t => t.Transact).HasColumnName("Transact");
            this.Property(t => t.RegistryRule).HasColumnName("RegistryRule");
            this.Property(t => t.TransactType).HasColumnName("TransactType");
            this.Property(t => t.DtTransact).HasColumnName("DtTransact");
            this.Property(t => t.AssetType).HasColumnName("AssetType");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.FoundationAccount).HasColumnName("FoundationAccount");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
            this.Property(t => t.LineId).HasColumnName("LineId");
            this.Property(t => t.Part).HasColumnName("Part");
            this.Property(t => t.Type).HasColumnName("Type");
        }
    }
}
