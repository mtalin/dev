using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class old_PendingTransactItemsMap : EntityTypeConfiguration<old_PendingTransactItems>
    {
        public old_PendingTransactItemsMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.CheckNo)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("old_PendingTransactItems");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.PendingTransact).HasColumnName("PendingTransact");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.CheckNo).HasColumnName("CheckNo");
            this.Property(t => t.DtCheck).HasColumnName("DtCheck");

            // Relationships
            this.HasRequired(t => t.PendingTransact1)
                .WithMany(t => t.old_PendingTransactItems)
                .HasForeignKey(d => d.PendingTransact);

        }
    }
}
