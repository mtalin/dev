using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class HouseholdValueMap : EntityTypeConfiguration<HouseholdValue>
    {
        public HouseholdValueMap()
        {
            // Primary Key
            this.HasKey(t => new { t.HouseholdType, t.Type, t.Amount, t.DtCreated });

            // Properties
            this.Property(t => t.HouseholdType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Type)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Amount)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("HouseholdValues");
            this.Property(t => t.HouseholdType).HasColumnName("HouseholdType");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Persons).HasColumnName("Persons");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
        }
    }
}
