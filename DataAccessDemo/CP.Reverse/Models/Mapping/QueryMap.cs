using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace CP.Reverse.Models.Mapping
{
    public class QueryMap : EntityTypeConfiguration<Query>
    {
        public QueryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(80);

            this.Property(t => t.DbName)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("Queries");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.QueryCategory).HasColumnName("QueryCategory");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Query1).HasColumnName("Query");
            this.Property(t => t.DbName).HasColumnName("DbName");
            this.Property(t => t.QueryParams).HasColumnName("QueryParams");

            // Relationships
            this.HasOptional(t => t.QueryCategory1)
                .WithMany(t => t.Queries)
                .HasForeignKey(d => d.QueryCategory);

        }
    }
}
