using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReturnState
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public string State { get; set; }
        public string Comments { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
