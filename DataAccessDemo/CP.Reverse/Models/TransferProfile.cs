using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TransferProfile
    {
        public TransferProfile()
        {
            this.TransferGroups = new List<TransferGroup>();
        }

        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Contract { get; set; }
        public int Foundation { get; set; }
        public bool IsCharity { get; set; }
        public Nullable<int> Signer1 { get; set; }
        public Nullable<int> Signer2 { get; set; }
        public Nullable<int> Signer3 { get; set; }
        public string SignerOther { get; set; }
        public int Method { get; set; }
        public string EmailInstructionNote { get; set; }
        public string TransferMemo { get; set; }
        public decimal MinAmount { get; set; }
        public Nullable<decimal> MaxAmount { get; set; }
        public bool SpecialHandling { get; set; }
        public string SpecialNote { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public bool IsDraft { get; set; }
        public virtual Contract Contract1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Person Person2 { get; set; }
        public virtual Person Person3 { get; set; }
        public virtual Person Person4 { get; set; }
        public virtual ICollection<TransferGroup> TransferGroups { get; set; }
    }
}
