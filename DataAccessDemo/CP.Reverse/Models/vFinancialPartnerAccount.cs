using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vFinancialPartnerAccount
    {
        public int ID { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public Nullable<int> Foundation { get; set; }
        public string AccountName { get; set; }
        public string BankName { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string BankAddress { get; set; }
        public string Transactions { get; set; }
    }
}
