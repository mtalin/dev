using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PersonRightsHistory
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public int Author { get; set; }
        public int PersonFoundation { get; set; }
        public int Role { get; set; }
        public Nullable<int> OldRole { get; set; }
        public string Rights { get; set; }
        public string OldRights { get; set; }
        public virtual PersonFoundation PersonFoundation1 { get; set; }
        public virtual PersonRole PersonRole { get; set; }
        public virtual PersonRole PersonRole1 { get; set; }
        public virtual User User { get; set; }
    }
}
