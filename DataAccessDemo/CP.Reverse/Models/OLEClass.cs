using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class OLEClass
    {
        public OLEClass()
        {
            this.Tasks_Old = new List<Tasks_Old>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Tasks_Old> Tasks_Old { get; set; }
    }
}
