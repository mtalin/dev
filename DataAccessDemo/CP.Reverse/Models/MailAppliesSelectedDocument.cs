using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MailAppliesSelectedDocument
    {
        public int MailApply { get; set; }
        public int MailAppliesDocument { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public Nullable<System.DateTime> DtSent { get; set; }
        public virtual MailApply MailApply1 { get; set; }
        public virtual MailAppliesDocument MailAppliesDocument1 { get; set; }
        public virtual User User { get; set; }
    }
}
