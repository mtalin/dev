using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vQuestAllHasIssue
    {
        public int id { get; set; }
        public Nullable<int> Section { get; set; }
        public Nullable<bool> HasIssues { get; set; }
        public Nullable<int> Resolved { get; set; }
        public Nullable<bool> Enabled { get; set; }
        public Nullable<int> ResolvedBy { get; set; }
        public string dtResolved { get; set; }
        public Nullable<int> fsAddrIssuesBy { get; set; }
    }
}
