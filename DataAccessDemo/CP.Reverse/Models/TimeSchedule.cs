using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TimeSchedule
    {
        public TimeSchedule()
        {
            this.Expenses = new List<Expens>();
        }

        public int ID { get; set; }
        public System.DateTime DtStart { get; set; }
        public Nullable<System.DateTime> DtStop { get; set; }
        public int Type { get; set; }
        public Nullable<int> DayPeriod { get; set; }
        public Nullable<int> WeekPeriod { get; set; }
        public Nullable<int> Day { get; set; }
        public Nullable<int> Week { get; set; }
        public string DayList { get; set; }
        public string MonthList { get; set; }
        public int EventClass { get; set; }
        public Nullable<System.DateTime> DtLastRun { get; set; }
        public Nullable<System.DateTime> DtNextRun { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string HotData { get; set; }
        public double Reminder { get; set; }
        public bool OnHold { get; set; }
        public bool SplitOverrides { get; set; }
        public bool InProcess { get; set; }
        public bool BusinessDaysOnly { get; set; }
        public int DaysBefore { get; set; }
        public Nullable<System.DateTime> ScheduledDate { get; set; }
        public virtual EventClass EventClass1 { get; set; }
        public virtual ICollection<Expens> Expenses { get; set; }
        public virtual GrantSchedule GrantSchedule { get; set; }
    }
}
