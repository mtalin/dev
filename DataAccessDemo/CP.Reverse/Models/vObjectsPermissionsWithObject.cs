using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vObjectsPermissionsWithObject
    {
        public int Id { get; set; }
        public string Object { get; set; }
        public string AppliedPermissions { get; set; }
        public string ResolvedPermissions { get; set; }
        public string NoteText { get; set; }
        public Nullable<int> NoteAuthor { get; set; }
        public Nullable<int> ObjectId { get; set; }
    }
}
