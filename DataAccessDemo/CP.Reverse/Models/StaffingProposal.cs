using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class StaffingProposal
    {
        public int ID { get; set; }
        public int Proposal { get; set; }
        public Nullable<bool> Excluded { get; set; }
        public Nullable<int> Person { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int TheAction { get; set; }
        public int Appointment { get; set; }
        public Nullable<int> OldAppointment { get; set; }
        public Nullable<int> DetectedProblems { get; set; }
        public virtual Proposal Proposal1 { get; set; }
    }
}
