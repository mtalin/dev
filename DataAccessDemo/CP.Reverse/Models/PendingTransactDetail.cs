using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PendingTransactDetail
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> Transact { get; set; }
        public decimal Amount { get; set; }
        public int Type { get; set; }
        public string Reason { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public int Step { get; set; }
        public Nullable<System.DateTime> DtFaxGenerated { get; set; }
        public Nullable<int> DtFaxAuthor { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
        public Nullable<int> PeriodType { get; set; }
        public Nullable<int> FoundationAccount { get; set; }
        public Nullable<int> TransferGroup { get; set; }
        public Nullable<int> Payment { get; set; }
        public Nullable<int> LastQTransaction { get; set; }
        public Nullable<int> ACHTransact { get; set; }
        public Nullable<bool> Outside { get; set; }
        public string TargetName { get; set; }
        public string TypeName { get; set; }
        public string LongTypeName { get; set; }
        public string TypeInfo { get; set; }
        public Nullable<int> Recurring { get; set; }
        public Nullable<int> Donation { get; set; }
        public Nullable<int> Tax { get; set; }
        public Nullable<int> Expense { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public Nullable<int> FinancialAdvisor { get; set; }
        public string FoundationName { get; set; }
        public string FinancialPartnerName { get; set; }
        public string FinancialAdvisorName { get; set; }
        public Nullable<System.DateTime> TaxPeriod { get; set; }
        public Nullable<int> TaxPeriodType { get; set; }
    }
}
