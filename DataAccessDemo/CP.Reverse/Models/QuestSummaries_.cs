using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QuestSummaries_
    {
        public int id { get; set; }
        public System.DateTime dtCreated { get; set; }
        public int Author { get; set; }
        public int Questionnaire { get; set; }
        public int Section { get; set; }
        public bool HasIssues { get; set; }
        public int Resolved { get; set; }
        public bool Enabled { get; set; }
        public Nullable<int> ResolvedBy { get; set; }
        public Nullable<System.DateTime> dtResolved { get; set; }
        public Nullable<int> fsAddrIssuesBy { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Person Person2 { get; set; }
    }
}
