using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QDeposit
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public System.DateTime DtTransact { get; set; }
        public Nullable<int> Parent { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> Donor { get; set; }
        public string Security { get; set; }
        public int Shares { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Type { get; set; }
        public bool Restricted { get; set; }
        public Nullable<int> Transact { get; set; }
        public Nullable<decimal> HighPrice { get; set; }
        public Nullable<decimal> LowPrice { get; set; }
        public Nullable<decimal> AveragePrice { get; set; }
        public Nullable<int> QAdmin { get; set; }
        public Nullable<int> ReadOnly { get; set; }
        public Nullable<int> Allocated { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual User User { get; set; }
    }
}
