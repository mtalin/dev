using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class enums_Designs
    {
        public int ItemID { get; set; }
        public string Name { get; set; }
        public string Abbrev { get; set; }
        public string Flags { get; set; }
        public Nullable<int> SortOrder { get; set; }
    }
}
