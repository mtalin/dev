using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CustomReportsTemplate
    {
        public int CustomReport { get; set; }
        public Nullable<int> DocumentTemplate { get; set; }
        public int Format { get; set; }
        public virtual CustomReport CustomReport1 { get; set; }
        public virtual DocumentTemplate DocumentTemplate1 { get; set; }
    }
}
