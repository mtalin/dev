using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class old_Nominees
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Person { get; set; }
        public int Appointment { get; set; }
        public decimal DonationsLimit { get; set; }
        public int RegistrationStep { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> NewPosition { get; set; }
        public Nullable<int> StaffingProposal { get; set; }
        public Nullable<System.DateTime> DtAccDeadline { get; set; }
        public Nullable<int> DeclaredAppointment { get; set; }
        public Nullable<System.DateTime> DtAccepted { get; set; }
        public bool Reelection { get; set; }
        public Nullable<decimal> ApprovableLimit { get; set; }
        public bool GrantCommitteeMember { get; set; }
        public Nullable<System.DateTime> DtTermsExpired { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
