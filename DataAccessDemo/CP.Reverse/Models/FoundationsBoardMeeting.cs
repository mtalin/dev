using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FoundationsBoardMeeting
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int MeetingMonth { get; set; }
        public int MeetingDay { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
