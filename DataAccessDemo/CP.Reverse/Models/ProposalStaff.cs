using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ProposalStaff
    {
        public int Proposal { get; set; }
        public int Person { get; set; }
        public int Appointment { get; set; }
        public int NomineeStep { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Proposal Proposal1 { get; set; }
    }
}
