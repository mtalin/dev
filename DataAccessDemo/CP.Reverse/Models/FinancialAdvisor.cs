using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FinancialAdvisor
    {
        public FinancialAdvisor()
        {
            this.FAPPackageRequests = new List<FAPPackageRequest>();
            this.FinancialAdvisorsFoundations = new List<FinancialAdvisorsFoundation>();
            this.FinancialAdvisorsRequests = new List<FinancialAdvisorsRequest>();
        }

        public int ID { get; set; }
        public string Alt_Contact { get; set; }
        public string Alt_Phone { get; set; }
        public string Alt_Email { get; set; }
        public string URL { get; set; }
        public string D_BankName { get; set; }
        public string D_ABA { get; set; }
        public string D_AccName { get; set; }
        public string D_AccNumber { get; set; }
        public string M_BankName { get; set; }
        public string M_ABA { get; set; }
        public string M_AccName { get; set; }
        public string M_AccNumber { get; set; }
        public Nullable<System.DateTime> DtAccepted { get; set; }
        public Nullable<int> FinancialAdvisorGroup { get; set; }
        public string FirmName { get; set; }
        public Nullable<int> CompanyPosition { get; set; }
        public Nullable<int> StaffingPolicy { get; set; }
        public virtual ICollection<FAPPackageRequest> FAPPackageRequests { get; set; }
        public virtual FinancialAdvisorGroup FinancialAdvisorGroup1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<FinancialAdvisorsFoundation> FinancialAdvisorsFoundations { get; set; }
        public virtual ICollection<FinancialAdvisorsRequest> FinancialAdvisorsRequests { get; set; }
    }
}
