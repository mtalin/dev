using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TrHoldingMap
    {
        public int ID { get; set; }
        public string HoldingRule { get; set; }
        public string Name { get; set; }
        public virtual TrHoldingRule TrHoldingRule { get; set; }
    }
}
