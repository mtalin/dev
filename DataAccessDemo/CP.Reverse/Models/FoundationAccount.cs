using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FoundationAccount
    {
        public FoundationAccount()
        {
            this.FoundationAccountsSteps = new List<FoundationAccountsStep>();
            this.PendingTransacts = new List<PendingTransact>();
            this.QModifyReports = new List<QModifyReport>();
            this.QModifyTransactions = new List<QModifyTransaction>();
            this.QTransactions = new List<QTransaction>();
            this.StmtTrackings = new List<StmtTracking>();
            this.Transacts = new List<Transact>();
            this.TransferGroups = new List<TransferGroup>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public string Account { get; set; }
        public int AccountType { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> DeleteAuthor { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public string BankName { get; set; }
        public Nullable<int> BankAddress { get; set; }
        public int Status { get; set; }
        public Nullable<System.DateTime> DtReconciled { get; set; }
        public Nullable<int> Access { get; set; }
        public string PortfolioCode { get; set; }
        public bool OnlineAccess { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public string AccountName { get; set; }
        public int ServiceProvider { get; set; }
        public Nullable<int> Department { get; set; }
        public Nullable<int> Statements { get; set; }
        public Nullable<int> Frequency { get; set; }
        public string URL { get; set; }
        public Nullable<int> CheckingAccount { get; set; }
        public string RoutingNumber { get; set; }
        public Nullable<int> EFTPSAccountType { get; set; }
        public string BatchFilerID { get; set; }
        public string MasterInquiryPIN { get; set; }
        public Nullable<int> PurposeType { get; set; }
        public Nullable<System.DateTime> DtOpened { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public string Note { get; set; }
        public Nullable<int> InvestmentCompany { get; set; }
        public Nullable<int> InvestmentCompanyDepartment { get; set; }
        public Nullable<int> Division { get; set; }
        public Nullable<int> YearEnd { get; set; }
        public virtual CheckingAccount CheckingAccount1 { get; set; }
        public virtual Department Department1 { get; set; }
        public virtual Department Department2 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual PostalAddress PostalAddress { get; set; }
        public virtual ServiceProvider ServiceProvider1 { get; set; }
        public virtual ServiceProvider ServiceProvider2 { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
        public virtual ICollection<FoundationAccountsStep> FoundationAccountsSteps { get; set; }
        public virtual ICollection<PendingTransact> PendingTransacts { get; set; }
        public virtual ICollection<QModifyReport> QModifyReports { get; set; }
        public virtual ICollection<QModifyTransaction> QModifyTransactions { get; set; }
        public virtual ICollection<QTransaction> QTransactions { get; set; }
        public virtual ICollection<StmtTracking> StmtTrackings { get; set; }
        public virtual ICollection<Transact> Transacts { get; set; }
        public virtual ICollection<TransferGroup> TransferGroups { get; set; }
    }
}
