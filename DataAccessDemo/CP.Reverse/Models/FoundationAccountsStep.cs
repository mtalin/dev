using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FoundationAccountsStep
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int FoundationAccount { get; set; }
        public int Author { get; set; }
        public Nullable<System.DateTime> DtReconciled_Prev { get; set; }
        public Nullable<System.DateTime> DtReconciled_New { get; set; }
        public Nullable<int> Status_Prev { get; set; }
        public int Status_New { get; set; }
        public Nullable<int> AccountType_Prev { get; set; }
        public int AccountType_New { get; set; }
        public Nullable<int> FinancialPartner_Prev { get; set; }
        public Nullable<int> FinancialPartner_New { get; set; }
        public Nullable<int> ServiceProvider_Prev { get; set; }
        public Nullable<int> ServiceProvider_New { get; set; }
        public Nullable<int> Department_Prev { get; set; }
        public Nullable<int> Department_New { get; set; }
        public virtual FoundationAccount FoundationAccount1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
