using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Position
    {
        public Position()
        {
            this.OfficersPositions = new List<OfficersPosition>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Foundation { get; set; }
        public string Name { get; set; }
        public string Rights { get; set; }
        public int Priority { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<OfficersPosition> OfficersPositions { get; set; }
    }
}
