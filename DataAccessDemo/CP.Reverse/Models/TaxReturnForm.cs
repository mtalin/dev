using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReturnForm
    {
        public TaxReturnForm()
        {
            this.TaxReturnDocuments = new List<TaxReturnDocument>();
        }

        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int TaxReturn { get; set; }
        public int Theme { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public string Note { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<TaxReturnDocument> TaxReturnDocuments { get; set; }
        public virtual TaxReturn TaxReturn1 { get; set; }
    }
}
