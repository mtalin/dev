using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vHistoryInfo
    {
        public int Foundation { get; set; }
        public Nullable<decimal> TotalDonations { get; set; }
        public Nullable<decimal> TotalAccounted { get; set; }
    }
}
