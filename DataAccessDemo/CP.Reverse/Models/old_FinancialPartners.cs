using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class old_FinancialPartners
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string PhoneExt { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public Nullable<int> Address { get; set; }
        public string Institution { get; set; }
        public string URL { get; set; }
        public string AccessURL { get; set; }
        public Nullable<decimal> Step6Amount { get; set; }
        public string Step6Note { get; set; }
        public bool SupportsOnline { get; set; }
        public bool IsTestPartner { get; set; }
        public Nullable<double> FlatPartnerRate { get; set; }
        public string BrochureSheetTitle { get; set; }
        public int SingleSignOn { get; set; }
        public bool HideSuggestion { get; set; }
        public string UID { get; set; }
        public Nullable<int> StructuredCompany { get; set; }
        public string tmp_Action { get; set; }
    }
}
