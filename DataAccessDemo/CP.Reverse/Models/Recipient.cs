using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Recipient
    {
        public Recipient()
        {
            this.EmailRecipients = new List<EmailRecipient>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string CustomEmail { get; set; }
        public Nullable<int> Person { get; set; }
        public virtual ICollection<EmailRecipient> EmailRecipients { get; set; }
        public virtual EventRecipient EventRecipient { get; set; }
        public virtual GlobalRecipient GlobalRecipient { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual SystemRecipient SystemRecipient { get; set; }
    }
}
