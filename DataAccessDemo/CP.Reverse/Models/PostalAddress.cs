using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PostalAddress
    {
        public PostalAddress()
        {
            this.CheckPayees = new List<CheckPayee>();
            this.Companies = new List<Company>();
            this.Departments = new List<Department>();
            this.ExpenseReports = new List<ExpenseReport>();
            this.Expenses = new List<Expens>();
            this.Expenses1 = new List<Expens>();
            this.FAPPackageForms = new List<FAPPackageForm>();
            this.FAPPackageRequests = new List<FAPPackageRequest>();
            this.FinancialAdvisorGroups = new List<FinancialAdvisorGroup>();
            this.FoundationAccounts = new List<FoundationAccount>();
            this.Foundations = new List<Foundation>();
            this.Foundations1 = new List<Foundation>();
            this.Messages = new List<Message>();
            this.Payments = new List<Payment>();
            this.Persons = new List<Person>();
            this.Persons1 = new List<Person>();
            this.StructuredCompanies = new List<StructuredCompany>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Company { get; set; }
        public Nullable<int> Type { get; set; }
        public string Description { get; set; }
        public string CareOf { get; set; }
        public string Salutation { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public virtual ICollection<CheckPayee> CheckPayees { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<Department> Departments { get; set; }
        public virtual ICollection<ExpenseReport> ExpenseReports { get; set; }
        public virtual ICollection<Expens> Expenses { get; set; }
        public virtual ICollection<Expens> Expenses1 { get; set; }
        public virtual ICollection<FAPPackageForm> FAPPackageForms { get; set; }
        public virtual ICollection<FAPPackageRequest> FAPPackageRequests { get; set; }
        public virtual ICollection<FinancialAdvisorGroup> FinancialAdvisorGroups { get; set; }
        public virtual ICollection<FoundationAccount> FoundationAccounts { get; set; }
        public virtual ICollection<Foundation> Foundations { get; set; }
        public virtual ICollection<Foundation> Foundations1 { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
        public virtual ICollection<Person> Persons1 { get; set; }
        public virtual ICollection<StructuredCompany> StructuredCompanies { get; set; }
    }
}
