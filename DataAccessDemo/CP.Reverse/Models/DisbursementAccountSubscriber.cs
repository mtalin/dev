using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DisbursementAccountSubscriber
    {
        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> ServiceProvider { get; set; }
        public Nullable<int> Contract { get; set; }
        public int DisbursementAccount { get; set; }
        public string Transactions { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual Contract Contract1 { get; set; }
        public virtual DisbursementAccount DisbursementAccount1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ServiceProvider ServiceProvider1 { get; set; }
    }
}
