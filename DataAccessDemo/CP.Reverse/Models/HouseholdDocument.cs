using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class HouseholdDocument
    {
        public int ID { get; set; }
        public int Document { get; set; }
        public int HouseholdAccount { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual Document1 Document1 { get; set; }
        public virtual HouseholdAccount HouseholdAccount1 { get; set; }
    }
}
