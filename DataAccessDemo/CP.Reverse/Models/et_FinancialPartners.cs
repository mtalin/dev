using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_FinancialPartners
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool isSSORequired { get; set; }
        public string Phone { get; set; }
        public Nullable<int> PhoneExt { get; set; }
        public string Fax { get; set; }
        public Nullable<int> Email { get; set; }
        public Nullable<int> Address { get; set; }
        public string Institution { get; set; }
        public string Contact { get; set; }
        public string URL { get; set; }
        public Nullable<int> AccessURL { get; set; }
    }
}
