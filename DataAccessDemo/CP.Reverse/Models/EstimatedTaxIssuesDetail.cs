using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EstimatedTaxIssuesDetail
    {
        public int ID { get; set; }
        public int FSAccount { get; set; }
        public string Foundation { get; set; }
        public Nullable<int> PcaId { get; set; }
        public string PCA { get; set; }
        public Nullable<int> RegionId { get; set; }
        public string Region { get; set; }
        public int YE { get; set; }
        public int Status { get; set; }
        public Nullable<int> Reconciliation { get; set; }
        public Nullable<int> MCB { get; set; }
        public Nullable<int> ULYM { get; set; }
        public Nullable<int> PCD { get; set; }
        public Nullable<int> MC { get; set; }
        public Nullable<int> PXIN { get; set; }
        public Nullable<int> DataChange { get; set; }
        public Nullable<int> Other { get; set; }
        public Nullable<int> MissStatements { get; set; }
        public Nullable<int> CapitalGainsReview { get; set; }
        public Nullable<int> NotFunded { get; set; }
        public Nullable<int> Upload990PF { get; set; }
        public Nullable<int> PartnershipCD { get; set; }
        public System.DateTime dtRepaymentTax { get; set; }
        public string StatusName { get; set; }
        public string Note { get; set; }
        public int Quarter { get; set; }
        public Nullable<System.DateTime> dtStatusChange { get; set; }
        public string FoundationName { get; set; }
    }
}
