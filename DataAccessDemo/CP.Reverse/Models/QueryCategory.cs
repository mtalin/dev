using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QueryCategory
    {
        public QueryCategory()
        {
            this.Queries = new List<Query>();
        }

        public int ID { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Query> Queries { get; set; }
    }
}
