using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GlobalRecipient
    {
        public int ID { get; set; }
        public string SQLScript { get; set; }
        public string Flags { get; set; }
        public virtual Recipient Recipient { get; set; }
    }
}
