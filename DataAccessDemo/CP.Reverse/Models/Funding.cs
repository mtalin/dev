using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Funding
    {
        public int ID { get; set; }
        public int Person { get; set; }
        public int Foundation { get; set; }
        public int Type { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public decimal Amount { get; set; }
        public Nullable<int> RecurringID { get; set; }
        public Nullable<int> RecurringFreq { get; set; }
        public string EFTBankName { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public string StockTransferDestination { get; set; }
        public int Step { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
