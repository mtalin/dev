using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class UpdatesLog
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public System.DateTime DtCreated { get; set; }
    }
}
