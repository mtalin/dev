using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PartnershipsStep
    {
        public int Id { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Partnership { get; set; }
        public int Step { get; set; }
        public virtual Partnership Partnership1 { get; set; }
        public virtual User User { get; set; }
    }
}
