using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CompanyPosition
    {
        public CompanyPosition()
        {
            this.CompanyPositions1 = new List<CompanyPosition>();
            this.PricingScheduleInstances = new List<PricingScheduleInstance>();
        }

        public int ID { get; set; }
        public int StaffingPolicy { get; set; }
        public Nullable<int> Department { get; set; }
        public bool HeadOfDepartment { get; set; }
        public Nullable<int> SubordinateTo { get; set; }
        public Nullable<int> Person { get; set; }
        public Nullable<System.DateTime> DtNominated { get; set; }
        public Nullable<System.DateTime> DtAccepted { get; set; }
        public Nullable<System.DateTime> DtRemoved { get; set; }
        public Nullable<int> RemovedBy { get; set; }
        public Nullable<System.DateTime> DtExpired { get; set; }
        public int StructuredCompany { get; set; }
        public Nullable<int> Note { get; set; }
        public virtual CompanyElement CompanyElement { get; set; }
        public virtual ICollection<CompanyPosition> CompanyPositions1 { get; set; }
        public virtual CompanyPosition CompanyPosition1 { get; set; }
        public virtual Department Department1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Person Person2 { get; set; }
        public virtual StaffingPolicy StaffingPolicy1 { get; set; }
        public virtual StructuredCompany StructuredCompany1 { get; set; }
        public virtual ICollection<PricingScheduleInstance> PricingScheduleInstances { get; set; }
    }
}
