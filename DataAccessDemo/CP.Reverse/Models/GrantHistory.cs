using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GrantHistory
    {
        public int ID { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<System.DateTime> DtGrant { get; set; }
        public decimal Amount { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public Nullable<int> CharityEIN { get; set; }
        public Nullable<int> Grantor { get; set; }
        public bool Anonymous { get; set; }
        public string Purpose { get; set; }
        public string PurposeDescription { get; set; }
        public Nullable<int> SpecPurposeType { get; set; }
        public string SpecPurposeText1 { get; set; }
        public string SpecPurposeText2 { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public Nullable<int> GrantLetter { get; set; }
        public int GrantDetails { get; set; }
    }
}
