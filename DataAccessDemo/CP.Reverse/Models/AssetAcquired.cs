using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AssetAcquired
    {
        public int Transact { get; set; }
        public int Asset { get; set; }
        public string HowAcq { get; set; }
        public Nullable<System.DateTime> DateAcq { get; set; }
        public Nullable<int> Purchase { get; set; }
        public Nullable<int> Donate { get; set; }
        public Nullable<int> Begining { get; set; }
    }
}
