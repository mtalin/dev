using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Rounding
    {
        public int TID { get; set; }
        public Nullable<decimal> UnitsNumber { get; set; }
        public Nullable<decimal> FixedNumber { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> FixedAmount { get; set; }
        public Nullable<int> QID { get; set; }
        public Nullable<int> FID { get; set; }
        public Nullable<int> AID { get; set; }
    }
}
