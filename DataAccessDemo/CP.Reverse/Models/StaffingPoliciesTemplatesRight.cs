using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class StaffingPoliciesTemplatesRight
    {
        public int ID { get; set; }
        public int CompanyType { get; set; }
        public int SystemPosition { get; set; }
        public string Name { get; set; }
        public string Permissions { get; set; }
        public string AppliedPermissions { get; set; }
        public string InheritedPermissions { get; set; }
    }
}
