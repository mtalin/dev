using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QBalance
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public System.DateTime DtBalance { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> FoundationAccount { get; set; }
        public string Account { get; set; }
        public decimal Cash { get; set; }
        public decimal Securities { get; set; }
        public decimal CashDiscrepancy { get; set; }
        public decimal SecurityDiscrepancy { get; set; }
        public int QReport { get; set; }
        public int ReportLine { get; set; }
        public string ErrorInfo { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeleteAuthor { get; set; }
        public string Memo { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public virtual QReport QReport1 { get; set; }
    }
}
