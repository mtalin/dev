using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EventSource
    {
        public EventSource()
        {
            this.EventClasses = new List<EventClass>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<EventClass> EventClasses { get; set; }
    }
}
