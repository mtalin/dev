using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FoundationsGrowthInfo
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> GrowthLevel { get; set; }
        public Nullable<int> InfluenceLevel { get; set; }
        public Nullable<int> Staffed { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public int Author { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
