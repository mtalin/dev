using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class bak_ActivityPricings
    {
        public int ID { get; set; }
        public int PricingScheduleInstance { get; set; }
        public Nullable<decimal> UptoAmount { get; set; }
        public decimal BaseAmount { get; set; }
        public decimal FeeValue { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
    }
}
