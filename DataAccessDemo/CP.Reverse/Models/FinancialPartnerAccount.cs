using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FinancialPartnerAccount
    {
        public int ID { get; set; }
        public int FinancialPartner { get; set; }
        public string AccountName { get; set; }
        public string BankName { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string BankAddress { get; set; }
        public string Transactions { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
