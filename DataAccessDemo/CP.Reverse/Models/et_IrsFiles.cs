using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_IrsFiles
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> IrsInstructionDate { get; set; }
        public Nullable<System.DateTime> IrsReleaseDate { get; set; }
        public Nullable<int> IrsRecordsNumber { get; set; }
    }
}
