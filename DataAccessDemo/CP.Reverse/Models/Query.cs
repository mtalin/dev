using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Query
    {
        public int ID { get; set; }
        public Nullable<int> QueryCategory { get; set; }
        public string Description { get; set; }
        public string Query1 { get; set; }
        public string DbName { get; set; }
        public string QueryParams { get; set; }
        public virtual QueryCategory QueryCategory1 { get; set; }
    }
}
