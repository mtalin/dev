using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vCsPortalTask
    {
        public int ID { get; set; }
        public Nullable<int> EventClass { get; set; }
        public Nullable<int> TaskCategory { get; set; }
        public string Subject { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public Nullable<int> DueDays { get; set; }
        public Nullable<int> ResponsibleDepartment { get; set; }
        public string ResponsibleDepartmentName { get; set; }
        public Nullable<int> assignedPerson { get; set; }
        public string AssignedPersonFirstName { get; set; }
        public string AssignedPersonLastName { get; set; }
        public bool NoteExists { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> Recipient { get; set; }
        public byte Priority { get; set; }
    }
}
