using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ActivityPricing
    {
        public int ID { get; set; }
        public int PricingScheduleInstance { get; set; }
        public Nullable<decimal> UptoAmount { get; set; }
        public decimal BaseAmount { get; set; }
        public float FeeValue { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual PricingScheduleInstance PricingScheduleInstance1 { get; set; }
    }
}
