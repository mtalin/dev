using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CompanyPositionsRight
    {
        public int ID { get; set; }
        public int Person { get; set; }
        public int StructuredCompany { get; set; }
        public int Foundation { get; set; }
        public string Permissions { get; set; }
        public string AppliedPermissions { get; set; }
        public string InheritedPermissions { get; set; }
    }
}
