using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class StaticPermission
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string DefaultValue { get; set; }
    }
}
