using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_PostalAddresses
    {
        public int ID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Country { get; set; }
        public string Company { get; set; }
        public string StateName { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string FullAddress { get; set; }
    }
}
