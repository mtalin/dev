using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ContractPlayer
    {
        public int ID { get; set; }
        public int Contract { get; set; }
        public int CompanyElement { get; set; }
        public int RoleInContract { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<int> ObjectPermissions { get; set; }
        public virtual CompanyElement CompanyElement1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Contract Contract1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ObjectsPermission ObjectsPermission { get; set; }
    }
}
