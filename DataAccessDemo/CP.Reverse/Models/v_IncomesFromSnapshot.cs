using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class v_IncomesFromSnapshot
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Year { get; set; }
        public int Quarter { get; set; }
        public Nullable<decimal> IntIncome { get; set; }
        public Nullable<decimal> DivIncome { get; set; }
        public Nullable<decimal> CapitalGains { get; set; }
        public Nullable<decimal> AssetManagementFees { get; set; }
        public Nullable<decimal> MiscCredits { get; set; }
        public Nullable<decimal> Line11Income { get; set; }
        public Nullable<decimal> CrossInvInc { get; set; }
        public Nullable<decimal> NetInvIncome { get; set; }
        public Nullable<decimal> PartnershipIncome { get; set; }
        public Nullable<decimal> TaxRate { get; set; }
        public System.DateTime dtStartPeriod { get; set; }
        public System.DateTime dtEndPeriod { get; set; }
    }
}
