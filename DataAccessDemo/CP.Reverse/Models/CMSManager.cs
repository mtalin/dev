using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CMSManager
    {
        public int ID { get; set; }
        public Nullable<int> NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public int Role { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessFax { get; set; }
        public string Description { get; set; }
        public byte[] Picture { get; set; }
        public string PictureType { get; set; }
        public int SortOrder { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public virtual Person Person { get; set; }
    }
}
