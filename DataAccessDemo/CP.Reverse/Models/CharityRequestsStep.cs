using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CharityRequestsStep
    {
        public int ID { get; set; }
        public int CharityRequest { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public int Step { get; set; }
        public int Step2 { get; set; }
        public Nullable<int> Message { get; set; }
        public Nullable<int> Note { get; set; }
        public Nullable<int> Event { get; set; }
        public Nullable<int> Pending { get; set; }
        public Nullable<int> PendingPrev { get; set; }
        public virtual CharityRequest CharityRequest1 { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Note Note1 { get; set; }
    }
}
