using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class HouseholdAccountDetail
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public int Step { get; set; }
        public string Address { get; set; }
        public string ApartmentNo { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public int Flags { get; set; }
        public decimal Saving { get; set; }
        public decimal IRA { get; set; }
        public decimal ValueOfHouse { get; set; }
        public decimal OtherAssets { get; set; }
        public decimal CCDebt { get; set; }
        public decimal CarLoans { get; set; }
        public decimal Mortgage { get; set; }
        public decimal OtherLoans { get; set; }
        public Nullable<decimal> InitialAmount { get; set; }
        public Nullable<System.DateTime> DtAuthorized { get; set; }
        public Nullable<int> AuthorizedBy { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtApproved { get; set; }
        public Nullable<System.DateTime> DtExpired { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string TypeName { get; set; }
        public string ShortTypeName { get; set; }
        public string ShortTypeName2 { get; set; }
        public Nullable<int> PrimaryApplicant { get; set; }
        public string PrimaryApplicantFirstName { get; set; }
        public string PrimaryApplicantLastName { get; set; }
        public Nullable<int> Status { get; set; }
        public string StatusName { get; set; }
        public string AuthorizedByName { get; set; }
        public string AddressName { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public bool AllowMailingCheckDirectly { get; set; }
        public Nullable<System.DateTime> dtDeleted { get; set; }
    }
}
