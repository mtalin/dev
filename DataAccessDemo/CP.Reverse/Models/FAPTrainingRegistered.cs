using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FAPTrainingRegistered
    {
        public int ID { get; set; }
        public int Event { get; set; }
        public int Person { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public virtual FAPTraining FAPTraining { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
