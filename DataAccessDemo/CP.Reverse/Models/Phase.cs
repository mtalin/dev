using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Phase
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
    }
}
