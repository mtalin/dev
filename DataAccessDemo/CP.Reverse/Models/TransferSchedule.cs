using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TransferSchedule
    {
        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string Days { get; set; }
        public Nullable<System.DateTime> DtStart { get; set; }
        public Nullable<System.DateTime> DtEnd { get; set; }
        public Nullable<int> TransferProfile { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
