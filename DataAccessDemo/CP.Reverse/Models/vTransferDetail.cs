using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vTransferDetail
    {
        public int Foundation { get; set; }
        public string FaxAttnCompany { get; set; }
        public string FaxAttnName { get; set; }
        public string FaxAttnPhone { get; set; }
        public string FaxAttnFax { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
    }
}
