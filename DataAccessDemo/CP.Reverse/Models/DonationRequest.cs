using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DonationRequest
    {
        public DonationRequest()
        {
            this.GrantDetails = new List<GrantDetail>();
        }

        public int ID { get; set; }
        public Nullable<int> CharityEIN { get; set; }
        public int Foundation { get; set; }
        public decimal Amount { get; set; }
        public int GrantRequestId { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Step { get; set; }
        public Nullable<int> SpecPurposeType { get; set; }
        public string SpecPurposeText1 { get; set; }
        public string SpecPurposeText2 { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactMiddleName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactTitle { get; set; }
        public string ContactSalutation { get; set; }
        public Nullable<int> HouseholdAccount { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual HouseholdAccount HouseholdAccount1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<GrantDetail> GrantDetails { get; set; }
    }
}
