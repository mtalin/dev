using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PersonNotification
    {
        public int ID { get; set; }
        public Nullable<int> Person { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> Notification { get; set; }
    }
}
