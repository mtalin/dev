using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ContractPricing
    {
        public ContractPricing()
        {
            this.PricingScheduleInstances = new List<PricingScheduleInstance>();
        }

        public int ID { get; set; }
        public int Contract { get; set; }
        public int PricingSchedule { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual Person Person { get; set; }
        public virtual Contract Contract1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual PricingSchedule PricingSchedule1 { get; set; }
        public virtual ICollection<PricingScheduleInstance> PricingScheduleInstances { get; set; }
    }
}
