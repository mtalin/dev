using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TrustedAction
    {
        public TrustedAction()
        {
            this.Crossings = new List<Crossing>();
        }

        public int ID { get; set; }
        public int Admin { get; set; }
        public int Person { get; set; }
        public int Foundation { get; set; }
        public System.DateTime DtInstructed { get; set; }
        public string InstructedBy { get; set; }
        public string Instruction { get; set; }
        public Nullable<System.DateTime> DtEntered { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> ActivityLog { get; set; }
        public Nullable<int> Document { get; set; }
        public Nullable<int> DeclaredAction { get; set; }
        public virtual ActivityLog ActivityLog1 { get; set; }
        public virtual ICollection<Crossing> Crossings { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Person Person2 { get; set; }
    }
}
