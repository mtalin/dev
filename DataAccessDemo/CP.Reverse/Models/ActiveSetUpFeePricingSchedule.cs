using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ActiveSetUpFeePricingSchedule
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> BillTo { get; set; }
        public int Contract { get; set; }
        public int FeeType { get; set; }
    }
}
