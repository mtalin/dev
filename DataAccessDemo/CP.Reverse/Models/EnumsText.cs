using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EnumsText
    {
        public int ID { get; set; }
        public string LargeValue { get; set; }
        public virtual Enum Enum { get; set; }
    }
}
