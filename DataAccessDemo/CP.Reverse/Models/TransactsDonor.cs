using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TransactsDonor
    {
        public int ID { get; set; }
        public int Transact { get; set; }
        public Nullable<decimal> ChildAmount { get; set; }
        public Nullable<int> Donor { get; set; }
        public Nullable<int> PhantomDonor { get; set; }
        public Nullable<int> Spouse { get; set; }
        public string InCareOf { get; set; }
        public Nullable<bool> IsDonorAlive { get; set; }
        public Nullable<bool> IsSpouseAlive { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<decimal> GoodsServices { get; set; }
        public virtual Donor Donor1 { get; set; }
        public virtual Donor Donor2 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Transact Transact1 { get; set; }
    }
}
