using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class enums_Signers
    {
        public int ID { get; set; }
        public string FullName { get; set; }
    }
}
