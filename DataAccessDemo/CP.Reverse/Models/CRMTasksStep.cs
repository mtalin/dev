using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CRMTasksStep
    {
        public int ID { get; set; }
        public int CRMTask { get; set; }
        public int Step { get; set; }
        public Nullable<int> Note { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual CRMTask CRMTask1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual User User { get; set; }
    }
}
