using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ExcludedEmailApplicant
    {
        public int ID { get; set; }
        public int EmailTemplate { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public Nullable<int> FinancialAdvisorGroup { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public virtual EmailTemplate EmailTemplate1 { get; set; }
        public virtual FinancialAdvisorGroup FinancialAdvisorGroup1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
