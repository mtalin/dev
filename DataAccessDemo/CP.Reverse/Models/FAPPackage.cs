using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FAPPackage
    {
        public FAPPackage()
        {
            this.FAPPackageRequests = new List<FAPPackageRequest>();
            this.FAPPackages1 = new List<FAPPackage>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> Document { get; set; }
        public int PackageForm { get; set; }
        public Nullable<int> Parent { get; set; }
        public virtual Document1 Document1 { get; set; }
        public virtual FAPPackageForm FAPPackageForm { get; set; }
        public virtual ICollection<FAPPackageRequest> FAPPackageRequests { get; set; }
        public virtual ICollection<FAPPackage> FAPPackages1 { get; set; }
        public virtual FAPPackage FAPPackage1 { get; set; }
    }
}
