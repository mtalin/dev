using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class IRSFile
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public Nullable<System.DateTime> IRSInstructionDate { get; set; }
        public Nullable<System.DateTime> IRSReleaseDate { get; set; }
        public Nullable<int> IRSRecordsNumber { get; set; }
        public int State { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> DtLastChecked { get; set; }
        public Nullable<System.DateTime> DtDownloadStart { get; set; }
        public Nullable<int> DownloadAttempts { get; set; }
        public Nullable<int> TotalDownloadSize { get; set; }
        public Nullable<int> CurrentDownloadSize { get; set; }
        public string DownloadFolder { get; set; }
        public Nullable<System.DateTime> DtDownloadFinish { get; set; }
        public Nullable<System.DateTime> DtParseStart { get; set; }
        public Nullable<int> GoodParsedRecordsNumber { get; set; }
        public Nullable<int> BadParsedRecordsNumber { get; set; }
        public Nullable<int> ErrorFormatRecordsNumber { get; set; }
        public Nullable<int> FilteredRecordsNumber { get; set; }
        public Nullable<int> ParsedRecordsInPerSecond { get; set; }
        public Nullable<int> CopiedCURecordsNumber { get; set; }
        public Nullable<int> CopiedBCRecordsNumber { get; set; }
        public Nullable<System.DateTime> DtParseFinish { get; set; }
        public Nullable<System.DateTime> DtImportStart { get; set; }
        public Nullable<System.DateTime> DtImportFinish { get; set; }
        public byte ImportStep { get; set; }
        public int ImportTotal { get; set; }
        public Nullable<System.DateTime> ImportP1Start { get; set; }
        public Nullable<System.DateTime> ImportP2Start { get; set; }
        public Nullable<System.DateTime> ImportP3Start { get; set; }
        public Nullable<System.DateTime> ImportP4Start { get; set; }
        public Nullable<System.DateTime> ImportP5Start { get; set; }
        public Nullable<System.DateTime> ImportP6Start { get; set; }
        public Nullable<System.DateTime> ImportP7Start { get; set; }
        public Nullable<System.DateTime> ImportP8Start { get; set; }
        public Nullable<System.DateTime> ImportP9Start { get; set; }
        public int ImportFixedAddresses { get; set; }
        public int ImportMatchedAuto { get; set; }
        public int ImportMatchedManual { get; set; }
        public int ImportRemovedAuto { get; set; }
        public int ImportRemovedTotal { get; set; }
        public int ImportCopiedTotal { get; set; }
        public int ImportNewCharities { get; set; }
        public int ImportChangesTotal { get; set; }
        public int ImportChangesProcessed { get; set; }
        public int ImportChangesProcessedPerSecond { get; set; }
        public int ImportHoldByFoundationCode { get; set; }
        public int ImportHoldByGroupExempt { get; set; }
        public int Stat1VerifiedPublicCharities { get; set; }
        public int Stat1MissingData { get; set; }
        public int Stat1NonPublicCharities { get; set; }
        public int Stat1SupportingOrgsCode17 { get; set; }
        public int Stat1SupportingOrgsTypeIII { get; set; }
        public int Stat1GroupExemptOrgs { get; set; }
        public int Stat2VerifiedPublicCharities { get; set; }
        public int Stat2MissingData { get; set; }
        public int Stat2NonPublicCharities { get; set; }
        public int Stat2SupportingOrgsCode17 { get; set; }
        public int Stat2SupportingOrgsTypeIII { get; set; }
        public int Stat2GroupExemptOrgs { get; set; }
    }
}
