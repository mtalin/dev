using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FinancialStatementsPart
    {
        public int BookId { get; set; }
        public int PartId { get; set; }
        public Nullable<int> LineId { get; set; }
        public string Name { get; set; }
    }
}
