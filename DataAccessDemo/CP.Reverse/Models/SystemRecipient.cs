using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class SystemRecipient
    {
        public int ID { get; set; }
        public string Address { get; set; }
        public string Flags { get; set; }
        public string TagName { get; set; }
        public virtual Recipient Recipient { get; set; }
    }
}
