using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AssetBook
    {
        public AssetBook()
        {
            this.AssetValuations = new List<AssetValuation>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public string Description { get; set; }
        public bool UBTI { get; set; }
        public Nullable<int> UBTIDocument { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtExpired { get; set; }
        public int Author { get; set; }
        public Nullable<int> UBTINote { get; set; }
        public virtual Document1 Document { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual LocalAsset LocalAsset { get; set; }
        public virtual Note Note { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<AssetValuation> AssetValuations { get; set; }
    }
}
