using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TrRegistryRulesCompiled
    {
        public int ID { get; set; }
        public string Compiled { get; set; }
    }
}
