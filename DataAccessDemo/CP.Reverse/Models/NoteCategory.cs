using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class NoteCategory
    {
        public NoteCategory()
        {
            this.Notes = new List<Note>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Flags { get; set; }
        public Nullable<int> Section { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
    }
}
