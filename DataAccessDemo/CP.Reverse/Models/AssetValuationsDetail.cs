using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AssetValuationsDetail
    {
        public int ID { get; set; }
        public int AssetBook { get; set; }
        public int Operation { get; set; }
        public System.DateTime DtOperation { get; set; }
        public decimal Amount { get; set; }
        public Nullable<int> Document { get; set; }
        public string AppCompany { get; set; }
        public string AppContact { get; set; }
        public string AppPhone { get; set; }
        public Nullable<int> Transact { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public Nullable<System.DateTime> DtAppraisal { get; set; }
        public Nullable<int> Note { get; set; }
        public int Foundation { get; set; }
        public int AssetType { get; set; }
        public string AssetTypeName { get; set; }
        public string AssetTypeFlags { get; set; }
        public System.DateTime dtTransact { get; set; }
        public Nullable<System.DateTime> NextDtTransact { get; set; }
    }
}
