using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_StaffingProposals
    {
        public int ID { get; set; }
        public string FoundationName { get; set; }
        public string ProposalDate { get; set; }
        public string ProposalTime { get; set; }
        public string ProposalCloseDate { get; set; }
        public string DeadlineDate { get; set; }
        public int IsApproved { get; set; }
        public int IsRejected { get; set; }
        public int IsSuspended { get; set; }
        public string Purpose { get; set; }
        public int Author { get; set; }
    }
}
