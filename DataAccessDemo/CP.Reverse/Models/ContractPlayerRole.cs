using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ContractPlayerRole
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> ObjectPermissions { get; set; }
        public virtual ObjectsPermission ObjectsPermission { get; set; }
    }
}
