using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class qd_FAddresses
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Questionnaire { get; set; }
        public Nullable<int> type { get; set; }
        public Nullable<bool> useFsAddress { get; set; }
        public Nullable<bool> isNew { get; set; }
        public Nullable<bool> updated { get; set; }
        public string Name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string careof { get; set; }
    }
}
