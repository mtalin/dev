using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FinancialPartnerChunk
    {
        public int ID { get; set; }
        public int FinancialPartner { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<decimal> CP_SetupFee { get; set; }
        public Nullable<decimal> MinYearFee { get; set; }
        public Nullable<decimal> TerminationFee { get; set; }
        public Nullable<decimal> CheckFee { get; set; }
        public Nullable<decimal> IncorporationFee { get; set; }
        public Nullable<decimal> IRSFee { get; set; }
        public Nullable<decimal> MinGrantSize { get; set; }
        public bool FC_Allowed { get; set; }
        public string FC_Address1 { get; set; }
        public string FC_Address2 { get; set; }
        public string FC_Address3 { get; set; }
        public string FC_City { get; set; }
        public string FC_State { get; set; }
        public string FC_ZIP { get; set; }
        public string FC_To { get; set; }
        public bool FW_Allowed { get; set; }
        public string FW_Bank { get; set; }
        public string FW_ABA { get; set; }
        public string FW_BankAddress { get; set; }
        public string FW_AccName { get; set; }
        public string FW_AccNumber { get; set; }
        public string FW_Instructions { get; set; }
        public bool FS_Allowed { get; set; }
        public string FS_Instructions { get; set; }
        public string FS_DTC { get; set; }
        public bool FO_Allowed { get; set; }
        public string FO_Name { get; set; }
        public string FO_Instructions { get; set; }
        public Nullable<int> CWTR_ContactType { get; set; }
        public Nullable<int> CWTR_ContactVia { get; set; }
        public string CWTR_FirstName { get; set; }
        public string CWTR_MiddleName { get; set; }
        public string CWTR_LastName { get; set; }
        public string CWTR_Phone { get; set; }
        public string CWTR_Email { get; set; }
        public Nullable<int> TrProcMethod { get; set; }
        public int TrCheckSupport { get; set; }
        public bool BalanceFile { get; set; }
        public string FaxAttnName { get; set; }
        public string FaxAttnCompany { get; set; }
        public string FaxAttnPhone { get; set; }
        public string FaxAttnFax { get; set; }
        public string CSG_Name { get; set; }
        public string CSG_Phone { get; set; }
        public string CSG_Fax { get; set; }
        public bool CombineRiderBFees { get; set; }
        public Nullable<decimal> FlatOver { get; set; }
        public Nullable<decimal> FlatFeePerQuarter { get; set; }
        public Nullable<double> FlatFSRate { get; set; }
        public bool ASA_Addendum { get; set; }
        public bool SetupFeeToThirdParty { get; set; }
        public Nullable<int> SetupFeeBillingAddress { get; set; }
        public bool OngoingFeeToThirdParty { get; set; }
        public Nullable<int> OngoingFeeBillingAddress { get; set; }
        public bool Ovr_ASA { get; set; }
        public bool Ovr_Rider { get; set; }
        public bool Ovr_DAccounts { get; set; }
        public bool Ovr_BalanceFile { get; set; }
        public bool Ovr_Fax { get; set; }
        public bool Ovr_GeneralFees { get; set; }
        public bool Ovr_OngoingFees { get; set; }
        public bool Ovr_Billing { get; set; }
        public Nullable<decimal> SetupFeeToThirdPartyAmount { get; set; }
        public Nullable<decimal> DiscountFlatFeePerQuarter { get; set; }
        public Nullable<decimal> DiscountFlatFeePerQuarterUpTo { get; set; }
        public string CustomContactsHTML { get; set; }
        public bool Ovr_CustomContacts { get; set; }
        public Nullable<decimal> PT_SetupFee { get; set; }
        public Nullable<int> StructuredCompany { get; set; }
        public string tmp_Action { get; set; }
        public virtual Company Company { get; set; }
        public virtual Company Company1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
