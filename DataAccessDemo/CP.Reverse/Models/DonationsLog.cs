using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DonationsLog
    {
        public int ID { get; set; }
        public int Donate { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtChecked { get; set; }
        public int CharityEIN { get; set; }
        public int Foundation { get; set; }
        public int Person { get; set; }
        public decimal Amount { get; set; }
        public string GrantPurpose { get; set; }
        public string PurposeDescription { get; set; }
    }
}
