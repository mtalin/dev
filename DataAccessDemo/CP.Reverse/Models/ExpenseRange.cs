using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ExpenseRange
    {
        public int ID { get; set; }
        public int Year { get; set; }
        public decimal MealsHi { get; set; }
        public decimal MealsLo { get; set; }
        public decimal LodgingHi { get; set; }
        public decimal LodgingLo { get; set; }
    }
}
