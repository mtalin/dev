using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class SubCategory
    {
        public int Activiticode { get; set; }
        public string Name { get; set; }
        public string C1st { get; set; }
        public string C2nd { get; set; }
        public string C3rd { get; set; }
        public string C4th { get; set; }
    }
}
