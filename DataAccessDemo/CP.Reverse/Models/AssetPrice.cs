using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AssetPrice
    {
        public int ID { get; set; }
        public int Asset { get; set; }
        public System.DateTime DtPrice { get; set; }
        public decimal LowPrice { get; set; }
        public decimal HiPrice { get; set; }
        public decimal ClsPrice { get; set; }
        public int PriceAuthor { get; set; }
        public virtual Asset Asset1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
