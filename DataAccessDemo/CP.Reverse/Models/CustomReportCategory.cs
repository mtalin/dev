using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CustomReportCategory
    {
        public CustomReportCategory()
        {
            this.CustomReports = new List<CustomReport>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int Handler { get; set; }
        public virtual ICollection<CustomReport> CustomReports { get; set; }
    }
}
