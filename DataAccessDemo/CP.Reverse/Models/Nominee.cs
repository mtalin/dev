using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Nominee
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Person { get; set; }
        public Nullable<int> Appointment { get; set; }
        public Nullable<decimal> DonationsLimit { get; set; }
        public int RegistrationStep { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public Nullable<int> NewPosition { get; set; }
        public Nullable<int> StaffingProposal { get; set; }
        public Nullable<System.DateTime> DtAccDeadline { get; set; }
        public Nullable<int> DeclaredAppointment { get; set; }
        public Nullable<System.DateTime> DtAccepted { get; set; }
        public int Reelection { get; set; }
        public Nullable<decimal> ApprovableLimit { get; set; }
        public bool GrantCommitteeMember { get; set; }
        public Nullable<System.DateTime> DtTermsExpired { get; set; }
        public bool ApproveGrants { get; set; }
    }
}
