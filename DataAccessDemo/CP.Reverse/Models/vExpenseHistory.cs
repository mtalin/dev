using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vExpenseHistory
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public int PayeeType { get; set; }
        public Nullable<int> Payee { get; set; }
        public string PayeeName { get; set; }
        public string SortPayeeName { get; set; }
        public Nullable<int> Category { get; set; }
        public string CategoryName { get; set; }
        public string SortCategoryName { get; set; }
        public Nullable<int> Type { get; set; }
        public string TypeName { get; set; }
        public string SortTypeName { get; set; }
        public Nullable<int> Program { get; set; }
        public string ProgramName { get; set; }
        public string SortProgramName { get; set; }
        public Nullable<int> Project { get; set; }
        public string ProjectName { get; set; }
        public string SortProjectName { get; set; }
        public int TransactID { get; set; }
        public string Description { get; set; }
    }
}
