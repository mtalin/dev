using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ActivityCode
    {
        public short Activitycode1 { get; set; }
        public string Name { get; set; }
        public string Category1 { get; set; }
        public string Category2 { get; set; }
        public string Category3 { get; set; }
        public string Category4 { get; set; }
    }
}
