using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class enum_SystemAddresses
    {
        public int ItemID { get; set; }
        public string GroupName { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string CareOf { get; set; }
        public string Company { get; set; }
    }
}
