using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class StructuredCompany
    {
        public StructuredCompany()
        {
            this.CompanyPersons = new List<CompanyPerson>();
            this.CompanyPositions = new List<CompanyPosition>();
            this.Departments = new List<Department>();
            this.Foundations = new List<Foundation>();
            this.Invoices = new List<Invoice>();
            this.ServiceProviders = new List<ServiceProvider>();
            this.StaffingPolicies = new List<StaffingPolicy>();
            this.Taxes = new List<Tax>();
        }

        public int ID { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Nullable<System.DateTime> dtActivated { get; set; }
        public virtual CompanyElement CompanyElement { get; set; }
        public virtual ICollection<CompanyPerson> CompanyPersons { get; set; }
        public virtual ICollection<CompanyPosition> CompanyPositions { get; set; }
        public virtual ICollection<Department> Departments { get; set; }
        public virtual ICollection<Foundation> Foundations { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual PostalAddress PostalAddress1 { get; set; }
        public virtual ICollection<ServiceProvider> ServiceProviders { get; set; }
        public virtual ICollection<StaffingPolicy> StaffingPolicies { get; set; }
        public virtual ICollection<Tax> Taxes { get; set; }
    }
}
