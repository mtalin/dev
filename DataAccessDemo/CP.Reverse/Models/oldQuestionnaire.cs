using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class oldQuestionnaire
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public string Version { get; set; }
        public int FiscalYear { get; set; }
        public string FormData { get; set; }
        public int Step { get; set; }
        public bool HasIssues { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtSubmitted { get; set; }
        public Nullable<System.DateTime> DtCompleted { get; set; }
        public int Author { get; set; }
    }
}
