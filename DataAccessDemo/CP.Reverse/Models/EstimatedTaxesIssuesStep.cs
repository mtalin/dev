using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EstimatedTaxesIssuesStep
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int EstimatedTaxIssue { get; set; }
        public int Step { get; set; }
        public Nullable<int> PrevStep { get; set; }
        public virtual EstimatedTaxesIssue EstimatedTaxesIssue { get; set; }
        public virtual Person Person { get; set; }
    }
}
