using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class SatelliteDocument
    {
        public int ID { get; set; }
        public int ParentDocument { get; set; }
        public int ChildDocument { get; set; }
        public virtual Document1 Document { get; set; }
        public virtual Document1 Document1 { get; set; }
    }
}
