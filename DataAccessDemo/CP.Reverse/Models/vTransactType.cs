using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vTransactType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Abbrev { get; set; }
        public string Flags { get; set; }
        public Nullable<int> SortOrder { get; set; }
    }
}
