using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CRMTask
    {
        public CRMTask()
        {
            this.CRMTasksSteps = new List<CRMTasksStep>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public Nullable<int> Foundation { get; set; }
        public int Event { get; set; }
        public Nullable<int> Message { get; set; }
        public Nullable<int> Recipient { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int State { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public Nullable<int> PersonClosed { get; set; }
        public int EmailTemplate { get; set; }
        public Nullable<int> assignedPerson { get; set; }
        public Nullable<int> EventClass { get; set; }
        public bool NoteExists { get; set; }
        public byte Priority { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual EmailTemplate EmailTemplate1 { get; set; }
        public virtual EventClass EventClass1 { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person2 { get; set; }
        public virtual Person Person3 { get; set; }
        public virtual ICollection<CRMTasksStep> CRMTasksSteps { get; set; }
    }
}
