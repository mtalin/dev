using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class HouseholdMember
    {
        public int ID { get; set; }
        public int HouseholdAccount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public Nullable<int> Age { get; set; }
        public Nullable<decimal> Income { get; set; }
        public bool Primary { get; set; }
        public bool GmxRef { get; set; }
        public virtual HouseholdAccount HouseholdAccount1 { get; set; }
    }
}
