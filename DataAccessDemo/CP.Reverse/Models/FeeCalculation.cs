using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FeeCalculation
    {
        public FeeCalculation()
        {
            this.Taxes = new List<Tax>();
        }

        public int ID { get; set; }
        public int FeeType { get; set; }
        public decimal Amount { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> Contract { get; set; }
        public Nullable<int> BillTo { get; set; }
        public string Snapshot { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Step { get; set; }
        public string Note { get; set; }
        public virtual CompanyElement CompanyElement { get; set; }
        public virtual Contract Contract1 { get; set; }
        public virtual User User { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<Tax> Taxes { get; set; }
    }
}
