using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class RegistryRules990Prepared
    {
        public byte Part { get; set; }
        public short LineId { get; set; }
        public string ColumnId { get; set; }
        public short RegistryRule { get; set; }
        public short TransactType { get; set; }
        public byte AssetType { get; set; }
    }
}
