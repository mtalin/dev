using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DocumentTemplate
    {
        public DocumentTemplate()
        {
            this.CustomReportsTemplates = new List<CustomReportsTemplate>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int Format { get; set; }
        public string Text { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string Cache { get; set; }
        public virtual ICollection<CustomReportsTemplate> CustomReportsTemplates { get; set; }
        public virtual Person Person { get; set; }
    }
}
