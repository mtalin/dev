using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class YahooQuery
    {
        public int ID { get; set; }
        public int Asset { get; set; }
        public System.DateTime DtMin { get; set; }
        public System.DateTime DtMax { get; set; }
        public System.DateTime DtLastQuery { get; set; }
        public Nullable<System.DateTime> DtLastSuccess { get; set; }
    }
}
