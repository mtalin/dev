using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class RegistryRulesStatementsPrepared
    {
        public int BookId { get; set; }
        public byte PartId { get; set; }
        public short LineId { get; set; }
        public short TransactType { get; set; }
        public byte AssetType { get; set; }
        public short RegistryRule { get; set; }
    }
}
