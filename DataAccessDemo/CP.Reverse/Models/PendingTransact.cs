using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PendingTransact
    {
        public PendingTransact()
        {
            this.Expenses = new List<Expens>();
            this.Grants = new List<Grant>();
            this.old_PendingTransactItems = new List<old_PendingTransactItems>();
            this.PendingTransactsSteps = new List<PendingTransactsStep>();
            this.Taxes = new List<Tax>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> Transact { get; set; }
        public decimal Amount { get; set; }
        public int Type { get; set; }
        public string Reason { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public int Step { get; set; }
        public Nullable<System.DateTime> DtFaxGenerated { get; set; }
        public Nullable<int> DtFaxAuthor { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
        public Nullable<int> PeriodType { get; set; }
        public Nullable<int> FoundationAccount { get; set; }
        public Nullable<int> TransferGroup { get; set; }
        public Nullable<int> Payment { get; set; }
        public Nullable<int> LastQTransaction { get; set; }
        public Nullable<int> ACHTransact { get; set; }
        public Nullable<bool> Outside { get; set; }
        public virtual ACHTransact ACHTransact1 { get; set; }
        public virtual ICollection<Expens> Expenses { get; set; }
        public virtual FoundationAccount FoundationAccount1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<Grant> Grants { get; set; }
        public virtual ICollection<old_PendingTransactItems> old_PendingTransactItems { get; set; }
        public virtual Payment Payment1 { get; set; }
        public virtual Transact Transact1 { get; set; }
        public virtual TransferGroup TransferGroup1 { get; set; }
        public virtual ICollection<PendingTransactsStep> PendingTransactsSteps { get; set; }
        public virtual ICollection<Tax> Taxes { get; set; }
    }
}
