using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TrHoldingRule
    {
        public TrHoldingRule()
        {
            this.TrHoldingMaps = new List<TrHoldingMap>();
        }

        public string ID { get; set; }
        public byte SortOrder { get; set; }
        public string RuleXX { get; set; }
        public string RuleUSD { get; set; }
        public string RuleMF { get; set; }
        public string RuleMU { get; set; }
        public string RuleFI { get; set; }
        public string RuleAA { get; set; }
        public string RuleCR { get; set; }
        public virtual ICollection<TrHoldingMap> TrHoldingMaps { get; set; }
    }
}
