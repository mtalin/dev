using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DocumentView
    {
        public int DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string DocumentFileName { get; set; }
        public string COMClassName { get; set; }
        public int IsActive { get; set; }
    }
}
