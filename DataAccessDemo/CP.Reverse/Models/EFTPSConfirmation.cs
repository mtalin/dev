using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EFTPSConfirmation
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> dtCreated { get; set; }
        public int EFTPSConfReport { get; set; }
        public Nullable<int> EFTPSPayment { get; set; }
        public string EIN { get; set; }
        public string taxPeriod { get; set; }
        public Nullable<decimal> amount { get; set; }
        public string errors { get; set; }
        public Nullable<int> TaxType { get; set; }
        public string EFTNumber { get; set; }
        public Nullable<System.DateTime> SettlementDate { get; set; }
        public string CancelEFT { get; set; }
        public Nullable<int> Line { get; set; }
        public virtual EFTPSConfReport EFTPSConfReport1 { get; set; }
        public virtual EFTPSPayment EFTPSPayment1 { get; set; }
    }
}
