using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_DonationProposals
    {
        public int ID { get; set; }
        public string FoundationName { get; set; }
        public string ProposalDate { get; set; }
        public string ProposalTime { get; set; }
        public string Reason { get; set; }
        public string ProposalCloseDate { get; set; }
        public string VotingCommunity { get; set; }
        public int IsApproved { get; set; }
        public int IsRejected { get; set; }
        public int IsExpired { get; set; }
        public string RejectionLanguage { get; set; }
        public int Donation { get; set; }
    }
}
