using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vGrant
    {
        public int ID { get; set; }
        public Nullable<int> Grant { get; set; }
        public Nullable<int> CharityRequest { get; set; }
        public Nullable<int> GrantProposal { get; set; }
        public Nullable<int> Proposal { get; set; }
        public int Type { get; set; }
        public Nullable<int> GrantDetails { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Step { get; set; }
        public int State { get; set; }
        public Nullable<int> PendingTransact { get; set; }
        public Nullable<int> Transact { get; set; }
        public decimal Amount { get; set; }
        public bool Anonymous { get; set; }
        public Nullable<int> Certificate { get; set; }
        public string ChAddress { get; set; }
        public string ChAddress2 { get; set; }
        public Nullable<int> CharityEIN { get; set; }
        public string ChCity { get; set; }
        public string ChInCareOf { get; set; }
        public string ChOrgName { get; set; }
        public string ChState { get; set; }
        public string ChZIP { get; set; }
        public string ChCountry { get; set; }
        public string ConfirmDesc { get; set; }
        public bool ConfirmGuideStar { get; set; }
        public Nullable<bool> ConfirmIRSbulletins { get; set; }
        public Nullable<bool> ConfirmIRSwebsite { get; set; }
        public Nullable<int> DonationRequest { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public Nullable<int> GrantLetter { get; set; }
        public string GrantLetterData { get; set; }
        public Nullable<int> HouseholdAccount { get; set; }
        public bool IsSpecialDelivery { get; set; }
        public string NoteForEmail { get; set; }
        public bool PermanentChanges { get; set; }
        public int Priority { get; set; }
        public string Purpose { get; set; }
        public string PurposeDescription { get; set; }
        public int RecipientType { get; set; }
        public bool ReviewedByAdmin { get; set; }
        public string SpecialUserNote { get; set; }
        public string SpecPurposeText1 { get; set; }
        public string SpecPurposeText2 { get; set; }
        public Nullable<int> SpecPurposeType { get; set; }
        public string TargetedDescription { get; set; }
        public Nullable<byte> GrantType { get; set; }
        public Nullable<int> NumPayments { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<int> PaymentNumber { get; set; }
        public string ProcessInstruction { get; set; }
        public Nullable<int> Template { get; set; }
        public bool GrantLetterApproved { get; set; }
        public string GrantReason { get; set; }
    }
}
