using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TrRegistryMap
    {
        public int ID { get; set; }
        public int TransactType { get; set; }
        public int RegistryRule { get; set; }
        public string Description { get; set; }
    }
}
