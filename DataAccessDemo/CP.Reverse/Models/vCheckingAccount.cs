using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vCheckingAccount
    {
        public int ID { get; set; }
        public Nullable<int> DisbursementAccount { get; set; }
        public Nullable<int> FoundationAccount { get; set; }
        public int Bank { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string RoutingNumber { get; set; }
        public string FullAccountName { get; set; }
        public string PrintedAccountName { get; set; }
        public string BatchFilerID { get; set; }
        public string MasterInquiryPIN { get; set; }
        public int NextCheckNo { get; set; }
        public Nullable<int> LinkedBank { get; set; }
        public string LinkedBankName { get; set; }
        public string LinkedBankAddress { get; set; }
        public string LinkedAccountName { get; set; }
        public string LinkedAccountNumber { get; set; }
        public string LinkedRoutingNumber { get; set; }
        public Nullable<int> AccountType { get; set; }
        public string FullLinkedAccountName { get; set; }
    }
}
