using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_FoundationAccounts
    {
        public int ID { get; set; }
        public string DtCreated { get; set; }
        public string DtReconciled { get; set; }
        public int Foundation { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public int IsPrimary { get; set; }
        public int ServiceProvider { get; set; }
        public string Division { get; set; }
        public string PortfolioCode { get; set; }
        public string DataAccess { get; set; }
        public bool OnlineAccess { get; set; }
        public string OnlineAccessURL { get; set; }
        public string Statements { get; set; }
        public string Frequency { get; set; }
        public string RoutingNumber { get; set; }
        public string BatchFilerID { get; set; }
        public string MasterInquiryPIN { get; set; }
        public Nullable<int> NextCheckNo { get; set; }
    }
}
