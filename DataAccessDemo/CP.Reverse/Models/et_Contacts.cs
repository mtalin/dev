using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_Contacts
    {
        public int ID { get; set; }
        public string DateCreated { get; set; }
        public Nullable<int> Foundation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Organization { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string Comments { get; set; }
        public string FullName { get; set; }
        public string FullStateName { get; set; }
    }
}
