using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TransactNote
    {
        public int ID { get; set; }
        public int Note { get; set; }
        public int Transact { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual Transact Transact1 { get; set; }
    }
}
