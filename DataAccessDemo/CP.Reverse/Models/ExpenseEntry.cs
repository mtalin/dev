using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ExpenseEntry
    {
        public int ID { get; set; }
        public int Expense { get; set; }
        public int Officer { get; set; }
        public int Type { get; set; }
        public Nullable<System.DateTime> DtFrom { get; set; }
        public Nullable<System.DateTime> DtTo { get; set; }
        public string CityFrom { get; set; }
        public string StateFrom { get; set; }
        public string CityTo { get; set; }
        public string StateTo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<bool> IDCConfirm { get; set; }
        public virtual Expens Expens { get; set; }
        public virtual Person Person { get; set; }
    }
}
