using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vChangedTaxReturnsStatus
    {
        public int id { get; set; }
        public int Step2BeSet { get; set; }
    }
}
