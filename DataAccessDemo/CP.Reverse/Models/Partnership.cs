using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Partnership
    {
        public Partnership()
        {
            this.PartnershipAmounts = new List<PartnershipAmount>();
            this.PartnershipsSteps = new List<PartnershipsStep>();
        }

        public int Id { get; set; }
        public int Foundation { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Step { get; set; }
        public int FiscalYear { get; set; }
        public string Note { get; set; }
        public Nullable<int> Document { get; set; }
        public int Asset { get; set; }
        public Nullable<int> QReport { get; set; }
        public Nullable<System.DateTime> DtTransaction { get; set; }
        public virtual Asset Asset1 { get; set; }
        public virtual Document1 Document1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<PartnershipAmount> PartnershipAmounts { get; set; }
        public virtual QReport QReport1 { get; set; }
        public virtual ICollection<PartnershipsStep> PartnershipsSteps { get; set; }
        public virtual PartnersInfo PartnersInfo { get; set; }
    }
}
