using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Event
    {
        public Event()
        {
            this.CharityRequestsSteps = new List<CharityRequestsStep>();
            this.ContactsSteps = new List<ContactsStep>();
            this.CRMTasks = new List<CRMTask>();
            this.DonationCertificates = new List<DonationCertificate>();
            this.GrantMessages = new List<GrantMessage>();
            this.GrantsSteps = new List<GrantsStep>();
            this.HouseholdAccountsSteps = new List<HouseholdAccountsStep>();
            this.Jobs = new List<Job>();
            this.MailAppliesSteps = new List<MailAppliesStep>();
            this.MeetingsSteps = new List<MeetingsStep>();
            this.Messages = new List<Message>();
            this.PaymentsSteps = new List<PaymentsStep>();
            this.PendingTransactsSteps = new List<PendingTransactsStep>();
            this.ProposalsSteps = new List<ProposalsStep>();
            this.RequestDonorsSteps = new List<RequestDonorsStep>();
            this.ShellCorporationsSteps = new List<ShellCorporationsStep>();
            this.TaxesSteps = new List<TaxesStep>();
            this.TaxReturnsSteps = new List<TaxReturnsStep>();
        }

        public int ID { get; set; }
        public int EventClass { get; set; }
        public string HotData { get; set; }
        public string PlainData { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtRegistered { get; set; }
        public Nullable<System.DateTime> DtProcessed { get; set; }
        public int State { get; set; }
        public virtual ICollection<CharityRequestsStep> CharityRequestsSteps { get; set; }
        public virtual ICollection<ContactsStep> ContactsSteps { get; set; }
        public virtual ICollection<CRMTask> CRMTasks { get; set; }
        public virtual ICollection<DonationCertificate> DonationCertificates { get; set; }
        public virtual EventClass EventClass1 { get; set; }
        public virtual ICollection<GrantMessage> GrantMessages { get; set; }
        public virtual ICollection<GrantsStep> GrantsSteps { get; set; }
        public virtual ICollection<HouseholdAccountsStep> HouseholdAccountsSteps { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
        public virtual ICollection<MailAppliesStep> MailAppliesSteps { get; set; }
        public virtual ICollection<MeetingsStep> MeetingsSteps { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<PaymentsStep> PaymentsSteps { get; set; }
        public virtual ICollection<PendingTransactsStep> PendingTransactsSteps { get; set; }
        public virtual ICollection<ProposalsStep> ProposalsSteps { get; set; }
        public virtual ICollection<RequestDonorsStep> RequestDonorsSteps { get; set; }
        public virtual ICollection<ShellCorporationsStep> ShellCorporationsSteps { get; set; }
        public virtual ICollection<TaxesStep> TaxesSteps { get; set; }
        public virtual ICollection<TaxReturnsStep> TaxReturnsSteps { get; set; }
    }
}
