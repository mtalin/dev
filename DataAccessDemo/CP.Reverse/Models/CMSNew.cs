using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CMSNew
    {
        public int ID { get; set; }
        public System.DateTime DtNews { get; set; }
        public string Header { get; set; }
        public string SubHeader { get; set; }
        public string FullText { get; set; }
        public int SortOrder { get; set; }
        public bool Active { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public virtual Person Person { get; set; }
    }
}
