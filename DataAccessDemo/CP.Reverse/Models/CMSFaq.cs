using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CMSFaq
    {
        public int ID { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int SortOrder { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public virtual Person Person { get; set; }
    }
}
