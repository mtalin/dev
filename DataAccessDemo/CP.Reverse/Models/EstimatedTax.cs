using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EstimatedTax
    {
        public EstimatedTax()
        {
            this.EstimatedTaxesIssues = new List<EstimatedTaxesIssue>();
            this.EstimatedTaxesSteps = new List<EstimatedTaxesStep>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Foundation { get; set; }
        public System.DateTime DtStartPeriod { get; set; }
        public System.DateTime DtEndPeriod { get; set; }
        public System.DateTime DtRepaymentTax { get; set; }
        public Nullable<System.DateTime> DtSentToReview { get; set; }
        public Nullable<System.DateTime> DtSentToFinance { get; set; }
        public Nullable<System.DateTime> DtSentToPaid { get; set; }
        public int Year { get; set; }
        public int Quarter { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> NetInvInc { get; set; }
        public Nullable<decimal> AnnualizedTaxLiability { get; set; }
        public Nullable<decimal> AmountTaxProcessed { get; set; }
        public int Step { get; set; }
        public Nullable<int> Tax { get; set; }
        public int Method { get; set; }
        public string XmlData { get; set; }
        public Nullable<System.DateTime> dtLastRecalc { get; set; }
        public Nullable<System.DateTime> dtSentToPendingConfirm { get; set; }
        public Nullable<System.DateTime> dtSentToPendingHold { get; set; }
        public Nullable<System.DateTime> dtSentToPendingReject { get; set; }
        public Nullable<decimal> QtrAmount { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Tax Tax1 { get; set; }
        public virtual ICollection<EstimatedTaxesIssue> EstimatedTaxesIssues { get; set; }
        public virtual ICollection<EstimatedTaxesStep> EstimatedTaxesSteps { get; set; }
    }
}
