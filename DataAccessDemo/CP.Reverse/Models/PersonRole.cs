using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PersonRole
    {
        public PersonRole()
        {
            this.PersonFoundations = new List<PersonFoundation>();
            this.PersonRightsHistories = new List<PersonRightsHistory>();
            this.PersonRightsHistories1 = new List<PersonRightsHistory>();
            this.PersonRolesHistories = new List<PersonRolesHistory>();
        }

        public int ID { get; set; }
        public int RoleCode { get; set; }
        public string Name { get; set; }
        public Nullable<int> Partner { get; set; }
        public string Rights { get; set; }
        public string DisplayingName { get; set; }
        public Nullable<int> Foundation { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<PersonFoundation> PersonFoundations { get; set; }
        public virtual ICollection<PersonRightsHistory> PersonRightsHistories { get; set; }
        public virtual ICollection<PersonRightsHistory> PersonRightsHistories1 { get; set; }
        public virtual ICollection<PersonRolesHistory> PersonRolesHistories { get; set; }
    }
}
