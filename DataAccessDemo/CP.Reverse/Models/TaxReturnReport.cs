using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReturnReport
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public int TaxReturn { get; set; }
        public int CustomReport { get; set; }
        public string ReportSnapshot { get; set; }
        public string ReportFile { get; set; }
        public Nullable<System.DateTime> dtUpdated { get; set; }
        public virtual CustomReport CustomReport1 { get; set; }
        public virtual TaxReturn TaxReturn1 { get; set; }
    }
}
