using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CarryforwardDetail
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int FromYear { get; set; }
        public int ToYear { get; set; }
        public decimal EGC { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
