using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FoundationsCharity
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Foundation { get; set; }
        public int CharityEIN { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
