using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ServiceUseCas
    {
        public int Id { get; set; }
        public int Service { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int SortOrder { get; set; }
        public string Flags { get; set; }
    }
}
