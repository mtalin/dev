using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class tmp_GrantsImport
    {
        public Nullable<int> PublicEIN { get; set; }
        public string CharityName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public Nullable<int> GrantorID { get; set; }
        public System.DateTime DtGrant { get; set; }
        public decimal Amount { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public Nullable<int> SpecPurposeType { get; set; }
        public string SpecPurposeText1 { get; set; }
    }
}
