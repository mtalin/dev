using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class RecentFoundationLookup
    {
        public int FoundationID { get; set; }
        public string FoundationName { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
    }
}
