using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class StaffingPolicy
    {
        public StaffingPolicy()
        {
            this.CompanyPositions = new List<CompanyPosition>();
        }

        public int ID { get; set; }
        public int StructuredCompany { get; set; }
        public int SystemPosition { get; set; }
        public Nullable<int> MinInstances { get; set; }
        public Nullable<int> MaxInstances { get; set; }
        public Nullable<int> PeriodType { get; set; }
        public Nullable<int> ExpirationPeriod { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual CompanyElement CompanyElement { get; set; }
        public virtual ICollection<CompanyPosition> CompanyPositions { get; set; }
        public virtual Person Person { get; set; }
        public virtual StructuredCompany StructuredCompany1 { get; set; }
        public virtual SystemPosition SystemPosition1 { get; set; }
    }
}
