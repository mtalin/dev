using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EFTPSEnrollmentsStep
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public int Author { get; set; }
        public Nullable<int> EFTPSEnrollment { get; set; }
        public Nullable<int> CheckingAccount { get; set; }
        public int Step { get; set; }
        public string EFTPSPin { get; set; }
        public Nullable<int> Note { get; set; }
        public virtual CheckingAccount CheckingAccount1 { get; set; }
        public virtual EFTPSEnrollment EFTPSEnrollment1 { get; set; }
        public virtual Note Note1 { get; set; }
    }
}
