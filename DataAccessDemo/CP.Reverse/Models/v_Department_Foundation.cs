using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class v_Department_Foundation
    {
        public int Foundation { get; set; }
        public Nullable<int> PCA { get; set; }
        public Nullable<int> Department { get; set; }
        public string Name { get; set; }
        public Nullable<int> Customer { get; set; }
    }
}
