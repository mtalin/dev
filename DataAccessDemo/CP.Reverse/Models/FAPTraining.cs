using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FAPTraining
    {
        public FAPTraining()
        {
            this.FAPTrainingRegistereds = new List<FAPTrainingRegistered>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public System.DateTime EventDate { get; set; }
        public int FinancialPartner { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int SortOrder { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<FAPTrainingRegistered> FAPTrainingRegistereds { get; set; }
    }
}
