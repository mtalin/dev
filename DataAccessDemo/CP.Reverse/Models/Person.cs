using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Person
    {
        public Person()
        {
            this.ACHTransacts = new List<ACHTransact>();
            this.ActivityLogs = new List<ActivityLog>();
            this.ActivityPricings = new List<ActivityPricing>();
            this.ActivityPricings1 = new List<ActivityPricing>();
            this.AssetBooks = new List<AssetBook>();
            this.AssetPrices = new List<AssetPrice>();
            this.AssetValuations = new List<AssetValuation>();
            this.CharityRequests = new List<CharityRequest>();
            this.CheckingAccounts = new List<CheckingAccount>();
            this.CheckingAccounts1 = new List<CheckingAccount>();
            this.CheckPayees = new List<CheckPayee>();
            this.Checks = new List<Check>();
            this.Checks1 = new List<Check>();
            this.Checks2 = new List<Check>();
            this.Checks3 = new List<Check>();
            this.CMSBrochures = new List<CMSBrochure>();
            this.CMSContacts = new List<CMSContact>();
            this.CMSEvents = new List<CMSEvent>();
            this.CMSFaqs = new List<CMSFaq>();
            this.CMSManagers = new List<CMSManager>();
            this.CMSNews = new List<CMSNew>();
            this.CMSPublications = new List<CMSPublication>();
            this.CompanyElements = new List<CompanyElement>();
            this.CompanyElements1 = new List<CompanyElement>();
            this.CompanyPersons = new List<CompanyPerson>();
            this.CompanyPersons1 = new List<CompanyPerson>();
            this.CompanyPersons2 = new List<CompanyPerson>();
            this.CompanyPositions = new List<CompanyPosition>();
            this.CompanyPositions1 = new List<CompanyPosition>();
            this.Contacts = new List<Contact>();
            this.ContractPlayers = new List<ContractPlayer>();
            this.ContractPlayers1 = new List<ContractPlayer>();
            this.ContractPricings = new List<ContractPricing>();
            this.ContractPricings1 = new List<ContractPricing>();
            this.Contracts = new List<Contract>();
            this.Contracts1 = new List<Contract>();
            this.ContractServicePackages = new List<ContractServicePackage>();
            this.ContractServicePackages1 = new List<ContractServicePackage>();
            this.ContractServices = new List<ContractService>();
            this.ContractServices1 = new List<ContractService>();
            this.CRMTasks = new List<CRMTask>();
            this.CRMTasks1 = new List<CRMTask>();
            this.CRMTasks2 = new List<CRMTask>();
            this.CRMTasks3 = new List<CRMTask>();
            this.CustomReports = new List<CustomReport>();
            this.DisbursementAccounts = new List<DisbursementAccount>();
            this.DisbursementAccounts1 = new List<DisbursementAccount>();
            this.DisbursementAccountSubscribers = new List<DisbursementAccountSubscriber>();
            this.DisbursementAccountSubscribers1 = new List<DisbursementAccountSubscriber>();
            this.DocumentTemplates = new List<DocumentTemplate>();
            this.DonationCertificates = new List<DonationCertificate>();
            this.DonationCertificates1 = new List<DonationCertificate>();
            this.DonationRequests = new List<DonationRequest>();
            this.Donors = new List<Donor>();
            this.Donors1 = new List<Donor>();
            this.EFTPSConfReports = new List<EFTPSConfReport>();
            this.EFTPSPayments = new List<EFTPSPayment>();
            this.EmailTemplates = new List<EmailTemplate>();
            this.EstimatedTaxes = new List<EstimatedTax>();
            this.EstimatedTaxesIssues = new List<EstimatedTaxesIssue>();
            this.EstimatedTaxesIssuesSteps = new List<EstimatedTaxesIssuesStep>();
            this.EstimatedTaxesSteps = new List<EstimatedTaxesStep>();
            this.ExpenseEntries = new List<ExpenseEntry>();
            this.ExpenseOfficers = new List<ExpenseOfficer>();
            this.ExpenseReports = new List<ExpenseReport>();
            this.Expenses = new List<Expens>();
            this.ExpensesSteps = new List<ExpensesStep>();
            this.FAPBrochures = new List<FAPBrochure>();
            this.FAPTrainings = new List<FAPTraining>();
            this.FAPTrainingRegistereds = new List<FAPTrainingRegistered>();
            this.FavoriteCharities = new List<FavoriteCharity>();
            this.FeePricings = new List<FeePricing>();
            this.FeePricings1 = new List<FeePricing>();
            this.FinancialAdvisorsRequests = new List<FinancialAdvisorsRequest>();
            this.FoundationAccountsSteps = new List<FoundationAccountsStep>();
            this.FoundationProjects = new List<FoundationProject>();
            this.FoundationProjects1 = new List<FoundationProject>();
            this.Foundations = new List<Foundation>();
            this.FoundationsCharities = new List<FoundationsCharity>();
            this.FoundationsGrowthInfoes = new List<FoundationsGrowthInfo>();
            this.FoundationsRatings = new List<FoundationsRating>();
            this.GrantDetails = new List<GrantDetail>();
            this.GrantingParams = new List<GrantingParam>();
            this.GrantProposals = new List<GrantProposal>();
            this.Grants = new List<Grant>();
            this.HistoricalGrants = new List<HistoricalGrant>();
            this.HouseholdAccounts = new List<HouseholdAccount>();
            this.HouseholdAccountsSteps = new List<HouseholdAccountsStep>();
            this.IndividualNotes = new List<IndividualNote>();
            this.LinkRepositories = new List<LinkRepository>();
            this.ManualPayments = new List<ManualPayment>();
            this.Meetings = new List<Meeting>();
            this.MeetingsAttendeds = new List<MeetingsAttended>();
            this.MeetingsSteps = new List<MeetingsStep>();
            this.ObjectsPermissions = new List<ObjectsPermission>();
            this.OfficersPositions = new List<OfficersPosition>();
            this.old_Nominees = new List<old_Nominees>();
            this.PayeeProfiles = new List<PayeeProfile>();
            this.Payments = new List<Payment>();
            this.PaymentsSteps = new List<PaymentsStep>();
            this.PersonFoundations = new List<PersonFoundation>();
            this.tmp_GrantHistory = new List<tmp_GrantHistory>();
            this.tmp_GrantHistory1 = new List<tmp_GrantHistory>();
            this.PersonsPositions = new List<PersonsPosition>();
            this.PricingScheduleInstances = new List<PricingScheduleInstance>();
            this.PricingScheduleInstances1 = new List<PricingScheduleInstance>();
            this.PricingSchedules = new List<PricingSchedule>();
            this.PricingSchedules1 = new List<PricingSchedule>();
            this.Proposals = new List<Proposal>();
            this.ProposalStaffs = new List<ProposalStaff>();
            this.QModifyReports = new List<QModifyReport>();
            this.QModifyReports1 = new List<QModifyReport>();
            this.QModifyTransactions = new List<QModifyTransaction>();
            this.QReports = new List<QReport>();
            this.QTransactions = new List<QTransaction>();
            this.QuestSummaries_ = new List<QuestSummaries_>();
            this.QuestSummaries_1 = new List<QuestSummaries_>();
            this.QuestSummaries_2 = new List<QuestSummaries_>();
            this.Recipients = new List<Recipient>();
            this.PersonsSteps = new List<PersonsStep>();
            this.ServicePackages = new List<ServicePackage>();
            this.ServicePackages1 = new List<ServicePackage>();
            this.ServicePackageServices = new List<ServicePackageService>();
            this.ServicePackageServices1 = new List<ServicePackageService>();
            this.ServiceProviders = new List<ServiceProvider>();
            this.StaffingPolicies = new List<StaffingPolicy>();
            this.SubstantialContributors = new List<SubstantialContributor>();
            this.Successors = new List<Successor>();
            this.Successors1 = new List<Successor>();
            this.TaxReceipts = new List<TaxReceipt>();
            this.TaxReceipts1 = new List<TaxReceipt>();
            this.TaxReturnDocuments = new List<TaxReturnDocument>();
            this.TaxReturnDocuments1 = new List<TaxReturnDocument>();
            this.TaxReturnForms = new List<TaxReturnForm>();
            this.TaxReturnForms1 = new List<TaxReturnForm>();
            this.TaxReturnIssues = new List<TaxReturnIssue>();
            this.TaxReturnIssues1 = new List<TaxReturnIssue>();
            this.TaxReturnIssuesSteps = new List<TaxReturnIssuesStep>();
            this.TaxReturns = new List<TaxReturn>();
            this.TaxReturns1 = new List<TaxReturn>();
            this.TaxReturnsSteps = new List<TaxReturnsStep>();
            this.TaxReturnsSteps1 = new List<TaxReturnsStep>();
            this.ToDoes = new List<ToDo>();
            this.ToDoNotes = new List<ToDoNote>();
            this.TransactsDonors = new List<TransactsDonor>();
            this.TransferContacts = new List<TransferContact>();
            this.TransferContacts1 = new List<TransferContact>();
            this.TransferContacts2 = new List<TransferContact>();
            this.TransferGroups = new List<TransferGroup>();
            this.TransferProfiles = new List<TransferProfile>();
            this.TransferProfiles1 = new List<TransferProfile>();
            this.TransferProfiles2 = new List<TransferProfile>();
            this.TransferProfiles3 = new List<TransferProfile>();
            this.TransferProfiles4 = new List<TransferProfile>();
            this.TransferSchedules = new List<TransferSchedule>();
            this.TransferSchedules1 = new List<TransferSchedule>();
            this.TrustedActions = new List<TrustedAction>();
            this.TrustedActions1 = new List<TrustedAction>();
            this.Votings = new List<Voting>();
        }

        public int ID { get; set; }
        public Nullable<int> NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public Nullable<int> NameSuffix { get; set; }
        public Nullable<int> KeyQuestion { get; set; }
        public string KeyAnswer { get; set; }
        public string SSN { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<bool> Gender { get; set; }
        public Nullable<int> MaritalStatus { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessFax { get; set; }
        public string Employer { get; set; }
        public Nullable<int> SalaryRange { get; set; }
        public int RegistrationStep { get; set; }
        public Nullable<bool> MatchingGift { get; set; }
        public string AnotherPosition { get; set; }
        public string ShareHolder { get; set; }
        public Nullable<bool> StockExchange { get; set; }
        public Nullable<int> SHAddress { get; set; }
        public bool AllertMessage { get; set; }
        public int Rank { get; set; }
        public Nullable<int> ProgramCode { get; set; }
        public Nullable<System.DateTime> DOD { get; set; }
        public string EmployeeID { get; set; }
        public Nullable<int> DesignMode { get; set; }
        public string Rights { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public Nullable<int> PostalAddress2 { get; set; }
        public Nullable<int> PostalAddress3 { get; set; }
        public string CellPhone { get; set; }
        public string OtherPhone { get; set; }
        public string Assistant { get; set; }
        public string AsstPhone { get; set; }
        public string Dear { get; set; }
        public bool OnlineAccess { get; set; }
        public bool AllowEmail { get; set; }
        public bool AllowMailing { get; set; }
        public Nullable<int> MailingTax { get; set; }
        public Nullable<int> MailingTaxAddress { get; set; }
        public Nullable<int> MailingMarketing { get; set; }
        public Nullable<int> MailingMarketingAddress { get; set; }
        public Nullable<int> MailingInformal { get; set; }
        public Nullable<int> MailingInformalAddress { get; set; }
        public Nullable<int> MailingFormal { get; set; }
        public Nullable<int> MailingFormalAddress { get; set; }
        public Nullable<int> MailingNewServices { get; set; }
        public Nullable<int> MailingNewServicesAddress { get; set; }
        public string MailingDescription { get; set; }
        public bool AllowCall { get; set; }
        public bool CallGrantsQuestions { get; set; }
        public bool CallNewFeaturesReview { get; set; }
        public bool CallMarketing { get; set; }
        public bool CallMisc { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public Nullable<int> MoreThanMoney { get; set; }
        public Nullable<int> MoreThanMoneyAddress { get; set; }
        public bool ExpenseReinbursement { get; set; }
        public Nullable<int> ExpenseReinbursementAddress { get; set; }
        public string GmxRefs { get; set; }
        public virtual ICollection<ACHTransact> ACHTransacts { get; set; }
        public virtual ICollection<ActivityLog> ActivityLogs { get; set; }
        public virtual ICollection<ActivityPricing> ActivityPricings { get; set; }
        public virtual ICollection<ActivityPricing> ActivityPricings1 { get; set; }
        public virtual ICollection<AssetBook> AssetBooks { get; set; }
        public virtual ICollection<AssetPrice> AssetPrices { get; set; }
        public virtual ICollection<AssetValuation> AssetValuations { get; set; }
        public virtual ICollection<CharityRequest> CharityRequests { get; set; }
        public virtual ICollection<CheckingAccount> CheckingAccounts { get; set; }
        public virtual ICollection<CheckingAccount> CheckingAccounts1 { get; set; }
        public virtual ICollection<CheckPayee> CheckPayees { get; set; }
        public virtual ICollection<Check> Checks { get; set; }
        public virtual ICollection<Check> Checks1 { get; set; }
        public virtual ICollection<Check> Checks2 { get; set; }
        public virtual ICollection<Check> Checks3 { get; set; }
        public virtual ICollection<CMSBrochure> CMSBrochures { get; set; }
        public virtual ICollection<CMSContact> CMSContacts { get; set; }
        public virtual ICollection<CMSEvent> CMSEvents { get; set; }
        public virtual ICollection<CMSFaq> CMSFaqs { get; set; }
        public virtual ICollection<CMSManager> CMSManagers { get; set; }
        public virtual ICollection<CMSNew> CMSNews { get; set; }
        public virtual ICollection<CMSPublication> CMSPublications { get; set; }
        public virtual ICollection<CompanyElement> CompanyElements { get; set; }
        public virtual ICollection<CompanyElement> CompanyElements1 { get; set; }
        public virtual ICollection<CompanyPerson> CompanyPersons { get; set; }
        public virtual ICollection<CompanyPerson> CompanyPersons1 { get; set; }
        public virtual ICollection<CompanyPerson> CompanyPersons2 { get; set; }
        public virtual ICollection<CompanyPosition> CompanyPositions { get; set; }
        public virtual ICollection<CompanyPosition> CompanyPositions1 { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual ICollection<ContractPlayer> ContractPlayers { get; set; }
        public virtual ICollection<ContractPlayer> ContractPlayers1 { get; set; }
        public virtual ICollection<ContractPricing> ContractPricings { get; set; }
        public virtual ICollection<ContractPricing> ContractPricings1 { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
        public virtual ICollection<Contract> Contracts1 { get; set; }
        public virtual ICollection<ContractServicePackage> ContractServicePackages { get; set; }
        public virtual ICollection<ContractServicePackage> ContractServicePackages1 { get; set; }
        public virtual ICollection<ContractService> ContractServices { get; set; }
        public virtual ICollection<ContractService> ContractServices1 { get; set; }
        public virtual ICollection<CRMTask> CRMTasks { get; set; }
        public virtual ICollection<CRMTask> CRMTasks1 { get; set; }
        public virtual ICollection<CRMTask> CRMTasks2 { get; set; }
        public virtual ICollection<CRMTask> CRMTasks3 { get; set; }
        public virtual ICollection<CustomReport> CustomReports { get; set; }
        public virtual ICollection<DisbursementAccount> DisbursementAccounts { get; set; }
        public virtual ICollection<DisbursementAccount> DisbursementAccounts1 { get; set; }
        public virtual ICollection<DisbursementAccountSubscriber> DisbursementAccountSubscribers { get; set; }
        public virtual ICollection<DisbursementAccountSubscriber> DisbursementAccountSubscribers1 { get; set; }
        public virtual ICollection<DocumentTemplate> DocumentTemplates { get; set; }
        public virtual ICollection<DonationCertificate> DonationCertificates { get; set; }
        public virtual ICollection<DonationCertificate> DonationCertificates1 { get; set; }
        public virtual ICollection<DonationRequest> DonationRequests { get; set; }
        public virtual ICollection<Donor> Donors { get; set; }
        public virtual ICollection<Donor> Donors1 { get; set; }
        public virtual ICollection<EFTPSConfReport> EFTPSConfReports { get; set; }
        public virtual ICollection<EFTPSPayment> EFTPSPayments { get; set; }
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
        public virtual ICollection<EstimatedTax> EstimatedTaxes { get; set; }
        public virtual ICollection<EstimatedTaxesIssue> EstimatedTaxesIssues { get; set; }
        public virtual ICollection<EstimatedTaxesIssuesStep> EstimatedTaxesIssuesSteps { get; set; }
        public virtual ICollection<EstimatedTaxesStep> EstimatedTaxesSteps { get; set; }
        public virtual ICollection<ExpenseEntry> ExpenseEntries { get; set; }
        public virtual ICollection<ExpenseOfficer> ExpenseOfficers { get; set; }
        public virtual ICollection<ExpenseReport> ExpenseReports { get; set; }
        public virtual ICollection<Expens> Expenses { get; set; }
        public virtual ICollection<ExpensesStep> ExpensesSteps { get; set; }
        public virtual ICollection<FAPBrochure> FAPBrochures { get; set; }
        public virtual ICollection<FAPTraining> FAPTrainings { get; set; }
        public virtual ICollection<FAPTrainingRegistered> FAPTrainingRegistereds { get; set; }
        public virtual ICollection<FavoriteCharity> FavoriteCharities { get; set; }
        public virtual ICollection<FeePricing> FeePricings { get; set; }
        public virtual ICollection<FeePricing> FeePricings1 { get; set; }
        public virtual FinancialAdvisor FinancialAdvisor { get; set; }
        public virtual ICollection<FinancialAdvisorsRequest> FinancialAdvisorsRequests { get; set; }
        public virtual ICollection<FoundationAccountsStep> FoundationAccountsSteps { get; set; }
        public virtual ICollection<FoundationProject> FoundationProjects { get; set; }
        public virtual ICollection<FoundationProject> FoundationProjects1 { get; set; }
        public virtual ICollection<Foundation> Foundations { get; set; }
        public virtual ICollection<FoundationsCharity> FoundationsCharities { get; set; }
        public virtual ICollection<FoundationsGrowthInfo> FoundationsGrowthInfoes { get; set; }
        public virtual ICollection<FoundationsRating> FoundationsRatings { get; set; }
        public virtual ICollection<GrantDetail> GrantDetails { get; set; }
        public virtual ICollection<GrantingParam> GrantingParams { get; set; }
        public virtual ICollection<GrantProposal> GrantProposals { get; set; }
        public virtual ICollection<Grant> Grants { get; set; }
        public virtual ICollection<HistoricalGrant> HistoricalGrants { get; set; }
        public virtual ICollection<HouseholdAccount> HouseholdAccounts { get; set; }
        public virtual ICollection<HouseholdAccountsStep> HouseholdAccountsSteps { get; set; }
        public virtual ICollection<IndividualNote> IndividualNotes { get; set; }
        public virtual ICollection<LinkRepository> LinkRepositories { get; set; }
        public virtual ICollection<ManualPayment> ManualPayments { get; set; }
        public virtual ICollection<Meeting> Meetings { get; set; }
        public virtual ICollection<MeetingsAttended> MeetingsAttendeds { get; set; }
        public virtual ICollection<MeetingsStep> MeetingsSteps { get; set; }
        public virtual ICollection<ObjectsPermission> ObjectsPermissions { get; set; }
        public virtual ICollection<OfficersPosition> OfficersPositions { get; set; }
        public virtual ICollection<old_Nominees> old_Nominees { get; set; }
        public virtual ICollection<PayeeProfile> PayeeProfiles { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<PaymentsStep> PaymentsSteps { get; set; }
        public virtual ICollection<PersonFoundation> PersonFoundations { get; set; }
        public virtual ICollection<tmp_GrantHistory> tmp_GrantHistory { get; set; }
        public virtual ICollection<tmp_GrantHistory> tmp_GrantHistory1 { get; set; }
        public virtual PostalAddress PostalAddress1 { get; set; }
        public virtual PostalAddress PostalAddress4 { get; set; }
        public virtual ProgramCode ProgramCode1 { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<PersonsPosition> PersonsPositions { get; set; }
        public virtual ICollection<PricingScheduleInstance> PricingScheduleInstances { get; set; }
        public virtual ICollection<PricingScheduleInstance> PricingScheduleInstances1 { get; set; }
        public virtual ICollection<PricingSchedule> PricingSchedules { get; set; }
        public virtual ICollection<PricingSchedule> PricingSchedules1 { get; set; }
        public virtual ICollection<Proposal> Proposals { get; set; }
        public virtual ICollection<ProposalStaff> ProposalStaffs { get; set; }
        public virtual ICollection<QModifyReport> QModifyReports { get; set; }
        public virtual ICollection<QModifyReport> QModifyReports1 { get; set; }
        public virtual ICollection<QModifyTransaction> QModifyTransactions { get; set; }
        public virtual ICollection<QReport> QReports { get; set; }
        public virtual ICollection<QTransaction> QTransactions { get; set; }
        public virtual ICollection<QuestSummaries_> QuestSummaries_ { get; set; }
        public virtual ICollection<QuestSummaries_> QuestSummaries_1 { get; set; }
        public virtual ICollection<QuestSummaries_> QuestSummaries_2 { get; set; }
        public virtual ICollection<Recipient> Recipients { get; set; }
        public virtual ICollection<PersonsStep> PersonsSteps { get; set; }
        public virtual ICollection<ServicePackage> ServicePackages { get; set; }
        public virtual ICollection<ServicePackage> ServicePackages1 { get; set; }
        public virtual ICollection<ServicePackageService> ServicePackageServices { get; set; }
        public virtual ICollection<ServicePackageService> ServicePackageServices1 { get; set; }
        public virtual ICollection<ServiceProvider> ServiceProviders { get; set; }
        public virtual ICollection<StaffingPolicy> StaffingPolicies { get; set; }
        public virtual ICollection<SubstantialContributor> SubstantialContributors { get; set; }
        public virtual ICollection<Successor> Successors { get; set; }
        public virtual ICollection<Successor> Successors1 { get; set; }
        public virtual ICollection<TaxReceipt> TaxReceipts { get; set; }
        public virtual ICollection<TaxReceipt> TaxReceipts1 { get; set; }
        public virtual ICollection<TaxReturnDocument> TaxReturnDocuments { get; set; }
        public virtual ICollection<TaxReturnDocument> TaxReturnDocuments1 { get; set; }
        public virtual ICollection<TaxReturnForm> TaxReturnForms { get; set; }
        public virtual ICollection<TaxReturnForm> TaxReturnForms1 { get; set; }
        public virtual ICollection<TaxReturnIssue> TaxReturnIssues { get; set; }
        public virtual ICollection<TaxReturnIssue> TaxReturnIssues1 { get; set; }
        public virtual ICollection<TaxReturnIssuesStep> TaxReturnIssuesSteps { get; set; }
        public virtual ICollection<TaxReturn> TaxReturns { get; set; }
        public virtual ICollection<TaxReturn> TaxReturns1 { get; set; }
        public virtual ICollection<TaxReturnsStep> TaxReturnsSteps { get; set; }
        public virtual ICollection<TaxReturnsStep> TaxReturnsSteps1 { get; set; }
        public virtual ICollection<ToDo> ToDoes { get; set; }
        public virtual ICollection<ToDoNote> ToDoNotes { get; set; }
        public virtual ICollection<TransactsDonor> TransactsDonors { get; set; }
        public virtual ICollection<TransferContact> TransferContacts { get; set; }
        public virtual ICollection<TransferContact> TransferContacts1 { get; set; }
        public virtual ICollection<TransferContact> TransferContacts2 { get; set; }
        public virtual ICollection<TransferGroup> TransferGroups { get; set; }
        public virtual ICollection<TransferProfile> TransferProfiles { get; set; }
        public virtual ICollection<TransferProfile> TransferProfiles1 { get; set; }
        public virtual ICollection<TransferProfile> TransferProfiles2 { get; set; }
        public virtual ICollection<TransferProfile> TransferProfiles3 { get; set; }
        public virtual ICollection<TransferProfile> TransferProfiles4 { get; set; }
        public virtual ICollection<TransferSchedule> TransferSchedules { get; set; }
        public virtual ICollection<TransferSchedule> TransferSchedules1 { get; set; }
        public virtual ICollection<TrustedAction> TrustedActions { get; set; }
        public virtual ICollection<TrustedAction> TrustedActions1 { get; set; }
        public virtual ICollection<Voting> Votings { get; set; }
    }
}
