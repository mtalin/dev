using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Job
    {
        public Job()
        {
            this.Jobs1 = new List<Job>();
            this.Jobs11 = new List<Job>();
        }

        public int ID { get; set; }
        public int Event { get; set; }
        public Nullable<int> ParentJob { get; set; }
        public Nullable<int> PreviousJob { get; set; }
        public int JobClass { get; set; }
        public Nullable<System.DateTime> DtSchedule { get; set; }
        public int State { get; set; }
        public string Result { get; set; }
        public int RetryNo { get; set; }
        public Nullable<System.DateTime> DtStart { get; set; }
        public Nullable<System.DateTime> DtStop { get; set; }
        public string JobData { get; set; }
        public int Priority { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual JobClass JobClass1 { get; set; }
        public virtual ICollection<Job> Jobs1 { get; set; }
        public virtual Job Job1 { get; set; }
        public virtual ICollection<Job> Jobs11 { get; set; }
        public virtual Job Job2 { get; set; }
    }
}
