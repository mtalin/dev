using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AssetValuation
    {
        public int ID { get; set; }
        public int AssetBook { get; set; }
        public int Operation { get; set; }
        public System.DateTime DtOperation { get; set; }
        public decimal Amount { get; set; }
        public Nullable<int> Document { get; set; }
        public string AppCompany { get; set; }
        public string AppContact { get; set; }
        public string AppPhone { get; set; }
        public Nullable<int> Transact { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public Nullable<System.DateTime> DtAppraisal { get; set; }
        public Nullable<int> Note { get; set; }
        public virtual AssetBook AssetBook1 { get; set; }
        public virtual Document1 Document1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Transact Transact1 { get; set; }
    }
}
