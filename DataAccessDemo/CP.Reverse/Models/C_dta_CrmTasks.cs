using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class C_dta_CrmTasks
    {
        public Nullable<System.DateTime> C_col_1 { get; set; }
        public Nullable<int> C_col_2 { get; set; }
        public int C_col_3 { get; set; }
        public System.DateTime C_col_4 { get; set; }
        public string C_col_5 { get; set; }
        public Nullable<int> C_col_6 { get; set; }
        public Nullable<int> C_col_7 { get; set; }
        public Nullable<int> C_col_8 { get; set; }
        public Nullable<int> C_col_9 { get; set; }
        public Nullable<int> C_col_10 { get; set; }
    }
}
