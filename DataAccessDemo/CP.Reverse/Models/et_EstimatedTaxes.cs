using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_EstimatedTaxes
    {
        public int ID { get; set; }
        public Nullable<decimal> NIIExcCapGain { get; set; }
        public Nullable<decimal> AnnualizedNIIExcCapGain { get; set; }
        public Nullable<decimal> CapitalGains { get; set; }
        public Nullable<decimal> AnnualizedIncome { get; set; }
        public Nullable<decimal> AnnualizedTaxLiability { get; set; }
        public Nullable<decimal> QuarterlyTaxPayment { get; set; }
        public Nullable<decimal> Paid { get; set; }
        public Nullable<decimal> ActEstTaxPayment { get; set; }
        public Nullable<decimal> RoundedPayment { get; set; }
        public Nullable<int> Month { get; set; }
        public int Quarter { get; set; }
        public Nullable<decimal> Factor { get; set; }
        public System.DateTime dtStartPeriod { get; set; }
        public Nullable<System.DateTime> dtEndPeriod { get; set; }
        public Nullable<int> Tax { get; set; }
        public int Year { get; set; }
        public string FullNameQuarter { get; set; }
        public Nullable<int> Percent { get; set; }
        public Nullable<decimal> IntIncome { get; set; }
        public Nullable<decimal> DivIncome { get; set; }
        public Nullable<decimal> AssetManagementFees { get; set; }
        public Nullable<decimal> NetInvIncome { get; set; }
        public int IsFullYear { get; set; }
        public int Method { get; set; }
        public Nullable<int> Foundation { get; set; }
        public string FoundationName { get; set; }
        public Nullable<int> FiscalYearEnd { get; set; }
        public Nullable<System.DateTime> TaxYearEnd { get; set; }
        public Nullable<decimal> Line11Income { get; set; }
        public Nullable<decimal> PartnershipIncome { get; set; }
    }
}
