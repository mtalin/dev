using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MQin
    {
        public int ID { get; set; }
        public System.DateTime Created { get; set; }
        public int Action { get; set; }
        public string ObjectType { get; set; }
        public int ObjectId { get; set; }
        public virtual MQout MQout { get; set; }
    }
}
