using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReturnsStep
    {
        public int ID { get; set; }
        public Nullable<int> TaxReturn { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public Nullable<int> Step { get; set; }
        public Nullable<int> State1 { get; set; }
        public Nullable<int> State2 { get; set; }
        public Nullable<int> State3 { get; set; }
        public Nullable<int> State4 { get; set; }
        public Nullable<int> ResponsiblePerson { get; set; }
        public Nullable<System.DateTime> ActualDate { get; set; }
        public Nullable<int> Note { get; set; }
        public Nullable<int> Event { get; set; }
        public bool IsNoteClosed { get; set; }
        public Nullable<int> NoteType { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual TaxReturn TaxReturn1 { get; set; }
    }
}
