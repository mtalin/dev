using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Tax
    {
        public Tax()
        {
            this.EstimatedTaxes = new List<EstimatedTax>();
            this.InvoicesItems = new List<InvoicesItem>();
            this.old_TaxesItems = new List<old_TaxesItems>();
            this.TaxesSteps = new List<TaxesStep>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public Nullable<decimal> Amount2 { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
        public int PeriodType { get; set; }
        public int PeriodCount { get; set; }
        public int Step { get; set; }
        public Nullable<int> PendingTransact { get; set; }
        public string DataSnapShot { get; set; }
        public Nullable<int> Contract { get; set; }
        public Nullable<int> BillTo { get; set; }
        public Nullable<int> FeeCalculation { get; set; }
        public bool Q1Covered { get; set; }
        public bool Q2Covered { get; set; }
        public bool Q3Covered { get; set; }
        public bool Q4Covered { get; set; }
        public virtual Contract Contract1 { get; set; }
        public virtual ICollection<EstimatedTax> EstimatedTaxes { get; set; }
        public virtual FeeCalculation FeeCalculation1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<InvoicesItem> InvoicesItems { get; set; }
        public virtual ICollection<old_TaxesItems> old_TaxesItems { get; set; }
        public virtual PendingTransact PendingTransact1 { get; set; }
        public virtual StructuredCompany StructuredCompany { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<TaxesStep> TaxesSteps { get; set; }
    }
}
