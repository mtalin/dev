using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class StmtTracking
    {
        public int ID { get; set; }
        public int FoundationAccount { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public Nullable<System.DateTime> CheckDate { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
        public virtual FoundationAccount FoundationAccount1 { get; set; }
    }
}
