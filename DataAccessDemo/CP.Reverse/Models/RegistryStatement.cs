using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class RegistryStatement
    {
        public int ID { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public int Currency { get; set; }
        public int Asset { get; set; }
        public int Transact { get; set; }
        public short RegistryRule { get; set; }
        public short TransactType { get; set; }
        public System.DateTime DtTransact { get; set; }
        public byte AssetType { get; set; }
        public int Foundation { get; set; }
        public int FoundationAccount { get; set; }
        public Nullable<short> FiscalYear { get; set; }
        public int BookId { get; set; }
        public byte PartId { get; set; }
        public short LineId { get; set; }
        public string PartName { get; set; }
        public string LineName { get; set; }
    }
}
