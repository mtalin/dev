using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class YearRangeDate
    {
        public int Year { get; set; }
        public System.DateTime Day1 { get; set; }
        public System.DateTime Day2 { get; set; }
    }
}
