using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vProject
    {
        public int ID { get; set; }
        public int Area { get; set; }
        public Nullable<int> Program { get; set; }
        public Nullable<int> Project { get; set; }
        public string AreaName { get; set; }
        public string ProgramName { get; set; }
        public string ProjectName { get; set; }
        public int Foundation { get; set; }
        public int Level { get; set; }
        public string Name { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> AuthorDeleted { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
    }
}
