using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ExpenseOfficer
    {
        public int ID { get; set; }
        public int Expense { get; set; }
        public int Person { get; set; }
        public virtual Expens Expens { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
