using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReturnIssue
    {
        public TaxReturnIssue()
        {
            this.TaxReturnIssuesSteps = new List<TaxReturnIssuesStep>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int TaxReturn { get; set; }
        public int IssueType { get; set; }
        public int Source { get; set; }
        public int Step { get; set; }
        public string Description { get; set; }
        public System.DateTime DtOpen { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public Nullable<int> ClosedBy { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual TaxReturn TaxReturn1 { get; set; }
        public virtual ICollection<TaxReturnIssuesStep> TaxReturnIssuesSteps { get; set; }
    }
}
