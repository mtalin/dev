using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vContributedAmount
    {
        public Nullable<int> Donor { get; set; }
        public int Foundation { get; set; }
        public System.DateTime DtTransact { get; set; }
        public Nullable<short> FiscalYear { get; set; }
        public Nullable<decimal> ActualAmount { get; set; }
        public Nullable<decimal> PhantomAmount { get; set; }
    }
}
