using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class qd_FPersons_
    {
        public int id { get; set; }
        public System.DateTime dtCreated { get; set; }
        public int Questionnaire { get; set; }
        public int Type { get; set; }
        public Nullable<int> Person { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public bool Excluded { get; set; }
        public string Hours { get; set; }
        public string Comments { get; set; }
    }
}
