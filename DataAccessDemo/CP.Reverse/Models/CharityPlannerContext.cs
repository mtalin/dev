using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using CP.Reverse.Models.Mapping;

namespace CP.Reverse.Models
{
    public class CharityPlannerContext : DbContext
    {
        static CharityPlannerContext()
        {
            Database.SetInitializer<CharityPlannerContext>(null);
        }

		public CharityPlannerContext()
			: base("Name=CharityPlannerContext")
		{
		}

        public DbSet<AbbreviateAddress> AbbreviateAddresses { get; set; }
        public DbSet<ACHTransact> ACHTransacts { get; set; }
        public DbSet<ActionsLog> ActionsLogs { get; set; }
        public DbSet<ActivityCode> ActivityCodes { get; set; }
        public DbSet<ActivityLogItem> ActivityLogItems { get; set; }
        public DbSet<ActivityLog> ActivityLogs { get; set; }
        public DbSet<ActivityPricing> ActivityPricings { get; set; }
        public DbSet<AnnualPayout> AnnualPayouts { get; set; }
        public DbSet<AspSession> AspSessions { get; set; }
        public DbSet<AssetBook> AssetBooks { get; set; }
        public DbSet<AssetPrice> AssetPrices { get; set; }
        public DbSet<AssetPricesTemp> AssetPricesTemps { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<AssetValuation> AssetValuations { get; set; }
        public DbSet<BAConfig> BAConfigs { get; set; }
        public DbSet<bak_ActivityPricings> bak_ActivityPricings { get; set; }
        public DbSet<bak_FeePricings> bak_FeePricings { get; set; }
        public DbSet<bak_PricingScheduleInstances> bak_PricingScheduleInstances { get; set; }
        public DbSet<bak_PricingSchedules> bak_PricingSchedules { get; set; }
        public DbSet<bak_ServicePackages> bak_ServicePackages { get; set; }
        public DbSet<bak_ServicePackageServices> bak_ServicePackageServices { get; set; }
        public DbSet<BanksOperator> BanksOperators { get; set; }
        public DbSet<BATempGrantList> BATempGrantLists { get; set; }
        public DbSet<BogusSSN> BogusSSNs { get; set; }
        public DbSet<CarryforwardDetail> CarryforwardDetails { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CertifiedMail> CertifiedMails { get; set; }
        public DbSet<CharityRequest> CharityRequests { get; set; }
        public DbSet<CharityRequestsStep> CharityRequestsSteps { get; set; }
        public DbSet<CharityRespons> CharityResponses { get; set; }
        public DbSet<CheckingAccount> CheckingAccounts { get; set; }
        public DbSet<CheckPayee> CheckPayees { get; set; }
        public DbSet<Check> Checks { get; set; }
        public DbSet<CMSBrochure> CMSBrochures { get; set; }
        public DbSet<CMSContact> CMSContacts { get; set; }
        public DbSet<CMSEvent> CMSEvents { get; set; }
        public DbSet<CMSFaq> CMSFaqs { get; set; }
        public DbSet<CMSManager> CMSManagers { get; set; }
        public DbSet<CMSNew> CMSNews { get; set; }
        public DbSet<CMSPublication> CMSPublications { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyElement> CompanyElements { get; set; }
        public DbSet<CompanyPerson> CompanyPersons { get; set; }
        public DbSet<CompanyPosition> CompanyPositions { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactsStep> ContactsSteps { get; set; }
        public DbSet<ContactsStepsContact> ContactsStepsContacts { get; set; }
        public DbSet<ContactsStepsNote> ContactsStepsNotes { get; set; }
        public DbSet<ContractPlayerRole> ContractPlayerRoles { get; set; }
        public DbSet<ContractPlayer> ContractPlayers { get; set; }
        public DbSet<ContractPricing> ContractPricings { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<ContractServicePackage> ContractServicePackages { get; set; }
        public DbSet<ContractService> ContractServices { get; set; }
        public DbSet<CostBas> CostBases { get; set; }
        public DbSet<CreditCard> CreditCards { get; set; }
        public DbSet<CRMTask> CRMTasks { get; set; }
        public DbSet<CRMTasksStep> CRMTasksSteps { get; set; }
        public DbSet<Crossing> Crossings { get; set; }
        public DbSet<CTL_Errors> CTL_Errors { get; set; }
        public DbSet<CurrentBalance> CurrentBalances { get; set; }
        public DbSet<CustomReportCategory> CustomReportCategories { get; set; }
        public DbSet<CustomReport> CustomReports { get; set; }
        public DbSet<CustomReportsTemplate> CustomReportsTemplates { get; set; }
        public DbSet<DefaultCharity> DefaultCharities { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<DesignVotingOfferItem> DesignVotingOfferItems { get; set; }
        public DbSet<DisbursementAccount> DisbursementAccounts { get; set; }
        public DbSet<DisbursementAccountSubscriber> DisbursementAccountSubscribers { get; set; }
        public DbSet<DistributableAmount> DistributableAmounts { get; set; }
        public DbSet<DistributableAmountsTemp> DistributableAmountsTemps { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Document1> Documents1 { get; set; }
        public DbSet<DocumentTemplate> DocumentTemplates { get; set; }
        public DbSet<DocumentView> DocumentViews { get; set; }
        public DbSet<DonationCertificate> DonationCertificates { get; set; }
        public DbSet<DonationRequest> DonationRequests { get; set; }
        public DbSet<DonationsLog> DonationsLogs { get; set; }
        public DbSet<Donor> Donors { get; set; }
        public DbSet<dtproperty> dtproperties { get; set; }
        public DbSet<EFTPSConfirmation> EFTPSConfirmations { get; set; }
        public DbSet<EFTPSConfReport> EFTPSConfReports { get; set; }
        public DbSet<EFTPSEnrollment> EFTPSEnrollments { get; set; }
        public DbSet<EFTPSEnrollmentsStep> EFTPSEnrollmentsSteps { get; set; }
        public DbSet<EFTPSPayment> EFTPSPayments { get; set; }
        public DbSet<EFT> EFTs { get; set; }
        public DbSet<EMailNotifyTransact> EMailNotifyTransacts { get; set; }
        public DbSet<EmailRecipient> EmailRecipients { get; set; }
        public DbSet<EMail> EMails { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<Enum> Enums { get; set; }
        public DbSet<EnumsCategory> EnumsCategories { get; set; }
        public DbSet<EnumsText> EnumsTexts { get; set; }
        public DbSet<EstimatedTax> EstimatedTaxes { get; set; }
        public DbSet<EstimatedTaxesIssue> EstimatedTaxesIssues { get; set; }
        public DbSet<EstimatedTaxesIssuesStep> EstimatedTaxesIssuesSteps { get; set; }
        public DbSet<EstimatedTaxesStep> EstimatedTaxesSteps { get; set; }
        public DbSet<EventAction> EventActions { get; set; }
        public DbSet<EventClass> EventClasses { get; set; }
        public DbSet<EventRecipient> EventRecipients { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventSource> EventSources { get; set; }
        public DbSet<EventType> EventTypes { get; set; }
        public DbSet<ExcludedEmailApplicant> ExcludedEmailApplicants { get; set; }
        public DbSet<ExpenseEntry> ExpenseEntries { get; set; }
        public DbSet<ExpenseLocation> ExpenseLocations { get; set; }
        public DbSet<ExpenseOfficer> ExpenseOfficers { get; set; }
        public DbSet<ExpenseRange> ExpenseRanges { get; set; }
        public DbSet<ExpenseReport> ExpenseReports { get; set; }
        public DbSet<Expens> Expenses { get; set; }
        public DbSet<ExpensesStep> ExpensesSteps { get; set; }
        public DbSet<FAPBrochure> FAPBrochures { get; set; }
        public DbSet<FAPPackageForm> FAPPackageForms { get; set; }
        public DbSet<FAPPackageRequest> FAPPackageRequests { get; set; }
        public DbSet<FAPPackage> FAPPackages { get; set; }
        public DbSet<FAPTraining> FAPTrainings { get; set; }
        public DbSet<FAPTrainingRegistered> FAPTrainingRegistereds { get; set; }
        public DbSet<FavoriteCharity> FavoriteCharities { get; set; }
        public DbSet<FeeCalculation> FeeCalculations { get; set; }
        public DbSet<FeePricing> FeePricings { get; set; }
        public DbSet<FinancialAdvisorGroup> FinancialAdvisorGroups { get; set; }
        public DbSet<FinancialAdvisor> FinancialAdvisors { get; set; }
        public DbSet<FinancialAdvisorsFoundation> FinancialAdvisorsFoundations { get; set; }
        public DbSet<FinancialAdvisorsRequest> FinancialAdvisorsRequests { get; set; }
        public DbSet<FinancialAdvisorsRequestsStep> FinancialAdvisorsRequestsSteps { get; set; }
        public DbSet<FinancialPartnerAccount> FinancialPartnerAccounts { get; set; }
        public DbSet<FinancialPartnerChunk> FinancialPartnerChunks { get; set; }
        public DbSet<FinancialPartnerDocument> FinancialPartnerDocuments { get; set; }
        public DbSet<FinancialStatementsPart> FinancialStatementsParts { get; set; }
        public DbSet<FoundationAccount> FoundationAccounts { get; set; }
        public DbSet<FoundationAccountsStep> FoundationAccountsSteps { get; set; }
        public DbSet<FoundationEvent> FoundationEvents { get; set; }
        public DbSet<FoundationProject> FoundationProjects { get; set; }
        public DbSet<Foundation> Foundations { get; set; }
        public DbSet<FoundationsBoardMeeting> FoundationsBoardMeetings { get; set; }
        public DbSet<FoundationsCharity> FoundationsCharities { get; set; }
        public DbSet<FoundationsGrowthInfo> FoundationsGrowthInfoes { get; set; }
        public DbSet<FoundationsRating> FoundationsRatings { get; set; }
        public DbSet<FoundationsStep> FoundationsSteps { get; set; }
        public DbSet<FundingDocument> FundingDocuments { get; set; }
        public DbSet<Funding> Fundings { get; set; }
        public DbSet<GenericEmail> GenericEmails { get; set; }
        public DbSet<GenericText> GenericTexts { get; set; }
        public DbSet<GlobalAsset> GlobalAssets { get; set; }
        public DbSet<GlobalRecipient> GlobalRecipients { get; set; }
        public DbSet<GrantApplication> GrantApplications { get; set; }
        public DbSet<GrantDetail> GrantDetails { get; set; }
        public DbSet<GrantingParam> GrantingParams { get; set; }
        public DbSet<GrantMessage> GrantMessages { get; set; }
        public DbSet<GrantProposal> GrantProposals { get; set; }
        public DbSet<Grant> Grants { get; set; }
        public DbSet<GrantSchedule> GrantSchedules { get; set; }
        public DbSet<GrantsStep> GrantsSteps { get; set; }
        public DbSet<HistoricalGrant> HistoricalGrants { get; set; }
        public DbSet<Holding> Holdings { get; set; }
        public DbSet<HouseholdAccount> HouseholdAccounts { get; set; }
        public DbSet<HouseholdAccountsStep> HouseholdAccountsSteps { get; set; }
        public DbSet<HouseholdDocument> HouseholdDocuments { get; set; }
        public DbSet<HouseholdMember> HouseholdMembers { get; set; }
        public DbSet<HouseholdValue> HouseholdValues { get; set; }
        public DbSet<IndividualNote> IndividualNotes { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoicesItem> InvoicesItems { get; set; }
        public DbSet<JobClass> JobClasses { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<LinkRepository> LinkRepositories { get; set; }
        public DbSet<LocalAsset> LocalAssets { get; set; }
        public DbSet<MailApply> MailApplies { get; set; }
        public DbSet<MailAppliesDocument> MailAppliesDocuments { get; set; }
        public DbSet<MailAppliesSelectedDocument> MailAppliesSelectedDocuments { get; set; }
        public DbSet<MailAppliesStep> MailAppliesSteps { get; set; }
        public DbSet<MailAppliesStepsNote> MailAppliesStepsNotes { get; set; }
        public DbSet<MailNotifyTransact> MailNotifyTransacts { get; set; }
        public DbSet<ManualPayment> ManualPayments { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<MeetingsAttended> MeetingsAttendeds { get; set; }
        public DbSet<MeetingsProposal> MeetingsProposals { get; set; }
        public DbSet<MeetingsStep> MeetingsSteps { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<MoneyTransferTransact> MoneyTransferTransacts { get; set; }
        public DbSet<NCode> NCodes { get; set; }
        public DbSet<NoteCategory> NoteCategories { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<ObjectsPermission> ObjectsPermissions { get; set; }
        public DbSet<OfficersPosition> OfficersPositions { get; set; }
        public DbSet<old_FinancialPartners> old_FinancialPartners { get; set; }
        public DbSet<old_Nominees> old_Nominees { get; set; }
        public DbSet<old_NomineesSteps> old_NomineesSteps { get; set; }
        public DbSet<old_PendingTransactItems> old_PendingTransactItems { get; set; }
        public DbSet<old_PendingTransactItemSteps> old_PendingTransactItemSteps { get; set; }
        public DbSet<old_TaxesItems> old_TaxesItems { get; set; }
        public DbSet<oldQuestionnaire> oldQuestionnaires { get; set; }
        public DbSet<OLEClass> OLEClasses { get; set; }
        public DbSet<PartnershipAmount> PartnershipAmounts { get; set; }
        public DbSet<Partnership> Partnerships { get; set; }
        public DbSet<PartnershipsStep> PartnershipsSteps { get; set; }
        public DbSet<PartnersInfo> PartnersInfoes { get; set; }
        public DbSet<PartnersQuarterlyFee> PartnersQuarterlyFees { get; set; }
        public DbSet<PartnersText> PartnersTexts { get; set; }
        public DbSet<PayeeProfile> PayeeProfiles { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentsBatch> PaymentsBatches { get; set; }
        public DbSet<PaymentsStep> PaymentsSteps { get; set; }
        public DbSet<PendingTransact> PendingTransacts { get; set; }
        public DbSet<PendingTransactsStep> PendingTransactsSteps { get; set; }
        public DbSet<PersonFoundation> PersonFoundations { get; set; }
        public DbSet<PersonNotification> PersonNotifications { get; set; }
        public DbSet<PersonRightsHistory> PersonRightsHistories { get; set; }
        public DbSet<PersonRole> PersonRoles { get; set; }
        public DbSet<PersonRolesHistory> PersonRolesHistories { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PersonsPosition> PersonsPositions { get; set; }
        public DbSet<PersonsStep> PersonsSteps { get; set; }
        public DbSet<Phase> Phases { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<PostalAddress> PostalAddresses { get; set; }
        public DbSet<PricingScheduleInstance> PricingScheduleInstances { get; set; }
        public DbSet<PricingSchedule> PricingSchedules { get; set; }
        public DbSet<ProgramCode> ProgramCodes { get; set; }
        public DbSet<Proposal> Proposals { get; set; }
        public DbSet<ProposalsStep> ProposalsSteps { get; set; }
        public DbSet<ProposalStaff> ProposalStaffs { get; set; }
        public DbSet<QBalance> QBalances { get; set; }
        public DbSet<QCapitalGain> QCapitalGains { get; set; }
        public DbSet<qd_FAddresses_> qd_FAddresses_ { get; set; }
        public DbSet<qd_FPersons_> qd_FPersons_ { get; set; }
        public DbSet<qd_YNQuestions_> qd_YNQuestions_ { get; set; }
        public DbSet<QDeposit> QDeposits { get; set; }
        public DbSet<QHistoryTransaction> QHistoryTransactions { get; set; }
        public DbSet<QModifyReport> QModifyReports { get; set; }
        public DbSet<QModifyTransaction> QModifyTransactions { get; set; }
        public DbSet<QParser> QParsers { get; set; }
        public DbSet<QPortfolio> QPortfolios { get; set; }
        public DbSet<QReport> QReports { get; set; }
        public DbSet<QSecurityPrice> QSecurityPrices { get; set; }
        public DbSet<QTransaction> QTransactions { get; set; }
        public DbSet<Query> Queries { get; set; }
        public DbSet<QueryCategory> QueryCategories { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<QuestionnairesStep> QuestionnairesSteps { get; set; }
        public DbSet<QuestSummaries_> QuestSummaries_ { get; set; }
        public DbSet<RecentFoundationLookup> RecentFoundationLookups { get; set; }
        public DbSet<Recipient> Recipients { get; set; }
        public DbSet<Registry> Registries { get; set; }
        public DbSet<RegistryRules990> RegistryRules990 { get; set; }
        public DbSet<RegistryRules990Prepared> RegistryRules990Prepared { get; set; }
        public DbSet<RegistryRulesStatement> RegistryRulesStatements { get; set; }
        public DbSet<RegistryRulesStatementsPrepared> RegistryRulesStatementsPrepareds { get; set; }
        public DbSet<RequestDonor> RequestDonors { get; set; }
        public DbSet<RequestDonorsStep> RequestDonorsSteps { get; set; }
        public DbSet<ReservedName> ReservedNames { get; set; }
        public DbSet<RightsGroup> RightsGroups { get; set; }
        public DbSet<SatelliteDocument> SatelliteDocuments { get; set; }
        public DbSet<ServicePackage> ServicePackages { get; set; }
        public DbSet<ServicePackageService> ServicePackageServices { get; set; }
        public DbSet<ServiceProvider> ServiceProviders { get; set; }
        public DbSet<ServiceUseCas> ServiceUseCases { get; set; }
        public DbSet<ServiceUseCaseToStaticPermission> ServiceUseCaseToStaticPermissions { get; set; }
        public DbSet<ShellCorporation> ShellCorporations { get; set; }
        public DbSet<ShellCorporationsStep> ShellCorporationsSteps { get; set; }
        public DbSet<ShellNumber> ShellNumbers { get; set; }
        public DbSet<StaffingPolicy> StaffingPolicies { get; set; }
        public DbSet<StaffingPoliciesTemplate> StaffingPoliciesTemplates { get; set; }
        public DbSet<StaffingProposal> StaffingProposals { get; set; }
        public DbSet<Stamp> Stamps { get; set; }
        public DbSet<StaticPermission> StaticPermissions { get; set; }
        public DbSet<StmtTracking> StmtTrackings { get; set; }
        public DbSet<StructuredCompany> StructuredCompanies { get; set; }
        public DbSet<StructuredCompanyType> StructuredCompanyTypes { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<SubstantialContributor> SubstantialContributors { get; set; }
        public DbSet<Subtask> Subtasks { get; set; }
        public DbSet<Successor> Successors { get; set; }
        public DbSet<SummaryAccount> SummaryAccounts { get; set; }
        public DbSet<SystemPosition> SystemPositions { get; set; }
        public DbSet<SystemRecipient> SystemRecipients { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Tasks_Old> Tasks_Old { get; set; }
        public DbSet<Tax> Taxes { get; set; }
        public DbSet<TaxesSnapshot> TaxesSnapshots { get; set; }
        public DbSet<TaxesSnapshotInstance> TaxesSnapshotInstances { get; set; }
        public DbSet<TaxesStep> TaxesSteps { get; set; }
        public DbSet<TaxReceipt> TaxReceipts { get; set; }
        public DbSet<TaxReturnDocument> TaxReturnDocuments { get; set; }
        public DbSet<TaxReturnForm> TaxReturnForms { get; set; }
        public DbSet<TaxReturnIssue> TaxReturnIssues { get; set; }
        public DbSet<TaxReturnIssuesStep> TaxReturnIssuesSteps { get; set; }
        public DbSet<TaxReturnReport> TaxReturnReports { get; set; }
        public DbSet<TaxReturn> TaxReturns { get; set; }
        public DbSet<TaxReturnsStep> TaxReturnsSteps { get; set; }
        public DbSet<TaxReturnState> TaxReturnStates { get; set; }
        public DbSet<temp_foundation_data> temp_foundation_data { get; set; }
        public DbSet<TimeSchedule> TimeSchedules { get; set; }
        public DbSet<tmp_GrantHistory> tmp_GrantHistory { get; set; }
        public DbSet<tmp_GrantsImport> tmp_GrantsImport { get; set; }
        public DbSet<ToDo> ToDoes { get; set; }
        public DbSet<ToDoNote> ToDoNotes { get; set; }
        public DbSet<TransactNote> TransactNotes { get; set; }
        public DbSet<Transact> Transacts { get; set; }
        public DbSet<TransactsDonor> TransactsDonors { get; set; }
        public DbSet<TransactType> TransactTypes { get; set; }
        public DbSet<TransferContact> TransferContacts { get; set; }
        public DbSet<TransferGroup> TransferGroups { get; set; }
        public DbSet<TransferProfile> TransferProfiles { get; set; }
        public DbSet<TransferSchedule> TransferSchedules { get; set; }
        public DbSet<TrHoldingMacro> TrHoldingMacros { get; set; }
        public DbSet<TrHoldingMap> TrHoldingMaps { get; set; }
        public DbSet<TrHoldingRule> TrHoldingRules { get; set; }
        public DbSet<TrRegistryMap> TrRegistryMaps { get; set; }
        public DbSet<TrRegistryRule> TrRegistryRules { get; set; }
        public DbSet<TrRegistryRulesCompiled> TrRegistryRulesCompileds { get; set; }
        public DbSet<TrustedAction> TrustedActions { get; set; }
        public DbSet<UnallocatedSharesStep> UnallocatedSharesSteps { get; set; }
        public DbSet<UpdatesLog> UpdatesLogs { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<VotingOfferItem> VotingOfferItems { get; set; }
        public DbSet<VotingOfferResult> VotingOfferResults { get; set; }
        public DbSet<VotingOffer> VotingOffers { get; set; }
        public DbSet<VotingResult> VotingResults { get; set; }
        public DbSet<VotingResultsStep> VotingResultsSteps { get; set; }
        public DbSet<Voting> Votings { get; set; }
        public DbSet<YahooQuery> YahooQueries { get; set; }
        public DbSet<YearRangeDate> YearRangeDates { get; set; }
        public DbSet<MQ> MQs { get; set; }
        public DbSet<MQin> MQins { get; set; }
        public DbSet<MQout> MQouts { get; set; }
        public DbSet<CompanyPositionsRight> CompanyPositionsRights { get; set; }
        public DbSet<DepartmentsRight> DepartmentsRights { get; set; }
        public DbSet<PersonsRight> PersonsRights { get; set; }
        public DbSet<StaffingPoliciesRight> StaffingPoliciesRights { get; set; }
        public DbSet<StaffingPoliciesTemplatesRight> StaffingPoliciesTemplatesRights { get; set; }
        public DbSet<StructuredCompaniesRight> StructuredCompaniesRights { get; set; }
        public DbSet<StructuredCompanyTypesRight> StructuredCompanyTypesRights { get; set; }
        public DbSet<SystemPositionsRight> SystemPositionsRights { get; set; }
        public DbSet<ObjectsActionsLog> ObjectsActionsLogs { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Rounding> Roundings { get; set; }
        public DbSet<C_dta_CrmTasks> C_dta_CrmTasks { get; set; }
        public DbSet<ActivePricingsOfQuaterlyFeeFromSnapshot> ActivePricingsOfQuaterlyFeeFromSnapshots { get; set; }
        public DbSet<ActiveSetUpFeePricingSchedule> ActiveSetUpFeePricingSchedules { get; set; }
        public DbSet<AllFinancialPartner> AllFinancialPartners { get; set; }
        public DbSet<AllTenOuFoundation> AllTenOuFoundations { get; set; }
        public DbSet<AnnualTax> AnnualTaxes { get; set; }
        public DbSet<AssetAcquired> AssetAcquireds { get; set; }
        public DbSet<AssetsDetail> AssetsDetails { get; set; }
        public DbSet<AssetValuationsDetail> AssetValuationsDetails { get; set; }
        public DbSet<BalancesOfQuaterlyFeeFromSnapshot> BalancesOfQuaterlyFeeFromSnapshots { get; set; }
        public DbSet<Charity> Charities { get; set; }
        public DbSet<CharitiesEdit> CharitiesEdits { get; set; }
        public DbSet<DistributableAmountValue> DistributableAmountValues { get; set; }
        public DbSet<DocumentsDetail> DocumentsDetails { get; set; }
        public DbSet<enum_CRPendings> enum_CRPendings { get; set; }
        public DbSet<enum_SystemAddresses> enum_SystemAddresses { get; set; }
        public DbSet<enums_Appointment> enums_Appointment { get; set; }
        public DbSet<enums_AssetTypes> enums_AssetTypes { get; set; }
        public DbSet<enums_BillingEvents> enums_BillingEvents { get; set; }
        public DbSet<enums_CalcMethods> enums_CalcMethods { get; set; }
        public DbSet<enums_CharityRequestSteps> enums_CharityRequestSteps { get; set; }
        public DbSet<enums_ContactUsSteps> enums_ContactUsSteps { get; set; }
        public DbSet<enums_CSContactTypes> enums_CSContactTypes { get; set; }
        public DbSet<enums_Designs> enums_Designs { get; set; }
        public DbSet<enums_DonationsSteps> enums_DonationsSteps { get; set; }
        public DbSet<enums_EmailBelonging> enums_EmailBelonging { get; set; }
        public DbSet<enums_ExpenseCategories> enums_ExpenseCategories { get; set; }
        public DbSet<enums_ExpenseTypes> enums_ExpenseTypes { get; set; }
        public DbSet<enums_ForClient> enums_ForClient { get; set; }
        public DbSet<enums_FoundationAccountAccess> enums_FoundationAccountAccess { get; set; }
        public DbSet<enums_FoundationAccountFrequency> enums_FoundationAccountFrequency { get; set; }
        public DbSet<enums_FoundationAccountStatements> enums_FoundationAccountStatements { get; set; }
        public DbSet<enums_FoundationAccountType> enums_FoundationAccountType { get; set; }
        public DbSet<enums_OnBehalfActions> enums_OnBehalfActions { get; set; }
        public DbSet<enums_PaymentTypes> enums_PaymentTypes { get; set; }
        public DbSet<enums_PendingTransactSteps> enums_PendingTransactSteps { get; set; }
        public DbSet<enums_Position> enums_Position { get; set; }
        public DbSet<enums_PricingScheduleTypes> enums_PricingScheduleTypes { get; set; }
        public DbSet<enums_PrintMaterialsSteps> enums_PrintMaterialsSteps { get; set; }
        public DbSet<enums_ReferralSource> enums_ReferralSource { get; set; }
        public DbSet<enums_RegistryRules> enums_RegistryRules { get; set; }
        public DbSet<enums_RegistryTypes> enums_RegistryTypes { get; set; }
        public DbSet<enums_RequestDonorsSteps> enums_RequestDonorsSteps { get; set; }
        public DbSet<enums_RequestFrom> enums_RequestFrom { get; set; }
        public DbSet<enums_ShellFounders> enums_ShellFounders { get; set; }
        public DbSet<enums_Signers> enums_Signers { get; set; }
        public DbSet<enums_TaxCalcMethods> enums_TaxCalcMethods { get; set; }
        public DbSet<enums_TaxReturnFilingMethod> enums_TaxReturnFilingMethod { get; set; }
        public DbSet<enums_TaxReturnIssueTypes> enums_TaxReturnIssueTypes { get; set; }
        public DbSet<enums_TaxReturnPriorities> enums_TaxReturnPriorities { get; set; }
        public DbSet<enums_TaxReturnState3> enums_TaxReturnState3 { get; set; }
        public DbSet<enums_TaxReturnState4> enums_TaxReturnState4 { get; set; }
        public DbSet<enums_TaxReturnSteps> enums_TaxReturnSteps { get; set; }
        public DbSet<enums_TransactGroups> enums_TransactGroups { get; set; }
        public DbSet<enums_TransactionTypeXIn> enums_TransactionTypeXIn { get; set; }
        public DbSet<enums_TransactionTypeXOut> enums_TransactionTypeXOut { get; set; }
        public DbSet<enums_TransactTypes> enums_TransactTypes { get; set; }
        public DbSet<enums_ValuationTypes> enums_ValuationTypes { get; set; }
        public DbSet<et_AdvisorRoleKeepers> et_AdvisorRoleKeepers { get; set; }
        public DbSet<et_Charities> et_Charities { get; set; }
        public DbSet<et_CharitiesEdit> et_CharitiesEdit { get; set; }
        public DbSet<et_CharityRequests> et_CharityRequests { get; set; }
        public DbSet<et_Contacts> et_Contacts { get; set; }
        public DbSet<et_DonationCertificates> et_DonationCertificates { get; set; }
        public DbSet<et_DonationProposals> et_DonationProposals { get; set; }
        public DbSet<et_Donations> et_Donations { get; set; }
        public DbSet<et_Entities> et_Entities { get; set; }
        public DbSet<et_EstimatedTaxes> et_EstimatedTaxes { get; set; }
        public DbSet<et_FeesTaxes> et_FeesTaxes { get; set; }
        public DbSet<et_FinancialAdvisorGroups> et_FinancialAdvisorGroups { get; set; }
        public DbSet<et_FinancialPartners> et_FinancialPartners { get; set; }
        public DbSet<et_FoundationAccounts> et_FoundationAccounts { get; set; }
        public DbSet<et_Foundations> et_Foundations { get; set; }
        public DbSet<et_Grants> et_Grants { get; set; }
        public DbSet<et_IrsFiles> et_IrsFiles { get; set; }
        public DbSet<et_MailApplies> et_MailApplies { get; set; }
        public DbSet<et_Nominees> et_Nominees { get; set; }
        public DbSet<et_Persons> et_Persons { get; set; }
        public DbSet<et_PostalAddresses> et_PostalAddresses { get; set; }
        public DbSet<et_RoleKeepers> et_RoleKeepers { get; set; }
        public DbSet<et_StaffingProposals> et_StaffingProposals { get; set; }
        public DbSet<ExpenseDetail> ExpenseDetails { get; set; }
        public DbSet<FinancialPartner> FinancialPartners { get; set; }
        public DbSet<GrantHistory> GrantHistories { get; set; }
        public DbSet<HouseholdAccountDetail> HouseholdAccountDetails { get; set; }
        public DbSet<IncompleteAssetBook> IncompleteAssetBooks { get; set; }
        public DbSet<IRSFile> IRSFiles { get; set; }
        public DbSet<LocalAssetsDetail> LocalAssetsDetails { get; set; }
        public DbSet<Nominee> Nominees { get; set; }
        public DbSet<old_vFinancialPartners> old_vFinancialPartners { get; set; }
        public DbSet<PartnershipTransactsSummary> PartnershipTransactsSummaries { get; set; }
        public DbSet<PendingTransactDetail> PendingTransactDetails { get; set; }
        public DbSet<PrimaryFinancialAdvisor> PrimaryFinancialAdvisors { get; set; }
        public DbSet<qd_FAddresses> qd_FAddresses { get; set; }
        public DbSet<qd_FPersons> qd_FPersons { get; set; }
        public DbSet<qd_YNQuestions> qd_YNQuestions { get; set; }
        public DbSet<QualifyingExpendituresInProcess> QualifyingExpendituresInProcesses { get; set; }
        public DbSet<QuestSummary> QuestSummaries { get; set; }
        public DbSet<Registry990p> Registry990p { get; set; }
        public DbSet<Registry990p1> Registry990p1 { get; set; }
        public DbSet<Registry990p1b> Registry990p1b { get; set; }
        public DbSet<Registry990p1d> Registry990p1d { get; set; }
        public DbSet<Registry990p1e> Registry990p1e { get; set; }
        public DbSet<Registry990p1m> Registry990p1m { get; set; }
        public DbSet<Registry990p2> Registry990p2 { get; set; }
        public DbSet<Registry990p2c> Registry990p2c { get; set; }
        public DbSet<Registry990p3> Registry990p3 { get; set; }
        public DbSet<Registry990pX> Registry990pX { get; set; }
        public DbSet<RegistryEBP> RegistryEBPs { get; set; }
        public DbSet<RegistryStatement> RegistryStatements { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<TaxesDetail> TaxesDetails { get; set; }
        public DbSet<TenOuFoundation> TenOuFoundations { get; set; }
        public DbSet<TransactDetail> TransactDetails { get; set; }
        public DbSet<UnAllocatedCostBasi> UnAllocatedCostBasis { get; set; }
        public DbSet<v_Department_Foundation> v_Department_Foundation { get; set; }
        public DbSet<vActiveCompanyPerson> vActiveCompanyPersons { get; set; }
        public DbSet<vActiveCompanyPosition> vActiveCompanyPositions { get; set; }
        public DbSet<vActiveContractPlayer> vActiveContractPlayers { get; set; }
        public DbSet<vActiveContract> vActiveContracts { get; set; }
        public DbSet<vActiveContractService> vActiveContractServices { get; set; }
        public DbSet<vActiveDepartment> vActiveDepartments { get; set; }
        public DbSet<vActiveFoundationOfficer> vActiveFoundationOfficers { get; set; }
        public DbSet<vActiveFsContract> vActiveFsContracts { get; set; }
        public DbSet<vActivePhilanthropicDirector> vActivePhilanthropicDirectors { get; set; }
        public DbSet<vActiveRelationshipManager> vActiveRelationshipManagers { get; set; }
        public DbSet<vActiveSalesRepresentative> vActiveSalesRepresentatives { get; set; }
        public DbSet<vActiveStaffingPolicy> vActiveStaffingPolicies { get; set; }
        public DbSet<vAnnualContributedAmount> vAnnualContributedAmounts { get; set; }
        public DbSet<vAssetsBalanceDetail> vAssetsBalanceDetails { get; set; }
        public DbSet<vCheckingAccount> vCheckingAccounts { get; set; }
        public DbSet<vCheckRegister> vCheckRegisters { get; set; }
        public DbSet<vCompanyElementsWithType> vCompanyElementsWithTypes { get; set; }
        public DbSet<vCompanyPosition> vCompanyPositions { get; set; }
        public DbSet<vContractsDetail> vContractsDetails { get; set; }
        public DbSet<vContributedAmount> vContributedAmounts { get; set; }
        public DbSet<vContributedAmountsSUM> vContributedAmountsSUMs { get; set; }
        public DbSet<vCsPortalTask> vCsPortalTasks { get; set; }
        public DbSet<vDepartment> vDepartments { get; set; }
        public DbSet<vDisbursementAccount> vDisbursementAccounts { get; set; }
        public DbSet<vDocuments8566> vDocuments8566 { get; set; }
        public DbSet<vDonorDetail> vDonorDetails { get; set; }
        public DbSet<vExpenseHistory> vExpenseHistories { get; set; }
        public DbSet<vFinancialPartnerAccount> vFinancialPartnerAccounts { get; set; }
        public DbSet<vFinancialPartner> vFinancialPartners { get; set; }
        public DbSet<vFoundationContract> vFoundationContracts { get; set; }
        public DbSet<vFoundationEmailTemplate> vFoundationEmailTemplates { get; set; }
        public DbSet<vFoundationGeneralInfo> vFoundationGeneralInfoes { get; set; }
        public DbSet<vFoundationServiceProvider> vFoundationServiceProviders { get; set; }
        public DbSet<vGlobalEmailTemplate> vGlobalEmailTemplates { get; set; }
        public DbSet<vGrantActivity> vGrantActivities { get; set; }
        public DbSet<vGrantDetail> vGrantDetails { get; set; }
        public DbSet<vGrantHistory> vGrantHistories { get; set; }
        public DbSet<vGrantID> vGrantIDs { get; set; }
        public DbSet<vGrantingActivity> vGrantingActivities { get; set; }
        public DbSet<vGrant> vGrants { get; set; }
        public DbSet<vGroupEmailTemplate> vGroupEmailTemplates { get; set; }
        public DbSet<vHistoryInfo> vHistoryInfoes { get; set; }
        public DbSet<ViewPrintReceipt> ViewPrintReceipts { get; set; }
        public DbSet<vMainContract> vMainContracts { get; set; }
        public DbSet<vObjectsPermissionsWithObject> vObjectsPermissionsWithObjects { get; set; }
        public DbSet<vOnlineStaffingProposal> vOnlineStaffingProposals { get; set; }
        public DbSet<vPaidExpens> vPaidExpenses { get; set; }
        public DbSet<vPartnerEmailTemplate> vPartnerEmailTemplates { get; set; }
        public DbSet<vProject> vProjects { get; set; }
        public DbSet<vQuestAllHasIssue> vQuestAllHasIssues { get; set; }
        public DbSet<vQuestEachHasIssue> vQuestEachHasIssues { get; set; }
        public DbSet<vRelationshipManager> vRelationshipManagers { get; set; }
        public DbSet<vRelationshipManagersFoundation> vRelationshipManagersFoundations { get; set; }
        public DbSet<vServiceProvider> vServiceProviders { get; set; }
        public DbSet<vShellStatu> vShellStatus { get; set; }
        public DbSet<vStaffingPolicy> vStaffingPolicies { get; set; }
        public DbSet<vStructuredCompany> vStructuredCompanies { get; set; }
        public DbSet<vTaxReturnState> vTaxReturnStates { get; set; }
        public DbSet<vTransactsDonor> vTransactsDonors { get; set; }
        public DbSet<vTransactType> vTransactTypes { get; set; }
        public DbSet<vTransferDetail> vTransferDetails { get; set; }
        public DbSet<vUnassignedDeposit> vUnassignedDeposits { get; set; }
        public DbSet<TaxReturnDetail> TaxReturnDetails { get; set; }
        public DbSet<vChangedTaxReturnsStatus> vChangedTaxReturnsStatuses { get; set; }
        public DbSet<ContractsService> ContractsServices { get; set; }
        public DbSet<PersonsNative> PersonsNatives { get; set; }
        public DbSet<enums_EstimatedTaxIssueTypes> enums_EstimatedTaxIssueTypes { get; set; }
        public DbSet<enums_EstimatedTaxSteps> enums_EstimatedTaxSteps { get; set; }
        public DbSet<EstimatedTaxIssuesDetail> EstimatedTaxIssuesDetails { get; set; }
        public DbSet<v_CombinedTaxQ1Q2> v_CombinedTaxQ1Q2 { get; set; }
        public DbSet<v_IncomesFromSnapshot> v_IncomesFromSnapshot { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AbbreviateAddressMap());
            modelBuilder.Configurations.Add(new ACHTransactMap());
            modelBuilder.Configurations.Add(new ActionsLogMap());
            modelBuilder.Configurations.Add(new ActivityCodeMap());
            modelBuilder.Configurations.Add(new ActivityLogItemMap());
            modelBuilder.Configurations.Add(new ActivityLogMap());
            modelBuilder.Configurations.Add(new ActivityPricingMap());
            modelBuilder.Configurations.Add(new AnnualPayoutMap());
            modelBuilder.Configurations.Add(new AspSessionMap());
            modelBuilder.Configurations.Add(new AssetBookMap());
            modelBuilder.Configurations.Add(new AssetPriceMap());
            modelBuilder.Configurations.Add(new AssetPricesTempMap());
            modelBuilder.Configurations.Add(new AssetMap());
            modelBuilder.Configurations.Add(new AssetValuationMap());
            modelBuilder.Configurations.Add(new BAConfigMap());
            modelBuilder.Configurations.Add(new bak_ActivityPricingsMap());
            modelBuilder.Configurations.Add(new bak_FeePricingsMap());
            modelBuilder.Configurations.Add(new bak_PricingScheduleInstancesMap());
            modelBuilder.Configurations.Add(new bak_PricingSchedulesMap());
            modelBuilder.Configurations.Add(new bak_ServicePackagesMap());
            modelBuilder.Configurations.Add(new bak_ServicePackageServicesMap());
            modelBuilder.Configurations.Add(new BanksOperatorMap());
            modelBuilder.Configurations.Add(new BATempGrantListMap());
            modelBuilder.Configurations.Add(new BogusSSNMap());
            modelBuilder.Configurations.Add(new CarryforwardDetailMap());
            modelBuilder.Configurations.Add(new CategoryMap());
            modelBuilder.Configurations.Add(new CertifiedMailMap());
            modelBuilder.Configurations.Add(new CharityRequestMap());
            modelBuilder.Configurations.Add(new CharityRequestsStepMap());
            modelBuilder.Configurations.Add(new CharityResponsMap());
            modelBuilder.Configurations.Add(new CheckingAccountMap());
            modelBuilder.Configurations.Add(new CheckPayeeMap());
            modelBuilder.Configurations.Add(new CheckMap());
            modelBuilder.Configurations.Add(new CMSBrochureMap());
            modelBuilder.Configurations.Add(new CMSContactMap());
            modelBuilder.Configurations.Add(new CMSEventMap());
            modelBuilder.Configurations.Add(new CMSFaqMap());
            modelBuilder.Configurations.Add(new CMSManagerMap());
            modelBuilder.Configurations.Add(new CMSNewMap());
            modelBuilder.Configurations.Add(new CMSPublicationMap());
            modelBuilder.Configurations.Add(new CompanyMap());
            modelBuilder.Configurations.Add(new CompanyElementMap());
            modelBuilder.Configurations.Add(new CompanyPersonMap());
            modelBuilder.Configurations.Add(new CompanyPositionMap());
            modelBuilder.Configurations.Add(new ContactMap());
            modelBuilder.Configurations.Add(new ContactsStepMap());
            modelBuilder.Configurations.Add(new ContactsStepsContactMap());
            modelBuilder.Configurations.Add(new ContactsStepsNoteMap());
            modelBuilder.Configurations.Add(new ContractPlayerRoleMap());
            modelBuilder.Configurations.Add(new ContractPlayerMap());
            modelBuilder.Configurations.Add(new ContractPricingMap());
            modelBuilder.Configurations.Add(new ContractMap());
            modelBuilder.Configurations.Add(new ContractServicePackageMap());
            modelBuilder.Configurations.Add(new ContractServiceMap());
            modelBuilder.Configurations.Add(new CostBasMap());
            modelBuilder.Configurations.Add(new CreditCardMap());
            modelBuilder.Configurations.Add(new CRMTaskMap());
            modelBuilder.Configurations.Add(new CRMTasksStepMap());
            modelBuilder.Configurations.Add(new CrossingMap());
            modelBuilder.Configurations.Add(new CTL_ErrorsMap());
            modelBuilder.Configurations.Add(new CurrentBalanceMap());
            modelBuilder.Configurations.Add(new CustomReportCategoryMap());
            modelBuilder.Configurations.Add(new CustomReportMap());
            modelBuilder.Configurations.Add(new CustomReportsTemplateMap());
            modelBuilder.Configurations.Add(new DefaultCharityMap());
            modelBuilder.Configurations.Add(new DepartmentMap());
            modelBuilder.Configurations.Add(new DesignVotingOfferItemMap());
            modelBuilder.Configurations.Add(new DisbursementAccountMap());
            modelBuilder.Configurations.Add(new DisbursementAccountSubscriberMap());
            modelBuilder.Configurations.Add(new DistributableAmountMap());
            modelBuilder.Configurations.Add(new DistributableAmountsTempMap());
            modelBuilder.Configurations.Add(new DocumentMap());
            modelBuilder.Configurations.Add(new Document1Map());
            modelBuilder.Configurations.Add(new DocumentTemplateMap());
            modelBuilder.Configurations.Add(new DocumentViewMap());
            modelBuilder.Configurations.Add(new DonationCertificateMap());
            modelBuilder.Configurations.Add(new DonationRequestMap());
            modelBuilder.Configurations.Add(new DonationsLogMap());
            modelBuilder.Configurations.Add(new DonorMap());
            modelBuilder.Configurations.Add(new dtpropertyMap());
            modelBuilder.Configurations.Add(new EFTPSConfirmationMap());
            modelBuilder.Configurations.Add(new EFTPSConfReportMap());
            modelBuilder.Configurations.Add(new EFTPSEnrollmentMap());
            modelBuilder.Configurations.Add(new EFTPSEnrollmentsStepMap());
            modelBuilder.Configurations.Add(new EFTPSPaymentMap());
            modelBuilder.Configurations.Add(new EFTMap());
            modelBuilder.Configurations.Add(new EMailNotifyTransactMap());
            modelBuilder.Configurations.Add(new EmailRecipientMap());
            modelBuilder.Configurations.Add(new EMailMap());
            modelBuilder.Configurations.Add(new EmailTemplateMap());
            modelBuilder.Configurations.Add(new EnumMap());
            modelBuilder.Configurations.Add(new EnumsCategoryMap());
            modelBuilder.Configurations.Add(new EnumsTextMap());
            modelBuilder.Configurations.Add(new EstimatedTaxMap());
            modelBuilder.Configurations.Add(new EstimatedTaxesIssueMap());
            modelBuilder.Configurations.Add(new EstimatedTaxesIssuesStepMap());
            modelBuilder.Configurations.Add(new EstimatedTaxesStepMap());
            modelBuilder.Configurations.Add(new EventActionMap());
            modelBuilder.Configurations.Add(new EventClassMap());
            modelBuilder.Configurations.Add(new EventRecipientMap());
            modelBuilder.Configurations.Add(new EventMap());
            modelBuilder.Configurations.Add(new EventSourceMap());
            modelBuilder.Configurations.Add(new EventTypeMap());
            modelBuilder.Configurations.Add(new ExcludedEmailApplicantMap());
            modelBuilder.Configurations.Add(new ExpenseEntryMap());
            modelBuilder.Configurations.Add(new ExpenseLocationMap());
            modelBuilder.Configurations.Add(new ExpenseOfficerMap());
            modelBuilder.Configurations.Add(new ExpenseRangeMap());
            modelBuilder.Configurations.Add(new ExpenseReportMap());
            modelBuilder.Configurations.Add(new ExpensMap());
            modelBuilder.Configurations.Add(new ExpensesStepMap());
            modelBuilder.Configurations.Add(new FAPBrochureMap());
            modelBuilder.Configurations.Add(new FAPPackageFormMap());
            modelBuilder.Configurations.Add(new FAPPackageRequestMap());
            modelBuilder.Configurations.Add(new FAPPackageMap());
            modelBuilder.Configurations.Add(new FAPTrainingMap());
            modelBuilder.Configurations.Add(new FAPTrainingRegisteredMap());
            modelBuilder.Configurations.Add(new FavoriteCharityMap());
            modelBuilder.Configurations.Add(new FeeCalculationMap());
            modelBuilder.Configurations.Add(new FeePricingMap());
            modelBuilder.Configurations.Add(new FinancialAdvisorGroupMap());
            modelBuilder.Configurations.Add(new FinancialAdvisorMap());
            modelBuilder.Configurations.Add(new FinancialAdvisorsFoundationMap());
            modelBuilder.Configurations.Add(new FinancialAdvisorsRequestMap());
            modelBuilder.Configurations.Add(new FinancialAdvisorsRequestsStepMap());
            modelBuilder.Configurations.Add(new FinancialPartnerAccountMap());
            modelBuilder.Configurations.Add(new FinancialPartnerChunkMap());
            modelBuilder.Configurations.Add(new FinancialPartnerDocumentMap());
            modelBuilder.Configurations.Add(new FinancialStatementsPartMap());
            modelBuilder.Configurations.Add(new FoundationAccountMap());
            modelBuilder.Configurations.Add(new FoundationAccountsStepMap());
            modelBuilder.Configurations.Add(new FoundationEventMap());
            modelBuilder.Configurations.Add(new FoundationProjectMap());
            modelBuilder.Configurations.Add(new FoundationMap());
            modelBuilder.Configurations.Add(new FoundationsBoardMeetingMap());
            modelBuilder.Configurations.Add(new FoundationsCharityMap());
            modelBuilder.Configurations.Add(new FoundationsGrowthInfoMap());
            modelBuilder.Configurations.Add(new FoundationsRatingMap());
            modelBuilder.Configurations.Add(new FoundationsStepMap());
            modelBuilder.Configurations.Add(new FundingDocumentMap());
            modelBuilder.Configurations.Add(new FundingMap());
            modelBuilder.Configurations.Add(new GenericEmailMap());
            modelBuilder.Configurations.Add(new GenericTextMap());
            modelBuilder.Configurations.Add(new GlobalAssetMap());
            modelBuilder.Configurations.Add(new GlobalRecipientMap());
            modelBuilder.Configurations.Add(new GrantApplicationMap());
            modelBuilder.Configurations.Add(new GrantDetailMap());
            modelBuilder.Configurations.Add(new GrantingParamMap());
            modelBuilder.Configurations.Add(new GrantMessageMap());
            modelBuilder.Configurations.Add(new GrantProposalMap());
            modelBuilder.Configurations.Add(new GrantMap());
            modelBuilder.Configurations.Add(new GrantScheduleMap());
            modelBuilder.Configurations.Add(new GrantsStepMap());
            modelBuilder.Configurations.Add(new HistoricalGrantMap());
            modelBuilder.Configurations.Add(new HoldingMap());
            modelBuilder.Configurations.Add(new HouseholdAccountMap());
            modelBuilder.Configurations.Add(new HouseholdAccountsStepMap());
            modelBuilder.Configurations.Add(new HouseholdDocumentMap());
            modelBuilder.Configurations.Add(new HouseholdMemberMap());
            modelBuilder.Configurations.Add(new HouseholdValueMap());
            modelBuilder.Configurations.Add(new IndividualNoteMap());
            modelBuilder.Configurations.Add(new InvoiceMap());
            modelBuilder.Configurations.Add(new InvoicesItemMap());
            modelBuilder.Configurations.Add(new JobClassMap());
            modelBuilder.Configurations.Add(new JobMap());
            modelBuilder.Configurations.Add(new LinkRepositoryMap());
            modelBuilder.Configurations.Add(new LocalAssetMap());
            modelBuilder.Configurations.Add(new MailApplyMap());
            modelBuilder.Configurations.Add(new MailAppliesDocumentMap());
            modelBuilder.Configurations.Add(new MailAppliesSelectedDocumentMap());
            modelBuilder.Configurations.Add(new MailAppliesStepMap());
            modelBuilder.Configurations.Add(new MailAppliesStepsNoteMap());
            modelBuilder.Configurations.Add(new MailNotifyTransactMap());
            modelBuilder.Configurations.Add(new ManualPaymentMap());
            modelBuilder.Configurations.Add(new MeetingMap());
            modelBuilder.Configurations.Add(new MeetingsAttendedMap());
            modelBuilder.Configurations.Add(new MeetingsProposalMap());
            modelBuilder.Configurations.Add(new MeetingsStepMap());
            modelBuilder.Configurations.Add(new MessageMap());
            modelBuilder.Configurations.Add(new MoneyTransferTransactMap());
            modelBuilder.Configurations.Add(new NCodeMap());
            modelBuilder.Configurations.Add(new NoteCategoryMap());
            modelBuilder.Configurations.Add(new NoteMap());
            modelBuilder.Configurations.Add(new ObjectsPermissionMap());
            modelBuilder.Configurations.Add(new OfficersPositionMap());
            modelBuilder.Configurations.Add(new old_FinancialPartnersMap());
            modelBuilder.Configurations.Add(new old_NomineesMap());
            modelBuilder.Configurations.Add(new old_NomineesStepsMap());
            modelBuilder.Configurations.Add(new old_PendingTransactItemsMap());
            modelBuilder.Configurations.Add(new old_PendingTransactItemStepsMap());
            modelBuilder.Configurations.Add(new old_TaxesItemsMap());
            modelBuilder.Configurations.Add(new oldQuestionnaireMap());
            modelBuilder.Configurations.Add(new OLEClassMap());
            modelBuilder.Configurations.Add(new PartnershipAmountMap());
            modelBuilder.Configurations.Add(new PartnershipMap());
            modelBuilder.Configurations.Add(new PartnershipsStepMap());
            modelBuilder.Configurations.Add(new PartnersInfoMap());
            modelBuilder.Configurations.Add(new PartnersQuarterlyFeeMap());
            modelBuilder.Configurations.Add(new PartnersTextMap());
            modelBuilder.Configurations.Add(new PayeeProfileMap());
            modelBuilder.Configurations.Add(new PaymentMap());
            modelBuilder.Configurations.Add(new PaymentsBatchMap());
            modelBuilder.Configurations.Add(new PaymentsStepMap());
            modelBuilder.Configurations.Add(new PendingTransactMap());
            modelBuilder.Configurations.Add(new PendingTransactsStepMap());
            modelBuilder.Configurations.Add(new PersonFoundationMap());
            modelBuilder.Configurations.Add(new PersonNotificationMap());
            modelBuilder.Configurations.Add(new PersonRightsHistoryMap());
            modelBuilder.Configurations.Add(new PersonRoleMap());
            modelBuilder.Configurations.Add(new PersonRolesHistoryMap());
            modelBuilder.Configurations.Add(new PersonMap());
            modelBuilder.Configurations.Add(new PersonsPositionMap());
            modelBuilder.Configurations.Add(new PersonsStepMap());
            modelBuilder.Configurations.Add(new PhaseMap());
            modelBuilder.Configurations.Add(new PositionMap());
            modelBuilder.Configurations.Add(new PostalAddressMap());
            modelBuilder.Configurations.Add(new PricingScheduleInstanceMap());
            modelBuilder.Configurations.Add(new PricingScheduleMap());
            modelBuilder.Configurations.Add(new ProgramCodeMap());
            modelBuilder.Configurations.Add(new ProposalMap());
            modelBuilder.Configurations.Add(new ProposalsStepMap());
            modelBuilder.Configurations.Add(new ProposalStaffMap());
            modelBuilder.Configurations.Add(new QBalanceMap());
            modelBuilder.Configurations.Add(new QCapitalGainMap());
            modelBuilder.Configurations.Add(new qd_FAddresses_Map());
            modelBuilder.Configurations.Add(new qd_FPersons_Map());
            modelBuilder.Configurations.Add(new qd_YNQuestions_Map());
            modelBuilder.Configurations.Add(new QDepositMap());
            modelBuilder.Configurations.Add(new QHistoryTransactionMap());
            modelBuilder.Configurations.Add(new QModifyReportMap());
            modelBuilder.Configurations.Add(new QModifyTransactionMap());
            modelBuilder.Configurations.Add(new QParserMap());
            modelBuilder.Configurations.Add(new QPortfolioMap());
            modelBuilder.Configurations.Add(new QReportMap());
            modelBuilder.Configurations.Add(new QSecurityPriceMap());
            modelBuilder.Configurations.Add(new QTransactionMap());
            modelBuilder.Configurations.Add(new QueryMap());
            modelBuilder.Configurations.Add(new QueryCategoryMap());
            modelBuilder.Configurations.Add(new QuestionnaireMap());
            modelBuilder.Configurations.Add(new QuestionnairesStepMap());
            modelBuilder.Configurations.Add(new QuestSummaries_Map());
            modelBuilder.Configurations.Add(new RecentFoundationLookupMap());
            modelBuilder.Configurations.Add(new RecipientMap());
            modelBuilder.Configurations.Add(new RegistryMap());
            modelBuilder.Configurations.Add(new RegistryRules990Map());
            modelBuilder.Configurations.Add(new RegistryRules990PreparedMap());
            modelBuilder.Configurations.Add(new RegistryRulesStatementMap());
            modelBuilder.Configurations.Add(new RegistryRulesStatementsPreparedMap());
            modelBuilder.Configurations.Add(new RequestDonorMap());
            modelBuilder.Configurations.Add(new RequestDonorsStepMap());
            modelBuilder.Configurations.Add(new ReservedNameMap());
            modelBuilder.Configurations.Add(new RightsGroupMap());
            modelBuilder.Configurations.Add(new SatelliteDocumentMap());
            modelBuilder.Configurations.Add(new ServicePackageMap());
            modelBuilder.Configurations.Add(new ServicePackageServiceMap());
            modelBuilder.Configurations.Add(new ServiceProviderMap());
            modelBuilder.Configurations.Add(new ServiceUseCasMap());
            modelBuilder.Configurations.Add(new ServiceUseCaseToStaticPermissionMap());
            modelBuilder.Configurations.Add(new ShellCorporationMap());
            modelBuilder.Configurations.Add(new ShellCorporationsStepMap());
            modelBuilder.Configurations.Add(new ShellNumberMap());
            modelBuilder.Configurations.Add(new StaffingPolicyMap());
            modelBuilder.Configurations.Add(new StaffingPoliciesTemplateMap());
            modelBuilder.Configurations.Add(new StaffingProposalMap());
            modelBuilder.Configurations.Add(new StampMap());
            modelBuilder.Configurations.Add(new StaticPermissionMap());
            modelBuilder.Configurations.Add(new StmtTrackingMap());
            modelBuilder.Configurations.Add(new StructuredCompanyMap());
            modelBuilder.Configurations.Add(new StructuredCompanyTypeMap());
            modelBuilder.Configurations.Add(new SubCategoryMap());
            modelBuilder.Configurations.Add(new SubstantialContributorMap());
            modelBuilder.Configurations.Add(new SubtaskMap());
            modelBuilder.Configurations.Add(new SuccessorMap());
            modelBuilder.Configurations.Add(new SummaryAccountMap());
            modelBuilder.Configurations.Add(new SystemPositionMap());
            modelBuilder.Configurations.Add(new SystemRecipientMap());
            modelBuilder.Configurations.Add(new TaskMap());
            modelBuilder.Configurations.Add(new Tasks_OldMap());
            modelBuilder.Configurations.Add(new TaxMap());
            modelBuilder.Configurations.Add(new TaxesSnapshotMap());
            modelBuilder.Configurations.Add(new TaxesSnapshotInstanceMap());
            modelBuilder.Configurations.Add(new TaxesStepMap());
            modelBuilder.Configurations.Add(new TaxReceiptMap());
            modelBuilder.Configurations.Add(new TaxReturnDocumentMap());
            modelBuilder.Configurations.Add(new TaxReturnFormMap());
            modelBuilder.Configurations.Add(new TaxReturnIssueMap());
            modelBuilder.Configurations.Add(new TaxReturnIssuesStepMap());
            modelBuilder.Configurations.Add(new TaxReturnReportMap());
            modelBuilder.Configurations.Add(new TaxReturnMap());
            modelBuilder.Configurations.Add(new TaxReturnsStepMap());
            modelBuilder.Configurations.Add(new TaxReturnStateMap());
            modelBuilder.Configurations.Add(new temp_foundation_dataMap());
            modelBuilder.Configurations.Add(new TimeScheduleMap());
            modelBuilder.Configurations.Add(new tmp_GrantHistoryMap());
            modelBuilder.Configurations.Add(new tmp_GrantsImportMap());
            modelBuilder.Configurations.Add(new ToDoMap());
            modelBuilder.Configurations.Add(new ToDoNoteMap());
            modelBuilder.Configurations.Add(new TransactNoteMap());
            modelBuilder.Configurations.Add(new TransactMap());
            modelBuilder.Configurations.Add(new TransactsDonorMap());
            modelBuilder.Configurations.Add(new TransactTypeMap());
            modelBuilder.Configurations.Add(new TransferContactMap());
            modelBuilder.Configurations.Add(new TransferGroupMap());
            modelBuilder.Configurations.Add(new TransferProfileMap());
            modelBuilder.Configurations.Add(new TransferScheduleMap());
            modelBuilder.Configurations.Add(new TrHoldingMacroMap());
            modelBuilder.Configurations.Add(new TrHoldingMapMap());
            modelBuilder.Configurations.Add(new TrHoldingRuleMap());
            modelBuilder.Configurations.Add(new TrRegistryMapMap());
            modelBuilder.Configurations.Add(new TrRegistryRuleMap());
            modelBuilder.Configurations.Add(new TrRegistryRulesCompiledMap());
            modelBuilder.Configurations.Add(new TrustedActionMap());
            modelBuilder.Configurations.Add(new UnallocatedSharesStepMap());
            modelBuilder.Configurations.Add(new UpdatesLogMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new VotingOfferItemMap());
            modelBuilder.Configurations.Add(new VotingOfferResultMap());
            modelBuilder.Configurations.Add(new VotingOfferMap());
            modelBuilder.Configurations.Add(new VotingResultMap());
            modelBuilder.Configurations.Add(new VotingResultsStepMap());
            modelBuilder.Configurations.Add(new VotingMap());
            modelBuilder.Configurations.Add(new YahooQueryMap());
            modelBuilder.Configurations.Add(new YearRangeDateMap());
            modelBuilder.Configurations.Add(new MQMap());
            modelBuilder.Configurations.Add(new MQinMap());
            modelBuilder.Configurations.Add(new MQoutMap());
            modelBuilder.Configurations.Add(new CompanyPositionsRightMap());
            modelBuilder.Configurations.Add(new DepartmentsRightMap());
            modelBuilder.Configurations.Add(new PersonsRightMap());
            modelBuilder.Configurations.Add(new StaffingPoliciesRightMap());
            modelBuilder.Configurations.Add(new StaffingPoliciesTemplatesRightMap());
            modelBuilder.Configurations.Add(new StructuredCompaniesRightMap());
            modelBuilder.Configurations.Add(new StructuredCompanyTypesRightMap());
            modelBuilder.Configurations.Add(new SystemPositionsRightMap());
            modelBuilder.Configurations.Add(new ObjectsActionsLogMap());
            modelBuilder.Configurations.Add(new SessionMap());
            modelBuilder.Configurations.Add(new RoundingMap());
            modelBuilder.Configurations.Add(new C_dta_CrmTasksMap());
            modelBuilder.Configurations.Add(new ActivePricingsOfQuaterlyFeeFromSnapshotMap());
            modelBuilder.Configurations.Add(new ActiveSetUpFeePricingScheduleMap());
            modelBuilder.Configurations.Add(new AllFinancialPartnerMap());
            modelBuilder.Configurations.Add(new AllTenOuFoundationMap());
            modelBuilder.Configurations.Add(new AnnualTaxMap());
            modelBuilder.Configurations.Add(new AssetAcquiredMap());
            modelBuilder.Configurations.Add(new AssetsDetailMap());
            modelBuilder.Configurations.Add(new AssetValuationsDetailMap());
            modelBuilder.Configurations.Add(new BalancesOfQuaterlyFeeFromSnapshotMap());
            modelBuilder.Configurations.Add(new CharityMap());
            modelBuilder.Configurations.Add(new CharitiesEditMap());
            modelBuilder.Configurations.Add(new DistributableAmountValueMap());
            modelBuilder.Configurations.Add(new DocumentsDetailMap());
            modelBuilder.Configurations.Add(new enum_CRPendingsMap());
            modelBuilder.Configurations.Add(new enum_SystemAddressesMap());
            modelBuilder.Configurations.Add(new enums_AppointmentMap());
            modelBuilder.Configurations.Add(new enums_AssetTypesMap());
            modelBuilder.Configurations.Add(new enums_BillingEventsMap());
            modelBuilder.Configurations.Add(new enums_CalcMethodsMap());
            modelBuilder.Configurations.Add(new enums_CharityRequestStepsMap());
            modelBuilder.Configurations.Add(new enums_ContactUsStepsMap());
            modelBuilder.Configurations.Add(new enums_CSContactTypesMap());
            modelBuilder.Configurations.Add(new enums_DesignsMap());
            modelBuilder.Configurations.Add(new enums_DonationsStepsMap());
            modelBuilder.Configurations.Add(new enums_EmailBelongingMap());
            modelBuilder.Configurations.Add(new enums_ExpenseCategoriesMap());
            modelBuilder.Configurations.Add(new enums_ExpenseTypesMap());
            modelBuilder.Configurations.Add(new enums_ForClientMap());
            modelBuilder.Configurations.Add(new enums_FoundationAccountAccessMap());
            modelBuilder.Configurations.Add(new enums_FoundationAccountFrequencyMap());
            modelBuilder.Configurations.Add(new enums_FoundationAccountStatementsMap());
            modelBuilder.Configurations.Add(new enums_FoundationAccountTypeMap());
            modelBuilder.Configurations.Add(new enums_OnBehalfActionsMap());
            modelBuilder.Configurations.Add(new enums_PaymentTypesMap());
            modelBuilder.Configurations.Add(new enums_PendingTransactStepsMap());
            modelBuilder.Configurations.Add(new enums_PositionMap());
            modelBuilder.Configurations.Add(new enums_PricingScheduleTypesMap());
            modelBuilder.Configurations.Add(new enums_PrintMaterialsStepsMap());
            modelBuilder.Configurations.Add(new enums_ReferralSourceMap());
            modelBuilder.Configurations.Add(new enums_RegistryRulesMap());
            modelBuilder.Configurations.Add(new enums_RegistryTypesMap());
            modelBuilder.Configurations.Add(new enums_RequestDonorsStepsMap());
            modelBuilder.Configurations.Add(new enums_RequestFromMap());
            modelBuilder.Configurations.Add(new enums_ShellFoundersMap());
            modelBuilder.Configurations.Add(new enums_SignersMap());
            modelBuilder.Configurations.Add(new enums_TaxCalcMethodsMap());
            modelBuilder.Configurations.Add(new enums_TaxReturnFilingMethodMap());
            modelBuilder.Configurations.Add(new enums_TaxReturnIssueTypesMap());
            modelBuilder.Configurations.Add(new enums_TaxReturnPrioritiesMap());
            modelBuilder.Configurations.Add(new enums_TaxReturnState3Map());
            modelBuilder.Configurations.Add(new enums_TaxReturnState4Map());
            modelBuilder.Configurations.Add(new enums_TaxReturnStepsMap());
            modelBuilder.Configurations.Add(new enums_TransactGroupsMap());
            modelBuilder.Configurations.Add(new enums_TransactionTypeXInMap());
            modelBuilder.Configurations.Add(new enums_TransactionTypeXOutMap());
            modelBuilder.Configurations.Add(new enums_TransactTypesMap());
            modelBuilder.Configurations.Add(new enums_ValuationTypesMap());
            modelBuilder.Configurations.Add(new et_AdvisorRoleKeepersMap());
            modelBuilder.Configurations.Add(new et_CharitiesMap());
            modelBuilder.Configurations.Add(new et_CharitiesEditMap());
            modelBuilder.Configurations.Add(new et_CharityRequestsMap());
            modelBuilder.Configurations.Add(new et_ContactsMap());
            modelBuilder.Configurations.Add(new et_DonationCertificatesMap());
            modelBuilder.Configurations.Add(new et_DonationProposalsMap());
            modelBuilder.Configurations.Add(new et_DonationsMap());
            modelBuilder.Configurations.Add(new et_EntitiesMap());
            modelBuilder.Configurations.Add(new et_EstimatedTaxesMap());
            modelBuilder.Configurations.Add(new et_FeesTaxesMap());
            modelBuilder.Configurations.Add(new et_FinancialAdvisorGroupsMap());
            modelBuilder.Configurations.Add(new et_FinancialPartnersMap());
            modelBuilder.Configurations.Add(new et_FoundationAccountsMap());
            modelBuilder.Configurations.Add(new et_FoundationsMap());
            modelBuilder.Configurations.Add(new et_GrantsMap());
            modelBuilder.Configurations.Add(new et_IrsFilesMap());
            modelBuilder.Configurations.Add(new et_MailAppliesMap());
            modelBuilder.Configurations.Add(new et_NomineesMap());
            modelBuilder.Configurations.Add(new et_PersonsMap());
            modelBuilder.Configurations.Add(new et_PostalAddressesMap());
            modelBuilder.Configurations.Add(new et_RoleKeepersMap());
            modelBuilder.Configurations.Add(new et_StaffingProposalsMap());
            modelBuilder.Configurations.Add(new ExpenseDetailMap());
            modelBuilder.Configurations.Add(new FinancialPartnerMap());
            modelBuilder.Configurations.Add(new GrantHistoryMap());
            modelBuilder.Configurations.Add(new HouseholdAccountDetailMap());
            modelBuilder.Configurations.Add(new IncompleteAssetBookMap());
            modelBuilder.Configurations.Add(new IRSFileMap());
            modelBuilder.Configurations.Add(new LocalAssetsDetailMap());
            modelBuilder.Configurations.Add(new NomineeMap());
            modelBuilder.Configurations.Add(new old_vFinancialPartnersMap());
            modelBuilder.Configurations.Add(new PartnershipTransactsSummaryMap());
            modelBuilder.Configurations.Add(new PendingTransactDetailMap());
            modelBuilder.Configurations.Add(new PrimaryFinancialAdvisorMap());
            modelBuilder.Configurations.Add(new qd_FAddressesMap());
            modelBuilder.Configurations.Add(new qd_FPersonsMap());
            modelBuilder.Configurations.Add(new qd_YNQuestionsMap());
            modelBuilder.Configurations.Add(new QualifyingExpendituresInProcessMap());
            modelBuilder.Configurations.Add(new QuestSummaryMap());
            modelBuilder.Configurations.Add(new Registry990pMap());
            modelBuilder.Configurations.Add(new Registry990p1Map());
            modelBuilder.Configurations.Add(new Registry990p1bMap());
            modelBuilder.Configurations.Add(new Registry990p1dMap());
            modelBuilder.Configurations.Add(new Registry990p1eMap());
            modelBuilder.Configurations.Add(new Registry990p1mMap());
            modelBuilder.Configurations.Add(new Registry990p2Map());
            modelBuilder.Configurations.Add(new Registry990p2cMap());
            modelBuilder.Configurations.Add(new Registry990p3Map());
            modelBuilder.Configurations.Add(new Registry990pXMap());
            modelBuilder.Configurations.Add(new RegistryEBPMap());
            modelBuilder.Configurations.Add(new RegistryStatementMap());
            modelBuilder.Configurations.Add(new ServiceMap());
            modelBuilder.Configurations.Add(new TaxesDetailMap());
            modelBuilder.Configurations.Add(new TenOuFoundationMap());
            modelBuilder.Configurations.Add(new TransactDetailMap());
            modelBuilder.Configurations.Add(new UnAllocatedCostBasiMap());
            modelBuilder.Configurations.Add(new v_Department_FoundationMap());
            modelBuilder.Configurations.Add(new vActiveCompanyPersonMap());
            modelBuilder.Configurations.Add(new vActiveCompanyPositionMap());
            modelBuilder.Configurations.Add(new vActiveContractPlayerMap());
            modelBuilder.Configurations.Add(new vActiveContractMap());
            modelBuilder.Configurations.Add(new vActiveContractServiceMap());
            modelBuilder.Configurations.Add(new vActiveDepartmentMap());
            modelBuilder.Configurations.Add(new vActiveFoundationOfficerMap());
            modelBuilder.Configurations.Add(new vActiveFsContractMap());
            modelBuilder.Configurations.Add(new vActivePhilanthropicDirectorMap());
            modelBuilder.Configurations.Add(new vActiveRelationshipManagerMap());
            modelBuilder.Configurations.Add(new vActiveSalesRepresentativeMap());
            modelBuilder.Configurations.Add(new vActiveStaffingPolicyMap());
            modelBuilder.Configurations.Add(new vAnnualContributedAmountMap());
            modelBuilder.Configurations.Add(new vAssetsBalanceDetailMap());
            modelBuilder.Configurations.Add(new vCheckingAccountMap());
            modelBuilder.Configurations.Add(new vCheckRegisterMap());
            modelBuilder.Configurations.Add(new vCompanyElementsWithTypeMap());
            modelBuilder.Configurations.Add(new vCompanyPositionMap());
            modelBuilder.Configurations.Add(new vContractsDetailMap());
            modelBuilder.Configurations.Add(new vContributedAmountMap());
            modelBuilder.Configurations.Add(new vContributedAmountsSUMMap());
            modelBuilder.Configurations.Add(new vCsPortalTaskMap());
            modelBuilder.Configurations.Add(new vDepartmentMap());
            modelBuilder.Configurations.Add(new vDisbursementAccountMap());
            modelBuilder.Configurations.Add(new vDocuments8566Map());
            modelBuilder.Configurations.Add(new vDonorDetailMap());
            modelBuilder.Configurations.Add(new vExpenseHistoryMap());
            modelBuilder.Configurations.Add(new vFinancialPartnerAccountMap());
            modelBuilder.Configurations.Add(new vFinancialPartnerMap());
            modelBuilder.Configurations.Add(new vFoundationContractMap());
            modelBuilder.Configurations.Add(new vFoundationEmailTemplateMap());
            modelBuilder.Configurations.Add(new vFoundationGeneralInfoMap());
            modelBuilder.Configurations.Add(new vFoundationServiceProviderMap());
            modelBuilder.Configurations.Add(new vGlobalEmailTemplateMap());
            modelBuilder.Configurations.Add(new vGrantActivityMap());
            modelBuilder.Configurations.Add(new vGrantDetailMap());
            modelBuilder.Configurations.Add(new vGrantHistoryMap());
            modelBuilder.Configurations.Add(new vGrantIDMap());
            modelBuilder.Configurations.Add(new vGrantingActivityMap());
            modelBuilder.Configurations.Add(new vGrantMap());
            modelBuilder.Configurations.Add(new vGroupEmailTemplateMap());
            modelBuilder.Configurations.Add(new vHistoryInfoMap());
            modelBuilder.Configurations.Add(new ViewPrintReceiptMap());
            modelBuilder.Configurations.Add(new vMainContractMap());
            modelBuilder.Configurations.Add(new vObjectsPermissionsWithObjectMap());
            modelBuilder.Configurations.Add(new vOnlineStaffingProposalMap());
            modelBuilder.Configurations.Add(new vPaidExpensMap());
            modelBuilder.Configurations.Add(new vPartnerEmailTemplateMap());
            modelBuilder.Configurations.Add(new vProjectMap());
            modelBuilder.Configurations.Add(new vQuestAllHasIssueMap());
            modelBuilder.Configurations.Add(new vQuestEachHasIssueMap());
            modelBuilder.Configurations.Add(new vRelationshipManagerMap());
            modelBuilder.Configurations.Add(new vRelationshipManagersFoundationMap());
            modelBuilder.Configurations.Add(new vServiceProviderMap());
            modelBuilder.Configurations.Add(new vShellStatuMap());
            modelBuilder.Configurations.Add(new vStaffingPolicyMap());
            modelBuilder.Configurations.Add(new vStructuredCompanyMap());
            modelBuilder.Configurations.Add(new vTaxReturnStateMap());
            modelBuilder.Configurations.Add(new vTransactsDonorMap());
            modelBuilder.Configurations.Add(new vTransactTypeMap());
            modelBuilder.Configurations.Add(new vTransferDetailMap());
            modelBuilder.Configurations.Add(new vUnassignedDepositMap());
            modelBuilder.Configurations.Add(new TaxReturnDetailMap());
            modelBuilder.Configurations.Add(new vChangedTaxReturnsStatusMap());
            modelBuilder.Configurations.Add(new ContractsServiceMap());
            modelBuilder.Configurations.Add(new PersonsNativeMap());
            modelBuilder.Configurations.Add(new enums_EstimatedTaxIssueTypesMap());
            modelBuilder.Configurations.Add(new enums_EstimatedTaxStepsMap());
            modelBuilder.Configurations.Add(new EstimatedTaxIssuesDetailMap());
            modelBuilder.Configurations.Add(new v_CombinedTaxQ1Q2Map());
            modelBuilder.Configurations.Add(new v_IncomesFromSnapshotMap());
        }
    }
}
