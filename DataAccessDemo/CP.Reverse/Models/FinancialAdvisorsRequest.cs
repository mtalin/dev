using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FinancialAdvisorsRequest
    {
        public FinancialAdvisorsRequest()
        {
            this.FinancialAdvisorsRequestsSteps = new List<FinancialAdvisorsRequestsStep>();
        }

        public int ID { get; set; }
        public int Person { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> FinancialAdvisor { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Step { get; set; }
        public virtual FinancialAdvisor FinancialAdvisor1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<FinancialAdvisorsRequestsStep> FinancialAdvisorsRequestsSteps { get; set; }
    }
}
