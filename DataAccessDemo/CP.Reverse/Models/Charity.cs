using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Charity
    {
        public int Id { get; set; }
        public int CharityEIN { get; set; }
        public Nullable<int> PublicEIN { get; set; }
        public string OrgName { get; set; }
        public string AltName { get; set; }
        public string InCareOf { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipPlus4 { get; set; }
        public Nullable<short> GroupExemptNum { get; set; }
        public Nullable<byte> SubsectCode { get; set; }
        public Nullable<byte> AffiliationCode { get; set; }
        public Nullable<byte> ClassCode1 { get; set; }
        public Nullable<byte> ClassCode2 { get; set; }
        public Nullable<byte> ClassCode3 { get; set; }
        public Nullable<byte> ClassCode4 { get; set; }
        public Nullable<System.DateTime> RulingDate { get; set; }
        public Nullable<byte> DeductCode { get; set; }
        public Nullable<byte> FoundationCode { get; set; }
        public Nullable<short> ActivityCode1 { get; set; }
        public Nullable<short> ActivityCode2 { get; set; }
        public Nullable<short> ActivityCode3 { get; set; }
        public Nullable<byte> OrgCode { get; set; }
        public Nullable<byte> DOJCode { get; set; }
        public Nullable<System.DateTime> AdvRuleExpire { get; set; }
        public Nullable<System.DateTime> TaxPeriod { get; set; }
        public Nullable<byte> AssetCode { get; set; }
        public Nullable<byte> IncomeCode { get; set; }
        public Nullable<byte> FilingReqCode { get; set; }
        public Nullable<bool> FilingReqCode2 { get; set; }
        public Nullable<byte> AcctPeriod { get; set; }
        public Nullable<decimal> AssetAmount { get; set; }
        public Nullable<decimal> IncomeAmount { get; set; }
        public string NTEECode { get; set; }
        public string SortName { get; set; }
        public string BankABA { get; set; }
        public string BankAccountNo { get; set; }
        public string Phone { get; set; }
        public string URL { get; set; }
        public string Email { get; set; }
        public Nullable<bool> PublicSch { get; set; }
        public Nullable<bool> PublicLib { get; set; }
        public Nullable<bool> Religious { get; set; }
        public Nullable<bool> SOI { get; set; }
        public Nullable<bool> IRB { get; set; }
        public Nullable<bool> Government { get; set; }
        public Nullable<bool> GuideStar { get; set; }
        public Nullable<bool> FSChurchSubstantiation { get; set; }
        public Nullable<bool> FSIntegratedAuxiliaryForm { get; set; }
        public Nullable<bool> InfoFromCharity { get; set; }
        public int Hold { get; set; }
        public string OrgInfo { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> Step { get; set; }
        public Nullable<int> Author { get; set; }
        public System.DateTime DtUpdated { get; set; }
        public Nullable<System.DateTime> DtIRBHold { get; set; }
        public string AltAddress { get; set; }
        public string AltCity { get; set; }
        public string AltState { get; set; }
        public string AltZipPlus4 { get; set; }
        public bool UseAltName { get; set; }
        public bool UseAltAddress { get; set; }
        public bool ExpenditureResponsibilities { get; set; }
        public bool CopyOfExistingCharity { get; set; }
        public string Country { get; set; }
        public string ContactPerson { get; set; }
        public System.DateTime DtUploaded { get; set; }
        public string CheckPayableTo { get; set; }
        public string CheckMemo { get; set; }
        public byte[] timestamp { get; set; }
        public bool FiscalAgent { get; set; }
        public Nullable<int> NoteTemplate { get; set; }
        public Nullable<bool> CatholicDirectory { get; set; }
        public bool UseSpecPurpose { get; set; }
        public Nullable<int> SpecPurposeType { get; set; }
        public string SpecPurposeText1 { get; set; }
        public string SpecPurposeText2 { get; set; }
        public string AltInCareOf { get; set; }
        public string Address2 { get; set; }
        public string AltAddress2 { get; set; }
        public string AltCountry { get; set; }
    }
}
