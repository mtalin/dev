using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FavoriteCharity
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int CharityEIN { get; set; }
        public bool IsPublic { get; set; }
        public int Person { get; set; }
        public Nullable<int> old_Nominee { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
