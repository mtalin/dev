using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DesignVotingOfferItem
    {
        public int VotingOffer { get; set; }
        public int Index { get; set; }
        public string IID { get; set; }
        public string IReserved { get; set; }
        public string ICurrFName { get; set; }
        public string ICurrLName { get; set; }
        public string ICurrEmail { get; set; }
        public string IRemove { get; set; }
        public string IReElect { get; set; }
        public string INewFName { get; set; }
        public string INewLName { get; set; }
        public string INewEmail { get; set; }
        public string INotAllowRemove { get; set; }
        public string ICurrent { get; set; }
        public string IStDate { get; set; }
        public string INewNominee { get; set; }
    }
}
