using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ActionsLog
    {
        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int TheAction { get; set; }
        public string Description { get; set; }
        public virtual User User { get; set; }
    }
}
