using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ShellCorporation
    {
        public ShellCorporation()
        {
            this.Documents = new List<Document1>();
            this.Foundations = new List<Foundation>();
            this.Notes = new List<Note>();
            this.ShellCorporationsSteps = new List<ShellCorporationsStep>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int President { get; set; }
        public Nullable<System.DateTime> DtIncorporation { get; set; }
        public string EIN { get; set; }
        public string EINProfileNumber { get; set; }
        public string DCFProfileNumber { get; set; }
        public Nullable<System.DateTime> DtDCFReceived { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public string Account { get; set; }
        public string EFTPSTracer { get; set; }
        public string EFTPSPin { get; set; }
        public string EFTPSPassword { get; set; }
        public Nullable<System.DateTime> DtAssigned { get; set; }
        public Nullable<System.DateTime> DtCOIFiled { get; set; }
        public Nullable<System.DateTime> DtRegistration { get; set; }
        public Nullable<System.DateTime> DtCOIReceived { get; set; }
        public Nullable<System.DateTime> DtNameChangeFiled { get; set; }
        public Nullable<System.DateTime> DtNameChangeReceived { get; set; }
        public Nullable<System.DateTime> DtStartupMailed { get; set; }
        public Nullable<System.DateTime> DtStartupReceived { get; set; }
        public Nullable<System.DateTime> DtIRSPacketMailed { get; set; }
        public Nullable<System.DateTime> DtDocLocatorReceived { get; set; }
        public string DocLocatorNumber { get; set; }
        public string IRSContactName { get; set; }
        public string IRScontactPhone { get; set; }
        public string IRSContactFax { get; set; }
        public string IRSContactEmail { get; set; }
        public string IRSContactBadgeNo { get; set; }
        public Nullable<int> IRSContactAddress { get; set; }
        public Nullable<System.DateTime> DtIRSApproval { get; set; }
        public int Step { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public bool AccountLocked { get; set; }
        public Nullable<System.DateTime> Dt8655Mailed { get; set; }
        public Nullable<System.DateTime> DtCP575Filed { get; set; }
        public Nullable<System.DateTime> Dt1023Mailed { get; set; }
        public Nullable<System.DateTime> DtTAD { get; set; }
        public int Conversion { get; set; }
        public Nullable<int> ShellNumber { get; set; }
        public Nullable<decimal> IRSUserFee { get; set; }
        public string IRSFeeCheckNo { get; set; }
        public Nullable<System.DateTime> dtIRSFeeCheck { get; set; }
        public string IncorpState { get; set; }
        public Nullable<int> LegalForm { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public virtual ICollection<Document1> Documents { get; set; }
        public virtual ICollection<Foundation> Foundations { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<ShellCorporationsStep> ShellCorporationsSteps { get; set; }
    }
}
