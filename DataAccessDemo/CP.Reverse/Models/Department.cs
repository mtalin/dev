using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Department
    {
        public Department()
        {
            this.CompanyPositions = new List<CompanyPosition>();
            this.Departments1 = new List<Department>();
            this.EmailTemplates = new List<EmailTemplate>();
            this.FoundationAccounts = new List<FoundationAccount>();
            this.FoundationAccounts1 = new List<FoundationAccount>();
            this.PricingScheduleInstances = new List<PricingScheduleInstance>();
        }

        public int ID { get; set; }
        public int StructuredCompany { get; set; }
        public Nullable<int> HigherDepartment { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Design { get; set; }
        public virtual CompanyElement CompanyElement { get; set; }
        public virtual ICollection<CompanyPosition> CompanyPositions { get; set; }
        public virtual ICollection<Department> Departments1 { get; set; }
        public virtual Department Department1 { get; set; }
        public virtual PostalAddress PostalAddress1 { get; set; }
        public virtual StructuredCompany StructuredCompany1 { get; set; }
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
        public virtual ICollection<FoundationAccount> FoundationAccounts { get; set; }
        public virtual ICollection<FoundationAccount> FoundationAccounts1 { get; set; }
        public virtual ICollection<PricingScheduleInstance> PricingScheduleInstances { get; set; }
    }
}
