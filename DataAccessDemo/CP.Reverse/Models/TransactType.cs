using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TransactType
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> CashIn { get; set; }
        public Nullable<int> AssetIn { get; set; }
    }
}
