using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GrantSchedule
    {
        public int ID { get; set; }
        public int GrantDetailsTemplate { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public int Foundation { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual GrantDetail GrantDetail { get; set; }
        public virtual TimeSchedule TimeSchedule { get; set; }
    }
}
