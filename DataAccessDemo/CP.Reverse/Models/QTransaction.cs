using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QTransaction
    {
        public QTransaction()
        {
            this.QHistoryTransactions = new List<QHistoryTransaction>();
            this.Transacts = new List<Transact>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public System.DateTime DtTransact { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> FoundationAccount { get; set; }
        public string Account { get; set; }
        public string FPTransactID { get; set; }
        public string Type { get; set; }
        public Nullable<int> TypeCode { get; set; }
        public Nullable<int> Sign { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> PrincipalCost { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> Shares { get; set; }
        public string SecurityName { get; set; }
        public string SecuritySymbol { get; set; }
        public string CUSIP { get; set; }
        public Nullable<int> SecurityType { get; set; }
        public Nullable<decimal> Commission { get; set; }
        public Nullable<decimal> Interest { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<decimal> SecFee { get; set; }
        public Nullable<decimal> MiscFee { get; set; }
        public Nullable<decimal> OthFee { get; set; }
        public Nullable<decimal> TefraWithholding { get; set; }
        public Nullable<decimal> ClearingCharge { get; set; }
        public Nullable<decimal> BrokerageCharge { get; set; }
        public Nullable<decimal> CostBasis { get; set; }
        public Nullable<bool> Taxable { get; set; }
        public Nullable<int> SplitNumerator { get; set; }
        public Nullable<int> SplitDenominator { get; set; }
        public string MergeTicker { get; set; }
        public Nullable<double> Ratio { get; set; }
        public Nullable<decimal> BookBasis { get; set; }
        public Nullable<int> QReport { get; set; }
        public Nullable<int> ReportLine { get; set; }
        public string Memo { get; set; }
        public string Chain { get; set; }
        public Nullable<int> Correction { get; set; }
        public string ErrorInfo { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeleteAuthor { get; set; }
        public Nullable<int> Asset { get; set; }
        public Nullable<int> Asset2 { get; set; }
        public bool Modified { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public string Description { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public Nullable<System.DateTime> dtDeposit { get; set; }
        public Nullable<int> FoundationAccount2 { get; set; }
        public string Account2 { get; set; }
        public virtual Asset Asset1 { get; set; }
        public virtual Asset Asset3 { get; set; }
        public virtual FoundationAccount FoundationAccount1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<QHistoryTransaction> QHistoryTransactions { get; set; }
        public virtual QReport QReport1 { get; set; }
        public virtual ICollection<Transact> Transacts { get; set; }
    }
}
