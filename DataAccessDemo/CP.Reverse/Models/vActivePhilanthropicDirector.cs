using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vActivePhilanthropicDirector
    {
        public int ID { get; set; }
        public int StaffingPolicy { get; set; }
        public Nullable<int> Department { get; set; }
        public bool HeadOfDepartment { get; set; }
        public Nullable<int> SubordinateTo { get; set; }
        public Nullable<int> Person { get; set; }
        public Nullable<System.DateTime> DtNominated { get; set; }
        public Nullable<System.DateTime> DtAccepted { get; set; }
        public Nullable<System.DateTime> DtRemoved { get; set; }
        public Nullable<int> RemovedBy { get; set; }
        public Nullable<System.DateTime> DtExpired { get; set; }
        public int StructuredCompany { get; set; }
        public Nullable<int> Note { get; set; }
    }
}
