using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class RequestDonor
    {
        public RequestDonor()
        {
            this.RequestDonorsSteps = new List<RequestDonorsStep>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Person { get; set; }
        public Nullable<int> QTransaction { get; set; }
        public Nullable<int> QDeposit { get; set; }
        public Nullable<int> Transact { get; set; }
        public int Step { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Document { get; set; }
        public virtual Document1 Document1 { get; set; }
        public virtual ICollection<RequestDonorsStep> RequestDonorsSteps { get; set; }
    }
}
