using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EFT
    {
        public int ID { get; set; }
        public string BankName { get; set; }
        public int RoutingNo { get; set; }
        public string AccountNo { get; set; }
    }
}
