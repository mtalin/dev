using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MailAppliesStepsNote
    {
        public int StepID { get; set; }
        public string Note { get; set; }
        public virtual MailAppliesStep MailAppliesStep { get; set; }
    }
}
