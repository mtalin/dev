using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ContactsStepsNote
    {
        public int StepID { get; set; }
        public string Note { get; set; }
        public virtual ContactsStep ContactsStep { get; set; }
    }
}
