using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EMailNotifyTransact
    {
        public int Task { get; set; }
        public string Address { get; set; }
        public Nullable<int> Confirmation { get; set; }
        public virtual Tasks_Old Tasks_Old { get; set; }
    }
}
