using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QuestionnairesStep
    {
        public int ID { get; set; }
        public int Questionnaire { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Step { get; set; }
        public virtual Questionnaire Questionnaire1 { get; set; }
        public virtual User User { get; set; }
    }
}
