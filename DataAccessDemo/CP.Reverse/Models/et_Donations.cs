using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_Donations
    {
        public Nullable<int> ID { get; set; }
        public Nullable<int> CharityEIN { get; set; }
        public string FoundationName { get; set; }
        public Nullable<int> Person { get; set; }
        public string DateCreated { get; set; }
        public decimal Amount { get; set; }
        public string Attributes { get; set; }
        public string Purpose { get; set; }
        public string PurposeDescription { get; set; }
        public int SpecPurposeType { get; set; }
        public string SpecPurposeText1 { get; set; }
        public string SpecPurposeText2 { get; set; }
        public string FullPurposeText { get; set; }
        public bool IsSpecialDelivery { get; set; }
        public bool Anonymous { get; set; }
        public int IsSpecPurpose { get; set; }
        public int IsCertificate { get; set; }
        public int IsAwaitngApproval { get; set; }
        public Nullable<int> HouseholdAccount { get; set; }
        public Nullable<bool> IsMultiYear { get; set; }
        public Nullable<int> NumPayments { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<int> PaymentNumber { get; set; }
        public string grantreason { get; set; }
        public string ProgramArea { get; set; }
    }
}
