using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QReport
    {
        public QReport()
        {
            this.Partnerships = new List<Partnership>();
            this.QBalances = new List<QBalance>();
            this.QModifyReports = new List<QModifyReport>();
            this.QModifyTransactions = new List<QModifyTransaction>();
            this.QTransactions = new List<QTransaction>();
        }

        public int ID { get; set; }
        public int QParser { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtProcessed { get; set; }
        public int TotalRecords { get; set; }
        public int IgnoredRecords { get; set; }
        public string Source { get; set; }
        public Nullable<System.DateTime> DtReport { get; set; }
        public string SourceName { get; set; }
        public string Note { get; set; }
        public virtual ICollection<Partnership> Partnerships { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<QBalance> QBalances { get; set; }
        public virtual ICollection<QModifyReport> QModifyReports { get; set; }
        public virtual ICollection<QModifyTransaction> QModifyTransactions { get; set; }
        public virtual QParser QParser1 { get; set; }
        public virtual ICollection<QTransaction> QTransactions { get; set; }
    }
}
