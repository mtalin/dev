using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ContractsService
    {
        public int Contract { get; set; }
        public Nullable<long> Services { get; set; }
    }
}
