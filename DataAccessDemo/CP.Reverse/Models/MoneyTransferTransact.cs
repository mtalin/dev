using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MoneyTransferTransact
    {
        public int Task { get; set; }
        public int SourceAccount { get; set; }
        public int TargetAccount { get; set; }
        public decimal Amount { get; set; }
        public virtual Tasks_Old Tasks_Old { get; set; }
    }
}
