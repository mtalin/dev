using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GrantingParam
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public int Author { get; set; }
        public int CompanyElement { get; set; }
        public Nullable<decimal> GrantingLimit { get; set; }
        public Nullable<decimal> ApprovableLimit { get; set; }
        public bool GrantCommitteeMember { get; set; }
        public virtual CompanyElement CompanyElement1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
