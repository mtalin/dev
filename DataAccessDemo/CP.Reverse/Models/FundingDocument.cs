using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FundingDocument
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
    }
}
