using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxesDetail
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public Nullable<decimal> Amount2 { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
        public int PeriodType { get; set; }
        public int PeriodCount { get; set; }
        public int Step { get; set; }
        public Nullable<int> PendingTransact { get; set; }
        public string DataSnapShot { get; set; }
        public Nullable<int> Contract { get; set; }
        public Nullable<int> BillTo { get; set; }
        public Nullable<int> FeeCalculation { get; set; }
        public bool Q1Covered { get; set; }
        public bool Q2Covered { get; set; }
        public bool Q3Covered { get; set; }
        public bool Q4Covered { get; set; }
        public string TargetName { get; set; }
        public string TypeName { get; set; }
        public string LongTypeName { get; set; }
        public string TypeInfo { get; set; }
        public Nullable<int> Recurring { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public Nullable<int> FinancialAdvisor { get; set; }
        public string FoundationName { get; set; }
        public string FinancialPartnerName { get; set; }
        public string FinancialAdvisorName { get; set; }
        public string TaxCalcMethod { get; set; }
        public Nullable<int> Quarter { get; set; }
        public Nullable<int> QuarterCoverageMask { get; set; }
        public string PeriodName { get; set; }
        public string TaxesResponsibleNote { get; set; }
    }
}
