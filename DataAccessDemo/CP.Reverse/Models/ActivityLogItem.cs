using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ActivityLogItem
    {
        public int ID { get; set; }
        public int ActivityLog { get; set; }
        public int LoggedAction { get; set; }
        public Nullable<int> Object { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual ActivityLog ActivityLog1 { get; set; }
    }
}
