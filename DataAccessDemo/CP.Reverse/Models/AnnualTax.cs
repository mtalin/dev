using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AnnualTax
    {
        public int Type { get; set; }
        public int Foundation { get; set; }
        public decimal Amount { get; set; }
        public Nullable<System.DateTime> dtPeriod { get; set; }
    }
}
