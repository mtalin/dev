using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GlobalAsset
    {
        public int ID { get; set; }
        public string Symbol { get; set; }
        public string CUSIP { get; set; }
        public virtual Asset Asset { get; set; }
    }
}
