using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReturnDocument
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int TaxReturn { get; set; }
        public int Document { get; set; }
        public string XmlData { get; set; }
        public Nullable<int> Template { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public int Theme { get; set; }
        public Nullable<int> TaxReturnForm { get; set; }
        public virtual Document1 Document1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual TaxReturnForm TaxReturnForm1 { get; set; }
        public virtual TaxReturn TaxReturn1 { get; set; }
    }
}
