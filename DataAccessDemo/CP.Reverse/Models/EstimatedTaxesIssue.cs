using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EstimatedTaxesIssue
    {
        public EstimatedTaxesIssue()
        {
            this.EstimatedTaxesIssuesSteps = new List<EstimatedTaxesIssuesStep>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public Nullable<int> EstimatedTax { get; set; }
        public int Step { get; set; }
        public int IssueType { get; set; }
        public System.DateTime DtOpen { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public Nullable<int> ClosedBy { get; set; }
        public Nullable<System.DateTime> dtLastRecalc { get; set; }
        public virtual EstimatedTax EstimatedTax1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<EstimatedTaxesIssuesStep> EstimatedTaxesIssuesSteps { get; set; }
    }
}
