using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TrHoldingMacro
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
