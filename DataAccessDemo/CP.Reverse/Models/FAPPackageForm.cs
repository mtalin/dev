using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FAPPackageForm
    {
        public FAPPackageForm()
        {
            this.FAPPackages = new List<FAPPackage>();
        }

        public int ID { get; set; }
        public int Partner { get; set; }
        public bool DisplayPackageLink { get; set; }
        public string LinkText { get; set; }
        public string Introduction { get; set; }
        public bool SendToAdvisor { get; set; }
        public bool SendToDistributionCenter { get; set; }
        public Nullable<int> DCAddress { get; set; }
        public bool SendToSpecificRecipient { get; set; }
        public virtual PostalAddress PostalAddress { get; set; }
        public virtual ICollection<FAPPackage> FAPPackages { get; set; }
    }
}
