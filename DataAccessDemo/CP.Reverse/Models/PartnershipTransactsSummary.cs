using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PartnershipTransactsSummary
    {
        public int Partnership { get; set; }
        public Nullable<int> PfLine { get; set; }
        public int K1Line { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<int> TType { get; set; }
        public Nullable<decimal> A { get; set; }
        public Nullable<decimal> B { get; set; }
        public Nullable<decimal> Ubi { get; set; }
    }
}
