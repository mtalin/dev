using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EstimatedTaxesStep
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int EstimatedTax { get; set; }
        public int Step { get; set; }
        public Nullable<int> PrevStep { get; set; }
        public virtual EstimatedTax EstimatedTax1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
