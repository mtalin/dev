using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Document1
    {
        public Document1()
        {
            this.AssetBooks = new List<AssetBook>();
            this.AssetValuations = new List<AssetValuation>();
            this.CertifiedMails = new List<CertifiedMail>();
            this.ExpenseReports = new List<ExpenseReport>();
            this.ExpenseReports1 = new List<ExpenseReport>();
            this.Expenses = new List<Expens>();
            this.Expenses1 = new List<Expens>();
            this.FAPPackages = new List<FAPPackage>();
            this.FinancialPartnerDocuments = new List<FinancialPartnerDocument>();
            this.GrantDetails = new List<GrantDetail>();
            this.HouseholdDocuments = new List<HouseholdDocument>();
            this.Partnerships = new List<Partnership>();
            this.PayeeProfiles = new List<PayeeProfile>();
            this.Proposals = new List<Proposal>();
            this.RequestDonors = new List<RequestDonor>();
            this.SatelliteDocuments = new List<SatelliteDocument>();
            this.SatelliteDocuments1 = new List<SatelliteDocument>();
            this.TaxReturnDocuments = new List<TaxReturnDocument>();
            this.TransferGroups = new List<TransferGroup>();
            this.TaxReceipts = new List<TaxReceipt>();
        }

        public int ID { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> ShellCorporation { get; set; }
        public int Theme { get; set; }
        public string Extention { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public byte[] Body { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
        public Nullable<int> PeriodType { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtReceived { get; set; }
        public Nullable<System.DateTime> DtBookMark { get; set; }
        public string Notes { get; set; }
        public string XMLData { get; set; }
        public Nullable<int> CharityEIN { get; set; }
        public Nullable<int> CharityEdit { get; set; }
        public bool IsUploaded { get; set; }
        public virtual ICollection<AssetBook> AssetBooks { get; set; }
        public virtual ICollection<AssetValuation> AssetValuations { get; set; }
        public virtual ICollection<CertifiedMail> CertifiedMails { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ShellCorporation ShellCorporation1 { get; set; }
        public virtual ICollection<ExpenseReport> ExpenseReports { get; set; }
        public virtual ICollection<ExpenseReport> ExpenseReports1 { get; set; }
        public virtual ICollection<Expens> Expenses { get; set; }
        public virtual ICollection<Expens> Expenses1 { get; set; }
        public virtual ICollection<FAPPackage> FAPPackages { get; set; }
        public virtual ICollection<FinancialPartnerDocument> FinancialPartnerDocuments { get; set; }
        public virtual ICollection<GrantDetail> GrantDetails { get; set; }
        public virtual ICollection<HouseholdDocument> HouseholdDocuments { get; set; }
        public virtual ICollection<Partnership> Partnerships { get; set; }
        public virtual ICollection<PayeeProfile> PayeeProfiles { get; set; }
        public virtual ICollection<Proposal> Proposals { get; set; }
        public virtual ICollection<RequestDonor> RequestDonors { get; set; }
        public virtual ICollection<SatelliteDocument> SatelliteDocuments { get; set; }
        public virtual ICollection<SatelliteDocument> SatelliteDocuments1 { get; set; }
        public virtual ICollection<TaxReturnDocument> TaxReturnDocuments { get; set; }
        public virtual ICollection<TransferGroup> TransferGroups { get; set; }
        public virtual ICollection<TaxReceipt> TaxReceipts { get; set; }
    }
}
