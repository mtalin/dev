using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vContributedAmountsSUM
    {
        public int Foundation { get; set; }
        public Nullable<int> donor { get; set; }
        public Nullable<decimal> ActualAmount { get; set; }
        public Nullable<System.DateTime> dtSC { get; set; }
    }
}
