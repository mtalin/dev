using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GrantDetail
    {
        public GrantDetail()
        {
            this.CharityRequests = new List<CharityRequest>();
            this.GrantDetails1 = new List<GrantDetail>();
            this.GrantMessages = new List<GrantMessage>();
            this.GrantProposals = new List<GrantProposal>();
            this.Grants = new List<Grant>();
            this.GrantSchedules = new List<GrantSchedule>();
            this.HistoricalGrants = new List<HistoricalGrant>();
        }

        public int ID { get; set; }
        public decimal Amount { get; set; }
        public bool Anonymous { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public string TargetedDescription { get; set; }
        public Nullable<int> GrantLetter { get; set; }
        public string GrantLetterData { get; set; }
        public int Priority { get; set; }
        public string SpecialUserNote { get; set; }
        public bool ReviewedByAdmin { get; set; }
        public Nullable<int> DonationRequest { get; set; }
        public Nullable<int> Certificate { get; set; }
        public int RecipientType { get; set; }
        public Nullable<int> CharityEIN { get; set; }
        public Nullable<int> HouseholdAccount { get; set; }
        public string Purpose { get; set; }
        public string PurposeDescription { get; set; }
        public Nullable<int> SpecPurposeType { get; set; }
        public string SpecPurposeText1 { get; set; }
        public string SpecPurposeText2 { get; set; }
        public bool IsSpecialDelivery { get; set; }
        public string NoteForEmail { get; set; }
        public string ChOrgName { get; set; }
        public string ChInCareOf { get; set; }
        public string ChAddress { get; set; }
        public string ChCity { get; set; }
        public string ChState { get; set; }
        public string ChZIP { get; set; }
        public bool ConfirmGuideStar { get; set; }
        public Nullable<bool> ConfirmIRSbulletins { get; set; }
        public Nullable<bool> ConfirmIRSwebsite { get; set; }
        public string ConfirmDesc { get; set; }
        public bool PermanentChanges { get; set; }
        public bool GrantLetterApproved { get; set; }
        public Nullable<byte> GrantType { get; set; }
        public Nullable<int> NumPayments { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<int> PaymentNumber { get; set; }
        public string ProcessInstruction { get; set; }
        public Nullable<int> Template { get; set; }
        public string CheckMemo { get; set; }
        public string ChAddress2 { get; set; }
        public string GrantReason { get; set; }
        public Nullable<int> Foundation { get; set; }
        public bool GmxRef { get; set; }
        public Nullable<int> Author { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public string ChCountry { get; set; }
        public string CheckModReason { get; set; }
        public Nullable<System.DateTime> DtBridger { get; set; }
        public virtual ICollection<CharityRequest> CharityRequests { get; set; }
        public virtual Document1 Document { get; set; }
        public virtual DonationCertificate DonationCertificate { get; set; }
        public virtual DonationRequest DonationRequest1 { get; set; }
        public virtual FoundationProject FoundationProject1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<GrantDetail> GrantDetails1 { get; set; }
        public virtual GrantDetail GrantDetail1 { get; set; }
        public virtual HouseholdAccount HouseholdAccount1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<GrantMessage> GrantMessages { get; set; }
        public virtual ICollection<GrantProposal> GrantProposals { get; set; }
        public virtual ICollection<Grant> Grants { get; set; }
        public virtual ICollection<GrantSchedule> GrantSchedules { get; set; }
        public virtual ICollection<HistoricalGrant> HistoricalGrants { get; set; }
    }
}
