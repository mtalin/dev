using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DonationCertificate
    {
        public DonationCertificate()
        {
            this.GrantDetails = new List<GrantDetail>();
        }

        public int ID { get; set; }
        public int Person { get; set; }
        public int Foundation { get; set; }
        public int Recipient { get; set; }
        public decimal Amount { get; set; }
        public Nullable<int> Message { get; set; }
        public int Step { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Event { get; set; }
        public bool SingleGrant { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Person Person2 { get; set; }
        public virtual ICollection<GrantDetail> GrantDetails { get; set; }
    }
}
