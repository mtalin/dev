using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FinancialPartnerDocument
    {
        public int ID { get; set; }
        public int Theme { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public Nullable<int> Document { get; set; }
        public bool Ignore { get; set; }
        public virtual Document1 Document1 { get; set; }
    }
}
