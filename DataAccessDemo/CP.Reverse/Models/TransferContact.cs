using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TransferContact
    {
        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Contact { get; set; }
        public bool Primary { get; set; }
        public bool CC { get; set; }
        public bool NotifyByFax { get; set; }
        public bool NotifyByEmail { get; set; }
        public Nullable<int> TransferProfile { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Person Person2 { get; set; }
    }
}
