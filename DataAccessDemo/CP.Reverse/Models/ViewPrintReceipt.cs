using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ViewPrintReceipt
    {
        public string DonorName { get; set; }
        public string Email { get; set; }
        public string FoundationName { get; set; }
        public Nullable<int> RYear { get; set; }
        public Nullable<int> DonorID { get; set; }
        public int FoundationID { get; set; }
        public string DonorLastName { get; set; }
        public int DonationAllocated { get; set; }
        public int AccountReconciled { get; set; }
        public int ReceiptStatus { get; set; }
        public Nullable<int> TaxReceiptID { get; set; }
        public Nullable<int> Note { get; set; }
        public int Step { get; set; }
        public Nullable<int> LastDoc { get; set; }
        public int Revised { get; set; }
        public int IsPriced { get; set; }
        public Nullable<int> DeliveryPrefence { get; set; }
        public int DeliveredBy { get; set; }
        public int CntPendingXIN { get; set; }
    }
}
