using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Donor
    {
        public Donor()
        {
            this.Donors1 = new List<Donor>();
            this.SubstantialContributors = new List<SubstantialContributor>();
            this.TaxReceipts = new List<TaxReceipt>();
            this.TransactsDonors = new List<TransactsDonor>();
            this.TransactsDonors1 = new List<TransactsDonor>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public int DonorType { get; set; }
        public Nullable<int> Person { get; set; }
        public Nullable<int> Company { get; set; }
        public Nullable<int> LastSpouse { get; set; }
        public string InCareOf { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int DeliveryMethod { get; set; }
        public virtual Company Company1 { get; set; }
        public virtual ICollection<Donor> Donors1 { get; set; }
        public virtual Donor Donor1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person2 { get; set; }
        public virtual ICollection<SubstantialContributor> SubstantialContributors { get; set; }
        public virtual ICollection<TaxReceipt> TaxReceipts { get; set; }
        public virtual ICollection<TransactsDonor> TransactsDonors { get; set; }
        public virtual ICollection<TransactsDonor> TransactsDonors1 { get; set; }
    }
}
