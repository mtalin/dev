using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AllFinancialPartner
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Nullable<int> PhoneExt { get; set; }
        public string Contact { get; set; }
        public Nullable<int> Email { get; set; }
        public Nullable<int> Address { get; set; }
        public string Institution { get; set; }
        public string URL { get; set; }
        public Nullable<int> AccessURL { get; set; }
        public Nullable<int> Step6Amount { get; set; }
        public Nullable<int> Step6Note { get; set; }
        public int SupportsOnline { get; set; }
        public int IsTestPartner { get; set; }
        public Nullable<int> FlatPartnerRate { get; set; }
        public Nullable<int> BrochureSheetTitle { get; set; }
        public bool SingleSignOn { get; set; }
        public int HideSuggestion { get; set; }
        public string UID { get; set; }
        public int StructuredCompany { get; set; }
    }
}
