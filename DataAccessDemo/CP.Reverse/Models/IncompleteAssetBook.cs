using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class IncompleteAssetBook
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public string Description { get; set; }
        public bool UBTI { get; set; }
        public Nullable<int> UBTIDocument { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtExpired { get; set; }
        public int Author { get; set; }
        public Nullable<int> UBTINote { get; set; }
    }
}
