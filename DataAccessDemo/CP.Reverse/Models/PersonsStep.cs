using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PersonsStep
    {
        public int ID { get; set; }
        public Nullable<int> Person { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public int Step { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual User User { get; set; }
    }
}
