using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CompanyElement
    {
        public CompanyElement()
        {
            this.ContractPlayers = new List<ContractPlayer>();
            this.Contracts = new List<Contract>();
            this.FeeCalculations = new List<FeeCalculation>();
            this.Foundations = new List<Foundation>();
            this.GrantingParams = new List<GrantingParam>();
            this.PricingScheduleInstances = new List<PricingScheduleInstance>();
        }

        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public string Name { get; set; }
        public Nullable<int> ObjectPermissions { get; set; }
        public virtual Person Person { get; set; }
        public virtual CompanyPosition CompanyPosition { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Department Department { get; set; }
        public virtual ObjectsPermission ObjectsPermission { get; set; }
        public virtual StaffingPolicy StaffingPolicy { get; set; }
        public virtual StructuredCompany StructuredCompany { get; set; }
        public virtual ICollection<ContractPlayer> ContractPlayers { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
        public virtual ICollection<FeeCalculation> FeeCalculations { get; set; }
        public virtual ICollection<Foundation> Foundations { get; set; }
        public virtual ICollection<GrantingParam> GrantingParams { get; set; }
        public virtual ICollection<PricingScheduleInstance> PricingScheduleInstances { get; set; }
    }
}
