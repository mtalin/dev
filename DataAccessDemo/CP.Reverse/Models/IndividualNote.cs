using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class IndividualNote
    {
        public int ID { get; set; }
        public int Person { get; set; }
        public int Note { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
