using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vCompanyElementsWithType
    {
        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public string Name { get; set; }
        public Nullable<int> ObjectPermissions { get; set; }
        public string Object { get; set; }
    }
}
