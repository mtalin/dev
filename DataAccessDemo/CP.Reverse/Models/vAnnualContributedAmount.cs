using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vAnnualContributedAmount
    {
        public Nullable<int> Donor { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<decimal> ActualAmount { get; set; }
        public Nullable<decimal> PhantomAmount { get; set; }
    }
}
