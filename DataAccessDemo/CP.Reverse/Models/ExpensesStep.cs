using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ExpensesStep
    {
        public int ID { get; set; }
        public int Expense { get; set; }
        public int Step { get; set; }
        public Nullable<int> Message { get; set; }
        public Nullable<int> Note { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual Expens Expens { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
