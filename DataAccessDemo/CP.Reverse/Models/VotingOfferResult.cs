using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class VotingOfferResult
    {
        public int ID { get; set; }
        public int VotingOffer { get; set; }
        public int Person { get; set; }
        public int Appointment { get; set; }
        public int Opinion { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual VotingOffer VotingOffer1 { get; set; }
    }
}
