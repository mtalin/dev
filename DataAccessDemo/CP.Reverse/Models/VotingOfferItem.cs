using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class VotingOfferItem
    {
        public int ID { get; set; }
        public int VotingOffer { get; set; }
        public int TheAction { get; set; }
        public Nullable<int> Person { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public int Appointment { get; set; }
        public virtual VotingOffer VotingOffer1 { get; set; }
    }
}
