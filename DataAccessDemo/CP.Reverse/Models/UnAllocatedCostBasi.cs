using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class UnAllocatedCostBasi
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Name4 { get; set; }
        public int Type { get; set; }
        public int Transact { get; set; }
        public int Foundation { get; set; }
        public int Asset { get; set; }
    }
}
