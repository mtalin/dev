using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class StaffingPoliciesRight
    {
        public int ID { get; set; }
        public int StructuredCompany { get; set; }
        public int Foundation { get; set; }
        public int SystemPosition { get; set; }
        public int StaffingPolicyTemplate { get; set; }
        public string Permissions { get; set; }
        public string AppliedPermissions { get; set; }
        public string InheritedPermissions { get; set; }
    }
}
