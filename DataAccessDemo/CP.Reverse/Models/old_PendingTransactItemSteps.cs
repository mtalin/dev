using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class old_PendingTransactItemSteps
    {
        public int ID { get; set; }
        public int PendingTransactItem { get; set; }
        public int Step { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual old_PendingTransactItems old_PendingTransactItems { get; set; }
    }
}
