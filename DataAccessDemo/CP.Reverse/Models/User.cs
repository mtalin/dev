using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class User
    {
        public User()
        {
            this.ActionsLogs = new List<ActionsLog>();
            this.ContactsSteps = new List<ContactsStep>();
            this.CRMTasksSteps = new List<CRMTasksStep>();
            this.EmailTemplates = new List<EmailTemplate>();
            this.FeeCalculations = new List<FeeCalculation>();
            this.FoundationAccounts = new List<FoundationAccount>();
            this.FoundationAccounts1 = new List<FoundationAccount>();
            this.FoundationsSteps = new List<FoundationsStep>();
            this.GrantsSteps = new List<GrantsStep>();
            this.MailAppliesSelectedDocuments = new List<MailAppliesSelectedDocument>();
            this.MailAppliesSteps = new List<MailAppliesStep>();
            this.Messages = new List<Message>();
            this.Notes = new List<Note>();
            this.old_NomineesSteps = new List<old_NomineesSteps>();
            this.PartnershipAmounts = new List<PartnershipAmount>();
            this.PartnershipsSteps = new List<PartnershipsStep>();
            this.PersonRightsHistories = new List<PersonRightsHistory>();
            this.PersonsSteps = new List<PersonsStep>();
            this.ProposalsSteps = new List<ProposalsStep>();
            this.QDeposits = new List<QDeposit>();
            this.QuestionnairesSteps = new List<QuestionnairesStep>();
            this.ShellCorporationsSteps = new List<ShellCorporationsStep>();
            this.SystemPositions = new List<SystemPosition>();
            this.Taxes = new List<Tax>();
        }

        public int ID { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public System.Guid SID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int CanChangeName { get; set; }
        public string SSOID { get; set; }
        public Nullable<int> RealID { get; set; }
        public string PartnerUID { get; set; }
        public virtual ICollection<ActionsLog> ActionsLogs { get; set; }
        public virtual ICollection<ContactsStep> ContactsSteps { get; set; }
        public virtual ICollection<CRMTasksStep> CRMTasksSteps { get; set; }
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
        public virtual ICollection<FeeCalculation> FeeCalculations { get; set; }
        public virtual ICollection<FoundationAccount> FoundationAccounts { get; set; }
        public virtual ICollection<FoundationAccount> FoundationAccounts1 { get; set; }
        public virtual ICollection<FoundationsStep> FoundationsSteps { get; set; }
        public virtual ICollection<GrantsStep> GrantsSteps { get; set; }
        public virtual ICollection<MailAppliesSelectedDocument> MailAppliesSelectedDocuments { get; set; }
        public virtual ICollection<MailAppliesStep> MailAppliesSteps { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<old_NomineesSteps> old_NomineesSteps { get; set; }
        public virtual ICollection<PartnershipAmount> PartnershipAmounts { get; set; }
        public virtual ICollection<PartnershipsStep> PartnershipsSteps { get; set; }
        public virtual ICollection<PersonRightsHistory> PersonRightsHistories { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<PersonsStep> PersonsSteps { get; set; }
        public virtual ICollection<ProposalsStep> ProposalsSteps { get; set; }
        public virtual ICollection<QDeposit> QDeposits { get; set; }
        public virtual ICollection<QuestionnairesStep> QuestionnairesSteps { get; set; }
        public virtual ICollection<ShellCorporationsStep> ShellCorporationsSteps { get; set; }
        public virtual ICollection<SystemPosition> SystemPositions { get; set; }
        public virtual ICollection<Tax> Taxes { get; set; }
    }
}
