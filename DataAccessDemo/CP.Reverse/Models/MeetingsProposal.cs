using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MeetingsProposal
    {
        public int ID { get; set; }
        public int Meeting { get; set; }
        public int Proposal { get; set; }
        public virtual Meeting Meeting1 { get; set; }
        public virtual Proposal Proposal1 { get; set; }
    }
}
