using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CreditCard
    {
        public int ID { get; set; }
        public Nullable<int> Person { get; set; }
        public int Type { get; set; }
        public string Number { get; set; }
        public System.DateTime ExpirationDate { get; set; }
    }
}
