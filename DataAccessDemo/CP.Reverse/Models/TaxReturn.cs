using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReturn
    {
        public TaxReturn()
        {
            this.TaxReturnDocuments = new List<TaxReturnDocument>();
            this.TaxReturnForms = new List<TaxReturnForm>();
            this.TaxReturnIssues = new List<TaxReturnIssue>();
            this.TaxReturnReports = new List<TaxReturnReport>();
            this.TaxReturnsSteps = new List<TaxReturnsStep>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Foundation { get; set; }
        public int Year { get; set; }
        public int Step { get; set; }
        public Nullable<System.DateTime> DtSentToSaber { get; set; }
        public Nullable<System.DateTime> DtSentToIRS { get; set; }
        public Nullable<System.DateTime> DtFinalized { get; set; }
        public string Note { get; set; }
        public string IssuesCache { get; set; }
        public string XmlData { get; set; }
        public Nullable<bool> TaxPackageRecalc { get; set; }
        public Nullable<System.DateTime> dtRecalc { get; set; }
        public Nullable<int> ResponsiblePerson { get; set; }
        public int State1 { get; set; }
        public int State2 { get; set; }
        public int State3 { get; set; }
        public int State4 { get; set; }
        public Nullable<int> FilingMethod { get; set; }
        public Nullable<int> MailCarrier { get; set; }
        public string TrackingNumber { get; set; }
        public Nullable<System.DateTime> DtSentToProcessing { get; set; }
        public int TaxCenterState { get; set; }
        public Nullable<int> DeliveredBy { get; set; }
        public Nullable<decimal> TaxDue { get; set; }
        public int Priority { get; set; }
        public string ReviewInstruction { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<TaxReturnDocument> TaxReturnDocuments { get; set; }
        public virtual ICollection<TaxReturnForm> TaxReturnForms { get; set; }
        public virtual ICollection<TaxReturnIssue> TaxReturnIssues { get; set; }
        public virtual ICollection<TaxReturnReport> TaxReturnReports { get; set; }
        public virtual ICollection<TaxReturnsStep> TaxReturnsSteps { get; set; }
    }
}
