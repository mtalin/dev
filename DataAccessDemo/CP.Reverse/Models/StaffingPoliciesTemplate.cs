using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class StaffingPoliciesTemplate
    {
        public int ID { get; set; }
        public int CompanyType { get; set; }
        public int SystemPosition { get; set; }
        public Nullable<int> MinInstances { get; set; }
        public Nullable<int> MaxInstances { get; set; }
        public Nullable<int> PeriodType { get; set; }
        public Nullable<int> ExpirationPeriod { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public Nullable<int> ObjectPermissions { get; set; }
        public virtual ObjectsPermission ObjectsPermission { get; set; }
        public virtual SystemPosition SystemPosition1 { get; set; }
    }
}
