using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EFTPSPayment
    {
        public EFTPSPayment()
        {
            this.EFTPSConfirmations = new List<EFTPSConfirmation>();
        }

        public int ID { get; set; }
        public int Payment { get; set; }
        public string EFTNumber { get; set; }
        public Nullable<System.DateTime> SettlementDate { get; set; }
        public string TaxPeriod { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual ICollection<EFTPSConfirmation> EFTPSConfirmations { get; set; }
        public virtual Person Person { get; set; }
        public virtual Payment Payment1 { get; set; }
    }
}
