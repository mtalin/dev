using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Questionnaire
    {
        public Questionnaire()
        {
            this.QuestionnairesSteps = new List<QuestionnairesStep>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public string Version { get; set; }
        public int FiscalYear { get; set; }
        public string FormData { get; set; }
        public int Step { get; set; }
        public bool HasIssues { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtSubmitted { get; set; }
        public Nullable<System.DateTime> DtCompleted { get; set; }
        public int Author { get; set; }
        public Nullable<int> CompletedBy { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<QuestionnairesStep> QuestionnairesSteps { get; set; }
    }
}
