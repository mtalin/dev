using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CustomReport
    {
        public CustomReport()
        {
            this.CustomReportsTemplates = new List<CustomReportsTemplate>();
            this.TaxReturnReports = new List<TaxReturnReport>();
            this.Foundations = new List<Foundation>();
        }

        public int ID { get; set; }
        public int Category { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public int Step { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public bool AdminOnly { get; set; }
        public bool ExtUserOnly { get; set; }
        public int ExclusionMode { get; set; }
        public Nullable<int> MatrixFormat { get; set; }
        public virtual CustomReportCategory CustomReportCategory { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<CustomReportsTemplate> CustomReportsTemplates { get; set; }
        public virtual ICollection<TaxReturnReport> TaxReturnReports { get; set; }
        public virtual ICollection<Foundation> Foundations { get; set; }
    }
}
