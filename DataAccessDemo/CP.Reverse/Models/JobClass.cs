using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class JobClass
    {
        public JobClass()
        {
            this.EventActions = new List<EventAction>();
            this.Jobs = new List<Job>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int RoutineType { get; set; }
        public string Routine { get; set; }
        public int Timeout { get; set; }
        public string RetryStrategy { get; set; }
        public virtual ICollection<EventAction> EventActions { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
    }
}
