using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ObjectsActionsLog
    {
        public int ID { get; set; }
        public string ObjectName { get; set; }
        public int ObjectID { get; set; }
        public string Action { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
    }
}
