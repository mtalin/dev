using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vFoundationGeneralInfo
    {
        public int Foundation { get; set; }
        public string FoundationName { get; set; }
        public string FoundationSortName { get; set; }
        public Nullable<int> StructuredCompany { get; set; }
        public Nullable<int> Pca { get; set; }
        public string PcaName { get; set; }
        public Nullable<System.DateTime> DtCommence { get; set; }
        public Nullable<int> Region { get; set; }
        public string RegionName { get; set; }
        public string StrategicPartnerName { get; set; }
        public Nullable<int> SalesRepresentative { get; set; }
        public string SalesRepresentativeName { get; set; }
    }
}
