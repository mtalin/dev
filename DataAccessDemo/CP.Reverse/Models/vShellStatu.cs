using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vShellStatu
    {
        public int ID { get; set; }
        public Nullable<int> Foundation { get; set; }
        public string Name { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public Nullable<System.DateTime> COIGenerated { get; set; }
        public Nullable<System.DateTime> EnterDateCOI { get; set; }
        public Nullable<System.DateTime> UploadDCF { get; set; }
        public Nullable<System.DateTime> MBOfficialCOI { get; set; }
        public Nullable<System.DateTime> ObtainEIN { get; set; }
        public Nullable<System.DateTime> MBCP575 { get; set; }
        public Nullable<System.DateTime> MailIRSForm8655 { get; set; }
        public Nullable<System.DateTime> MB8655 { get; set; }
        public Nullable<System.DateTime> Assigned { get; set; }
        public Nullable<System.DateTime> AssignAccount { get; set; }
        public Nullable<System.DateTime> AMCOIGenerated { get; set; }
        public Nullable<System.DateTime> FilingAMCOI { get; set; }
        public Nullable<System.DateTime> Resignation { get; set; }
        public Nullable<System.DateTime> ReceiveRACOI { get; set; }
        public Nullable<System.DateTime> MBOfficialRestatedCOI { get; set; }
        public Nullable<System.DateTime> MBRess { get; set; }
        public Nullable<System.DateTime> RequestIRSNameChange { get; set; }
        public Nullable<System.DateTime> ReceivedIRSNameChange { get; set; }
        public Nullable<System.DateTime> MBFIRSLetter { get; set; }
        public Nullable<System.DateTime> StartUpPacketMailing { get; set; }
        public Nullable<System.DateTime> UploadSCAgreement { get; set; }
        public Nullable<System.DateTime> UploadSuccDirResolution { get; set; }
        public Nullable<System.DateTime> Upload3rdResolution { get; set; }
        public Nullable<System.DateTime> Upload2848 { get; set; }
        public Nullable<System.DateTime> UploadStartUpPacket { get; set; }
        public Nullable<System.DateTime> MBConfirmationSAsigned { get; set; }
        public Nullable<System.DateTime> MBSuccessorSigned { get; set; }
        public Nullable<System.DateTime> MBOrgRes3signed { get; set; }
        public Nullable<System.DateTime> MBForm2848signed { get; set; }
        public Nullable<System.DateTime> F1023Generated { get; set; }
        public Nullable<System.DateTime> F1023PDF { get; set; }
        public Nullable<System.DateTime> F1023 { get; set; }
        public Nullable<System.DateTime> FORM8718 { get; set; }
        public Nullable<System.DateTime> AttorneyReview1023 { get; set; }
        public Nullable<System.DateTime> F1023Mailed { get; set; }
        public Nullable<System.DateTime> MailIRSPacket { get; set; }
        public Nullable<System.DateTime> ReceiveDocLocNum { get; set; }
        public Nullable<System.DateTime> UploadIRSLetter { get; set; }
        public Nullable<System.DateTime> MBIRSAskLetter { get; set; }
        public Nullable<System.DateTime> ReceiveIRSApproval { get; set; }
        public Nullable<System.DateTime> MBIRSTaxExemptLetter { get; set; }
        public Nullable<System.DateTime> Dt4thResolutionPrepared { get; set; }
        public Nullable<System.DateTime> Dt4thResolutionReceived { get; set; }
        public Nullable<System.DateTime> IRSUserFee { get; set; }
        public Nullable<System.DateTime> IRSUserFeeGenerate { get; set; }
        public Nullable<System.DateTime> IRSUserFeeComplete { get; set; }
        public Nullable<System.DateTime> InitialFunding { get; set; }
        public Nullable<decimal> SetupFee { get; set; }
        public Nullable<System.DateTime> DtSetupFee { get; set; }
        public int ThirdPartySetupFee { get; set; }
        public Nullable<System.DateTime> NominationProcess { get; set; }
        public Nullable<int> AcceptedNominees { get; set; }
        public Nullable<int> RejectedNominees { get; set; }
        public Nullable<int> ProcessedNominees { get; set; }
        public Nullable<int> SetupFeeToThirdParty { get; set; }
    }
}
