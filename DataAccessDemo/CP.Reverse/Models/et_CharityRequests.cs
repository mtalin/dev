using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_CharityRequests
    {
        public int ID { get; set; }
        public string DateCreated { get; set; }
        public Nullable<int> RequestAuthor { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> InitialGrant { get; set; }
        public string CharityName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string URL { get; set; }
        public string ContactPerson { get; set; }
        public string GeneralInfo { get; set; }
        public string AdditionalInfo { get; set; }
        public Nullable<int> CharityEdit { get; set; }
        public Nullable<int> PublicEIN { get; set; }
        public Nullable<bool> DiscloseIdentity { get; set; }
        public Nullable<int> Author { get; set; }
        public int PendingID { get; set; }
        public string PendingName { get; set; }
    }
}
