using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class qd_FPersons
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Questionnaire { get; set; }
        public int type { get; set; }
        public Nullable<int> Person { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Nullable<bool> Excluded { get; set; }
        public string Hours { get; set; }
        public string Comments { get; set; }
    }
}
