using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Grant
    {
        public Grant()
        {
            this.GrantsSteps = new List<GrantsStep>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> PendingTransact { get; set; }
        public int Step { get; set; }
        public int State { get; set; }
        public Nullable<int> GrantDetails { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public decimal Amount { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual GrantDetail GrantDetail { get; set; }
        public virtual PendingTransact PendingTransact1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<GrantsStep> GrantsSteps { get; set; }
    }
}
