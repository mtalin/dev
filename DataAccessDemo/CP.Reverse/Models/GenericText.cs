using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GenericText
    {
        public int ID { get; set; }
        public string Text { get; set; }
    }
}
