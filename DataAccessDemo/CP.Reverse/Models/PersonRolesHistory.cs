using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PersonRolesHistory
    {
        public int ID { get; set; }
        public int Author { get; set; }
        public int PersonRole { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int ApplyType { get; set; }
        public string Name { get; set; }
        public string OldName { get; set; }
        public string Rights { get; set; }
        public string OldRights { get; set; }
        public virtual PersonRole PersonRole1 { get; set; }
    }
}
