using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_CharitiesEdit
    {
        public int ID { get; set; }
        public Nullable<int> CharityEIN { get; set; }
        public Nullable<int> PublicEIN { get; set; }
        public string OrgName { get; set; }
        public string AltName { get; set; }
        public string InCareOf { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipPlus4 { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string URL { get; set; }
        public string Email { get; set; }
        public string AltAddress { get; set; }
        public string AltAddress2 { get; set; }
        public string AltCity { get; set; }
        public string AltState { get; set; }
        public string AltZipPlus4 { get; set; }
    }
}
