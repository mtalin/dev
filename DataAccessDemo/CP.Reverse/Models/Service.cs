using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Service
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Flags { get; set; }
        public Nullable<int> GroupID { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public string Tag { get; set; }
    }
}
