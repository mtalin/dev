using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ReservedName
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public System.DateTime DtCreated { get; set; }
    }
}
