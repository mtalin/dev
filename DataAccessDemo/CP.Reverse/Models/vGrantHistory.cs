using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vGrantHistory
    {
        public int RecipientType { get; set; }
        public Nullable<int> GrantID { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public Nullable<System.DateTime> DtTransact { get; set; }
        public Nullable<int> RecipientID { get; set; }
        public string RecipientName { get; set; }
        public string SortRecipientName { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<int> ProgramID { get; set; }
        public Nullable<int> SubProgramID { get; set; }
        public Nullable<int> ParentProgramID { get; set; }
        public string ProgramName { get; set; }
        public string SubProgramName { get; set; }
        public string FullProgramName { get; set; }
        public string SortProgramName { get; set; }
        public Nullable<int> GrantorID { get; set; }
        public string GrantorName { get; set; }
        public string SortGrantorName { get; set; }
        public Nullable<int> GrantProposalID { get; set; }
        public Nullable<int> SpecificPurposeType { get; set; }
        public string SpecificPurposeText1 { get; set; }
        public string SpecificPurposeText2 { get; set; }
        public string SpecificPurposeName { get; set; }
        public string SortSpecificPurposeName { get; set; }
        public string State { get; set; }
        public Nullable<int> TransactID { get; set; }
        public Nullable<int> GrantLetter { get; set; }
        public string Purpose { get; set; }
        public string PurposeDescription { get; set; }
        public int GrantDetails { get; set; }
    }
}
