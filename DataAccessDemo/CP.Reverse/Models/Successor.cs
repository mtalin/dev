using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Successor
    {
        public int ID { get; set; }
        public int Person { get; set; }
        public int President { get; set; }
        public int Foundation { get; set; }
        public string Relationship { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Document { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Person Person2 { get; set; }
    }
}
