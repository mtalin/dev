using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FoundationEvent
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Event { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> BeginDate { get; set; }
        public Nullable<int> BeginBy { get; set; }
        public Nullable<System.DateTime> CompleteDate { get; set; }
        public Nullable<int> CompleteBy { get; set; }
    }
}
