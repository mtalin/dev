using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReceipt
    {
        public TaxReceipt()
        {
            this.Documents = new List<Document1>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Foundation { get; set; }
        public int RYear { get; set; }
        public int Donor { get; set; }
        public Nullable<int> Note { get; set; }
        public int Step { get; set; }
        public Nullable<System.DateTime> DtPrinted { get; set; }
        public Nullable<int> PrintedBy { get; set; }
        public Nullable<System.DateTime> DtRevised { get; set; }
        public Nullable<int> RevisedBy { get; set; }
        public Nullable<int> DeliveredBy { get; set; }
        public virtual Donor Donor1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<Document1> Documents { get; set; }
    }
}
