using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class tmp_GrantHistory
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public System.DateTime DtGrant { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int CharityEIN { get; set; }
        public Nullable<int> Grantor { get; set; }
        public bool Anonymous { get; set; }
        public string Purpose { get; set; }
        public string PurposeDescription { get; set; }
        public Nullable<int> SpecPurposeType { get; set; }
        public string SpecPurposeText1 { get; set; }
        public string SpecPurposeText2 { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public Nullable<int> GrantLetter { get; set; }
        public virtual FoundationProject FoundationProject1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
