using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_Grants
    {
        public Nullable<int> ID { get; set; }
        public Nullable<int> CharityEIN { get; set; }
        public Nullable<int> HouseholdAccount { get; set; }
        public string FoundationName { get; set; }
        public Nullable<int> Person { get; set; }
        public Nullable<int> Author { get; set; }
        public System.DateTime DateCreated { get; set; }
        public decimal DonationAmount { get; set; }
        public int SpecPurposeType { get; set; }
        public string SpecPurposeText1 { get; set; }
        public string SpecPurposeText2 { get; set; }
        public bool SpecialDelivery { get; set; }
        public bool Anonymous { get; set; }
        public string SpecPurposeTypeName { get; set; }
        public string Purpose { get; set; }
        public string PurposeDescription { get; set; }
        public string NoteForEmail { get; set; }
        public int IsSpecPurpose { get; set; }
        public int IsAwaitngApproval { get; set; }
        public Nullable<bool> IsMultiYear { get; set; }
        public int isRecipient { get; set; }
        public int UseRecipientAddress { get; set; }
        public Nullable<int> NumPayments { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<int> PaymentNumber { get; set; }
        public string TargetedDescription { get; set; }
        public string ProgramName { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public Nullable<int> ParentProgram { get; set; }
        public string ShortProgramName { get; set; }
        public string CharityName { get; set; }
        public string CharityAddress { get; set; }
        public string CharityAddress2 { get; set; }
        public string CharityCity { get; set; }
        public string CharityCountry { get; set; }
        public string CharityStateZIPCountry { get; set; }
        public string AltName { get; set; }
        public Nullable<byte> FoundationCode { get; set; }
        public Nullable<bool> FiscalAgent { get; set; }
        public string CheckPayableTo { get; set; }
        public string CheckMemo { get; set; }
        public Nullable<int> DonationID { get; set; }
        public string OfficerName { get; set; }
        public string OfficerAddress { get; set; }
        public string OfficerCityStateZip { get; set; }
        public string AddressOfc1 { get; set; }
        public string AddressOfc2 { get; set; }
        public string OfcCareOf { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string FndCareOf { get; set; }
        public string FndAddress { get; set; }
        public string FndCityStateZip { get; set; }
        public string ReqContactFirstName { get; set; }
        public string ReqContactMiddleName { get; set; }
        public string ReqContactLastName { get; set; }
        public string ReqContactTitle { get; set; }
        public string ReqContactSalutation { get; set; }
        public Nullable<System.DateTime> FiscalEndDate { get; set; }
    }
}
