using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vFoundationContract
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int ServiceProvider { get; set; }
        public Nullable<System.DateTime> DtCommence { get; set; }
        public Nullable<System.DateTime> DtTerminate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> ParentContract { get; set; }
        public int IsMainContract { get; set; }
    }
}
