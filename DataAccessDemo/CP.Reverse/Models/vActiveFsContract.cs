using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vActiveFsContract
    {
        public int ID { get; set; }
        public string ContractNo { get; set; }
        public int ServiceProvider { get; set; }
        public Nullable<int> Customer { get; set; }
        public Nullable<int> ParentContract { get; set; }
        public bool IsActive { get; set; }
        public bool IsTemplate { get; set; }
        public Nullable<System.DateTime> DtCommence { get; set; }
        public Nullable<System.DateTime> DtTerminate { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtActivated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
    }
}
