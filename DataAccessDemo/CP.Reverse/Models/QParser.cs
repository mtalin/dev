using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QParser
    {
        public QParser()
        {
            this.QReports = new List<QReport>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public int Source { get; set; }
        public virtual ICollection<QReport> QReports { get; set; }
    }
}
