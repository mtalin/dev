using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AssetPricesTemp
    {
        public int ID { get; set; }
        public int Asset { get; set; }
        public System.DateTime DtPrice { get; set; }
        public decimal LowPrice { get; set; }
        public decimal HiPrice { get; set; }
        public decimal ClsPrice { get; set; }
        public int PriceAuthor { get; set; }
    }
}
