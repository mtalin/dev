using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class old_NomineesSteps
    {
        public int ID { get; set; }
        public Nullable<int> Nominee { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public int Step { get; set; }
        public Nullable<int> Appointment { get; set; }
        public Nullable<int> TypeOfChange { get; set; }
        public Nullable<int> StaffingProposal { get; set; }
        public System.DateTime DtAccepted { get; set; }
        public Nullable<int> CompanyPosition { get; set; }
        public virtual User User { get; set; }
    }
}
