using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class HouseholdValue
    {
        public int HouseholdType { get; set; }
        public int Type { get; set; }
        public Nullable<int> Persons { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime DtCreated { get; set; }
    }
}
