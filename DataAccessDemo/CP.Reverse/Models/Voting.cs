using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Voting
    {
        public Voting()
        {
            this.Proposals = new List<Proposal>();
            this.VotingResults = new List<VotingResult>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Type { get; set; }
        public int Filter { get; set; }
        public int Step { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<Proposal> Proposals { get; set; }
        public virtual ICollection<VotingResult> VotingResults { get; set; }
    }
}
