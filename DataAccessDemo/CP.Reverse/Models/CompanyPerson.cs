using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CompanyPerson
    {
        public int ID { get; set; }
        public int StructuredCompany { get; set; }
        public int Person { get; set; }
        public int Author { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<decimal> GrantingLimit { get; set; }
        public bool ApproveGrants { get; set; }
        public Nullable<decimal> ApprovableLimit { get; set; }
        public bool GrantCommitteeMember { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Person Person2 { get; set; }
        public virtual Person Person3 { get; set; }
        public virtual StructuredCompany StructuredCompany1 { get; set; }
    }
}
