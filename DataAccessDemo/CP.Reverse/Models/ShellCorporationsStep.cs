using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ShellCorporationsStep
    {
        public int ID { get; set; }
        public Nullable<int> ShellCorporation { get; set; }
        public int Step { get; set; }
        public Nullable<int> Note { get; set; }
        public Nullable<int> Message { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Event { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual ShellCorporation ShellCorporation1 { get; set; }
        public virtual User User { get; set; }
    }
}
