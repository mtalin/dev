using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MQ
    {
        public int ID { get; set; }
        public System.DateTime Created { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
        public System.DateTime LastActive { get; set; }
        public int Status { get; set; }
    }
}
