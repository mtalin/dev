using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class LinkRepository
    {
        public int ID { get; set; }
        public string ParametersLinks { get; set; }
        public string Description { get; set; }
        public int Category { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtExpired { get; set; }
        public Nullable<System.DateTime> DtVisited { get; set; }
        public int Author { get; set; }
        public virtual Person Person { get; set; }
    }
}
