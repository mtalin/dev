using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Invoice
    {
        public Invoice()
        {
            this.InvoicesItems = new List<InvoicesItem>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Company { get; set; }
        public int Author { get; set; }
        public int Step { get; set; }
        public Nullable<int> StructuredCompany { get; set; }
        public virtual Company Company1 { get; set; }
        public virtual StructuredCompany StructuredCompany1 { get; set; }
        public virtual ICollection<InvoicesItem> InvoicesItems { get; set; }
    }
}
