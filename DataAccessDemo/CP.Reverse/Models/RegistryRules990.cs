using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class RegistryRules990
    {
        public int Id { get; set; }
        public byte Part { get; set; }
        public Nullable<short> TransactType { get; set; }
        public Nullable<byte> AssetType { get; set; }
        public short LineId { get; set; }
        public Nullable<short> ColumnA { get; set; }
        public Nullable<short> ColumnB { get; set; }
        public Nullable<short> ColumnC { get; set; }
        public Nullable<short> ColumnD { get; set; }
        public Nullable<short> ColumnE { get; set; }
        public Nullable<short> ColumnF { get; set; }
        public string Line990 { get; set; }
    }
}
