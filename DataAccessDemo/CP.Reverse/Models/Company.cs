using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Company
    {
        public Company()
        {
            this.Companies1 = new List<Company>();
            this.DistributableAmounts = new List<DistributableAmount>();
            this.Donors = new List<Donor>();
            this.FinancialPartnerChunks = new List<FinancialPartnerChunk>();
            this.FinancialPartnerChunks1 = new List<FinancialPartnerChunk>();
            this.Invoices = new List<Invoice>();
            this.PayeeProfiles = new List<PayeeProfile>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DtOpen { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Nullable<int> OriginalCompany { get; set; }
        public bool Draft { get; set; }
        public string Ein { get; set; }
        public virtual PostalAddress PostalAddress1 { get; set; }
        public virtual ICollection<Company> Companies1 { get; set; }
        public virtual Company Company1 { get; set; }
        public virtual ICollection<DistributableAmount> DistributableAmounts { get; set; }
        public virtual ICollection<Donor> Donors { get; set; }
        public virtual ICollection<FinancialPartnerChunk> FinancialPartnerChunks { get; set; }
        public virtual ICollection<FinancialPartnerChunk> FinancialPartnerChunks1 { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual ICollection<PayeeProfile> PayeeProfiles { get; set; }
    }
}
