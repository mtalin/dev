using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ExpenseLocation
    {
        public int ID { get; set; }
        public int State { get; set; }
        public string Location { get; set; }
        public System.DateTime DtFrom { get; set; }
        public System.DateTime DtTo { get; set; }
        public bool Active { get; set; }
    }
}
