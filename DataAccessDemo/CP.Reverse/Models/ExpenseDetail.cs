using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ExpenseDetail
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> Category { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> Type { get; set; }
        public string TypeName { get; set; }
        public Nullable<int> Officer { get; set; }
        public Nullable<int> ContactInfo { get; set; }
        public Nullable<int> PayeeProfile { get; set; }
        public Nullable<int> Company { get; set; }
        public string PayeeName { get; set; }
        public string Description { get; set; }
        public decimal AssetsAmount { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<int> PendingTransact { get; set; }
        public int Step { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Document { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public string SortPayeeName { get; set; }
        public string InvoiceNumber { get; set; }
        public Nullable<int> InvoiceDocument { get; set; }
        public Nullable<int> Template { get; set; }
        public Nullable<int> TimeSchedule { get; set; }
    }
}
