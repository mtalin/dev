using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FoundationProject
    {
        public FoundationProject()
        {
            this.Expenses = new List<Expens>();
            this.FoundationProjects1 = new List<FoundationProject>();
            this.GrantDetails = new List<GrantDetail>();
            this.tmp_GrantHistory = new List<tmp_GrantHistory>();
            this.HouseholdAccounts = new List<HouseholdAccount>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public string Name { get; set; }
        public Nullable<int> Parent { get; set; }
        public string Code { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> AuthorDeleted { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public string Description { get; set; }
        public Nullable<int> Type { get; set; }
        public virtual ICollection<Expens> Expenses { get; set; }
        public virtual ICollection<FoundationProject> FoundationProjects1 { get; set; }
        public virtual FoundationProject FoundationProject1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<GrantDetail> GrantDetails { get; set; }
        public virtual ICollection<tmp_GrantHistory> tmp_GrantHistory { get; set; }
        public virtual ICollection<HouseholdAccount> HouseholdAccounts { get; set; }
    }
}
