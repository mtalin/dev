using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vFoundationServiceProvider
    {
        public int Foundation { get; set; }
        public int ServiceProvider { get; set; }
        public string ServiceProviderName { get; set; }
        public int ServiceProviderCompany { get; set; }
    }
}
