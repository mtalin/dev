using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AbbreviateAddress
    {
        public string Abbrev { get; set; }
        public string FullName { get; set; }
    }
}
