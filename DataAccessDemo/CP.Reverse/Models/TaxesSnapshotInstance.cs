using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxesSnapshotInstance
    {
        public TaxesSnapshotInstance()
        {
            this.TaxesSnapshots = new List<TaxesSnapshot>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual ICollection<TaxesSnapshot> TaxesSnapshots { get; set; }
    }
}
