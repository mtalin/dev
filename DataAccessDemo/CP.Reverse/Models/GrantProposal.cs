using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GrantProposal
    {
        public int ID { get; set; }
        public int Proposal { get; set; }
        public int GrantDetails { get; set; }
        public bool Excluded { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public virtual GrantDetail GrantDetail { get; set; }
        public virtual Person Person { get; set; }
        public virtual Proposal Proposal1 { get; set; }
    }
}
