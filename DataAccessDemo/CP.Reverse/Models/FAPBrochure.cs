using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FAPBrochure
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] PDF { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public int SortOrder { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public virtual Person Person { get; set; }
    }
}
