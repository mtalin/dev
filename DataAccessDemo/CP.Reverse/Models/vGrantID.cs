using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vGrantID
    {
        public Nullable<int> GrantDetails { get; set; }
        public int ActivityType { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Step { get; set; }
        public Nullable<int> PendingTransact { get; set; }
        public Nullable<int> Transact { get; set; }
    }
}
