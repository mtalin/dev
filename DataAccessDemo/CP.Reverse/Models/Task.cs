using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Task
    {
        public int ID { get; set; }
        public int Author { get; set; }
        public int State { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtStart { get; set; }
        public Nullable<System.DateTime> DtStop { get; set; }
        public Nullable<System.DateTime> DtNext { get; set; }
        public int Template { get; set; }
        public string PredAction { get; set; }
        public string PredParam { get; set; }
        public string BodyAction { get; set; }
        public string BodyParam { get; set; }
        public string PostAction { get; set; }
        public string PostParam { get; set; }
        public Nullable<int> ReturnCode { get; set; }
        public string TaskParam { get; set; }
    }
}
