using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_Nominees
    {
        public int ID { get; set; }
        public int Person { get; set; }
        public Nullable<int> Appointment { get; set; }
        public int Foundation { get; set; }
        public string PositionName { get; set; }
        public string RoleName { get; set; }
    }
}
