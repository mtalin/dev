using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ManualPayment
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Payment { get; set; }
        public int Type { get; set; }
        public Nullable<int> Payee { get; set; }
        public virtual CheckPayee CheckPayee { get; set; }
        public virtual Person Person { get; set; }
        public virtual Payment Payment1 { get; set; }
    }
}
