using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Transact
    {
        public Transact()
        {
            this.AssetValuations = new List<AssetValuation>();
            this.CostBases = new List<CostBas>();
            this.PendingTransacts = new List<PendingTransact>();
            this.TransactNotes = new List<TransactNote>();
            this.TransactsDonors = new List<TransactsDonor>();
        }

        public int ID { get; set; }
        public int FinancialPartner { get; set; }
        public int Foundation { get; set; }
        public int FoundationAccount { get; set; }
        public string FPTransactID { get; set; }
        public System.DateTime DtTransact { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public Nullable<decimal> Principal { get; set; }
        public Nullable<decimal> Commision { get; set; }
        public decimal Tax { get; set; }
        public decimal SecFee { get; set; }
        public decimal MiscFee { get; set; }
        public decimal OthFee { get; set; }
        public decimal ClearingCharge { get; set; }
        public decimal BrokerageCharge { get; set; }
        public decimal InvestValue { get; set; }
        public Nullable<bool> Taxable { get; set; }
        public Nullable<decimal> UnitsNumber { get; set; }
        public Nullable<int> SplitNumerator { get; set; }
        public Nullable<int> SplitDenominator { get; set; }
        public string MergeTicker { get; set; }
        public Nullable<double> Ratio { get; set; }
        public System.DateTime DtCreated { get; set; }
        public bool Restricted { get; set; }
        public bool Hold { get; set; }
        public string Notes { get; set; }
        public Nullable<int> QTransaction { get; set; }
        public decimal Interest { get; set; }
        public Nullable<int> Asset { get; set; }
        public Nullable<int> Asset2 { get; set; }
        public Nullable<decimal> CostBasis { get; set; }
        public Nullable<decimal> BookBasis { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public Nullable<short> FiscalYear { get; set; }
        public Nullable<System.DateTime> dtDeposit { get; set; }
        public bool Ignore { get; set; }
        public virtual Asset Asset1 { get; set; }
        public virtual Asset Asset3 { get; set; }
        public virtual ICollection<AssetValuation> AssetValuations { get; set; }
        public virtual ICollection<CostBas> CostBases { get; set; }
        public virtual FoundationAccount FoundationAccount1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<PendingTransact> PendingTransacts { get; set; }
        public virtual QTransaction QTransaction1 { get; set; }
        public virtual ICollection<TransactNote> TransactNotes { get; set; }
        public virtual ICollection<TransactsDonor> TransactsDonors { get; set; }
    }
}
