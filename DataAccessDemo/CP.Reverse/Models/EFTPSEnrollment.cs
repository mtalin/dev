using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EFTPSEnrollment
    {
        public EFTPSEnrollment()
        {
            this.EFTPSEnrollmentsSteps = new List<EFTPSEnrollmentsStep>();
        }

        public int ID { get; set; }
        public Nullable<System.DateTime> DtCreated { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> CheckingAccount { get; set; }
        public int Step { get; set; }
        public string EFTPSPin { get; set; }
        public virtual CheckingAccount CheckingAccount1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<EFTPSEnrollmentsStep> EFTPSEnrollmentsSteps { get; set; }
    }
}
