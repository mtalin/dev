using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ActivePricingsOfQuaterlyFeeFromSnapshot
    {
        public int ID { get; set; }
        public Nullable<int> PsID { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> DtStart { get; set; }
        public Nullable<System.DateTime> DtEnd { get; set; }
        public Nullable<decimal> Limit { get; set; }
        public Nullable<decimal> Addend { get; set; }
        public Nullable<float> Multiplier { get; set; }
        public Nullable<decimal> From { get; set; }
        public Nullable<decimal> To { get; set; }
    }
}
