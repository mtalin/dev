using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EventAction
    {
        public int ID { get; set; }
        public int EventClass { get; set; }
        public int JobClass { get; set; }
        public int Priority { get; set; }
        public virtual EventClass EventClass1 { get; set; }
        public virtual JobClass JobClass1 { get; set; }
    }
}
