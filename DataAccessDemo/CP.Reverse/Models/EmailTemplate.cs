using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EmailTemplate
    {
        public EmailTemplate()
        {
            this.CRMTasks = new List<CRMTask>();
            this.EmailRecipients = new List<EmailRecipient>();
            this.EmailTemplates1 = new List<EmailTemplate>();
            this.ExcludedEmailApplicants = new List<ExcludedEmailApplicant>();
            this.Messages = new List<Message>();
        }

        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Parent { get; set; }
        public int GenericEmail { get; set; }
        public bool Active { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public Nullable<int> FinancialAdvisorGroup { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> Person { get; set; }
        public bool isPlaceHolder { get; set; }
        public int JobSubType { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public Nullable<int> TaskCategory { get; set; }
        public Nullable<int> DueDays { get; set; }
        public Nullable<int> ResponsibleDepartment { get; set; }
        public short PriorityLow { get; set; }
        public short PriorityMedium { get; set; }
        public short PriorityHigh { get; set; }
        public short PriorityCritical { get; set; }
        public bool AlwaysEmail { get; set; }
        public virtual ICollection<CRMTask> CRMTasks { get; set; }
        public virtual Department Department { get; set; }
        public virtual ICollection<EmailRecipient> EmailRecipients { get; set; }
        public virtual ICollection<EmailTemplate> EmailTemplates1 { get; set; }
        public virtual EmailTemplate EmailTemplate1 { get; set; }
        public virtual FinancialAdvisorGroup FinancialAdvisorGroup1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual GenericEmail GenericEmail1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ExcludedEmailApplicant> ExcludedEmailApplicants { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
    }
}
