using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EventRecipient
    {
        public int ID { get; set; }
        public int EventClass { get; set; }
        public string XSLScript { get; set; }
        public string Flags { get; set; }
        public virtual EventClass EventClass1 { get; set; }
        public virtual Recipient Recipient { get; set; }
    }
}
