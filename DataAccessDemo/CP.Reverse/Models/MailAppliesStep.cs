using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MailAppliesStep
    {
        public int ID { get; set; }
        public int MailApply { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public int Step { get; set; }
        public Nullable<int> Message { get; set; }
        public Nullable<int> Note { get; set; }
        public Nullable<int> Event { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual MailApply MailApply1 { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual User User { get; set; }
        public virtual MailAppliesStepsNote MailAppliesStepsNote { get; set; }
        public virtual Message Message2 { get; set; }
    }
}
