using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Check
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Payment { get; set; }
        public string CheckNo { get; set; }
        public System.DateTime CheckDate { get; set; }
        public Nullable<System.DateTime> DtProcessed { get; set; }
        public Nullable<int> ProcessedBy { get; set; }
        public Nullable<System.DateTime> DtVoid { get; set; }
        public Nullable<int> VoidBy { get; set; }
        public string VoidNote { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<int> VoidType { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Payment Payment1 { get; set; }
        public virtual Person Person2 { get; set; }
        public virtual Person Person3 { get; set; }
    }
}
