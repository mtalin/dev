using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GrantApplication
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public string ApplicationName { get; set; }
        public string Template { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtSubmitted { get; set; }
        public string Applicant { get; set; }
        public Nullable<decimal> RequestedAmount { get; set; }
        public Nullable<decimal> ReceivedAmount { get; set; }
        public string XMLData { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
