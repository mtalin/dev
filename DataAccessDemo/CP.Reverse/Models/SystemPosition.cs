using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class SystemPosition
    {
        public SystemPosition()
        {
            this.StaffingPolicies = new List<StaffingPolicy>();
            this.StaffingPoliciesTemplates = new List<StaffingPoliciesTemplate>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> dtDeleted { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public string Flags { get; set; }
        public Nullable<int> ObjectPermissions { get; set; }
        public virtual ObjectsPermission ObjectsPermission { get; set; }
        public virtual ICollection<StaffingPolicy> StaffingPolicies { get; set; }
        public virtual ICollection<StaffingPoliciesTemplate> StaffingPoliciesTemplates { get; set; }
        public virtual User User { get; set; }
    }
}
