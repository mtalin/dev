using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PartnersInfo
    {
        public int Id { get; set; }
        public Nullable<int> AccountingStandard { get; set; }
        public Nullable<int> EntityType { get; set; }
        public Nullable<bool> Final { get; set; }
        public Nullable<bool> Amended { get; set; }
        public Nullable<bool> InvestInPtp { get; set; }
        public Nullable<bool> InvestInForeign { get; set; }
        public Nullable<bool> InterestSold { get; set; }
        public Nullable<bool> Form8886Disclosed { get; set; }
        public Nullable<bool> ManualVerified { get; set; }
        public Nullable<bool> UBIMoreThousand { get; set; }
        public Nullable<bool> OrdinaryGain { get; set; }
        public Nullable<decimal> BeginProfit { get; set; }
        public Nullable<decimal> EndProfit { get; set; }
        public Nullable<decimal> BeginLoss { get; set; }
        public Nullable<decimal> EndLoss { get; set; }
        public Nullable<decimal> BeginCapital { get; set; }
        public Nullable<decimal> EndCapital { get; set; }
        public Nullable<decimal> Nonrecourse { get; set; }
        public Nullable<decimal> QualifiedNonrecourse { get; set; }
        public Nullable<decimal> Recourse { get; set; }
        public Nullable<decimal> BeginCapitalAccount { get; set; }
        public Nullable<decimal> EndCapitalAccount { get; set; }
        public Nullable<decimal> WD { get; set; }
        public Nullable<decimal> CapitalContributedDuringYear { get; set; }
        public Nullable<decimal> CurrentYearChanges { get; set; }
        public Nullable<bool> ContributeBuiltInChanges { get; set; }
        public virtual Partnership Partnership { get; set; }
    }
}
