using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class old_TaxesItems
    {
        public int ID { get; set; }
        public int Tax { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public decimal Amount2 { get; set; }
        public virtual Tax Tax1 { get; set; }
    }
}
