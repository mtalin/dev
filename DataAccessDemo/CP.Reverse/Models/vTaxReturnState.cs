using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vTaxReturnState
    {
        public int Foundation { get; set; }
        public string State1 { get; set; }
        public string State2 { get; set; }
        public string State3 { get; set; }
    }
}
