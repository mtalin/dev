using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MQout
    {
        public int ID { get; set; }
        public System.DateTime Created { get; set; }
        public string Error { get; set; }
        public virtual MQin MQin { get; set; }
    }
}
