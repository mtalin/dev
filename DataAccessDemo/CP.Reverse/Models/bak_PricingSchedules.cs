using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class bak_PricingSchedules
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string ScheduleNumber { get; set; }
        public int ServiceProvider { get; set; }
        public int Type { get; set; }
        public int PricingSource { get; set; }
        public Nullable<int> BillingEvent { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
    }
}
