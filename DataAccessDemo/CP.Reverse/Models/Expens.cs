using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Expens
    {
        public Expens()
        {
            this.ExpenseEntries = new List<ExpenseEntry>();
            this.ExpenseOfficers = new List<ExpenseOfficer>();
            this.Expenses1 = new List<Expens>();
            this.ExpensesSteps = new List<ExpensesStep>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> ContactInfo { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public decimal AssetsAmount { get; set; }
        public int Step { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> PendingTransact { get; set; }
        public Nullable<int> Document { get; set; }
        public Nullable<int> FoundationProject { get; set; }
        public string InvoiceNumber { get; set; }
        public Nullable<int> InvoiceDocument { get; set; }
        public Nullable<int> PayeeProfile { get; set; }
        public Nullable<int> ExpenseReport { get; set; }
        public Nullable<int> AdditionalFiling { get; set; }
        public decimal AdditionalAmount { get; set; }
        public Nullable<int> ReimbursableTo { get; set; }
        public Nullable<int> ReimbursableContactInfo { get; set; }
        public string SpecInstruct { get; set; }
        public bool IsSentEmail { get; set; }
        public Nullable<int> Template { get; set; }
        public Nullable<int> TimeSchedule { get; set; }
        public virtual Document1 Document1 { get; set; }
        public virtual Document1 Document2 { get; set; }
        public virtual ICollection<ExpenseEntry> ExpenseEntries { get; set; }
        public virtual ICollection<ExpenseOfficer> ExpenseOfficers { get; set; }
        public virtual ExpenseReport ExpenseReport1 { get; set; }
        public virtual PostalAddress PostalAddress { get; set; }
        public virtual FoundationProject FoundationProject1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual PayeeProfile PayeeProfile1 { get; set; }
        public virtual PayeeProfile PayeeProfile2 { get; set; }
        public virtual PendingTransact PendingTransact1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual PostalAddress PostalAddress1 { get; set; }
        public virtual ICollection<Expens> Expenses1 { get; set; }
        public virtual Expens Expens1 { get; set; }
        public virtual TimeSchedule TimeSchedule1 { get; set; }
        public virtual ICollection<ExpensesStep> ExpensesSteps { get; set; }
    }
}
