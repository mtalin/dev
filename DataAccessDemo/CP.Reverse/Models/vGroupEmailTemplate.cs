using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vGroupEmailTemplate
    {
        public int ID { get; set; }
        public int FinancialAdvisorGroup { get; set; }
        public string Name { get; set; }
        public int EventClass { get; set; }
        public string TriggerName { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Parent { get; set; }
        public int GenericEmail { get; set; }
        public bool Active { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string FromAddress { get; set; }
        public int Type { get; set; }
        public string TypeName { get; set; }
        public int JobSubType { get; set; }
    }
}
