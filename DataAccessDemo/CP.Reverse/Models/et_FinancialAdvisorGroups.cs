using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_FinancialAdvisorGroups
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public int FinancialPartner { get; set; }
        public string Phone1 { get; set; }
        public Nullable<int> Phone2 { get; set; }
        public string Fax { get; set; }
        public Nullable<int> Email { get; set; }
        public Nullable<int> PrimaryContact { get; set; }
    }
}
