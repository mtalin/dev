using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Subtask
    {
        public int Task { get; set; }
        public int Subtask1 { get; set; }
        public Nullable<int> SeqOrder { get; set; }
        public virtual Tasks_Old Tasks_Old { get; set; }
        public virtual Tasks_Old Tasks_Old1 { get; set; }
    }
}
