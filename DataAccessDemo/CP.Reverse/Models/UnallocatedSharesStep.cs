using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class UnallocatedSharesStep
    {
        public int ID { get; set; }
        public Nullable<int> QDeposit { get; set; }
        public Nullable<int> TrSecurity { get; set; }
        public int Step { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public Nullable<int> Message { get; set; }
        public Nullable<int> Note { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Note Note1 { get; set; }
    }
}
