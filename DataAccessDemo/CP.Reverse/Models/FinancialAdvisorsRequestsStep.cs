using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FinancialAdvisorsRequestsStep
    {
        public int ID { get; set; }
        public int FinancialAdvisorsRequest { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> Author { get; set; }
        public int Step { get; set; }
        public Nullable<int> Message { get; set; }
        public Nullable<int> Note { get; set; }
        public virtual FinancialAdvisorsRequest FinancialAdvisorsRequest1 { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Note Note1 { get; set; }
    }
}
