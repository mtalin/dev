using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class RegistryRulesStatement
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public byte PartId { get; set; }
        public short LineId { get; set; }
        public Nullable<short> TransactType { get; set; }
        public Nullable<byte> AssetType { get; set; }
        public Nullable<short> RegistryRule { get; set; }
    }
}
