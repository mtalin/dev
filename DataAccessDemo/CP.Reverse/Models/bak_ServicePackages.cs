using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class bak_ServicePackages
    {
        public int ID { get; set; }
        public int ServiceProvider { get; set; }
        public string Name { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
    }
}
