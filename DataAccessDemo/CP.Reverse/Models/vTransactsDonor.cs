using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vTransactsDonor
    {
        public int ID { get; set; }
        public int Transact { get; set; }
        public Nullable<decimal> ChildAmount { get; set; }
        public Nullable<int> Donor { get; set; }
        public Nullable<int> PhantomDonor { get; set; }
        public Nullable<int> Spouse { get; set; }
        public string InCareOf { get; set; }
        public Nullable<bool> IsDonorAlive { get; set; }
        public Nullable<bool> IsSpouseAlive { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<decimal> GoodsServices { get; set; }
        public int Foundation { get; set; }
        public System.DateTime dtTransact { get; set; }
        public Nullable<short> FiscalYear { get; set; }
        public int type { get; set; }
        public Nullable<int> asset { get; set; }
        public Nullable<decimal> unitsNumber { get; set; }
        public Nullable<decimal> ActualAmount { get; set; }
    }
}
