using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class qd_YNQuestions
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Questionnaire { get; set; }
        public int Section { get; set; }
        public Nullable<int> Question { get; set; }
        public bool Checked { get; set; }
        public string Comments { get; set; }
        public string ResolveNote { get; set; }
    }
}
