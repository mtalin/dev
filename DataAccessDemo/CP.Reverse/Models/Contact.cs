using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Contact
    {
        public Contact()
        {
            this.ContactsSteps = new List<ContactsStep>();
        }

        public int ID { get; set; }
        public Nullable<int> Person { get; set; }
        public Nullable<int> Foundation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string Comments { get; set; }
        public bool Post { get; set; }
        public bool Phone { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Step { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> DtAlert { get; set; }
        public string Organization { get; set; }
        public string Country { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<ContactsStep> ContactsSteps { get; set; }
    }
}
