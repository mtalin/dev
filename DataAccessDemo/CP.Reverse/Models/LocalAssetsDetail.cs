using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class LocalAssetsDetail
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Name4 { get; set; }
        public int Type { get; set; }
        public string Symbol { get; set; }
        public string CUSIP { get; set; }
        public string TypeFlags { get; set; }
        public string TypeName { get; set; }
        public string CategoryName { get; set; }
        public int Foundation { get; set; }
        public string FoundationName { get; set; }
        public Nullable<int> AssetBook { get; set; }
        public string Description { get; set; }
        public Nullable<bool> UBTI { get; set; }
        public Nullable<int> UBTIDocument { get; set; }
        public Nullable<int> UBTINote { get; set; }
        public Nullable<System.DateTime> DtExpired { get; set; }
        public Nullable<System.DateTime> DtLastAcquisition { get; set; }
        public string LastAcqMethod { get; set; }
        public Nullable<System.DateTime> DtLastValuation { get; set; }
        public float InvPercent { get; set; }
        public float ChrPercent { get; set; }
    }
}
