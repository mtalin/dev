using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CheckingAccount
    {
        public CheckingAccount()
        {
            this.DisbursementAccounts = new List<DisbursementAccount>();
            this.EFTPSEnrollmentsSteps = new List<EFTPSEnrollmentsStep>();
            this.EFTPSEnrollments = new List<EFTPSEnrollment>();
            this.FoundationAccounts = new List<FoundationAccount>();
            this.Payments = new List<Payment>();
        }

        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public string BatchFilerID { get; set; }
        public string MasterInquiryPIN { get; set; }
        public Nullable<int> AccountType { get; set; }
        public Nullable<int> LinkedBank { get; set; }
        public string LinkedBankAddress { get; set; }
        public string LinkedAccountName { get; set; }
        public string LinkedAccountNumber { get; set; }
        public string LinkedRoutingNumber { get; set; }
        public Nullable<int> NextCheckNo { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ServiceProvider ServiceProvider { get; set; }
        public virtual ICollection<DisbursementAccount> DisbursementAccounts { get; set; }
        public virtual ICollection<EFTPSEnrollmentsStep> EFTPSEnrollmentsSteps { get; set; }
        public virtual ICollection<EFTPSEnrollment> EFTPSEnrollments { get; set; }
        public virtual ICollection<FoundationAccount> FoundationAccounts { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
    }
}
