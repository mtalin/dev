using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EventClass
    {
        public EventClass()
        {
            this.CRMTasks = new List<CRMTask>();
            this.EventActions = new List<EventAction>();
            this.EventRecipients = new List<EventRecipient>();
            this.Events = new List<Event>();
            this.GenericEmails = new List<GenericEmail>();
            this.TimeSchedules = new List<TimeSchedule>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string EventData { get; set; }
        public int EventSource { get; set; }
        public int EventType { get; set; }
        public bool Active { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual ICollection<CRMTask> CRMTasks { get; set; }
        public virtual ICollection<EventAction> EventActions { get; set; }
        public virtual EventSource EventSource1 { get; set; }
        public virtual EventType EventType1 { get; set; }
        public virtual ICollection<EventRecipient> EventRecipients { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<GenericEmail> GenericEmails { get; set; }
        public virtual ICollection<TimeSchedule> TimeSchedules { get; set; }
    }
}
