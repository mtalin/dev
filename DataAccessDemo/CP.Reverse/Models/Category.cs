using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Category
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}
