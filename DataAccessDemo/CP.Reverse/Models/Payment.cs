using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Payment
    {
        public Payment()
        {
            this.Checks = new List<Check>();
            this.EFTPSPayments = new List<EFTPSPayment>();
            this.ManualPayments = new List<ManualPayment>();
            this.PaymentsSteps = new List<PaymentsStep>();
            this.PendingTransacts = new List<PendingTransact>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public Nullable<int> Foundation { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public int Step { get; set; }
        public Nullable<int> CheckingAccount { get; set; }
        public string PendingTransactData { get; set; }
        public string Memo { get; set; }
        public string Notes { get; set; }
        public Nullable<bool> IsGrouped { get; set; }
        public string PayableTo { get; set; }
        public bool IsModified { get; set; }
        public bool ModRequired { get; set; }
        public string ModReason { get; set; }
        public Nullable<int> ProcessingOffice { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public string PayeeLine2 { get; set; }
        public Nullable<int> Batch { get; set; }
        public virtual CheckingAccount CheckingAccount1 { get; set; }
        public virtual ICollection<Check> Checks { get; set; }
        public virtual ICollection<EFTPSPayment> EFTPSPayments { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<ManualPayment> ManualPayments { get; set; }
        public virtual Person Person { get; set; }
        public virtual PaymentsBatch PaymentsBatch { get; set; }
        public virtual PostalAddress PostalAddress1 { get; set; }
        public virtual ICollection<PaymentsStep> PaymentsSteps { get; set; }
        public virtual ICollection<PendingTransact> PendingTransacts { get; set; }
    }
}
