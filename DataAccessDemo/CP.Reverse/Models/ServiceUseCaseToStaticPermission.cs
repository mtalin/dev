using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ServiceUseCaseToStaticPermission
    {
        public int ServiceUseCase { get; set; }
        public int StaticPermission { get; set; }
        public string Value { get; set; }
        public byte Priority { get; set; }
    }
}
