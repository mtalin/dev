using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ContactsStep
    {
        public int ID { get; set; }
        public int Contact { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Step { get; set; }
        public Nullable<int> Message { get; set; }
        public Nullable<int> Note { get; set; }
        public Nullable<int> Event { get; set; }
        public virtual Contact Contact1 { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual Message Message1 { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual User User { get; set; }
        public virtual ContactsStepsContact ContactsStepsContact { get; set; }
        public virtual ContactsStepsNote ContactsStepsNote { get; set; }
        public virtual Message Message2 { get; set; }
    }
}
