using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FinancialAdvisorsFoundation
    {
        public int FinancialAdvisor { get; set; }
        public int Foundation { get; set; }
        public int Role { get; set; }
        public virtual FinancialAdvisor FinancialAdvisor1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
