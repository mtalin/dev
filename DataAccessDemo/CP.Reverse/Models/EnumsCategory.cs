using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EnumsCategory
    {
        public EnumsCategory()
        {
            this.Enums = new List<Enum>();
            this.EnumsCategories1 = new List<EnumsCategory>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> Parent { get; set; }
        public string Path { get; set; }
        public virtual ICollection<Enum> Enums { get; set; }
        public virtual ICollection<EnumsCategory> EnumsCategories1 { get; set; }
        public virtual EnumsCategory EnumsCategory1 { get; set; }
    }
}
