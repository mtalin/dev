using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class RightsGroup
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string Name { get; set; }
        public string Rights { get; set; }
    }
}
