using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CertifiedMail
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string TrackingNumber { get; set; }
        public int Document { get; set; }
        public Nullable<System.DateTime> DtSent { get; set; }
        public Nullable<System.DateTime> DtResponded { get; set; }
        public bool ReceiptFiled { get; set; }
        public bool ResponseReceived { get; set; }
        public bool ResponseFiled { get; set; }
        public virtual Document1 Document1 { get; set; }
    }
}
