using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReturnIssuesStep
    {
        public int TaxReturnIssue { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Step { get; set; }
        public Nullable<int> PrevStep { get; set; }
        public virtual Person Person { get; set; }
        public virtual TaxReturnIssue TaxReturnIssue1 { get; set; }
    }
}
