using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_RoleKeepers
    {
        public int Role { get; set; }
        public int RoleCode { get; set; }
        public Nullable<int> Person { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> FinancialAdvisorGroup { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
    }
}
