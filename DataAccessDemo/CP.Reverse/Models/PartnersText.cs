using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PartnersText
    {
        public int FinancialPartner { get; set; }
        public string Name { get; set; }
        public string TextValue { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
