using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vGrantActivity
    {
        public int Nominee { get; set; }
        public int CompanyPersonId { get; set; }
        public int Person { get; set; }
        public Nullable<int> Foundation { get; set; }
        public int StructuredCompany { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<decimal> Made { get; set; }
        public Nullable<decimal> Completed { get; set; }
        public Nullable<decimal> Pending { get; set; }
        public Nullable<decimal> WaitingRequest { get; set; }
        public Nullable<decimal> WaitingApproval { get; set; }
        public Nullable<decimal> Remaining { get; set; }
        public Nullable<decimal> GrantingLimit { get; set; }
        public bool ApproveGrants { get; set; }
        public bool GrantCommitteeMember { get; set; }
    }
}
