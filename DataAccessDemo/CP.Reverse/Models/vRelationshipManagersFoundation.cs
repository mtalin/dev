using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vRelationshipManagersFoundation
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int StructuredCompany { get; set; }
        public string StructuredCompanyName { get; set; }
        public string Position { get; set; }
        public int CompanyPosition { get; set; }
        public int Foundation { get; set; }
    }
}
