using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DocumentsDetail
    {
        public int ID { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> ShellCorporation { get; set; }
        public int Theme { get; set; }
        public string Extention { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtReceived { get; set; }
        public Nullable<System.DateTime> DtBookMark { get; set; }
        public string Notes { get; set; }
        public string Title { get; set; }
        public string ShortTitle { get; set; }
        public string TitleWithDetails { get; set; }
        public string ShortTitleWithDetails { get; set; }
        public string Flags { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public string SortName { get; set; }
        public int ItemID { get; set; }
        public string TitleName { get; set; }
        public Nullable<int> GroupID { get; set; }
    }
}
