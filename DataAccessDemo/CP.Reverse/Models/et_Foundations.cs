using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_Foundations
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> Founder { get; set; }
        public Nullable<int> President { get; set; }
        public bool isSSORequired { get; set; }
        public Nullable<int> FoundationRep { get; set; }
        public Nullable<int> RelationshipManager { get; set; }
        public string DesignCode { get; set; }
        public Nullable<int> SalesRep { get; set; }
        public Nullable<int> PrimaryTaxMailing { get; set; }
        public Nullable<int> SecondaryTaxMailing { get; set; }
        public Nullable<int> PrimaryExpReimbursement { get; set; }
        public Nullable<int> SecondaryExpReimbursement { get; set; }
    }
}
