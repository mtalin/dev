using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Session
    {
        public int ID { get; set; }
        public System.DateTime dtCreated { get; set; }
        public Nullable<System.DateTime> dtClosed { get; set; }
    }
}
