using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QCapitalGain
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Foundation { get; set; }
        public string Security { get; set; }
        public int Shares { get; set; }
        public System.DateTime DtBought { get; set; }
        public System.DateTime DtSold { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal CostBasis { get; set; }
        public decimal RealizedGain { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
