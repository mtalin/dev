using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Crossing
    {
        public int ID { get; set; }
        public string AuthorInfo { get; set; }
        public Nullable<int> TrustedAction { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual TrustedAction TrustedAction1 { get; set; }
    }
}
