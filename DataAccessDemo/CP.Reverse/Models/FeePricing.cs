using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class FeePricing
    {
        public int ID { get; set; }
        public int PricingScheduleInstance { get; set; }
        public int FeeType { get; set; }
        public decimal Amount { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual PricingScheduleInstance PricingScheduleInstance1 { get; set; }
    }
}
