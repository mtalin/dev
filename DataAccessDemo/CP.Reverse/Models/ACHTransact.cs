using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ACHTransact
    {
        public ACHTransact()
        {
            this.PendingTransacts = new List<PendingTransact>();
        }

        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime dtCreated { get; set; }
        public int Foundation { get; set; }
        public int Step { get; set; }
        public decimal Amount { get; set; }
        public virtual Person Person { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual ICollection<PendingTransact> PendingTransacts { get; set; }
    }
}
