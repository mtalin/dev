using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vServiceProvider
    {
        public int ID { get; set; }
        public int StructuredCompany { get; set; }
        public string URL { get; set; }
        public bool SingleSignOn { get; set; }
        public string Design { get; set; }
        public string CustomerServiceContacts { get; set; }
        public string CustomContactsHTML { get; set; }
        public string Abbrev { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string Name { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Nullable<int> ObjectPermissions { get; set; }
    }
}
