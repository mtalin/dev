using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GenericEmail
    {
        public GenericEmail()
        {
            this.EmailTemplates = new List<EmailTemplate>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public int EventClass { get; set; }
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
        public virtual EventClass EventClass1 { get; set; }
    }
}
