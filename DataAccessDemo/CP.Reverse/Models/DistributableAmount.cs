using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class DistributableAmount
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Year { get; set; }
        public Nullable<decimal> DA { get; set; }
        public Nullable<decimal> UI { get; set; }
        public Nullable<decimal> EGC { get; set; }
        public Nullable<decimal> NCUA { get; set; }
        public Nullable<decimal> AQD { get; set; }
        public Nullable<decimal> C1_NII { get; set; }
        public Nullable<decimal> QD { get; set; }
        public Nullable<decimal> TOC { get; set; }
        public Nullable<decimal> RQE { get; set; }
        public Nullable<decimal> DAB { get; set; }
        public Nullable<decimal> DAE { get; set; }
        public Nullable<decimal> ETY { get; set; }
        public Nullable<decimal> ProjectedUI { get; set; }
        public Nullable<decimal> ProjectedDA { get; set; }
        public Nullable<int> UI_Year1 { get; set; }
        public Nullable<int> UI_Year2 { get; set; }
        public Nullable<int> UI_Year3 { get; set; }
        public Nullable<int> UI_Year4 { get; set; }
        public Nullable<int> UISP_Year1 { get; set; }
        public Nullable<int> UISP_Year2 { get; set; }
        public Nullable<int> UISP_Year3 { get; set; }
        public Nullable<int> UISP_Year4 { get; set; }
        public Nullable<decimal> TOTAL_UI { get; set; }
        public Nullable<int> UIA_Year1 { get; set; }
        public Nullable<int> UIA_Year2 { get; set; }
        public Nullable<int> UIA_Year3 { get; set; }
        public Nullable<int> UIA_Year4 { get; set; }
        public Nullable<decimal> UIA_Amount1 { get; set; }
        public Nullable<decimal> UIA_Amount2 { get; set; }
        public Nullable<decimal> UIA_Amount3 { get; set; }
        public Nullable<decimal> UIA_Amount4 { get; set; }
        public Nullable<bool> Percent_NII { get; set; }
        public Nullable<decimal> TotalDonations { get; set; }
        public Nullable<System.DateTime> Dt990SentIRS { get; set; }
        public Nullable<System.DateTime> TaxPaymentQuarter { get; set; }
        public Nullable<int> Responsible { get; set; }
        public Nullable<System.DateTime> DtPostEntry { get; set; }
        public Nullable<bool> FiledByClient { get; set; }
        public Nullable<bool> Outsourced990PF { get; set; }
        public Nullable<int> OutsourcingCompany { get; set; }
        public Nullable<bool> Q1TaxesResponsible { get; set; }
        public Nullable<bool> Q2TaxesResponsible { get; set; }
        public Nullable<bool> Q3TaxesResponsible { get; set; }
        public Nullable<bool> Q4TaxesResponsible { get; set; }
        public Nullable<decimal> Q1TaxesAmount { get; set; }
        public Nullable<decimal> Q2TaxesAmount { get; set; }
        public Nullable<decimal> Q3TaxesAmount { get; set; }
        public Nullable<decimal> Q4TaxesAmount { get; set; }
        public Nullable<System.DateTime> DtSentExtOnly1 { get; set; }
        public Nullable<System.DateTime> DtSentExtOnly2 { get; set; }
        public bool File1Ext { get; set; }
        public bool File2Ext { get; set; }
        public Nullable<decimal> NII { get; set; }
        public string TaxesResponsibleNote { get; set; }
        public Nullable<bool> TaxesResponsibleState { get; set; }
        public string StateFilingNote { get; set; }
        public string ResponsibleNote { get; set; }
        public Nullable<int> Q1TaxCalcMeth { get; set; }
        public Nullable<int> Q2TaxCalcMeth { get; set; }
        public Nullable<int> Q3TaxCalcMeth { get; set; }
        public Nullable<int> Q4TaxCalcMeth { get; set; }
        public Nullable<decimal> TaxRefundRequested { get; set; }
        public bool UndistributedIncome { get; set; }
        public Nullable<decimal> OutOfCorpusDistributions { get; set; }
        public bool OnePercentTaxRate { get; set; }
        public bool Req990T { get; set; }
        public bool File990TExt1 { get; set; }
        public bool File990TExt2 { get; set; }
        public Nullable<System.DateTime> DtSent990TExtOnly1 { get; set; }
        public Nullable<System.DateTime> DtSent990TExtOnly2 { get; set; }
        public Nullable<System.DateTime> DtSent990TIRS { get; set; }
        public bool Req4720 { get; set; }
        public bool File4720Ext1 { get; set; }
        public bool File4720Ext2 { get; set; }
        public Nullable<System.DateTime> DtSent4720ExtOnly1 { get; set; }
        public Nullable<System.DateTime> DtSent4720ExtOnly2 { get; set; }
        public Nullable<System.DateTime> DtSent4720IRS { get; set; }
        public bool ReqAmendedReturn { get; set; }
        public Nullable<System.DateTime> DtAmendedReturn { get; set; }
        public Nullable<decimal> Line9PaymentDue { get; set; }
        public byte[] TimeStamp { get; set; }
        public Nullable<decimal> Line6AEstPymt { get; set; }
        public Nullable<decimal> Line6CTaxPaid { get; set; }
        public Nullable<decimal> Line3CExtAmountDue { get; set; }
        public Nullable<decimal> Line8CExtAmountDue { get; set; }
        public string SpecialNote { get; set; }
        public virtual Company Company { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
