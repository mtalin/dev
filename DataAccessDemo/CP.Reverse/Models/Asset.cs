using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Asset
    {
        public Asset()
        {
            this.AssetPrices = new List<AssetPrice>();
            this.Partnerships = new List<Partnership>();
            this.QModifyTransactions = new List<QModifyTransaction>();
            this.QModifyTransactions1 = new List<QModifyTransaction>();
            this.QTransactions = new List<QTransaction>();
            this.QTransactions1 = new List<QTransaction>();
            this.Transacts = new List<Transact>();
            this.Transacts1 = new List<Transact>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Name4 { get; set; }
        public int Type { get; set; }
        public virtual ICollection<AssetPrice> AssetPrices { get; set; }
        public virtual GlobalAsset GlobalAsset { get; set; }
        public virtual LocalAsset LocalAsset { get; set; }
        public virtual ICollection<Partnership> Partnerships { get; set; }
        public virtual ICollection<QModifyTransaction> QModifyTransactions { get; set; }
        public virtual ICollection<QModifyTransaction> QModifyTransactions1 { get; set; }
        public virtual ICollection<QTransaction> QTransactions { get; set; }
        public virtual ICollection<QTransaction> QTransactions1 { get; set; }
        public virtual ICollection<Transact> Transacts { get; set; }
        public virtual ICollection<Transact> Transacts1 { get; set; }
    }
}
