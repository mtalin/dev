using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class GrantMessage
    {
        public int ID { get; set; }
        public int GrantDetails { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Nullable<int> Message { get; set; }
        public Nullable<int> Event { get; set; }
        public virtual Event Event1 { get; set; }
        public virtual GrantDetail GrantDetail { get; set; }
        public virtual Message Message1 { get; set; }
    }
}
