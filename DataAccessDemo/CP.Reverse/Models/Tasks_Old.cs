using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Tasks_Old
    {
        public Tasks_Old()
        {
            this.Subtasks = new List<Subtask>();
            this.Subtasks1 = new List<Subtask>();
        }

        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtStart { get; set; }
        public Nullable<System.DateTime> DtStop { get; set; }
        public int State { get; set; }
        public System.DateTime CndTime { get; set; }
        public Nullable<int> Condition { get; set; }
        public Nullable<int> BodyAction { get; set; }
        public Nullable<int> PostAction { get; set; }
        public int OLEClass { get; set; }
        public int ReturnCode { get; set; }
        public virtual EMailNotifyTransact EMailNotifyTransact { get; set; }
        public virtual MailNotifyTransact MailNotifyTransact { get; set; }
        public virtual MoneyTransferTransact MoneyTransferTransact { get; set; }
        public virtual OLEClass OLEClass1 { get; set; }
        public virtual ICollection<Subtask> Subtasks { get; set; }
        public virtual ICollection<Subtask> Subtasks1 { get; set; }
    }
}
