using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class SubstantialContributor
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Donor { get; set; }
        public System.DateTime DtDate { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public bool ReportIn990PF { get; set; }
        public Nullable<short> FiscalYear { get; set; }
        public virtual Donor Donor1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
