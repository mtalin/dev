using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CheckPayee
    {
        public CheckPayee()
        {
            this.ManualPayments = new List<ManualPayment>();
        }

        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int PostalAddress { get; set; }
        public virtual Person Person { get; set; }
        public virtual PostalAddress PostalAddress1 { get; set; }
        public virtual ICollection<ManualPayment> ManualPayments { get; set; }
    }
}
