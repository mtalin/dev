using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CMSEvent
    {
        public int ID { get; set; }
        public System.DateTime EventDate { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Text { get; set; }
        public byte[] PDF { get; set; }
        public string Link { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public Nullable<System.DateTime> EventToDate { get; set; }
        public virtual Person Person { get; set; }
    }
}
