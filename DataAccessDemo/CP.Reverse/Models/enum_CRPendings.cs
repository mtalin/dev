using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class enum_CRPendings
    {
        public int ItemID { get; set; }
        public string Name { get; set; }
        public Nullable<int> sortOrder { get; set; }
    }
}
