using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PersonFoundation
    {
        public PersonFoundation()
        {
            this.PersonRightsHistories = new List<PersonRightsHistory>();
        }

        public int ID { get; set; }
        public int Person { get; set; }
        public int Foundation { get; set; }
        public int Role { get; set; }
        public string Rights { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual PersonRole PersonRole { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual ICollection<PersonRightsHistory> PersonRightsHistories { get; set; }
    }
}
