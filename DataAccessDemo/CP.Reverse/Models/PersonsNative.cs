using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PersonsNative
    {
        public int Person { get; set; }
        public int Company { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<int> CompanyType { get; set; }
        public int Position { get; set; }
        public int SystemPosition { get; set; }
        public Nullable<decimal> GrantingLimit { get; set; }
        public Nullable<decimal> ApprovalLimit { get; set; }
        public bool GrantCommitteeMember { get; set; }
    }
}
