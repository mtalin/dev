using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_FeesTaxes
    {
        public int ID { get; set; }
        public int FoundationId { get; set; }
        public int Type { get; set; }
        public string TypeName { get; set; }
        public Nullable<int> AuthorId { get; set; }
        public string DateCreated { get; set; }
        public decimal Amount { get; set; }
        public Nullable<decimal> Amount2 { get; set; }
        public string TargetName { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
        public int PeriodType { get; set; }
        public string PeriodName { get; set; }
        public string ReasonForFee { get; set; }
        public string CalcMethod { get; set; }
        public int IsMultiQuarter { get; set; }
        public bool Q1Covered { get; set; }
        public bool Q2Covered { get; set; }
        public bool Q3Covered { get; set; }
        public bool Q4Covered { get; set; }
        public Nullable<int> TaxYear { get; set; }
        public Nullable<int> TaxQuarter { get; set; }
        public Nullable<System.DateTime> TaxYearEnd { get; set; }
        public string VerbosedQuarters { get; set; }
        public int TaxRate { get; set; }
        public string ShortNameQuarters { get; set; }
    }
}
