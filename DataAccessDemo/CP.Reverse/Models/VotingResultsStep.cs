using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class VotingResultsStep
    {
        public int ID { get; set; }
        public int VotingResult { get; set; }
        public int Step { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string Comment { get; set; }
        public virtual VotingResult VotingResult1 { get; set; }
    }
}
