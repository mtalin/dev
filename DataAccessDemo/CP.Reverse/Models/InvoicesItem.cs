using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class InvoicesItem
    {
        public int ID { get; set; }
        public int Invoice { get; set; }
        public int Tax { get; set; }
        public virtual Invoice Invoice1 { get; set; }
        public virtual Tax Tax1 { get; set; }
    }
}
