using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class NCode
    {
        public int ID { get; set; }
        public Nullable<int> Code { get; set; }
    }
}
