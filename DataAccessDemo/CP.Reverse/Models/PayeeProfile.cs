using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PayeeProfile
    {
        public PayeeProfile()
        {
            this.ExpenseReports = new List<ExpenseReport>();
            this.Expenses = new List<Expens>();
            this.Expenses1 = new List<Expens>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> Company { get; set; }
        public bool Trustee { get; set; }
        public bool Requires1099 { get; set; }
        public Nullable<int> Officer { get; set; }
        public Nullable<int> CompanyType { get; set; }
        public Nullable<bool> Disqualified { get; set; }
        public Nullable<int> FormW9 { get; set; }
        public bool IsDisabled { get; set; }
        public virtual Company Company1 { get; set; }
        public virtual Document1 Document { get; set; }
        public virtual ICollection<ExpenseReport> ExpenseReports { get; set; }
        public virtual ICollection<Expens> Expenses { get; set; }
        public virtual ICollection<Expens> Expenses1 { get; set; }
        public virtual Foundation Foundation1 { get; set; }
        public virtual Person Person { get; set; }
    }
}
