using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ToDoNote
    {
        public int ID { get; set; }
        public int ToDo { get; set; }
        public string Text { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual Person Person { get; set; }
        public virtual ToDo ToDo1 { get; set; }
    }
}
