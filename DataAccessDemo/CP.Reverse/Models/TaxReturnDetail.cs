using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class TaxReturnDetail
    {
        public int ID { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Foundation { get; set; }
        public int Year { get; set; }
        public int Step { get; set; }
        public Nullable<System.DateTime> DtSentToSaber { get; set; }
        public Nullable<System.DateTime> DtSentToIRS { get; set; }
        public Nullable<System.DateTime> DtFinalized { get; set; }
        public string Note { get; set; }
        public string IssuesCache { get; set; }
        public string XmlData { get; set; }
        public string FoundationName { get; set; }
        public int FiscalYearEnd { get; set; }
        public string StepName { get; set; }
        public int FileExt { get; set; }
        public bool Outsourced { get; set; }
        public Nullable<int> OutsourcingCompany { get; set; }
        public Nullable<int> TaxReturnReport { get; set; }
        public Nullable<System.DateTime> DtStatusChange { get; set; }
        public Nullable<bool> TaxPackageRecalc { get; set; }
        public Nullable<System.DateTime> dtRecalc { get; set; }
        public int State3 { get; set; }
        public int State4 { get; set; }
        public Nullable<int> ResponsiblePerson { get; set; }
        public bool Req990T { get; set; }
        public bool Req4720 { get; set; }
        public string FilingNote { get; set; }
        public Nullable<int> FilingMethod { get; set; }
        public bool File990PFExt1 { get; set; }
        public Nullable<System.DateTime> DtSent990PFExt1 { get; set; }
        public bool File990PFExt2 { get; set; }
        public Nullable<System.DateTime> DtSent990PFExt2 { get; set; }
        public bool File990TExt1 { get; set; }
        public Nullable<System.DateTime> DtSent990TExt1 { get; set; }
        public bool File990TExt2 { get; set; }
        public Nullable<System.DateTime> DtSent990TExt2 { get; set; }
        public bool File4720Ext1 { get; set; }
        public Nullable<System.DateTime> DtSent4720Ext1 { get; set; }
        public bool File4720Ext2 { get; set; }
        public Nullable<System.DateTime> DtSent4720Ext2 { get; set; }
        public Nullable<int> MailCarrier { get; set; }
        public string TrackingNumber { get; set; }
        public Nullable<System.DateTime> DtStatusChangeState3 { get; set; }
        public Nullable<System.DateTime> DtStatusChangeState4 { get; set; }
        public Nullable<decimal> TaxDue { get; set; }
        public int Priority { get; set; }
    }
}
