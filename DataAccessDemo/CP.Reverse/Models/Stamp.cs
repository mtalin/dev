using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Stamp
    {
        public string StampName { get; set; }
        public string StampValue { get; set; }
    }
}
