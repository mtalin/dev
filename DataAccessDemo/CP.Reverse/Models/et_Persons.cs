using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class et_Persons
    {
        public int ID { get; set; }
        public string NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string NameSuffix { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public string BusinessPhone { get; set; }
        public string HomePhone { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public int IsFoundationOfficer { get; set; }
        public bool OnlineAccess { get; set; }
        public string Dear { get; set; }
        public string CellPhone { get; set; }
        public string OtherPhone { get; set; }
        public string AsstPhone { get; set; }
    }
}
