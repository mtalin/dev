using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class Enum
    {
        public int ID { get; set; }
        public int Category { get; set; }
        public int ItemID { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Abbrev { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public string TheValue { get; set; }
        public string Tag { get; set; }
        public Nullable<int> GroupID { get; set; }
        public virtual EnumsCategory EnumsCategory { get; set; }
        public virtual EnumsText EnumsText { get; set; }
    }
}
