using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class bak_PricingScheduleInstances
    {
        public int ID { get; set; }
        public Nullable<int> PricingSchedule { get; set; }
        public Nullable<int> ContractPricing { get; set; }
        public bool IsActive { get; set; }
        public int PricingSource { get; set; }
        public Nullable<int> BillingEvent { get; set; }
        public Nullable<int> BillingRecipient { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
    }
}
