using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class MeetingsAttended
    {
        public int ID { get; set; }
        public int Meeting { get; set; }
        public Nullable<int> Person { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public bool IsSecretary { get; set; }
        public bool WasPresent { get; set; }
        public Nullable<int> Role { get; set; }
        public virtual Meeting Meeting1 { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
