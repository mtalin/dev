using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class AssetsDetail
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Name4 { get; set; }
        public int Type { get; set; }
        public string Symbol { get; set; }
        public string CUSIP { get; set; }
        public string TypeFlags { get; set; }
        public string TypeName { get; set; }
        public string CategoryName { get; set; }
    }
}
