using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vUnassignedDeposit
    {
        public int ID { get; set; }
        public System.DateTime DtTransact { get; set; }
        public string Symbol { get; set; }
        public string AssetName { get; set; }
        public int Type { get; set; }
        public string TypeName { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> Shares { get; set; }
        public string FoundationName { get; set; }
        public int Foundation { get; set; }
        public Nullable<int> ChildID { get; set; }
        public Nullable<decimal> ChildAmount { get; set; }
        public Nullable<int> Asset { get; set; }
        public Nullable<int> CheckRequest { get; set; }
        public int FinancialPartner { get; set; }
        public Nullable<short> FiscalYear { get; set; }
    }
}
