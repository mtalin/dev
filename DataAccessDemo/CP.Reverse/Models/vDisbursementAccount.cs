using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vDisbursementAccount
    {
        public int ID { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Bank { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public string BankAddress { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public string BatchFilerID { get; set; }
        public string MasterInquiryPIN { get; set; }
        public Nullable<int> CheckingAccount { get; set; }
        public string BankName { get; set; }
    }
}
