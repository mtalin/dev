using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class BalancesOfQuaterlyFeeFromSnapshot
    {
        public int ID { get; set; }
        public Nullable<decimal> Cash { get; set; }
        public Nullable<decimal> Security { get; set; }
        public Nullable<decimal> AltAsset { get; set; }
        public Nullable<System.DateTime> DtPeriod { get; set; }
    }
}
