using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CostBas
    {
        public int ID { get; set; }
        public int Transact { get; set; }
        public decimal LotPrice { get; set; }
        public decimal UnitsNumber { get; set; }
        public System.DateTime DtCreated { get; set; }
        public bool HasAllocated { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public Nullable<System.DateTime> dtDeposit { get; set; }
        public virtual Transact Transact1 { get; set; }
    }
}
