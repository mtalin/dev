using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class ContractService
    {
        public int ID { get; set; }
        public int Author { get; set; }
        public int Contract { get; set; }
        public int Service { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public virtual Contract Contract1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
    }
}
