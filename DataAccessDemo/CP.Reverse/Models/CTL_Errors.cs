using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CTL_Errors
    {
        public int ID { get; set; }
        public int Transact { get; set; }
        public string Description { get; set; }
    }
}
