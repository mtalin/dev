using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class PartnersQuarterlyFee
    {
        public int FinancialPartner { get; set; }
        public decimal Amount { get; set; }
        public decimal CPFee { get; set; }
        public decimal PartnerFee { get; set; }
        public bool CPFlat { get; set; }
        public bool PartnerFlat { get; set; }
        public Nullable<int> Foundation { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
