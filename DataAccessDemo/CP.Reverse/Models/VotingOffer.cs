using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class VotingOffer
    {
        public VotingOffer()
        {
            this.VotingOfferItems = new List<VotingOfferItem>();
            this.VotingOfferResults = new List<VotingOfferResult>();
        }

        public int ID { get; set; }
        public int Foundation { get; set; }
        public int Person { get; set; }
        public string Purpose { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtClosed { get; set; }
        public Nullable<int> CloseReason { get; set; }
        public Nullable<int> Meeting { get; set; }
        public int Step { get; set; }
        public virtual Meeting Meeting1 { get; set; }
        public virtual ICollection<VotingOfferItem> VotingOfferItems { get; set; }
        public virtual ICollection<VotingOfferResult> VotingOfferResults { get; set; }
    }
}
