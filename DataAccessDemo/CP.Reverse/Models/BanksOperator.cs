using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class BanksOperator
    {
        public int Bank { get; set; }
        public int FinancialPartner { get; set; }
    }
}
