using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QuestSummary
    {
        public int id { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public int Questionnaire { get; set; }
        public Nullable<int> Section { get; set; }
        public Nullable<bool> HasIssues { get; set; }
        public Nullable<int> Resolved { get; set; }
        public Nullable<bool> Enabled { get; set; }
        public Nullable<int> ResolvedBy { get; set; }
        public string dtResolved { get; set; }
        public Nullable<int> fsAddrIssuesBy { get; set; }
    }
}
