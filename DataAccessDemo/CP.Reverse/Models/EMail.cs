using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class EMail
    {
        public int ID { get; set; }
        public string SendTo { get; set; }
        public string CopyTo { get; set; }
        public string Body { get; set; }
        public virtual Message Message { get; set; }
    }
}
