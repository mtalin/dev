using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class vStructuredCompany
    {
        public int ID { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> PostalAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Nullable<System.DateTime> dtActivated { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public string Name { get; set; }
        public Nullable<int> ObjectPermissions { get; set; }
    }
}
