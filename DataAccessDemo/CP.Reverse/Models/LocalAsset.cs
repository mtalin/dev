using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class LocalAsset
    {
        public int ID { get; set; }
        public int Foundation { get; set; }
        public float InvPercent { get; set; }
        public float ChrPercent { get; set; }
        public virtual AssetBook AssetBook { get; set; }
        public virtual Asset Asset { get; set; }
        public virtual Foundation Foundation1 { get; set; }
    }
}
