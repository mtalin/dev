using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class SystemPositionsRight
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Permissions { get; set; }
        public string AppliedPermissions { get; set; }
        public string InheritedPermissions { get; set; }
    }
}
