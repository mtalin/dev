using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class HistoricalGrant
    {
        public int ID { get; set; }
        public int GrantDetails { get; set; }
        public System.DateTime GrantDate { get; set; }
        public Nullable<int> Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual GrantDetail GrantDetail { get; set; }
        public virtual Person Person { get; set; }
    }
}
