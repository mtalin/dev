using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class OfficersPosition
    {
        public int ID { get; set; }
        public int Person { get; set; }
        public int Position { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual Position Position1 { get; set; }
    }
}
