using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class VotingResult
    {
        public VotingResult()
        {
            this.VotingResultsSteps = new List<VotingResultsStep>();
        }

        public int ID { get; set; }
        public int Voting { get; set; }
        public int Person { get; set; }
        public int Appointment { get; set; }
        public int Step { get; set; }
        public System.DateTime DtCreated { get; set; }
        public virtual Voting Voting1 { get; set; }
        public virtual ICollection<VotingResultsStep> VotingResultsSteps { get; set; }
    }
}
