using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class QModifyReport
    {
        public int ID { get; set; }
        public int QReport { get; set; }
        public int Foundation { get; set; }
        public int FoundationAccount { get; set; }
        public Nullable<System.DateTime> DtFrom { get; set; }
        public Nullable<System.DateTime> DtTo { get; set; }
        public string TransactionTypes { get; set; }
        public string SecuritySymbol { get; set; }
        public string ShowFields { get; set; }
        public int Author { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<int> AuthorProcess { get; set; }
        public Nullable<System.DateTime> DtProcessed { get; set; }
        public Nullable<int> Note { get; set; }
        public Nullable<System.DateTime> DtDeleted { get; set; }
        public Nullable<System.DateTime> AuthorDeleted { get; set; }
        public virtual FoundationAccount FoundationAccount1 { get; set; }
        public virtual Person Person { get; set; }
        public virtual Person Person1 { get; set; }
        public virtual QReport QReport1 { get; set; }
    }
}
