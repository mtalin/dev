using System;
using System.Collections.Generic;

namespace CP.Reverse.Models
{
    public class CMSPublication
    {
        public int ID { get; set; }
        public System.DateTime IssueDate { get; set; }
        public string Header { get; set; }
        public string SubHeader { get; set; }
        public string Text { get; set; }
        public byte[] PDF { get; set; }
        public string Link { get; set; }
        public int SortOrder { get; set; }
        public System.DateTime DtCreated { get; set; }
        public int Author { get; set; }
        public virtual Person Person { get; set; }
    }
}
