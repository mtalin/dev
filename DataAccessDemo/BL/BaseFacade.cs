﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ms.Model;
using ms.DAL;

namespace ms.BL
{
	public abstract class BaseFacade<TEntity> where TEntity : BaseEntity, new()
    {
		public BaseFacade(IRepository repository)
		{
			Repository = repository;
			Entity = new TEntity();
		}

		public IRepository Repository { get; private set; }
		public IUnitOfWork UnitOfWork { get { return Repository.UnitOfWork; } }

		public TEntity Entity { get; private set; }

		public void CreateNew()
		{
			if (Loaded)
			{
				ClearVariables();
			}
			else
			{
				Entity = new TEntity();
			}
		}

		public void Load(int ID)
		{
			Entity = Repository.GetEntity<TEntity>(ID);
		}

		public void Update()
		{
			if (!Loaded)
			{
				throw new Exception(string.Format(@"{0} Upadte Error. Object was not loaded", EntityName()));
			}
			
			CheckInvariant();

			UnitOfWork.BeginTransaction();
			try
			{
				if (Entity.ID == 0)
				{
					Repository.AddEntity(Entity);
				}
				UnitOfWork.CommitTransaction();
			}
			catch (Exception ex)
			{
				UnitOfWork.RollbackTransaction();
				throw new Exception(string.Format(@"{0} Upadte Error. ID = {0}\r\n{1}", EntityName(), Entity.ID, ex.Message));
			}
		}

		public string Validate()
		{
			if (!Loaded)
			{
				throw new Exception(string.Format(@"{0} Validate Error. Object was not loaded", EntityName()));
			}

			return Repository.IsEntityValid(Entity).Item2;
		}

		public void CheckInvariant()
		{
			string message = Validate();

			if (!string.IsNullOrEmpty(message))
				throw new Exception(string.Format(@"{0} CheckInvariant Error: {1}", EntityName(), message));
		}

		public bool Loaded
		{
			get { return Entity != null;}
		}

		private void ClearVariables()
		{ }

		private static string EntityName()
		{
			return string.Format(@"{0}", typeof(TEntity).Name);
		}

		#region Fields
		
		public int ID { get { return Entity.ID; } set { Entity.ID = value; } }
		
		#endregion
	}
}
