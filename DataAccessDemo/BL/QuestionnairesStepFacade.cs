﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ms.Model;
using ms.DAL;

namespace ms.BL
{
	class QuestionnairesStepFacade: BaseFacade<QuestionnairesStep>
	{
		public QuestionnairesStepFacade(IRepository repository)
			: base(repository)
		{ }
		
		#region Fields

		public int Questionnaire { get { return Entity.Questionnaire; } set { Entity.Questionnaire = value; } }
		public DateTime DtCreated { get { return Entity.DtCreated; } set { Entity.DtCreated = value; } }
		public int Author { get { return Entity.Author; } set { Entity.Author = value; } }
		public int Step { get { return Entity.Step; } set { Entity.Step = value; } }

		#endregion
	}
}
