﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ms.Model;
using ms.DAL;

namespace ms.BL
{
	class FoundationFacade: BaseFacade<Foundation>
	{
		public FoundationFacade(IRepository repository)
			: base(repository)
		{ }

		#region Fields

		public string Name { get { return Entity.Name; } set { Entity.Name = value; } }
		public string EIN { get { return Entity.EIN; } set { Entity.EIN = value; } }
		public int? Founder { get { return Entity.Founder; } set { Entity.Founder = value; } }
		public int? FinancialPartner { get { return Entity.FinancialPartner; } set { Entity.FinancialPartner = value; } }
		public int RegistrationStep { get { return Entity.RegistrationStep; } set { Entity.RegistrationStep = value; } }
		public bool AutoDefaults { get { return Entity.AutoDefaults; } set { Entity.AutoDefaults = value; } }
		public DateTime DtCreated { get { return Entity.DtCreated; } set { Entity.DtCreated = value; } }
		public decimal PlannedFunding { get { return Entity.PlannedFunding; } set { Entity.PlannedFunding = value; } }
		public DateTime? CorpFilingDate { get { return Entity.CorpFilingDate; } set { Entity.CorpFilingDate = value; } }
		public bool IsOnlineApp { get { return Entity.IsOnlineApp; } set { Entity.IsOnlineApp = value; } }
		public int? ShellCorporation { get { return Entity.ShellCorporation; } set { Entity.ShellCorporation = value; } }
		public int? SetupAdmin { get { return Entity.SetupAdmin; } set { Entity.SetupAdmin = value; } }
		public int Conversion { get { return Entity.Conversion; } set { Entity.Conversion = value; } }
		public int HasOfficialStatus { get { return Entity.HasOfficialStatus; } set { Entity.HasOfficialStatus = value; } }
		public int? ProgramCode { get { return Entity.ProgramCode; } set { Entity.ProgramCode = value; } }
		public DateTime? DtBeginCalc { get { return Entity.DtBeginCalc; } set { Entity.DtBeginCalc = value; } }
		public int Type { get { return Entity.Type; } set { Entity.Type = value; } }
		public bool AllowRecommendations { get { return Entity.AllowRecommendations; } set { Entity.AllowRecommendations = value; } }
		public bool ApproveRecomUnanimously { get { return Entity.ApproveRecomUnanimously; } set { Entity.ApproveRecomUnanimously = value; } }
		public bool ApproveRecomByMeetingsOnly { get { return Entity.ApproveRecomByMeetingsOnly; } set { Entity.ApproveRecomByMeetingsOnly = value; } }
		public bool NotifyDirectorsOnNewRecom { get { return Entity.NotifyDirectorsOnNewRecom; } set { Entity.NotifyDirectorsOnNewRecom = value; } }
		public int FiscalYearEnd { get { return Entity.FiscalYearEnd; } set { Entity.FiscalYearEnd = value; } }
		public bool PresidentMayRequireApproveAllGrants { get { return Entity.PresidentMayRequireApproveAllGrants; } set { Entity.PresidentMayRequireApproveAllGrants = value; } }
		public DateTime? DtFirstBalance { get { return Entity.DtFirstBalance;} set { Entity.DtFirstBalance = value; } }
		public int? PublicRecordsAddress { get { return Entity.PublicRecordsAddress;} set { Entity.PublicRecordsAddress = value; } }
		public int? CorrespondenceAddress { get { return Entity.CorrespondenceAddress;} set { Entity.CorrespondenceAddress = value; } }
		public bool NoNominees { get { return Entity.NoNominees;} set { Entity.NoNominees = value; } }
		public bool ClientContacted { get { return Entity.ClientContacted;} set { Entity.ClientContacted = value; } }
		public DateTime? DtTerminated { get { return Entity.DtTerminated;} set { Entity.DtTerminated = value; } }
		public int Rank { get { return Entity.Rank;} set { Entity.Rank = value; } }
		public string KeyInfo { get { return Entity.KeyInfo;} set { Entity.KeyInfo = value; } }
		public bool BalanceFile { get { return Entity.BalanceFile;} set { Entity.BalanceFile = value; } }
		public decimal? ExpectedFunding { get { return Entity.ExpectedFunding;} set { Entity.ExpectedFunding = value; } }
		public string ClientKeyInfo { get { return Entity.ClientKeyInfo;} set { Entity.ClientKeyInfo = value; } }
		public string Design { get { return Entity.Design;} set { Entity.Design = value; } }
		public bool RevApprovGrants { get { return Entity.RevApprovGrants;} set { Entity.RevApprovGrants = value; } }
		public bool RevApprovGrantsCertRecipient { get { return Entity.RevApprovGrantsCertRecipient;} set { Entity.RevApprovGrantsCertRecipient = value; } }
		public int? old_FinancialPartner { get { return Entity.old_FinancialPartner;} set { Entity.old_FinancialPartner = value; } }
		public int? CustomerServiceContact { get { return Entity.CustomerServiceContact;} set { Entity.CustomerServiceContact = value; } }
		public string CustomerServiceContactText { get { return Entity.CustomerServiceContactText;} set { Entity.CustomerServiceContactText = value; } }
		public int? StrategicPartner { get { return Entity.StrategicPartner;} set { Entity.StrategicPartner = value; } }
		public int StructuredCompany { get { return Entity.StructuredCompany;} set { Entity.StructuredCompany = value; } }
		public string CustomContactsHTML { get { return Entity.CustomContactsHTML;} set { Entity.CustomContactsHTML = value; } }
		public bool Ovr_CustomContactsHTML { get { return Entity.Ovr_CustomContactsHTML;} set { Entity.Ovr_CustomContactsHTML = value; } }
		public bool FSPublicRecordsAddress { get { return Entity.FSPublicRecordsAddress;} set { Entity.FSPublicRecordsAddress = value; } }
		public bool FSCorrespondenceAddress { get { return Entity.FSCorrespondenceAddress;} set { Entity.FSCorrespondenceAddress = value; } }
		public int? CheckDesign { get { return Entity.CheckDesign;} set { Entity.CheckDesign = value; } }
		public bool GmxRef { get { return Entity.GmxRef;} set { Entity.GmxRef = value; } }
		public string PhilanthropicInterest { get { return Entity.PhilanthropicInterest;} set { Entity.PhilanthropicInterest = value; } }
		public bool UsesAccrual { get { return Entity.UsesAccrual;} set { Entity.UsesAccrual = value; } }
		public int? Category { get { return Entity.Category;} set { Entity.Category = value; } }
		public string KeyTaxReturnNote { get { return Entity.KeyTaxReturnNote;} set { Entity.KeyTaxReturnNote = value; } }
		public decimal? AnnualRevenue { get { return Entity.AnnualRevenue;} set { Entity.AnnualRevenue = value; } }
		public int? PIPrograms { get { return Entity.PIPrograms;} set { Entity.PIPrograms = value; } }
		public byte[] TimeStamp { get { return Entity.TimeStamp;} set { Entity.TimeStamp = value; } }

		#endregion
	}
}
