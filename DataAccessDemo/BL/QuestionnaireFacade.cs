﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ms.Model;
using ms.DAL;

namespace ms.BL
{
	class QuestionnaireFacade: BaseFacade<Questionnaire>
	{
		public QuestionnaireFacade(IRepository repository)
			: base(repository)
		{ }

		#region Fields

		public int Foundation { get { return Entity.Foundation; } set { Entity.Foundation = value; } }
		public string Version { get { return Entity.Version; } set { Entity.Version = value; } }
		public int FiscalYear { get { return Entity.FiscalYear; } set { Entity.FiscalYear = value; } }
		public string FormData { get { return Entity.FormData; } set { Entity.FormData = value; } }
		public int Step { get { return Entity.Step; } set { Entity.Step = value; } }
		public bool HasIssues { get { return Entity.HasIssues; } set { Entity.HasIssues = value; } }
		public DateTime DtCreated { get { return Entity.DtCreated; } set { Entity.DtCreated = value; } }
		public DateTime? DtSubmitted { get { return Entity.DtSubmitted; } set { Entity.DtSubmitted = value; } }
		public DateTime? DtCompleted { get { return Entity.DtCompleted; } set { Entity.DtCompleted = value; } }
		public int Author { get { return Entity.Author; } set { Entity.Author = value; } }
		public int? CompletedBy { get { return Entity.CompletedBy; } set { Entity.CompletedBy = value; } }

		#endregion
	}
}
