﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ms.Model;
using ms.DAL;

namespace ms.BL
{
	public class DepartmentFacade : BaseFacade<PrimDepartment>
	{
		public DepartmentFacade(IRepository repository)
			: base(repository)
		{ }
	}
}
