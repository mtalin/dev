﻿namespace ms.DAL
{
	public interface IContextFactory
	{
		/// <summary>
		/// Gets the current uo W.
		/// </summary>
		/// <value>The current uo W.</value>
		IUnitOfWork CurrentUoW { get; }
	}
}
