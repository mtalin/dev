﻿using System;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;

namespace ms.DAL.EF
{
	public class Repository: IRepository
	{
		public IUnitOfWork UnitOfWork { get; private set; }

		public Repository(IUnitOfWork unitOfWork)
		{
			UnitOfWork = unitOfWork;
		}

		#region IRepository Members

		public T AddEntity<T>(T entity) where T : class
		{
			this.UnitOfWork.Add(entity);
			return entity;
		}

		public T UpdateEntity<T>(T entity) where T : class
		{
			this.UnitOfWork.Update(entity);
			return entity;
		}

		public void DeleteEntity<T>(T entity) where T : class
		{
			this.UnitOfWork.Delete(entity);
		}

		public IQueryable<T> GetList<T>() where T : class
		{
			return Context.Set<T>();
		}

		public IQueryable<T> GetList<T>(Expression<Func<T, bool>> query) where T : class
		{
			return Context.Set<T>().Where(query);
		}

		public T GetEntity<T>(object primaryKey) where T : class
		{
			return Context.Set<T>().Find((primaryKey));
		}

		public Tuple<bool, string> IsEntityValid<T>(T entity) where T : class
		{
			var res = Context.Entry<T>(entity).GetValidationResult();

			var sb = new StringBuilder();
			foreach (var e in res.ValidationErrors)
			{
				sb.AppendFormat("{0}{1}", e.ErrorMessage, "\r\n");
			}

			return new Tuple<bool, string>(res.IsValid, sb.ToString());
		}

		#endregion

		private DbContext Context
		{
			get { return (DbContext)UnitOfWork.Orm; }
		}
	}
}
