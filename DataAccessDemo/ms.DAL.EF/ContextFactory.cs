﻿using System;
using System.Data.Entity;

namespace ms.DAL.EF
{
	public class ContextFactory: IContextFactory
	{
		public ContextFactory(bool dropCreate = false)
		{
			if (dropCreate)
			{
				Database.SetInitializer(new DropCreateDatabaseIfModelChanges<msDataContext>());
				//Database.SetInitializer(new DropCreateDatabaseAlways<msDataContext>());
			}
			else
			{
				Database.SetInitializer<msDataContext>(null);
			}

			Database.DefaultConnectionFactory = new ConnectionFactory();
		}

		private IUnitOfWork _uow;

		#region IContextFactory Members

		public IUnitOfWork CurrentUoW
		{
			get
			{
				if (_uow == null)
				{
					_uow = GetUnitOfWork();
				}

				return _uow;
			}
		}

		#endregion

		private IUnitOfWork GetUnitOfWork()
		{
			return new UnitOfWork(new msDataContext("Test2"));
		}
	}
}
