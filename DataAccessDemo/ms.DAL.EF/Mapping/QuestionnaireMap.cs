using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using ms.Model;

namespace ms.DAL.EF.Mapping
{
    public class QuestionnaireMap : EntityTypeConfiguration<Questionnaire>
    {
        public QuestionnaireMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Version)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.FormData)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Questionnaires");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Foundation).HasColumnName("Foundation");
            this.Property(t => t.Version).HasColumnName("Version");
            this.Property(t => t.FiscalYear).HasColumnName("FiscalYear");
            this.Property(t => t.FormData).HasColumnName("FormData");
            this.Property(t => t.Step).HasColumnName("Step");
            this.Property(t => t.HasIssues).HasColumnName("HasIssues");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.DtSubmitted).HasColumnName("DtSubmitted");
            this.Property(t => t.DtCompleted).HasColumnName("DtCompleted");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.CompletedBy).HasColumnName("CompletedBy");

            // Relationships
            this.HasRequired(t => t.Foundation1)
                .WithMany(t => t.Questionnaires)
                .HasForeignKey(d => d.Foundation);

        }
    }
}
