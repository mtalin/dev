﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;
using ms.Model;

namespace ms.DAL.EF.Mapping
{
	class msDepartmentMap : EntityTypeConfiguration<PrimDepartment>
	{
		public msDepartmentMap()
		{
			ToTable("msDepartments");

			// Primary key
			HasKey(e => e.ID);

			// Columns
			//Property(e => e.ID)
			//	.HasColumnName("ID")
			//	.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

			Property(e => e.Name)
				.HasColumnName("Name")
				.IsRequired()
				.HasMaxLength(50);

			Property(e => e.Description)
				.HasColumnName("Description")
				.HasMaxLength(1000);

			Property(e => e.DtCreated)
			//	.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed)
				.IsRequired();

			// Relationships
			//HasOptional(d => d.ChiefPerson)
			//	.WithMany(p => p.Departments)
			//	.HasForeignKey(d => d.ChiefId);
		}
	}
}
