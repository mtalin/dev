﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;

using ms.Model;

namespace ms.DAL.EF.Mapping
{
	public class msPersonStateMap : EntityTypeConfiguration<PrimPersonState>
	{
		public msPersonStateMap()
		{
			ToTable("msPersonStates");

			// Primary key
			HasKey(e => e.ID);

			// Columns
			//Property(e => e.PersonStateId)
			//	.HasColumnName("PersonStateId")
			//	.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
		}
	}
}
