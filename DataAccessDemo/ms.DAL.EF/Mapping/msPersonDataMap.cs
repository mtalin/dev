﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;
using ms.Model;

namespace ms.DAL.EF.Mapping
{
	public class msPersonDataMap : EntityTypeConfiguration<PrimPersonData>
	{
		public msPersonDataMap()
		{
			ToTable("msPersonData");

			// Primary key
			HasKey(e => e.ID);

			// Columns
			//Property(e => e.PersonDataId)
			//	.HasColumnName("PersonDataID")
			//	.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
		}
	}
}
