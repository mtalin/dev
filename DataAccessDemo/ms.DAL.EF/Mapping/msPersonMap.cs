﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;
using ms.Model;

namespace ms.DAL.EF.Mapping
{
	class msPersonMap : EntityTypeConfiguration<PrimPerson>
	{
		public msPersonMap()
		{
			ToTable("msPersons");

			// Primary key
			HasKey(e => e.ID);

			// Columns
			//Property(e => e.PersonId)
			//	.HasColumnName("PersonID")
			//	.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
		}
	}
}
