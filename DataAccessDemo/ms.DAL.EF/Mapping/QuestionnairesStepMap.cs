using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using ms.Model;

namespace ms.DAL.EF.Mapping
{
    public class QuestionnairesStepMap : EntityTypeConfiguration<QuestionnairesStep>
    {
        public QuestionnairesStepMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("QuestionnairesSteps");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Questionnaire).HasColumnName("Questionnaire");
            this.Property(t => t.DtCreated).HasColumnName("DtCreated");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Step).HasColumnName("Step");

            // Relationships
            this.HasRequired(t => t.Questionnaire1)
                .WithMany(t => t.QuestionnairesSteps)
                .HasForeignKey(d => d.Questionnaire);
            //this.HasRequired(t => t.User)
            //    .WithMany(t => t.QuestionnairesSteps)
            //    .HasForeignKey(d => d.Author);
        }
    }
}
