﻿using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
//using FirebirdSql.Data.FirebirdClient;

namespace ms.DAL.EF
{
	public class ConnectionFactory: IDbConnectionFactory
	{
		#region IDbConnectionFactory Members

		public DbConnection CreateConnection(string nameOrConnectionString)
		{
			var name = nameOrConnectionString
				.Split('.').Last()
				.Replace("Context", string.Empty);

			if (msDataContext.DbType == DatabaseType.MsSql)
			{
				var builder = new SqlConnectionStringBuilder
								{
									//DataSource = @".\SQLEXPRESS",
									DataSource = "11v2-DB",
									InitialCatalog = name,
									MultipleActiveResultSets = true,
									IntegratedSecurity = false,
									UserID = "sa",
									Password = "post+Office2",
								};

				return new SqlConnection(builder.ToString());
			}

			//else
			//{
			//	var builder = new FbConnectionStringBuilder
			//					{
			//						UserID = "SYSDBA",
			//						Password = "masterkey",
			//						Database = @"C:\@programs\@DB\" + name + ".fdb",
			//						DataSource = "127.0.0.1",
			//						Port = 3050,
			//						Dialect = 3,
			//						Charset = "NONE",
			//						Role = string.Empty,
			//						ConnectionLifeTime = 15,
			//						Pooling = true,
			//						MinPoolSize = 0,
			//						MaxPoolSize = 50,
			//						PacketSize = 8192,
			//						ServerType = 0
			//					};

			//	return new FbConnection(builder.ToString());
			//}
		}

		#endregion
	}
}
