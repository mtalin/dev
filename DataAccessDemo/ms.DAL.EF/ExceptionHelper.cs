﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ms.DAL.EF
{
    public static class ExceptionHelper
    {
		public static string FullMessage(this Exception ex)
		{
			var sb = new StringBuilder();
			while (ex != null)
			{
				sb.AppendFormat("{0}\r\n", ex.Message);
				ex = ex.InnerException;
			}
			return sb.ToString();
		}
    }
}
