﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

using ms.Model;
using ms.DAL.EF.Mapping;

namespace ms.DAL.EF
{
	public enum DatabaseType { MsSql, Firebird }

	public class msDataContext : DbContext
	{
		public const DatabaseType DbType = DatabaseType.MsSql;
		//public const DatabaseType DbType = DatabaseType.Firebird;

		public msDataContext()
		{ }

		public msDataContext(string databaseName)
			: base(databaseName) 
		{ }

		public msDataContext(DbConnection connection)
			: base(connection, contextOwnsConnection: false) 
		{ }

		public DbSet<PrimDepartment> Departments { get; set; }
		public DbSet<PrimPerson> Persons { get; set; }
		public DbSet<PrimPersonData> PersonData { get; set; }
		public DbSet<PrimPersonState> PersonStates { get; set; }

		public DbSet<Foundation> Foundations { get; set; }
		public DbSet<Questionnaire> Questionnaires { get; set; }
		public DbSet<QuestionnairesStep> QuestionnairesSteps { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new msDepartmentMap());
			modelBuilder.Configurations.Add(new msPersonMap());
			modelBuilder.Configurations.Add(new msPersonDataMap());
			modelBuilder.Configurations.Add(new msPersonStateMap());

			modelBuilder.Configurations.Add(new FoundationMap());
			modelBuilder.Configurations.Add(new QuestionnaireMap());
			modelBuilder.Configurations.Add(new QuestionnairesStepMap());
		}
	}
}
