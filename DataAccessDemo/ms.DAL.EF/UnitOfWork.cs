﻿using System;
using System.Text;
using System.Transactions;
using System.Data.Entity;

namespace ms.DAL.EF
{
	public class UnitOfWork: IUnitOfWork
	{
		private TransactionScope ts;
		private int TransactionCount = 0;

		public UnitOfWork(DbContext orm)
		{ 
			this.Orm = orm;
		}

		#region IUnitOfWork Members

		public object Orm { get; private set; }

		public void Add<T>(T entity) where T : class
		{
			try
			{
				((DbContext)Orm).Set<T>().Add(entity);
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format(@"An error occured during the Add Entity {0}.\r\n{1}", EntityName<T>(), ex.FullMessage()));
			}
		}

		public void Update<T>(T entity) where T : class
		{
			try
			{
				((DbContext)Orm).Set<T>().Attach(entity);
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format(@"An error occured during the Update Entity {0}.\r\n{0}", EntityName<T>(), ex.FullMessage()));
			}
		}

		public void Delete<T>(T entity) where T : class
		{
			try
			{
				((DbContext)Orm).Set<T>().Remove(entity);
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("An error occured during the Delete Entity.\r\n{0}", ex.FullMessage()));
			}
		}

		public void SaveChanges()
		{
			try
			{
				((DbContext)Orm).SaveChanges();
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format(@"An error occured during the Save changes.\r\n{0}", ex.FullMessage()));
			}
		}

		public void BeginTransaction()
		{
			try
			{
				if (TransactionCount == 0)
				{
					ts = new TransactionScope();
				}
				TransactionCount++;
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("An error occured during the Begin transaction.\r\n{0}", ex.Message));
			}
		}

		public void CommitTransaction()
		{
			try
			{
				if (ts == null)
				{
					throw new TransactionException(@"The current transaction is not started!");
				}
				if (TransactionCount > 0)
				{
					if (--TransactionCount == 0)
					{
						try
						{
							SaveChanges();
							ts.Complete();
						}
						finally
						{
							ts.Dispose();
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format(@"An error occured during the Commit transaction.\r\n{0}", ex.FullMessage()));
			}
		}

		public void RollbackTransaction()
		{
			try
			{
				if (ts != null)
				{
					ts.Dispose();
				}
				TransactionCount = 0;
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format(@"An error occured during the Rollback transaction.\r\n{0}", ex.FullMessage()));
			}
		}

		#endregion

		private static string EntityName<T>()
		{
			return string.Format(@"{0}s", typeof(T).Name);
		}
	}
}
