using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ms.Model
{
	public class QuestionnairesStep : BaseEntity
    {
		//public int ID { get; set; }
		public int Questionnaire { get; set; }
		public System.DateTime DtCreated { get; set; }
		public int Author { get; set; }
		public int Step { get; set; }
		public virtual Questionnaire Questionnaire1 { get; set; }
        
		//public virtual User User { get; set; }

		#region IValidatableObject Members

		public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (ID == 0 && DtCreated.Date != DateTime.Today)
			{
				yield return
					new ValidationResult(
						string.Format("The field DtCreated ({0}) must be today ({1})", DtCreated.Date, DateTime.Today),
						new[] { "DtCreated" }
					);
			}
		}

		#endregion
	}
}
