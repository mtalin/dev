﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ms.Model
{
	public class PrimPerson : BaseEntity
	{
		public PrimPerson()
        {
			this.PersonData = new List<PrimPersonData>();
			this.PersonStates = new List<PrimPersonState>();
		}

		//public int PersonId { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string FullName
		{
			get { return FirstName + " " + LastName; }
		}
		public DateTime DtCreated { get; set; }
		public DateTime? BirthDate { get; set; }

		public int State { get; set; }

		//public ICollection<Department> Departments { get; set; }
		public ICollection<PrimPersonData> PersonData { get; set; }
		public ICollection<PrimPersonState> PersonStates { get; set; }

		#region IValidatableObject Members

		public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (ID == 0 && DtCreated.Date != DateTime.Today)
			{
				yield return
					new ValidationResult(
						string.Format("The field DtCreated ({0}) must be today ({1})", DtCreated.Date, DateTime.Today),
						new[] { "DtCreated" }
					);
			}
		}

		#endregion
	}
}
