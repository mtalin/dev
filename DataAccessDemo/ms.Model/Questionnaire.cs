using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ms.Model
{
	public class Questionnaire : BaseEntity
    {
        public Questionnaire()
        {
            this.QuestionnairesSteps = new List<QuestionnairesStep>();
        }

        //public int ID { get; set; }
        public int Foundation { get; set; }
        public string Version { get; set; }
        public int FiscalYear { get; set; }
        public string FormData { get; set; }
        public int Step { get; set; }
        public bool HasIssues { get; set; }
        public System.DateTime DtCreated { get; set; }
        public Nullable<System.DateTime> DtSubmitted { get; set; }
        public Nullable<System.DateTime> DtCompleted { get; set; }
        public int Author { get; set; }
        public Nullable<int> CompletedBy { get; set; }
        public virtual Foundation Foundation1 { get; set; }

        public virtual ICollection<QuestionnairesStep> QuestionnairesSteps { get; set; }

		#region IValidatableObject Members

		public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (ID == 0 && DtCreated.Date != DateTime.Today)
			{
				yield return
					new ValidationResult(
						string.Format("The field DtCreated ({0}) must be today ({1})", DtCreated.Date, DateTime.Today),
						new[] { "DtCreated" }
					);
			}
		}

		#endregion
	}
}
