using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ms.Model
{
	public class Foundation : BaseEntity
    {
        public Foundation()
        {
			//this.ACHTransacts = new List<ACHTransact>();
			//this.AnnualPayouts = new List<AnnualPayout>();
			//this.AssetBooks = new List<AssetBook>();
			//this.CarryforwardDetails = new List<CarryforwardDetail>();
			//this.CharityRequests = new List<CharityRequest>();
			//this.Contacts = new List<Contact>();
			//this.CRMTasks = new List<CRMTask>();
			//this.DefaultCharities = new List<DefaultCharity>();
			//this.DistributableAmounts = new List<DistributableAmount>();
			//this.Documents = new List<Document1>();
			//this.DonationCertificates = new List<DonationCertificate>();
			//this.DonationRequests = new List<DonationRequest>();
			//this.Donors = new List<Donor>();
			//this.EFTPSEnrollments = new List<EFTPSEnrollment>();
			//this.EmailTemplates = new List<EmailTemplate>();
			//this.EstimatedTaxes = new List<EstimatedTax>();
			//this.ExcludedEmailApplicants = new List<ExcludedEmailApplicant>();
			//this.ExpenseReports = new List<ExpenseReport>();
			//this.Expenses = new List<Expens>();
			//this.FavoriteCharities = new List<FavoriteCharity>();
			//this.FeeCalculations = new List<FeeCalculation>();
			//this.FinancialAdvisorsFoundations = new List<FinancialAdvisorsFoundation>();
			//this.FinancialAdvisorsRequests = new List<FinancialAdvisorsRequest>();
			//this.FinancialPartnerAccounts = new List<FinancialPartnerAccount>();
			//this.FinancialPartnerChunks = new List<FinancialPartnerChunk>();
			//this.FoundationAccounts = new List<FoundationAccount>();
			//this.FoundationProjects = new List<FoundationProject>();
			//this.FoundationsBoardMeetings = new List<FoundationsBoardMeeting>();
			//this.FoundationsCharities = new List<FoundationsCharity>();
			//this.FoundationsGrowthInfoes = new List<FoundationsGrowthInfo>();
			//this.FoundationsRatings = new List<FoundationsRating>();
			//this.FoundationsSteps = new List<FoundationsStep>();
			//this.Fundings = new List<Funding>();
			//this.GrantApplications = new List<GrantApplication>();
			//this.GrantDetails = new List<GrantDetail>();
			//this.tmp_GrantHistory = new List<tmp_GrantHistory>();
			//this.Grants = new List<Grant>();
			//this.GrantSchedules = new List<GrantSchedule>();
			//this.HouseholdAccounts = new List<HouseholdAccount>();
			//this.LocalAssets = new List<LocalAsset>();
			//this.Meetings = new List<Meeting>();
			//this.Messages = new List<Message>();
			//this.old_Nominees = new List<old_Nominees>();
			//this.Notes = new List<Note>();
			//this.Partnerships = new List<Partnership>();
			//this.PartnersQuarterlyFees = new List<PartnersQuarterlyFee>();
			//this.PartnersTexts = new List<PartnersText>();
			//this.PayeeProfiles = new List<PayeeProfile>();
			//this.Payments = new List<Payment>();
			//this.PendingTransacts = new List<PendingTransact>();
			//this.PersonFoundations = new List<PersonFoundation>();
			//this.PersonRoles = new List<PersonRole>();
			//this.Positions = new List<Position>();
			//this.Proposals = new List<Proposal>();
			//this.QCapitalGains = new List<QCapitalGain>();
			//this.QDeposits = new List<QDeposit>();
			//this.QModifyTransactions = new List<QModifyTransaction>();
			//this.QPortfolios = new List<QPortfolio>();
			//this.QTransactions = new List<QTransaction>();

            this.Questionnaires = new List<Questionnaire>();

			//this.Successors = new List<Successor>();
			//this.Taxes = new List<Tax>();
			//this.TaxReceipts = new List<TaxReceipt>();
			//this.TaxReturns = new List<TaxReturn>();
			//this.TaxReturnStates = new List<TaxReturnState>();
			//this.temp_foundation_data = new List<temp_foundation_data>();
			//this.Transacts = new List<Transact>();
			//this.TransferGroups = new List<TransferGroup>();
			//this.TransferProfiles = new List<TransferProfile>();
			//this.TrustedActions = new List<TrustedAction>();
			//this.Votings = new List<Voting>();
			//this.CustomReports = new List<CustomReport>();
        }

        //public int ID { get; set; }
        public string Name { get; set; }
        public string EIN { get; set; }
        public Nullable<int> Founder { get; set; }
        public Nullable<int> FinancialPartner { get; set; }
        public int RegistrationStep { get; set; }
        public bool AutoDefaults { get; set; }
        public System.DateTime DtCreated { get; set; }
        public decimal PlannedFunding { get; set; }
        public Nullable<System.DateTime> CorpFilingDate { get; set; }
        public bool IsOnlineApp { get; set; }
        public Nullable<int> ShellCorporation { get; set; }
        public Nullable<int> SetupAdmin { get; set; }
        public int Conversion { get; set; }
        public int HasOfficialStatus { get; set; }
        public Nullable<int> ProgramCode { get; set; }
        public Nullable<System.DateTime> DtBeginCalc { get; set; }
        public int Type { get; set; }
        public bool AllowRecommendations { get; set; }
        public bool ApproveRecomUnanimously { get; set; }
        public bool ApproveRecomByMeetingsOnly { get; set; }
        public bool NotifyDirectorsOnNewRecom { get; set; }
        public int FiscalYearEnd { get; set; }
        public bool PresidentMayRequireApproveAllGrants { get; set; }
        public Nullable<System.DateTime> DtFirstBalance { get; set; }
        public Nullable<int> PublicRecordsAddress { get; set; }
        public Nullable<int> CorrespondenceAddress { get; set; }
        public bool NoNominees { get; set; }
        public bool ClientContacted { get; set; }
        public Nullable<System.DateTime> DtTerminated { get; set; }
        public int Rank { get; set; }
        public string KeyInfo { get; set; }
        public bool BalanceFile { get; set; }
        public Nullable<decimal> ExpectedFunding { get; set; }
        public string ClientKeyInfo { get; set; }
        public string Design { get; set; }
        public bool RevApprovGrants { get; set; }
        public bool RevApprovGrantsCertRecipient { get; set; }
        public Nullable<int> old_FinancialPartner { get; set; }
        public Nullable<int> CustomerServiceContact { get; set; }
        public string CustomerServiceContactText { get; set; }
        public Nullable<int> StrategicPartner { get; set; }
        public int StructuredCompany { get; set; }
        public string CustomContactsHTML { get; set; }
        public bool Ovr_CustomContactsHTML { get; set; }
        public bool FSPublicRecordsAddress { get; set; }
        public bool FSCorrespondenceAddress { get; set; }
        public Nullable<int> CheckDesign { get; set; }
        public bool GmxRef { get; set; }
        public string PhilanthropicInterest { get; set; }
        public bool UsesAccrual { get; set; }
        public Nullable<int> Category { get; set; }
        public string KeyTaxReturnNote { get; set; }
        public Nullable<decimal> AnnualRevenue { get; set; }
        public Nullable<int> PIPrograms { get; set; }
        public byte[] TimeStamp { get; set; }

		//public virtual ICollection<ACHTransact> ACHTransacts { get; set; }
		//public virtual ICollection<AnnualPayout> AnnualPayouts { get; set; }
		//public virtual ICollection<AssetBook> AssetBooks { get; set; }
		//public virtual ICollection<CarryforwardDetail> CarryforwardDetails { get; set; }
		//public virtual ICollection<CharityRequest> CharityRequests { get; set; }
		//public virtual CompanyElement CompanyElement { get; set; }
		//public virtual ICollection<Contact> Contacts { get; set; }
		//public virtual ICollection<CRMTask> CRMTasks { get; set; }
		//public virtual ICollection<DefaultCharity> DefaultCharities { get; set; }
		//public virtual ICollection<DistributableAmount> DistributableAmounts { get; set; }
		//public virtual ICollection<Document1> Documents { get; set; }
		//public virtual ICollection<DonationCertificate> DonationCertificates { get; set; }
		//public virtual ICollection<DonationRequest> DonationRequests { get; set; }
		//public virtual ICollection<Donor> Donors { get; set; }
		//public virtual ICollection<EFTPSEnrollment> EFTPSEnrollments { get; set; }
		//public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
		//public virtual ICollection<EstimatedTax> EstimatedTaxes { get; set; }
		//public virtual ICollection<ExcludedEmailApplicant> ExcludedEmailApplicants { get; set; }
		//public virtual ICollection<ExpenseReport> ExpenseReports { get; set; }
		//public virtual ICollection<Expens> Expenses { get; set; }
		//public virtual ICollection<FavoriteCharity> FavoriteCharities { get; set; }
		//public virtual ICollection<FeeCalculation> FeeCalculations { get; set; }
		//public virtual ICollection<FinancialAdvisorsFoundation> FinancialAdvisorsFoundations { get; set; }
		//public virtual ICollection<FinancialAdvisorsRequest> FinancialAdvisorsRequests { get; set; }
		//public virtual ICollection<FinancialPartnerAccount> FinancialPartnerAccounts { get; set; }
		//public virtual ICollection<FinancialPartnerChunk> FinancialPartnerChunks { get; set; }
		//public virtual ICollection<FoundationAccount> FoundationAccounts { get; set; }
		//public virtual ICollection<FoundationProject> FoundationProjects { get; set; }

		//public virtual PostalAddress PostalAddress { get; set; }
		//public virtual ProgramCode ProgramCode1 { get; set; }
		//public virtual PostalAddress PostalAddress1 { get; set; }
		//public virtual ShellCorporation ShellCorporation1 { get; set; }
		//public virtual StructuredCompany StructuredCompany1 { get; set; }
		//public virtual Person Person { get; set; }

		//public virtual ICollection<FoundationsBoardMeeting> FoundationsBoardMeetings { get; set; }
		//public virtual ICollection<FoundationsCharity> FoundationsCharities { get; set; }
		//public virtual ICollection<FoundationsGrowthInfo> FoundationsGrowthInfoes { get; set; }
		//public virtual ICollection<FoundationsRating> FoundationsRatings { get; set; }
		//public virtual ICollection<FoundationsStep> FoundationsSteps { get; set; }
		//public virtual ICollection<Funding> Fundings { get; set; }
		//public virtual ICollection<GrantApplication> GrantApplications { get; set; }
		//public virtual ICollection<GrantDetail> GrantDetails { get; set; }
		//public virtual ICollection<tmp_GrantHistory> tmp_GrantHistory { get; set; }
		//public virtual ICollection<Grant> Grants { get; set; }
		//public virtual ICollection<GrantSchedule> GrantSchedules { get; set; }
		//public virtual ICollection<HouseholdAccount> HouseholdAccounts { get; set; }
		//public virtual ICollection<LocalAsset> LocalAssets { get; set; }
		//public virtual ICollection<Meeting> Meetings { get; set; }
		//public virtual ICollection<Message> Messages { get; set; }
		//public virtual ICollection<old_Nominees> old_Nominees { get; set; }
		//public virtual ICollection<Note> Notes { get; set; }
		//public virtual ICollection<Partnership> Partnerships { get; set; }
		//public virtual ICollection<PartnersQuarterlyFee> PartnersQuarterlyFees { get; set; }
		//public virtual ICollection<PartnersText> PartnersTexts { get; set; }
		//public virtual ICollection<PayeeProfile> PayeeProfiles { get; set; }
		//public virtual ICollection<Payment> Payments { get; set; }
		//public virtual ICollection<PendingTransact> PendingTransacts { get; set; }
		//public virtual ICollection<PersonFoundation> PersonFoundations { get; set; }
		//public virtual ICollection<PersonRole> PersonRoles { get; set; }
		//public virtual ICollection<Position> Positions { get; set; }
		//public virtual ICollection<Proposal> Proposals { get; set; }
		//public virtual ICollection<QCapitalGain> QCapitalGains { get; set; }
		//public virtual ICollection<QDeposit> QDeposits { get; set; }
		//public virtual ICollection<QModifyTransaction> QModifyTransactions { get; set; }
		//public virtual ICollection<QPortfolio> QPortfolios { get; set; }
		//public virtual ICollection<QTransaction> QTransactions { get; set; }

        public virtual ICollection<Questionnaire> Questionnaires { get; set; }

		//public virtual ICollection<Successor> Successors { get; set; }
		//public virtual ICollection<Tax> Taxes { get; set; }
		//public virtual ICollection<TaxReceipt> TaxReceipts { get; set; }
		//public virtual ICollection<TaxReturn> TaxReturns { get; set; }
		//public virtual ICollection<TaxReturnState> TaxReturnStates { get; set; }
		//public virtual ICollection<temp_foundation_data> temp_foundation_data { get; set; }
		//public virtual ICollection<Transact> Transacts { get; set; }
		//public virtual ICollection<TransferGroup> TransferGroups { get; set; }
		//public virtual ICollection<TransferProfile> TransferProfiles { get; set; }
		//public virtual ICollection<TrustedAction> TrustedActions { get; set; }
		//public virtual ICollection<Voting> Votings { get; set; }
		//public virtual ICollection<CustomReport> CustomReports { get; set; }

		#region IValidatableObject Members

		public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (ID == 0 && DtCreated.Date != DateTime.Today)
			{
				yield return
					new ValidationResult(
						string.Format("The field DtCreated ({0}) must be today ({1})", DtCreated.Date, DateTime.Today),
						new[] { "DtCreated" }
					);
			}
		}

		#endregion
	}
}
