﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ms.Model
{
	public class PrimDepartment : BaseEntity
	{
		public PrimDepartment()
		{
			this.Persons = new List<PrimPerson>();
		}

		//public int DepartmentId { get; set; }
		public string Name { get; set; }

		[RegularExpression(@"^[A-Z]+[A-Za-z0-9 ]*")]
		public string Description { get; set; }

		public DateTime DtCreated { get; set; }

		//public int? ChiefId { get; set; }
		//public Person ChiefPerson { get; set; }

		public ICollection<PrimPerson> Persons { get; set; }

		#region IValidatableObject Members

		public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (ID == 0 && DtCreated.Date != DateTime.Today)
			{
				yield return
					new ValidationResult(
						string.Format("The field DtCreated ({0}) must be today ({1})", DtCreated.Date, DateTime.Today),
						new[] { "DtCreated" }
					);
			}
		}

		#endregion
	}
}
