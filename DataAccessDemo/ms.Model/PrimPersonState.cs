﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ms.Model
{
	public class PrimPersonState : BaseEntity
	{
		//public int PersonStateId { get; set; }
		public int State { get; set; }
		public DateTime DtCreated { get; set; }

		public int PersonId { get; set; }
		public PrimPerson msPerson { get; set; }

		#region IValidatableObject Members

		public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (ID == 0 && DtCreated.Date != DateTime.Today)
			{
				yield return
					new ValidationResult(
						string.Format("The field DtCreated ({0}) must be today ({1})", DtCreated.Date, DateTime.Today),
						new[] { "DtCreated" }
					);
			}
		}

		#endregion
	}
}
