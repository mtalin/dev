﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ms.Model
{
	public class PrimPersonData: BaseEntity
	{
		//public int PersonDataId { get; set; }
		public DateTime DtCreated { get; set; }
		public DateTime DtClosed { get; set; }
		public int Amount { get; set; }
		public int Advance { get; set; }

		public int PersonId { get; set; }
		public PrimPerson msPerson { get; set; }

		public int DepartmentId { get; set; }
		public PrimDepartment msDepartment { get; set; }

		#region IValidatableObject Members

		public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (ID == 0 && DtCreated.Date != DateTime.Today)
			{
				yield return
					new ValidationResult(
						string.Format("The field DtCreated ({0}) must be today ({1})", DtCreated.Date, DateTime.Today),
						new[] { "DtCreated" }
					);
			}
			if (Amount <= Advance)
			{
				yield return
					new ValidationResult(
						string.Format("The field Advance ({0}) must be less than Amount ({1})", Advance, Amount),
						new[] { "DtCreated" }
					);
			}
		}

		#endregion
	}
}
