﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace ms.Model
{
	public abstract class BaseEntity : IValidatableObject
	{
		public int ID { get; set; }

		#region IValidatableObject Members

		public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			yield return ValidationResult.Success;
		}

		#endregion
	}
}
