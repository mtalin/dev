﻿using FinalStateMachine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FinalStateMachine.Tests
{


    /// <summary>
    ///This is a test class for FsmGuardTest and is intended
    ///to contain all FsmGuardTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FsmGuardTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for NewGuard
        ///</summary>
        [TestMethod()]
        public void NewGuard_Test()
        {
            string name = "GuardTest";
            Func<string, bool> condition = s => !string.IsNullOrEmpty(s);
            FsmGuard target = FsmGuard.NewGuard(name, condition);

            Assert.AreEqual(name, target.Name, target.Name);
            Assert.IsNotNull(target.Condition);
        }

        /// <summary>
        ///A test for Check
        ///</summary>
        [TestMethod()]
        public void Check_Test()
        {
            string name = "GuardTest";
            Func<string, bool> condition = s => !string.Equals(name, s);
            FsmGuard target = FsmGuard.NewGuard(name, condition);

            bool expected = false;
            bool actual = target.Check();

            Assert.AreEqual(expected, actual);
        }
    }
}
