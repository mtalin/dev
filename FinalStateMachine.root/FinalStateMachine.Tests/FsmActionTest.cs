﻿using FinalStateMachine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FinalStateMachine.Tests
{


    /// <summary>
    ///This is a test class for FsmActionTest and is intended
    ///to contain all FsmActionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FsmActionTest
    {
        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for NewAction
        ///</summary>
        [TestMethod()]
        public void NewAction_Test()
        {
            string name = "ActionTest";
            Action<string> action = s => s.ToUpper();

            FsmAction target = FsmAction.NewAction(name, action);

            Assert.IsNotNull(target.Action);
            Assert.AreEqual(name, target.Name);
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void NewAction_EmptyName_Test()
        {
            string name = null;
            Action<string> action = s => s.ToUpper();

            FsmAction target = FsmAction.NewAction(name, action);

            Assert.IsNotNull(target.Action);
            Assert.AreEqual(name, target.Name);
        }

        [TestMethod()]
        public void NewAction_EmptyAction_Test()
        {
            string name = "ActionTest";

            FsmAction target = FsmAction.NewAction(name);

            Assert.IsNull(target.Action);
            Assert.AreEqual(name, target.Name);
        }

        /// <summary>
        ///A test for Execute
        ///</summary>
        [TestMethod()]
        public void Execute_Test()
        {
            string name = "ActionTest";
            string str = null;

            Action<string> action = s => str = s.ToUpper();
            FsmAction target = FsmAction.NewAction(name, action);

            target.Execute();
            Assert.AreEqual(name.ToUpper(), str);
        }

        [TestMethod()]
        public void Execute_EmptyAction_Test()
        {
            string name = "ActionTest";
            string str = null;

            FsmAction target = FsmAction.NewAction(name);

            Assert.AreEqual(name, target.Name, target.Name);
            target.Execute();
            Assert.AreEqual(name, target.Name, target.Name);
        }
    }
}
