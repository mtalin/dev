﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Xml;
using Lexxys;

namespace FinalStateMachine
{
	public enum FsmActionType { Enter, Exit, Transition }

	[DebuggerDisplay("{Name}")]
	public class FsmAction<T> where T : class
	{
		public string Name { get; private set; }
		public FsmActionType Type { get; private set; }
		public Action<FsmActionType, string, string, int?, string, int?> Action { get; set; }
		public FsmContext<T> Context { get; private set; }

		private FsmAction(FsmContext<T> context)
		{
			if (context == null)
				throw EX.ArgumentNull("context");
			Context = context;
		}

		public static FsmAction<T> NewAction(FsmContext<T> context, string name, FsmActionType type, Action<FsmActionType, string, string, int?, string, int?> action = null)
		{
			if (context == null)
				throw EX.ArgumentNull("context");
			if (string.IsNullOrEmpty(name))
				throw EX.ArgumentNull("name");

			return new FsmAction<T>(context) { Name = name, Action = action, Type = type };
		}

		public void Execute(string evt, FsmState<T> source, FsmState<T> target)
		{
			MethodInfo method = typeof(T).GetMethod(Name);
			if (method != null && Context.Owner != null)
			{
				method.Invoke(Context.Owner, null);
				return;
			}
			string sn = source == null ? "" : source.Name;
			int? sv = source == null ? null : source.Value;
			string tn = target == null ? "" : target.Name;
			int? tv = target == null ? null : target.Value;
			if (Action != null)
				Action(Type, evt, sn, sv, tn, tv);
			else if (Context.Action != null)
				Context.Action(Type, evt, sn, sv, tn, tv);
		}
	}

	[DebuggerDisplay("{Name}")]
	public class FsmGuard<T> where T : class
	{
		public string Name { get; private set; }
		public Func<string, bool> Condition { get; set; }
		public FsmContext<T> Context { get; private set; }

		private FsmGuard(FsmContext<T> context)
		{
			if (context == null)
				throw EX.ArgumentNull("context");
			Context = context;
		}

		public static FsmGuard<T> NewGuard(FsmContext<T> context, string name, Func<string, bool> condition = null)
		{
			if (context == null)
				throw EX.ArgumentNull("context");
			if (string.IsNullOrEmpty(name))
				throw EX.ArgumentNull("name");

			return new FsmGuard<T>(context) { Name = name, Condition = condition };
		}

		public bool Check(string evt, string sourceName, int? sourceVal, string targetName, int? targetVal)
		{
			MethodInfo method = typeof (T).GetMethod(Name);
			if (method != null && Context.Owner != null)
				return (bool) method.Invoke(Context.Owner, null);
			if (Condition != null)
				return Condition(Name);
			if (Context.Guard != null)
				return Context.Guard(evt, sourceName, sourceVal, targetName, targetVal);
			return true;
		}
	}

	public enum FsmStateType { Root, Simple, And, Or }
	public enum FsmActivityType { Inactive, Active, Stored }

	[DebuggerDisplay("{Name}, {Activity}, {Transitions.Count} transitions, {Children.Count} children, Parent={Parent.Name}")]
	public class FsmState<T> where T : class
	{
		public string Name { get; private set; }
		public FsmStateType StateType { get; private set; }
		public FsmActivityType Activity { get; private set; }
		public FsmAction<T> Enter { get; private set; }
		public FsmAction<T> Exit { get; private set; }
		public FsmState<T> Parent { get; private set; }
		public List<FsmState<T>> Children { get; private set; }
		public FsmContext<T> Context { get; private set; }
		public Type EnumType { get; private set; }
		public int? Value { get; private set; }
		public PropertyInfo Property { get; private set; }

		public List<FsmTransition<T>> Transitions { get; private set; }

		private FsmState(FsmContext<T> context)
		{
			if (context == null)
				throw new Exception("context");
			Context = context;
			Children = new List<FsmState<T>>();
			Transitions = new List<FsmTransition<T>>();
		}

		public static FsmState<T> NewState(FsmContext<T> context, string name, 
			FsmStateType stateType = FsmStateType.Or, FsmActivityType activityType = FsmActivityType.Inactive, 
			FsmAction<T> enter = null, FsmAction<T> exit = null, 
			Type enumType = null, int? value = null, PropertyInfo property = null)
		{
			if (context == null)
				throw EX.ArgumentNull("context");
			if (string.IsNullOrEmpty(name))
				throw EX.ArgumentNull("name");

			return new FsmState<T>(context) { Name = name, StateType = stateType, Activity = activityType, Enter = enter, Exit = exit, EnumType = enumType, Value = value, Property = property};
		}

		public string FullName
		{
			get
			{
				return Parent == null ? Name : (Parent.IsRoot ? FsmContext<T>.RootName + Name : Parent.FullName + @"\" + Name);
			}
		}

		public bool IsActive
		{
			get { return Activity == FsmActivityType.Active; }
		}

		public bool IsInactive
		{
			get { return Activity == FsmActivityType.Inactive; }
		}

		public bool IsStored
		{
			get { return Activity == FsmActivityType.Stored; }
		}

		public bool IsRoot
		{
			get { return StateType == FsmStateType.Root; }
		}

		public bool IsSimple
		{
			get { return StateType == FsmStateType.Simple; }
		}

		//public bool IsNode
		//{ get { return !IsRoot && Children.Any(); } }

		//public bool IsLeaf
		//{ get { return !IsRoot && !Children.Any(); } }

		public IEnumerable<FsmState<T>> Siblings
		{
			get
			{
				return Parent == null ? null : Parent.Children.Where(child => (child != this));
			}
		}

		public IEnumerable<FsmState<T>> ActiveChildren
		{
			get { return Children.Where(child => (child.IsActive)); }
		}

		public bool IsSibling(FsmState<T> state)
		{
			if (Parent == null || state.Parent == null)
				return false;
			return (Parent == state.Parent);
		}

		private bool CheckParent()
		{
			if (Parent == null)
				throw EX.ArgumentNull("Parent");
			if (Parent.IsSimple)
				throw EX.Argument("Parent cannot be Simple!");
			return !Parent.IsRoot;
		}

		internal static FsmActivityType CalcActivity(bool active, FsmActivityType parentActivity)
		{
			return active
					? (parentActivity == FsmActivityType.Active ? FsmActivityType.Active : FsmActivityType.Stored)
					: FsmActivityType.Inactive;
		}

		#region Related Addings
		internal FsmState<T> AddChild(FsmState<T> state)
		{
			state.Parent = this;
			Children.Add(state);
			if (StateType == FsmStateType.Simple)
				StateType = FsmStateType.Or;
			return state;
		}

		internal FsmState<T> AddChild(string name, FsmStateType stateType = FsmStateType.Simple, FsmActivityType activityType = FsmActivityType.Inactive, FsmAction<T> enter = null, FsmAction<T> exit = null, Type enumType = null, int? value = null, PropertyInfo property = null)
		{
			return AddChild(NewState(Context, name, stateType, activityType, enter, exit, enumType, value, property));
		}

		internal FsmState<T> AddSibling(FsmState<T> state)
		{
			if (Parent == null)
				throw new Exception("Parent is null! Adding sibling is impossible!");
			
			return Parent.AddChild(state);
		}

		internal FsmState<T> AddSibling(string name, FsmStateType stateType = FsmStateType.Simple, FsmActivityType activityType = FsmActivityType.Inactive, FsmAction<T> enter = null, FsmAction<T> exit = null)
		{
			return AddSibling(NewState(Context, name, stateType, activityType, enter, exit));
		}

		internal FsmState<T> AddUncle(FsmState<T> state)
		{
			if (Parent == null || Parent.Parent == null)
				throw new Exception("Parent or Grandfather is null! Adding uncle is impossible!");

			return Parent.Parent.AddChild(state);
		}

		internal FsmState<T> AddUncle(string name, FsmStateType stateType = FsmStateType.Simple, FsmActivityType activityType = FsmActivityType.Inactive, FsmAction<T> enter = null, FsmAction<T> exit = null)
		{
			return AddUncle(NewState(Context, name, stateType, activityType, enter, exit));
		}
		#endregion

		private FsmState<T> AddTransition(string evt, FsmState<T> source, FsmState<T> target, FsmGuard<T> guard = null, FsmAction<T> action = null, FsmTransitionType transitionType = FsmTransitionType.WithoutRestore)
		{
			Transitions.Add(FsmTransition<T>.NewTransition(Context, evt, source, target, guard, action, transitionType));
			return this;
		}

		internal FsmState<T> AddTargetTransition(string evt, FsmState<T> target, FsmGuard<T> guard = null, FsmAction<T> action = null, FsmTransitionType transitionType = FsmTransitionType.WithoutRestore)
		{
			return AddTransition(evt, this, target, guard, action, transitionType);
		}

		internal FsmState<T> AddSourceTransition(string evt, FsmState<T> source, FsmGuard<T> guard = null, FsmAction<T> action = null, FsmTransitionType transitionType = FsmTransitionType.WithoutRestore)
		{
			return AddTransition(evt, source, this, guard, action, transitionType);
		}

		private FsmState<T> AddSelfOffTransition(string evt, FsmGuard<T> guard = null, FsmAction<T> action = null, FsmTransitionType transitionType = FsmTransitionType.WithoutRestore)
		{
			return AddTargetTransition(evt, null, guard, action, transitionType:transitionType);
		}

		private FsmState<T> AddSelfOnTransition(string evt, FsmGuard<T> guard = null, FsmAction<T> action = null, FsmTransitionType transitionType = FsmTransitionType.WithoutRestore)
		{
			return AddSourceTransition(evt, null, guard, action, transitionType:transitionType);
		}

		internal void Store()
		{
			foreach (var child in Children)
			{
				if (child.IsActive)
					child.Activity = FsmActivityType.Stored;
				child.Store();
			}
		}

		internal void Restore()
		{
			foreach (var child in Children)
			{
				if (child.IsStored)
					child.Activity = FsmActivityType.Active;
				child.Restore();
			}
		}

		internal void Reset()
		{
			foreach (var child in Children)
			{
				if (child.IsStored || child.IsActive)
					child.Activity = FsmActivityType.Inactive;
				child.Reset();
			}
		}

		//public void Activate(bool withRestore = false)
		//{
		//	Activity = FsmActivityType.Active;
		//	if (withRestore)
		//		Restore();

		//	if (CheckParent())
		//	{
		//		switch (Parent.StateType)
		//		{
		//			case FsmStateType.Or:
		//				foreach (var s in Siblings.Where(s => s.IsActive))
		//				{
		//					s.Activity = FsmActivityType.Inactive;
		//					if (s.Exit != null) 
		//						s.Exit.Execute();
		//				}

		//				if (Parent.IsInactive)
		//					Parent.Activate();
		//				break;

		//			case FsmStateType.And:
		//				if (Parent.IsInactive && Siblings.All(s => s.IsActive))
		//					Parent.Activate();
		//				break;
		//		}
		//	}
		//	if (Enter != null)
		//		Enter.Execute();
		//}

		//public void Deactivate()
		//{
		//	Activity = FsmActivityType.Inactive;
		//	Store();

		//	if (CheckParent())
		//	{
		//		switch (Parent.StateType)
		//		{
		//			case FsmStateType.Or:
		//				if (Parent.IsActive && Siblings.All(s => s.IsInactive))
		//					Parent.Deactivate();
		//				break;

		//			case FsmStateType.And:
		//				if (Parent.IsActive)
		//					Parent.Deactivate();
		//				break;
		//		}
		//	}

		//	if (Exit != null)
		//		Exit.Execute();
		//}

		public void SetActive(bool withRestore = false)
		{
			Activity = FsmActivityType.Active;
			if (withRestore)
				Restore();

			if (CheckParent())
			{
				switch (Parent.StateType)
				{
					case FsmStateType.Or:
						foreach (var s in Siblings.Where(s => s.IsActive))
						{
							s.Activity = FsmActivityType.Inactive;
						}

						if (Parent.IsInactive)
							Parent.SetActive();
						break;

					case FsmStateType.And:
						if (Parent.IsInactive && Siblings.All(s => s.IsActive))
							Parent.SetActive();
						break;
				}
			}
		}

		public void SetInactive()
		{
			Activity = FsmActivityType.Inactive;
			Store();

			if (CheckParent())
			{
				switch (Parent.StateType)
				{
					case FsmStateType.Or:
						if (Parent.IsActive && Siblings.All(s => s.IsInactive))
							Parent.SetInactive();
						break;

					case FsmStateType.And:
						if (Parent.IsActive)
							Parent.SetInactive();
						break;
				}
			}
		}

		internal static void BuildBranchOff(FsmState<T> state, List<FsmState<T>> branchOff)
		{
			while (true)
			{
				state.Activity = FsmActivityType.Inactive;
				branchOff.Add(state);
				if (state.CheckParent() && state.Parent.IsActive)
				{
					state = state.Parent;
					continue;
				}
				break;
			}
		}

		//internal static void BuildBranchOff(FsmState state, List<FsmState> branchOff)
		//{
		//	state.Activity = FsmActivityType.Inactive;
		//	branchOff.Add(state);
		//	if (state.CheckParent() && state.Parent.IsActive)
		//		BuildBranchOff(state.Parent, branchOff);
		//}

		internal static void BuildBranchOn(FsmState<T> state, List<FsmState<T>> branchOn)
		{
			state.Activity = FsmActivityType.Active;

			if (state.CheckParent() && !state.Parent.IsActive)
				switch (state.Parent.StateType)
				{
					case FsmStateType.Or:
						BuildBranchOn(state.Parent, branchOn);
						foreach (var s in state.Siblings.Where(s => state.IsActive)) // ???
						{
							s.Activity = FsmActivityType.Inactive;
						}
						break;

					case FsmStateType.And:
						if (state.Siblings.All(s => s.IsActive))
							BuildBranchOn(state.Parent, branchOn);
						break;
				}

			branchOn.Add(state);
		}
	}
	
	public enum FsmTransitionType { WithoutRestore, WithRestore }

	[DebuggerDisplay("{Event}, Source={Source.Name}, Target={Target.Name}, Guard={Guard.Name}, Action={Action.Name}")]
	public class FsmTransition<T> where T : class
	{
		public string Event { get; private set; }
		public FsmState<T> Source { get; private set; }
		public FsmState<T> Target { get; private set; }
		public FsmGuard<T> Guard { get; private set; }
		public FsmAction<T> Action { get; private set; }
		public FsmTransitionType TransitionType { get; private set; }
		public FsmContext<T> Context { get; private set; }

		private FsmTransition(FsmContext<T> context)
		{
			if (context == null)
				throw EX.ArgumentNull("context");
			Context = context;
		}

		public static FsmTransition<T> NewTransition(FsmContext<T> context, string evt, FsmState<T> source, FsmState<T> target, FsmGuard<T> guard = null, FsmAction<T> action = null, FsmTransitionType transitionType = FsmTransitionType.WithoutRestore)
		{
			if (context == null)
				throw EX.ArgumentNull("context");
			if (string.IsNullOrEmpty(evt))
				throw EX.ArgumentNull("Event is empty");
			if (source == null && target == null)
				throw EX.Argument("Source and Target cannot be Empty");

			return new FsmTransition<T>(context) { Event = evt, Source = source, Target = target, Guard = guard, Action = action, TransitionType = transitionType };
		}

		public bool IsSwitchOn
		{
			get { return (Source == null && Target != null); }
		}

		public bool IsSwitchOff
		{
			get { return (Source != null && Target == null); }
		}

		public bool IsTransition
		{
			get { return (Source != null && Target != null); }
		}

		public bool Enabled
		{
			get
			{
				return Guard == null || Guard.Check(Event, Source.Name, Source.Value, Target.Name, Target.Value);
			}
		}

		public void Execute()
		{
			if (!Enabled)
				return;

			if (Source == null && Target == null)
				throw EX.Argument("Source and Target cannot be Empty");

			List<FsmState<T>> branchOn = new List<FsmState<T>>();
			List<FsmState<T>> branchOff = new List<FsmState<T>>();

			if (Source != null)
			{
				FsmState<T>.BuildBranchOff(Source, branchOff);
				Source.Store();
			}

			if (Target != null)
			{
				FsmState<T>.BuildBranchOn(Target, branchOn);
				if (TransitionType == FsmTransitionType.WithRestore)
					Target.Restore();
				else
					Target.Reset();
			}

			foreach (var state in branchOff.Intersect(branchOn).ToArray())
			{
				branchOff.Remove(state);
				branchOn.Remove(state);
			}

			foreach (var state in branchOff.Where(state => state.Exit != null))
			{
				state.Exit.Execute(Event, Source, Target);
			}

			if (Action != null)
				Action.Execute(Event, Source, Target);

			foreach (var state in branchOn)
			{
				if (state.Value != null && state.Parent.Property != null)
					state.Parent.Property.SetValue(Context.Owner, state.Value);
				if (state.Enter != null)
					state.Enter.Execute(Event, Source, Target);
			}
		}
	}

	public sealed class FsmContext<T> where T : class
	{
		public const string RootName = @"\";

		private readonly Queue<string> _eventQueue = new Queue<string>();
		private readonly Queue<FsmTransition<T>> _transitionQueue = new Queue<FsmTransition<T>>();

		public FsmState<T> RootState { get; private set; }
		public Dictionary<string, FsmState<T>> States { get; private set; }

		public Func<string, string, int?, string, int?, bool> Guard;
		public Action<FsmActionType, string, string, int?, string, int?> Action;

		public T Owner { get; set; }

		public FsmContext()
		{
			RootState = FsmState<T>.NewState(this, RootName, FsmStateType.Root, FsmActivityType.Active);
			States = new Dictionary<string, FsmState<T>> { { RootState.FullName, RootState } };
		}

		public IEnumerable<FsmState<T>> GetActiveStates()
		{
			return States.Values.Where(s => s.IsActive);
		}

		// !!! => works only for OR type of states and active AND-states (ready to switch off)
		// availability for not active AND-states (ready to switch on) is not clear
		
		public IEnumerable<string> GetAvailableEvents()
		{
			//from state in GetActiveStates() from transition in state.Transitions select transition.Event;))
			return GetActiveStates().SelectMany(state => state.Transitions, (state, transition) => transition.Event);
		}

		public bool SetState(string stateName, int stateValue)
		{
			var propertyState = States.Values.First(s => s.Property != null);
			if (propertyState == null) return false;

			var state = propertyState.Children.First(s => s.Value == stateValue);
			if (state == null) return false;

			state.SetActive();
			return true;
		}

		public void RunTransition(string evt)
		{
			_eventQueue.Clear();
			_transitionQueue.Clear();

			EnqueueEvent(evt);

			while (_eventQueue.Any())
			{
				DelegateEventToStates(_eventQueue.Dequeue(), RootState);

				while (_transitionQueue.Any())
				{
					_transitionQueue.Dequeue().Execute();
				}
			}
		}

		private void EnqueueEvent(string e)
		{
			_eventQueue.Enqueue(e);
		}

		private void EnqueueTransition(FsmTransition<T> t)
		{
			_transitionQueue.Enqueue(t);
		}

		private void DelegateEventToStates(string evt, FsmState<T> state)
		{
			foreach (var st in state.Children)
			{
				DelegateEventToStates(evt, st);
			}
			ProcessTransitions(evt, state);
		}

		private void ProcessTransitions(string evt, FsmState<T> state)
		{
			foreach (var t in state.Transitions.Where(t => t.Event == evt)) // ???
			{
				if	(
					(t.IsTransition && t.Source.IsActive && t.Target.IsInactive) ||
					(t.IsSwitchOn && t.Target.IsInactive) ||
					(t.IsSwitchOff && t.Source.IsActive)
					)
					EnqueueTransition(t);
			}
		}

		private void EnlistStates(FsmState<T> state)
		{
			if (!States.ContainsKey(state.FullName))
				States.Add(state.FullName, state);

			foreach (var child in state.Children)
			{
				EnlistStates(child);
			}
		}

		private FsmState<T> AddState(string name, FsmState<T> parent = null, FsmStateType stateType = FsmStateType.Simple, FsmActivityType activityType = FsmActivityType.Inactive, FsmAction<T> enter = null, FsmAction<T> exit = null, Type enumType = null, int? value = null, PropertyInfo property = null)
		{
			if (string.IsNullOrEmpty(name))
				throw EX.ArgumentNull("name");

			parent = parent ?? RootState;

			if (!States.ContainsValue(parent))
				throw new Exception("Inavlid parent");

			if (States.ContainsKey(parent.FullName + "\\" + name))
				throw new Exception("State already exists " + name);
			FsmState<T> state = parent.AddChild(name, stateType, activityType, enter, exit, enumType, value, property);
			States.Add(state.FullName, state);

			return state;
		}

		private FsmState<T> AddState(string name, string parentName = RootName, FsmStateType stateType = FsmStateType.Simple, bool active = false, Type enumType = null, int? value = null, PropertyInfo property = null)
		{
			if (string.IsNullOrEmpty(parentName))
				throw EX.ArgumentNull("parentName");

			if (!States.ContainsKey(parentName))
				throw EX.Argument("Wrong parent state name " + parentName);

			FsmState<T> parent = States[parentName];

			FsmActivityType activityType = FsmState<T>.CalcActivity(active, parent.Activity);

			FsmAction<T> enterAction = FsmAction<T>.NewAction(this, name + "Enter", FsmActionType.Enter, Action);
			FsmAction<T> exitAction = FsmAction<T>.NewAction(this, name + "Exit", FsmActionType.Exit, Action);

			return AddState(name, parent, stateType, activityType, enterAction, exitAction, enumType, value, property);
		}

		private void AddTransition(string evt, FsmState<T> source, FsmState<T> target, FsmTransitionType transitionType = FsmTransitionType.WithoutRestore, FsmAction<T> action = null, FsmGuard<T> guard = null)
		{
			if (string.IsNullOrEmpty(evt))
				throw EX.ArgumentNull("evt");
			if (source == null && target == null)
				throw EX.Argument("Source and target cannot be empty");
			if (source != null && !States.ContainsValue(source))
				throw EX.Argument("Invalid source state");
			if (target != null && !States.ContainsValue(target))
				throw EX.Argument("Invalid target state");

			if (source != null)
				source.AddTargetTransition(evt, target, guard, action, transitionType);
			else
				target.AddSourceTransition(evt, null, guard, action, transitionType);
		}

		private void AddTransition(string evt, string sourceName, string targetName, FsmTransitionType transitionType = FsmTransitionType.WithoutRestore, string guardName = null)
		{
			if (!string.IsNullOrEmpty(sourceName) && !States.ContainsKey(sourceName))
				throw EX.Argument("Wrong source state name " + sourceName);
			if (!string.IsNullOrEmpty(targetName) && !States.ContainsKey(targetName))
				throw EX.Argument("Wrong target state name " + targetName);

			FsmState<T> source = string.IsNullOrEmpty(sourceName) ? null : States[sourceName];
			FsmState<T> target = string.IsNullOrEmpty(targetName) ? null : States[targetName];

			FsmGuard<T> guard = FsmGuard<T>.NewGuard(this, string.IsNullOrEmpty(guardName) ? evt + "Guard" : guardName);
			FsmAction<T> action = FsmAction<T>.NewAction(this, evt, FsmActionType.Transition);

			AddTransition(evt, source, target, transitionType, action, guard);
		}

		#region Configuration
		public static FsmContext<T> FromXml(XmlReader reader)
		{
			FsmContext<T> result = new FsmContext<T>();

			if (reader.MoveToContent() != XmlNodeType.Element)
				reader.Read();
			string name = reader["name"] ?? reader.Name;

			List<FsmStateConfigRecord> stateConfigRecords = new List<FsmStateConfigRecord>();

			reader.Read();
			while (reader.MoveToContent() == XmlNodeType.Element && reader.Name == "fsmState")
			{
				FsmStateConfigRecord stateConfigRecord = FsmStateConfigRecord.FromXml(reader);
				if (stateConfigRecord != null)
					stateConfigRecords.Add(stateConfigRecord);
			}

			AddStates(result, stateConfigRecords);
			AddTransitions(result, stateConfigRecords);

			return result;
		}

		private static void AddStates(FsmContext<T> context, List<FsmStateConfigRecord> stateConfigRecords)
		{
			foreach (var stateConfigRecord in stateConfigRecords)
			{
				stateConfigRecord.FullName = context.AddState(stateConfigRecord.Name, stateConfigRecord.Parent != null ? stateConfigRecord.Parent.FullName : RootName, stateConfigRecord.Type, enumType:stateConfigRecord.EnumType, value: stateConfigRecord.Value, property: stateConfigRecord.Property).FullName;
				if (stateConfigRecord.StateConfigRecords != null && stateConfigRecord.StateConfigRecords.Any())
					AddStates(context, stateConfigRecord.StateConfigRecords);
			}
		}

		private static void AddTransitions(FsmContext<T> context, List<FsmStateConfigRecord> stateConfigRecords)
		{
			foreach (var stateConfigRecord in stateConfigRecords)
			{
				foreach (var transitionConfigRecord in stateConfigRecord.TransitionConfigRecords)
				{
					context.AddTransition(transitionConfigRecord.Event, stateConfigRecord.FullName, transitionConfigRecord.TargetStateName, transitionConfigRecord.Type, transitionConfigRecord.Guard);
				}
				if (stateConfigRecord.StateConfigRecords != null && stateConfigRecord.StateConfigRecords.Any())
					AddTransitions(context, stateConfigRecord.StateConfigRecords);
			}
		}

		[DebuggerDisplay("{Name}, {Parent.Name}, {FullName}")]
		class FsmStateConfigRecord
		{
			public string Name;
			public FsmStateConfigRecord Parent;
			public Type EnumType;
			public Dictionary<string, string> Attributes;
			public string FullName;
			public FsmStateType Type;
			public int? Value;
			public PropertyInfo Property;

			public readonly List<FsmTransitionConfigRecord> TransitionConfigRecords = new List<FsmTransitionConfigRecord>();
			public readonly List<FsmStateConfigRecord> StateConfigRecords = new List<FsmStateConfigRecord>();

			public static FsmStateConfigRecord FromXml(XmlReader reader, FsmStateConfigRecord parent = null)
			{
				FsmStateConfigRecord stateConfigRecord = new FsmStateConfigRecord {Attributes = ReadAttributes(reader)};

				if (stateConfigRecord.Attributes.ContainsKey("name"))
					stateConfigRecord.Name = stateConfigRecord.Attributes["name"];
				if (string.IsNullOrEmpty(stateConfigRecord.Name))
				{
					reader.Skip();
					return null;
				}

				stateConfigRecord.Parent = parent;

				stateConfigRecord.Type = FsmStateType.Simple;
				if (stateConfigRecord.Attributes.ContainsKey("type"))
				{
					switch (stateConfigRecord.Attributes["type"])
					{
						case "OR":
							stateConfigRecord.Type = FsmStateType.Or;
							break;
						case "AND":
							stateConfigRecord.Type = FsmStateType.And;
							break;
					}
				}

				if (stateConfigRecord.Attributes.ContainsKey("enum"))
					stateConfigRecord.EnumType = Factory.GetType(stateConfigRecord.Attributes["enum"]);
				if (parent != null && parent.Name != RootName && parent.EnumType != null)
				{
					stateConfigRecord.Value = (int) Enum.Parse(parent.EnumType, stateConfigRecord.Name, true);
				}
				if (stateConfigRecord.Attributes.ContainsKey("property"))
				{
					var p = stateConfigRecord.Attributes["property"];
					if (!string.IsNullOrEmpty(p))
						stateConfigRecord.Property = typeof(T).GetProperty(p); ;
				}

				if (!reader.IsEmptyElement && reader.Read())
				{
					while (reader.MoveToContent() == XmlNodeType.Element && reader.Name == "fsmTransition")
					{
						FsmTransitionConfigRecord transitionConfigRecord = FsmTransitionConfigRecord.FromXml(reader);
						if (transitionConfigRecord != null)
							stateConfigRecord.TransitionConfigRecords.Add(transitionConfigRecord);
					}
					while (reader.MoveToContent() == XmlNodeType.Element && reader.Name == "fsmState" && reader.NodeType != XmlNodeType.EndElement)
					{
						FsmStateConfigRecord childStateConfigRecord = FromXml(reader, stateConfigRecord);
						if (childStateConfigRecord != null)
							stateConfigRecord.StateConfigRecords.Add(childStateConfigRecord);
					}
				}
				reader.Skip();

				return stateConfigRecord;
			}
		}

		[DebuggerDisplay("{Event}, {TargetStateName}")]
		class FsmTransitionConfigRecord
		{
			public string Event;
			public string TargetStateName;
			public string Guard;
			public FsmTransitionType Type;
			public Dictionary<string, string> Attributes;

			public static FsmTransitionConfigRecord FromXml(XmlReader reader)
			{
				FsmTransitionConfigRecord transitionConfigRecord = new FsmTransitionConfigRecord {Attributes = ReadAttributes(reader)};

				if (transitionConfigRecord.Attributes.ContainsKey("event"))
					transitionConfigRecord.Event = transitionConfigRecord.Attributes["event"];
				if (string.IsNullOrEmpty(transitionConfigRecord.Event))
				{
					reader.Skip();
					return null;
				}

				if (transitionConfigRecord.Attributes.ContainsKey("target"))
					transitionConfigRecord.TargetStateName = transitionConfigRecord.Attributes["target"];
				if (transitionConfigRecord.Attributes.ContainsKey("guard"))
					transitionConfigRecord.Guard = transitionConfigRecord.Attributes["guard"];
				else
					transitionConfigRecord.Guard = transitionConfigRecord.Event + "Guard";

				transitionConfigRecord.Type = FsmTransitionType.WithoutRestore;
				if (transitionConfigRecord.Attributes.ContainsKey("type"))
				{
					switch (transitionConfigRecord.Attributes["type"])
					{
						case "withRestore":
							transitionConfigRecord.Type = FsmTransitionType.WithRestore;
							break;
					}
				}
				reader.Skip();

				return transitionConfigRecord;
			}
		}

		private static Dictionary<string, string> ReadAttributes(XmlReader reader)
		{
			Dictionary<string, string> result = new Dictionary<string, string>();

			reader.MoveToFirstAttribute();
			do
			{
				result.Add(reader.Name, reader.Value);
			} while (reader.MoveToNextAttribute());
			reader.MoveToElement();

			return result;
		}

		#endregion
	}
}
