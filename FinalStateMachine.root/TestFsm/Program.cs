﻿using System;
using FinalStateMachine;
using Lexxys;
using Lexxys.Configuration;
using Lexxys.Workflow;

namespace TestFsm
{
	class Program
	{
		static void Main(string[] args)
		{
			var expense = new Expense();
			Console.WriteLine("Current state: {0}\n", expense.State);

			var aes = expense.StateMachine.GetAvailableEvents();
			Console.WriteLine("\tAvailable events:");
			foreach (var ae in aes)
			{
				Console.WriteLine("\t{0}", ae);
			}
			
			expense.StateMachine.RunTransition(ExpenseTransition.SubmitForPayment.ToString());
			Console.WriteLine("Current state: {0}\n", expense.State);
			
			expense.StateMachine.RunTransition(ExpenseTransition.PaymentProcess.ToString());
			Console.WriteLine("Current state: {0}\n", expense.State);

			expense.StateMachine.RunTransition(ExpenseTransition.Cancel.ToString());
			Console.WriteLine("Current state: {0}\n", expense.State);

			//var taxReturn = new TaxReturn();
			//taxReturn.StateMachine.States[@"\State\GettingReady\NotReady"].SetActive();
			//TestTaxReturn(taxReturn);

			//TestTaxReceipt();

			//Console.Read();
		}

		private static void TestTaxReturn(TaxReturn taxReturn)
		{
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("IssuesResolved");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("SendToPreparation");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("PreparerStarts");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("UploadInstructions");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("IssuesDetected");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("Redo");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("SendToPreparation");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("Ignore");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("SendToClient");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("LogForClient");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("Review");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("SendToIRS");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();

			taxReturn.StateMachine.RunTransition("Finalize");
			PrintActiveStates(taxReturn.StateMachine);
			taxReturn.PrintValues();
		}

		//private static void TestTaxReceipt()
		//{
		//	var taxReceipt = new TaxReceipt();

		//	PrintActiveStates(taxReceipt.sm);

		//	taxReceipt.sm.ProcessEvent("AllocateDonation");
		//	PrintActiveStates(taxReceipt.sm);

		//	taxReceipt.sm.ProcessEvent("ReconcileAccount");
		//	PrintActiveStates(taxReceipt.sm);

		//	taxReceipt.sm.ProcessEvent("PriceAsset");
		//	PrintActiveStates(taxReceipt.sm);

		//	taxReceipt.sm.ProcessEvent("DealocateDonation");
		//	PrintActiveStates(taxReceipt.sm);

		//	Console.WriteLine("...\n");

		//	taxReceipt.sm.ProcessEvent("Print");
		//	PrintActiveStates(taxReceipt.sm);

		//	taxReceipt.sm.ProcessEvent("Print");
		//	PrintActiveStates(taxReceipt.sm);

		//	taxReceipt.sm.ProcessEvent("Print");
		//	PrintActiveStates(taxReceipt.sm);
		//}

		private static void PrintActiveStates<T>(FsmContext<T> sm) where T : class
		{
			Console.Write("\nActive States: ");
			foreach (var activeState in sm.GetActiveStates())
			{
				Console.Write(activeState.Name + ";");
			}
			Console.Write("\nAvailable Events: ");
			foreach (var s in sm.GetAvailableEvents())
			{
				Console.Write(s + ';');
			}

			Console.WriteLine();
		}
	}
}
