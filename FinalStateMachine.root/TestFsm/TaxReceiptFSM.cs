﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FinalStateMachine;

/* Events
 * 
 * AllocateDonation
 * DealocateDonation (fiction)
 * ReconsileAccount
 * PriceAsset
 *  
 * Print
 * 
 */

namespace TestFsm
{
	public class TaxReceipt 
	{
		//public FsmContext sm = new FsmContext();
		
		//public TaxReceipt()
		//{
		//	InitiateStateTree();
		//}

		//private void InitiateStateTree()
		//{
		//	#region ~ProcessedStatus~
		//	sm.RootState
		//		.AddChild("ProcessedStatus", FsmStateType.Or, FsmActivityType.Active, FsmAction.NewAction("ProcessedStatus", EnterActions), FsmAction.NewAction("ProcessedStatus", ExitActions))
		//			.AddChild("Initial", FsmStateType.Simple, FsmActivityType.Active, FsmAction.NewAction("Initial", EnterActions), FsmAction.NewAction("Initial", ExitActions))
		//			.AddSibling("Printed", FsmStateType.Simple, FsmActivityType.Inactive, FsmAction.NewAction("Printed", EnterActions), FsmAction.NewAction("Printed", ExitActions))
		//			.AddSibling("Revised", FsmStateType.Simple, FsmActivityType.Inactive, FsmAction.NewAction("Revised", EnterActions), FsmAction.NewAction("Revised", ExitActions));
		//	#endregion

		//	#region ~IsFinal~
		//	sm.RootState
		//		.AddChild("IsFinal", FsmStateType.And, FsmActivityType.Inactive, FsmAction.NewAction("IsFinal", EnterActions), FsmAction.NewAction("IsFinal", ExitActions))
		//			.AddChild("DonationAllocated", FsmStateType.Simple, FsmActivityType.Inactive, FsmAction.NewAction("DonationAllocated", EnterActions), FsmAction.NewAction("DonationAllocated", ExitActions))
		//			.AddSibling("AccountReconciled", FsmStateType.Simple, FsmActivityType.Inactive, FsmAction.NewAction("AccountReconciled", EnterActions), FsmAction.NewAction("AccountReconciled", ExitActions))
		//			.AddSibling("AssetPriced", FsmStateType.Simple, FsmActivityType.Inactive, FsmAction.NewAction("AssetPriced", EnterActions), FsmAction.NewAction("AssetPriced", ExitActions));

		//	#endregion

		//	sm.EnlistStates(sm.RootState);

		//	#region ~ProcessedStatus Transitions~
		//	sm.States["Root.ProcessedStatus.Initial"]
		//		.AddTargetTransition("Print", sm.States["Root.ProcessedStatus.Printed"], null,
		//			FsmAction.NewAction("Print1", (s) => Console.WriteLine("TaxReceipt was printed for the first time on {0}", DateTime.Now)));
		//	sm.States["Root.ProcessedStatus.Printed"]
		//		.AddTargetTransition("Print", sm.States["Root.ProcessedStatus.Revised"], null,
		//			FsmAction.NewAction("Print2", (s) => Console.WriteLine("TaxReceipt was printed for the second time on {0}", DateTime.Now)));
		//	#endregion

		//	#region ~IsFinal Transitions~
		//	sm.States["Root.IsFinal.DonationAllocated"].AddSelfOnTransition("AllocateDonation", null, FsmAction.NewAction("AllocateDonation", Actions));
		//	sm.States["Root.IsFinal.DonationAllocated"].AddSelfOffTransition("DealocateDonation", null, FsmAction.NewAction("DealocateDonation", Actions));
		//	sm.States["Root.IsFinal.AccountReconciled"].AddSelfOnTransition("ReconcileAccount", null, FsmAction.NewAction("ReconcileAccount", Actions));
		//	sm.States["Root.IsFinal.AssetPriced"].AddSelfOnTransition("PriceAsset", null, FsmAction.NewAction("PriceAsset", Actions));
		//	#endregion            
			
		//}

		//private static bool Guards(string guardName)
		//{
		//	return true;
		//}

		//private static void EnterActions(string actionName)
		//{
		//	Console.WriteLine("{0} {1} ENTER", DateTime.Now, actionName);
		//	return;
		//}

		//private static void ExitActions(string actionName)
		//{
		//	Console.WriteLine("{0} {1} EXIT", DateTime.Now, actionName);
		//	return;
		//}

		//private static void Actions(string actionName)
		//{
		//	string from = "", to = "";
		//	switch (actionName)
		//	{
		//		case "AllocateDonation":
		//			from = "";
		//			to = "DonationAllocated";
		//			break;
		//		case "DealocateDonation":
		//			from = "DonationAllocated";
		//			to = "";
		//			break;
		//		case "ReconcileAccount":
		//			from = "";
		//			to = "AccountReconciled";
		//			break;
		//		case "PriceAsset":
		//			from = "";
		//			to = "AssetPriced";
		//			break;
		//	}
		//	Console.WriteLine("{0} {1}: from '{2}' to '{3}'", DateTime.Now, actionName, from, to);
		//}

	}
}
