﻿using System;
using FinalStateMachine;
using Lexxys;

namespace TestFsm
{
	public enum ExpenseState
	{
		Removed = -2,
		Canceled = -1,
		AdminDraft = 0,
		Review = 10,
		FinOpsReview = 25,
		PaymentProcessing = 30,
		Finalized = 99
	}

	public enum ExpenseTransition
	{
		SubmitForPayment,
		SubmitForReview,
		Cancel,
		Delete,
		MoreInfo,
		PaymentProcess,
		PaymentDone
	}

	public class Expense
	{
		public ExpenseState State { get; set; }
		public FsmContext<Expense> StateMachine;

		public Expense()
		{
			StateMachine = Config.GetValue<FsmContext<Expense>>("StateMachines.Expense");
			StateMachine.Owner = this;
			StateMachine.Action = SmAction;					// Action<FsmActionType, string, string, int?, string, int? >
			StateMachine.Guard = SmGuard;					// Func<string, string, int?, string, int? bool>

			State = ExpenseState.Review;
			StateMachine.SetState("State", (int) State);
		}

		public bool SmGuard(string trans, string sourceName, int? sourceVal, string targetName, int? targetVal)
		{
			Console.WriteLine("Common Guard: {0}, {1}, {2}, {3}, {4}\n", trans, sourceName, sourceVal, targetName, targetVal);
	
			ExpenseTransition t;
			Enum.TryParse(trans, true, out t);
			ExpenseState source = (ExpenseState)sourceVal.GetValueOrDefault();
			ExpenseState target = (ExpenseState)targetVal.GetValueOrDefault();
			ExpenseState source2;
			Enum.TryParse(sourceName, true, out source2);
			ExpenseState target2;
			Enum.TryParse(targetName, true, out target2);
			Console.WriteLine("\t{0}, {1}, {2}, {3}, {4}\n", t, source, source2, target, target2);

			return true;
		}

		public void SmAction(FsmActionType type, string trans, string sourceName, int? sourceVal, string targetName, int? targetVal)
		{
			ExpenseTransition t;
			Enum.TryParse(trans, true, out t);
			Console.WriteLine("Common Action: {0}, {1}, {2}, {3}, {4}, {5}\n", type, trans, sourceName, sourceVal, targetName, targetVal);
		}

		public void SubmitForPayment()
		{
			Console.WriteLine("SubmitForPayment()\n");
		}
		public bool SubmitForPaymentGuard()
		{
			Console.WriteLine("SubmitForPaymentGuard\n");
			return true;
		}

		public void FinOpsReviewEnter()
		{
			Console.WriteLine("FinOpsReviewEnter()\n");
		}

		public void FinOpsReviewExit()
		{
			Console.WriteLine("FinOpsReviewExit()\n");
		}

		public void SubmitForReview()
		{
			Console.WriteLine("SubmitForReview()\n");
		}
		public void Cancel()
		{
			Console.WriteLine("Cancel()\n");
		}
		public bool GuardCancel()
		{
			Console.WriteLine("GuardCancel\n");
			return true;
		}
		public void Delete()
		{
			Console.WriteLine("Delete()\n");
		}
		public void MoreInfo()
		{
			Console.WriteLine("MoreInfo()\n");
		}
		public void PaymentProcess()
		{
			Console.WriteLine("PaymentProcess()\n");
		}
		public void PaymentDone()
		{
			Console.WriteLine("PaymentDone()\n");
		}
	}
}
