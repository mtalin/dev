﻿using System;
using FinalStateMachine;
/*
 * Events
 * 
 * IssuesResolved
 * SendToPreparation
 * IssuesDetected
 * PreparerStarts
 * UploadInstructions
 * SendToClient
 * LogForClient
 * Review
 * SendToIRS
 * Ignore
 * Redo
 * Finalize
 * 
 */
using Lexxys;

namespace TestFsm
{
	public enum TaxReturnState1
	{
		Default = 0,
		GettingReady = 10,
		Wip = 20,
		InPrepIssues = 30,
		SentToIrs = 95,
		Finalized = 99,
	}

	public enum TaxReturnState2
	{
		Default = 0,
		NotReady = 10,
		Ready = 99,
	}

	public enum TaxReturnState3
	{
		None = -1,
		Default = 0,
		AssignedForPreparation = 5,
		InPrep = 10,
		ReadyForReview = 20,
		AssignedForReview = 25,
		FirstReview = 30,
		SecondReview = 40,
		FinalCorrection = 50,
		FinalReview = 55,
		PendingUpload = 60,
		TaxCenterReview = 70,
	}

	public enum TaxReturnState4
	{
		None = -1,
		Default = 0,
		PendingClientReview = 30,
		ClientsComment = 40,
		Awaiting8879 = 45,
		Recieved8879 = 48,
		ReadyEFile = 50,
		Transmitted = 53,
		FailedEFile = 55,
		ReadyToPrint = 60,
		ReadyToMail = 65,
		SentToClient = 70,
		SignedMailedByClient = 80,
		Recieved = 85,
		Assembled = 87,
		ReadyForFiling = 90,
	}

	class TaxReturn
	{
		public TaxReturnState1 State1 { get; set; }
		public TaxReturnState2 State2 { get; set; }
		public TaxReturnState3 State3 { get; set; }
		public TaxReturnState4 State4 { get; set; }

		public FsmContext<TaxReturn> StateMachine;

		public TaxReturn()
		{
			StateMachine = Config.GetValue<FsmContext<TaxReturn>>("StateMachines.TaxReturn");
			StateMachine.Owner = this;
			//StateMachine.Action = Actions;
			//StateMachine.Guard = Guards;
			State1 = TaxReturnState1.GettingReady;
			State2 = TaxReturnState2.NotReady;
			State3 = TaxReturnState3.TaxCenterReview;
			State4 = TaxReturnState4.Default;
		}

		//public void InitiateStateMachine()
		//{
		//	StateMachine.Action = Actions;
		//	StateMachine.Guard = Guards;

		//	#region ~Status~
		//	//sm.RootState
		//	//    .AddChild(new FsmState { Name = "Satus", StateType = FsmStateType.Or, Activity = FsmActivityType.Inactive })
		//	//        .AddChild(new FsmState { Name = "NotResponsible", StateType = FsmStateType.Or, Activity = FsmActivityType.Inactive })
		//	//        .AddSibling(new FsmState { Name = "NotReady", StateType = FsmStateType.Or, Activity = FsmActivityType.Inactive })
		//	//        .AddSibling(new FsmState { Name = "Ready", StateType = FsmStateType.Or, Activity = FsmActivityType.Inactive })
		//	//        .AddSibling(new FsmState { Name = "SentToPrep", StateType = FsmStateType.Or, Activity = FsmActivityType.Inactive })
		//	//        .AddSibling(new FsmState { Name = "SentWithIssues", StateType = FsmStateType.Or, Activity = FsmActivityType.Inactive })
		//	//        .AddSibling(new FsmState { Name = "SentToProcessing", StateType = FsmStateType.Or, Activity = FsmActivityType.Inactive })
		//	//        .AddSibling(new FsmState { Name = "SentToIRS", StateType = FsmStateType.Or, Activity = FsmActivityType.Inactive })
		//	//        .AddSibling(new FsmState { Name = "Finalized", StateType = FsmStateType.Or, Activity = FsmActivityType.Inactive });
		//	#endregion

		//	#region ~States~

		//	#region ~Old variant~
		//	//sm.RootState
		//	//    .AddChild("State", FsmStateType.Or, stateAct)
		//	//        .AddChild("GettingReady", FsmStateType.Or, gettingReadyAct, FsmAction.NewAction("GettingReady", EnterActions), FsmAction.NewAction("GettingReady", ExitActions))
		//	//            .AddChild("NotReady", FsmStateType.Simple, notReadyAct, FsmAction.NewAction("NotReady", EnterActions), FsmAction.NewAction("NotReady", ExitActions))
		//	//            .AddSibling("Ready", FsmStateType.Simple, readyAct, FsmAction.NewAction("Ready", EnterActions), FsmAction.NewAction("Ready", ExitActions))
		//	//        .AddUncle("InPrepIssues", FsmStateType.Simple, inPrepIssuesAct, FsmAction.NewAction("InPrepIssues", EnterActions), FsmAction.NewAction("InPrepIssues", ExitActions))
		//	//        .AddSibling("SentToIRS", FsmStateType.Simple, sentToIRSAct, FsmAction.NewAction("SentToIRS", EnterActions), FsmAction.NewAction("SentToIRS", ExitActions))
		//	//        .AddSibling("Finalized", FsmStateType.Simple, finalizedAct, FsmAction.NewAction("Finalized", EnterActions), FsmAction.NewAction("Finalized", ExitActions))
		//	//        .AddSibling("WIP", FsmStateType.Or, wipAct, FsmAction.NewAction("WIP", EnterActions), FsmAction.NewAction("WIP", ExitActions))
		//	//            .AddChild("InPreparation", FsmStateType.Or, inPreparationAct, FsmAction.NewAction("InPreparation", EnterActions), FsmAction.NewAction("InPreparation", ExitActions))
		//	//                .AddChild("AssignedForPreparation", FsmStateType.Simple, assignedForPrepAct, FsmAction.NewAction("AssignedForPreparation", EnterActions), FsmAction.NewAction("AssignedForPreparation", ExitActions))
		//	//                .AddSibling("InPrep", FsmStateType.Simple, inPrepAct, FsmAction.NewAction("InPrep", EnterActions), FsmAction.NewAction("InPrep", ExitActions))
		//	//                .AddSibling("TaxCenterReview", FsmStateType.Or, taxCenterReviewAct, FsmAction.NewAction("TaxCenterReview", EnterActions), FsmAction.NewAction("TaxCenterReview", ExitActions))
		//	//            .AddUncle("Processing", FsmStateType.Or, processingAct, FsmAction.NewAction("Processing", EnterActions), FsmAction.NewAction("Processing", ExitActions))
		//	//                .AddChild("PendingClientReview", FsmStateType.Simple, pendingClientReviewAct, FsmAction.NewAction("PendingClientReview", EnterActions), FsmAction.NewAction("PendingClientReview", ExitActions))
		//	//                .AddSibling("Received", FsmStateType.Simple, receivedAct, FsmAction.NewAction("Received", EnterActions), FsmAction.NewAction("Received", ExitActions))
		//	//                .AddSibling("ReadyForFilling", FsmStateType.Simple, readyForFillingAct, FsmAction.NewAction("ReadyForFilling", EnterActions), FsmAction.NewAction("ReadyForFilling", ExitActions));
		//	//sm.EnlistStates(sm.RootState);
		//	#endregion

		//	StateMachine.AddState("State", @"\", FsmStateType.Or, State1 > TaxReturnState1.Default);

		//	StateMachine.AddState("GettingReady", @"\State", FsmStateType.Or, State1 == TaxReturnState1.GettingReady);
		//	StateMachine.AddState("NotReady", @"\State\GettingReady", FsmStateType.Simple, State2 == TaxReturnState2.NotReady);
		//	StateMachine.AddState("Ready", @"\State\GettingReady", FsmStateType.Simple, State2 == TaxReturnState2.Ready);
		//	StateMachine.AddState("WIP", @"\State", FsmStateType.Or, State1 == TaxReturnState1.Wip);
		//	StateMachine.AddState("InPreparation", @"\State\WIP", FsmStateType.Or, State3 > TaxReturnState3.Default);
		//	StateMachine.AddState("AssignedForPreparation", @"\State\WIP\InPreparation", FsmStateType.Simple, State3 == TaxReturnState3.AssignedForPreparation);
		//	StateMachine.AddState("InPrep", @"\State\WIP\InPreparation", FsmStateType.Simple, State3 == TaxReturnState3.InPrep);
		//	StateMachine.AddState("TaxCenterReview", @"\State\WIP\InPreparation", FsmStateType.Simple, State3 == TaxReturnState3.TaxCenterReview);
		//	StateMachine.AddState("Processing", @"\State\WIP", FsmStateType.Or, State4 > TaxReturnState4.Default);
		//	StateMachine.AddState("PendingClientReview", @"\State\WIP\Processing", FsmStateType.Simple, State4 == TaxReturnState4.PendingClientReview);
		//	StateMachine.AddState("Received", @"\State\WIP\Processing", FsmStateType.Simple, State4 == TaxReturnState4.Recieved);
		//	StateMachine.AddState("ReadyForFilling", @"\State\WIP\Processing", FsmStateType.Simple, State4 == TaxReturnState4.ReadyForFiling);
		//	StateMachine.AddState("InPrepIssues", @"\State", FsmStateType.Simple, State1 == TaxReturnState1.InPrepIssues);
		//	StateMachine.AddState("SentToIRS", @"\State", FsmStateType.Simple, State1 == TaxReturnState1.SentToIrs);
		//	StateMachine.AddState("Finalized", @"\State", FsmStateType.Simple, State1 == TaxReturnState1.Finalized);
		//	#endregion

		//	#region ~Transitions~

		//	#region ~Root.State.GettingReady~
		//	#region ~Variant~
		//	//sm.States["Root.State.GettingReady.NotReady"]
		//	//    .AddTargetTransition("IssuesResolved",
		//	//        sm.States["Root.State.GettingReady.Ready"],
		//	//        FsmGuard.NewGuard("IssuesResolved", Guards),
		//	//        FsmAction.NewAction("IssuesResolved", Actions));

		//	//sm.States["Root.State.GettingReady.Ready"]
		//	//    .AddTargetTransition("SendToPreparation", 
		//	//        sm.States["Root.State.WIP.InPreparation.AssignedForPreparation"],
		//	//        FsmGuard.NewGuard("SendToPreparation", Guards),
		//	//        FsmAction.NewAction("SendToPreparation", Actions));
		//	#endregion

		//	StateMachine.AddTransition("IssuesResolved", @"\State\GettingReady\NotReady", @"\State\GettingReady\Ready");
		//	StateMachine.AddTransition("SendToPreparation", @"\State\GettingReady\Ready", @"\State\WIP\InPreparation\AssignedForPreparation");
		//	#endregion

		//	#region ~Root.State.WIP~
		//	#region ~Variant~
		//	//sm.States["Root.State.WIP"]
		//	//    .AddTargetTransition("IssuesDetected", 
		//	//        sm.States["Root.State.InPrepIssues"],
		//	//        FsmGuard.NewGuard("IssuesDetected", Guards),
		//	//        FsmAction.NewAction("IssuesDetected", Actions));

		//	//sm.States["Root.State.WIP.InPreparation.AssignedForPreparation"]
		//	//    .AddTargetTransition("PreparerStarts", 
		//	//        sm.States["Root.State.WIP.InPreparation.InPrep"],
		//	//        FsmGuard.NewGuard("PreparerStarts", Guards),
		//	//        FsmAction.NewAction("PreparerStarts", Actions));

		//	//sm.States["Root.State.WIP.InPreparation.InPrep"]
		//	//    .AddTargetTransition("UploadInstructions", 
		//	//        sm.States["Root.State.WIP.InPreparation.TaxCenterReview"],
		//	//        FsmGuard.NewGuard("UploadInstructions", Guards),
		//	//        FsmAction.NewAction("UploadInstructions", Actions));

		//	//sm.States["Root.State.WIP.InPreparation.TaxCenterReview"]
		//	//    .AddTargetTransition("SendToClient", 
		//	//        sm.States["Root.State.WIP.Processing.PendingClientReview"],
		//	//        FsmGuard.NewGuard("SendToClient", Guards),
		//	//        FsmAction.NewAction("SendToClient", Actions));

		//	//sm.States["Root.State.WIP.Processing.PendingClientReview"]
		//	//    .AddTargetTransition("LogForClient", 
		//	//        sm.States["Root.State.WIP.Processing.Received"],
		//	//        FsmGuard.NewGuard("LogForClient", Guards),
		//	//        FsmAction.NewAction("LogForClient", Actions));

		//	//sm.States["Root.State.WIP.Processing.Received"]
		//	//    .AddTargetTransition("Review", 
		//	//        sm.States["Root.State.WIP.Processing.ReadyForFilling"],
		//	//        FsmGuard.NewGuard("Review", Guards),
		//	//        FsmAction.NewAction("Review", Actions));

		//	//sm.States["Root.State.WIP.Processing.ReadyForFilling"]
		//	//    .AddTargetTransition("SendToIRS", 
		//	//        sm.States["Root.State.SentToIRS"],
		//	//        FsmGuard.NewGuard("SendToIRS", Guards),
		//	//        FsmAction.NewAction("SendToIRS", Actions));
		//	#endregion

		//	StateMachine.AddTransition("IssuesDetected", @"\State\WIP", @"\State\InPrepIssues");
		//	StateMachine.AddTransition("PreparerStarts", @"\State\WIP\InPreparation\AssignedForPreparation", @"\State\WIP\InPreparation\InPrep");
		//	StateMachine.AddTransition("UploadInstructions", @"\State\WIP\InPreparation\InPrep", @"\State\WIP\InPreparation\TaxCenterReview");
		//	StateMachine.AddTransition("SendToClient", @"\State\WIP\InPreparation\TaxCenterReview", @"\State\WIP\Processing\PendingClientReview");
		//	StateMachine.AddTransition("LogForClient", @"\State\WIP\Processing\PendingClientReview", @"\State\WIP\Processing\Received");
		//	StateMachine.AddTransition("Review", @"\State\WIP\Processing\Received", @"\State\WIP\Processing\ReadyForFilling");
		//	StateMachine.AddTransition("SendToIRS", @"\State\WIP\Processing\ReadyForFilling", @"\State\SentToIRS");

		//	#endregion

		//	#region ~Root.State.InPrepIssues~
		//	#region ~variant~
		//	//sm.States["Root.State.InPrepIssues"]
		//	//    .AddTargetTransition("Redo", 
		//	//        sm.States["Root.State.GettingReady"], 
		//	//        FsmGuard.NewGuard("Redo", Guards),
		//	//        FsmAction.NewAction("Redo", Actions));

		//	//sm.States["Root.State.InPrepIssues"]
		//	//    .AddTargetTransition("Ignore", 
		//	//        sm.States["Root.State.WIP"],
		//	//        FsmGuard.NewGuard("Ignore", Guards),
		//	//        FsmAction.NewAction("Ignore", Actions),
		//	//        FsmTransitionType.WithRestore);

		//	//sm.States["Root.State.SentToIRS"]
		//	//    .AddTargetTransition("Finalize", 
		//	//        sm.States["Root.State.Finalized"],
		//	//        FsmGuard.NewGuard("Finalize", Guards),
		//	//        FsmAction.NewAction("Finalize", Actions));
		//	#endregion

		//	StateMachine.AddTransition("Redo", @"\State\InPrepIssues", @"\State\GettingReady");
		//	StateMachine.AddTransition("Ignore", @"\State\InPrepIssues", @"\State\WIP", FsmTransitionType.WithRestore);
		//	StateMachine.AddTransition("Finalize", @"\State\SentToIRS", @"\State\Finalized");

		//	#endregion

		//	#endregion
		//}
		
		public bool Guards(string guardName)
		{
			switch (guardName)
			{
				case "IssuesResolved.Guard":
					return true;
			}
			return true;
		}

		public bool CheckReady()
		{
			return true;
		}

		public void Actions(string actionName)
		{
			if (actionName.EndsWith(".Enter"))
			{
				EnterActions(actionName);
			}
			else if (actionName.EndsWith(".Exit"))
			{
				ExitActions(actionName);
			}
			else
			{
				TransitionActions(actionName);
			}
		}

		private void EnterActions(string actionName)
		{
			switch (actionName)
			{
				case "GettingReady.Enter":
					State1 = TaxReturnState1.GettingReady;
					//CalcReady();
					break;
				case "WIP.Enter":
					State1 = TaxReturnState1.Wip;
					break;
				case "InPrepIssues.Enter":
					State1 = TaxReturnState1.InPrepIssues;
					break;
				case "SentToIRS.Enter":
					State1 = TaxReturnState1.SentToIrs;
					break;
				case "Finalized.Enter":
					State1 = TaxReturnState1.Finalized;
					break;
				case "NotReady.Enter":
					State2 = TaxReturnState2.NotReady;
					break;
				case "Ready.Enter":
					State2 = TaxReturnState2.Ready;
					break;
				case "AssignedForPreparation.Enter":
					State3 = TaxReturnState3.AssignedForPreparation;
					break;
				case "InPrep.Enter":
					State3 = TaxReturnState3.InPrep;
					break;
				case "TaxCenterReview.Enter":
					State3 = TaxReturnState3.TaxCenterReview;
					break;
				case "PendingClientReview.Enter":
					State4 = TaxReturnState4.PendingClientReview;
					break;
				case "Received.Enter":
					State4 = TaxReturnState4.Recieved;
					break;
				case "ReadyForFilling.Enter":
					State4 = TaxReturnState4.ReadyForFiling;
					break;
			}

			Console.WriteLine("{0} {1}", DateTime.Now, actionName);
		}

		private void ExitActions(string actionName)
		{
			Console.WriteLine("{0} {1}", DateTime.Now, actionName);
		}

		public void IssuesResolved()
		{
			Console.WriteLine("********* IssuesResolved ********");
		}
		private void TransitionActions(string actionName)
		{
			string from = "", to = "";
			switch (actionName)
			{
				case "IssuesResolved":
					from = "NotReady";
					to = "Ready";
					break;
				case "SendToPreparation":
					from = "Ready";
					to = "AssignedForPreparation";
					break;
				case "IssuesDetected":
					from = "WIP";
					to = "InPrepIssues";
					break;
				case "PreparerStarts":
					from = "AssignedForPreparation";
					to = "InPrep";
					break;
				case "UploadInstructions":
					from = "InPrep";
					to = "TaxCenterReview";
					break;
				case "SendToClient":
					from = "TaxCenterReview";
					to = "PendingClientReview";
					break;
				case "LogForClient":
					from = "PendingClientReview";
					to = "Received";
					break;
				case "Review":
					from = "Received";
					to = "ReadyForFilling";
					break;
				case "SendToIRS":
					from = "ReadyForFilling";
					to = "SentToIRS";
					break;
				case "Redo":
					from = "InPrepIssues";
					to = "GettingReady";
					//CalcReady();
					break;
				case "Ignore":
					from = "InPrepIssues";
					to = "WIP";
					break;
				case "Finalize":
					from = "SentToIRS";
					to = "Finalized";
					break;
			}
			Console.WriteLine("{0} {1}: from '{2}' to '{3}'", DateTime.Now, actionName, from, to);
		}
		
		//private void CalcReady()
		//{
		//	var r = new Random();
		//	if (r.Next() % 2 == 0)
		//		StateMachine.States[@"\State\GettingReady\NotReady"].Activate();
		//	else
		//		StateMachine.States[@"\State\GettingReady\Ready"].Activate();
		//}

		public void PrintValues()
		{
			Console.WriteLine("State1={0} State2={1} State3={2} State4={3}\n", State1, State2, State3, State4);
		}
	}
}
